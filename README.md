Instructions on how to import CMS-Demo project:


1. Register at our site: http://querix.com/register/

2. Download the latest Lycia III version:
	http://querix.com/lycia/downloads/
	
3. Obtain a trial license to be able to compile the project:
	
    a) install Lycia III. Here is a link to the installation procedure described in our documentation:
http://querix.com/go/docs_online/#lycia_installation/win_install.htm
	
    b) activate the trial license (1 license per 1 developer):
http://querix.com/go/docs_online/#lycia_licensing/installing_and_activating_trial_licenses.htm

Please contact us if you need more compilation seats. We'd also appreciate you letting us know the number of developers who will be engaged in this project.

4. Import CMS-Demo project from the GIT repository via LyciaStudio. GIT plugin is embedded into LyciaStudio. Please refer to the documentation on importing a project from GIT into LyciaStudio:
http://querix.com/go/docs_online/#developers_guide/working_with_repositories/git/git_perspective_and_views/git_repos_view/clone_repo.htm

Use the link below to download CMS-Demo project source files. Here is the link to be cloned:
https://gitlab.com/QuerixDemos/cms.git

Here is the link to Lycia documentation on GIT usage:
http://querix.com/go/docs_online/#developers_guide/working_with_repositories/repos.htm

5. Set up the connection to INFORMIX (or any other database you prefer).

6. Create the 'cms' database with logging.
We usually use DBAccess Informix tool:
https://www.ibm.com/support/knowledgecenter/SSGU8G_11.70.0/com.ibm.dba.doc/ids_dba_005.htm

Please execute the following query in DBAccess:
CREATE DATABASE cms WITH LOG;

7. Configure inet.env configuration file (default location - C:\ProgramData\Querix\Lycia\etc).
LOGNAME={Informix user login}
INFORMIXPASS={Informix user password}
LYCIA_DB_DRIVER=informix
INFORMIXDIR=C:\Program Files\IBM Informix Client SDK    #Default path to Informix SDK Client on Windows
INFORMIXSERVER={Informix Server name}
DBDATE=MDY4/   

8. Launch db_schema_tools application
    a)Build and Deploy the db_schema_tools application by right-clicking on it and choosing 'Build' from the context menu. 
        Please refer to our documentation on this:
        http://querix.com/go/docs_online/#lyciastudio/build/building_projects/building_and_compilation.htm

    b) Run the application via LyciaWeb graphical client
    c)  Click on ‘Re-Create Database’ button to import data into your database.
        Please skip ‘Warning - UNL Load File unl/qxt_toolbar.unl is empty!’ messages by clicking OK.

Launch CMS DataBase project (by following the instructions below).


Instructions on project launching via LyciaStudio:
1. Build the following modules. 
- cmsActivity   		- cmsMailInbox 	 - cmsCompany  	
- cms_mdi_menu   	- cmsContact		- cmsStock


To select all the modules at once, please keep the CTRL button pressed 


2. Deploy the selected modules to a server by right-clicking them and choosing the 'Deploy to Server' option from the context menu.


3. Set up Run Configurations for 'cms_mdi_menu' module:
    a) Create a new “Launch Configuration’ and add ‘lacy 4’ as an argument;
    b) Switch to ‘Graphical Clients’ tab and choose ‘LyciaWeb’ or LyciaDesktop.
    c) In ‘Application Server’ tab choose ‘cms_2016’ subdirectory.
    d) Click ‘Run’. 
    
After launching your CMS Demo project should look the same way as it is on our site:
http://localhost:9090/LyciaWeb/run/default/cms_2016/cms_mdi_menu.exe?params=-d%20informix%20lacy%204 


We’d appreciate you to clear your browsing data prior to launching the application.
