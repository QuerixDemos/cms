/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

querix.plugins.frontCallModuleList.inflair = {
	fixLoginTab: function() {
		var t = $("#qx-t-c");
		$(".qx-page-0-header").remove();
		cnt = $("#qx-page-0-content");
		cnt.children().detach();
		cnt.remove();
		t.tabs("refresh");
	}
}; 