/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

(function(querix){

	var $ = querix.$;
	var api = querix.plugins.api;
	var get$ = function() {
    	return api.getWindow(api.topWindow()).$;
	};

	querix.plugins.frontCallModuleList.tooltipster = {
	    tooltip: function(selector, content, type, position, timeout) {
	    	timeout = parseInt(timeout);
	    	if (isNaN(timeout)) {
	    		timeout = 3;
	    	}
	    	
	    	position = position || "top-right";
	    	
	    	typeIndex = ["error","warning","info","message","usage","other"].indexOf(type);
	    	if (typeIndex < 0) {
	    		type = "message";
	    	}
	    	
	    	opts = {
	    		content: content,
	    		position: position,
	    		theme: "tooltipster-default tooltipster-"+type
	    	}
	    	
	    	tipDestroyer = function(){
	    		setTimeout(function(){$(selector).tooltipster("destroy");},timeout*1000);
	    	}
	    	
	    	$(selector).tooltipster(opts);
	    	$(selector).tooltipster("show",tipDestroyer);
	    }
	}
	
})(querix);