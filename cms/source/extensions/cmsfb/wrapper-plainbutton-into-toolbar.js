/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

(function(querix){
    var mutabor = window.top.mutabor;
    if (!mutabor) {
        mutabor = window.top.mutabor = new window.top.Mutabor();
    }

    querix.plugins.wrappers.plainbutton_into_toolbar = {
        wrap: function(childId, el, sel) {
            return {
                remove: function() {
                },
                attach: function(to) {
                    var $ = window.top.$, winbo = $('.cms-border-of-'+sel);
                    if (winbo.length === 0) {
                        var _self = this;
                        var addButtonToCmsToolbar = function(node){
                            var $node = $(node);
                            if ($node.hasClass('cms-border-of-'+sel)) {
                                var newContainer = $node.find('.cms-toolbar:first');
                                newContainer.append(_self);
                                mutabor.removeObserver(addButtonToCmsToolbar);
                            }
                        }
                        mutabor.addObserver(addButtonToCmsToolbar);
                    } else {
                        winbo.find('.cms-toolbar:first').append(this);
                    }
                    return false;
                },
                prop: function(propertyName, propertyValue){
                    return true;
                },
            };
        }
    };
})(querix);
