/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

/**
 *  CMS wrappers:
 * - containermenu_transformer - modifies left-side menu and adds topbar
 * - toolbar_transformer - modifies toolbar
 * - formsearch_transformer - adds search field to topbar
 * - title_transformer - shows application title
 */
/*
    attach - is called before attaching the element to DOM tree
            when returns false, the framework will perform no default actions
            takes the DOM element where the other element will be attached as its first argument
    prop(propertName, propertyValue) signals that the property value changed
            when returns false, the default action on the property will be suppressed
            propertyName is a dollar-separated string specifying the path to the property in the abstract model (e.g., MinSize$Height for minimal height)
    detach - is called before detaching the element from DOM tree
    remove - is called before removing the element
    focus/blur - is called than the element receives or loses 4gl focus at runtime
    noHeavyInit - suppresses any heavy initialization actions performed by LyciaClient API
*/

(function(querix){
function underConstructionMessage(millis) {
    var disclaimerText = 'This demo is best viewed and fastest in Chrome - in some other environments it may perform slightly differently.<br/><br/>'+
    'All forms are responsive. Try the demo on desktop, mobile and tablet to experience the different layouts';
    // var disclaimerText = 'Please note this demo is best viewed in Chrome, in some other environments it maybe slightly different. '+
    // 'We will be grateful for feed back on those environments so the scripting can be improved.' +'<br/>'+
    // 'The Forms are responsive, and will automatically adjust the presentation according to the size of the device used for best Look and Feel.';
    if (window.top._UCM === undefined) {
        var $, ucm, ucmTimer, ucmRemove;
        window.top._UCM = true;
        $ = window.top.$;
        ucm = $('<div class="cms-popup-window"><div>'+disclaimerText+'</div><a class="mb-close"></a></div>');
        ucm.find('a:first').on('click',function(e){
            ucm.remove();
        });
        ucm.find('div:first').on('click',function(e){
            if (ucmTimer) {
                clearTimeout(ucmTimer);
                ucmTimer = undefined;
            }
        });
        millis = isNaN(millis) || millis <= 0 ? 3000 : millis;
        var ucmRemove = function(){
            ucmTimer = undefined;
            ucm.one('webkitTransitionEnd mozTransitionEnd transitionend',function(){
                ucm.remove();
            });
            var css = {
                transitionDuration: '.3s',
                transitionProperty: 'opacity',
                opacity:0
            };
            ucm.css(css);
        };
        ucmTimer = setTimeout(ucmRemove,millis);
        $('body').append(ucm);
    }
};
function setDeviceDependentClasses() {
    var $ = window.top.$;
    var root = $('html');
    var metaView = '<meta id="cms-meta-vieport" name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0,  maximum-scale=5.0, minimum-scale=0.1" />';
    root.find('body:first').attr('id','css-joker')

    if (root.hasClass('qx-target-web') && root.hasClass('qx-target-tablet')) {//tablet mode forced on web
        root.find('head').remove('#cms-meta-vieport').append(metaView);
        root.addClass('qx-devicemode-tablet');
        return;
    } else if (root.hasClass('qx-target-web') && root.hasClass('qx-target-mobile')) {//phone mode forced on web
        root.find('head').remove('#cms-meta-vieport').append(metaView);
        root.addClass('qx-devicemode-phone');
        return;
    }
    var minDeviceWidth = screen.width < screen.height ? screen.width : screen.height;
    var maxDeviceWidth = screen.width > screen.height ? screen.width : screen.height;
    if (minDeviceWidth < 450) {
        root.find('head').remove('#cms-meta-vieport').append(metaView);
        root.addClass('qx-devicemode-phone');
    } else if (minDeviceWidth < 768) {
        root.find('head').remove('#cms-meta-vieport').append(metaView);
        root.addClass('qx-devicemode-tablet');
    } else {
        root.addClass('qx-devicemode-desktop');
    }
}

function installMutationObservers() {
    var $ = window.top.$,body = $('body')[0], root = body.parentNode,
    config = {childList: true},
    observer = new window.top.MutationObserver(function(mutations) {
      mutations.forEach(function(mutation) {
        for (var i = 0; i < mutation.addedNodes.length; i++) {
            var node = mutation.addedNodes[i];
            var cls = node.getAttribute('class');
            if (cls) {
                if (cls.indexOf('qx-mb-dialog') > -1) {
                    fixMessageBox(node,$);
                } else if (cls.indexOf('qx-identifier-contextmenu') > -1) {
                    disableContextMenu(node,$);
                }
            }
        }
      });
    });
    observer.observe(body,config);
}

function disableContextMenu(m,$) {
    //for some reason Lycia initializes context menu after node append to DOM
    setTimeout(function(){
        var $m = $(m);
        if ($m.contextmenu('instance')) {
            $m.contextmenu('destroy');
        }
    },0);
}

function fixMessageBox(mb,$) {
    var $mb = $(mb), replaceButton = $('<a tabindex="0" class="mb-close"></a>'),
    boxShadow = $('<div class="box-shadow"></div>');
    replaceButton.on('click',function(e){
        $(this).parent().find('.ui-dialog-titlebar-close').triggerHandler('click');
    });
    var closeButton = $mb.find('.ui-dialog-titlebar-close:first');
    closeButton.after(replaceButton);
    boxShadow.append($mb.children().detach());
    $mb.append(boxShadow);
}

function popupFix() {
    var $$ = window.top.$;
    $$('body').on('click', '.qx-aum-toolbar-button', function(e){
        $$('.ui-popup').popup('close');
    });
}

setDeviceDependentClasses();
underConstructionMessage(2000);
popupFix();
installMutationObservers();

var qux = function(childId) {
    if (childId === querix.childId) {
        return querix;
    } else {
        if (querix.children && querix.children[childId]) {
            return querix.children[childId].root;
        }
    }
    console.error('Querix object [qx] not found for childId ',childId);
    return null;
};

var topbar = (function() {
    var devicemode = null;
        var topbarInstalled = false;

        function getDeviceMode($) {
                if (devicemode === null) {
                        var root = $('html');
                        if (root.hasClass('qx-devicemode-tablet')) {
                                devicemode = 'tablet';
                        } else if (root.hasClass('qx-devicemode-phone')) {
                                devicemode = 'phone';
                        } else {
                                devicemode = 'desktop';
                        }
                }
                return devicemode;
        }

        function setCss(el,name, value) {
                if(value === 'do not change') {
                        return;
                } else {
                        if (typeof value == 'function') {
                                el.css(name,value());
                        } else {
                                el.css(name,value);
                        }
                }
        }

        function addTopbar($) {
                var styles = '';
                var defaults = {
                        phone: {
                                menuWidth: '100%',
                                topbarPadding: 'do not change',
                                menuDisplay: 'table-cell'
                        },
                        tablet: {
                                menuWidth: function(){
                                        if(window.top.innerWidth > 300) {
                                                return 300;
                                        } else {
                                                return '100%';
                                        }
                                },
                                topbarPadding: 'do not change',
                                menuDisplay: 'table-cell'
                        },
                        desktop: {
                                menuWidth: 300,
                                topbarPadding: 300,
                                menuDisplay: 'table-cell'
                        }
                };
                var topbar = $('<form id="cms-topbar" class="cms-show-logo"><a id="cms-mainmenu-open" class="cms-button-mainmenu"><i></i></a><p class="cms-search-widget"></p><p><a id=\"lbValueUserName\"><img alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAIAAABuYg/PAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAB3RJTUUH4AQHBjgctdSzXQAAAAZiS0dEAP8A/wD/oL2nkwAACOFJREFUSMeNV9tuXNeRXatqn0tfeL+IIk1SlmQJGcVO7CTwBDODAQZI/GAnD/mGAeZtPmce5ilAfiDIvAyCPATxJdYgSGLJMnWXKJGiJJJNdfPS3eecXTUPp5uS7ATJRjf64PTpvWrXqlqrmv1uJ/nqV3hy1e3IKPAAd9IB4rVV3yFAc0fE0c1He59dTwfWLBEMlcBIAE44HAzwtkEdLiFMfvSz/N//U386c7AwfKRaOgwQQOD4S2CokdxBQEUzTbs7z3k4TE0ARKGRoLjTSEcwTzRkaZ7nrVbydFt6XX23e3t+YW7uzJx7dABkvWt98U0wAHSIQbPci6p4eqAVIFqJGsUoBjVRSJo1p5vt6ebklGswSNzd1X+bSc3j+oX1kMAZAfkbYCQdcCeYtdqDpx0/KVTTihqpEcKQ5q2J9tRc0phBkkdIhEaIVaYfLE4dHXUXl2Zm5iaMVu9JZ51FH71Ht8efdIAOFUXlx0/3tPCSKdNGlk+0JqezRts1N6QRSQSNahCnSh5a1RBf/flWMXBxpRvhzpdlQRhZgqUxWp1Id4cDBpXs4lqcnx0meTIx25xeaM+e0dCEZK6JUR1w0ggTGKEfzbYI6R/1FxcXZuem4AVIgKdZJCLlxLSqSIMSIoCbm9HSFpZWw/TaSU80zT1JHJJEIaQSdRIyIoSAkIEe1VkM/Ktrd5bXzqTtNHrEaTW6myCqOigeCAJeUPs6wdZCY3Gdy2vtJelsid+9mcTCAUDgpNUbWN0I7k4wkOaubnz0YHt7a/fCPyybHRuMoyrxCBmiKc7ExD2Nmg/T6XT5Sr54sUgnT5K8HXTxe/90+Ggz6ReRNBIOMTjHdBD1+fTDhTYAUEsrgbi6vqoJDHGcRzdIGZMqIib5sLGULL+fvfmvxcLlYTZVMSkRCmk02hODza+qzq5Ed6ETdRHVacSYFf3JQtNBkOb2oneweHZher4N2gjKYZIUYRqTq/nKO+nZd2X2UmwsDDWUI1FxI9KkFOvs372dFa4iJjAZ8QQSJEmQ+tFCc3xOL6sKVp27sKyJRMpQ0jI0y2w2XbnSOvd9m7oUW6sxNA0GlnV/0CEsRIZZWw63n/jzg5Tj3gDr0jjNpX403xydkxRI//h46ez85Nz8QFtH0yvhjXcbqz/A3KUimys1q0ij1+1Y7yMEnM6QNKe1H3v37qVlkRuCCQnnqFnql3640DIQkDqWqoyDwt749vvJhe/la+/J9IUqnS+0USE4BfBxk9cJqskgoBrSdpp1Hj7gfqfhyjoYeQ1MSg+RwSS4JC6JM9/brTo2F5a/qxNvVTpTIDGQqFk/XXiZIIDuxWCIicnF974/zKf6ECO9bn6H+0jqpGRimkvWDI2JtDnVnJyvJP/i+q3+oCpQVSF6iKPn/+oi6BQZpvnEt9/V9QvHkvhIQfHqD/WD5TOaZJpmmqQSEpOsH5u73f7ym+dmVs6WFGMgQDhe1ZVvwDkJhkwzRD+8f7d5ciwKeylDACiaNDTJJeSQ1Jg40ySZqDr9jd9+ZoeHwahR+ddRasdxwIhIFlnaevuynF0cOs0DqYSArNTLYPrh+jkNCRlAcZBgYpYQB90Xs+urS6tvDIs+BGOiXnGB04jhBjdx0KIWMiWhP+je3Q4QGh2IKQ6DPRl29afnzpMCCkCn0J1upBfRKuOFt6/ETGsLpnOsmXIKMzobEcUUBosDL2cmZnpbz+XghSu6Db1VnXyy8+jT7U39yfq50+IaSQLpFBj7vcHs2urk+oqd2jdQh/UqmDshdEEwBGNhkEbbKuvcv9Vl8enek/999OBa73irkvA1J/a6EgwJxY6H1z/5fPHKpWS6VcLr4n89kzI6mHvmYlWUtCH55G5VnVw+/2sMrn3xf8+KcqgZ2SYkvMpBHac4xGHASSzu3di48uXtlR9+B4GAw8Wd8PHMMHJshOhJUXmSHgV58KL3x/v3b355bYfhcToZxNLKQ4S4h9emKNbWTNa9qF72ejc+/uzs229hqmn1dyOHIuluBkBJ1UTS5EF3/+M/bvzp0eOt3uGwqrK18+VOR3b3tTwRlMTXwMZJMZCIDbPc+WTj+s7tW4vvvVME0qlOrRHdq8oTVaXu9A5+8/DLq7c39g4PS9JEmUlMWlPry0cvdouiBExcXgM75cydhAczB4aHhxu/u7py+TImE0anwYUCxsCYyIP95zfu3fli8+6d3l4fUVRFBDBzr8CplaX+1v3hoEd3hYdvTmtjMkZXGu3ptY3nG/eXf/Bdi6xUC+XQy92j3tVb1z6/s7F9dFAlEtI0GH2s0nSHV2xmzZWl7u5u9AE9BvytpWBx0Lvxyednv3WZ8wsD4OHB7u9vX7vx6N7j3adlYMzURdydFBFx93Gd2sBja2Xp6PF29fxZiGUYayxfq0k6vZ5URrJ268/X3rx5M75jV+/evr6xcb/7rFBHGiyI1TXpLiTgHDmcu3jpCK3W5LnVTud5o4rBKXDHWKMJEgJnPfI5GUWHaXjS6/z3L34ef3hlq39sEciVRIS7ox7YCAA26ncfTTuEVMp8eTE8nCmeFCFCQQMrZzRGMWZomWskPE2HZvtF/3b32Vb/aHf//nKrmnrrfNVIjHBAQHG41SMAXtrkqdg4QNjERHP94kGnDCPXBcfDJMvonoQj2v7geLt78OS4+9wHA2JI7j/cmr/4Zh/RnaQQhL9aTKdEON0Fom6gxyQ0V5b3t/aCusNhtbBLiKJDTbtVsfli72F3v+fxRDFUhshmxGB7r7f9LDm/UgFeKylf/X/zcmYXp46VqKwq5snkhTdCVrgHLUULYUl0isG9487OYe+oHA5gUTUqIRRD5iyP+rt3Hp5bPuN5UuHvWgaHe6U8c35Nlv/lH6sggzx9XA7+sPf098+2N17s7XnZT5MiJJUoKepCqiWaZvn+5s7R5k4w/4a3fV3QK0ERPAoVBPn+xW/pf/3PL59uPrh+986ftjc3i5OOuIUEmpQWbeSYFCchJiJpUhYVVNprZy3hWPj5coAacQ+nm9AIOsTxo0vv/Mc///j/AZWByBM1n+OVAAAAAElFTkSuQmCC" /><span class="qx-text"/><i class="toggle-popup"></i></a></p></form>');
                var accountPopup = $('<div data-role="popup" id="cms-account-menu" class="qx-popup qx-popup-userdata"/>')
                        .append($('<p class="content"></p>')).appendTo($('body'));
                accountPopup.popup({
                    history: false,
                    arrow: 't'
                });
                var topmost = $('html');
                if (topmost.hasClass('qx-devicemode-desktop')) {
                        topmost.addClass('cms-mainmenu-visible');
                }
                function toggleMainmenu(e) {
                        var $ = e.target.ownerDocument.defaultView.$;
                        var cm = $('#qx-container-menu');
                        var tb = $('#cms-topbar');
                        var hidden = !topmost.hasClass('cms-mainmenu-visible');
                        // var sp = defaults[getDeviceMode($)];
                        if (hidden) {
                                topmost.addClass('cms-mainmenu-visible');
                        } else {
                                topmost.removeClass('cms-mainmenu-visible');
                        }
                }
                topbar.find('#cms-mainmenu-open').on('click',toggleMainmenu);
                topbar.find('#lbValueUserName').on('click',function(){
                    try{
                        accountPopup.popup("open",{positionTo: '#lbValueUserName .toggle-popup'});
                    }
                    catch(e){
                        console.error("cannot open popup",e);
                    }
                });
                $('body').remove('#cms-topbar').append(topbar);
        }

        return {
                remove: function() {
                        if (topbarInstalled) {
                                window.top.$('#cms-topbar').remove();
                        }
                },
                attach: function(to) {
                        var $ = window.top.$;
                        if ($('#cms-topbar').length == 0) {
                                addTopbar($);
                                topbarInstalled = true;
                        }
                        return true;
                },
                prop: function(propertyName, propertyValue){
                        return true;
                },
        };
})();

if (typeof window.top.CMSTOOLBARS === 'undefined') {
    window.top.CMSTOOLBARS = {};
}
var toolbars = window.top.CMSTOOLBARS;
var arrowMap = {
    key_home: 'key_home',
    key_end: 'key_end',
    previous: 'previous',
    next: 'next',
    up: 'previous',
    down: 'next'
};
var arrows = {
    key_home: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAALCAYAAAB/Ca1DAAAAAXNSR0IArs4c6QAAALpJREFUKBVj+P///3Ug9mFAAkA+OxCvBeIFSMLEMYGaQCAXphrI5gLi3UD8HYjdYeJE00BNIAA2EEjzAPFBIP4KxM5EG4KkkAXGBhrAD2TvAGJtIHZnZGQ8ApMjhYYZyAXUtA+I9YE4B4hBLjQE0qSCFwxAjSCwCUJRTD6BufAo0CnSQAxz4UlSnQZVD3dhLtBt/EB8HIg/AbENmQYyMME0AiPhI5DtCsTngXgn0FCyYhkWhlRNh1TNKQBNBtw2TK+99gAAAABJRU5ErkJggg==',
    key_end: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAALCAYAAAB/Ca1DAAAAAXNSR0IArs4c6QAAALxJREFUKBWt0zEKwjAYhuFECoIIXSveQhAXnYp00aO4ehIP4Cy4C4LgUBwcpJMn6AEqFMf4/mKgIIj5NfDQpOT7CKE1JnA459bYot2Msp7h2nz31ZxQhjv26PgQ8wWcXwc9yaWocURXwjz1ha+CMSU3nBD7wohJjw2JbAocNfuXWOGADUyEM/qy+GEMyJaSl8IhNCeU/AhywgI55tANrmqCtztsaeooSsntcMHUWltpep4Zyv77HVL48U95AKJ0n0IRv6sHAAAAAElFTkSuQmCC',
    previous: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAALCAYAAACgR9dcAAAAAXNSR0IArs4c6QAAAHVJREFUKBVjYEAD////Twbi+0DMjCaFnwvUkAjE/4C4F79KNFmghjgg/gvEk9Ck8HOBGqKhGqfiV4kqywjUFAkUWgzEh4G4GIj/AzEx4CVI8wugSnFiVKOpeUaRzWDDyPUz3CVAA8gLbZgJQAPIi2ckA4hOYQB62GjiaIPFFgAAAABJRU5ErkJggg==',
    next: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAALCAYAAACgR9dcAAAAAXNSR0IArs4c6QAAAH9JREFUKBVjYCAA/v//zwzE94E4mYBS7NJAjb1A/A+IE7GrICAK1DgJiP8CcRwBpdilgRqnQg2IBqlgAXKkgLQ4duUYonOBIlpAvBCo7x8jkHgK5IAMIBW8BGkmxWZGoA29QGwLxLEk2Qa0CMXPRGsGaiQvtIEayYtnoEacKQwAkQlnoU2XEqsAAAAASUVORK5CYII='
};

function isArrowButton($el) {
    for (var key in arrowMap) {
        if ($el.hasClass('qx-identifier-'+key)) {
            return arrowMap[key];
        }
    }
    return null;
}

var buttonDimensions = {
    w: 11 ,
};

function calcPopupDimension(buttonCount) {
    var d = {width:0,height:'auto'};
    if (buttonCount < 5) {
        d.width = buttonDimensions.w;
    } else if (buttonCount < 10) {
        d.width = buttonDimensions.w * 2;
    } else {
        var cols = Math.ceil(buttonCount/5);
        d.width = buttonDimensions.w * cols;
    }
    d.width += 'em';
    return d;
}

querix.plugins.wrappers.toolbar_transformer = {
    wrap: function(childId, el, sel) {
        function updateToolbar(e) {
            var $ = this.ownerDocument.defaultView.$,
            objDir = qux(childId)._private._objDir,
            tid = this.qx$id, tbar = objDir[tid];
            regwin.windowStackMayChange('before update toolbar');
            var winbo = $('.cms-window-initialized:first');
            //console.error('trigger toolbarUpdated,~~~e.target,winbo',e.target,winbo);
            if (winbo.length === 0) {
                return;
            }
            var holder = getTitleAndToolbarHolder(winbo);
            holder.toolbar.find('.qx-aum-toolbar-item').detach();
            tbar.addClass('cms-toolbar-transformer');
            fullPid = childId + '_' + tid;
            bar = toolbars[fullPid];
            if (!bar) {
                return;
            }
            //3. get buttons
            var allButts = [];
            for(var bid in bar) {
                var fbid = '#'+childId+'_'+bid;
                var butt = objDir[bid];
                if (butt) {
                    if(!butt.hasClass('cms-wrapped')) {
                        var parentButt = butt.parent();
                        var parentClass = 'cms-wrapper-for-'+bid;
                        parentButt.addClass(parentClass);
                        butt.addClass('cms-wrapped');
                    }
                    allButts.push(butt);
                }
            }
            //4. sort buttons
            var expButts = [], popButts = [];
            for(var i in allButts) {
                var $b = allButts[i];
                var b = $b.storage();
                var arrow = isArrowButton($b);
                if (arrow) {
                    $b.addClass('cms-notxt').removeClass('cms-noimg');
                    var $img = $b.find('.qx-image');
                    var actualSrc = $img.attr('data-server-set-src');
                    if (!actualSrc) {
                        $img.attr('data-server-set-src',$b.attr('src'));
                        $img.attr('src',arrows[arrow]);
                    }
                    expButts.push($b);
                } else {
                    popButts.push($b);
                }
            }
            //5. show visible toolbar buttons
            for(var i in expButts) {
                expButts[i].insertBefore(holder.more);
            }
            if (popButts.length > 0) {
                var group = $();
                for (var i = 0; i < popButts.length; i++) {
                        group = group.add(popButts[i]);
                }
                group.enhanceWithin();
                var buttonsToRemove = holder.moreMenu.find('.qx-aum-toolbar-item');
                buttonsToRemove.detach();
                holder.moreMenu.append(group);
                holder.more.css('display','block');
            } else {
                holder.more.css('display','none');
            }
        }
        return {
            remove: function() {
                var $ = this.getWindow().$;
                //---$('html').removeClass('cms-toolbar-transformer');
                var tid = this.id();
                fullPid = childId + '_' + tid;
                bar = toolbars[fullPid];
                if (bar) {
                        delete toolbars[fullPid];
                }
                this.off('toolbarUpdated',updateToolbar);
                $('#b'+childId+'_'+tid).remove();
                var moreMenu = $('#p'+childId+'_'+tid);
                moreMenu.popup('destroy');
                moreMenu.remove();
                return true;
            },
            attach: function(to) {
                var $ = this.getWindow().$;
                var qx = qux(childId);
                //this.storage().CMSToolbarType = qx.themes.aumTopAncestor(this).ty();
                $('html').addClass('cms-toolbar-transformer');
                var tid = this.id();
                fullPid = childId + '_' + tid;
                bar = toolbars[fullPid];
                if (!bar) {
                        bar = toolbars[fullPid] = {};
                }
                this.on('toolbarUpdated',updateToolbar);
                this.trigger('toolbarUpdated');
                return true;
            },
            prop: function(propertyName, propertyValue){
                if (propertyName === 'ToolbarItems') {
                    var $ = this.getWindow().$, _this = this;
                    var fullPid = childId + '_' + this.id();
                    delete toolbars[fullPid];
                    var bar = toolbars[fullPid] = {};
                    propertyValue.each(function(i,tb){
                        var $tb = $(tb);
                        $tb.removeClass('qx-ip-t qx-ip-b qx-ip-r').addClass('qx-ip-l');
                        bar[$tb.id()] = true;
                    });
                    setTimeout(function(){
                        _this.trigger('toolbarUpdated');
                    },0);
                    //this.trigger('toolbarUpdated');
                }
                return true;
            },
        };
    }
};

querix.plugins.wrappers.containermenu_transformer = {
    wrap: function(childId, el, sel) {
        return {
            remove: function() {
                var $ = this.getWindow().$;
                $('html').removeClass('cmsfb');
                topbar.remove();
                this.parent().find('#cms-mainmenu-close').remove();
                this.removeClass('MENU-ITEM-TRANSFORMED');
            },
            attach: function(to) {
                var $ = this.getWindow().$, cuscId = 'custom-scrollbar';
                //$('#qx-container-menu').append('<a class="cms-to-querix-com" href="http://querix.com"></a>');
                $('html').addClass('cmsfb');
                var $topbar = $('#cms-topbar');
                if ($topbar.length == 0) {
                    topbar.attach();
                }
                this.find('a').on('click',function(){
                    $('#qx-container-menu li').removeClass('menu-item-selected');
                    $(this).parent().addClass('menu-item-selected');
                });
                this.addClass('MENU-ITEM-TRANSFORMED');
                var closeButton = this.parent().find('#cms-mainmenu-close');
                if (closeButton.length == 0) {
                    var closeButton = $('<a id="cms-mainmenu-close" class="cms-button-mainmenu"><i></i></a>');
                    closeButton.on('click',function(e){$('#cms-mainmenu-open').trigger('click');})
                    this.parent().append(closeButton);
                }
                return true;
            },
            prop: function(){return true;}
        };
    }
};

querix.plugins.wrappers.formsearch_transformer = {
    wrap: function(childId, el, sel) {
        return {
            remove: function() {
                var $ = this.getWindow().$;
                $('html').removeClass('cms-formsearch-transformer');
            },
            attach: function(to) {
                var $ = this.getWindow().$, textWidget = this.find('.qx-ff-text .qx-text'),
                buttonWidget = this.find('.qx-ff-button .qx-aum-button');
                $('html').addClass('cms-formsearch-transformer');
                var qx = qux(childId);
                if(qx) {
                    if (this.parent().hasClass('cms-search-widget')) {
                        return;
                    }
                    this.detach();
                    textWidget.attr('contenteditable','true').attr('placeholder','Search...');
                    textWidget.on('keydown',function(e){
                        console.error(e);
                        var keycode = (e.keyCode ? e.keyCode : e.which);
                        if (keycode == 13) {
                            var content = textWidget.text();
                            if (content && content.trim().length > 0) {
                                buttonWidget.triggerHandler('click');
                            }
                        }
                    });
                    $('#cms-topbar > .cms-search-widget').children().detach();
                    $('#cms-topbar > .cms-search-widget').append(this);
                }
                return false;
            },
            prop: function(propertyName, propertyValue){
                return true;
            },
        };
    }
};

querix.plugins.wrappers.title_transformer = {
    wrap: function(childId, el, sel) {
        var props = {};
        function getWindowBorder() {
            var winbo, qx = qux(childId);
            var window = qx.themes.aumAncestor(this,'Window');
            if (window) {
                winbo = window.closest('.qx-window-border');
            }
            if (winbo != null && winbo.length > 0) {
                return winbo;
            } else {
                return null;
            }
        }
        function getHolderInstance() {
            var holder, winbo = getWindowBorder.call(this);
            if (winbo) {
                holder = getTitleAndToolbarHolder(winbo);
            }
            return holder;
        }
        function updateTitle() {
            var holder = getHolderInstance.call(this);
            if (holder) {
                holder.setTitle(props['Text']);
            }
        }

        return {
            remove: function() {
                var holder = getHolderInstance.call(this);
                if (holder) {
                    holder.detachTitle(this, props);
                }
                return true;
            },
            attach: function(to) {
                var holder = getHolderInstance.call(this);
                holder.attachTitle(this, props);
                return false;
            },
            prop: function(propertyName, propertyValue){
                if (propertyName === 'Text' || propertyName === 'AttachComplete') {
                    props[propertyName] = propertyValue;
                    updateTitle.call(this);
                }
                return true;
            },
        };
    }
};

var onScrollTitleHolder = window.top.document.getElementById('cms-onscroll-title-holder');
if (!onScrollTitleHolder) {
    onScrollTitleHolder = window.top.document.createElement('div');
    onScrollTitleHolder.setAttribute('class','cms-onscroll-title-holder');
    window.top.document.body.appendChild(onScrollTitleHolder);
}

function getTitleAndToolbarHolder(winbo) {
    var holder = winbo.find('.cms-tito-holder:first'),title,toolbar,more,moreMenu,
        triggerId = 'triggerPopup_'+winbo.attr('id'), popupId = 'popup_'+winbo.attr('id');
    if (holder.length === 0) {
        holder = $('<ul class="cms-tito-holder cms-increase-specificity"><li class="cms-title"></li><li class="cms-toolbar"></li></ul>');
        toolbar = holder.find('.cms-toolbar');
        more = $('<a id="'+triggerId+'" data-role="button" data-rel="popup" class="cms-button-more" style="display:none">More</a>');
        moreMenu = $('<div data-role="popup" id="'+popupId+'" class="qx-popup qx-popup-toolbar"/>');
        holder.insertBefore(winbo.find('.qx-t-tb:first'));
        more.appendTo(toolbar);
        moreMenu.appendTo($('body'));
        moreMenu.popup({
            history: false,
            arrow: 't',
            /*beforeposition: function(){
                var w = 0,h = 0,popup = $(this);
                var butts = popup.find('.qx-aum-toolbar-button');
                var dim = calcPopupDimension(butts.length,popup);
                popup.css(dim);
            },*/
        });
        more.on('click',function(){
            try{
                moreMenu.popup("open",{positionTo: '#'+triggerId});
            }
            catch(e){
                console.error("cannot open popup",e);
            }
        });
    }
    title = holder.find('.cms-title:first');
    toolbar = holder.find('.cms-toolbar:first');
    more = toolbar.find('.cms-button-more:first');
    moreMenu = $('#'+popupId);
    return {
        tito: holder,
        title: title,
        toolbar: toolbar,
        more: more,
        moreMenu: moreMenu,
        attachTitle: function(x,props) {
            title.append(x);
            if (props.Text) {
                this.setTitle(props.Text);
            }
        },
        detachTitle: function(x) {
            title.children().detach();
        },
        setTitle: function(s) {
            title.find('.qx-text:first').html(s);
            onScrollTitleHolder.innerHTML = s;
        }
    };
}

if (typeof window.top.CMSREGWIN === 'undefined') {
    var regwin = window.top.CMSREGWIN = (function(){
        var topmost = null, registry = {};

        function isTopmost(win) {
            if (win == null) {
                return false;
            }
            return win.attr('id') === topmost;
        }

        function getTopmost() {
            return topmost;
        }

        function addToWindowRegistry(win) {
            var fullId = win.attr('id');
            if (registry[fullId] === undefined) {
                registry[fullId] = {};
            }
            return registry[fullId];
        }
        function removeFromWindowRegistry(win) {
            var fullId = win.attr('id');
            if (registry[fullId] !== undefined) {
                delete registry[fullId];
            }
        }
        function findWindow(fullId) {
            var qx, arr = fullId.split('_'), childId = parseInt(arr[0]), windowId = parseInt(arr[1]);
            if (childId === 0) {
                qx = window.top.querix;
            } else if (window.top.querix.children[childId] !== undefined) {
                qx = window.top.querix.children[childId].root;
            } else {
                return null;
            }
            return qx._private._objDir[windowId];
        }
        function findTopmost() {
            var _topmost = null, currentZOrder = -1;
            for (var fullId in registry) {
                var r = registry[fullId];
                if (r.ZOrder > currentZOrder) {
                    currentZOrder = r.ZOrder;
                    _topmost = fullId;
                }
            }
            if (_topmost != null) {
                topmost = {
                    fullId: _topmost,
                    winbo: registry[_topmost].WindowBorder
                };
            } else {
                topmost = null;
            }
            return topmost;
        }

        function setActiveWindowClass(w,$) {
            var identifier = w.storage().qx$s['Identifier'],
            html = $('html'), classes = $.filter(html.attr('class').split(' '),function(i,c){
                if (c.indexOf('cms-active-window-') > -1) {
                    return true;
                }
                return false;
            });
            if (!identifier) {
                identifier = w.id();
            } else {
                identifier = identifier.toLowerCase();
            }
            html.removeClass(classes.join(' ')).addClass('cms-active-window-'+identifier);
        }

        function windowStackMayChange(debugHint) {
            var $ = window.top.$, topmost = findTopmost();
            if (topmost == null) {
                return;
            }
            var activeBorder = $('.qx-window-border.cms-window-initialized');
            var topmostBorder, topmostWindow = findWindow(topmost.fullId);
            if (!topmostWindow) {
                //console.error(topmost,' Window detected as topmost not found');
                return;
            }
            topmostBorder = topmostWindow.closest('.qx-window-border');
            if (topmostBorder.length === 0) {
                //console.error('windowStackMayChange~~~cannot modify window stack - border not yet attached to the topmost window: ',topmost);
                return;
            }
            if (activeBorder.length > 0 && topmostBorder.id() === activeBorder.id()) {
                return;
            }
            activeBorder.removeClass('cms-window-initialized');
            topmostBorder.addClass('cms-window-initialized');
            setActiveWindowClass(topmostWindow,$);
            //console.error('windowStackMayChange~~~',debugHint);
        }
        /**
         *  CMS FB in MDI mode is a strange character.
         *  It opens first application as a window of MDI container,
         *  but open other apps as MDI children, leaving first window to hang around
         *  in the DOM tree. This function discards this window because after
         *  switch to another application it is not visible anymore and just cause of messes.
         */
        function ditchTheWitch(childId,$) {
            var wins,winbo;
            if (childId !== 0) {
                borders = $('.qx-window-border');
                borders.each(function(i,bo){
                    var $bo = $(bo), cid = $bo.attr('id').split('_')[0], $wi = $bo.find('.qx-aum-window:first');
                    if (parseInt(cid) === 0) {
                        $bo.detach();
                        removeFromWindowRegistry($wi);
                    }
                })
            }
        }
        return {
            ditchTheWitch: ditchTheWitch,
            windowStackMayChange: windowStackMayChange,
            findTopmost: findTopmost,
            findWindow: findWindow,
            removeFromWindowRegistry: removeFromWindowRegistry,
            addToWindowRegistry: addToWindowRegistry,
            registry: registry
        };
    })();
}
var regwin = window.top.CMSREGWIN;

querix.plugins.wrappers.window_enumerator = {
    getTitleAndToolbarHolder: getTitleAndToolbarHolder,
    wrap: function(childId, el, sel) {
        function processScroll(e) {
            var cssTitoOnScroll = 'cms-tito-holder-onscroll';
            if (this.scrollTop > 20) {
                $(this).closest('html').addClass(cssTitoOnScroll);
            } else {
                $(this).closest('html').removeClass(cssTitoOnScroll);
            }
        }
        return {
            remove: function() {
                var $ = this.getWindow().$, r = regwin.addToWindowRegistry(this), winbo = $('#'+r.WindowBorder);
                var qx = qux(childId);
                winbo.removeClass('cms-border-of-'+r.Identifier);
                winbo.removeClass('cms-window-enumerated');
                if (r.Identifier) {
                    $('html').removeClass('cms-active-window-'+r.Identifier.toLowerCase());
                }
                winbo.find('.qx-pane:first').off('scroll',processScroll);
                regwin.removeFromWindowRegistry(this);
                regwin.windowStackMayChange('window removed: '+this.attr('id'));
            },
            attach: function(to) {
                var $ = this.getWindow().$;
                regwin.ditchTheWitch(childId,$);
                var qx = qux(childId);
                r = regwin.addToWindowRegistry(this);
                var winbo = this.closest('.qx-window-border');
                r['Window'] = this.attr('id');
                r['WindowBorder'] = winbo.attr('id');
                winbo.addClass('cms-window-enumerated');
                getTitleAndToolbarHolder(winbo);
                if (r.Identifier) {
                    winbo.addClass('cms-border-of-'+r.Identifier);
                }
                winbo.find('.qx-pane:first').on('scroll',processScroll);
                regwin.windowStackMayChange('window attached: '+this.attr('id'));
                //console.error('window_enumerator.attach',r['Window'],r['WindowBorder']);
                return true;
            },
            prop: function(propertyName, propertyValue){
                var $ = this.getWindow().$,winbo;
                var qx = qux(childId);
                r = regwin.addToWindowRegistry(this);
                if (propertyName === 'ZOrder') {
                    r[propertyName] = propertyValue;
                    regwin.windowStackMayChange('window ZOrder changed: '+this.attr('id')+' = ',propertyValue);
                } else if (propertyName === 'WindowToolbars') {
                    r[propertyName] = propertyValue;
                    regwin.windowStackMayChange('window tolbar set: '+this.attr('id')+' = ',propertyValue);
                } else if (propertyName === 'HasFocus') {
                    regwin.windowStackMayChange('window gained or lost focus: '+this.attr('id')+' = ',propertyValue);
                }
                else if (propertyName === 'Identifier') {
                    if (r.WindowBorder) {
                        winbo = $('#'+r.WindowBorder);
                        winbo.addClass('cms-border-of-'+propertyValue);
                    }
                    r[propertyName] = propertyValue;
                    //console.error('window_enumerator.prop[Identifier]',r);
                    //TODO: move classes assignment to 4gl
                    var i = [
                        'w_email_view',
                        'w_edit_email',
                        'w_comp_short',
                        'w_act_edit',
                        'w_stock_item',
                    ].indexOf(propertyValue.toLowerCase());
                    if (i > -1) {
                        this.addClass('qx-c-cms-simple-window');
                    }
                    regwin.windowStackMayChange('window identifier set: '+this.attr('id')+' = ',propertyValue);
                }
                return true;
            },
        };
    }
};

})(querix);
