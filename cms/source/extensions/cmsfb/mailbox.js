/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

/**
 *  CMS wrappers:
 * - mailinbox_transformer
 */
/*
    attach - is called before attaching the element to DOM tree
            when returns false, the framework will perform no default actions
            takes the DOM element where the other element will be attached as its first argument
    prop(propertName, propertyValue) signals that the property value changed
            when returns false, the default action on the property will be suppressed
            propertyName is a dollar-separated string specifying the path to the property in the abstract model (e.g., MinSize$Height for minimal height)
    detach - is called before detaching the element from DOM tree
    remove - is called before removing the element
    focus/blur - is called than the element receives or loses 4gl focus at runtime
    noHeavyInit - suppresses any heavy initialization actions performed by LyciaClient API
*/

(function(querix){
    var qux = function(childId) {
        if (childId === querix.childId) {
            return querix;
        } else {
            if (querix.children && querix.children[childId]) {
                return querix.children[childId].root;
            }
        }
        console.error('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\nquerix object not found for childId ',childId);
        return null;
    };

    function installSubtreeObserver(recognizerFn,ditchWhenFound) {
        var $ = window.top.$,containerBody = $('#qx-container-body:first')[0],
        config = {childList:true,subtree: true},
        observer = new window.top.MutationObserver(function(mutations) {
          mutations.forEach(function(mutation) {
            for (var i = 0; i < mutation.addedNodes.length; i++) {
                var node = mutation.addedNodes[i];
                var result = recognizerFn(node);
                if (result && ditchWhenFound) {
                    observer.disconnect();
                }
            }
          });
        });
        observer.observe(containerBody,config);
    }

    querix.plugins.wrappers.mailinbox_transformer = {
        wrap: function(childId, el, sel) {
            var struct = [
                'id',
                'date',
                'contact',
                'from',
                'to',
                'subj',
                'read',
                'priority',
                'status',
                'content'
            ];

            var mailTpl = '<section class="cms-mail-list-entry">\
            	<div class="cms-mail-list-entry-sender"></div>\
            	<div class="cms-mail-list-entry-date"></div>\
            	<div class="cms-mail-list-entry-subj"></div>\
            	<div class="cms-mail-list-entry-content"></div>\
            	<div class="cms-mail-list-entry-ctrl">\
            		<p class="buttons">\
            			<a title="Reply" tabindex="0" class="cms-mail-reply">Reply</a>\
            			<a title="Forward" tabindex="0" class="cms-mail-forward">Forward</a>\
            			<a title="Redirect" tabindex="0" class="cms-mail-redirect">Redirect</a>\
            			<a title="Delete" tabindex="0" class="cms-mail-delete">Delete</a>\
            		</p>\
            		<p class="status">\
            			<a title="Close mail" tabindex="0" class="cms-mail-closed"></a>\
            			<a title="Open mail" tabindex="0" class="cms-mail-open"></a>\
            		</p>\
            		<p class="misc">\
            			<a title="Pin" tabindex="0" class="cms-mail-pin">Pin</a>\
            			<a title="Remind" tabindex="0" class="cms-mail-remind">Remind</a>\
            			<a title="Archive" tabindex="0" class="cms-mail-archive">Archive</a>\
            			<a title="More" tabindex="0" class="cms-mail-more">More</a>\
            		</p>\
            	</div>\
            </section>\
            ';
            var toolbarButtonsTpl = '<li class="cms-mail-inbox-toolbar-buttons">\
                <a title="Sort by sender name" tabindex="0" class="cms-button cms-mail-sort">Sort</a>\
                <input type="checkbox" class="cms-invisible-form-control" id="filter-mail-open" name="filter-mail-open"/>\
                <label title="Show open mails" tabindex="0" for="filter-mail-open" class="cms-button cms-mail-oflag">Open</label>\
                <input type="checkbox" class="cms-invisible-form-control" id="filter-mail-closed" name="filter-mail-closed"/>\
                <label title="Show closed mails" tabindex="0" for="filter-mail-closed" class="cms-button cms-mail-cflag">Closed</label>\
            </li>';
            var composeMessageTpl = '<a tabindex="0" class="cms-button cms-compose-button">Compose Message</a>';

            function addToolbarButtons(winbo) {
                var qx = qux(childId), holder;
                var we = qx.plugins.wrappers.window_enumerator || (qx.parent ? qx.parent.plugins.wrappers.window_enumerator : null);
                if (we) {
                    holder = we.getTitleAndToolbarHolder(winbo);
                    if (holder && holder.tito.length > 0) {
                        holder.tito.addClass('cms-no-toolbar-buttons');
                        var li = $(toolbarButtonsTpl);
                        holder.title.after(li);
                        var b = $(composeMessageTpl);
                        holder.toolbar.append(b);
                        holder.tito.find('.cms-mail-sort').on('click',handleToolbarClick);
                        holder.tito.find('input[type="checkbox"]').on('change',handleToolbarClick);
                    } else {
                        console.error('holder cms-tito-holder not found. Mailbox functionality is incomplete.');
                    }
                }
            }

            function removeToolbarButtons(winbo) {
                var qx = qux(childId), holder;
                var we = qx.plugins.wrappers.window_enumerator || (qx.parent ? qx.parent.plugins.wrappers.window_enumerator : null);
                if (we) {
                    holder = we.getTitleAndToolbarHolder(winbo);
                    if (holder && holder.tito.length > 0) {
                        holder.tito.find('.cms-mail-inbox-toolbar-buttons').remove();
                        holder.tito.find('.cms-compose-button').remove();
                        holder.tito.removeClass('cms-no-toolbar-buttons');
                    }
                }
            }

            function handleToolbarClick(e) {
                var t = $(e.target);
                if (t.hasClass('cms-mail-sort')) {
                    //See columns order in cmsMailInbox.css
                    $('.qx-identifier-sa_mailbox .qx-hrow th:nth-child(3)').trigger('click');
                } else if (t.prop('type') === 'checkbox') {
                    setOpenClosedStatus();
                }
            }

            function setOpenClosedStatus() {
                var mailOpen = $('#filter-mail-open'), mailClosed = $('#filter-mail-closed'),
                openChecked = mailOpen.prop('checked'), closedChecked = mailClosed.prop('checked');
                //prefix
                var p = '.qx-identifier-rb_open_filter_f';
                //button number in Hubert's database
                var n = {
                    open: 22,
                    closed: 23,
                    all: 24
                }
                //array of radio buttons
                var arb = {};
                var hasChecked = false;
                for (var i in n) {
                    arb[i] = $(p + n[i] + ' input:first');
                    if (arb[i].is(':checked')) {
                        hasChecked = true;
                    }
                }
                if (!openChecked && !closedChecked && arb.all.prop('checked')) {
                    //console.error('!openChecked && !closedChecked && arb.all.prop(checked)');
                } else if (openChecked && closedChecked && arb.all.prop('checked')) {
                    //console.error('openChecked && closedChecked && arb.all.prop(checked)');
                } else if (openChecked && !closedChecked && arb.open.prop('checked')) {
                    //console.error('openChecked && !closedChecked && arb.open.prop(checked)');
                } else if (!openChecked && closedChecked && arb.closed.prop('checked')) {
                    //console.error('!openChecked && closedChecked && arb.closed.prop(checked)');
                } else {
                    if ((openChecked && closedChecked) || (!openChecked && !closedChecked)) {
                        arb.all.trigger('click');
                    } else if (openChecked) {
                        arb.open.trigger('click');
                    } else if (closedChecked) {
                        arb.closed.trigger('click');
                    }
                }

                if (!hasChecked) {
                    arb.all.prop('checked',true);
                }
                //
                if (arb.all.is(':checked')) {
                    $('#filter-mail-open').prop('checked',true);
                    $('#filter-mail-closed').prop('checked',true);
                } else if (arb.open.is(':checked')) {
                    $('#filter-mail-open').prop('checked',true);
                    $('#filter-mail-closed').prop('checked',false);
                } else if (arb.closed.is(':checked')) {
                    $('#filter-mail-open').prop('checked',false);
                    $('#filter-mail-closed').prop('checked',true);
                }
            }

            function handleClick(e) {
                /*
                'Read mail' - 'qx-identifier-accept'
                'Mark Read' - 'f7'
                'Mark Status' - 'f8'
                'Delete' - 'f11'
                'Exit' - 'f12'
                'Help' - help
                'Find' - find
                */
                var b,
                $ = e.target.ownerDocument.defaultView.$,
                t = $(e.target),
                section = t.closest('section'),
                rowid = section.attr('data-for-mail'),
                tr = $('[data-mail="'+rowid+'"]');
                td = tr.find('td:nth-child(2)'),
                tb = tr.closest('#qx-main-layout').find('#qx-container-toolbars .qx-aum-toolbar');
                if (t.hasClass('cms-mail-reply')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f4:first');
                } else if (t.hasClass('cms-mail-forward')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f5:first');
                } else if (t.hasClass('cms-mail-redirect')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f6:first');
                } else if (t.hasClass('cms-mail-delete')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f11:first');
                } else if (t.hasClass('cms-mail-closed')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f10:first');
                } else if (t.hasClass('cms-mail-open')) {
                    b = $('.qx-aum-menu-command.qx-identifier-f9:first');
                }
                if (b && b.length) {
                    td.trigger('mousedown');
                    b.trigger('click');
                    e.stopPropagation();
                } else {
                    console.warn('Handler not found for',e.target);
                }
            }

            function setMailClass(tr,mail,colName,yesno,prefix,$) {
                var colIndex = struct.indexOf(colName) + 1,
                selector = 'td:nth-child('+colIndex+') .qx-text:first',
                val = tr.find(selector).text();
                if (yesno) {
                    if (val) {
                        val = yesno.yes;
                    } else {
                        val = yesno.no;
                    }
                } else {
                    if (val) {
                        val = val.toLowerCase();
                    }
                }
                if (val) {
                    mail.addClass(prefix+val);
                }
            }

            return {
                remove: function() {
                    var $ = this.getWindow().$;
                    var qx = qux(childId);
                    var window = qx.themes.aumAncestor(this, 'Window');
                    var winbo = window.closest('.qx-window-border');
                    removeToolbarButtons(winbo);
                    $('html').removeClass('cms-mail-inbox');
                },
                attach: function(to) {
                    var $ = this.getWindow().$;
                    var qx = qux(childId);
                    var window = qx.themes.aumAncestor(this, 'Window');
                    var winbo = window.closest('.qx-window-border');
                    addToolbarButtons(winbo);
                    $('html').addClass('cms-mail-inbox');
                    return true;
                },
                prop: function(propertyName, propertyValue){
                    return true;
                },
                SetFocusToRow: function() {
                    return false;
                },
                SyncTask: function(sync) {
                    var $ = this.getWindow().$,
                    segments = this.storage().qx$syn,
                    mailList = [];
                    ;
                    for (var i = 0; i < segments.length; i++) {
                        var atr = segments[i].insertData, startRow = segments[i].startRow;
                        for (var j = 0; j < atr.length; j++) {
                            var rowid = startRow + j,
                            tr = $(atr[j]).attr('data-mail',rowid),
                            mail = $(mailTpl).attr('data-for-mail',rowid);
                            var sender = tr.find('td:nth-child(3) .qx-text').text().toLowerCase();
                            sender = sender.substring(0,1).toUpperCase() + sender.substring(1) + ' - ' + tr.find('td:nth-child(4) .qx-text').text().toLowerCase();;
                            var date = tr.find('td:nth-child(2) .qx-text').text();
                            var subj = tr.find('td:nth-child(6) .qx-text').text();
                            var content = tr.find('td:nth-child(10) .qx-text').text();
                            var flag = tr.find('td:nth-child(7) .qx-text').text();
                            var priority = tr.find('td:nth-child(8) .qx-text').text();
                            var status = tr.find('td:nth-child(9) .qx-text').text();
                            mail.find('.cms-mail-list-entry-sender').html(sender);
                            mail.find('.cms-mail-list-entry-date').html(date);
                            mail.find('.cms-mail-list-entry-subj').html(subj);
                            mail.find('.cms-mail-list-entry-content').html(content);
                            setMailClass(tr,mail,'read',null,'qx-mail-',$);
                            setMailClass(tr,mail,'status',null,'qx-mail-',$);
                            setMailClass(tr,mail,'priority',{yes:'high',no:'normal'},'qx-mail-priority-',$);
                            mail.find('.cms-mail-list-entry-ctrl a').on('click',handleClick);
                            mailList.push(mail);
                        }
                    }
                    setOpenClosedStatus();
                    var rootContainer = $('.qx-identifier-w_mailbox_detail > div:first');
                    if (rootContainer.length > 0) {
                        rootContainer.children('section').remove()
                        rootContainer.append(mailList);
                        console.error('--------------------------Direct attach');
                    } else {
                        installSubtreeObserver(function(node){
                            var $n = $(node);
                            if ($n.hasClass('qx-identifier-rootcontainer') && $n.parent().hasClass('qx-identifier-w_mailbox_detail')) {
                                $n.children('section').remove()
                                $n.append(mailList);
                                console.error('--------------------------Observer initiated attach');
                                return true;
                            }
                            return false;
                        });

                    }
}
            };
        }
    };
})(querix);
