/*
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
*/

function Mutabor(config){
    var addNodeListeners = [], removeNodeListeners = [],
    $ = window.top.$, body = window.top.document.body,
    config = config || {childList:true,subtree: true,attributes:true},
    mutationProcessor = function(mutation) {
        for (var i = 0; i < mutation.addedNodes.length; i++) {
          var node = mutation.addedNodes[i];
          if (node.nodeType === 1) {
              for (var key in addNodeListeners) {
                  addNodeListeners[key](node);
              }
          }
        }
        for (var i = 0; i < mutation.removedNodes.length; i++) {
          var node = mutation.removedNodes[i];
          if (node.nodeType === 1) {
              for (var key in removeNodeListeners) {
                  removeNodeListeners[key](node);
              }
          }
        }
        if (mutation.target) {
            var node = mutation.target;
            if (node.nodeType === 1) {
                for (var key in addNodeListeners) {
                    addNodeListeners[key](node);
                }
            }
        }
    },
    observer = new window.top.MutationObserver(function(mutations) {mutations.forEach(mutationProcessor);}),
    observerStarted = false;

    function addObserver(type, callback) {
        if (arguments.length === 1) {
            callback = arguments[0];
            type = 'type-add';
        }
        if (type === 'type-add') {
            addNodeListeners.push(callback);
        } else {
            removeNodeListeners.push(callback);
        }
        if (addNodeListeners.length > 0 || removeNodeListeners.length > 0) {
            if (!observerStarted) {
                observer.observe(body,config);
                observerStarted = true;
            }
        }
    }

    function removeObserver(type,callback) {
        if (arguments.length === 1) {
            callback = arguments[0];
            type = 'type-add';
        }
        if (type === 'type-add') {
            var index = addNodeListeners.indexOf(callback);
            if (index > -1) {
                addNodeListeners.splice(index,1);
            }
        } else {
            var index = removeNodeListeners.indexOf(callback);
            if (index > -1) {
                removeNodeListeners.splice(index,1);
            }
        }
        if (addNodeListeners.length === 0 && removeNodeListeners.length === 0) {
            observer.disconnect();
            observerStarted = false;
        }
    }

    return {
        addObserver: addObserver,
        removeObserver: removeObserver
    };
}
