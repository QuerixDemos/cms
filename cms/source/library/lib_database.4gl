##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS
     DEFINE g_authDB_connected, g_productionDB_connected INTEGER
     DEFINE	md_currentDbConnection STRING
END GLOBALS

############################################################################
FUNCTION db_connectAuth()
############################################################################
  DEFINE errm CHAR(1000)

  IF g_authDB_connected = 0 THEN
    #WHENEVER ERROR CONTINUE
      DATABASE auth5
    #WHENEVER ERROR STOP

    IF sqlca.sqlcode <> 0 THEN
      CALL fgl_message_box("The Lycia Authentication System could not connect to\nthe authentication database.  If you wish to run a program\nauthenticated as a local user, provide a program name\nafter the \"login\" command")
      EXIT PROGRAM 1
    END IF

    LET g_authDB_connected = 1
    LET md_currentDbConnection = "Auth5"
  END IF
END FUNCTION

############################################################################
# Just a function to close the auth5 database connection and keep track on it's current connection status
############################################################################
FUNCTION db_disconnectAuth()
############################################################################
  DEFINE errm CHAR(1000)

  IF g_authDB_connected = 1 THEN

		CLOSE DATABASE	
    LET g_authDB_connected = 0
    LET md_currentDbConnection = "None"
  END IF
END FUNCTION

############################################################################
FUNCTION db_connectProduction(p_database)
############################################################################
  DEFINE errm CHAR(1000)
  DEFINE p_database VARCHAR(20)
  DEFINE msg STRING

  IF g_productionDB_connected = 0 THEN
    #WHENEVER ERROR CONTINUE
      DATABASE p_database


    #WHENEVER ERROR STOP

    IF sqlca.sqlcode <> 0 THEN
    	LET msg = "The Lycia Authentication System could not connect to\nthe production database: ", trim(p_database), "\nIf you wish to run a program\nauthenticated as a local user, provide a program name\nafter the \"login\" command"
      CALL fgl_winmessage("Could not connect to the database", msg, "ERROR")
      EXIT PROGRAM 1
    END IF
    LET g_productionDB_connected = 1
    LET md_currentDbConnection = "Production"    
		#DISPLAY "Connected to Production DB"
  END IF
END FUNCTION

############################################################################
# Just a function to close the production (i.e. catpres) database connection and keep track on it's current connection status
############################################################################
FUNCTION db_disconnectProduction(p_database)
############################################################################
  DEFINE errm CHAR(1000)
  DEFINE p_database VARCHAR(20)
  #argument is not used for now... waiting for an extension to query this
  IF g_productionDB_connected = 1 THEN

		CLOSE DATABASE	
    LET g_productionDB_connected = 0
    LET md_currentDbConnection = "None"      
    #DISPLAY "Disconnecting Production" 
  END IF
END FUNCTION
