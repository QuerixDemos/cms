##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#GLOBALS "tc_globs.4gl"
GLOBALS "../4gl/lib_globs.4gl"
#DATABASE catpres
#    "../../library/tc_globs.4gl" 
#####################################################################


##############################################################
FUNCTION screen_title(title_line)
	DEFINE  
    title_line STRING

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error

	#CALL clear_screen()

	#CALL fgl_winmessage("This needs checking and changing","huho inflib1 2414","error")
	DISPLAY title_line TO lbTitle
	#CALL fgl_settitle(title_line)
	CALL ui.Interface.setText(title_line)	
	#huho DISPLAY title_line AT 3,2 ATTRIBUTE (CYAN)
END FUNCTION 

##############################################################
FUNCTION blank_line_set()
	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error

	#huho single space would be enough for dynamic labels ALSO this is stored in a global variable ... oooohhh myyyy xxxxx ... I'll remove this now - don't think we need this at all (using dynamic labels now) / or it is just badly implemented - global variable for a 80 char string ????
	LET blank_line = "                                        ",
                 "                                       "
END FUNCTION #blank_line_set
##############################################################


####################################
# !This needs checking if this makes sense at all... very much character mode style - huho
####################################
FUNCTION clear_screen()
DEFINE 
    sub SMALLINT;

#	CALL fgl_winmessage("This should be removed or re-written","FUNCTION clear_screen()\nMakes no sense in modern layout\nBy Huho","info")

	#CALL blank_line_set()

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error


  #FOR sub = 3 TO 21
  #  DISPLAY blank_line AT sub,1
  #END FOR

  #CALL blank_lines(22)
END FUNCTION #clear_screen

##############################################################
{==================================================}
{ Amendments:                                      }
{--------------------------------------------------}
{ 1.2.01 This function no longer uses row numbers  }
{        to blank bottom lines, but calls the      }
{        "bottom lines display" functionns with a  }
{        blank line.                               }
{==================================================}
FUNCTION blank_lines(xpos)
	DEFINE
    xpos SMALLINT;

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error
#CALL fgl_winmessage("111","blank_lines() screen action needs open form","info")
	CALL write_prompt(" ")
	CALL write_keys(" ")

END FUNCTION #blank_lines


##############################################################
#FUNCTION write_prompt(line_text)
##############################################################
FUNCTION write_prompt(line_text)
	DEFINE 
    line_text STRING --CHAR(77); changed by HuHo -some text was clipped / size limit was based on character mode

	CALL blank_line_set()

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error


	IF gl_version[1] = "D" THEN
    #DISPLAY blank_line TO lbInfo1 #huho AT 1,1 #ATTRIBUTE (BLUE,REVERSE)
        DISPLAY line_text TO lbInfo1 #huho  AT 1,2 #ATTRIBUTE (BLUE,REVERSE)
       # DISPLAY "HELLO" AT 0,2 ATTRIBUTE (YELLOW)
   ELSE 
   #DISPLAY blank_line TO lbInfo1 #huho  AT 1,1 #ATTRIBUTE(UNDERLINE)
        DISPLAY line_text TO lbInfo1 #huho  AT 1,2 #ATTRIBUTE(RED,REVERSE)
       # DISPLAY "HELLO" AT 0,2 ATTRIBUTE (YELLOW)
	END IF


END FUNCTION #write_prompt



##############################################################
#FUNCTION write_keys1(line_text)
##############################################################
FUNCTION write_keys1(line_text)
DEFINE 
   
    line_text CHAR(79);

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error

	LET gl_user_help="N"



	CALL blank_line_set()

	IF gl_user_help = "Y"
	THEN
		RETURN
        #DISPLAY blank_line AT 2,1 ATTRIBUTE(WHITE)
        #DISPLAY line_text AT 2,2 ATTRIBUTE (WHITE)
	  # ELSE DISPLAY blank_line AT 1,1 ATTRIBUTE(BOLD,UNDERLINE)
    ELSE  DISPLAY line_text TO lbInfo1 #huho  AT 1,2 ATTRIBUTE (BOLD,UNDERLINE)
	END IF
END FUNCTION #write_keys


###############################################################
#FUNCTION write_keys(line_text)
###############################################################
FUNCTION write_keys(line_text)
	DEFINE 
   
    line_text CHAR(79);
	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error
	#LET gl_user_help="N"


	CALL blank_line_set()

        #DISPLAY blank_line  TO lbInfo2 #huho AT 2,1 #ATTRIBUTE(WHITE)
        DISPLAY line_text  TO lbInfo2 #huho AT 2,2 #ATTRIBUTE (WHITE)
  
END FUNCTION #write_keys



##############################################################
#FUNCTION esc_exit()
##############################################################

FUNCTION esc_exit()
	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error
	CALL write_keys("[Esc] Exit")
END FUNCTION {esc_exit}


##############################################################
FUNCTION esc_abandon()
	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error
	CALL write_keys("[Esc] Exit")
END FUNCTION {esc_abandon}
##############################################################



