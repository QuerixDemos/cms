##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS "../4gl/lib_globs.4gl"

############################################

#--------------------------------------------------
# Inflair            Version 4.2.07       02/07/93 
#--------------------------------------------------
# Author: K. Vijayan                               
# Module: tc_secur.4gl                             
# Purpose:  To check validity of Inflair           
#--------------------------------------------------
# Functions:                                       
# secure_check()                                    
# secure_search()                                  
# password_encrypt()                               
#--------------------------------------------------
#################################################################


##################################################################
#FUNCTION duplicate_pword(pword)
##################################################################
FUNCTION duplicate_pword(pword)

  DEFINE pword char(8)
  DEFINE retval SMALLINT
  whenever error continue

  SELECT COUNT(*) 
    INTO retval 
    FROM USERS where 
    password = pword

  IF retval THEN
    RETURN TRUE
   # database catpres
  ELSE 
    RETURN FALSE
    #database catpres
  END IF

END FUNCTION
#--------------------------------------------------------------------------------



##################################################################
#FUNCTION check_pword(a) 
##################################################################
FUNCTION check_pword(a) 
	DEFINE 
    a  CHAR(8),
    i,len,b,c INT
  LET i = 1 
  LET len = length(a) 

  FOR i =1 TO len
      IF a[i] MATCHES "[A-Z]" THEN
         let b = 1
      END IF
      IF a[i] MATCHES "[0-9]" THEN
         let c = 1
      END IF
  END FOR

  if b = 1 and c = 1 then
     RETURN 0
  else
     RETURN 1
  end if
END FUNCTION

