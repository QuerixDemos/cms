##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS "../4gl/lib_globs.4gl"

FUNCTION chooseTheme(argMenu,argStyle)
	DEFINE argMenu, argStyle SMALLINT


	#DATABASE catpres     
	DEFER INTERRUPT	
  #Set date in the environment

#viya check
  #CALL fgl_putenv("DBDATE=dmy4/")

	CLOSE WINDOW SCREEN 


	
	IF argMenu = 0 THEN
		CALL loadStyle(argStyle)
	ELSE 
		OPEN WINDOW wStyle AT 1,1 WITH 10 ROWS, 60 COLUMNS 

		MENU
			COMMAND "None"
				CALL loadStyle(0)		
				EXIT MENU
							
			COMMAND "Lycia"
				CALL loadStyle(1)	
				EXIT MENU
 		 
			COMMAND "WCE (2)"  
				CALL loadStyle(2)			
				EXIT MENU

			COMMAND "WCE (3)"  
				CALL loadStyle(3)	
				EXIT MENU
	
			COMMAND "Exit"
				EXIT PROGRAM
		END MENU		
	CLOSE WINDOW wStyle
	END IF

	

END FUNCTION

FUNCTION loadStyle(argStyle)
	DEFINE argStyle SMALLINT

#Let's see if I can remove the screen window here globally - huho
CLOSE WINDOW SCREEN

	CASE argStyle
		WHEN 0  #do nothing - we use this for css. each program module loads it's own .qxtheme file - which has an include to a nativeStyle theme file
		#do nothing
			#CALL apply_theme("theme/css_native_theme_0")		

		WHEN 1	#For ChildApps LyciaTheme		
			CALL apply_theme("theme/qxtheme_0")		

		WHEN 2  #For child-application CSS when loading dynamically
			CALL apply_theme("theme/nativeTheme1")	
			#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/reset.css"],[]) --reset styles
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/ui-lightness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/ui-lightness/jquery-ui.css","none","custom-theme"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector.css"],[])  --to apply style to elements with classes defined in the form		
			#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/window_selector.css"],[])  --to apply style to elements with classes defined in the form		
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])

		WHEN 3
			CALL apply_theme("theme/nativeTheme2")	
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/ui-lightness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])
	
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector.css"],[])  --to apply style to elements with classes defined in the form		
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])


		WHEN 4	#For ChildApps CSS launched from MDI		
			#CALL apply_theme("theme/css_native_theme_0")	
			#Let's do nothing and use progs theme file (prog filename = theme filename=
		WHEN 5 --CSS MDI frame  AND if you launch child apps without mdi-container
			#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/ui-lightness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/ui-lightness/jquery-ui.css","none","custom-theme"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector.css"],[])  --to apply style to elements with classes defined in the form
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/inflair-tooltips.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair-tooltips.js"],[])
			
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/jquery-tooltipster.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/jquery-tooltipster.js"],[])


		WHEN 6 --CSS MDI frame  AND if you launch child apps without mdi-container
			#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/ui-lightness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/smoothness/custom-smoothness.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/smoothness/form_class_selector-smoothness.css"],[])  --to apply style to elements with classes defined in the form
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])

		WHEN 7 --CSS MDI frame  AND if you launch child apps without mdi-container
			#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/ui-lightness/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/redmond/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector-blue.css"],[])  --to apply style to elements with classes defined in the form
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])
		WHEN 10	#For ChildApps LyciaTheme		
			CALL apply_theme("theme/qxtheme_10")			

		OTHERWISE  --default NO argument for theme provided in program call argument
			CALL ui.interface.frontcall("html5","styleImport",["https://code.jquery.com/ui/1.11.3/themes/redmond/jquery-ui.css","none","custom-theme"],[])
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js"],[])  --import wrapper script to alter the style of an widget
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/messages.css"],[])    
			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/messages.js"],[])

			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js"],[])  -- i.e. add additonal header and satusbar elements	
			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])	
			CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

			CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector-blue.css"],[])  --to apply style to elements with classes defined in the form
 			CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair.js"],[])
					#do nothing
		
		END CASE 
						
END FUNCTION
