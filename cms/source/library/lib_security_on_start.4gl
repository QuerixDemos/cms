##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS "../4gl/lib_globs.4gl"


FUNCTION on_start_init()
DEFINE 
  #gl_subs CHAR (2),  -- also in globals as char(1) 
  l_msg char(3),
  #gl_password CHAR (20),
  esc_on_prt CHAR(1), 
  dblock SMALLINT,
  wide_prt_rec RECORD LIKE reference.*,
    #repr_rec RECORD LIKE reference.*,
  repr_rec RECORD LIKE inf_log.*,
  val_version CHAR(1);

	DEFINE argError BOOLEAN  --used for program argument check/validation
	DEFINE l_record_found SMALLINT
#LET gl_mod = "td_main"
#LET gl_function = "td_main"


	#HuHo debug switch
	IF getDebugState() = TRUE THEN
		#options autorefresh 3
	END IF

######################################
# HuHo Temp security hack
# Inflair security stuff These fields are for unknown reasons in two tables, refmgm AND references AND refmst
#LET ref_key    = "CUSTOMER"
#LET ref_filler = "INFLAIR SOFTWARE LIMITED                                                                  1111111111111  2222222222222221579     1W8haan                                                                0.000"

LET gl_subs = getsubs() --getting it from the arguments by "subs A"
LET gl_password = getPassword() --getting it from the arguments by "gl_password 12345678"
LET gl_user_name = getUserName() --getting it from the arguments by "gl_user_name QUERIX"
LET gl_catpres = getCustomerDb() --production database
	LET argError = FALSE
	IF gl_user_name = "" THEN
		CALL fgl_winmessage("User argument missing","You need to run the module with a user argument i.e.   user QUERIX1","ERROR")
		LET argError = TRUE
	END IF
	IF gl_password = "" THEN
		CALL fgl_winmessage("Password argument missing","You need to run the module with a password argument i.e.   password 123456781","ERROR")
		LET argError = TRUE
	END IF
	IF gl_subs = "" THEN
		CALL fgl_winmessage("Subs argument missing","You need to run the module with a subs argument i.e.   subs A","ERROR")
		LET argError = TRUE
	END IF	

	IF gl_catpres = "" THEN
		CALL fgl_winmessage("Subs argument missing","You need to run the module with a customerdb argument i.e.   customerdb catpres","ERROR")
		LET argError = TRUE
	END IF	
	
	IF argError = TRUE THEN
		EXIT PROGRAM
	END IF	

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################################"
		DISPLAY "# Child Application Module                               #"
		DISPLAY "# In on_start_init()                                     #"
		DISPLAY "##########################################################"
		DISPLAY "getScreenWindowStyle()=", trim(getScreenWindowStyle())
		DISPLAY "getModuleArgument()=", trim(getModuleArgument())
		DISPLAY "getsubs()=", trim(getsubs())
		DISPLAY "getPassword()=", trim(getPassword())
		DISPLAY "getCustomerDb()=", trim(getCustomerDb())
		DISPLAY "getUserName()=", trim(getUserName())
		DISPLAY "getmodulegroup()=", trim(getmodulegroup())	
		DISPLAY "getDebugState()=", trim(getDebugState())	
		DISPLAY "getop_sys()=", trim(getop_sys())					

		DISPLAY "gl_subs=", trim(gl_subs)
		DISPLAY "gl_password=", trim(gl_password)
		DISPLAY "gl_user_name=", trim(gl_user_name)
		DISPLAY "gl_catpres=", trim(gl_catpres)
		DISPLAY "getModuleArgument()=", getModuleArgument()

	END IF



####################################



LET gl_version = "14.3.20G"
LET wide_prt_rec.ref_filler="N"
SELECT * into wide_prt_rec.*
  FROM reference
  WHERE ref_key="WIDE_PRT"

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################"
		DISPLAY "Note: wide_prt_rec is defined as local and global record"
		DISPLAY "There is no record with ref_key=\"WIDE_PRT\""
		DISPLAY "wide_prt_rec.*"
		DISPLAY wide_prt_rec.*
	END IF  
  
  SELECT * INTO repr_rec.* 
         FROM inf_log
         WHERE ref_key = "CUSTOMER"


	LET gl_repr_rec.ref_filler = repr_rec.ref_filler
	{
	#HuHo Let's see, if we need this information in form of cookies later
	LET repr_rec_cookieWrite[1] = trim(repr_rec.ref_key)			--CHAR(12),
	LET repr_rec_cookieWrite[2] = trim(repr_rec.ref_filler)	--CHAR(280)
	LET repr_rec_cookieWrite[3] = trim(repr_rec.macname)			--CHAR(20)
	LET repr_rec_cookieWrite[4] = trim(repr_rec.catpass)			--CHAR(20)
	LET repr_rec_cookieWrite[5] = trim(repr_rec.catusr)			--CHAR(20)
	LET repr_rec_cookieWrite[6] = trim(repr_rec.strt)				--DATETIME YEAR TO SECOND
	LET repr_rec_cookieWrite[7] = trim(repr_rec.hname)				--CHAR(40)
				
	CALL ui.Interface.frontCall("html5", "setLocalStorage", ["repr_rec",repr_rec_cookieWrite], cookieState)
}
	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################"
		DISPLAY "repr_rec.*"
		DISPLAY repr_rec.*
		DISPLAY "repr_rec.ref_key=", trim(repr_rec.ref_key)
		DISPLAY "repr_rec.ref_filler=", trim(repr_rec.ref_filler)
		DISPLAY "repr_rec.macname=", trim(repr_rec.macname)
		DISPLAY "repr_rec.catpass=", trim(repr_rec.catpass)
		DISPLAY "repr_rec.catusr=", trim(repr_rec.catusr)
		DISPLAY "repr_rec.ref_key=", trim(repr_rec.ref_key)
		DISPLAY "repr_rec.strt=", trim(repr_rec.strt)
		DISPLAY "repr_rec.hname=", trim(repr_rec.hname)
					
	END IF
	
    SELECT COUNT(*)
      INTO l_record_found
      FROM inf_log
      WHERE ref_key = "CUSTOMER"

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################"
		DISPLAY "    SELECT COUNT(*) INTO l_record_found FROM inf_log WHERE ref_key = \"CUSTOMER\""
		DISPLAY "l_record_found=", l_record_found
	END IF
      
   IF l_record_found <= 0 THEN
   	CALL fgl_winmessage("Error","Could not find Customer ref_key=CUSTOMER in table inf_log","ERROR")
   	EXIT PROGRAM
   END IF   

#WHENEVER WARNING CONTINUE # CALL file_error
#WHENEVER ERROR CONTINUE # CALL file_error

#OPTIONS
#  MESSAGE LINE 24,
#  PROMPT LINE 24,
#  ERROR LINE 24,
#  COMMENT LINE 24,
#  FORM LINE 3,
#  INSERT KEY F27,
#  DELETE KEY F28,
#  ACCEPT KEY F26
#DEFER INTERRUPT
LET esc_on_prt = "N"

#LET gl_subs = arg_val(2)  --huho ALWAYS module name i.e. A1
#LET gl_subs = getsubs() --getting it from the arguments by "gl_subs U"


#LET gl_password = arg_val(3)#------------------password from login
#LET gl_password = getPassword() --getting it from the arguments by "gl_password 12345678"


#LET gl_user_name = getUserName() --getting it from the arguments by "gl_user_name QUERIX"



#DISPLAY gl_user_name,"from tom_main"
#display "REFERENCE",repr_rec.*
#display "1,3",repr_rec.ref_filler[1,3]

############ this block is commented - I haven't got this dblock ####
#RUN "test -f $DBPATH/catrep.dbs/DBLOCK*" --agb
#     RETURNING dblock --agb
LET dblock = 1 --agb  we haven't got dblock - set to true
######################################################################
  if dblock = 0   --what should this be ?????? dummy test ?????
    then DISPLAY "INFLAIRNET DATABASE LOCKED BY DBA" 
          AT 12,24
          ATTRIBUTE(BLINK)
      SLEEP 10
        
     
else

call display_title() returning val_version --agb

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################"
		DISPLAY "val_version=", val_version
	END IF
	
# huho start hack for security
	#LET val_version = "Y"
	#LET gl_user_name  = gl_password   --really interesting mapping
# huho end of hack for security

#IF gl_password = gl_user_name 
# prompt "going to display title  " for l_msg #<
#call display_title() returning val_version
#end if
#if val_version = "Y"
    
   
#      OPEN WINDOW full_screen AT 1,1 WITH 33 ROWS,99 COLUMNS 
#           {OPEN WINDOW message_window AT 22,1 WITH 3 ROWS,79 COLUMNS#
#                ERROR "   ",gl_ref_name CLIPPED, "    ","Version:",gl_version CLIPPED,
#		"     ","User:",gl_user_name CLIPPED,"    ",date CLIPPED,"    ","Screens Open","    ",gl_count}
#     OPEN WINDOW message_window AT 24,1 WITH 3 ROWS,67 COLUMNS 
#                ERROR "   ",gl_ref_name CLIPPED, "    ","Version:",gl_version CLIPPED,
#		"     ","User:",gl_user_name CLIPPED,"    ",date CLIPPED,"    ","Screens Open","    ",gl_count," ","Connected to:"," ",gl_hname CLIPPED,"   ","Database:",catrep 
		
END IF
		
END FUNCTION