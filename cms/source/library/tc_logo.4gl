##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

{--------------------------------------------------}
{ Inflair     Version 4.3.05              05/05/02 }
{--------------------------------------------------}
{ Author: K. Vijayan                               }
{ Module: tc_logo.4gl                              }
{ Purpose:  To draw full-colour Airline Recipes    }
{           Title Screen                           }
{--------------------------------------------------}
{ Functions:                                       }
{ display_title()                                  }
{ make_password()                                  }
{--------------------------------------------------}
#DATABASE catpres
GLOBALS "../4gl/lib_globs.4gl"
####################################################################

{

FUNCTION display_title()
DEFINE
    esc_on_prt CHAR(1),
    xpos,sub,s1 SMALLINT,
    enter_date DATE,
    s2,val_version,good_user,valid,input_chr,color_mono,w_open CHAR(1),
    cust_pass,temp_code,enter_usr CHAR(8),
    term_name CHAR(8),
    l_passw CHAR(20),
    l_z,m_z int,
    user_key CHAR(12),
    userme char(20),
    cust_name CHAR(30);

	LET gl_mod = "tc_logo"
	LET gl_function = "display_title"


	SELECT * INTO r_rec.*
         FROM reference
         WHERE ref_key = "vatrate"

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################"
		DISPLAY "#### In function display_title() #########"
		DISPLAY "##########################################"

		DISPLAY "r_rec.*"
		DISPLAY r_rec.*
	END IF  

	LET gl_vat_rate = r_rec.ref_filler[1,7]
	#LET term_name = arg_val(2) changed by HuHo
	LET term_name = getsubs()
	LET user_key = "T",term_name
	#LET userme = arg_val(3)  arg_val(3) is the database name which is the customer name ????
	LET userme = getCustomerDb() 

	IF getDebugState() = TRUE THEN
		DISPLAY "####### in display_title() ####"
		DISPLAY "userme=", userme
		DISPLAY "term_name=", term_name
		DISPLAY "user_key=", user_key  --Tn ???
	END IF
	
	SELECT *
		INTO r_rec.*
		FROM inf_log
		WHERE ref_key = user_key 

	IF status = 0 THEN

		FOR sub = 1 TO 7
			LET gl_day[sub] = r_rec.ref_filler[60+sub]
		END FOR
		
		LET gl_d_perc = r_rec.ref_filler[116,120] USING "##.##"
		LET gl_q_due = r_rec.ref_filler[121,122]
		LET gl_b_due = r_rec.ref_filler[123,125]
		LET gl_inv_type = r_rec.ref_filler[126]
		LET gl_sales_vat = r_rec.ref_filler[127]

	END IF
	
	SELECT *
		INTO r_rec.*
		FROM inf_log                  
		WHERE ref_key = "VAT"
  
	LET gl_vatrates[1] = r_rec.ref_filler [1,5]
	LET gl_vatrates[2] = r_rec.ref_filler [5,10]
	LET gl_vatrates[3] = r_rec.ref_filler [11,15]
	LET gl_vatrates[4] = r_rec.ref_filler [16,20]
	LET gl_vatrates[5] = r_rec.ref_filler [21,25]
	LET gl_vatrates[6] = r_rec.ref_filler [26,30]
	LET gl_vatrates[7] = r_rec.ref_filler [31,35]
	LET gl_vatrates[8] = r_rec.ref_filler [36,40]
	LET gl_vatrates[9] = r_rec.ref_filler [41,45]
	LET gl_vatrates[10] = r_rec.ref_filler [46,50]

  
	SELECT *
		INTO r_rec.*
		FROM inf_log
		WHERE ref_key = "CONTROL"
   
	#IF ((arg_val(1) = "U")) --changed by huho
	IF ((getop_sys() = "U")) THEN
		LET val_version = "Y"
	ELSE
		CALL fgl_winmessage("Licence Security check 01","Illegal version of INFLAIR. [Esc] Exit from System","error")  #DISPLAY "Illegal version of INFLAIR. [Esc] Exit from System" AT 23,2
        #PROMPT "" FOR input_chr
        #   ATTRIBUTE(BLACK)
        #   ON KEY (ESCAPE)
        #         LET val_version = "N"
        #END PROMPT
        #LET val_version = "N"
				#hack huho
				LET val_version ="N"
	END IF

	IF val_version = "N"
   THEN RETURN val_version
	END IF

	SELECT * 
		INTO r_rec.*
		FROM inf_log
		WHERE ref_key = "CUSTOMER"

	IF getDebugState() = TRUE THEN
		DISPLAY "##### In  display_title() - CALL FUNCTION encrypt_password(user_code,action) #####################################"
		DISPLAY "SELECT * INTO r_rec.* FROM inf_log WHERE ref_key = \"CUSTOMER\""
		DISPLAY "r_rec.*"
		DISPLAY r_rec.*
		DISPLAY "r_rec.ref_key=", r_rec.ref_key
		DISPLAY "r_rec.ref_filler=", r_rec.ref_filler
		DISPLAY "r_rec.ref_filler[1,60]=", r_rec.ref_filler[1,60]
		DISPLAY "r_rec.ref_filler[61,120]=", r_rec.ref_filler[61,120]
		DISPLAY "r_rec.ref_filler[121,180]=", r_rec.ref_filler[121,180]
		DISPLAY "r_rec.ref_filler[181,240]=", r_rec.ref_filler[181,240]				
		DISPLAY "r_rec.macname=", r_rec.macname
		DISPLAY "r_rec.catpass", r_rec.catpass
		DISPLAY "r_rec.catusr", r_rec.catusr
		DISPLAY "r_rec.strt", r_rec.strt
		DISPLAY "r_rec.hname", r_rec.hname
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version			

	END IF

  
	LET cust_name = r_rec.ref_filler[1,30]
	LET gl_ref_name = cust_name


	#DISPLAY "CUSTNAME",gl_ref_name
	LET cust_pass = r_rec.ref_filler[131,136]

	IF getDebugState() = TRUE THEN
		DISPLAY "### display_title()  ###"
		DISPLAY "cust_name=", cust_name
		DISPLAY "gl_ref_name=", gl_ref_name
		DISPLAY "cust_pass=", cust_pass
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version			
	END IF
			
	IF cust_name = "I N F L A I R  SOFTWARE  LIMITED" THEN
		LET gl_demo_version = "Y"
	ELSE
		LET gl_demo_version = "N"
	END IF

	CALL secure_check() RETURNING val_version

	IF val_version = "N"
		THEN RETURN val_version
	END IF


	IF getDebugState() = TRUE THEN
		DISPLAY "##### In  display_title() - CALL FUNCTION encrypt_password(user_code,action) #####################################"
		DISPLAY "cust_pass=", trim(cust_pass)
	END IF	
	
	CALL encrypt_password(cust_pass,"D") RETURNING temp_code,cust_pass

	IF getDebugState() = TRUE THEN
		DISPLAY "### display_title()  ###"
		DISPLAY "cust_pass=", cust_pass
		DISPLAY "temp_code=", temp_code
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version			
	END IF
			
	LET enter_usr = "      "
	LET w_open = "N"
	LET enter_usr = cust_pass #______________________________________first success no password required

	WHILE enter_usr <> cust_pass OR enter_usr IS NULL
    IF w_open = "N" THEN
			OPEN WINDOW password WITH FORM "form/inflair_login_l2" ATTRIBUTE(BORDER)
			LET w_open = "Y"
		END IF

	#hack HuHo... need to be checked - can not see a reason why we can not use the some form again
	#DISPLAY "                    " AT 1,29
	#DISPLAY "Password   [      ] " AT 1,29 ATTRIBUTE(CYAN)

	#LET enter_usr = "      "

    IF color_mono = "M" THEN  -- where is a value assigned to color_mono ????
			CALL make_password() RETURNING enter_usr,val_version
		ELSE
			INPUT enter_usr WITHOUT DEFAULTS FROM scr_pass
				ON KEY (ESCAPE)
					LET val_version = "N"
					EXIT INPUT
			END INPUT
		END IF

		IF INT_FLAG != 0 THEN
			LET INT_FLAG = 0
			EXIT PROGRAM 0
		END IF

		IF val_version = "N" THEN
			CLOSE WINDOW password
			LET w_open = "N"
			EXIT WHILE
		END IF
    
	END WHILE

	IF w_open = "Y" THEN
		CLOSE WINDOW password
		LET w_open = "N"
	END IF

	IF val_version = "N"
		THEN RETURN val_version
	END IF

	IF getDebugState() = TRUE THEN
		DISPLAY "### display_title()  ###"
		DISPLAY "val_version=", val_version
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version	
	END IF
	
	#DISPLAY "                                                                            " AT 23,2
	#DISPLAY "                                                                            " AT 24,2

	LET good_user = "N"  #2nd success ----------------- normally "N"

	call getusr() returning l_passw

	IF getDebugState() = TRUE THEN
		DISPLAY "### Return from getusr() ###"
		DISPLAY "l_passw=", l_passw
		DISPLAY "good_user=", good_user	
	END IF
	
	WHILE good_user = "N"
		LET enter_usr = l_passw

		IF getDebugState() = TRUE THEN  
			DISPLAY "### display_title()  ###"
			DISPLAY "Calling encrypt_password with the argument"
			DISPLAY "enter_usr=", enter_usr
			DISPLAY "l_passw=", l_passw
			DISPLAY "good_user=", good_user		
			DISPLAY "val_version=", val_version			
		END IF
		
		LET w_open = "N"
		LET r_rec.ref_filler = " "
    
		CALL encrypt_password(enter_usr,"E") RETURNING temp_code,enter_usr
    
	LET r_rec.ref_key = "USER",enter_usr CLIPPED
    
	IF getDebugState() = TRUE THEN    
		DISPLAY "# Return values from encrypt_password()"
    DISPLAY "temp_code=", temp_code
		DISPLAY "enter_usr=", enter_usr
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version		
		DISPLAY "r_rec.ref_key=", r_rec.ref_key	
	END IF		
	
 
	SELECT *
		INTO r_rec.*
		FROM inf_log
		WHERE ref_key = r_rec.ref_key

#hack huho... I think, this is no longer a hack        
	IF STATUS != NOTFOUND THEN
		LET good_user = "Y"

		IF getDebugState() = TRUE THEN
			DISPLAY "# IF STATUS != NOTFOUND THEN"
			DISPLAY "good_user=", good_user		
		END IF		

	ELSE 

		IF getDebugState() = TRUE THEN
			DISPLAY "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
			DISPLAY "#######################################"
			DISPLAY "Credentials are not on the server"
			DISPLAY "#######################################"
			DISPLAY "### display_title()  ###"
			DISPLAY "r_rec.ref_key=", r_rec.ref_key
			DISPLAY "good_user=", good_user		
			DISPLAY "val_version=", val_version			
		END IF
		
		CALL fgl_winmessage("Inflairnet","needs fixing - Your User Credentials are not on the server\n - Report to Administrator - !","info")
#						#hack huho
#            #EXIT PROGRAM
        LET good_user = "Y"  --hack 
   END IF

    IF val_version = "N"
       THEN EXIT WHILE
    END IF

		LET gl_device = NULL
		LET gl_usr = enter_usr
		LET gl_user_name = r_rec.ref_filler [1,30]
  
		IF val_version = "N" THEN
			EXIT WHILE
		END IF
   

	END WHILE


	IF val_version = "N" THEN
		RETURN val_version
		END IF

		IF getDebugState() = TRUE THEN  
			DISPLAY "### display_title()  ###"
			DISPLAY "Calling encrypt_password with the argument"
			DISPLAY "enter_usr=", enter_usr
			DISPLAY "l_passw=", l_passw
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version			
		END IF
		
	CALL encrypt_password(enter_usr,"E")
     RETURNING temp_code,enter_usr

	LET user_key = "USER",temp_code CLIPPED

	IF getDebugState() = TRUE THEN    
		DISPLAY "# Return values from encrypt_password()"
    DISPLAY "temp_code=", temp_code
		DISPLAY "enter_usr=", enter_usr
		DISPLAY "user_key=", user_key
		DISPLAY "good_user=", good_user		
		DISPLAY "val_version=", val_version		
	END IF	
	


	LET r_rec.ref_filler [31,31] = "Y"
	LET r_rec.ref_filler [34,36] = r_rec.ref_filler [34,36] +1
	let gl_count = r_rec.ref_filler [34,36]

	LET gl_hname = r_rec.hname

	IF getDebugState() = TRUE THEN
		DISPLAY "###some rec_filler job"
		DISPLAY "GL_COUNT",gl_count
		DISPLAY "gl_hname =", gl_hname
	END IF
	
    
          UPDATE inf_log
                 SET inf_log.ref_filler = r_rec.ref_filler 
                 WHERE inf_log.ref_key = user_key
               

     
	FOR xpos = 1 TO 47
	    LET gl_prg_acc [xpos] = r_rec.ref_filler [31+xpos,31+xpos]
	END FOR

	LET enter_date = TODAY

	IF val_version = "N" THEN
		IF getDebugState() = TRUE THEN
			DISPLAY "#################"
			DISPLAY "val_version=", val_version
		END IF
   # CLEAR SCREEN
		RETURN val_version
	END IF

	LET gl_today = enter_date
	LET gl_last_ordtmp = 0

	IF getDebugState() = TRUE THEN
		DISPLAY "val_version=", val_version
		DISPLAY "gl_today=", gl_today
	END IF
	
	RETURN val_version
END FUNCTION {display_title}





{
##################################################################
FUNCTION make_password()
DEFINE
    val_version,enter_char CHAR(1),
    enter_password CHAR(7),
    sub SMALLINT;
    
	LET gl_mod = "tc_logo"
	LET gl_function = "make_password"

	#WHENEVER WARNING CONTINUE # CALL file_error
	#WHENEVER ERROR CONTINUE # CALL file_error

	LET val_version = "Y"
	LET enter_password = "       "

	OPEN WINDOW make_pass AT 18,43 WITH 1 ROWS, 6 COLUMNS

	WHILE enter_password = "      "
    FOR sub = 1 TO 6
        LET enter_char = " "
        PROMPT "" FOR CHAR enter_char
            ON KEY (ESCAPE)
                LET val_version = "N"
                LET enter_password = "xxxxxx"
                LET sub = 7
        END PROMPT
        IF val_version = "N"
           THEN EXIT FOR
        END IF
        IF enter_char IS NULL
           THEN EXIT FOR
        END IF
        LET enter_password[sub] = UPSHIFT(enter_char)
    END FOR


	END WHILE


	CLOSE WINDOW make_pass

	RETURN enter_password,val_version
END FUNCTION 
#make_password#


}




##################################################################
{
FUNCTION getusr()
  define 
    l_passw char(20),
    userrec record
      host char(20),
      catusr char(20),
      catpass char(20)
    end record
	DEFINE l_record_found SMALLINT    
  DEFINE errMsg STRING

  LET userrec.host = fgl_get_property("system.network","hostname")
  #hack huho
	LET userrec.host = getClientHostName() --"Inflair_App"  --local hostname can not be retrieved using a browser
	#end huho hack

	IF getDebugState() = TRUE THEN
  	DISPLAY "######### in getusr()   ########"
		DISPLAY "userrec.host" = userrec.host
	END IF
# LET userrec.host = fgl_get_property("system.network","ipaddress")


--call fgl_winmessage(catrep,userrec.host,'info')
#CALL fgl_winmessage("need db name","need db name variable","info")
#database catrep @@@
CALL db_disconnectAuth()
CALL db_connectProduction(gl_catpres)


#select catpass
#      INTO userrec.catpass
#      FROM inf_log
#     # where catuser = userrec.catusr   # works with dumb terminals
#     -- WHERE macname = userrec.host    # will not work on dumb terminals
#		WHERE macname = "linvm"
 

	IF getDebugState() = TRUE THEN
		DISPLAY "####################################"
		DISPLAY "Critical query  - shit no record with macname                   #"
		DISPLAY "####################################"
		DISPLAY "select catpass INTO userrec.catpass FROM inf_log where macname = ", userrec.host
		DISPLAY "userrec.host=", userrec.host
	END IF

    SELECT COUNT(*)
      INTO l_record_found
      FROM inf_log
      WHERE macname = userrec.host

	CASE l_record_found
		WHEN 0
			LET errMsg = "User Record for this Client Machine was not found\nWHERE macname = userrec.host = ", trim(userrec.host), " \ Exit Program"
			CALL fgl_winmessage("Client Machine creditentials not found", errMsg,"error")
			exit Program
		WHEN l_record_found > 1
			LET errMsg = "More than one user Record for this Client Machine was found\nWHERE macname = userrec.host = ", trim(userrec.host), " \ Exit Program"
			CALL fgl_winmessage("Client Machine creditentials invalid or not unique", errMsg,"error")
			exit Program
		WHEN 1					
			select catpass
      	INTO userrec.catpass
      	FROM inf_log
				# where catuser = userrec.catusr   # works with dumb terminals
				WHERE macname = userrec.host    # will not work on dumb terminals
	END CASE

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################################"
		DISPLAY "# In FUNCTION getusr() - tc_logo.4gl                     #"
		DISPLAY "##########################################################"
		DISPLAY "getScreenWindowStyle()=", trim(getScreenWindowStyle())
		DISPLAY "getModuleArgument()=", trim(getModuleArgument())
		DISPLAY "getsubs()=", trim(getsubs())
		DISPLAY "getPassword()=", trim(getPassword())
		DISPLAY "getCustomerDb()=", trim(getCustomerDb())
		DISPLAY "getUserName()=", trim(getUserName())
		DISPLAY "getmodulegroup()=", trim(getmodulegroup())	
		DISPLAY "getDebugState()=", trim(getDebugState())	
		DISPLAY "getop_sys()=", trim(getop_sys())					

		DISPLAY "gl_subs=", trim(gl_subs)
		DISPLAY "gl_password=", trim(gl_password)
		DISPLAY "gl_user_name=", trim(gl_user_name)
		DISPLAY "gl_catpres=", trim(gl_catpres)
		DISPLAY "userrec.host=", trim(userrec.host)
		DISPLAY "userrec.catpass=", trim(userrec.catpass)
		DISPLAY "Special STATUS=", status
	END IF
 
 
	if status = 100 THEN
		CALL fgl_winmessage("Inflairnet","The Server has no record of your user credentials\n - Report to Administrator - !","info")
		EXIT PROGRAM
	end if

	let l_passw = userrec.catpass

	IF getDebugState() = TRUE THEN
		DISPLAY "## getusr() Returning l_passw"
		DISPLAY "l_passw=", l_passw
	END IF 

return l_passw
end function

}
FUNCTION getPseudoClientHostName(p_userName)
	DEFINE p_userName CHAR(8)
	DEFINE l_cookieString CHAR(20)
	#For cookie / pseudo client side host name as this is not supported for browsers	
	define cookieState INT
	DEFINE cookieWrite, cookieRead dynamic array of VARCHAR(20)
	DEFINE cookieRandomValue INT
	DEFINE pseudoHostName STRING
	DEFINE i SMALLINT

	IF getDebugState() = TRUE THEN
		DISPLAY "######################################"
		DISPLAY "getPseudoClientHostName(p_userName) Returning l_passw"
		DISPLAY "p_userName=",p_userName
	END IF 
# hostname is not possible/supported for browser clients - I'll create a pseudo unique host-id
 CALL ui.Interface.frontCall("html5", "getLocalStorage", ["pseudo.hostname"], cookieRead)

	IF getDebugState() = TRUE THEN
		DISPLAY "cookieRead[1]=", cookieRead[1]
	END IF 

 LET l_cookieString = cookieRead[1]
	
	for i = 1 TO 20
		IF l_cookieString[i] = " " THEN
			LET pseudoHostName = NULL
			EXIT FOR
		END IF
	END FOR
	
	#check if value was retrieved
	IF pseudoHostName = "" OR pseudoHostName IS NULL THEN  --no cookie with pseudo Host Name found

		IF p_userName = "" OR p_userName IS NULL THEN
			CALL fgl_winmessage("Invalid user name","Invalid user name in getPseudoClientHostName()","error")
		END IF
		
		#IF NOT, create pseudoHostName
		LET cookieRandomValue = fgl_random(1000, 99999)

		FOR i = 1 TO 8
			IF p_userName[i] = " " THEN
				LET p_userName[i] = "x" 
			END IF
		END FOR

		LET cookieWrite[1] = "InfHost", p_userName,trim(cookieRandomValue) 
 		LET pseudoHostName = cookieWrite[1]
 
 		CALL ui.Interface.frontCall("html5", "setLocalStorage", ["pseudo.hostname",cookieWrite], cookieState)

	END IF

	IF getDebugState() = TRUE THEN
		DISPLAY "getPseudoClientHostName(p_userName) Returning l_passw"
		DISPLAY "p_userName=",p_userName
		DISPLAY "pseudoHostName=", pseudoHostName
	END IF 
	
	RETURN pseudoHostName
END FUNCTION	