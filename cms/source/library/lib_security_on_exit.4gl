##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS "../4gl/lib_globs.4gl"

############################################################################

FUNCTION on_exit_clean_up()
# made a wrapper here in case I need to to do more clean up stuff
	CALL de_active()
	IF getDebugState() = TRUE THEN	
		DISPLAY "on_exit_clean_up() - END"
	END IF
END FUNCTION


FUNCTION de_active()
	DEFINE
    temp_code CHAR(8),
    user_key CHAR(12),
    run_stmt char (40),
    user_name CHAR(30);

	DEFINE l_record_found SMALLINT
	DEFINE msg STRING
	LET gl_mod = "ta_main"
	LET gl_function = "de_active"

#	WHENEVER WARNING CONTINUE # CALL file_error
#	WHENEVER ERROR CONTINUE # CALL file_error

########################
# hack huho
# set gl_user to QUERIX
#LET gl_usr="QUERIX"
#######################

	CALL encrypt_password(gl_usr,"E")
    RETURNING temp_code,gl_usr

	LET user_key = "USER",temp_code CLIPPED

    SELECT COUNT(*)
      INTO l_record_found
      FROM inf_log
      WHERE ref_key = user_key
      
   IF l_record_found <= 0 THEN
   	LET msg = "Could not find record in inf_log with ref_key=", trim(user_key), "\ngl_usr=", trim(gl_usr), "  temp_code=", trim(temp_code)
   	CALL fgl_winmessage("Error",msg,"ERROR")
   END IF 

   
          SELECT *
                 INTO r_rec.* FROM inf_log
                 WHERE ref_key = user_key
          LET r_rec.ref_filler [31,31] = "N"
           LET r_rec.ref_filler [34,36] = r_rec.ref_filler [34,36] -1 

          UPDATE inf_log
                SET inf_log.ref_filler = r_rec.ref_filler
                WHERE inf_log.ref_key = user_key
	
	IF getDebugState() = TRUE THEN
		DISPLAY "de_active() - END"  
	END IF
END FUNCTION 