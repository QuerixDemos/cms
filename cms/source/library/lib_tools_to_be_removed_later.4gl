##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS "../4gl/lib_globs.4gl"

DEFINE duration  DYNAMIC ARRAY OF INTERVAL SECOND TO FRACTION(3)
DEFINE br_start   	DATETIME SECOND TO FRACTION(3)          --test run Start Time
DEFINE br_end     	DATETIME SECOND TO FRACTION(3)          --test run End Time 


#function bang()
# from the orignal sources.. I leave it but just for a note
# This function is called 4 times from meditfl.4gl.... but does nothing
# so, from my point of view, you can remove it and the 4 calls
# Hubert
#######################################################################
# FUNCTION bang()
#
# DESCRIPTION: 
#
# RETURN NOTHING
#######################################################################
FUNCTION bang()
END FUNCTION
#FUNCTION bang()
#	DEFINE vbang	CHAR(25)
#	OPTIONS PROMPT LINE LAST
#	PROMPT "Taper la Commande : " FOR vbang
#	RUN vbang
#	PROMPT "Appuyer sur ENTER : " FOR vbang
#END FUNCTION

# Just used for some really basic bench tests

FUNCTION bm_start()
	LET br_start = CURRENT SECOND TO FRACTION(3)	
END FUNCTION

FUNCTION bm_end()
	LET br_end = CURRENT SECOND TO FRACTION(3)
END FUNCTION

FUNCTION bm_result()
DISPLAY "BenchStart: ", br_start
DISPLAY "BenchStart: ", br_end
DISPLAY "Duration:   ", br_end - br_start
END FUNCTION