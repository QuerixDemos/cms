##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DEFINE md_startWindowFullScreen BOOLEAN  --rendering of the first prog window 1 = windowed 0=fullScreen
DEFINE md_startWindowStyleSet BOOLEAN

DEFINE md_themeSet BOOLEAN

DEFINE md_argmod STRING
DEFINE md_subs CHAR
DEFINE md_password STRING
DEFINE md_user_name STRING
DEFINE md_debug BOOLEAN
DEFINE md_customerdb VARCHAR(18)
DEFINE md_modulegroup VARCHAR(3)
DEFINE md_op_sys CHAR(1)
DEFINE md_module STRING
DEFINE md_clientHostName VARCHAR(20)
DEFINE md_ignoreDivider BOOLEAN  --program argument to ignore divider menu items

DEFINE md_config_filename  STRING
DEFINE md_currentOperatorId INT

FUNCTION processArguments()
  DEFINE args ARRAY[20] OF STRING
  DEFINE i SMALLINT
  DEFINE l_arg, l_argsub, l_errMsg STRING
  DEFINE argSize INT

	LET md_argmod = NULL    
	LET argSize = NUM_ARGS()  
		
		  
	IF NUM_ARGS() > 30 THEN

		LET l_errMsg = "Too many arguments - closing application\n Argument count: ", argSize
		CALL fgl_winmessage("Too many arguments",l_errMsg,"error")
		EXIT PROGRAM
	END IF
    
    FOR i = 1 TO NUM_ARGS()
    	LET l_arg = ARG_VAL(i)
    	CALL l_arg.ToLowerCase()
    	#CALL fgl_Winmessage("arg=",arg,"info")
    	CASE l_arg
    	
      	WHEN "config"  --configuration file name - default is cms.cfg
      		LET md_config_filename = ARG_VAL(i+1)
      		LET i = i+1
					call fgl_winmessage("config", md_config_filename,"info")

      	WHEN "operatorId"  --configuration file name - default is cms.cfg
      		LET md_config_filename = ARG_VAL(i+1)
      		LET i = i+1
					call fgl_winmessage("config", md_config_filename,"info")



    	
      	WHEN "theme" 
      		CALL loadStyle(ARG_VAL(i+1))
      		LET md_themeSet = TRUE
       		LET i = i+1
       		
      	WHEN "startWindowFullScreen"  --startWindowStyle
      		LET l_argSub = ARG_VAL(i+1)
      		LET i = i+1
      		      		
      		CASE l_argSub  --.ToLowerCase()
      			WHEN "1"
      				LET md_startWindowFullScreen = TRUE --main opening window floating
      			WHEN "0"
      				LET md_startWindowFullScreen = FALSE --main opening window full screen
					END CASE
					
					LET md_startWindowStyleSet = TRUE
					
				WHEN "debugmode"
      		LET l_argSub = ARG_VAL(i+1)
      		LET i = i+1
      		      		
      		CASE l_argSub.ToLowerCase()
      			WHEN "true"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "false"
      				LET md_debug = FALSE --main opening window full screen

      			WHEN "1"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "0"
      				LET md_debug = FALSE --main opening window full screen


      			WHEN "t"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "f"
      				LET md_debug = FALSE --main opening window full screen

					END CASE				

					
				WHEN "module"
      		LET md_module = ARG_VAL(i+1)		
      		LET i = i+1
      		
				WHEN "argmod"
      		LET md_argmod = ARG_VAL(i+1)		
      		LET i = i+1
      							
				WHEN "subs"
					LET md_subs =  ARG_VAL(i+1)	
      		LET i = i+1
      		
				WHEN "modulegroup"
					LET md_modulegroup =  ARG_VAL(i+1)	
      		LET i = i+1
      							
				WHEN "password"
					LET md_password = ARG_VAL(i+1)	
      		LET i = i+1
      		
						
				WHEN "customerdb"
					LET md_customerdb = ARG_VAL(i+1)	
      		LET i = i+1
      		
				WHEN "op_sys"
					LET md_op_sys = ARG_VAL(i+1)	
      		LET i = i+1					

						
				WHEN "user"
					#CALL fgl_winmessage("WHEN gl_catusr ARG_VAL(i)",ARG_VAL(i),"info")	
					#CALL fgl_winmessage("WHEN gl_catusr ARG_VAL(i)",ARG_VAL(i+1),"info")	

					LET md_user_name = ARG_VAL(i+1)	
      		LET i = i+1
					#LET gl_catusr = ARG_VAL(i+1)

				WHEN "clientHostName"
					LET md_clientHostName = ARG_VAL(i+1)	
      		LET i = i+1					
			END CASE       			




# "gl_user_name ", trim(userrec.username), "gl_password ", trim(userrec.password)     
      
    END FOR    
    
	#hack.. can currently not see any sense as it is always 'U'   was an argument for each module call i.e. TAA n5 U
	IF md_subs = "" THEN
		LET md_subs = "A"
	END IF

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################################"
		DISPLAY "# In themeByArg()                                        #"
		DISPLAY "##########################################################"	
		DISPLAY "getStartWindowFullScreen()=", trim(getStartWindowFullScreen())
		DISPLAY "getModuleArgument()=", trim(getModuleArgument())
		DISPLAY "getsubs()=", trim(getsubs())
		DISPLAY "getPassword()=", trim(getPassword())
		DISPLAY "getCustomerDb()=", trim(getCustomerDb())
		DISPLAY "getUserName()=", trim(getUserName())
		DISPLAY "getmodulegroup()=", trim(getmodulegroup())		
		DISPLAY "getDebugState()=", trim(getDebugState())		
		DISPLAY "getop_sys()=", trim(getop_sys())	
	
	END IF


	#######################################
	# Default settings for missing arguments

	IF md_themeSet = FALSE THEN
		CALL loadStyle(7)  -- 7 is blue css style
	END IF

	IF md_startWindowStyleSet = FALSE THEN
		LET md_startWindowFullScreen = "1"  --Default 0=windowed 1=full screen / stretched
	END IF

		    
END FUNCTION

FUNCTION setDebugState(state)
	define state BOOLEAN
	let md_debug = state
end function

FUNCTION getDebugState()
	RETURN md_debug
END FUNCTION

FUNCTION getConfigFilename()
	CALL fgl_winmessage("getConfigFilename()",md_config_filename,"info")
	RETURN md_config_filename
END FUNCTION	

FUNCTION getArgCurrentOperatorId()
	CALL fgl_winmessage("getArgCurrentOperatorId()",md_currentOperatorId,"info")
	RETURN md_currentOperatorId
END FUNCTION

FUNCTION getPassword()
	RETURN md_password
END FUNCTION

FUNCTION getUserName()
	RETURN md_user_name
END FUNCTION

FUNCTION getsubs()
	DEFINE tempString1 CHAR(1)
	DEFINE tempString2 STRING
	LET tempString2 = md_subs[1]
	LET tempString1 = 	tempString2.toUpperCase()
	#CALL fgl_winmessage("tempString1",tempString1,"info")
	RETURN tempString1 --toUpperCase() --ToLowerCase()
END FUNCTION


FUNCTION getStartWindowFullScreen()
	#DISPLAY sWinStyle
	RETURN md_startWindowFullScreen
END FUNCTION


FUNCTION getModuleArgument()
	#DISPLAY sWinStyle
	IF md_argmod = "" OR md_argmod IS NULL THEN
		RETURN NULL
	ELSE
		RETURN md_argmod
	END IF
END FUNCTION

FUNCTION getModule()
	RETURN md_module
END FUNCTION


FUNCTION getCustomerDb()
	RETURN md_customerdb
END FUNCTION

FUNCTION getModuleGroup()
RETURN md_modulegroup
END FUNCTION


FUNCTION getop_sys()
RETURN md_op_sys
END FUNCTION

FUNCTION getClientHostName()
	RETURN md_clientHostName
END FUNCTION

### for mdi container
FUNCTION setIgnoreDividerState(state)
	define state BOOLEAN
	let md_ignoreDivider = state
end function

FUNCTION getIgnoreDividerState()
	RETURN md_ignoreDivider
END FUNCTION