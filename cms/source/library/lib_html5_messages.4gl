##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#IMPORT FGL inflib1
#IMPORT FGL inflib2 
GLOBALS "../4gl/lib_globs.4gl"
#universial wrapper for messages
# modules use custom functions, display, display at, message and error statements out of context 
FUNCTION showMsg(p_msgType,p_msg,p_context)   
	DEFINE p_msgType STRING
	DEFINE p_msg STRING
	DEFINE p_context STRING
	DEFINE gl_msgCustomStatusBar BOOLEAN
	DEFINE gl_msgTooltip BOOLEAN
	DEFINE gl_msg4glStatusBar BOOLEAN

		
	LET gl_msgCustomStatusBar = TRUE
	LET gl_msgTooltip = TRUE
	LET gl_msg4glStatusBar = TRUE

	LET p_msgType = p_msgType.ToUpperCase() 


	#Tooltips CSS/JS only	
	IF 	gl_msgTooltip = TRUE THEN

		CASE p_msgType
      	WHEN "ERROR" 
      		CASE p_context
      			WHEN "" -- attach to currently focused element
							CALL ui.interface.frontcall("messages","popup",[p_msg,3],[])      			
      			WHEN "global" -- show tooltip in a css defind location
							CALL ui.interface.frontcall("messages","showGlobal",[p_msg,3],[])      			
      			OTHERWISE   --show near the specified element
							CALL ui.interface.frontcall("messages","showForId",[".qx-identifier-"||p_context,p_msg,5],[])	  						
      		END CASE 
 
      	WHEN "WARNING" 
      		CASE p_context
      			WHEN "" -- attach to currently focused element
							CALL ui.interface.frontcall("messages","popup",[p_msg,3],[])      			
      			WHEN "global" -- show tooltip in a css defind location
							CALL ui.interface.frontcall("messages","showGlobal",[p_msg,3],[])      			
      			OTHERWISE   --show near the specified element
						CALL ui.interface.frontcall("messages","showForId",[p_context,p_msg,3],[])    						
      		END CASE 
      		
      		 
      	#WHEN "MESSAGE"

      	#WHEN "KeyUsage"

      	WHEN "FieldUsage"
      		CASE p_context
      			WHEN "" -- attach to currently focused element
							CALL ui.interface.frontcall("messages","popup",[p_msg,3],[])      			
      			WHEN "global" -- show tooltip in a css defind location
							CALL ui.interface.frontcall("messages","showGlobal",[p_msg,3],[])      			
      			OTHERWISE   --show near the specified element
						CALL ui.interface.frontcall("messages","showForId",[p_context,p_msg,3],[])    						
      		END CASE 

			END CASE
					

	END IF

	
	#CustomStatusBar (Form Fields)	
	IF 	gl_msgCustomStatusBar = TRUE THEN

		CASE p_msgType
      	WHEN "ERROR" 
					#CALL write_prompt(p_msg) 
      	WHEN "WARNING" 
					#CALL write_prompt(p_msg) 
      	WHEN "MESSAGE"
      		#CALL write_prompt(p_msg)
      	WHEN "KeyUsage"
      		#CALL write_prompt(p_msg)
      	WHEN "FieldUsage"
      		#CALL write_prompt(p_msg)
			END CASE
					

	END IF

	#4glStatusBar 	
	IF 	gl_msg4glStatusBar = TRUE THEN

		CASE p_msgType
      	WHEN "ERROR" 
					ERROR p_msg 
      	WHEN "WARNING" 
					MESSAGE p_msg 
      	WHEN "MESSAGE"
      		MESSAGE p_msg
      	WHEN "KeyUsage"
      		MESSAGE p_msg
      	WHEN "FieldUsage"
      		MESSAGE p_msg
			END CASE					

	END IF



	
#	IF 	gl_msgTooltip IS TRUE THEN
	
#	END IF


END FUNCTION

FUNCTION msgPopup(msgType,argMsg)   
	DEFINE msgType STRING
	DEFINE argMsg STRING
	DEFINE msg STRING
	
	CASE msgType.ToLowerCase()
		WHEN "error"
			CALL ui.interface.frontcall("messages","showGlobal",[argMsg,3],[])

		WHEN "wait"
			CALL ui.interface.frontcall("messages","showGlobal",[argMsg,3],[])
			
		OTHERWISE
			LET msg = "FUNCTION msgPopup(msgType,argMsg)\nInvalid msgType CASE argument\nmsgType=", msgType.ToLowerCase()
			CALL fgl_winmessage("Error in lib_html5_messages.4gl", msg, "error")	

	END CASE	

END FUNCTION

#FUNCTION msgFieldPopup(msg,FieldId)
#	DEFINE msg STRING
#	DEFINE FieldId STRING


#END FUNCTION	
	
	

FUNCTION msgPopupFocused(msgType,argMsg) --msgPopupFocused("usage","Enter VAT RATE (percent)")
	DEFINE msgType STRING
	DEFINE argMsg STRING
	DEFINE msg STRING
	
	CASE msgType.ToLowerCase()
		WHEN "usage"
			CALL ui.interface.frontcall("messages","showForId",[".qx-focused",argMsg,5],[])	
		WHEN "error"
			CALL ui.interface.frontcall("messages","showForId",[".qx-focused",argMsg,5],[])		
		WHEN "info"
			CALL ui.interface.frontcall("messages","showForId",[".qx-focused",argMsg,5],[])			
		WHEN "warning"
			CALL ui.interface.frontcall("messages","showForId",[".qx-focused",argMsg,5],[])			
		OTHERWISE
			LET msg = "Invalid msgType CASE argument\nmsgType=", msgType.ToLowerCase()
			CALL fgl_winmessage("Error in lib_html5_messages.4gl", msg, "error")	
	END CASE
		

		#fgl_dialog_getfieldname()
	 #showForId: function(fieldid, content, timeout)
	 
END FUNCTION

FUNCTION msgPopupById(msgType,argMsg,id)
	DEFINE msgType STRING
	DEFINE argMsg STRING
	DEFINE msg STRING
	DEFINE id STRING
	
	CASE msgType.ToLowerCase()
		WHEN "usage"
			CALL ui.interface.frontcall("messages","showForId",[".qx-identifier-"||id,argMsg,5],[])	
		WHEN "error"
			CALL ui.interface.frontcall("messages","showForId",[".qx-identifier-"||id,argMsg,5],[])		
		WHEN "info"
			CALL ui.interface.frontcall("messages","showForId",[".qx-identifier-"||id,argMsg,5],[])			
		WHEN "warning"
			CALL ui.interface.frontcall("messages","showForId",[".qx-identifier-"||id,argMsg,5],[])			
		OTHERWISE
			LET msg = "Invalid msgType CASE argument\nmsgType=", msgType.ToLowerCase()
			CALL fgl_winmessage("Error in lib_html5_messages.4gl", msg, "error")	
	END CASE
END FUNCTION