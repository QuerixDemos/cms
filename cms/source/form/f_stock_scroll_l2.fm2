<?xml version="1.0" encoding="UTF-8"?>
<!--
 cms-demo
 Property of Querix Ltd.
 Copyright (C) 2016  Querix Ltd. All rights reserved.
 This program is free software: you can redistribute it. 
 You may modify this program only using Lycia.

 This program is distributed in the hope that it will be useful,
 but without any warranty; without even the implied warranty of
 merchantability or fitness for a particular purpose.

 Email: info@querix.com
-->

<form xmlns="http://namespaces.querix.com/2015/fglForms">
	<form.rootContainer>
		<GridPanel classNames="fcls_rootContainer" fieldTable="" identifier="rootContainer">
			<GridPanel.gridRowDefinitions>
				<GridRowDefinition>
					<GridRowDefinition.gridLength>
						<GridLength gridLengthType="Auto"/>
					</GridRowDefinition.gridLength>
				</GridRowDefinition>
				<GridRowDefinition>
					<GridRowDefinition.gridLength>
						<GridLength gridLengthType="Percent" gridLengthValue="100" gridMinLength="200"/>
					</GridRowDefinition.gridLength>
				</GridRowDefinition>
				<GridRowDefinition>
					<GridRowDefinition.gridLength>
						<GridLength gridLengthType="Auto"/>
					</GridRowDefinition.gridLength>
				</GridRowDefinition>
			</GridPanel.gridRowDefinitions>
			<GridPanel.gridColumnDefinitions>
				<GridColumnDefinition>
					<GridColumnDefinition.gridLength>
						<GridLength gridLengthType="Percent" gridLengthValue="100"/>
					</GridColumnDefinition.gridLength>
				</GridColumnDefinition>
			</GridPanel.gridColumnDefinitions>
			<GridPanel classNames="fcls_title_cnt" visible="true" verticalAlignment="Top" gridItemLocation="0,0,1,1" fieldTable="formonly" identifier="cntTitle">
				<GridPanel.gridRowDefinitions>
					<GridRowDefinition>
						<GridRowDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridRowDefinition.gridLength>
					</GridRowDefinition>
				</GridPanel.gridRowDefinitions>
				<GridPanel.gridColumnDefinitions>
					<GridColumnDefinition>
						<GridColumnDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridColumnDefinition.gridLength>
					</GridColumnDefinition>
				</GridPanel.gridColumnDefinitions>
				<GridPanel classNames="fcls_title_cnt_sub" visible="true" verticalAlignment="Top" gridItemLocation="0,0,1,1" fieldTable="formonly" identifier="cntTitleSub">
					<GridPanel.gridRowDefinitions>
						<GridRowDefinition>
							<GridRowDefinition.gridLength>
								<GridLength gridLengthType="Auto"/>
							</GridRowDefinition.gridLength>
						</GridRowDefinition>
					</GridPanel.gridRowDefinitions>
					<GridPanel.gridColumnDefinitions>
						<GridColumnDefinition>
							<GridColumnDefinition.gridLength>
								<GridLength gridLengthType="Percent" gridLengthValue="100"/>
							</GridColumnDefinition.gridLength>
						</GridColumnDefinition>
					</GridPanel.gridColumnDefinitions>
					<Label isDynamic="true" text="Choose Stock Item" classNames="fcls_title_lb" visible="true" gridItemLocation="0,0,1,1" fieldTable="formonly" identifier="lbTitle"/>
				</GridPanel>
			</GridPanel>
			<GridPanel classNames="fcls_table_cnt" visible="true" gridItemLocation="0,1,1,1" fieldTable="formonly" identifier="cntTable" horizontalAlignment="Stretch" verticalAlignment="Stretch">
				<GridPanel.gridRowDefinitions>
					<GridRowDefinition>
						<GridRowDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridRowDefinition.gridLength>
					</GridRowDefinition>
				</GridPanel.gridRowDefinitions>
				<GridPanel.gridColumnDefinitions>
					<GridColumnDefinition>
						<GridColumnDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridColumnDefinition.gridLength>
					</GridColumnDefinition>
				</GridPanel.gridColumnDefinitions>
				<Table fieldTable="" identifier="sa_stock_item" gridItemLocation="0,0,1,1" minSize="85qch,10qch" horizontalAlignment="Stretch" verticalAlignment="Stretch">
				<TableColumn visible="true" fieldTable="" identifier="f0" text="Stock ID">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="11"/>
					</TableColumn.columnLength>
					<TextField maxLength="10" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="stock_item" identifier="stock_id"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f1">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="33"/>
					</TableColumn.columnLength>
					<TextField maxLength="32" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="stock_item" identifier="item_desc"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f2">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="9"/>
					</TableColumn.columnLength>
					<TextField maxLength="8" dataType="Money,,,," visible="true" textAlignment="Left," fieldTable="stock_item" identifier="item_cost"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f3">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="8"/>
					</TableColumn.columnLength>
					<TextField maxLength="7" format="&lt;&lt;&lt;&amp;.&amp;&amp;%" dataType="Decimal,,,," visible="true" textAlignment="Left," fieldTable="tax_rates" identifier="tax_rate"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f4">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="6"/>
					</TableColumn.columnLength>
					<TextField maxLength="5" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="formonly" identifier="quantity"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f5">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="6"/>
					</TableColumn.columnLength>
					<TextField maxLength="5" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="formonly" identifier="reserved"/>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f6">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="6"/>
					</TableColumn.columnLength>
					<TextField maxLength="5" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="formonly" identifier="available">
						<TextField.displayModes>
							<DisplayMode appearance="red,reverse">
								<DisplayMode.condition>
									<EqualTo>
										<EqualTo.leftLiteral>
											<FieldTagLiteral fieldTagValue="f6"/>
										</EqualTo.leftLiteral>
										<EqualTo.rightLiteral>
											<IntegerLiteral/>
										</EqualTo.rightLiteral>
									</EqualTo>
								</DisplayMode.condition>
							</DisplayMode>
						</TextField.displayModes>
					</TextField>
				</TableColumn>
				<TableColumn visible="true" fieldTable="" identifier="f7">
					<TableColumn.columnLength>
						<GridLength gridLengthType="Percent" gridLengthValue="6"/>
					</TableColumn.columnLength>
					<TextField maxLength="5" dataType="Char,,,," visible="true" textAlignment="Left," fieldTable="formonly" identifier="ordered"/>
				</TableColumn>
			</Table>
			</GridPanel>
			<GridPanel classNames="fcls_statusbar_cnt" visible="true" verticalAlignment="Top" gridItemLocation="0,2,1,1" fieldTable="formonly" identifier="cntStatusBar">
				<GridPanel.gridRowDefinitions>
					<GridRowDefinition>
						<GridRowDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridRowDefinition.gridLength>
					</GridRowDefinition>
				</GridPanel.gridRowDefinitions>
				<GridPanel.gridColumnDefinitions>
					<GridColumnDefinition>
						<GridColumnDefinition.gridLength>
							<GridLength gridLengthType="Percent" gridLengthValue="100"/>
						</GridColumnDefinition.gridLength>
					</GridColumnDefinition>
				</GridPanel.gridColumnDefinitions>
				<GridPanel classNames="fcls_statusbar_cnt_sub" visible="true" verticalAlignment="Top" gridItemLocation="0,0,1,1" fieldTable="formonly" identifier="cntStatusBarSub">
					<GridPanel.gridRowDefinitions>
						<GridRowDefinition>
							<GridRowDefinition.gridLength>
								<GridLength gridLengthType="Auto"/>
							</GridRowDefinition.gridLength>
						</GridRowDefinition>
						<GridRowDefinition>
							<GridRowDefinition.gridLength>
								<GridLength gridLengthType="Auto"/>
							</GridRowDefinition.gridLength>
						</GridRowDefinition>
						<GridRowDefinition>
							<GridRowDefinition.gridLength>
								<GridLength gridLengthType="Auto"/>
							</GridRowDefinition.gridLength>
						</GridRowDefinition>
					</GridPanel.gridRowDefinitions>
					<GridPanel.gridColumnDefinitions>
						<GridColumnDefinition>
							<GridColumnDefinition.gridLength>
								<GridLength gridLengthType="Percent" gridLengthValue="60"/>
							</GridColumnDefinition.gridLength>
						</GridColumnDefinition>
						<GridColumnDefinition>
							<GridColumnDefinition.gridLength>
								<GridLength gridLengthType="Percent" gridLengthValue="40"/>
							</GridColumnDefinition.gridLength>
						</GridColumnDefinition>
					</GridPanel.gridColumnDefinitions>
					<Label isDynamic="true" text="Choose Stock Item" classNames="fcls_sblabel" visible="true" gridItemLocation="0,0,2,1" fieldTable="formonly" identifier="lbInfo1"/>
					<Label isDynamic="true" text="Escape-OK    CtrlC-Cancel   F4-Add   F5-Edit   F6-Delete" classNames="fcls_sblabel" visible="true" gridItemLocation="0,1,2,1" fieldTable="formonly" identifier="lbInfo2"/>
					<StackPanel visible="true" identifier="spButtonGroupLeft" gridItemLocation="0,2,1,1" horizontalAlignment="Left" classNames="fcls_stackPanel_cnt_for_button_l,fcls_align_left">
						<Button text="New" visible="true" textAlignment="Center," fieldTable="" identifier="bt_new" classNames="fcls_button,fcls_image_default_add">
				<Button.onInvoke>
					<KeyEventHandler keyName="F4,false,false,false"/>
				</Button.onInvoke>
			</Button>
						<Button text="Edit" visible="true" textAlignment="Center," fieldTable="" identifier="bt_edit" classNames="fcls_button,fcls_image_default_edit">
				<Button.onInvoke>
					<KeyEventHandler keyName="F5,false,false,false"/>
				</Button.onInvoke>
			</Button>
						<Button text="Delete" visible="true" textAlignment="Center," fieldTable="" identifier="bt_delete" classNames="fcls_button,fcls_image_default_delete">
				<Button.onInvoke>
					<KeyEventHandler keyName="F6,false,false,false"/>
				</Button.onInvoke>
			</Button>
						<Button text="Find" visible="true" identifier="bt4" classNames="fcls_button,fcls_image_default_find">
							<Button.onInvoke>
								<ActionEventHandler type="actioneventhandler" actionName="FIND"/>
							</Button.onInvoke>
						</Button>		
						<Button text="Help" visible="true" identifier="bt3" classNames="fcls_button,fcls_image_default_help">
							<Button.onInvoke>
								<ActionEventHandler type="actioneventhandler" actionName="HELP"/>
							</Button.onInvoke>
						</Button>			
					</StackPanel>
					<StackPanel visible="true" identifier="spButtonGroupRight" gridItemLocation="1,2,1,1" horizontalAlignment="Right" classNames="fcls_stackPanel_cnt_for_button_r,fcls_align_right">
						<Button text="OK" visible="true" textAlignment="Center," fieldTable="" identifier="bt_ok" classNames="fcls_button,fcls_image_default_accept">
							<Button.onInvoke>
								<ActionEventHandler type="actioneventhandler" actionName="ACCEPT"/>
							</Button.onInvoke>
						</Button>
						<Button text="Close" visible="true" textAlignment="Center," fieldTable="" identifier="bt_close" classNames="fcls_button,fcls_image_default_cancel">
				<Button.onInvoke>
					<ActionEventHandler actionName="CANCEL"/>
				</Button.onInvoke>
			</Button>
					</StackPanel>
				</GridPanel>
			</GridPanel>
		</GridPanel>
	</form.rootContainer>
	<form.screenRecords>
		<ScreenRecord identifier="sa_stock_item" fields="stock_id,item_desc,item_cost,tax_rate,quantity,reserved,available,ordered"/>
		<ScreenRecord identifier="formonly" fields="quantity,reserved,available,ordered,bt_new,bt_edit,bt_delete,bt_ok,bt_close"/>
		<ScreenRecord identifier="stock_item" fields="item_cost,item_desc,stock_id"/>
		<ScreenRecord identifier="tax_rates" fields="tax_rate"/>
	</form.screenRecords>
</form>

