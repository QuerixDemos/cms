##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

GLOBALS

  DEFINE qxt_glob_int_switch SMALLINT

######################################################################
  #Data Types for HELP_HTML

#####################################################
# Html - Help System
#####################################################
  DEFINE 

    qxt_current_help_id SMALLINT --,

  DEFINE t_qxt_help_html_form_rec TYPE AS
    RECORD
     help_html_id          SMALLINT,
     language_name         VARCHAR(30),
     help_html_fname       VARCHAR(100),
     help_html_mod_date    DATE,
     help_html_data        TEXT
 
    END RECORD
{
  DEFINE t_qxt_help_html_no_blob_rec TYPE AS
    RECORD
     help_html_id          LIKE qxt_help_html.help_html_id,
     language_id           LIKE qxt_help_html.language_id,
     help_html_fname    LIKE qxt_help_html.help_html_fname,
     help_html_mod_date    LIKE qxt_help_html.help_html_mod_date
 
    END RECORD

  DEFINE t_qxt_help_html_file TYPE AS
    RECORD
     help_html_fname    LIKE qxt_help_html.help_html_fname,
     help_html_data        LIKE qxt_help_html.help_html_data
    END RECORD


  DEFINE t_qxt_help_html_fname_rec TYPE AS
    RECORD
     help_html_fname LIKE qxt_help_html.help_html_fname  
    END RECORD
}
#############################
## Help_URL_Map Section
############################

  DEFINE t_qxt_help_url_map_rec TYPE AS
    RECORD
     map_id          LIKE qxt_help_url_map.map_id,
     language_name   LIKE qxt_language.language_name,
     map_fname       LIKE qxt_help_url_map.map_fname


    END RECORD

  DEFINE t_qxt_help_url_map_form_rec TYPE AS
    RECORD
     map_id          LIKE qxt_help_url_map.map_id,
     language_name      LIKE qxt_language.language_name,
     map_fname          LIKE qxt_help_url_map.map_fname
 
    END RECORD

  DEFINE t_qxt_help_url_map_file TYPE AS
    RECORD
     help_url_map_fname    LIKE qxt_help_url_map.map_fname
    END RECORD


  DEFINE t_qxt_help_url_map_fname_rec TYPE AS
    RECORD
     help_url_map_fname LIKE qxt_help_url_map.map_fname  
    END RECORD


END GLOBALS







