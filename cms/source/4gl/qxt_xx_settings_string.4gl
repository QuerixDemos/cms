##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings String
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_string_edit()
#
# Edit string settings (string.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_string_edit()
  DEFINE 
    l_settings_string_rec  OF t_qxt_settings_string_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_string",5,5,get_form_path("f_qxt_settings_string"),FALSE)
  CALL qxt_settings_string_view_form_details()

  #Get the corresponding record
  CALL get_settings_string_form_rec()
    RETURNING  l_settings_string_rec.*

  #Input
  INPUT BY NAME l_settings_string_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    ON ACTION("settingsSave")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_string_form_rec(l_settings_string_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_string_form_rec(l_settings_string_rec.*) --Write to memory
      CALL process_string_cfg_export(NULL) --Write to disk
      EXIT INPUT

    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD string_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_string_form_rec(l_settings_string_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  CALL publish_toolbar("settingsCfg",1)

  IF NOT int_flag THEN
    CALL copy_settings_string_form_rec(l_settings_string_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_string")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_string_view_form_details()

  CALL fgl_settitle(get_str_tool(170))

  DISPLAY get_str_tool(171) TO dl_f71 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(172) TO dl_f72 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(173) TO dl_f73 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(174) TO dl_f74 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(175) TO dl_f75 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(176) TO dl_f76 ATTRIBUTE(BLUE, BOLD)

 
  CALL source_name_combo_list("string_tool_source_name")   
  CALL source_name_combo_list("string_app_source_name")   
END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_string_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_string_form_rec
  

  ##################################
  # string.cfg Configuration file
  ##################################
  #[string] Section

  LET l_settings_form.string_tool_source_name   = get_source_name(qxt_settings.string_tool_source) --string id
  LET l_settings_form.string_tool_filename      = qxt_settings.string_tool_filename                --default string to be used if a string does not exist in a particular string
  LET l_settings_form.string_tool_table_name    = qxt_settings.string_tool_table_name              --source is 0=file 1=db
  LET l_settings_form.string_app_source_name    = get_source_name(qxt_settings.string_app_source)  --string import file - lists all strings (not strings)
  LET l_settings_form.string_app_filename       = qxt_settings.string_app_filename                 --string table name - lists all strings (not strings)
  LET l_settings_form.string_app_table_name     = qxt_settings.string_app_table_name               --string table name - lists all strings (not strings)

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_string_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_string_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_string_form_rec
  
  ##################################
  # string.cfg Configuration file
  ##################################
  #[string] Section
  LET qxt_settings.string_tool_source        = get_source_id(p_settings_form_rec.string_tool_source_name) --string id
  LET qxt_settings.string_tool_filename      = p_settings_form_rec.string_tool_filename                   --default string to be used if a string does not exist in a particular string
  LET qxt_settings.string_tool_table_name    = p_settings_form_rec.string_tool_table_name                 --source is 0=file 1=db
  LET qxt_settings.string_app_source         = get_source_id(p_settings_form_rec.string_tool_source_name) --string import file - lists all strings (not strings)
  LET qxt_settings.string_app_filename       = p_settings_form_rec.string_app_filename                    --string table name - lists all strings (not strings)
  LET qxt_settings.string_app_table_name     = p_settings_form_rec.string_app_table_name                  --string table name - lists all strings (not strings)

END FUNCTION



####################################################################################################
# EOF
####################################################################################################
