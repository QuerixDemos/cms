##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Access functions for the usage related insructions
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

FUNCTION get_displayKeyInstructions()
  RETURN qxt_settings.displayKeyInstructions
END FUNCTION

FUNCTION get_displayUsageInstructions()
  RETURN qxt_settings.displayUsageInstructions
END FUNCTION

