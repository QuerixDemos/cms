##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

##################################################
# FUNCTION operatorSession_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (Cursor) for operator_popup()
#
# RETURN NONE
##################################################
FUNCTION operator_session_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "operator_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
  								"operator_session.operator_id, ",
  								"operator.name, ",
                 "operator_session.session_id, ",
                 "operator_session.session_counter, ",
                 "operator_session.expired, ",                 
                 "operator_session.session_created, ",
                 "operator_session.session_modified, ", 
                 "operator_session.client_host, ",
                 "operator_session.client_ip, ",
                 "operator_session.reserve1, ",
									"operator_session.reserve2, ",
  								"contact.cont_id, ",
                 "contact.cont_fname, ",
                 "contact.cont_lname ",
                 "FROM operator_session, contact, operator ",
                 
#                 "OUTER operator ",
									"WHERE ",
										"operator.operator_id = operator_session.operator_id ",
										"AND ",
										"contact.cont_id = operator.cont_id"




  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
    #LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ? ?"
  END IF

  PREPARE p_operator_session FROM sql_stmt
  DECLARE c_operator_session CURSOR FOR p_operator_session


END FUNCTION

##################################################
# FUNCTION operator_popup(p_operator_id)
#
# displays a list of all operator (like admin) and returns the selected one
# if the operation is canceled, it will return it's original calling argument
#
# RETURN l_operator_arr[i].operator_id OR p_operator_id
##################################################
FUNCTION operator_session_popup(p_session_id,p_order_field,p_accept_action)
  DEFINE 
    p_session_id					         LIKE operator_session.session_id,
    p_accept_action               SMALLINT,
    p_order_field,p_order_field2  VARCHAR(128),
    l_operator_session_arr        DYNAMIC ARRAY OF t_operator_session_rec2,
    l_arr_size                    SMALLINT,
    l_operator_session_id         LIKE operator_session.session_id,
    l_scroll_operator_session_id  LIKE operator_session.session_id,
    l_scroll_operator_line_id     SMALLINT,
    i                             INTEGER,
    err_msg                       VARCHAR(240),
    local_debug                   SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET l_arr_size = 100  -- sizeof(l_operator_arr)
  #Scroll variable init
  IF p_session_id IS NOT NULL THEN
    LET l_scroll_operator_session_id = p_session_id
  ELSE
    LET l_scroll_operator_session_id = 1
  END IF
  
  LET l_scroll_operator_line_id = 1

    IF NOT fgl_window_open("w_operator_session_scroll", 2, 8, get_form_path("f_operator_session_scroll_l2"), FALSE)  THEN
      CALL fgl_winmessage("Error","operator_popup()\nCan not open window w_operator_session_scroll","error")
      RETURN p_session_id
    END IF
    #CALL populate_operator_session_scroll_form_labels_g()
		CALL ui.Interface.setImage("qx://application/icon16/transportation/operator/people_call_operator-delay.png")
		CALL ui.Interface.setText("O-Session")

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL operator_session_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1


    FOREACH c_operator_session INTO l_operator_session_arr[i].*
      IF l_operator_session_arr[i].operator_id = l_scroll_operator_session_id THEN
        LET l_scroll_operator_line_id = i
      END IF
      LET i = i + 1
      #IF i > l_arr_size THEN  --!!! No longer required ... working with a dynamic array now
      #  CALL fgl_winmessage("To many rows in the table","There are too many rows in the table for this array size" || l_arr_size || "\nOnly the first " || l_arr_size || "rows will be displayed", "info")
      #  EXIT FOREACH
      #END IF
    END FOREACH
    LET i = i - 1


    IF NOT i THEN
      CALL fgl_window_close("w_operator_scroll")
      RETURN NULL
    END IF

		IF i > 0 THEN
			CALL l_operator_session_arr.resize(i)  --correct the last element of the dynamic array
		END IF
		
    #CALL set_count(i)

    LET int_flag = FALSE


    DISPLAY ARRAY l_operator_session_arr TO sc_operator_arr.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 910

      BEFORE DISPLAY
        CALL publish_toolbar("OperatorList",0)

        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF l_scroll_operator_line_id IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,l_scroll_operator_line_id)
        END IF

      BEFORE ROW
      	LET i = arr_curr() #set i to current row
        LET l_operator_session_id = l_operator_session_arr[i].session_id
        LET l_scroll_operator_session_id = l_operator_session_arr[i].session_id


      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        CASE p_accept_action
          WHEN 0
            EXIT WHILE
        
          #WHEN 1  --view
          #  CALL operator_view(l_operator_session_id)
          #  EXIT DISPLAY

          #WHEN 2  --edit
          #  CALL operator_edit(l_operator_session_id)
          #  EXIT DISPLAY

          WHEN 3  --delete
            CALL operator_session_delete(l_operator_session_id)
            EXIT DISPLAY

          #WHEN 4  --create/add
          #  CALL operator_create(l_operator_session_id)
          #  EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","operator_session_popup()\noperator Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL operator_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

          OTHERWISE
            LET err_msg = "operator_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("operator_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      #ON KEY (F4) -- add
      #  CALL operator_create("U")
      #  EXIT DISPLAY

      #ON KEY (F5) -- edit
      #  CALL operator_session_edit(l_operator_session_id)
      #  EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL operator_session_delete(l_operator_session_id)
        EXIT DISPLAY


      # Column Sort short cuts
     
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator_session.operator_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "session_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "session_counter"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "expired"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "session_created"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "session_modified"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
        
      ON KEY(F20)
        LET p_order_field2 = p_order_field
        LET p_order_field = "client_host"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY        


        
      ON KEY(F21)
        LET p_order_field2 = p_order_field
        LET p_order_field = "client_ip"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  
        
        
      ON KEY(F22)
        LET p_order_field2 = p_order_field
        LET p_order_field = "reserve1"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY                  
        
        
      ON KEY(F23)
        LET p_order_field2 = p_order_field
        LET p_order_field = "reserve2"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

        
      ON KEY(F24)
        LET p_order_field2 = p_order_field
        LET p_order_field = "contact.cont_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 
 
         
      ON KEY(F24)
        LET p_order_field2 = p_order_field
        LET p_order_field = "contact.cont_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY        

      ON KEY(F25)
        LET p_order_field2 = p_order_field
        LET p_order_field = "contact.cont_lname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY        
        
        
        
    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_operator_session_scroll")

  IF NOT int_flag THEN
    LET i = arr_curr()
    RETURN l_operator_session_arr[i].operator_id
  ELSE 
    LET int_flag = FALSE
    RETURN p_session_id
  END IF

END FUNCTION


