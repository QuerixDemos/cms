##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings help_html
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_help_html_edit()
#
# Edit help_html settings (help_html.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_help_html_edit()
  DEFINE 
    l_settings_help_html_rec  OF t_qxt_settings_help_html_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_help_html",5,5,get_form_path("f_qxt_settings_help_html"),FALSE)
  CALL qxt_settings_help_html_view_form_details()

  #Get the corresponding record
  CALL get_settings_help_html_form_rec()
    RETURNING  l_settings_help_html_rec.*

  #Input
  INPUT BY NAME l_settings_help_html_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    ON ACTION("settingsSave")  --save
      CALL fgl_dialog_update_data()
      CALL copy_settings_help_html_form_rec(l_settings_help_html_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_help_html_form_rec(l_settings_help_html_rec.*) --Write to memory
      CALL process_help_html_cfg_export(NULL) --Write to disk
      EXIT INPUT

    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD help_html_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_help_html_form_rec(l_settings_help_html_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  CALL publish_toolbar("settingsCfg",1)

  IF NOT int_flag THEN
    CALL copy_settings_help_html_form_rec(l_settings_help_html_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_help_html")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_help_html_view_form_details()

  CALL fgl_settitle(get_str_tool(190))

  DISPLAY get_str_tool(191) TO dl_f91 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(192) TO dl_f92 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(193) TO dl_f93 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(194) TO dl_f94 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(195) TO dl_f95 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(196) TO dl_f96 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(197) TO dl_f97 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(198) TO dl_f98 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(199) TO dl_f99 ATTRIBUTE(BLUE, BOLD)
 
  CALL help_system_type_combo_list("help_html_system_type_name")   

END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_help_html_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_help_html_form_rec,
    local_debug     SMALLINT
  
  LET local_debug = FALSE

  ##################################
  # help_html.cfg Configuration file
  ##################################
  #[help_html] Section

  LET l_settings_form.help_html_system_type_name  = get_help_system_type_name(qxt_settings.help_html_system_type)  --Help System Type (from app server file or online webserver)
  LET l_settings_form.help_html_url_map_fname     = qxt_settings.help_html_url_map_fname     --File name of the html help url map config file
  LET l_settings_form.help_html_url_map_tname     = qxt_settings.help_html_url_map_tname     --Table name of the html help url map config information
  LET l_settings_form.help_html_base_dir          = qxt_settings.help_html_base_dir          --Base DIR for the help file url ie. help/help-html
  LET l_settings_form.help_html_base_url          = qxt_settings.help_html_base_url          --Base URL for the help file url ie. www.querix.com/help
  LET l_settings_form.help_html_win_x             = qxt_settings.help_html_win_x             --x position of the help html window
  LET l_settings_form.help_html_win_y             = qxt_settings.help_html_win_y             --y position of the help html window
  LET l_settings_form.help_html_win_width         = qxt_settings.help_html_win_width         --width of the help html window
  LET l_settings_form.help_html_win_height        = qxt_settings.help_html_win_height        --height of the help html window

  IF local_debug THEN
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_system_type =",   qxt_settings.help_html_system_type 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_url_map_fname =", qxt_settings.help_html_url_map_fname 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_url_map_tname =", qxt_settings.help_html_url_map_tname 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_base_dir =",      qxt_settings.help_html_base_dir 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_base_url =",      qxt_settings.help_html_base_url 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_win_x =",         qxt_settings.help_html_win_x 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_win_y =",         qxt_settings.help_html_win_y 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_win_width =",     qxt_settings.help_html_win_width 
    DISPLAY "get_settings_help_html_form_rec() - qxt_settings.help_html_win_height =",    qxt_settings.help_html_win_height 

    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_system_type_name =", l_settings_form.help_html_system_type_name 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_url_map_fname =",    l_settings_form.help_html_url_map_fname 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_url_map_tname =",    l_settings_form.help_html_url_map_tname 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_base_dir =",         l_settings_form.help_html_base_dir 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_base_url =",         l_settings_form.help_html_base_url 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_win_x =",            l_settings_form.help_html_win_x 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_win_y =",            l_settings_form.help_html_win_y 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_win_width =",        l_settings_form.help_html_win_width 
    DISPLAY "get_settings_help_html_form_rec() - l_settings_form.help_html_win_height =",       l_settings_form.help_html_win_height 
  END IF

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_help_html_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_help_html_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_help_html_form_rec,
    local_debug         SMALLINT

  LET local_debug = FALSE
  
  ##################################
  # help_html.cfg Configuration file
  ##################################
  #[help_html] Section
  LET qxt_settings.help_html_system_type       = get_help_system_type_id(p_settings_form_rec.help_html_system_type_name)  --Help System Type (from app server file or online webserver)
  LET qxt_settings.help_html_url_map_fname     = p_settings_form_rec.help_html_url_map_fname     --File name of the html help url map config file
  LET qxt_settings.help_html_url_map_tname     = p_settings_form_rec.help_html_url_map_tname     --Table name of the html help url map config information
  LET qxt_settings.help_html_base_dir          = p_settings_form_rec.help_html_base_dir          --Base DIR for the help file url ie. help/help-html
  LET qxt_settings.help_html_base_url          = p_settings_form_rec.help_html_base_url          --Base URL for the help file url ie. www.querix.com/help
  LET qxt_settings.help_html_win_x             = p_settings_form_rec.help_html_win_x             --x position of the help html window
  LET qxt_settings.help_html_win_y             = p_settings_form_rec.help_html_win_y             --y position of the help html window
  LET qxt_settings.help_html_win_width         = p_settings_form_rec.help_html_win_width         --width of the help html window
  LET qxt_settings.help_html_win_height        = p_settings_form_rec.help_html_win_height        --height of the help html window

  IF local_debug THEN
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_system_type =",   qxt_settings.help_html_system_type 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_url_map_fname =", qxt_settings.help_html_url_map_fname 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_url_map_tname =", qxt_settings.help_html_url_map_tname 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_base_dir =",      qxt_settings.help_html_base_dir 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_base_url =",      qxt_settings.help_html_base_url 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_win_x =",         qxt_settings.help_html_win_x 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_win_y =",         qxt_settings.help_html_win_y 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_win_width =",     qxt_settings.help_html_win_width 
    DISPLAY "copy_settings_help_html_form_rec() - qxt_settings.help_html_win_height =",    qxt_settings.help_html_win_height 

    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_system_type_name =", p_settings_form_rec.help_html_system_type_name 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_url_map_fname =",    p_settings_form_rec.help_html_url_map_fname 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_url_map_tname =",    p_settings_form_rec.help_html_url_map_tname 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_base_dir =",         p_settings_form_rec.help_html_base_dir 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_base_url =",         p_settings_form_rec.help_html_base_url 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_win_x =",            p_settings_form_rec.help_html_win_x 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_win_y =",            p_settings_form_rec.help_html_win_y 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_win_width =",        p_settings_form_rec.help_html_win_width 
    DISPLAY "copy_settings_help_html_form_rec() - p_settings_form_rec.help_html_win_height =",       p_settings_form_rec.help_html_win_height 
  END IF


END FUNCTION



####################################################################################################
# EOF
####################################################################################################







