##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Shared Tool String functions (when strings are located in memory) 
#
# Strings are located in a file but copied to memory
#
################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_string_tool_in_mem_globals.4gl"

######################################################################################################################
# Init functions
######################################################################################################################

###########################################################
# FUNCTION qxt_empty_string_tool_array()
#
# Initialise too string array with blanks ""
#
# RETURN NONE
###########################################################
FUNCTION qxt_empty_string_tool_array()
  DEFINE
    id INTEGER
  FOR id = 1 TO qxt_string_tool_array_size
    LET qxt_string_tool[id] = ""
  END FOR
END FUNCTION



######################################################################################################################
# Data Access functions
######################################################################################################################


###########################################################
# FUNCTION get_string_tool(id)
#
# Get lib-tool related multi language string
#
# RETURN trim(qxt_multi_lingual_tool_str[ID].txt_en)
###########################################################
FUNCTION get_str_tool(id)
  DEFINE 
    id         SMALLINT

  RETURN qxt_string_tool[id]

END FUNCTION

function xxx()
  define id smallint
  FOR id = 1 to 1000
    DISPLAY id, " = ", qxt_string_tool[id]
  END FOR
end function
###########################################################
# FUNCTION set_string_tool(p_id,p_str)
#
# Set a single string item
#
# RETURN NONE
###########################################################
FUNCTION set_string_tool(p_id,p_str)
  DEFINE
    p_id  INTEGER,
    p_str VARCHAR(100)

  LET qxt_string_tool[p_id] = p_str

END FUNCTION



