##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



####################################################
# FUNCTION grid_header_f_stock_scroll_g()
#
# Poupulates the grid columns
#
# RETURN NONE
####################################################
FUNCTION grid_header_f_stock_scroll_g()
  CALL fgl_grid_header("sa_stock_item","stock_id" ,get_str(1330),"left","F13")  -- Stock ID
  CALL fgl_grid_header("sa_stock_item","item_desc",get_str(1331),"left","F14")  -- Item Description
  CALL fgl_grid_header("sa_stock_item","item_cost",get_str(1332),"left","F15")  -- Item Cost
  CALL fgl_grid_header("sa_stock_item","tax_rate" ,get_str(1333),"left","F16")  -- Tax Rate
  CALL fgl_grid_header("sa_stock_item","quantity" ,get_str(2550),"left","F17")  -- Quantity
  CALL fgl_grid_header("sa_stock_item","reserved" ,get_str(2564),"left","F18")  -- Outstanding
  CALL fgl_grid_header("sa_stock_item","available",get_str(2552),"left","F19")  -- Available
  CALL fgl_grid_header("sa_stock_item","ordered"  ,get_str(2563),"left","F20")  -- On Order
END FUNCTION





#######################################################
# FUNCTION populate_stock_item_list_form_labels_g()
#
# Populate stock item list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_stock_item_list_form_labels_g()
  DISPLAY get_str(1350) TO lbTitle
  DISPLAY get_str(1359) TO lbInfo2

  
  DISPLAY get_str(819) TO bt_new
  #DISPLAY "!" TO bt_new

  DISPLAY get_str(817) TO bt_delete
  #DISPLAY "!" TO bt_delete

  DISPLAY get_str(818) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str(812) TO bt_close
  #DISPLAY "!" TO bt_close

  CALL grid_header_f_stock_scroll_g()
  CALL fgl_settitle(get_str(1350))  --"Stock Item List")

END FUNCTION


#######################################################
# FUNCTION populate_stock_item_form_labels_g()
#
# Populate stock item form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_stock_item_form_labels_g()
  DISPLAY get_str(1310) TO lbTitle
  DISPLAY get_str(1311) TO dl_f1
  DISPLAY get_str(1312) TO dl_f2
  DISPLAY get_str(1313) TO dl_f3
  DISPLAY get_str(1314) TO dl_f4
  DISPLAY get_str(2550) TO dl_f5
  DISPLAY get_str(2564) TO dl_f6
  DISPLAY get_str(2552) TO dl_f7
  DISPLAY get_str(2553) TO dl_f8
  DISPLAY get_str(2554) TO dl_f9
  DISPLAY get_str(2555) TO dl_f10
  DISPLAY get_str(1319) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION






#######################################################
# FUNCTION populate_stock_item_form_edit_labels_g()
#
# Populate stock item form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_stock_item_form_edit_labels_g()
  DISPLAY get_str(1310) TO lbTitle
  DISPLAY get_str(1311) TO dl_f1
  DISPLAY get_str(1312) TO dl_f2
  DISPLAY get_str(1313) TO dl_f3
  DISPLAY get_str(1314) TO dl_f4
  DISPLAY get_str(2550) TO dl_f5
  DISPLAY get_str(2564) TO dl_f6
  DISPLAY get_str(2552) TO dl_f7
  DISPLAY get_str(2553) TO dl_f8
  DISPLAY get_str(2554) TO dl_f9
  DISPLAY get_str(2555) TO dl_f10
  DISPLAY get_str(1319) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(1301))  --"Stock Item")

END FUNCTION




