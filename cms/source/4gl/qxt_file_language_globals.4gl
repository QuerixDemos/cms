##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

GLOBALS

  DEFINE 
    qxt_glob_int_switch SMALLINT

  DEFINE 
    t_qxt_language_rec TYPE AS 
      RECORD
        language_id     INTEGER,
        language_name   VARCHAR(20),
        language_dir    CHAR(2),
        language_url    VARCHAR(10)
      END RECORD

  DEFINE
    qxt_language_rec      DYNAMIC ARRAY OF t_qxt_language_rec,
    qxt_lang_array_count  SMALLINT,
    qxt_lang_array_size   SMALLINT,
    qxt_lang_file_name    VARCHAR(50)


END GLOBALS


