##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tbi_obj_event table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_qxt_tbi_obj_event(p_language_id)  get tbi_obj_event id from p_language_id        l_tbi_obj_event
# get_language_id(category_id)    Get language_id from p_tbi_obj_event        l_language_id
# get_qxt_tbi_obj_event_rec(p_tbi_obj_event)   Get the tbi_obj_event record from p_tbi_obj_event  l_tbi_obj_event.*
# tbi_obj_event_popup_data_source()               Data Source (cursor) for tbi_obj_event_popup              NONE
# tbi_obj_event_popup                             tbi_obj_event selection window                            p_tbi_obj_event
# (p_tbi_obj_event,p_order_field,p_accept_action)
# tbi_obj_event_combo_list(cb_field_name)         Populates tbi_obj_event combo list from db                NONE
# tbi_obj_event_create()                          Create a new tbi_obj_event record                         NULL
# tbi_obj_event_edit(p_tbi_obj_event)      Edit tbi_obj_event record                                 NONE
# tbi_obj_event_input(p_tbi_obj_event_rec)    Input tbi_obj_event details (edit/create)                 l_tbi_obj_event.*
# tbi_obj_event_delete(p_tbi_obj_event)    Delete a tbi_obj_event record                             NONE
# tbi_obj_event_view(p_tbi_obj_event)      View tbi_obj_event record by ID in window-form            NONE
# tbi_obj_event_view_by_rec(p_tbi_obj_event_rec) View tbi_obj_event record in window-form               NONE
# get_qxt_tbi_obj_event_from_language_id(p_language_id)          Get the tbi_obj_event from a file                      l_tbi_obj_event
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# tbi_obj_event_name_count(p_tbi_obj_event)  tests if a record with this tbi_obj_event already exists r_count
# copy_tbi_obj_event_record_to_form_record        Copy normal tbi_obj_event record data to type tbi_obj_event_form_rec   l_tbi_obj_event_form_rec.*
# (p_tbi_obj_event_rec)
# copy_tbi_obj_event_form_record_to_record        Copy type tbi_obj_event_form_rec to normal tbi_obj_event record data   l_tbi_obj_event_rec.*
# (p_tbi_obj_event_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_tbi_obj_event_scroll()              Populate tbi_obj_event grid headers                       NONE
# populate_tbi_obj_event_form_labels_g()          Populate tbi_obj_event form labels for gui                NONE
# populate_tbi_obj_event_form_labels_t()          Populate tbi_obj_event form labels for text               NONE
# populate_tbi_obj_event_form_edit_labels_g()     Populate tbi_obj_event form edit labels for gui           NONE
# populate_tbi_obj_event_form_edit_labels_t()     Populate tbi_obj_event form edit labels for text          NONE
# populate_tbi_obj_event_form_view_labels_g()     Populate tbi_obj_event form view labels for gui           NONE
# populate_tbi_obj_event_form_view_labels_t()     Populate tbi_obj_event form view labels for text          NONE
# populate_tbi_obj_event_form_create_labels_g()   Populate tbi_obj_event form create labels for gui         NONE
# populate_tbi_obj_event_form_create_labels_t()   Populate tbi_obj_event form create labels for text        NONE
# populate_tbi_obj_event_list_form_labels_g()     Populate tbi_obj_event list form labels for gui           NONE
# populate_tbi_obj_event_list_form_labels_t()     Populate tbi_obj_event list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


############################################################################################
# Data Access Functions
############################################################################################


#########################################################
# FUNCTION get_qxt_tbi_obj_event_rec(p_tbi_obj_event)
#
# get tbi_obj_event record from p_tbi_obj_event
#
# RETURN l_tbi_obj_event_rec.*
#########################################################
FUNCTION get_qxt_tbi_obj_event_rec(p_tbi_obj_event_name)
  DEFINE 
    p_tbi_obj_event_name        LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    l_tbi_obj_event_rec         RECORD LIKE qxt_tbi_obj_event.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_qxt_tbi_obj_event() - p_tbi_obj_event_name = ", p_tbi_obj_event_name
  END IF

  IF p_tbi_obj_event_name IS NULL THEN
    SELECT qxt_tbi_obj_event.*
      INTO l_tbi_obj_event_rec.*
      FROM qxt_tbi_obj_event
      WHERE qxt_tbi_obj_event.tbi_obj_event_name IS NULL

  ELSE

    SELECT qxt_tbi_obj_event.*
      INTO l_tbi_obj_event_rec.*
      FROM qxt_tbi_obj_event
      WHERE qxt_tbi_obj_event.tbi_obj_event_name = p_tbi_obj_event_name
  END IF

  RETURN l_tbi_obj_event_rec.*
END FUNCTION



########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION tbi_obj_event_combo_list(p_cb_field_name)
#
# Populates tbi_obj_event_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_obj_event_name_combo_list(p_cb_field_name)
  DEFINE 
    p_cb_field_name           VARCHAR(50),   --form field name for the country combo list field
    l_tbi_obj_event_name      DYNAMIC ARRAY OF LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    row_count                 INTEGER,
    current_row               INTEGER,
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_event_combo_list() - p_cb_field_name=", p_cb_field_name
  END IF

  DECLARE c_tbi_obj_event_scroll2 CURSOR FOR 
    SELECT qxt_tbi_obj_event.tbi_obj_event_name
      FROM qxt_tbi_obj_event
      ORDER BY tbi_obj_event_name ASC


  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_obj_event_scroll2 INTO l_tbi_obj_event_name[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_tbi_obj_event_name[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tbi_obj_event_name[row_count] CLIPPED, ")"
      DISPLAY "tbi_obj_event_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION



######################################################
# FUNCTION tbi_obj_event_popup_data_source()
#
# Data Source (cursor) for tbi_obj_event_popup
#
# RETURN NONE
######################################################
FUNCTION tbi_obj_event_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    p_language_id       LIKE qxt_language.language_id,
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_event_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "tbi_obj_event_popup_data_source() - p_ord_dir=", p_ord_dir

  ENd IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "tbi_obj_event_name"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_tbi_obj_event.tbi_obj_event_name ",
                 "FROM qxt_tbi_obj_event "
                    

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED,  " "
  END IF

  IF local_debug THEN
    DISPLAY "tbi_obj_event_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_tbi_obj_event FROM sql_stmt
  DECLARE c_tbi_obj_event CURSOR FOR p_tbi_obj_event

END FUNCTION


######################################################
# FUNCTION tbi_obj_event_popup(p_tbi_obj_event,p_order_field,p_accept_action)
#
# tbi_obj_event selection window
#
# RETURN p_tbi_obj_event
######################################################
FUNCTION tbi_obj_event_popup(p_tbi_obj_event_name,p_order_field,p_accept_action)
  DEFINE 
    p_tbi_obj_event_name           LIKE qxt_tbi_obj_event.tbi_obj_event_name,  --default return value if user cancels
    l_tbi_obj_event_arr            DYNAMIC ARRAY OF t_qxt_tbi_obj_event_form_rec,    --RECORD LIKE qxt_tbi_obj_event.*,  
    i                                               INTEGER,
    p_accept_action                                 SMALLINT,
    err_msg                                         VARCHAR(240),
    p_order_field,p_order_field2                    VARCHAR(128), 
    l_tbi_obj_event_name           LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    l_language_id                                   LIKE qxt_language.language_id,
    current_row                                     SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                                     SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1


  IF local_debug THEN
    DISPLAY "tbi_obj_event_popup() - p_tbi_obj_event_name=",p_tbi_obj_event_name
  END IF


#  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_tbi_obj_event_scroll", 2, 8, get_form_path("f_qxt_tbi_obj_event_scroll_l2"),FALSE) 
    CALL populate_tbi_obj_event_list_form_labels_g()
#  ELSE  --text
#    CALL fgl_window_open("w_tbi_obj_event_scroll", 2, 8, get_form_path("f_qxt_tbi_obj_event_scroll_t"),FALSE) 
#    CALL populate_tbi_obj_event_list_form_labels_t()
#  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_tbi_obj_event

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL tbi_obj_event_create()
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL tbi_obj_event_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_tbi_obj_event INTO l_tbi_obj_event_arr[i].*
      IF local_debug THEN
        DISPLAY "tbi_obj_event_popup() - i=",i
        DISPLAY "tbi_obj_event_popup() - l_tbi_obj_event_arr[i].tbi_obj_event_name=",l_tbi_obj_event_arr[i].tbi_obj_event_name
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 100 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_tbi_obj_event_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF    


    IF local_debug THEN
      DISPLAY "tbi_obj_event_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      CALL fgl_window_close("w_tbi_obj_event_scroll")
      RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "tbi_obj_event_popup() - set_count(i)=",i
    END IF

    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_tbi_obj_event_arr TO sc_tbi_obj_event.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL publish_toolbar("ToolbarTbiObjectEventList",0)

        CALL fgl_dialog_setcurrline(1,current_row)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET current_row = i
        LET l_tbi_obj_event_name = l_tbi_obj_event_arr[i].tbi_obj_event_name

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tbi_obj_event_view(l_tbi_obj_event_name)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL tbi_obj_event_edit(l_tbi_obj_event_name)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL tbi_obj_event_delete(l_tbi_obj_event_name)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL tbi_obj_event_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tbi_obj_event_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "tbi_obj_event_popup(p_tbi_obj_event,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("tbi_obj_event_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL tbi_obj_event_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_tbi_obj_event_name = l_tbi_obj_event_arr[i].tbi_obj_event_name

        CALL tbi_obj_event_edit(l_tbi_obj_event_name)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_tbi_obj_event_name = l_tbi_obj_event_arr[i].tbi_obj_event_name

        CALL tbi_obj_event_delete(l_tbi_obj_event_name)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_obj_event_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_tbi_obj_event_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_tbi_obj_event_name
  ELSE 
    RETURN l_tbi_obj_event_name
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION tbi_obj_event_create()
#
# Create a new tbi_obj_event record
#
# RETURN NULL
######################################################
FUNCTION tbi_obj_event_create()
  DEFINE 
    l_tbi_obj_event RECORD LIKE qxt_tbi_obj_event.*,
    local_debug     SMALLINT

  LET local_debug = FALSE


#select max column from table
#informix guide to sql

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_l2"), TRUE) 
    CALL populate_tbi_obj_event_form_create_labels_g()

#  ELSE
#    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_t"), TRUE) 
#    CALL populate_tbi_obj_event_form_create_labels_t()
#  END IF



  LET int_flag = FALSE
  
  #Initialise some variables

  IF local_debug THEN
    DISPLAY "tbi_obj_event_create() - l_tbi_obj_event.tbi_obj_event_name=", l_tbi_obj_event.tbi_obj_event_name
  END IF

  # CALL the INPUT
  CALL tbi_obj_event_input(l_tbi_obj_event.*)
    RETURNING l_tbi_obj_event.*

  CALL fgl_window_close("w_tbi_obj_event")

  IF NOT int_flag THEN
    INSERT INTO qxt_tbi_obj_event VALUES (
      l_tbi_obj_event.tbi_obj_event_name
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_tbi_obj_event.tbi_obj_event_name
    END IF

    #DISPLAY sqlca.sqlerrd[2]
  
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION tbi_obj_event_edit(p_tbi_obj_event)
#
# Edit tbi_obj_event record
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_event_edit(p_tbi_obj_event_name)
  DEFINE 
    p_tbi_obj_event_name       LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    l_key1_tbi_obj_event_name  LIKE qxt_tbi_obj_event.tbi_obj_event_name,

    l_tbi_obj_event_rec        RECORD LIKE qxt_tbi_obj_event.*,
    local_debug                                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_event_edit() - Function Entry Point"
    DISPLAY "tbi_obj_event_edit() - p_tbi_obj_event_name=", p_tbi_obj_event_name
  END IF

  #Store the primary key for the SQL update
  LET l_key1_tbi_obj_event_name = p_tbi_obj_event_name

  #Get the record (by id)
  CALL get_qxt_tbi_obj_event_rec(p_tbi_obj_event_name) RETURNING l_tbi_obj_event_rec.*


#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_l2"), TRUE) 
    CALL populate_tbi_obj_event_form_edit_labels_g()

#  ELSE
#    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_t"), TRUE) 
#    CALL populate_tbi_obj_event_form_edit_labels_t()
#  END IF


  #Call the INPUT
  CALL tbi_obj_event_input(l_tbi_obj_event_rec.*) 
    RETURNING l_tbi_obj_event_rec.*

    IF local_debug THEN
      DISPLAY "tbi_obj_event_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF


  CALL fgl_window_close("w_tbi_obj_event")

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "tbi_obj_event_edit() - l_tbi_obj_event_rec.tbi_obj_event=",l_tbi_obj_event_rec.tbi_obj_event_name
      DISPLAY "tbi_obj_event_edit() - l_key1_tbi_obj_event=",l_key1_tbi_obj_event_name

    END IF

    UPDATE  qxt_tbi_obj_event
      SET   tbi_obj_event_name =       l_tbi_obj_event_rec.tbi_obj_event_name
      WHERE qxt_tbi_obj_event.tbi_obj_event_name = l_key1_tbi_obj_event_name


    IF local_debug THEN
      DISPLAY "tbi_obj_event_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(825),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(825),NULL)
      RETURN l_tbi_obj_event_rec.tbi_obj_event_name
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(825),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION tbi_obj_event_input(p_tbi_obj_event_rec)
#
# Input tbi_obj_event details (edit/create)
#
# RETURN l_tbi_obj_event.*
#################################################
FUNCTION tbi_obj_event_input(p_tbi_obj_event_rec)
  DEFINE 
    p_tbi_obj_event_rec            RECORD LIKE qxt_tbi_obj_event.*,
    l_tbi_obj_event_form_rec       OF t_qxt_tbi_obj_event_form_rec,
    l_orignal_tbi_obj_event_name   LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    local_debug                    SMALLINT,
    tmp_str                        VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_event_input() - p_tbi_obj_event_rec.tbi_obj_event_name=",p_tbi_obj_event_rec.tbi_obj_event_name
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_tbi_obj_event_name = p_tbi_obj_event_rec.tbi_obj_event_name

  #copy record data to form_record format record
  CALL copy_tbi_obj_event_record_to_form_record(p_tbi_obj_event_rec.*) RETURNING l_tbi_obj_event_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_tbi_obj_event_form_rec.* WITHOUT DEFAULTS HELP 1

    AFTER FIELD tbi_obj_event_name
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(826), l_tbi_obj_event_form_rec.tbi_obj_event_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_event
      END IF



    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #tbi_obj_event must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(826), l_tbi_obj_event_form_rec.tbi_obj_event_name,NULL,TRUE)  THEN
          NEXT FIELD tbi_obj_event
          CONTINUE INPUT
        END IF


      #The tbi_obj_event - Primary KEY
      IF tbi_obj_event_name_count(l_tbi_obj_event_form_rec.tbi_obj_event_name) THEN
        #Constraint only exists for newly created records (not modify)
       IF l_orignal_tbi_obj_event_name IS NULL OR  --it is not an edit operation
          l_orignal_tbi_obj_event_name <> l_tbi_obj_event_form_rec.tbi_obj_event_name THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(826),NULL)
          NEXT FIELD tbi_obj_event_name
          CONTINUE INPUT
        END IF
      END IF

  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "tbi_obj_event_input() - l_tbi_obj_event_form_rec.tbi_obj_event_name=",l_tbi_obj_event_form_rec.tbi_obj_event_name
  END IF


  #Copy the form record data to a normal tbi_obj_event record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_tbi_obj_event_form_record_to_record(l_tbi_obj_event_form_rec.*) RETURNING p_tbi_obj_event_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "tbi_obj_event_input() - p_tbi_obj_event_rec.tbi_obj_event_name=",p_tbi_obj_event_rec.tbi_obj_event_name
  END IF

  RETURN p_tbi_obj_event_rec.*

END FUNCTION


#################################################
# FUNCTION tbi_obj_event_delete(p_tbi_obj_event)
#
# Delete a tbi_obj_event record
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_event_delete(p_tbi_obj_event_name)
  DEFINE 
    p_tbi_obj_event_name       LIKE qxt_tbi_obj_event.tbi_obj_event_name

  #do you really want to delete...
  IF question_delete_record(get_str_tool(825), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_tbi_obj_event 
        WHERE qxt_tbi_obj_event_name = p_tbi_obj_event_name
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION tbi_obj_event_view(p_tbi_obj_event)
#
# View tbi_obj_event record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_event_view(p_tbi_obj_event_name)
  DEFINE 
    p_tbi_obj_event_name        LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    l_tbi_obj_event_rec         RECORD LIKE qxt_tbi_obj_event.*


  CALL get_qxt_tbi_obj_event_rec(p_tbi_obj_event_name) RETURNING l_tbi_obj_event_rec.*
  CALL tbi_obj_event_view_by_rec(l_tbi_obj_event_rec.*)

END FUNCTION


#################################################
# FUNCTION tbi_obj_event_view_by_rec(p_tbi_obj_event_rec)
#
# View tbi_obj_event record in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_event_view_by_rec(p_tbi_obj_event_rec)
  DEFINE 
    p_tbi_obj_event_rec     RECORD LIKE qxt_tbi_obj_event.*,
    inp_char                                 CHAR,
    tmp_str                                  VARCHAR(250)

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_l2"), TRUE) 
    CALL populate_tbi_obj_event_form_view_labels_g()
#  ELSE
#    CALL fgl_window_open("w_tbi_obj_event", 3, 3, get_form_path("f_qxt_tbi_obj_event_det_t"), TRUE) 
#    CALL populate_tbi_obj_event_form_view_labels_t()
#  END IF

  DISPLAY BY NAME p_tbi_obj_event_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_tbi_obj_event")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################



####################################################
# FUNCTION tbi_obj_event_name_count(p_tbi_obj_event_name)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION tbi_obj_event_name_count(p_tbi_obj_event_name)
  DEFINE
    p_tbi_obj_event_name  LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_obj_event
      WHERE qxt_tbi_obj_event.tbi_obj_event_name = p_tbi_obj_event_name

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_tbi_obj_event_record_to_form_record(p_tbi_obj_event_rec)  
#
# Copy normal tbi_obj_event record data to type tbi_obj_event_form_rec
#
# RETURN l_tbi_obj_event_form_rec.*
######################################################
FUNCTION copy_tbi_obj_event_record_to_form_record(p_tbi_obj_event_rec)  
  DEFINE
    p_tbi_obj_event_rec       RECORD LIKE qxt_tbi_obj_event.*,
    l_tbi_obj_event_form_rec  OF t_qxt_tbi_obj_event_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_tbi_obj_event_form_rec.tbi_obj_event_name   = p_tbi_obj_event_rec.tbi_obj_event_name

  IF local_debug THEN
    DISPLAY "copy_tbi_obj_event_record_to_form_record() - p_tbi_obj_event_rec.tbi_obj_event_name=",   p_tbi_obj_event_rec.tbi_obj_event_name

    DISPLAY "copy_tbi_obj_event_record_to_form_record() - l_tbi_obj_event_form_rec.tbi_obj_event_name=", l_tbi_obj_event_form_rec.tbi_obj_event_name
  END IF

  RETURN l_tbi_obj_event_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_tbi_obj_event_form_record_to_record(p_tbi_obj_event_form_rec)  
#
# Copy type tbi_obj_event_form_rec to normal tbi_obj_event record data
#
# RETURN l_tbi_obj_event_rec.*
######################################################
FUNCTION copy_tbi_obj_event_form_record_to_record(p_tbi_obj_event_form_rec)  
  DEFINE
    l_tbi_obj_event_rec       RECORD LIKE qxt_tbi_obj_event.*,
    p_tbi_obj_event_form_rec  OF t_qxt_tbi_obj_event_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_tbi_obj_event_rec.tbi_obj_event_name   = p_tbi_obj_event_form_rec.tbi_obj_event_name

  IF local_debug THEN
    DISPLAY "copy_tbi_obj_event_form_record_to_record() - p_tbi_obj_event_form_rec.tbi_obj_event_name=",   p_tbi_obj_event_form_rec.tbi_obj_event_name

    DISPLAY "copy_tbi_obj_event_form_record_to_record() - l_tbi_obj_event_rec.tbi_obj_event_name=", l_tbi_obj_event_rec.tbi_obj_event_name
  END IF


  RETURN l_tbi_obj_event_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_tbi_obj_event_scroll()
#
# Populate tbi_obj_event grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_tbi_obj_event_scroll()
  CALL fgl_grid_header("sc_tbi_obj_event","tbi_obj_event_name",  get_str_tool(826),"center","F13")  --tbi_obj_event

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_event_form_labels_g()
#
# Populate tbi_obj_event form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(825) TO lbTitle

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_event_form_labels_t()
#
# Populate tbi_obj_event form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_labels_t()

  DISPLAY get_str_tool(825) TO lbTitle

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_event_form_edit_labels_g()
#
# Populate tbi_obj_event form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(825)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_name")



END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_event_form_edit_labels_t()
#
# Populate tbi_obj_event form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_edit_labels_t()

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_event_form_view_labels_g()
#
# Populate tbi_obj_event form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(825)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_event_form_view_labels_t()
#
# Populate tbi_obj_event form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_view_labels_t()

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_language_id")

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_event_form_create_labels_g()
#
# Populate tbi_obj_event form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(825)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_event_form_create_labels_t()
#
# Populate tbi_obj_event form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_form_create_labels_t()

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_event_list_form_labels_g()
#
# Populate tbi_obj_event list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(825)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_tbi_obj_event_scroll()

  #DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4


  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete


END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_event_list_form_labels_t()
#
# Populate tbi_obj_event list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_event_list_form_labels_t()

  DISPLAY trim(get_str_tool(825)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(826) TO dl_f1
  #DISPLAY get_str_tool(827) TO dl_f2
  #DISPLAY get_str_tool(828) TO dl_f3
  #DISPLAY get_str_tool(829) TO dl_f4

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################


















































