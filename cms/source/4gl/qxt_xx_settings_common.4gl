##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Common / Shared functions for Settings String
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

FUNCTION get_source_id(p_source_name)
  DEFINE 
    p_source_name   VARCHAR(20)

  LET p_source_name = downshift(p_source_name)
  CASE p_source_name
    WHEN "db" 
      RETURN 1
    WHEN "file"
      RETURN 2
    WHEN "n/a"
      RETURN 3
    OTHERWISE
      CALL fgl_winmessage("Invalid argument in get_source_id()","p_source_name=" || p_source_name, "error")
  END CASE

  RETURN NULL

END FUNCTION


FUNCTION get_source_name(p_source_id)
  DEFINE 
    p_source_id  SMALLINT

  CASE p_source_id
    WHEN 1
      RETURN "DB"
    WHEN 2
      RETURN "File"
    WHEN 3
      RETURN "N/A"
    OTHERWISE
      CALL fgl_winmessage("Invalid argument in get_source_name()","p_source_name=" || p_source_id, "error")
  END CASE

  RETURN NULL

END FUNCTION


FUNCTION source_name_combo_list(cb_field_name)
  DEFINE
    cb_field_name      VARCHAR(40)   --form field name for the country combo list field

    CALL fgl_list_set(cb_field_name,1, "DB")
    CALL fgl_list_set(cb_field_name,2, "File")
    CALL fgl_list_set(cb_field_name,3, "N/A")


END FUNCTION
