##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the print html template library
#
#
#
################################################################################


################################################################################
# GLOBALS
################################################################################
GLOBALS

 # DEFINE
 #   qxt_print_html_template DYNAMIC ARRAY OF VARCHAR(80)   --Template file names for html print

  DEFINE
    t_qxt_print_html_template_rec TYPE AS
      RECORD
        id           SMALLINT,
        language_id  SMALLINT,
        filename     VARCHAR(100)
      END RECORD

  DEFINE
    t_qxt_print_html_template_short_rec TYPE AS
      RECORD
        lang_id  SMALLINT,
        filename VARCHAR(100)
      END RECORD


  DEFINE
    qxt_print_html_template_rec_arr DYNAMIC ARRAY OF t_qxt_print_html_template_rec   --Template file names for html print


END GLOBALS

