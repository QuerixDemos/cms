##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to access path / file access / file location
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# get_client_temp_directory()                     Return client temp directory                               qxt_client_temp_directory
# get_client_temp_path(file_name)                 Return file name with client temp path                     file_path
# get_cfg_directory()                             Return cfg directory                                       qxt_settings.cfg_path
# get_cfg_path(cfg_name)                          Return file name with cfg path                             trim(ret)
# get_form_directory()                            Return cfg directory                                       qxt_settings.form_path
# get_form_path(form_name)                        Return file name with form path                            trim(ret)
# get_image_directory()                           Return image directory                                     qxt_settings.image_path
# get_image_path(image_name)                      Return file name with image path                           trim(ret)
# get_document_directory()                        Return document directory                                  qxt_settings.document_path
# get_document_path(document_name)                Return file name with document path                        trim(ret)
# get_html_directory()                            Return html directory name                                 qxt_settings.html_path
# get_html_path(document_name)                    Return file name with html path                            trim(ret)
# get_msg_directory()                             Return msg directory                                       qxt_settings.msg_path 
# get_msg_path(file_name)                         Return file name with msg path                             trim(ret)
# get_unl_directory()                             Return unl directory                                       qxt_settings.unl_path
# get_unl_path(file_name)                         Return file name with unl path                             trim(ret)
# get_server_blob_temp_directory()                Return directory where the temporary blob files            qxt_settings.unl_path
# get_server_blob_temp_path(file_name)            Return file name with server temporary blob location path  trim(ret)                                                are stored on the server
# clean_server_blob_temp_directory()              Clean the temporary blob directory on the app server       NONE
# get_online_demo_path()                          Return online path                                         qxt_settings.unl_path
# clean_app_server_temp_files()                   Delete all temporary files from the application server     NONE
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


##################################################################################
# FUNCTION get_client_temp_directory()
#
# Return client temp directory
#
# RETURN qxt_client_temp_directory
##################################################################################
FUNCTION get_client_temp_directory()

  RETURN qxt_client_temp_directory

END FUNCTION


##################################################################################
# FUNCTION get_client_temp_path(file_name)
#
# Return file name with client temp path
#
# RETURN file_path
##################################################################################
FUNCTION get_client_temp_path(file_name)
  DEFINE
    file_path VARCHAR(200),
    file_name VARCHAR(100)
#was:   LET file_path = qxt_client_temp_directory CLIPPED, "/", file_name changed 20.4.2016 Huho
  LET file_path = qxt_client_temp_directory CLIPPED, trim(file_name)

  RETURN file_path

END FUNCTION


##################################################################################
# FUNCTION get_cfg_directory()
#
# Return cfg directory
#
# RETURN qxt_settings.cfg_path
##################################################################################
FUNCTION get_cfg_directory()

  RETURN qxt_settings.cfg_path

END FUNCTION


##################################################################################
# FUNCTION get_cfg_path(cfg_name)
#
# Return file name with cfg path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_cfg_path(cfg_name)
  DEFINE
    cfg_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.cfg_path), "/", trim(cfg_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_form_directory()
#
# Return cfg directory
#
# RETURN qxt_settings.form_path
##################################################################################
FUNCTION get_form_directory()

  RETURN qxt_settings.form_path

END FUNCTION


##################################################################################
# FUNCTION get_form_path(form_name)
#
# Return file name with form path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_form_path(form_name)
  DEFINE
    form_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.form_path), "/", trim(form_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_image_directory()
#
# Return image directory
#
# RETURN qxt_settings.image_path
##################################################################################
FUNCTION get_image_directory()

  RETURN qxt_settings.image_path

END FUNCTION


##################################################################################
# FUNCTION get_image_path(image_name)
#
# Return file name with image path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_image_path(image_name)
  DEFINE
    image_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.image_path), "/", trim(image_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_document_directory()
#
# Return document directory
#
# RETURN qxt_settings.document_path
##################################################################################
FUNCTION get_document_directory()

  RETURN qxt_settings.document_path

END FUNCTION


##################################################################################
# FUNCTION get_document_path(document_name)
#
# Return file name with document path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_document_path(document_name)
  DEFINE
    document_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.document_path), "/", trim(document_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_html_directory()
#
# Return html directory name
#
# RETURN qxt_settings.html_path
##################################################################################
FUNCTION get_html_directory()

  RETURN qxt_settings.html_path

END FUNCTION

##################################################################################
# FUNCTION get_html_path(document_name)
#
# Return file name with html path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_html_path(document_name)
  DEFINE
    document_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.html_path), "/", trim(document_name)
  RETURN trim(ret)
END FUNCTION



##################################################################################
# FUNCTION get_msg_directory()
#
# Return msg directory
#
# RETURN qxt_settings.msg_path
##################################################################################
FUNCTION get_msg_directory()

  RETURN qxt_settings.msg_path

END FUNCTION


##################################################################################
# FUNCTION get_msg_path(file_name)
#
# Return file name with msg path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_msg_path(file_name)
  DEFINE
    file_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.msg_path), "/", trim(file_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_unl_directory()
#
# Return unl directory
#
# RETURN qxt_settings.unl_path
##################################################################################
FUNCTION get_unl_directory()

  RETURN qxt_settings.unl_path

END FUNCTION


##################################################################################
# FUNCTION get_unl_path(file_name)
#
# Return file name with unl path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_unl_path(file_name)
  DEFINE
    file_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.unl_path), "/", trim(file_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_server_blob_temp_directory()
#
# Return directory where the temporary blob files are stored on the server
#
# RETURN qxt_settings.unl_path
##################################################################################
FUNCTION get_server_blob_temp_directory()

  RETURN qxt_settings.server_blob_temp_path

END FUNCTION


##################################################################################
# FUNCTION clean_server_blob_temp_directory()
#
# Clean the temporary blob directory on the app server
#
# RETURN NONE
##################################################################################
FUNCTION clean_server_blob_temp_directory()

  CALL fgl_rm(trim(qxt_settings.server_blob_temp_path) || "/*.*")

END FUNCTION



##################################################################################
# FUNCTION get_server_blob_temp_path(file_name)
#
# Return file name with server temporary blob location path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_server_blob_temp_path(file_name)
  DEFINE
    file_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.server_blob_temp_path), "/", trim(file_name)
  RETURN trim(ret)
END FUNCTION





##################################################################################
# FUNCTION get_server_app_temp_directory()
#
# Return directory where the temporary app files are stored on the server
#
# RETURN qxt_settings.unl_path
##################################################################################
FUNCTION get_server_app_temp_directory()

  RETURN qxt_settings.server_app_temp_path

END FUNCTION


##################################################################################
# FUNCTION clean_server_app_temp_directory()
#
# Clean the temporary app directory on the app server
#
# RETURN NONE
##################################################################################
FUNCTION clean_server_app_temp_directory()

  CALL fgl_rm(trim(qxt_settings.server_app_temp_path) || "/*.*")

END FUNCTION



##################################################################################
# FUNCTION get_server_app_temp_path(file_name)
#
# Return file name with server temporary app location path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_server_app_temp_path(file_name)
  DEFINE
    file_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.server_app_temp_path), "/", trim(file_name)
  RETURN trim(ret)
END FUNCTION




##################################################################################
# FUNCTION clean_app_server_temp_files()
#
# Delete all temporary files from the application server
#
# RETURN NONE
##################################################################################
FUNCTION clean_app_server_temp_files()

  CALL clean_server_app_temp_directory()
  #... add here any function calls to remove temporary files from the app server

END FUNCTION



##################################################################################
# FUNCTION get_online_demo_path()
#
# Return online path
#
# RETURN qxt_settings.unl_path
##################################################################################
FUNCTION get_online_demo_path()

  RETURN qxt_settings.online_demo_path

END FUNCTION



###################################################################################################
# EOF
###################################################################################################
