##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_string_in_memory_globals.4gl"

###########################################################
# FUNCTION process_string_import_file_to_memory(file_name)
#
# Import string file to memory
#
# RETURN NONE
###########################################################
FUNCTION process_string_import_to_memory(p_category)

  #International strings
  DEFINE
    file_name, tempstr            VARCHAR(100),
    local_debug,str_length        SMALLINT,
    p_category                    SMALLINT,
    row_count                     SMALLINT,
    string_temp_rec               OF t_qxt_string_rec,
    tmp_str                       VARCHAR(200)

  LET local_debug = FALSE

  #LET qxt_settings.string_system_type = 0 --String data from a TEXT file

  CASE p_category
    WHEN 0  --tools
      LET file_name = get_unl_path(qxt_settings.tool_string_file_name)
    WHEN 1  --app
      LET file_name =  get_unl_path(qxt_settings.app_string_file_name)
    OTHERWISE
      CALL fgl_winmessage("Error in process_string_import_to_memory()","process_string_import_to_memory()\nInvalid p_category","error")
      EXIT PROGRAM
  END CASE

  IF local_debug THEN
    DISPLAY "process_string_import_to_memory() - p_category=", p_category
    DISPLAY "process_string_import_to_memory() - file_name=", file_name

  END IF


  IF NOT fgl_test("e",file_name) THEN
    LET qxt_err_msg = "Following string import file does not exist: ", trim(file_name), "\nApplication needs closing down"
    CALL fgl_winmessage("File Error in process_string_import()" ,qxt_err_msg, "error")
    EXIT PROGRAM
  END IF

  IF NOT fgl_channel_open_file("stream",file_name, "r") THEN
    LET tmp_str = "Error in process_string_import_to_memory()\nCan not open file ", trim(file_name)
    CALL fgl_winmessage("Error in process_string_import_to_memory()", tmp_str, "error")
    RETURN
  END IF

  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET tmp_str = "Error in process_string_import_to_memory()\nCan not set delimiter for file ", trim(file_name)
    CALL fgl_winmessage("Error in process_string_import_to_memory()", tmp_str, "error")
    RETURN
  END IF



  IF local_debug THEN
    DISPLAY "### start ##### ",file_name, " ########### process_string_import(" , file_name, ")"  
  END IF


  LET row_count = 0


  WHILE fgl_channel_read("stream",string_temp_rec.*)
    IF  string_temp_rec.string_id IS NOT NULL AND string_temp_rec.string_id != 0 THEN
      LET row_count = row_count + 1
    ELSE
      EXIT WHILE
    END IF


    IF local_debug THEN
      DISPLAY "row_count = ", row_count
      #DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_id ", string_temp_rec.string_id
    END IF


    CASE p_category
      WHEN 0  -- tools strings
        IF string_temp_rec.language_id = qxt_settings.language THEN  --Only load strings to memory with current language_id 

          #Import Debug Help
          IF local_debug THEN
            #DISPLAY "### In multi_lingual_tool_str Loop ##### ########### process_string_import(" , file_name, ")" 
            #DISPLAY "process_string_import_file_to_memory() - qxt_tool_string[string_temp_rec.string_id] :  ", qxt_tool_string[string_temp_rec.string_id]
            DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_id:                             ", string_temp_rec.string_id
            #DISPLAY "process_string_import_file_to_memory() - string_temp_rec.language_id:                           ", string_temp_rec.language_id
            DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_data:                           ", string_temp_rec.string_data
          END IF

          LET qxt_tool_string[string_temp_rec.string_id] = string_temp_rec.string_data

        END IF


      WHEN 1  --  Program/application related strings
        IF string_temp_rec.language_id = qxt_settings.language THEN  --Only load strings to memory with current language_id 
          #Import Debug Help
          IF local_debug THEN
            #DISPLAY "### In multi_lingual_tool_str Loop ##### ########### process_string_import(" , file_name, ")" 
            #DISPLAY "process_string_import_file_to_memory() - qxt_tool_string[string_temp_rec.string_id] :  ", qxt_tool_string[string_temp_rec.string_id]
            DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_id:                             ", string_temp_rec.string_id
            #DISPLAY "process_string_import_file_to_memory() - string_temp_rec.language_id:                           ", string_temp_rec.language_id
            DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_data:                           ", string_temp_rec.string_data
          END IF

          LET qxt_app_string[string_temp_rec.string_id] = string_temp_rec.string_data


        END IF


      OTHERWISE

        LET qxt_err_msg = "Error in process_string_import_to_memory()\nInvalid argument p_category=", p_category
        CALL fgl_winmessage("Error in process_string_import_to_memory()", qxt_err_msg,"error")

    END CASE

    #LET id = id + 1
  END WHILE

    IF NOT fgl_channel_close("stream") THEN
      LET tmp_str = "Error in process_string_import_to_memory()\nCan not close file ", trim(file_name)
      CALL fgl_winmessage("Error in process_string_import_to_memory()", tmp_str, "error")
      RETURN
    END IF


  IF row_count < 1 THEN --nothing was imported i.e. file is empty
        LET qxt_err_msg = "Error in process_string_import_to_memory()\nThe import file ", trim(file_name), " is empty or does not exist!\nrow_count = ", row_count
        CALL fgl_winmessage("Error in process_string_import_to_memory()",qxt_err_msg,"error")
  END IF


  IF local_debug THEN
    DISPLAY "### END of int_str file import - File Name: ", file_name, " p_category= ", p_category   
  END IF

  CALL fgl_channel_close("stream")

END FUNCTION





###########################################################
# FUNCTION process_memory_string_to_file(file_name)
#
# Import string file to memory
#
# RETURN NONE
###########################################################
FUNCTION process_memory_string_to_file(category,file_name)

#Note: we only update the current language string - this means, we have to import the entire language string, 
#update it with the current string set and export it together

  #International strings
  DEFINE
    file_name, tempstr            VARCHAR(100),
    local_debug,id,id2,str_length SMALLINT,
    category                      SMALLINT,
    row_count                     SMALLINT,
    string_temp_rec               OF t_qxt_string_rec,
    string_temp_rec_arr           DYNAMIC ARRAY OF t_qxt_string_rec
  LET local_debug = FALSE
  LET id = 1

  #Check if the files exists
  IF NOT fgl_test("e",file_name) THEN
    LET qxt_err_msg = "Following string export-update file does not exist: ", file_name, "\nAbort String Update (Export) Operation"
    CALL fgl_winmessage("File Error in process_string_import()" ,qxt_err_msg, "error")
    RETURN
  END IF


  #Import the entire string file 
  LET row_count = 1
  WHILE fgl_channel_read("stream",string_temp_rec.*)
    IF  string_temp_rec.string_id IS NOT NULL AND string_temp_rec.string_id != 0 THEN
      #LET id2 =  qxt_tmp_int_str.string_id

      LET string_temp_rec_arr[string_temp_rec.string_id].string_id = string_temp_rec.string_id
      LET string_temp_rec_arr[string_temp_rec.string_id].language_id = string_temp_rec.language_id
      LET string_temp_rec_arr[string_temp_rec.string_id].string_data = string_temp_rec.string_data

      LET row_count = row_count + 1
    ELSE
      LET row_count = row_count - 1  --correct
      EXIT WHILE
    END IF

    IF local_debug THEN
      DISPLAY "process_memory_string_to_file() - row_count = ", row_count
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[string_temp_rec.string_id].string_id ", string_temp_rec_arr[string_temp_rec.string_id].string_id
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[string_temp_rec.string_id].language_id ", string_temp_rec_arr[string_temp_rec.string_id].language_id
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[string_temp_rec.string_id].string_data ", string_temp_rec_arr[string_temp_rec.string_id].string_data
    END IF

  END WHILE

  IF row_count < 1 THEN --nothing was imported i.e. file is empty
        LET qxt_err_msg = get_str_tool(726) CLIPPED, file_name
        CALL fgl_winmessage(get_str_tool(30),qxt_err_msg,"error")
        RETURN "error"
  END IF

  IF local_debug THEN
    DISPLAY "### END of process_memory_string_to_file - File Name: ", trim(file_name), " Category= ", trim(category), "row_count=", trim(row_count)   
  END IF

  CALL fgl_channel_close("stream")


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# MERGE - Overwrite big array string with current language strings

    CASE category
      WHEN 0  -- tools strings
        FOR id = 1 TO row_count
          IF string_temp_rec_arr[id].language_id = qxt_settings.language THEN
            LET string_temp_rec_arr[id].string_data = qxt_tool_string[id]
          END IF
        END FOR

      WHEN 0  -- tools strings
        FOR id = 1 TO row_count
          IF string_temp_rec_arr[id].language_id = qxt_settings.language THEN
            LET string_temp_rec_arr[id].string_data = qxt_app_string[id]
          END IF
        END FOR

      OTHERWISE
        CALL fgl_winmessage("Error in process_memory_string_to_file()", "Error in process_memory_string_to_file()\nInvalid category","error")
        RETURN "ERROR"
    END CASE

#@@@@@@@@@@@@@@@@@@@@
  #Export newly merged string from memory to file
  CALL fgl_channel_open_file("stream",file_name, "w")

  CALL fgl_channel_set_delimiter("stream","|")

  IF local_debug THEN
    DISPLAY "### start ##### ",file_name, " ########### process_memory_string_to_file(" , file_name, ")"  
  END IF



{
    t_string_app       ARRAY[3000] OF VARCHAR(100),
    t_string_tool      ARRAY[1000] OF VARCHAR(100),
    t_string_temp_rec  
      RECORD
        string_id     SMALLINT,
        language_id   SMALLINT,
        string_data   VARCHAR(100)
      END RECORD
}

  LET row_count = 1

  WHILE fgl_channel_write("stream",string_temp_rec_arr[row_count].*)
    IF  string_temp_rec_arr[row_count].string_id IS NOT NULL AND string_temp_rec_arr[row_count].string_id != 0 THEN
      #LET id2 =  qxt_tmp_int_str.string_id
      LET row_count = row_count + 1
    ELSE
      LET row_count = row_count - 1
      EXIT WHILE
    END IF

    IF local_debug THEN
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[row_count].string_id = ", string_temp_rec_arr[row_count].string_id 
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[row_count].language_id = ", string_temp_rec_arr[row_count].language_id 
      DISPLAY "process_memory_string_to_file() - string_temp_rec_arr[row_count].string_data = ", string_temp_rec_arr[row_count].string_data 
    END IF

  END WHILE

  IF row_count < 1 THEN --nothing was imported i.e. file is empty
        LET qxt_err_msg = get_str_tool(726) CLIPPED, file_name
        CALL fgl_winmessage(get_str_tool(30),qxt_err_msg,"error")
  END IF


  IF local_debug THEN
    DISPLAY "### END of process_memory_string_to_file - File Name: ", trim(file_name), " Category= ", trim(category) , "Row_count=", trim(row_count)  
  END IF

  CALL fgl_channel_close("stream")

END FUNCTION






