##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

########################################################################################################################
# Email functions
########################################################################################################################
#
# FUNCTION:                                             DESCRIPTION:                                   RETURN
# email_write(p_contact_id, l_subject,                  Write an E-Mail                                NONE
#             l_body, actid, p_operator_id2)          
# email_send((p_email, p_subject, p_body,               Send an E-Mail                                 INT i   (i = sqlca.sqlerrd[2])
#            actid, p_operator)
# email_view(p_email_id,direction)                      displays email details in window               NONE
# email_reply (p_mail_id)                               Reply to an email                              NONE
# email_create(p_email_rec)                             Create an mailbox & activity rec               mailbox_id
# rec_copy_t_email_rec_to_activity(p_email_rec)         Copy t_emai_rec data to activity rec           l_activity.*
# rec_copy_t_email_rec_to_mailbox(p_email_rec)          Copy t_emai_rec data to mailbox rec            l_mailbox.*
# rec_copy_t_email_rec_to_t_email_form_rec(p_email_rec) Copy t_email_rec data to t_email_form_rec      l_email_form_rec.*
# rec_copy_mailbox_to_t_email_rec(p_mailbox_rec)        Copy mailbox record data to t_email_rec        l_email_rec.*
# rec_copy_activity_rec_t_email_rec(p_activity_rec)     Copy activity record to t_emai_rec             l_email_rec.*
# rec_copy_mailbox_and_activity_rec_t_email_rec         Copy mailbox & activity record to t_email_rec  l_email_rec.*
#  (p_mailbox_rec,p_activity_rec)
#
# get_email_activity_id(p_mail_id)                      get email activity id from mail_id             r_activity_id LIKE activity.activity_id
# get_t_email_rec_from_mail_id(p_mail_id)               get a email record from the mail id            l_email_rec.*
#
# email_set_read(p_email_id)                            Set the read_flag to true                      NONE
# email_set_unread(p_email_id)                          Set the read_flag to false                     NONE
########################################################################################################################


#################################################
# GLOBALS
#################################################
GLOBALS "globals.4gl"

#################################################
# FUNCTION get_email_activity_id(p_mail_id)
#
# get email activity id from mail_id
#
# RETURN r_activity_id LIKE activity.activity_id
#################################################
FUNCTION get_email_activity_id(p_mail_id)
  DEFINE 
    p_mail_id LIKE mailbox.mail_id,
    r_activity_id LIKE activity.activity_id

  SELECT activity_id
    INTO r_activity_id
    FROM mailbox
    WHERE mailbox.mail_id = p_mail_id

  RETURN r_activity_id

END FUNCTION

###########################################################
# FUNCTION get_t_email_rec_from_mail_id(p_mail_id)
#
# get a email record from the mail id
#
# RETURN l_email_rec.*
###########################################################
FUNCTION get_t_email_rec_from_mail_id(p_mail_id)
  DEFINE 
    p_mail_id      LIKE mailbox.mail_id,
    l_mailbox_rec  RECORD LIKE mailbox.*,
    l_activity_rec RECORD LIKE activity.*,
    l_email_rec     OF t_email_rec

  LOCATE l_activity_rec.long_desc IN MEMORY
  LOCATE l_email_rec.long_desc IN MEMORY


  CALL get_mailbox_rec_from_mail_id(p_mail_id) RETURNING l_mailbox_rec.*
  CALL get_activity_rec(l_mailbox_rec.activity_id) RETURNING l_activity_rec.*

  CALL rec_copy_mailbox_and_activity_rec_t_email_rec(l_mailbox_rec.*,l_activity_rec.*,l_email_rec.*) RETURNING l_email_rec.*

  RETURN l_email_rec.*

END FUNCTION


##########################################################################
# FUNCTION rec_copy_t_email_rec_to_t_email_form_rec(p_email_rec)
#
# Copy t_email_rec data to t_email_form_rec 
#
# RETURN l_email_form_rec.*
##########################################################################
FUNCTION rec_copy_t_email_rec_to_t_email_form_rec(p_email_rec,p_email_form_rec)
DEFINE
  p_email_rec OF t_email_rec,
  p_email_form_rec OF t_email_form_rec

  LET p_email_form_rec.from_email_address = p_email_rec.from_email_address
  LET p_email_form_rec.from_cont_id = p_email_rec.from_cont_id
  LET p_email_form_rec.from_cont_fname = p_email_rec.from_cont_fname
  LET p_email_form_rec.from_cont_lname = p_email_rec.from_cont_lname
  LET p_email_form_rec.to_email_address = p_email_rec.to_email_address
  LET p_email_form_rec.to_cont_id = p_email_rec.to_cont_id
  LET p_email_form_rec.to_cont_fname = p_email_rec.to_cont_fname
  LET p_email_form_rec.to_cont_lname = p_email_rec.to_cont_lname
  LET p_email_form_rec.recv_date = p_email_rec.recv_date
  LET p_email_form_rec.read_flag = p_email_rec.read_flag
  LET p_email_form_rec.urgent_flag = p_email_rec.urgent_flag
  LET p_email_form_rec.short_desc = p_email_rec.short_desc
  LET p_email_form_rec.long_desc = p_email_rec.long_desc
  LET p_email_form_rec.mail_status = p_email_rec.mail_status

  RETURN p_email_form_rec.*
END FUNCTION


#################################################
# FUNCTION rec_copy_t_email_rec_to_activity(p_email_rec)
#
# Copy t_emai_rec data to activity rec
#
# RETURN l_activity.*
#################################################
FUNCTION rec_copy_t_email_rec_to_activity(p_email_rec,p_activity)
  DEFINE
    p_email_rec  OF t_email_rec,
    p_activity   RECORD LIKE activity.*

  LET p_activity.open_date = p_email_rec.recv_date
  LET p_activity.contact_id = p_email_rec.to_cont_id
  LET p_activity.comp_id = get_contact_company_id(p_email_rec.to_cont_id)
  LET p_activity.operator_id = p_email_rec.operator_id

  LET p_activity.short_desc = p_email_rec.short_desc
  LET p_activity.long_desc =  p_email_rec.long_desc
  LET p_activity.a_owner =   p_email_rec.operator_id

  IF p_email_rec.urgent_flag THEN
    LET p_activity.priority = 2
  ELSE
    LET p_activity.priority = 1
  END IF

  LET p_activity.act_type = 1  --email is always activity type 1

  RETURN p_activity.*
END FUNCTION



#################################################
# FUNCTION rec_copy_activity_rec_t_email_rec(p_activity_rec)
#
# Copy activity record to t_emai_rec
#
# RETURN l_email_rec.*
#################################################
FUNCTION rec_copy_activity_rec_t_email_rec(p_activity_rec,p_email_rec)
  DEFINE
    p_email_rec      OF t_email_rec,
    p_activity_rec   RECORD LIKE activity.*

  #LET l_email_rec.recv_date = p_activity_rec.open_date
  #LET l_email_rec.to_cont_id = p_activity_rec.contact_id
  #LET l_email_rec.operator_id = p_activity_rec.operator_id
  LET p_email_rec.short_desc = p_activity_rec.short_desc
  LET p_email_rec.long_desc =  p_activity_rec.long_desc

  RETURN p_email_rec.*
END FUNCTION




#################################################
# FUNCTION rec_copy_mailbox_and_activity_rec_t_email_rec(p_mailbox_rec,p_activity_rec)
#
# Copy mailbox & activity record to t_email_rec
#
# RETURN l_email_rec.*
#################################################
FUNCTION rec_copy_mailbox_and_activity_rec_t_email_rec(p_mailbox_rec,p_activity_rec,p_email_rec)
  DEFINE
    p_email_rec      OF t_email_rec,
    p_mailbox_rec    RECORD LIKE mailbox.*,
    p_activity_rec   RECORD LIKE activity.*

  CALL rec_copy_mailbox_to_t_email_rec(p_mailbox_rec.*, p_email_rec.*)   RETURNING p_email_rec.*

  CALL rec_copy_activity_rec_t_email_rec(p_activity_rec.*,p_email_rec.*)  RETURNING p_email_rec.*

  RETURN p_email_rec.*
END FUNCTION




#################################################
# FUNCTION rec_copy_t_email_rec_to_mailbox(p_email_rec)
#
# Copy t_emai_rec data to mailbox rec
#
# RETURN l_mailbox.*
#################################################
FUNCTION rec_copy_t_email_rec_to_mailbox(p_email_rec,p_mailbox_rec)
  DEFINE
    p_email_rec   OF t_email_rec,
    p_mailbox_rec RECORD LIKE mailbox.*

  LET p_mailbox_rec.mail_id = p_email_rec.mail_id
  LET p_mailbox_rec.activity_id =  p_email_rec.activity_id  -- 0 will create auto index
  LET p_mailbox_rec.operator_id =  p_email_rec.operator_id
  LET p_mailbox_rec.from_email_address =  p_email_rec.from_email_address
  LET p_mailbox_rec.from_cont_id =  p_email_rec.from_cont_id
  LET p_mailbox_rec.to_email_address =  p_email_rec.to_email_address
  LET p_mailbox_rec.to_cont_id =  p_email_rec.to_cont_id
  LET p_mailbox_rec.recv_date =  p_email_rec.recv_date
  LET p_mailbox_rec.read_flag =  p_email_rec.read_flag
  LET p_mailbox_rec.urgent_flag =  p_email_rec.urgent_flag
  LET p_mailbox_rec.mail_status =  p_email_rec.mail_status

  RETURN p_mailbox_rec.*
END FUNCTION



#################################################
# FUNCTION rec_copy_mailbox_to_t_email_rec(p_mailbox_rec)
#
# Copy mailbox record data to t_email_rec 
#
# RETURN l_email_rec.*
#################################################
FUNCTION rec_copy_mailbox_to_t_email_rec(p_mailbox_rec,p_email_rec)
  DEFINE
    p_email_rec  OF t_email_rec,
    p_mailbox_rec RECORD LIKE mailbox.*

  LET p_email_rec.mail_id = p_mailbox_rec.mail_id
  LET p_email_rec.activity_id =  p_mailbox_rec.activity_id  -- 0 will create auto index
  LET p_email_rec.operator_id =  p_mailbox_rec.operator_id

  LET p_email_rec.from_email_address =  p_mailbox_rec.from_email_address
  LET p_email_rec.from_cont_id =  p_mailbox_rec.from_cont_id
  LET p_email_rec.from_cont_fname = get_contact_fname(p_mailbox_rec.from_cont_id)
  LET p_email_rec.from_cont_lname = get_contact_lname(p_mailbox_rec.from_cont_id)

  LET p_email_rec.to_email_address =  p_mailbox_rec.to_email_address
  LET p_email_rec.to_cont_id =  p_mailbox_rec.to_cont_id
  LET p_email_rec.to_cont_fname = get_contact_fname(p_mailbox_rec.to_cont_id)
  LET p_email_rec.to_cont_lname = get_contact_lname(p_mailbox_rec.to_cont_id)


  LET p_email_rec.recv_date =  p_mailbox_rec.recv_date
  LET p_email_rec.read_flag =  p_mailbox_rec.read_flag
  LET p_email_rec.urgent_flag =  p_mailbox_rec.urgent_flag
  LET p_email_rec.mail_status =  p_mailbox_rec.mail_status


  RETURN p_email_rec.*
END FUNCTION



#################################################
# FUNCTION email_set_read(p_email_id)
#
# Set the read_flag to true
#
# RETURN NONE
#################################################
FUNCTION email_set_read(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  SELECT mailbox.activity_id
    INTO l_activity_id
    FROM mailbox
    WHERE mailbox.mail_id = p_email_id

  #mark email as read
  UPDATE mailbox 
    SET mailbox.read_flag = 1
    WHERE mailbox.mail_id = p_email_id


END FUNCTION


#################################################
# FUNCTION email_set_unread(p_email_id)
#
# Set the read_flag to false
#
# RETURN NONE
#################################################
FUNCTION email_set_unread(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  SELECT mailbox.activity_id
    INTO l_activity_id
    FROM mailbox
    WHERE mailbox.mail_id = p_email_id

  #mark email as read
  UPDATE mailbox 
    SET mailbox.read_flag = 0
    WHERE mailbox.mail_id = p_email_id


END FUNCTION


#################################################
# FUNCTION email_close(p_email_id)
#
# Closes the e-mail by setting a closed date (today)
#
# RETURN NONE
#################################################
FUNCTION email_close(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  SELECT mailbox.activity_id
    INTO l_activity_id
    FROM mailbox
    WHERE mailbox.mail_id = p_email_id


  #mark email as read
  UPDATE mailbox 
    SET mailbox.mail_status = 1
    #SET activity.close_date = TODAY
    WHERE mailbox.mail_id = p_email_id


  CALL activity_close(l_activity_id)


END FUNCTION


#################################################
# FUNCTION email_open(p_email_id)
#################################################
FUNCTION email_open(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  SELECT mailbox.activity_id
    INTO l_activity_id
    FROM mailbox
    WHERE mailbox.mail_id = p_email_id


  #mark email as read
  UPDATE mailbox 
    SET mailbox.mail_status = 0
    WHERE mailbox.mail_id = p_email_id

  CALL activity_open(l_activity_id)


END FUNCTION

#################################################
# FUNCTION set_mail_status_to_sent(p_email_id)
#
# Changes a queued email to sent
#
# RETURN NONE
#################################################
FUNCTION set_mail_status_to_sent(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  #mark email as read
  UPDATE mailbox 
    SET mailbox.mail_status = 1
    WHERE mailbox.mail_id = p_email_id

END FUNCTION


#################################################
# FUNCTION set_mail_status_to_queued(p_email_id)
#
# Changes a sent email to queued
#
# RETURN NONE
#################################################
FUNCTION set_mail_status_to_queued(p_email_id)
  DEFINE
    p_email_id   LIKE mailbox.mail_id,
    rec          LIKE contact.cont_email,
    l_activity_id LIKE activity.activity_id


  #mark email as read
  UPDATE mailbox 
    SET mailbox.mail_status = 0
    WHERE mailbox.mail_id = p_email_id

END FUNCTION


