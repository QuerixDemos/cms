##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# DDE tools
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
# FUNCTION                                           DESCRIPTION                                                 RETURN
#                                                    starts the associated application
# exec_error_check(error_message)                    Check execution for errors                                  dde_error_id
# dde_transfer_excel_cell(r,c,value)                 Transfer a value to a cell                                  NONE
# dde_excel_format_data()                            Select a cell and apply formatting                          NONE
# process_dde_cfg_import(filename)                  Process dde configuration file                              ret
# dde_transfer_bookmark(bookmark,value)              Transfer a value to a bookmark in word                      NONE
# init_dde(p_app_name,p_filename,timeout,p_debug)   Initialise the dde connection with the application name for qxt_dde.success
#                                                    the dde (excel or winword) and document
# establish_dde_connection(p_document)               Connect to DDE client                                       qxt_dde.success
#                                                   (argument can be SYSTEM or document name)
# dde_execute(command)                               To execute any dde command                                  qxt_dde.success
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


############################################################################################
# Configuration / Init Functions
############################################################################################

###########################################################
# FUNCTION process_dde_cfg_import(filename)
#
# Import/Process cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_dde_cfg_import(p_cfg_filename)

DEFINE 
  p_cfg_filename  VARCHAR(150),
  ret SMALLINT,
  local_debug SMALLINT


  LET local_debug = FALSE  --0=off 1=on

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_dde_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    RETURN NULL
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[DDE]
  #dde section
  LET qxt_settings.dde_timeout                   = configGetINT(ret, "[DDE]", "dde_timeout")
  LET qxt_settings.dde_font_excel_filename       = configGet(ret, "[DDE]", "dde_font_excel_filename")



  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_dde_cfg_filename = ", p_cfg_filename CLIPPED, "###############################"

    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [DDE] Section               x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    
    #dde section
    DISPLAY "configInit() - qxt_settings.dde_timeout =",             qxt_settings.dde_timeout 
    DISPLAY "configInit() - qxt_settings.dde_font_excel_filename =", qxt_settings.dde_font_excel_filename 

  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_dde_cfg_export(filename)
#
# Export dde cfg data
#
# RETURN ret
###########################################################
FUNCTION process_dde_cfg_export(p_cfg_filename)
  DEFINE 
    ret SMALLINT,
    p_cfg_filename VARCHAR(100)

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_dde_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_cfg_filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[DDE]
  #write to dde section
  CALL configWrite(ret, "[DDE]",              "dde_timeout", qxt_settings.dde_timeout )
  CALL configWrite(ret, "[DDE]",              "dde_font_excel_filename", qxt_settings.dde_font_excel_filename )

END FUNCTION



#########################################################################################################
# Data Access Functions
#########################################################################################################

############################################################
# FUNCTION get_dde_cfg_filename()
#
# Get dde cfg file name
#
# RETURN qxt_settings.dde_cfg_filename
############################################################
FUNCTION get_dde_cfg_filename()
  RETURN qxt_settings.dde_cfg_filename
END FUNCTION


############################################################
# FUNCTION get_dde_cfg_filename()
#
# Get dde cfg file name
#
# RETURN qxt_settings.dde_cfg_filename
############################################################
FUNCTION set_dde_cfg_filename(p_filename)
  DEFINE
    p_filename VARCHAR(150)

  LET qxt_settings.dde_cfg_filename = p_filename

END FUNCTION


############################################################
# FUNCTION get_dde_font_excel_unl_filename()
#
# Get dde cfg file name
#
# RETURN qxt_settings.dde_cfg_filename
############################################################
FUNCTION get_dde_font_excel_unl_filename()
  RETURN qxt_settings.dde_font_excel_filename
END FUNCTION


############################################################
# FUNCTION set_dde_font_excel_unl_filename(p_filename)
#
# Get dde cfg file name
#
# RETURN qxt_settings.dde_cfg_filename
############################################################
FUNCTION set_dde_font_excel_unl_filename(p_filename)
  DEFINE
    p_filename VARCHAR(150)

  LET qxt_settings.dde_font_excel_filename = p_filename

END FUNCTION

##########################################################
# FUNCTION get_dde_timeout()
#
# Get dde timeout
#
# RETURN qxt_settings.dde_timeout
##########################################################
FUNCTION get_dde_timeout()
  RETURN qxt_settings.dde_timeout
END FUNCTION


##########################################################
# FUNCTION set_dde_timeout(dde_timeout)
#
# Set dde timeout
#
# RETURN NONE
###########################################################
FUNCTION set_dde_timeout(dde_timeout)
  DEFINE
    dde_timeout SMALLINT

  LET qxt_settings.dde_timeout = dde_timeout
END FUNCTION




