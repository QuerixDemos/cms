##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#################################################################################################################
# Main menu functions
#################################################################################################################
#
# FUNCTION:                        DESCRIPTION:                                        RETURN:
# load_menu(p_menu_name)           Loads the menu text                                 NONE
# load_main_menu()                 Loads the MAIN menu text                            NONE
################################################################################################################

########################################################################
# GLOBALS
########################################################################
GLOBALS "globals.4gl"


########################################################################
# FUNCTION load_menu(p_menu_name)
#
# Loads the menu text
#
# RETURN NONE
########################################################################
FUNCTION load_menu(p_menu_name)
  DEFINE 
    p_menu_name LIKE menus.menu_name,
    i,j         INTEGER,
    l_language   SMALLINT,
    local_Debug SMALLINT

  LET local_debug = FALSE
  LET l_language = get_language()

  IF local_debug THEN
    DISPLAY "load_menu() - funtion entry"
    DISPLAY "load_menu() - l_language = ", l_language
  END IF

  DECLARE c_menu CURSOR FOR
    SELECT string1.string_data,
           string2.string_data,
           menu_options.option_keypress,
           menu_options.option_id 
      FROM menu_options, menus , qxt_string_app string1 , qxt_string_app string2
     WHERE menus.menu_name = p_menu_name
       AND menu_options.menu_id = menus.menu_id
       AND menu_options.option_name_id = string1.string_id
       AND menu_options.option_comment_id = string2.string_id
       AND string1.language_id = l_language
       AND string2.language_id = l_language
  ORDER BY menu_options.option_id 

  IF local_debug THEN
    DISPLAY "load_menu() - AFTER QUERY"
  END IF

  LET i = 1
  FOREACH c_menu INTO gl_std_menu[i].*
    IF local_debug THEN
      DISPLAY "load_menu() - i=", i
    END IF

    #IF i = 20 THEN
    #  EXIT FOREACH
    #END IF
    LET i = i + 1
  END FOREACH

		IF i > 0 THEN
			CALL gl_std_menu.resize(i)  --correct the last element of the dynamic array
		END IF 
		
  IF local_debug THEN
    DISPLAY "load_menu() - END OF FUNCTION"
  END IF

END FUNCTION


########################################################################
# FUNCTION load_main_menu()
#
# Loads the MAIN menu text
#
# RETURN NONE
########################################################################
FUNCTION load_main_menu()
  DEFINE 
    i           INTEGER,
    local_debug SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "load_main_menu() - with argument \"main\""
  END IF

  CALL load_menu("main")
  FOR i = 1 TO 20
    IF local_debug THEN
      DISPLAY "load_main_menu() - gl_std_menu[i].option_name =", gl_std_menu[i].option_name
      DISPLAY "load_main_menu() - gl_std_menu[i].option_comment =", gl_std_menu[i].option_comment
      DISPLAY "load_main_menu() - gl_std_menu[i].option_keypress =", gl_std_menu[i].option_keypress
    END IF
    LET main_menu[i].* = gl_std_menu[i].*
  END FOR
END FUNCTION





