##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION title_grid_header_scroll()
#
# Pupulate grid header
#
# RETURN NONE
####################################################
FUNCTION title_grid_header_scroll()
  CALL fgl_grid_header("sc_title_scroll","title",get_str(1281),"left","F13") --Title

END FUNCTION


#######################################################
# FUNCTION populate_title_form_labels_g()
#
# Populate title form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_title_form_labels_g()
  DISPLAY get_str(1280) TO lbTitle
  DISPLAY get_str(1281) TO dl_f1
  #DISPLAY get_str(1282) TO dl_f2
  #DISPLAY get_str(1283) TO dl_f3
  #DISPLAY get_str(1284) TO dl_f4
  #DISPLAY get_str(1285) TO dl_f5
  #DISPLAY get_str(1286) TO dl_f6
  #DISPLAY get_str(1287) TO dl_f7
  #DISPLAY get_str(1288) TO dl_f8
  DISPLAY get_str(1289) TO lbInfo1

  CALL fgl_settitle(get_str(1280))

  DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_title_list_form_labels_g()
#
# Populate title list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_title_list_form_labels_g()
  DISPLAY get_str(1290) TO lbTitle
  #DISPLAY get_str(1291) TO dl_f1
  #DISPLAY get_str(1292) TO dl_f2
  #DISPLAY get_str(1293) TO dl_f3
  #DISPLAY get_str(1294) TO dl_f4
  #DISPLAY get_str(1295) TO dl_f5
  #DISPLAY get_str(1296) TO dl_f6
  #DISPLAY get_str(1297) TO dl_f7
  #DISPLAY get_str(1298) TO dl_f8
  DISPLAY get_str(1299) TO lbInfo1


  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1290))

  CALL title_grid_header_scroll()

END FUNCTION

