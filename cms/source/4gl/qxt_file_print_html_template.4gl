##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_file_print_html_template_globals.4gl"


###########################################################
# FUNCTION get_print_html_template_lib_info()
#
# Simply returns libray type /  name
#
# RETURN NONE
###########################################################
FUNCTION get_print_html_template_lib_info()
  RETURN "FILE - qxt_file_print_html_template"
END FUNCTION 


#####################################################################################
# Init/Config/Import Functions
#####################################################################################

###########################################################
# FUNCTION process_print_html_template_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_print_html_template_init(p_filename)
  DEFINE 
    p_filename VARCHAR(100),
    local_Debug  SMALLINT

  LET local_debug = FALSE

  IF p_filename IS NULL THEN
    LET p_filename = get_print_html_template_unl_filename()
  END IF

  IF local_Debug THEN
    DISPLAY "process_print_html_template_init() - p_filename=", p_filename
  END IF

  CALL qxt_print_html_template_import_file(get_unl_path(p_filename))
  #This function must be compatible with the file toolbar library
END FUNCTION

###########################################################
# FUNCTION qxt_print_html_template_import_file(p_filename)
#
# Import the print html template file list from a file
#
# RETURN NONE
###########################################################
FUNCTION qxt_print_html_template_import_file(p_filename)
  DEFINE 
    p_filename              VARCHAR(150),
    l_tmp_qxt_print_html_template_rec OF t_qxt_print_html_template_rec,
    sql_stmt                CHAR(1000),
    local_debug             SMALLINT,
    err_msg                 VARCHAR(200),
    id                       SMALLINT

  LET local_debug = FALSE


  IF local_debug THEN
    DISPLAY "qxt_print_html_template_import_file() - Open file p_filename=",p_filename 
  END IF

  #Check if file exists
  CALL validate_file_server_side_exists_advanced(p_filename,"f",TRUE)

  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_print_html_template_import_file()\nCannot open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_template_import_file()", err_msg, "error")
    RETURN
  END IF

  IF local_debug THEN
    DISPLAY "qxt_print_html_template_import_file() - Set Delimiter"
  END IF


  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_print_html_template_import_file()\nCannot set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_template_import_file()", err_msg, "error")
    RETURN
  END IF

  LET id = 1

  WHILE fgl_channel_read("stream",qxt_print_html_template_rec_arr[id].*) 
    #LET id = l_tmp_qxt_print_html_template_rec.id
    #LET qxt_print_html_template_rec_arr[id].id = l_tmp_qxt_print_html_template_rec.id
    #LET qxt_print_html_template_rec_arr[id].language_id = l_tmp_qxt_print_html_template_rec.language_id
    #LET qxt_print_html_template_rec_arr[id].filename = l_tmp_qxt_print_html_template_rec.filename

    IF local_debug THEN
      DISPLAY "qxt_print_html_template_import_file() - qxt_print_html_template_rec_arr[", trim(id), "].id=",           qxt_print_html_template_rec_arr[id].id 
      DISPLAY "qxt_print_html_template_import_file() - qxt_print_html_template_rec_arr[", trim(id), "].language_id=",  qxt_print_html_template_rec_arr[id].language_id 
      DISPLAY "qxt_print_html_template_import_file() - qxt_print_html_template_rec_arr[", trim(id), "].filename=",     qxt_print_html_template_rec_arr[id].filename 
    END IF 

  LET id = id + 1
  END WHILE

  #Close the file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_print_html_template_import_file()\nCan not close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_template_import_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION



############################################################################################
# Data Access Functions
############################################################################################


##############################################################
# FUNCTION get_print_html_template_filename(p_id,p_language_id)
#
# Returns the corresponding html print template file name
#
# RETURN qxt_settings.print_html_template[id]
##############################################################

FUNCTION get_print_html_template_filename(p_id,p_language_id)
  DEFINE
    p_id          SMALLINT,
    p_language_id SMALLINT,
    local_debug   SMALLINT,
    ret           VARCHAR(200),
    err_msg       VARCHAR(200),
    i             SMALLINT

  LET local_debug = FALSE
  LET ret = NULL

    IF local_debug THEN
      DISPLAY "get_print_html_template_filename() - p_id=", p_id
      DISPLAY "get_print_html_template_filename() - p_language_id=", p_language_id
    END IF

  FOR i = 1 TO sizeof(qxt_print_html_template_rec_arr)
    IF local_debug THEN
      DISPLAY "get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].id=",          qxt_print_html_template_rec_arr[i].id
      DISPLAY "get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].language_id=", qxt_print_html_template_rec_arr[i].language_id
      DISPLAY "get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].filename=",    qxt_print_html_template_rec_arr[i].filename
    END IF


    IF qxt_print_html_template_rec_arr[i].id = p_id AND
       qxt_print_html_template_rec_arr[i].language_id = p_language_id THEN
      IF local_debug THEN
        DISPLAY "*hit* get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].id=",          qxt_print_html_template_rec_arr[i].id
        DISPLAY "*hit* get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].language_id=", qxt_print_html_template_rec_arr[i].language_id
        DISPLAY "*hit* get_print_html_template_filename() - qxt_print_html_template_rec_arr[i].filename=",    qxt_print_html_template_rec_arr[i].filename
      END IF

      LET ret = qxt_print_html_template_rec_arr[i].filename
      EXIT FOR
    END IF
  END FOR

  IF ret IS NULL THEN
    let err_msg = "Html print template id=", trim(p_id), " and language= ", trim(p_language_id), " could not be matched"
    CALL fgl_winmessage("Template file id could not be matched - get_print_html_template_filename()",err_msg, "error")
  END IF

  IF local_debug THEN
    DISPLAY "get_print_html_template_filename() - p_id= ", p_id
    DISPLAY "get_print_html_template_filename() - p_language_id= ", p_language_id
    DISPLAY "get_print_html_template_filename() - RETURN ret= ", ret
  END IF

  RETURN ret

END FUNCTION










######################################################
# FUNCTION download_blob_print_template_to_server(p_template_id,p_language_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the App-Server file system
#
# RETURN NONE
######################################################
FUNCTION download_blob_print_template_to_server(p_template_id,p_language_id,p_server_file_path)
  DEFINE 
    p_template_id        SMALLINT,
    p_language_id        SMALLINT,
    local_debug          SMALLINT,
    p_server_file_path   VARCHAR(250),
    l_server_file_path   VARCHAR(250),
    l_server_file_blob   BYTE


  #Retrieve the filename
  LET l_server_file_path = get_print_html_template_filename(p_template_id,p_language_id)

  #There is no need to copy/move the file
  #We simply return it's location
  RETURN get_print_html_template_path( get_language_dir_path(p_language_id,l_server_file_path))

 
{
  LET local_debug = FALSE

  IF p_server_file_path IS NULL THEN
    LET l_server_file_path = get_server_blob_temp_path(get_print_html_template_filename(p_template_id,p_language_id))  
  ELSE
    LET l_server_file_path = p_server_file_path
  END IF

  IF local_debug THEN
    DISPLAY "download_blob_print_template_to_server() - p_template_id=", p_template_id
    DISPLAY "download_blob_print_template_to_server() - p_language_id=", p_language_id
    DISPLAY "download_blob_print_template_to_server() - p_server_file_path=", p_server_file_path
    DISPLAY "download_blob_print_template_to_server() - l_server_file_path=", l_server_file_path
  END IF
  #CALL get_blob_print_template(p_print_template_id) RETURNING l_print_template_file.*


  #LET default_file_name = p_image_name CLIPPED, ".jpg"
  #LET local_file_name = default_file_name


  IF l_server_file_path IS NOT NULL THEN
    LOCATE l_server_file_blob IN FILE l_server_file_path

    SELECT template_data
      INTO l_server_file_blob
      FROM qxt_print_template
      WHERE template_id = p_template_id
        AND language_id   = p_language_id
  

    FREE l_server_file_blob
    #FREE l_print_template_file.print_template_blob

    RETURN l_server_file_path
  ELSE
    CALL fgl_winmessage("Error","download_blob_print_template_to_server() - l_server_file_path IS NULL","error")
    RETURN NULL
  END IF
}
END FUNCTION













