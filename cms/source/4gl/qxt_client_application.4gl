##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# client application tools
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
# FUNCTION                                           DESCRIPTION                                                 RETURN
# run_client_application_with_server_document(doc_name)    Transfers the document from the server to the client and    doc_name
#                                                          starts the associated application
#
#########################################################################################################



###############################################################
# FUNCTION run_client_application_with_client_document(p_doc_name)
#
# starts the document associated application (guess Windows only)
#
# RETURN doc_name
###############################################################
FUNCTION run_client_application_with_client_document(p_doc_name)
  DEFINE 
    p_doc_name   VARCHAR(200),  
    #doc_name     VARCHAR(200), --doc_name is the document name without  any path information
    local_debug  SMALLINT,
    err_msg      VARCHAR(200),
    success      SMALLINT

  LET local_debug = FALSE

  #check if file exists on client machine
  IF NOT fgl_getproperty("gui","system.file.exists",p_doc_name) THEN
    #The specified document does not exist on the server
    LET err_msg = get_str_tool(672) CLIPPED, "\n", get_str_tool(675) CLIPPED, p_doc_name
    CALL fgl_winmessage(get_str_tool(672),err_msg,"error")
  END IF

  IF local_debug THEN
    DISPLAY "run_client_application_with_client_document() - p_doc_name=", p_doc_name
  END IF

  LET success = WinShellExec(p_doc_name)  -- 1 = error 0= OK

  #Makes no sense as Windows always returns 0 for WinShellExec operations
 
  #IF success THEN
  #  LET err_msg = get_str_tool(671) CLIPPED, " ", p_doc_name CLIPPED, "\nWinShellExec Return = ", success CLIPPED
  #  CALL fgl_winmessage("Error in run_local_application_with_server_doc()",err_msg, "error")
  #END IF

  #IF local_debug THEN
  #  DISPLAY "run_local_application_with_server_doc() - success=", success
  #END IF

  #We change this return value - I like 0/Null for false and 1=True success

  IF success = 0 THEN
    LET success = TRUE
  ELSE
    LET success = FALSE
  ENd IF

  RETURN success


END FUNCTION



###############################################################
# FUNCTION run_local_application_with_server_doc(doc_name)
#
# Transfers the document from the server to the client and starts the associated application
#
# RETURN doc_name
###############################################################
FUNCTION run_client_application_with_server_document(p_doc_name)
  DEFINE 
    p_doc_name   VARCHAR(100),  
    doc_name     VARCHAR(200), --doc_name is the document name without  any path information
    local_debug  SMALLINT,
    err_msg      VARCHAR(200),
    success      SMALLINT

  LET local_debug = FALSE

  IF NOT fgl_test("e",p_doc_name) THEN
    #The specified document does not exist on the server
    LET err_msg = get_str_tool(673) CLIPPED, "\n", get_str_tool(673) CLIPPED, p_doc_name
    #File does not exist - 
    CALL fgl_winmessage(get_str_tool(672),err_msg,"error")
  END IF

  LET doc_name = fgl_download(p_doc_name, get_client_temp_path(p_doc_name))

  IF local_debug THEN
    DISPLAY "run_local_application_with_server_doc() - p_doc_name=", p_doc_name
    DISPLAY "run_local_application_with_server_doc() - doc_name=", doc_name
  END IF

  LET success = run_client_application_with_client_document(doc_name)  -- 1 = error 0= OK

  #IF success THEN
  #  LET err_msg = get_str_tool(671) CLIPPED, " ", p_doc_name CLIPPED
  #  CALL fgl_winmessage("Error in run_local_application_with_server_doc()",err_msg, "error")
  #END IF

  IF local_debug THEN
    DISPLAY "run_local_application_with_server_doc() - success=", success
  END IF

  RETURN doc_name

END FUNCTION

