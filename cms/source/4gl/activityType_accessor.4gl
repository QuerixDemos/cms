##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


##################################################
# FUNCTION get_activity_type_name(p_activity_type_id)
#
# Get the activity type name from the id                     
# RETURN l_activity_type_name
##################################################
FUNCTION get_activity_type_name(p_activity_type_id)
  DEFINE 
    p_activity_type_id   LIKE activity_type.type_id,
    l_activity_type_name LIKE activity_type.atype_name

  SELECT activity_type.atype_name
    INTO l_activity_type_name
    FROM activity_type
    WHERE activity_type.type_id = p_activity_type_id

  RETURN l_activity_type_name
END FUNCTION



##################################################
# FUNCTION get_activity_type_id(p_activity_type_name)
#
# Get the activity type id from the activity type name                     
# RETURN l_activity_type_id
##################################################
FUNCTION get_activity_type_id(p_activity_type_name)
  DEFINE 
    l_activity_type_id    LIKE activity_type.type_id,
    p_activity_type_name  LIKE activity_type.atype_name,
    local_debug           SMALLINT

  LET local_debug = FALSE

  SELECT activity_type.type_id
    INTO l_activity_type_id
    FROM activity_type
    WHERE activity_type.atype_name = p_activity_type_name

  IF local_debug THEN
    DISPLAY "get_activity_type_id() - p_activity_type_name=", p_activity_type_name
    DISPLAY "get_activity_type_id() - l_activity_type_id=", l_activity_type_id
  END IF


  RETURN l_activity_type_id


END FUNCTION





######################################################
# FUNCTION get_activity_type_rec(p_activity_type_id)
#
# Get an activity_type record from id
#
# RETURN l_activity_type_rec.*
######################################################
FUNCTION get_activity_type_rec(p_activity_type_id)
  DEFINE 
    p_activity_type_id  LIKE activity_type.type_id,
    l_activity_type_rec OF t_act_type_rec

  SELECT *
    INTO l_activity_type_rec.*
    FROM activity_type
    WHERE activity_type.type_id = p_activity_type_id

  RETURN l_activity_type_rec.*

END FUNCTION



###################################################################################
# FUNCTION activity_type_combo_list(cb_field_name)
###################################################################################
FUNCTION activity_type_combo_list(cb_field_name)
  DEFINE l_activity_type_arr DYNAMIC ARRAY OF 
    RECORD
     atype_name LIKE activity_type.atype_name  
    END RECORD,
    row_count INTEGER,
    current_row INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    abort_flag SMALLINT
 
  LET abort_flag = FALSE

  DECLARE c_activity_type_scroll2 CURSOR FOR 
    SELECT activity_type.atype_name 
      FROM activity_type

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_activity_type_scroll2 INTO l_activity_type_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_activity_type_arr[row_count].atype_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

		If row_count > 0 THEN
			CALL l_activity_type_arr.resize(row_count)   --remove the last item which has no data
		END IF 

END FUNCTION

