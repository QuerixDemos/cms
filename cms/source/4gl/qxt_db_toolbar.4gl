##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Graphical toolbar manager
#
# Created:
# 10.10.06 HH - V3 - This set of functions handles the import of toolbar configurations and
# allow to display and remove complete toolbars with a single function call
#
# It is written in a re-usable way. Feel free to integrate it into your 4gl application
# to have fully dynamic and customizable toolbars.
#
#
# FUNCTION                                        DESCRIPTION                                            RETURN
# process_toolbar_init(file_name)                 Import toolbar config file -                           NONE
#                                                 each menu name has one or more menu items 
# publish_toolbar(action_time)                    Publish a group of toolbar menu icons                  NONE
# draw_tb_icon()                                  Draw a single menu item                                NONE
# hide_dialog_navigation_toolbar(choice)          Hide all auto-generated navigation buttons             NONE
# manage_toolbar_list()                           Display a list of all toolbar menu items -             NONE
#                                                 items can also be edited in detail
# copy_t_toolbar_to_t_toolbar_short(tb_rec)       copy t_toolbar data rec to t_toolbar_short             tb_short_rec.* 
# icon_detail_edit(id)                            Edit icon configuration (for a single menu item)       NONE
# icon_browser()                                  Displays the list of all icons (using the lazy way...) icon_list[i]
#
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_toolbar_globals.4gl"

###########################################################
# FUNCTION get_toolbar_lib_info()
#
# Simply returns libray name
#
# RETURN "FILE - qxt_file_toolbar"
###########################################################
FUNCTION get_toolbar_lib_info()
  RETURN "DB - qxt_file_toolbar"
END FUNCTION 


#####################################################################################
# Init/Config Functions
#####################################################################################

###########################################################
# FUNCTION process_toolbar_init(file_name)
#
# Do Nothing - just stay compatible with file library
#
# RETURN NONE
###########################################################
FUNCTION process_toolbar_init(file_name1,file_name2)
  DEFINE 
    file_name1,file_name2 VARCHAR(100)

   #This function must be compatible with the file toolbar library

   #Optionally, we could verify the cache for the existence of all icons
   #CALL validate_client_icon_file_cache(get_current_application_id())

END FUNCTION




#####################################################################################
# Toolbar Draw/Record Handling functions
#
# Note: draw_tb_icon(p_tbi_rec.*) is located in the main qxt.lib
#####################################################################################

###########################################################
# FUNCTION publish_toolbar(p_tb_name,p_instance)
#
# Publish a group of toolbar menu icons
#
# RETURN NONE
###########################################################
FUNCTION publish_toolbar(p_tb_name,p_instance)
  DEFINE
    p_tb_name          LIKE qxt_tb.tb_name,
    p_instance         LIKE qxt_tb.tb_instance,
    local_debug        SMALLINT

  LET local_debug = FALSE
    
  IF local_debug THEN
    DISPLAY "publish_toolbar() - p_tb_name=", p_tb_name
    DISPLAY "publish_toolbar() - p_instance=", p_instance
  END IF

  CALL draw_toolbar(get_current_application_id(),p_tb_name,p_instance, get_language())

END FUNCTION



###########################################################
# FUNCTION draw_toolbar(p_application_id,p_tb_name,p_instance, p_language_id)
#
# Publish a group of toolbar menu icons
#
# RETURN NONE
###########################################################
FUNCTION draw_toolbar(p_application_id,p_tb_name,p_instance, p_language_id)
  DEFINE
    p_application_id   LIKE qxt_tb.application_id,
    p_tb_name          LIKE qxt_tb.tb_name,
    p_instance         LIKE qxt_tb.tb_instance,
    p_language_id      LIKE qxt_language.language_id,
    l_toolbar_rec      OF t_qxt_toolbar_rec,
    sql_stmt           STRING, --CHAR(1000),
    p_tbi_rec          OF t_qxt_toolbar_item_rec,
    local_debug        BOOLEAN

  LET local_debug = FALSE


  LET sql_stmt = "SELECT ",
    "qxt_tb.application_id, ",
    "qxt_tb.tb_name, ",
    "qxt_tb.tb_instance, ",
    "qxt_tb.tbi_name, ",
    "qxt_tbi.tbi_position ",
    "FROM qxt_tb , qxt_tbi ",
    "WHERE ",
    "qxt_tb.application_id = ", p_application_id , " ",
    "AND ",
    "tb_name = '", p_tb_name , "' ",
    "AND ",
    "tb_instance = ", p_instance , " ",
    "AND ",
    "qxt_tb.tbi_name = qxt_tbi.tbi_name ",
    "ORDER BY qxt_tbi.tbi_position ASC "

  PREPARE p_tb FROM sql_stmt
  DECLARE c_tb CURSOR FOR p_tb

  FOREACH c_tb INTO l_toolbar_rec.*
    CALL get_toolbar_item_rec(l_toolbar_rec.application_id,
                              l_toolbar_rec.tbi_name,
                              l_toolbar_rec.tb_instance,
                              p_language_id)
      RETURNING p_tbi_rec.*

    IF local_debug THEN
      DISPLAY "draw_toolbar() - l_toolbar_rec.application_id=", l_toolbar_rec.application_id
      DISPLAY "draw_toolbar() - l_toolbar_rec.tbi_name=", l_toolbar_rec.tbi_name
      DISPLAY "draw_toolbar() - l_toolbar_rec.tb_instance=", l_toolbar_rec.tb_instance
      DISPLAY "draw_toolbar() - p_language_id=", p_language_id
    END IF 

    CALL draw_tb_icon(p_tbi_rec.*)
  END FOREACH

END FUNCTION




###########################################################
# FUNCTION get_toolbar_item_rec(p_application_id,p_tbi_name,p_tb_instance,p_language_id)
#
# Get a toolbar icon/item record to draw
#
# RETURN l_toolbar_rec.*
###########################################################
FUNCTION get_toolbar_item_rec(p_application_id,p_tbi_name,p_tb_instance,p_language_id)
  DEFINE 
    #p_tb_rec            OF t_qxt_toolbar_rec,
    p_application_id     LIKE qxt_tb.application_id,
    p_tbi_name           LIKE qxt_tb.tbi_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    p_language_id        LIKE qxt_language.language_id,
    l_toolbar_item_rec   OF t_qxt_toolbar_item_rec,
    local_debug          BOOLEAN,
    l_tooltip_exists       SMALLINT
  
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_toolbar_item_rec() - p_application_id=", p_application_id
    DISPLAY "get_toolbar_item_rec() - p_tbi_name=",       p_tbi_name
    DISPLAY "get_toolbar_item_rec() - p_tb_instance=",    p_tb_instance
    DISPLAY "get_toolbar_item_rec() - p_language_id=",    p_language_id
  END IF

  #Attempt to get toolbar item with choosen language for toolip
  SELECT 
    qxt_tbi.event_type_id,
    qxt_tbi.tbi_event_name,
    qxt_tbi_obj.tbi_obj_action_id,
    qxt_tbi.tbi_scope_id,
    qxt_tbi.tbi_position,
    qxt_tbi.tbi_static_id,
    qxt_tbi_obj.icon_filename,
    qxt_tbi_tooltip.label_data,
    qxt_tbi_tooltip.string_data

  INTO l_toolbar_item_rec

  FROM
    #qxt_tb,
    qxt_tbi,
    qxt_tbi_obj,
    qxt_tbi_tooltip


  WHERE
      qxt_tbi.application_id = p_application_id
    AND
      qxt_tbi.tbi_name = p_tbi_name
    AND
      qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name
    AND
      qxt_tbi_obj.string_id = qxt_tbi_tooltip.string_id
    AND
      qxt_tbi_tooltip.language_id = p_language_id
    #AND
    #  qxt_tb.tb_instance = p_tb_instance
    #AND
    #  qxt_tb.tb_name = p_tb_name


  IF l_toolbar_item_rec.string_data IS NULL THEN
    LET p_language_id = get_language_default()
  SELECT 
    qxt_tbi.event_type_id,
    qxt_tbi.tbi_event_name,
    qxt_tbi_obj.tbi_obj_action_id,
    qxt_tbi.tbi_scope_id,
    qxt_tbi.tbi_position,
    qxt_tbi.tbi_static_id,
    qxt_tbi_obj.icon_filename,
    qxt_tbi_tooltip.label_data,
    qxt_tbi_tooltip.string_data
  INTO l_toolbar_item_rec

  FROM
    #qxt_tb,
    qxt_tbi,
    qxt_tbi_obj,
    qxt_tbi_tooltip


  WHERE
      qxt_tbi.application_id = p_application_id
    AND
      qxt_tbi.tbi_name = p_tbi_name
    AND
      qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name
    AND
      qxt_tbi_obj.string_id = qxt_tbi_tooltip.string_id
    AND
      qxt_tbi_tooltip.language_id = p_language_id

  END IF

      
  RETURN l_toolbar_item_rec.*

END FUNCTION




##################################################################
# FUNCTION validate_client_icon_file_existence(p_application_id,p_location)
#
# Validate if the toolbar icons are located in the client cache or on the server
# p_location 0 = server 1=client
#
# RETURN NONE
##################################################################
FUNCTION validate_client_icon_file_existence(p_application_id,p_location)
  DEFINE
    p_application_id    LIKE qxt_application.application_id,
    p_location          SMALLINT,  --0 = server 1=client cache
    l_icon_filename     LIKE qxt_tbi_obj.icon_filename,
    sql_stmt            CHAR(1000),
    err_msg             VARCHAR(200),
    local_debug         SMALLINT,
    validation_error    SMALLINT


  LET validation_error = FALSE

  IF p_location IS NULL THEN
    LET p_location = 0  --Server is default
  END IF

  LET sql_stmt= "SELECT ",
                  "qxt_tbi_obj.icon_filename ",

                "FROM qxt_tb,qxt_tbi,qxt_tbi_obj ",
                "WHERE ",
                  "qxt_tb.application_id = ", trim(p_application_id), " ",
                "AND ",
                  "qxt_tb.tbi_name = qxt_tbi.tbi_name ",
                "AND ",
                  "qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name "

  PREPARE p_icon FROM sql_stmt
  DECLARE c_icon CURSOR FOR p_icon

  FOREACH c_icon INTO l_icon_filename
    #DISPLAY "Icon =", l_icon_filename
    CASE p_location
      WHEN 0 --Server check
        IF NOT validate_file_server_side_exists_advanced(get_tb_icon_path(l_icon_filename),"f",TRUE) THEN
          LET validation_error = TRUE
          IF yes_no("Validate icon existence on Application Server","Do you want to abort ?") THEN
            EXIT FOREACH
          END IF
        END IF
      WHEN 1 --Client cache check
        IF NOT validate_file_client_side_cache_exists(get_tb_icon_path(l_icon_filename), NULL,TRUE) THEN
          LET validation_error = TRUE
          IF yes_no("Validate icon existence on Client Side Cache","Do you want to abort ?") THEN
            EXIT FOREACH
          END IF
        END IF
      OTHERWISE
        LET err_msg = "Invalid argument in  validate_client_icon_file_cache()\nInvalid location was specified 0=Server 1=Client\np_location = ", p_location
        CALL fgl_winmessage("Invalid argument in  validate_client_icon_file_cache()",err_msg, "error")
    END CASE

    IF local_debug THEN
      DISPLAY "draw_toolbar() - l_icon_filename=", l_icon_filename
    END IF 

  END FOREACH


  RETURN validation_error

END FUNCTION




























