##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


####################################################################################################################
#
# Print HTML functions
#
####################################################################################################################

#########################################################################################################
# FUNCTION print_contact_html(p_company_id)
#
# Prints company details using a dynamically created html page.
#
# RETURN NONE
#########################################################################################################
FUNCTION print_contact_html(p_contact_id)
  DEFINE 
    p_contact_id  LIKE contact.cont_id,
    arg_str VARCHAR(2000),
    server_template_path_file VARCHAR(200),
    server_print_file         VARCHAR(200),
    client_path_file VARCHAR(300),

    l_contact_rec RECORD LIKE contact.*,
    local_debug SMALLINT,
    previous_help_file_id SMALLINT

  LET local_debug = FALSE
  LET previous_help_file_id = get_current_classic_help()

  IF NOT contact_id_count(p_contact_id) THEN
    #CALL fgl_winmessage(get_str(320)"Error printing","You must save the contact record to print it!","error")
    CALL fgl_winmessage(get_str(320),get_str(321),"error")
    RETURN
  END IF

  CALL web_get_contact_rec(p_contact_id) RETURNING l_contact_rec.*


  LET arg_str = append_arg_str(arg_str, "hm_s01", get_str(330))  --Contact Id:
  LET arg_str = append_arg_str(arg_str, "hm_s02", get_str(331))  --"Title:"
  LET arg_str = append_arg_str(arg_str, "hm_s03", get_str(332))  --"Name:")
  LET arg_str = append_arg_str(arg_str, "hm_s04", get_str(333))  --"F-Name:")
  LET arg_str = append_arg_str(arg_str, "hm_s05", get_str(334))  --"L-Name:")
  LET arg_str = append_arg_str(arg_str, "hm_s06", get_str(335))  --"Address:")
  LET arg_str = append_arg_str(arg_str, "hm_s07", get_str(336))  --"")
  LET arg_str = append_arg_str(arg_str, "hm_s08", get_str(337))  --"")
  LET arg_str = append_arg_str(arg_str, "hm_s09", get_str(338))  --"City:") 
  LET arg_str = append_arg_str(arg_str, "hm_s10", get_str(339))  -- "Zone:")  
  LET arg_str = append_arg_str(arg_str, "hm_s11", get_str(340))  --"Zip:")  
  LET arg_str = append_arg_str(arg_str, "hm_s12", get_str(341))  --"Country:")  
  LET arg_str = append_arg_str(arg_str, "hm_s13", get_str(342))  --"Phone:")  
  LET arg_str = append_arg_str(arg_str, "hm_s14", get_str(343))  --"Fax:")  
  LET arg_str = append_arg_str(arg_str, "hm_s15", get_str(344))  --"Mobile:")  
  LET arg_str = append_arg_str(arg_str, "hm_s16", get_str(345))  --"Email:")  
  LET arg_str = append_arg_str(arg_str, "hm_s17", get_str(346))  --"Department:") 
  LET arg_str = append_arg_str(arg_str, "hm_s18", get_str(347))  --"Company:")  
  LET arg_str = append_arg_str(arg_str, "hm_s19", get_str(348))  --"Position:")  
  #LET arg_str = append_arg_str(arg_str, "hm_s20", "Website:")  -- not used
  LET arg_str = append_arg_str(arg_str, "hm_s21",l_contact_rec.cont_id)
  LET arg_str = append_arg_str(arg_str, "hm_s22",l_contact_rec.cont_title)
  LET arg_str = append_arg_str(arg_str, "hm_s23",l_contact_rec.cont_name)
  LET arg_str = append_arg_str(arg_str, "hm_s24",l_contact_rec.cont_fname)
  LET arg_str = append_arg_str(arg_str, "hm_s25",l_contact_rec.cont_lname)
  LET arg_str = append_arg_str(arg_str, "hm_s26",l_contact_rec.cont_addr1)
  LET arg_str = append_arg_str(arg_str, "hm_s27",l_contact_rec.cont_addr2)
  LET arg_str = append_arg_str(arg_str, "hm_s28",l_contact_rec.cont_addr3)
  LET arg_str = append_arg_str(arg_str, "hm_s29",l_contact_rec.cont_city) 
  LET arg_str = append_arg_str(arg_str, "hm_s30",l_contact_rec.cont_zone) 
  LET arg_str = append_arg_str(arg_str, "hm_s31",l_contact_rec.cont_zip) 
  LET arg_str = append_arg_str(arg_str, "hm_s32",l_contact_rec.cont_country) 
  LET arg_str = append_arg_str(arg_str, "hm_s33",l_contact_rec.cont_phone) 
  LET arg_str = append_arg_str(arg_str, "hm_s34",l_contact_rec.cont_fax) 
  LET arg_str = append_arg_str(arg_str, "hm_s35",l_contact_rec.cont_mobile) 
  LET arg_str = append_arg_str(arg_str, "hm_s36",l_contact_rec.cont_email) 
  LET arg_str = append_arg_str(arg_str, "hm_s37",l_contact_rec.cont_dept) 
  LET arg_str = append_arg_str(arg_str, "hm_s38",get_company_name(l_contact_rec.cont_org)) 
  LET arg_str = append_arg_str(arg_str, "hm_s39",l_contact_rec.cont_position) 
  #LET arg_str = append_arg_str(arg_str, "hm_s40",l_contact_rec.cont_mobile) 


  # download images and get path information
  LET arg_str = append_arg_str(arg_str, "hm_i01",download_contact_image(l_contact_rec.cont_id, get_image_path(get_print_html_image_filename(2)),FALSE))
  #CALL download_contact_image(l_contact_rec.cont_id, get_image_path(get_print_html_image_filename(2)),FALSE)

  #FUNCTION download_contact_image(p_contact_id, p_image_name,p_file_dialog)

  #long text / memo
  LET arg_str = append_arg_str(arg_str, "hm_l01",l_contact_rec.cont_notes)
  

  
  IF local_debug THEN
    DISPLAY "print_company_html() - get_print_html_template_filename(1,get_language()) = ", get_print_html_template_filename(1,get_language())
    DISPLAY "print_company_html() - get_html_path(get_print_html_template_filename(1,get_language())) = ", get_html_path(get_print_html_template_filename(1,get_language()))
    DISPLAY "print_company_html() - get_print_html_output() = ", get_print_html_output()
    DISPLAY "print_company_html() - get_html_path(get_print_html_output()) = ", get_html_path(get_print_html_output())
    DISPLAY "print_company_html() - get_image_path(get_print_html_image_filename(2)) = ", get_image_path(get_print_html_image_filename(2))
    DISPLAY "print_company_html() - arg_str[1,80] = ", arg_str[1,80]
    DISPLAY "print_company_html() - arg_str[81,160] = ", arg_str[81,160]
    DISPLAY "print_company_html() - arg_str[161,240] = ", arg_str[161,240]
    DISPLAY "print_company_html() - arg_str[241,320] = ", arg_str[241,320]
    DISPLAY "print_company_html() - arg_str[321,400] = ", arg_str[321,400]
    DISPLAY "print_company_html() - arg_str[401,480] = ", arg_str[401,480]
    DISPLAY "print_company_html() - arg_str[481,560] = ", arg_str[481,560]
    DISPLAY "print_company_html() - arg_str[561,640] = ", arg_str[561,640]
    DISPLAY "print_company_html() - arg_str[641,720] = ", arg_str[641,720]
  END IF



  #Download template from DB blob to server (or file if you use the qxt_file_template library)
  LET server_template_path_file = download_blob_print_template_to_server(2,get_language(),NULL)

  #IF local_debug THEN
  #  DISPLAY "2 - print_contact_html() - server_template_path_file= ", server_template_path_file
  #END IF

  #Merge Template data with application data
  LET server_print_file = print_html_simple(server_template_path_file,NULL,arg_str,"n")

  #IF local_debug THEN
  #  DISPLAY "3 - print_contact_html() - server_print_file= ", server_print_file
  #  DISPLAY "3 - print_contact_html() - client_path_file (before download) = ", client_path_file
  #END IF

  #Download print file to client
  LET client_path_file = file_download(server_print_file,fgl_basename(server_print_file), FALSE,NULL)

  #IF local_debug THEN
  #  DISPLAY "4 - print_contact_html() - client_path_file= ", client_path_file
  #END IF


  #IF local_debug THEN
  #  DISPLAY "print_contact_html() - client_path_file = ", client_path_file
  #END IF

  #Display client side file name to client browser window
  CALL print_html(client_path_file)                                                     -- print this html file

  CALL set_classic_help_file(previous_help_file_id)


END FUNCTION



