##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


##########################################################################
# settings_menu(win_x)  --pull down look like
##########################################################################
FUNCTION settings_menu(win_x)
  DEFINE 
    win_x,win_y             SMALLINT,  -- x position of window
    l_data                  ARRAY[8] OF CHAR(35),
    i,item_count,y          INTEGER,
    next_action             SMALLINT,
    previous_help_file_id   SMALLINT,
    menu_str_arr1           DYNAMIC ARRAY OF VARCHAR(30),  --i.e. used for rinmenu strings
    ret                     SMALLINT


  LET previous_help_file_id = get_current_classic_help()

  #advanced settings menu makes only sense for gui clients (char clients have no toolbar, dde, web service etc..)


  ###############################
  # TEXT mode clients
  ###############################
{  IF NOT fgl_fglgui() THEN   --menu for text mode client
    MENU "Settings"
      BEFORE MENU
        LET menu_str_arr1[1] = get_str(951)
        LET menu_str_arr1[2] = get_str(952)
        LET menu_str_arr1[3] = get_str(953)
        LET menu_str_arr1[4] = get_str(954)
        LET menu_str_arr1[5] = get_str(955)
        LET menu_str_arr1[6] = get_str(956)
        LET menu_str_arr1[7] = get_str(957)
        LET menu_str_arr1[8] = get_str(90)

      COMMAND "General" "General Settings"
        #CALL settings_general_text()
        CALL qxt_settings_main_edit()
      COMMAND "Change Password"
        CALL change_password()
      COMMAND "Administer Tables"
        CALL admin_main()
      COMMAND "Return"
        EXIT MENU
    END MENU

    RETURN  
  END IF 

}

  #####################
  # Menu for gui mode
  #####################
  LET item_count = sizeof(l_data)
  LET win_y = 5

  FOR i = 1 TO item_count -1  -- -1 because the EXIT menu item is from a different string id 
    #DISPLAY "", trim(i + 899),  " {", get_str(i+950) CLIPPED, "}"
    LET l_data[i] = "action_", trim(i),  " {", get_str(i+950) CLIPPED, "}"  -- menu strings start with index 950
    #DISPLAY l_data[i]
  END FOR
    #DISPLAY "action", trim(i + 899),  " {", get_str(90) CLIPPED, "}"
    LET l_data[i] =  "action_", trim(i),  " {", get_str(90) CLIPPED, "}"


  OPEN WINDOW w_settings
    AT win_y, win_x
    WITH FORM "form/f_settings_dropdown_l2"
    #ATTRIBUTE (BORDER, COMMENT LINE OFF)

	CALL ui.Interface.setImage("qx://application/icon16/industry/technical_drawing-tools.png")
	CALL ui.Interface.setText("Settings")
	


  #CALL fgl_window_open("w_settings",win_y,win_x,get_form_path("f_settings_dropdown_g"),1)

  #CALL set_count(sizeof(l_data))  -- may be not too good...
  CALL set_count(item_count)
  LET int_flag = FALSE

  DISPLAY ARRAY l_data TO sc_rec.* 
    ATTRIBUTE(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
    BEFORE DISPLAY
      CALL publish_toolbar("country_sel",0)
    BEFORE ROW
      LET next_action = arr_curr()   --testing arr_curr() function
      
    ON ACTION (action_1)
      LET next_action = 1
      EXIT DISPLAY

    ON ACTION (action_2)
      LET next_action = 2
      EXIT DISPLAY

    ON  ACTION (action_3)
      LET next_action = 3
      EXIT DISPLAY

    ON  ACTION (action_4) 
      LET next_action = 4
      EXIT DISPLAY


    ON  ACTION (action_5) 
      LET next_action = 5
      EXIT DISPLAY


    ON ACTION (action_6)  
      LET next_action = 6
      EXIT DISPLAY


    ON  ACTION (action_7) 
      LET next_action = 7
      EXIT DISPLAY


    ON  ACTION (action_8) 
      LET next_action = 8
      EXIT DISPLAY
  
  END DISPLAY

  CLEAR SCREEN

  CLOSE WINDOW w_settings


  CASE next_action
    WHEN 1
      #CALL settings_general()
      CALL qxt_settings_main_edit()
      CALL set_classic_help_file(previous_help_file_id)

    WHEN 2
      CALL qxt_settings_language_edit()
      #CALL manage_toolbar_list()
      CALL set_classic_help_file(previous_help_file_id)

    WHEN 3
      CALL change_password()

    WHEN 4
      CALL admin_main()

    WHEN 5
      RUN "db_qxt_manager.exe"  
        RETURNING ret
      #DISPLAY "RETURN =", ret

    WHEN 6
      RUN "db_qxt_toolbar_manager.exe"
        RETURNING ret
      #DISPLAY "RETURN =", ret

    WHEN 7  --About
      CALL about()

    WHEN 8  --Exit
      RETURN

    OTHERWISE
      CALL fgl_winmessage("Error","Error in settings_menu() - CASE value for next_action next_action=" || next_action,"ERROR")

  END CASE      



END FUNCTION




