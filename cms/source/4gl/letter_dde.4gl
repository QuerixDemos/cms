##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions for contact management
##################################################################################################################
#
# FUNCTION:                                          DESCRIPTION:                                           RETURN
#
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


#################################################
# FUNCTION letter_write_by_id (p_contact_id, l_subject, l_body, actid, p_operator_id2) 
#
# RETURN NONE
#################################################
FUNCTION letter_write_by_id(p_from_cont_id, p_to_cont_id) 
  DEFINE 
    p_from_cont_id LIKE contact.cont_id,
    p_to_cont_id LIKE contact.cont_id,
    l_from_contact  OF t_contact_address_rec,
    l_to_contact    OF t_contact_address_rec,
    doc_name           VARCHAR(100),
    l_client_doc_name_with_path  VARCHAR(250),
    #doc_name_with_path VARCHAR(200),
    ret                CHAR(3),
    l_document         BYTE,
    #l_email_rec   OF t_email_rec,
    #l_email_form_rec OF t_email_form_rec,
    local_debug SMALLINT

  LET local_debug = FALSE   --0=off 1=on

  IF local_debug THEN
    DISPLAY "letter_write_by_id() p_from_cont_id=",p_from_cont_id
    DISPLAY "letter_write_by_id() p_to_cont_id=",p_to_cont_id
  END IF

  #Check if contact record exists
  IF NOT contact_id_count(p_to_cont_id) THEN
    LET tmp_str = get_str(424), "\n",get_str(425)
    CALL fgl_winmessage(get_str(423),tmp_str,"error")
    RETURN
  END IF 


  CALL web_get_contact_address_rec(p_from_cont_id) RETURNING l_from_contact.*
  CALL web_get_contact_address_rec(p_to_cont_id) RETURNING l_to_contact.*



  LET doc_name = "cms_contact_letter1.doc"
  LET l_client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

  #Get the document_id for the file (the file will be retrieved from the document database table)
  # And download the document from the table to the server
  LET l_client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),l_client_doc_name_with_path,0)
###################################
# Note: Word is not Word - Different version and 'locations i.e. German MS-Word & US MS-Word
# can also show different behaviour in the DDE implementation
###################################


  #Start the corresponding (file system associated app in windows) with this document
  #Start up Word using the document - file will be downloaded by the function
  IF NOT run_client_application_with_client_document(l_client_doc_name_with_path) THEN
    CALL fgl_winmessage("Error","Could not start associated application with the document","error")
    #RETURN
  END IF

  sleep 1
  
  #Initialisee DDE environment inc. connection
  CALL init_dde("winword",doc_name,l_client_doc_name_with_path,NULL,FALSE)  --10=timeout  TRUE/FALSE turns debug on/off

  #Transfer Data
  CALL dde_word_transfer_from_contact_data(l_from_contact.*)
  CALL dde_word_transfer_to_contact_data(l_to_contact.*)

  #Save the document to be sure it works
  CALL dde_execute("[FileSave]",FALSE)

  #Optional Printing in Excel
  LET ret = fgl_winquestion ("Print", "Do you want to print the MS-Word Document?", "Yes", "Yes|No", "question", 1)
  
  IF ret = "Yes" THEN
    CALL dde_execute("[FilePrintDefault]",FALSE)   --FALSE turns error checking/capturing on
  END IF

  #Finnish/Close all DDE connections
  CALL DDEFinishAll()


END FUNCTION




#################################################################################
# FUNCTION dde_word_transfer_from_contact_data()
#
# Transfer data to MS-Word via DDE (from contact details)
#
# RETURN NONE
#################################################################################
FUNCTION dde_word_transfer_from_contact_data(p_from_contact)
  DEFINE
    p_from_contact  OF t_contact_address_rec


  CALL dde_transfer_bookmark("from_title",p_from_contact.cont_title)
  CALL dde_transfer_bookmark("from_firstname",p_from_contact.cont_fname)
  CALL dde_transfer_bookmark("from_lastname",p_from_contact.cont_lname)
  CALL dde_transfer_bookmark("from_company",p_from_contact.comp_name)
  CALL dde_transfer_bookmark("from_addr1",p_from_contact.cont_addr1)
  CALL dde_transfer_bookmark("from_addr2",p_from_contact.cont_addr2)
  CALL dde_transfer_bookmark("from_addr3",p_from_contact.cont_addr3)
  CALL dde_transfer_bookmark("from_city",p_from_contact.cont_city)
  CALL dde_transfer_bookmark("from_zone",p_from_contact.cont_zone)
  CALL dde_transfer_bookmark("from_zip",p_from_contact.cont_zip)
  CALL dde_transfer_bookmark("from_country",p_from_contact.cont_country)
  CALL dde_transfer_bookmark("from_phone",p_from_contact.cont_phone)
  CALL dde_transfer_bookmark("from_mobile",p_from_contact.cont_mobile)
  CALL dde_transfer_bookmark("from_fax",p_from_contact.cont_fax)
  CALL dde_transfer_bookmark("from_email",p_from_contact.cont_email)


END FUNCTION


#################################################################################
# FUNCTION dde_word_transfer_to_contact_data()
#
# Transfer data to MS-Word via DDE (to contact details)
#
# RETURN NONE
#################################################################################
FUNCTION dde_word_transfer_to_contact_data(p_to_contact)
  DEFINE
    p_to_contact   OF t_contact_address_rec

  CALL dde_transfer_bookmark("to_title",p_to_contact.cont_title)
  CALL dde_transfer_bookmark("to_firstname",p_to_contact.cont_fname)
  CALL dde_transfer_bookmark("to_lastname",p_to_contact.cont_lname)
  CALL dde_transfer_bookmark("to_company",p_to_contact.comp_name)
  CALL dde_transfer_bookmark("to_addr1",p_to_contact.cont_addr1)
  CALL dde_transfer_bookmark("to_addr2",p_to_contact.cont_addr2)
  CALL dde_transfer_bookmark("to_addr3",p_to_contact.cont_addr3)
  CALL dde_transfer_bookmark("to_city",p_to_contact.cont_city)
  CALL dde_transfer_bookmark("to_zone",p_to_contact.cont_zone)
  CALL dde_transfer_bookmark("to_zip",p_to_contact.cont_zip)
  CALL dde_transfer_bookmark("to_country",p_to_contact.cont_country)
  CALL dde_transfer_bookmark("to_phone",p_to_contact.cont_phone)
  CALL dde_transfer_bookmark("to_mobile",p_to_contact.cont_mobile)
  CALL dde_transfer_bookmark("to_fax",p_to_contact.cont_fax)
  CALL dde_transfer_bookmark("to_email",p_to_contact.cont_email)

  CALL dde_transfer_bookmark("to_title_1",p_to_contact.cont_title)
  CALL dde_transfer_bookmark("to_firstname_1",p_to_contact.cont_fname)
  CALL dde_transfer_bookmark("to_lastname_1",p_to_contact.cont_lname)



END FUNCTION

