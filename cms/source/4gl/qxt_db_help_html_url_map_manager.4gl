##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the help html facility - URL Adresses
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# 
#
#########################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_html_url_map_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_language_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_url_map_manager_lib_info()
  RETURN "qxt_db_help_url_map_manager"
END FUNCTION 

{
######################################################
# FUNCTION help_url_map_popup(p_help_url_map_id,p_order_field,p_accept_action)
#
# NOTE: This function is only for compatibility reasons with the db tools
#
# RETURN p_help_url_map_id
######################################################
FUNCTION help_url_map_popup(p_help_url_map_id,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_help_url_map_id            INTEGER,
    p_language_id                INTEGER,
    p_accept_action              SMALLINT,
    p_order_field                VARCHAR(128)

  CALL fgl_winmessage("Not available","This feature is not available ","info")

  RETURN 0
END FUNCTION
}

########################################################################################################################
# Data Access Functions
########################################################################################################################


#########################################################
# FUNCTION get_help_url_map_id(p_help_filename,p_language_id)
#
# get help_url_map id from name
#
# RETURN l_map_id
#########################################################
FUNCTION get_help_url_map_id(p_map_fname,p_language_id)
  DEFINE 
    p_map_fname               LIKE qxt_help_url_map.map_fname,
    p_language_id             LIKE qxt_help_url_map.language_id,
    l_map_id                  LIKE qxt_help_url_map.map_id,
    local_debug               SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_url_map_id() - p_help_filename = ", p_map_fname
  END IF

  SELECT map_id
    INTO l_map_id
    FROM qxt_help_url_map
    WHERE map_fname     = p_map_fname
      AND language_id   = p_language_id

  RETURN l_map_id
END FUNCTION



#########################################################
# FUNCTION get_help_url_map_fname(map_id,p_language_id)
#
# Get map_id from name
#
# RETURN l_map_fname
#########################################################
FUNCTION get_help_url_map_fname(p_map_id,p_language_id)
  DEFINE 
    p_map_id         LIKE qxt_help_url_map.map_id,
    p_language_id    LIKE qxt_help_url_map.language_id,
    l_map_fname      LIKE qxt_help_url_map.map_fname

  SELECT map_fname
    INTO l_map_fname
    FROM qxt_help_url_map
    WHERE map_id = p_map_id
      AND language_id = p_language_id

  RETURN l_map_fname
END FUNCTION


######################################################
# FUNCTION get_help_url_map_rec(p_map_id,p_language_id)
#
# Get the help_url_map record from pa_method_id
#
# RETURN l_help_url_map.*
######################################################
FUNCTION get_help_url_map_rec(p_map_id,p_language_id)
  DEFINE 
    p_map_id        LIKE qxt_help_url_map.map_id,
    p_language_id   LIKE qxt_help_url_map.language_id,
    l_help_url_map  RECORD LIKE qxt_help_url_map.*

  SELECT *
    INTO l_help_url_map.*
    FROM qxt_help_url_map
    WHERE map_id = p_map_id
      AND language_id   = p_language_id

  RETURN l_help_url_map.*
END FUNCTION



######################################################
# FUNCTION get_map_id_from_filename(p_filename)
#
# Get the map_id from a file
#
# RETURN l_map_id
######################################################
FUNCTION get_map_id_from_filename(p_filename,p_language)
  DEFINE 
    p_filename     LIKE qxt_help_url_map.map_fname,
    p_language     LIKE qxt_help_url_map.language_id,
    l_map_id LIKE qxt_help_url_map.map_id,
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_url_map_id() - map_fname = ", p_filename
  END IF

    SELECT map_id
      INTO l_map_id
      FROM qxt_help_url_map
      WHERE map_fname     = p_filename
        AND language_id   = p_language

  IF local_debug THEN
    DISPLAY "get_help_url_map_id() - l_map_id = ", l_map_id
  END IF

  RETURN l_map_id

END FUNCTION


####################################################
# FUNCTION map_fname_count(p_filename,p_language_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION map_fname_count(p_filename,p_language_id)
  DEFINE
    p_filename    LIKE qxt_help_url_map.map_fname,
    p_language_id LIKE qxt_help_url_map.language_id,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_help_url_map
      WHERE qxt_help_url_map.map_fname = p_filename
        AND qxt_help_url_map.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION contact_name_count(p_cont_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION map_id_and_lang_id_count(p_map_id,p_language_id)
  DEFINE
    p_map_id    LIKE qxt_help_url_map.map_id,
    p_language_id     LIKE qxt_help_url_map.language_id,
    r_count           SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_help_url_map
      WHERE qxt_help_url_map.map_id = p_map_id
        AND qxt_help_url_map.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION




########################################################################################################################
# List/Combo Functions
########################################################################################################################

######################################################
# FUNCTION help_url_map_popup_data_source()
#
# Data Source (cursor) for help_url_map_popup
#
# RETURN NONE
######################################################
FUNCTION help_url_map_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "map_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_help_url_map.map_id, ",
                 "qxt_language.language_name, ",
                 "qxt_help_url_map.map_fname, ",
                 "qxt_help_url_map.map_ext ",

                 "FROM qxt_help_url_map, qxt_language ",
                 "WHERE qxt_help_url_map.language_id = qxt_language.language_id "
                 #"help_url_map.language_id, ",


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "help_url_map_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_help_url_map FROM sql_stmt
  DECLARE c_help_url_map CURSOR FOR p_help_url_map

END FUNCTION


######################################################
# FUNCTION help_url_map_popup(p_map_id,p_order_field,p_accept_action)
#
# help_url_map selection window
#
# RETURN p_map_id
######################################################
FUNCTION help_url_map_popup(p_map_id,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_map_id                     LIKE qxt_help_url_map.map_id,  --default return value if user cancels
    p_language_id                LIKE qxt_help_url_map.language_id,   --default return value if user cancels
    l_help_url_map_arr           DYNAMIC ARRAY OF t_qxt_help_url_map_form_rec,    --RECORD LIKE qxt_help_url_map.*,  
    l_array_size                 SMALLINT,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_map_id                     LIKE qxt_help_url_map.map_id,
    l_language_id                LIKE qxt_help_url_map.language_id,
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "help_url_map_popup() - p_map_id=",p_map_id
    DISPLAY "help_url_map_popup() - p_order_field=",p_order_field
    DISPLAY "help_url_map_popup() - p_accept_action=",p_accept_action
  END IF


#  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_help_url_map_scroll", 2, 8, get_form_path("f_qxt_help_html_url_map_scroll_l2"),FALSE) 
    CALL populate_help_url_map_list_form_labels_g()
#  ELSE  --text
#    CALL fgl_window_open("w_help_url_map_scroll", 2, 8, get_form_path("f_qxt_help_html_url_map_scroll_t"),FALSE) 
#    CALL populate_help_url_map_list_form_labels_t()
#  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_help_url_map

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL help_url_map_create()
    END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL help_url_map_popup_data_source(p_order_field,get_toggle_switch())


    LET l_array_size = 500 -- due to dynamic array ...sizeof(l_help_url_map_arr)
    LET i = 1
    FOREACH c_help_url_map INTO l_help_url_map_arr[i].*
      IF local_debug THEN
        DISPLAY "help_url_map_popup() - i=",i
        DISPLAY "help_url_map_popup() - l_help_url_map_arr[i].map_id=",     l_help_url_map_arr[i].map_id
        DISPLAY "help_url_map_popup() - l_help_url_map_arr[i].language_id=",l_help_url_map_arr[i].language_name
        DISPLAY "help_url_map_popup() - l_help_url_map_arr[i].map_fname=",  l_help_url_map_arr[i].map_fname
        DISPLAY "help_url_map_popup() - l_help_url_map_arr[i].map_ext=",    l_help_url_map_arr[i].map_ext

      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_array_size THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_help_url_map_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		
    IF local_debug THEN
      DISPLAY "help_url_map_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      CALL fgl_window_close("w_help_url_map_scroll")
      RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "help_url_map_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)

    LET int_flag = FALSE
    DISPLAY ARRAY l_help_url_map_arr TO sc_help_url_map.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE ROW
        LET i = arr_curr()
        LET l_map_id = l_help_url_map_arr[i].map_id
        LET l_language_id = get_language_id(l_help_url_map_arr[i].language_name)

 
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        #LET i = l_help_url_map_arr[i].map_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL help_url_map_view(l_map_id, l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL help_url_map_edit(l_map_id, l_language_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL help_url_map_delete(l_map_id, l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL help_url_map_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","help_url_map_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "help_url_map_popup(p_map_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("help_url_map_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL help_url_map_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL help_url_map_edit(l_map_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL help_url_map_delete(l_map_id,l_language_id)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "map_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "map_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_help_url_map_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_map_id,  p_language_id 
  ELSE 
    RETURN l_map_id,  l_language_id 
  END IF

END FUNCTION




########################################################################################################################
# Edit,Create,Input,View,Delete... Functions
########################################################################################################################


######################################################
# FUNCTION help_url_map_create()
#
# Create a new help_url_map record
#
# RETURN NULL
######################################################
FUNCTION help_url_map_create()
  DEFINE 
    l_help_url_map RECORD LIKE qxt_help_url_map.*

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_l2"), FALSE) 
    CALL populate_help_url_map_form_create_labels_g()

#  ELSE
#    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_t"), TRUE) 
#    CALL populate_help_url_map_form_create_labels_t()
#  END IF

  LET l_help_url_map.map_id = 0

  LET int_flag = FALSE
  
  #Initialise some variables
  LET l_help_url_map.language_id = 1

  # CALL the INPUT
  CALL help_url_map_input(l_help_url_map.*)
    RETURNING l_help_url_map.*

  IF NOT int_flag THEN
    INSERT INTO qxt_help_url_map VALUES (
      l_help_url_map.map_id,
      l_help_url_map.language_id, 
      l_help_url_map.map_fname,
      l_help_url_map.map_ext
                                        )

  END IF

  #Always actions
  CALL fgl_window_close("w_help_url_map")

  IF NOT int_flag THEN
 
    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_help_url_map.map_id, l_help_url_map.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL, NULL
  END IF

END FUNCTION


#################################################
# FUNCTION help_url_map_edit(p_map_id)
#
# Edit help_url_map record
#
# RETURN NONE
#################################################
FUNCTION help_url_map_edit(p_map_id,p_language_id)
  DEFINE 
    p_map_id                 LIKE qxt_help_url_map.map_id,
    p_language_id            LIKE qxt_help_url_map.language_id,
    l_key1_map_id            LIKE qxt_help_url_map.map_id,
    l_key2_language_id       LIKE qxt_help_url_map.language_id,
    #l_help_url_map_form_rec OF t_help_url_map_form_rec,
    l_help_url_map_rec       RECORD LIKE qxt_help_url_map.*,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "help_url_map_edit() - Function Entry Point"
    DISPLAY "help_url_map_edit() - p_map_id=", p_map_id
    DISPLAY "help_url_map_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key combination for the SQL update
  LET l_key1_map_id = p_map_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_help_url_map_rec(p_map_id,p_language_id) 
    RETURNING l_help_url_map_rec.*

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_l2"), FALSE) 
    CALL populate_help_url_map_form_edit_labels_g()

#  ELSE
#    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_t"), TRUE) 
#    CALL populate_help_url_map_form_edit_labels_t()
#  END IF


  #Call the INPUT
  CALL help_url_map_input(l_help_url_map_rec.*) 
    RETURNING l_help_url_map_rec.*

    IF local_debug THEN
      DISPLAY "help_url_map_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "help_url_map_edit() - l_help_url_map_rec.map_id=",     l_help_url_map_rec.map_id
      DISPLAY "help_url_map_edit() - l_help_url_map_rec.language_id=",l_help_url_map_rec.language_id
      DISPLAY "help_url_map_edit() - l_help_url_map_rec.map_fname=",  l_help_url_map_rec.map_fname
      DISPLAY "help_url_map_edit() - l_help_url_map_rec.map_ext=",    l_help_url_map_rec.map_ext
    END IF

    UPDATE qxt_help_url_map
      SET 
          map_id =             l_help_url_map_rec.map_id,
          language_id =        l_help_url_map_rec.language_id,
          map_fname =          l_help_url_map_rec.map_fname,
          map_ext =            l_help_url_map_rec.map_ext
      WHERE qxt_help_url_map.map_id =       l_key1_map_id
        AND qxt_help_url_map.language_id  = l_key2_language_id


    IF local_debug THEN
      DISPLAY "help_url_map_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    END IF

  CALL fgl_window_close("w_help_url_map")

  #Check if user canceled
  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL,NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_help_url_map_rec.map_id, l_help_url_map_rec.language_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    LET int_flag = FALSE
    RETURN NULL,NULL

  END IF


END FUNCTION


#################################################
# FUNCTION help_url_map_input(p_help_url_map)
#
# Input help_url_map details (edit/create)
#
# RETURN l_help_url_map.*
#################################################
FUNCTION help_url_map_input(p_help_url_map_rec)
  DEFINE 
    p_help_url_map_rec            RECORD LIKE qxt_help_url_map.*,
    l_help_url_map_form_rec       OF t_qxt_help_url_map_form_rec,
    l_server_blob_file            VARCHAR(300),  --,
    l_orignal_filename            LIKE qxt_help_url_map.map_fname,
    l_orignal_map_id              LIKE qxt_help_url_map.map_id,
    local_debug                   SMALLINT,
    tmp_str                       VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_filename = p_help_url_map_rec.map_fname
  LET l_orignal_map_id = p_help_url_map_rec.map_id

  #copy record data to form_record format record
  CALL copy_help_url_map_record_to_form_record(p_help_url_map_rec.*) RETURNING l_help_url_map_form_rec.*

  #Preview html in viewer field - only for gui clients
  IF fgl_fglgui() THEN
    LET tmp_str = get_help_html_base_url_path(l_help_url_map_form_rec.map_fname) CLIPPED, l_help_url_map_form_rec.map_ext
    DISPLAY  tmp_str TO preview
    DISPLAY  tmp_str TO preview_url
  END IF

  IF local_debug THEN
    DISPLAY  "help_url_map_input() - URL (tmp_str)=", tmp_str
  END IF

  ####################
  #Start actual INPUT
  INPUT BY NAME l_help_url_map_form_rec.* WITHOUT DEFAULTS HELP 1


    AFTER FIELD map_fname
      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_url_map_form_rec.map_fname,NULL,TRUE)  THEN
        NEXT FIELD map_fname
      END IF
      #Preview html in viewer field - only for gui clients
      IF fgl_fglgui() THEN
        LET tmp_str = get_help_html_base_url_path(l_help_url_map_form_rec.map_fname) CLIPPED, l_help_url_map_form_rec.map_ext
        DISPLAY  tmp_str TO preview
        DISPLAY  tmp_str TO preview_url
      END IF

    IF local_debug THEN
      DISPLAY  "help_url_map_input() - URL (tmp_str)=", tmp_str
    END IF


    AFTER FIELD map_ext
      #Preview html in viewer field - only for gui clients
      IF fgl_fglgui() THEN
        LET tmp_str = get_help_html_base_url_path(l_help_url_map_form_rec.map_fname) CLIPPED, l_help_url_map_form_rec.map_ext
        DISPLAY  tmp_str TO preview
        DISPLAY  tmp_str TO preview_url
      END IF

    IF local_debug THEN
      DISPLAY  "help_url_map_input() - URL (tmp_str)=", tmp_str
    END IF

    AFTER FIELD language_name
      #Language must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(972), l_help_url_map_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD language_name
      END IF



    # AFTER INPUT BLOCK
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #Language must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(972), l_help_url_map_form_rec.language_name,NULL,TRUE)  THEN
          NEXT FIELD language_name
          CONTINUE INPUT
        END IF
      #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_url_map_form_rec.map_fname,NULL,TRUE)  THEN

          NEXT FIELD map_fname
          CONTINUE INPUT
        END IF

      #The combination language_id and map_id must be unique
      IF map_id_and_lang_id_count(l_help_url_map_form_rec.map_id,get_language_id(l_help_url_map_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_map_id <> l_help_url_map_form_rec.map_id THEN  --it is not an edit operation
          CALL tl_msg_duplicate_doublekey(get_str_tool(311),get_str_tool(312),NULL)
          NEXT FIELD map_id
          CONTINUE INPUT
        END IF
      END IF

  END INPUT

  IF local_debug THEN
    DISPLAY "help_url_map_input() - l_help_url_map_form_rec.map_id=",       l_help_url_map_form_rec.map_id
    DISPLAY "help_url_map_input() - l_help_url_map_form_rec.language_name=",l_help_url_map_form_rec.language_name
    DISPLAY "help_url_map_input() - l_help_url_map_form_rec.map_fname=",    l_help_url_map_form_rec.map_fname
    DISPLAY "help_url_map_input() - l_help_url_map_form_rec.map_ext=",      l_help_url_map_form_rec.map_ext
  END IF


  #Copy the form record data to a normal help_url_map record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_help_url_map_form_record_to_record(l_help_url_map_form_rec.*) RETURNING p_help_url_map_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "help_url_map_input() - p_help_url_map_rec.map_id=",      p_help_url_map_rec.map_id
    DISPLAY "help_url_map_input() - p_help_url_map_rec.language_id=", p_help_url_map_rec.language_id
    DISPLAY "help_url_map_input() - p_help_url_map_rec.map_fname=",   p_help_url_map_rec.map_fname
    DISPLAY "help_url_map_input() - p_help_url_map_rec.map_ext=",     p_help_url_map_rec.map_ext
  END IF

  RETURN p_help_url_map_rec.*

END FUNCTION


#################################################
# FUNCTION help_url_map_delete(p_map_id,p_language_id)
#
# Delete a help_url_map record
#
# RETURN NONE
#################################################
FUNCTION help_url_map_delete(p_map_id,p_language_id)
  DEFINE 
    p_map_id       LIKE qxt_help_url_map.map_id,
    p_language_id  LIKE qxt_help_url_map.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(980), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_help_url_map 
        WHERE map_id = p_map_id
          AND qxt_help_url_map.language_id   = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION help_url_map_view(p_map_id,p_language_id)
#
# View help_url_map record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION help_url_map_view(p_map_id,p_language_id)
  DEFINE 
    p_map_id             LIKE qxt_help_url_map.map_id,
    p_language_id        LIKE qxt_help_url_map.language_id,
    l_help_url_map_rec   RECORD LIKE qxt_help_url_map.*


  CALL get_help_url_map_rec(p_map_id,p_language_id) 
    RETURNING l_help_url_map_rec.*

  CALL help_url_map_view_by_rec(l_help_url_map_rec.*)

END FUNCTION


#################################################
# FUNCTION help_url_map_view_by_rec(p_help_url_map_rec)
#
# View help_url_map record in window-form
#
# RETURN NONE
#################################################
FUNCTION help_url_map_view_by_rec(p_help_url_map_rec)
  DEFINE 
    p_help_url_map_rec  RECORD LIKE qxt_help_url_map.*,
    inp_char            CHAR,
    tmp_str             VARCHAR(250)

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_l2"), FALSE) 
    CALL populate_help_url_map_form_view_labels_g()

#  ELSE
#    CALL fgl_window_open("w_help_url_map", 3, 3, get_form_path("f_qxt_help_html_url_map_det_t"), TRUE) 
#    CALL populate_help_url_map_form_view_labels_t()
#
#  END IF

  #Preview html in viewer field - only for gui clients
  IF fgl_fglgui() THEN
    LET tmp_str = get_help_html_base_url_path(p_help_url_map_rec.map_fname)
    DISPLAY  tmp_str TO preview
    DISPLAY  tmp_str TO preview_url
  END IF

  DISPLAY BY NAME p_help_url_map_rec.*


  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_help_url_map")
END FUNCTION





########################################################################################################################
# Record Copy Functions
########################################################################################################################


######################################################
# FUNCTION copy_help_url_map_record_to_form_record(p_help_url_map_rec)  
#
# Copy normal help_url_map record data to type help_url_map_form_rec
#
# RETURN l_help_url_map_form_rec.*
######################################################
FUNCTION copy_help_url_map_record_to_form_record(p_help_url_map_rec)  
  DEFINE
    p_help_url_map_rec       RECORD LIKE qxt_help_url_map.*,
    l_help_url_map_form_rec  OF t_qxt_help_url_map_form_rec

  LET l_help_url_map_form_rec.map_id =        p_help_url_map_rec.map_id
  LET l_help_url_map_form_rec.language_name = get_language_name(p_help_url_map_rec.language_id)
  LET l_help_url_map_form_rec.map_fname =     p_help_url_map_rec.map_fname
  LET l_help_url_map_form_rec.map_ext =       p_help_url_map_rec.map_ext

  RETURN l_help_url_map_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_help_url_map_form_record_to_record(p_help_url_map_form_rec)  
#
# Copy type help_url_map_form_rec to normal help_url_map record data
#
# RETURN l_help_url_map_rec.*
######################################################
FUNCTION copy_help_url_map_form_record_to_record(p_help_url_map_form_rec)  
  DEFINE
    l_help_url_map_rec       RECORD LIKE qxt_help_url_map.*,
    p_help_url_map_form_rec  OF t_qxt_help_url_map_form_rec

  LET l_help_url_map_rec.map_id =      p_help_url_map_form_rec.map_id
  LET l_help_url_map_rec.language_id = get_language_id(p_help_url_map_form_rec.language_name)
  LET l_help_url_map_rec.map_fname =   p_help_url_map_form_rec.map_fname
  LET l_help_url_map_rec.map_ext =     p_help_url_map_form_rec.map_ext

  RETURN l_help_url_map_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_help_url_map_scroll()
#
# Populate help_url_map grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_help_url_map_scroll()
  CALL fgl_grid_header("sc_help_url_map","map_id",get_str_tool(311),"right","F13")  --Help ID
  CALL fgl_grid_header("sc_help_url_map","language_name",get_str_tool(312),"left","F14")  --language
  CALL fgl_grid_header("sc_help_url_map","map_fname",get_str_tool(313),"left","F15")  --file name
  CALL fgl_grid_header("sc_help_url_map","map_ext",get_str_tool(314),"left","F16")  --file name

END FUNCTION

#######################################################
# FUNCTION populate_help_url_map_form_labels_g()
#
# Populate help_url_map form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(310) TO lbTitle

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_help_url_map_form_labels_t()
#
# Populate help_url_map form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_labels_t()

  DISPLAY get_str_tool(310) TO lbTitle

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_help_url_map_form_edit_labels_g()
#
# Populate help_url_map form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_edit_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","Preview") 
  CALL fgl_settitle(trim(get_str_tool(310)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_name")


END FUNCTION



#######################################################
# FUNCTION populate_help_url_map_form_edit_labels_t()
#
# Populate help_url_map form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_edit_labels_t()

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_help_url_map_form_view_labels_g()
#
# Populate help_url_map form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_view_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","Preview")
  CALL fgl_settitle(trim(get_str_tool(310)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_help_url_map_form_view_labels_t()
#
# Populate help_url_map form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_view_labels_t()

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_name")

END FUNCTION


#######################################################
# FUNCTION populate_help_url_map_form_create_labels_g()
#
# Populate help_url_map form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_create_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","Preview")
  CALL fgl_settitle(trim(get_str_tool(310)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_help_url_map_form_create_labels_t()
#
# Populate help_url_map form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_form_create_labels_t()

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_help_url_map_list_form_labels_g()
#
# Populate help_url_map list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(310)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_help_url_map_scroll()

  #DISPLAY get_str_tool(311) TO dl_f1
  #DISPLAY get_str_tool(312) TO dl_f2
  #DISPLAY get_str_tool(313) TO dl_f3
  #DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete

END FUNCTION


#######################################################
# FUNCTION populate_help_url_map_list_form_labels_t()
#
# Populate help_url_map list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_url_map_list_form_labels_t()

  DISPLAY trim(get_str_tool(310)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(311) TO dl_f1
  DISPLAY get_str_tool(312) TO dl_f2
  DISPLAY get_str_tool(313) TO dl_f3
  DISPLAY get_str_tool(314) TO dl_f4
  #DISPLAY get_str_tool(315) TO dl_f5
  #DISPLAY get_str_tool(316) TO dl_f6
  #DISPLAY get_str_tool(317) TO dl_f7
  #DISPLAY get_str_tool(318) TO dl_f8
  #DISPLAY get_str_tool(319) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION



########################################################################################################################
# EOF
########################################################################################################################






