##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

#####################################################################################
# FUNCTION loadThemeResources(argThemeId)
#
# Function to load theme files
#####################################################################################
FUNCTION loadThemeResources(argThemeId)
	DEFINE argThemeId SMALLINT
	DEFINE msgStr STRING

	#display "ThemeId=", argThemeId 
		CASE argThemeId
			WHEN 0
				#DISPLAy "no theme - themeId=",argThemeId

			WHEN 1  -- classic 
				#display "load theme/_master_theme.qxtheme themeId=",argThemeId
				CALL apply_theme("theme/_master_theme.qxtheme")				
	
			WHEN 2 --classic  
				#display "load theme/_master_theme.qxtheme themeId=",argThemeId
				CALL apply_theme("theme/_master_theme.qxtheme")				

			WHEN 3  --facebook design - direct link
				#display "load theme  theme/zeplin- FACEBOOK themeId=",argThemeId
				call apply_theme("theme/zeplin")

			WHEN 4  --facebook design - mdi with no-direct-link/history-link
				#display "call apply_theme(theme/mdi-modern)"
				call apply_theme("theme/mdi-modern")

			OTHERWISE 
				LET msgStr = "The specified theme id was not found\nID=", trim(get_themeId())
				CALL fgl_winmessage("Invalid theme id was found in argument",msgStr,"error")
		END CASE



	

END FUNCTION