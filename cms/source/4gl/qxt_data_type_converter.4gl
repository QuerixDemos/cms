##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Access functions for the current application id
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


FUNCTION get_boolean_enabled_name(p_id)
  DEFINE 
    p_id     SMALLINT,
    err_msg  VARCHAR(200)

  CASE p_id
    WHEN 0
      RETURN "Disabled"
 
    WHEN 1
      RETURN "Enabled"

 
    WHEN 2
      RETURN "N/A"

    OTHERWISE
      LET err_msg = "Error in get_boolean_enabled_name()\nInvalid boolean value in argument\np_id = ", p_id
      CALL fgl_winmessage("Error in get_boolean_enabled_name()",err_msg, "error")

  END CASE

  RETURN NULL
END FUNCTION


FUNCTION get_boolean_enabled_id(p_name)
  DEFINE 
    p_name   VARCHAR(20),
    err_msg  VARCHAR(200)

  LET p_name = downshift(p_name)
  CASE p_name
    WHEN "disabled"
      RETURN 0
 
    WHEN "enabled"
      RETURN 1 

    WHEN "n/a"
      RETURN 2

    OTHERWISE
      LET err_msg = "Error in get_boolean_enabled_id()\nInvalid boolean value in argument\p_name = ", p_name
      CALL fgl_winmessage("Error in get_boolean_enabled_id()",err_msg, "error")

  END CASE

  RETURN NULL
END FUNCTION


###################################################################################
# FUNCTION boolean_enabled_combo_list(cb_field_name)
#
# Populates the combo box with enabled/disabled
# Note: uses/requires tool_lib_db OR tool_lib_txt
#
# RETURN NONE
###################################################################################
FUNCTION boolean_enabled_combo_list(cb_field_name)
  DEFINE 
    cb_field_name VARCHAR(35),   --form field name for the country combo list field
    local_debug   SMALLINT

  LET local_debug = FALSE
  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "boolean_enabled_combo_list() - cb_field_name =", cb_field_name
  END IF

  CALL fgl_list_set(cb_field_name,1,"Disabled")
  CALL fgl_list_set(cb_field_name,2,"Enabled")
  CALL fgl_list_set(cb_field_name,3,"N/A")

END FUNCTION

