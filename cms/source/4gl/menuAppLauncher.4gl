##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



#####################################################################
# FUNCTION loadMenuAndStyles()
#
# Load/Apply menu
#####################################################################
FUNCTION loadMenuAndBaseUiStyles()
	DEFINE cssId smallint
	DEFINE l_child CHAR
	
	LET l_child = fgl_getenv("qx_child")

	IF l_child IS NOT NULL THEN  -- Relevant for apps which are launched in mdi mode

		#This is a child process/program
		#... guess, I don't need to do anything
		# just continue to launch/run app 
		# as I don't need to reload the menu and all css styles etc.. are inherited/applied automatically
	ELSE
		#display "menuId =", getMenuId()
		CASE getMenuId()
		
			WHEN 0 --don't load any startmenu i.e. to run the cms (cms-all) without the startmenu
#display "do NOT load a startmenu"			
			WHEN 1	--classic SDI
#display "load classic startmenu startmenu/cms_startmenu_sdi"
				CALL ui.Interface.setType("container")
				CALL ui.Interface.setName("CMS 7.0")  
				CALL ui.Application.GetCurrent().setMenuType("Tree")
				CALL ui.Application.GetCurrent().SetClassNames(["tabbed_container","single"])

  			CALL ui.Interface.loadStartMenu("startmenu/cms_startmenu_sdi")

			WHEN 2  --classic MDI
#display "load classic startmenu startmenu/cms_startmenu_mdi"
				CALL ui.Interface.setType("container")
				CALL ui.Interface.setName("CMS 7.0")  
				CALL ui.Application.GetCurrent().setMenuType("Tree")
				CALL ui.Application.GetCurrent().SetClassNames(["tabbed_container"])
 				CALL ui.Interface.loadStartMenu("startmenu/cms_startmenu_mdi")

			WHEN 3
#display "load facebook startmenu startmenu/cms_startmenu_zeplin_sdi"		
				CALL ui.Interface.setType("container")
				CALL ui.Interface.setName("CMS 7.0")  
				CALL ui.Application.GetCurrent().setMenuType("Tree")
				CALL ui.Application.GetCurrent().SetClassNames(["tabbed_container","disable-mobile-transform","single"])
  			CALL ui.Interface.loadStartMenu("startmenu/cms_startmenu_zeplin_sdi")
  			
			WHEN 4
#display "load facebook startmenu startmenu/cms_startmenu_zeplin_mdi"				
				CALL ui.Interface.setType("container")
				CALL ui.Interface.setName("CMS 7.0")  
				CALL ui.Application.GetCurrent().setMenuType("Tree")
--				CALL ui.Application.GetCurrent().SetClassNames(["tabbed_container","single"])		
				CALL ui.Application.GetCurrent().SetClassNames(["single"])		
  				CALL ui.Interface.loadStartMenu("startmenu/cms_startmenu_zeplin_mdi")

			OTHERWISE
  			CALL fgl_winmessage("Invalid switch","Invalid value for case passed to menuID / loadMenuAndBaseUiStyles","error") --CALL ui.Interface.loadStartMenu("startmenu/cms_startmenu_sdi")
		END CASE
		
	END IF

END FUNCTION  


#####################################################################
# FUNCTION appLauncherMenu()
#
# Depending on the startmenu.fm2, we need a corresponding menu function
#####################################################################
FUNCTION appLauncherMenu()
	DEFINE l_menuId INTEGER
	
	LET l_menuId = getMenuId() 
	
	CASE l_menuId
	
		WHEN 0 -- do nothing / no startmenu is loaded
			display "do NOT run any menu logic code menuId=", getMenuId()
			 
		WHEN 1 --classic cms / SDI startmenu
			display "run CLASSIC CMS SDI menu logic code menuId=", getMenuId()
			MENU
				ON ACTION "actExitMenu"		
					EXIT MENU

				ON ACTION "actLogOut"  --Note: this action is not used / startmenu has got a batchevent handler  calls first funciton to disable session folloed by actExit
					CALL disable_current_session_record()
					EXIT MENU				
			END MENU

		WHEN 2 --classic style cms / MDI startmenu
			display "run CLASSIC CMS MDI menu logic code menuId=", getMenuId()
			MENU
--alpr#115997354				ON ACTION "actMenuButtonInvoked"
--alpr#115997354					CALL runProgram(fgl_getlastwidgetid(),g_operator.operator_id) 

				ON ACTION "actExitMenu"		
					EXIT MENU

				ON ACTION "actLogOut"  --Note: this action is not used / startmenu has got a batchevent handler  calls first funciton to disable session folloed by actExit
					CALL disable_current_session_record()
					EXIT MENU
			END MENU

		WHEN 3 --facebook cms / SDI startmenu
			display "run FACEBOOK CMS SDI menu logic code menuId=", getMenuId()
			MENU
				ON ACTION "actExitMenu"		
					EXIT MENU

				ON ACTION "actLogOut"  --Note: this action is not used / startmenu has got a batchevent handler  calls first funciton to disable session folloed by actExit
					CALL disable_current_session_record()
					EXIT MENU				
			END MENU

		WHEN 4 --facebook style cms / MDI startmenu
			display "run FACEBOOK CMS MDI menu logic code menuId=", getMenuId()
			MENU
--alpr#115997354				ON ACTION "actMenuButtonInvoked"
--alpr#115997354					CALL runProgram(fgl_getlastwidgetid(),g_operator.operator_id) 

				ON ACTION "actExitMenu"		
					EXIT MENU

				ON ACTION "actLogOut"  --Note: this action is not used / startmenu has got a batchevent handler  calls first funciton to disable session folloed by actExit
					CALL disable_current_session_record()
					EXIT MENU
			END MENU

			OTHERWISE
				CALL fgl_winmessage("4GL demo code error","Invalid value for menuId","error")
				EXIT PROGRAM

	END CASE
END FUNCTION


#######################################################
# NOTE: This is only used for the MDI container menu ! NOT SDI
#
# FUNCTION runProgram(progID, argCurrent_operator_id)
# RETURN void
# Launches programs depending on the last selected menu item id in the startmenu
#######################################################
FUNCTION runProgram(progID, argCurrent_operator_id)
	DEFINE progID STRING
	DEFINE argCurrent_operator_id INT
	DEFINE argString STRING
	DEFINE launchString STRING

	#form the argument string which is common/reused for ALL module launches
#	LET argString =  "config ", trim (get_main_cfg_filename()), " operatorId ", trim(argCurrent_operator_id), " parentSessionId ", trim(getParentSessionId()) 	   
	LET argString =  "config ", trim (get_main_cfg_filename()), " ThemeId 1" 	   
 	   

	
	              
	CASE progID
		WHEN "progCMS"
			LET launchString = "cms"  -- " config ", trim (get_main_cfg_filename())
			#RUN launchString WITHOUT WAITING
                       
		WHEN "progCompany"
			LET launchString = "cmsCompany"

		WHEN "progContact"
			LET launchString = "cmsContact"

                 
                     
                        
		WHEN "progCmsAccount"
			LET launchString = "cmsAccount"   

		WHEN "progCmsActivity"
			LET launchString = "cmsActivity"   
		WHEN "progCmsInvoice"
			LET launchString = "cmsInvoice"   
		WHEN "progCmsMailInbox"
			LET launchString = "cmsMailInbox"   
		WHEN "progCmsMailOutbox"
			LET launchString = "cmsMailOutbox"   
		WHEN "progCmsMailbox"
			LET launchString = "cmsMailbox"   
		WHEN "progCmsStock"
			LET launchString = "cmsStock"   
                   
		WHEN "progCmsSupplies"
			LET launchString = "cmsSupplies"   
                   
		WHEN "progCmsReport"
			LET launchString = "cmsReport"   

		WHEN "progCmsReportInvoice"
			LET launchString = "cmsReport module invoice"   

 
                   
		WHEN "progCmsAdmin"
			LET launchString = "cmsAdmin"   
                   
		WHEN "progCmsOperator"
			LET launchString = "cmsModuleOperator"            
			
          ###########################                                                          
					#   Settings Section      #
					########################### 			

                   
		WHEN "progCmsSettingsGeneral"
			LET launchString = "cmsSettingsGeneral"  
			                   
		WHEN "progCmsSettingsLanguage"
			LET launchString = "cmsSettingsLanguage"   			
			                   
		WHEN "progCmsPassword"
			LET launchString = "cmsPassword"   			
						
			       

          ###########################                                                          
					#   Setup Section         #
					###########################                                      

		WHEN "progCmsSetupOperator"
			LET launchString = "cmsSetupOperator"          

		WHEN "progCmsSetupOperatorSession"
			LET launchString = "cmsSetupOperatorSession"          



		WHEN "progCmsSetupIndustryType"
			LET launchString = "cmsSetupIndustryType"          

		WHEN "progCmsSetupCompanyType"
			LET launchString = "cmsSetupCompanyType"  

		WHEN "progCmsSetupTaxRate"
			LET launchString = "cmsSetupTaxRate"  

		WHEN "progCmsSetupPaymentMethod"
			LET launchString = "cmsSetupPaymentMethod"   
 
		WHEN "progCmsSetupCurrency"
			LET launchString = "cmsSetupCurrency"   
			
		WHEN "progCmsSetupDeliveryType"
			LET launchString = "cmsSetupDeliveryType"  
			 
		WHEN "progCmsSetupCountry"
			LET launchString = "cmsSetupCountry"   

		WHEN "progCmsSetupTitle"
			LET launchString = "cmsSetupTitle" 			 

		WHEN "progCmsSetupContactDepartment"
			LET launchString = "cmsSetupContactDepartment" 			 

		WHEN "progCmsSetupPositionType"
			LET launchString = "cmsSetupPositionType" 

		WHEN "progCmsSetupActivityType"
			LET launchString = "cmsSetupActivityType" 

		WHEN "progCmsSetupDocument"
			LET launchString = "cmsSetupDocument" 
 
 		WHEN "progCmsSetupDocument"
		LET launchString = "cmsSetupDocument" 




 		WHEN "progCmsSetupHelpHtml"
		LET launchString = "cmsSetupHelpHtml" 


 		WHEN "progCmsSetupHelpUrlMap"
		LET launchString = "cmsSetupHelpUrlMap" 


 		WHEN "progCmsSetupLanguage"
		LET launchString = "cmsSetupLanguage" 




		### Section for qxt_tools setup options

 		WHEN "progCmsSetupApplicationMap"
		LET launchString = "cmsSetupApplicationMap" 


		#main config tools
		WHEN "progDbQxtManager"
			LET launchString = "db_qxt_manager"		
                        
		WHEN "progDbQxtToolbarManager"
			LET launchString = "db_qxt_toolbarManager" 
			

		#About...

 		WHEN "progCmsAbout"
		LET launchString = "cmsAbout" 




          ###########################                                                          
					#   OTHERWISE Section     #
					###########################                          
                OTHERWISE
                        CALL fgl_winmessage("Invalid button identifier",progID,"error")
        END CASE                  

	#Run the app
	LET launchstring = launchstring, " ", trim(argString)
	CALL fgl_winmessage("launchstring",launchstring,"info")
	RUN launchString WITHOUT WAITING

END FUNCTION




