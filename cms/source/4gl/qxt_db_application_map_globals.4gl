##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS



#application_map TYPE
  DEFINE t_qxt_application_rec TYPE AS
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name
    END RECORD

  DEFINE t_qxt_application_form_rec TYPE AS
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name

    END RECORD

  DEFINE t_qxt_application_form_rec2 TYPE AS  --used for grid
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name
    END RECORD


END GLOBALS


