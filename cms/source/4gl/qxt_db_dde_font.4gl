##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the dde_font table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_qxt_dde_font(p_language_id)  get dde_font id from p_language_id        l_dde_font
# get_language_id(category_id)    Get language_id from p_dde_font        l_language_id
# get_dde_font_rec(p_dde_font)   Get the dde_font record from p_dde_font  l_dde_font.*
# dde_font_popup_data_source()               Data Source (cursor) for dde_font_popup              NONE
# dde_font_popup                             dde_font selection window                            p_dde_font
# (p_dde_font,p_order_field,p_accept_action)
# dde_font_combo_list(cb_field_name)         Populates dde_font combo list from db                NONE
# dde_font_create()                          Create a new dde_font record                         NULL
# dde_font_edit(p_dde_font)      Edit dde_font record                                 NONE
# dde_font_input(p_dde_font_rec)    Input dde_font details (edit/create)                 l_dde_font.*
# dde_font_delete(p_dde_font)    Delete a dde_font record                             NONE
# dde_font_view(p_dde_font)      View dde_font record by ID in window-form            NONE
# dde_font_view_by_rec(p_dde_font_rec) View dde_font record in window-form               NONE
# get_qxt_dde_font_from_language_id(p_language_id)          Get the dde_font from a file                      l_dde_font
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# dde_font_id_count(p_dde_font)  tests if a record with this dde_font already exists r_count
# copy_dde_font_record_to_form_record        Copy normal dde_font record data to type dde_font_form_rec   l_dde_font_form_rec.*
# (p_dde_font_rec)
# copy_dde_font_form_record_to_record        Copy type dde_font_form_rec to normal dde_font record data   l_dde_font_rec.*
# (p_dde_font_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_dde_font_scroll()              Populate dde_font grid headers                       NONE
# populate_dde_font_form_labels_g()          Populate dde_font form labels for gui                NONE
# populate_dde_font_form_labels_t()          Populate dde_font form labels for text               NONE
# populate_dde_font_form_edit_labels_g()     Populate dde_font form edit labels for gui           NONE
# populate_dde_font_form_edit_labels_t()     Populate dde_font form edit labels for text          NONE
# populate_dde_font_form_view_labels_g()     Populate dde_font form view labels for gui           NONE
# populate_dde_font_form_view_labels_t()     Populate dde_font form view labels for text          NONE
# populate_dde_font_form_create_labels_g()   Populate dde_font form create labels for gui         NONE
# populate_dde_font_form_create_labels_t()   Populate dde_font form create labels for text        NONE
# populate_dde_font_list_form_labels_g()     Populate dde_font list form labels for gui           NONE
# populate_dde_font_list_form_labels_t()     Populate dde_font list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_dde_font_globals.4gl"

###########################################################
# FUNCTION get_dde_font_lib_info()
#
# Simply returns libray type /  name
#
# RETURN "FILE - qxt_file_dde_font"
###########################################################
FUNCTION get_dde_font_lib_info()
  RETURN "DB - qxt_file_dde_font"
END FUNCTION 


###########################################################
# FUNCTION process_dde_font_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_dde_font_init(p_filename)
  DEFINE 
    p_filename     VARCHAR(100)

  #Doesn't need to do anything with DB library
  # no files need to be imported
  #This function must be compatible with the file & db toolbar library
END FUNCTION



############################################################################################
# Data Access Functions
############################################################################################


###########################################################
# FUNCTION get_excel_dde_font_rec(p_dde_font_id)
#
# Get the excel dde font record
#
# RETURN NONE
###########################################################
FUNCTION get_excel_dde_font_rec(p_dde_font_id)
  DEFINE
    p_dde_font_id LIKE qxt_dde_font.dde_font_id,
    r_count       SMALLINT,
    l_font_rec    OF t_qxt_dde_short_rec


  SELECT
      font_name, 
      font_bold,      
      font_size,           
      strike_through,       
      super_script,          
      lower_script,         
      option_1,              
      option_2,            
      option_3,             
      font_color  
  INTO   l_font_rec
  FROM qxt_dde_font
  WHERE dde_font_id = p_dde_font_id



  RETURN l_font_rec.*

END FUNCTION

#####################################################################################
# EOF
#####################################################################################
