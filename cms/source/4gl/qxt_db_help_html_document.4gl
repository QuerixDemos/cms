##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_language_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_html_document_globals.4gl"



######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_help_html_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_html_lib_info()
  RETURN "qxt_db_help_html"
END FUNCTION 





FUNCTION init_help_html()
  #Do nothing - only for help html FILE compatibility
END FUNCTION


###########################################################
# FUNCTION set_help_id(id)
#
# Set the html help id
#
# RETURN NONE
###########################################################
FUNCTION set_help_id(id)
  DEFINE 
    id SMALLINT
  LET qxt_current_help_id = id

END FUNCTION

###########################################################
# FUNCTION get_help_url(id)
#
# get the fully qualified help url by id
#
# RETURN tl_help_target_current
###########################################################
FUNCTION get_help_url(p_url_map_id)
  DEFINE 
    p_url_map_id         SMALLINT,
    c, invalid_id        SMALLINT,
    base_url             VARCHAR(20),
    local_debug          SMALLINT,
    l_client_file_path   VARCHAR(250),
    tmp_str              VARCHAR(250),
    err_msg              VARCHAR(200),
    url                  VARCHAR(200)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_url() - p_url_map_id = ", p_url_map_id
  END IF

  CASE get_help_html_system_type()
    WHEN 0 -- URL for a website / Online
      LET url = get_help_html_web_url(p_url_map_id)

    WHEN 1  -- Help file located on the application server file system
      LET url = get_help_html_app_server_url(p_url_map_id)

    WHEN 2 --application server html file (DB-BLOB) system

      #Initialise client file path with temp folder & language directory
      LET tmp_str = get_help_html_base_dir_path( get_language_url_path(get_language(),get_help_html_filename(p_url_map_id, get_language())) )
      LET l_client_file_path = get_client_temp_path(tmp_str)

      IF local_debug THEN
        DISPLAY "get_help_url() - tmp_str = ", tmp_str
        DISPLAY "get_help_url() - p_url_map_id = ", p_url_map_id
        DISPLAY "get_help_url() - get_language() = ", get_language()
        DISPLAY "get_help_url() - get_language_dir(get_language()) = ", get_language_dir(get_language())
        DISPLAY "get_help_url() - get_language_url(get_language()) = ", get_language_url(get_language())
        DISPLAY "get_help_url() - get_help_html_filename(p_url_map_id, get_language())  = ", get_help_html_filename(p_url_map_id, get_language()) 
        DISPLAY "get_help_url() - l_client_file_path = ", l_client_file_path
      END IF

      LET url = download_blob_help_html_to_client(p_url_map_id,get_language(),l_client_file_path,FALSE)


    OTHERWISE
      LET tmp_str = "Invalid help_system_type in get_help_url()\ntl_settings.help_system_type = ", get_help_html_system_type()
      CALL fgl_winmessage("Demo Source Code Error - get_help_url()",tmp_str,"error")
      RETURN NULL
  END CASE

  RETURN url
	#RETURN "qx://help_html/uk/help_example_01.html"


END FUNCTION




{
#########################################################################################################
# FUNCTION html_help(help_url)
#
# Call the html help feature (with a fully qualified url)
#
# RETURN NONE
######################################################################################################### 
FUNCTION html_help(help_url)
  DEFINE 
    help_url    VARCHAR(100),
    inp_char    CHAR,
    local_debug SMALLINT,
    err_msg     VARCHAR(200),
    tmp_str     VARCHAR(100)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "html_help() - help_url = ", help_url
    DISPLAY "html_help() - lt_settings.help_system_type = ", get_help_html_system_type()
  END IF



  CASE get_help_html_system_type()
    WHEN 0 --application server html file (DB-BLOB) system
      IF fgl_getproperty("gui","system.file.exists",help_url) IS NULL THEN
        LET err_msg = get_str_tool(728), "\n", help_url
        CALL fgl_winmessage(get_str_tool(30), err_msg, "ERROR")
      ELSE
        IF local_debug = 1 THEN
          LET err_msg = "File " , help_url , " does exist on the Client PC"
          CALL fgl_winmessage("File does exist",err_msg, "info")
        END IF
      END IF
    WHEN 1  --ONLINE URL
      #Nothing to do - URL is enough, you don't need to download any files manually (webbrowser viewer)...
  END CASE


  #don't bother about text client... text clients don't support html widgets.

  CALL fgl_window_open("w_html_help", 1,60, get_form_path("f_qxt_html_help_g"), TRUE)

  LET tmp_str = get_str_tool(600), " - ", help_url
  CALL fgl_settitle("w_html_help",tmp_str)

  IF local_debug THEN
    DISPLAY "html_help() - help_url = ", help_url
  END IF

  DISPLAY help_url TO f_browser

  
  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 300
      BEFORE PROMPT
        CALL publish_toolbar("help_html",0) 

      ON KEY(F12,INTERRUPT,ACCEPT)
        LET int_flag = FALSE
        EXIT WHILE

      AFTER PROMPT
        CALL publish_toolbar("help_html",1) 

    END PROMPT
  END WHILE

  CALL fgl_window_close("w_html_help")    


END FUNCTION
}





######################################################
# FUNCTION download_blob_help_html_to_client(p_help_html_doc_id,p_language_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN p_client_file_path
######################################################
FUNCTION download_blob_help_html_to_client(p_help_html_doc_id,p_language_id,p_client_file_path,p_dialog)
  DEFINE 
    p_help_html_doc_id   LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id        LIKE qxt_help_html_doc.language_id,

    #l_help_html_file     OF t_help_html_file,
    local_debug         SMALLINT,
    p_client_file_path  VARCHAR(250),
    l_server_file_path  VARCHAR(250),
    p_dialog            SMALLINT,
    l_server_file_blob  BYTE

  LET local_debug = FALSE

  LET l_server_file_path = get_server_blob_temp_path(get_help_html_filename(p_help_html_doc_id,p_language_id))  

  IF local_debug THEN
    DISPLAY "download_blob_help_html_to_client() - p_help_html_doc_id=", p_help_html_doc_id
    DISPLAY "download_blob_help_html_to_client() - p_language_id=", p_language_id
    DISPLAY "download_blob_help_html_to_client() - p_client_file_path=", p_client_file_path
    DISPLAY "download_blob_help_html_to_client() - p_dialog=", p_dialog


  END IF
  #CALL get_blob_help_html(p_help_html_doc_id) RETURNING l_help_html_file.*


  #LET default_file_name = p_image_name CLIPPED, ".jpg"
  #LET local_file_name = default_file_name

  #If argument has NULL, use file dialog to choose target file name
  IF p_dialog THEN
    CALL fgl_file_dialog("save", 0, get_str(273), p_client_file_path, p_client_file_path, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file_path
  ELSE
    LET p_client_file_path = p_client_file_path
  END IF

    IF local_debug THEN
      DISPLAY "download_blob_help_html_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_help_html_to_client() - l_server_file_path=", l_server_file_path
    END IF


  IF p_client_file_path IS NOT NULL THEN
    LOCATE l_server_file_blob IN FILE l_server_file_path

    IF local_debug THEN
      DISPLAY "download_blob_help_html_to_client() - BEFORE BLOB Extract SQL Query"
      DISPLAY "download_blob_help_html_to_client() - help_html_doc_id=", p_help_html_doc_id
      DISPLAY "download_blob_help_html_to_client() - language_id=", p_language_id
    END IF

    SELECT help_html_data
      INTO l_server_file_blob
      FROM qxt_help_html_doc
      WHERE help_html_doc_id = p_help_html_doc_id
        AND language_id   = p_language_id
  
    IF local_debug THEN
      DISPLAY "download_blob_help_html_to_client() - BEFORE DOWNLOAD"
      DISPLAY "download_blob_help_html_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_help_html_to_client() - l_server_file_path=", l_server_file_path
    END IF

    LET p_client_file_path = fgl_download(l_server_file_path, p_client_file_path)

    IF local_debug THEN
      DISPLAY "download_blob_help_html_to_client() - AFTER DOWNLOAD"
      DISPLAY "download_blob_help_html_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_help_html_to_client() - l_server_file_path=", l_server_file_path
    END IF

    FREE l_server_file_blob
    #FREE l_help_html_file.help_html_blob

    RETURN p_client_file_path
  ELSE
    CALL fgl_winmessage("Error","download_blob_help_html_to_client() - p_client_file_path IS NULL","error")
    RETURN NULL
  END IF
END FUNCTION



#########################################################
# FUNCTION get_help_html_filename(help_html_doc_id,p_language_id)
#
# Get help_html_doc_id from name
#
# RETURN l_help_html_filename
#########################################################
FUNCTION get_help_html_filename(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id       LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id        LIKE qxt_help_html_doc.language_id,
    l_help_html_filename LIKE qxt_help_html_doc.help_html_filename

  SELECT help_html_filename
    INTO l_help_html_filename
    FROM qxt_help_html_doc
    WHERE help_html_doc_id = p_help_html_doc_id
      AND language_id   = p_language_id

  RETURN l_help_html_filename
END FUNCTION



#############################################################################################
# EOF
#############################################################################################
