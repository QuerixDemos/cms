##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_language_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_file_help_html_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_help_html_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_html_lib_info()
  RETURN "FILE - qxt_file_help_html"
END FUNCTION 



###########################################################
# FUNCTION set_help_id(id)
#
# Set the html help id
#
# RETURN NONE
###########################################################
FUNCTION set_help_id(id)
  DEFINE 
    id SMALLINT
  LET qxt_current_help_id = id

END FUNCTION



###########################################################
# FUNCTION get_help_url(id)
#
# get the fully qualified help url by id
#
# RETURN qxt_help_target_current
###########################################################
FUNCTION get_help_url(p_url_map_id)
  DEFINE 
    p_url_map_id         SMALLINT,
    c, invalid_id        SMALLINT,
    base_url             VARCHAR(20),
    local_debug          SMALLINT,
    l_client_file_path   VARCHAR(250),
    tmp_str,err_msg      VARCHAR(200),
    url                  VARCHAR(200)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_url() - p_url_map_id = ", p_url_map_id
  END IF


  CASE get_help_html_system_type()
    WHEN 0 -- URL for a website
      LET url = get_help_html_web_url(p_url_map_id)

      IF local_debug THEN
        DISPLAY "get_help_url() - Help System = WEBSITE URL"
      END IF

    WHEN 1  -- Help file located on the application server file system
      LET url = get_help_html_app_server_url(p_url_map_id)

      IF local_debug THEN
        DISPLAY "get_help_url() - Help System = App Server File URL"
      END IF

    OTHERWISE
      LET err_msg = "Invalid help system type\nget_help_html_system_type() = ", get_help_html_system_type() CLIPPED
      CALL fgl_winmessage("Error in get_help_url()",err_msg,"error")
      RETURN NULL
  END CASE

  IF local_debug THEN
    DISPLAY "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    DISPLAY "get_help_url() - URL Return  = ", url
    DISPLAY "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  END IF

  RETURN url


END FUNCTION

