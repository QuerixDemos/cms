##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the industry_type
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                 RETURN VALUE:
# industry_type_combo_list(cb_field_name)          populates a combo box with DB data           NONE
# get_industry_type_id(p_itype_name)               return ind_type_id based on type_name        l_type_id  (LIKE industry_type.type_id)
# get_industry_type_name(p_type_id)                return ind_type_name                         l_itype_name (LIKE industry_type.itype_name)
# industry_type_popup(p_ind_type_id)               popup selection for ind. type selection      l_industry_type_arr[i].type_id OR p_ind_type_id
# industry_type_popup_data_source                  Data Source (cursor) for                     NONE
#   (p_order_field,p_ord_dir)                      industry_type_popup()
# industry_type_create()                           create new ind. type record                  NONE
# industry_type_delete(p_type_id)                  delete ind. type record                      NONE
# industry_type_edit(p_type_id)                    edit ind. type record                        NONE
# industry_type_view_by_rec(p_industry_type_rec)   Display ind_type details on a form           NONE
# industry_type_view(p_industry_type_rec)          Display ind_type details on a form using ID  NONE
# industry_type_input(p_industry_type)             input ind. type details                      l_industry_type.*
# grid_header_ind_type_scroll()                    Populates grid header labels                 NONE
############################################################################################################


#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"

###################################################################################
# FUNCTION industry_type_combo_list(cb_field_name)
#
# RETURN NONE
#
# Populates the combo box with the industry type names
###################################################################################
FUNCTION industry_type_combo_list(cb_field_name)

  DEFINE 
    l_industry_type_arr DYNAMIC ARRAY OF t_industry_type_rec,
    rv                  LIKE industry_type.itype_name,  --return value = found company_type
    row_count           INTEGER,
    current_row         INTEGER,
    cb_field_name       VARCHAR(20),   --form field name for the country combo list field
    abort_flag         SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_industry_type_scroll2 CURSOR FOR 
    SELECT industry_type.itype_name 
      FROM industry_type

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_industry_type_scroll2 INTO l_industry_type_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_industry_type_arr[row_count].itype_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1
		If row_count > 0 THEN
			CALL l_industry_type_arr.resize(row_count)  --remove the last item which has no data
		END IF

END FUNCTION



#################################################
# FUNCTION get_industry_type_id(p_itype_name)
#
# return industry type id from name
#
# RETURN 
#################################################
FUNCTION get_industry_type_id(p_itype_name)
  DEFINE 
    p_itype_name LIKE industry_type.itype_name,
    r_type_id    LIKE industry_type.type_id

  SELECT industry_type.type_id
    INTO r_type_id
    FROM industry_type
    WHERE industry_type.itype_name = p_itype_name

  RETURN r_type_id
END FUNCTION


#################################################
# FUNCTION get_industry_type_name(p_type_id)
#################################################
FUNCTION get_industry_type_name(p_type_id)
  DEFINE 
    l_itype_name LIKE industry_type.itype_name,
    p_type_id    LIKE industry_type.type_id

  SELECT industry_type.itype_name
    INTO l_itype_name
    FROM industry_type
    WHERE industry_type.type_id = p_type_id

  RETURN l_itype_name
END FUNCTION



#################################################
# FUNCTION get_industry_type_rec(p_type_id)
#################################################
FUNCTION get_industry_type_rec(p_type_id)
  DEFINE 
    r_ind_type_rec RECORD LIKE industry_type.*,
    p_type_id    LIKE industry_type.type_id

  SELECT *
    INTO r_ind_type_rec
    FROM industry_type
    WHERE industry_type.type_id = p_type_id

  RETURN r_ind_type_rec.*
END FUNCTION


#################################################
# FUNCTION get_industry_type(p_itype_name)
#################################################
FUNCTION get_industry_type(p_type_id)
  DEFINE 
    p_type_id    LIKE industry_type.type_id,
    l_itype_name LIKE industry_type.itype_name

  SELECT industry_type.itype_name
    INTO l_itype_name
    FROM industry_type
    WHERE industry_type.type_id = p_type_id

  RETURN l_itype_name
END FUNCTION

