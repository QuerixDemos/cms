##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_language_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_print_image_globals.4gl"


###########################################################
# FUNCTION get_print_html_image_lib_info()
#
# Simply returns libray type /  name
#
# RETURN "FILE - qxt_file_print_html_image"
###########################################################
FUNCTION get_print_html_image_lib_info()
  RETURN "DB - qxt_file_print_html_image"
END FUNCTION 


###########################################################
# FUNCTION process_print_html_image_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_print_html_image_init(p_filename)
  DEFINE 
    p_filename VARCHAR(100)

  #This function must be compatible with the file toolbar library
END FUNCTION


#########################################################
# FUNCTION get_print_html_image_filename(p_image_id)
#
# get print_image name from id
#
# RETURN l_image_id
#########################################################
FUNCTION get_print_html_image_filename(p_image_id)
  DEFINE 
    p_image_id       LIKE qxt_print_image.image_id,
    l_filename       LIKE qxt_print_image.filename,
    local_debug      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_print_html_image_filename() - p_image_id = ", p_image_id
  END IF

  SELECT filename
    INTO l_filename
    FROM qxt_print_image
    WHERE image_id = p_image_id

  RETURN l_filename
END FUNCTION


######################################################
# FUNCTION download_blob_print_image_to_client(p_print_image_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION download_blob_print_image_to_client(p_image_id,p_client_file_path,p_dialog)
  DEFINE 
    p_image_id           LIKE qxt_print_image.image_id,
    local_debug          SMALLINT,
    p_client_file_path   VARCHAR(250),
    l_server_file_path   VARCHAR(250),
    p_dialog             SMALLINT,
    l_server_file_blob   BYTE

  LET local_debug = FALSE

  LET l_server_file_path = get_server_blob_temp_path(get_print_html_image_filename(p_image_id))  

  IF local_debug THEN
    DISPLAY "download_blob_print_image_to_client() - p_image_id=", p_image_id
    DISPLAY "download_blob_print_image_to_client() - p_client_file_path=", p_client_file_path
    DISPLAY "download_blob_print_image_to_client() - p_dialog=", p_dialog


  END IF
  #CALL get_blob_print_image(p_print_image_id) RETURNING l_print_image_file.*


  #LET default_file_name = p_image_name CLIPPED, ".jpg"
  #LET local_file_name = default_file_name

  #If argument has NULL, use file dialog to choose target file name
  IF p_dialog THEN
    CALL fgl_file_dialog("save", 0, get_str(273), p_client_file_path, p_client_file_path, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file_path
  ELSE
    LET p_client_file_path = p_client_file_path
  END IF

    IF local_debug THEN
      DISPLAY "download_blob_print_image_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_print_image_to_client() - l_server_file_path=", l_server_file_path
    END IF


  IF p_client_file_path IS NOT NULL THEN
    LOCATE l_server_file_blob IN FILE l_server_file_path

    IF local_debug THEN
      DISPLAY "download_blob_print_image_to_client() - BEFORE BLOB Extract SQL Query"
      DISPLAY "download_blob_print_image_to_client() - image_id=", p_image_id
    END IF

    SELECT image_data
      INTO l_server_file_blob
      FROM qxt_print_image
      WHERE image_id = p_image_id
  
    IF local_debug THEN
      DISPLAY "download_blob_print_image_to_client() - BEFORE DOWNLOAD"
      DISPLAY "download_blob_print_image_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_print_image_to_client() - l_server_file_path=", l_server_file_path
    END IF

    LET p_client_file_path = fgl_download(l_server_file_path, p_client_file_path)

    IF local_debug THEN
      DISPLAY "download_blob_print_image_to_client() - AFTER DOWNLOAD"
      DISPLAY "download_blob_print_image_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_print_image_to_client() - l_server_file_path=", l_server_file_path
    END IF

    FREE l_server_file_blob
    #FREE l_print_image_file.print_image_blob

    RETURN p_client_file_path
  ELSE
    CALL fgl_winmessage("Error","download_blob_print_image_to_client() - p_client_file_path IS NULL","error")
    RETURN NULL
  END IF
END FUNCTION






