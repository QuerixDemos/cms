##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the print html template library
#
#
#
################################################################################


################################################################################
# GLOBALS
################################################################################
GLOBALS

  DEFINE
    qxt_print_html_image DYNAMIC ARRAY OF VARCHAR(80)       --Image locations for print html

  DEFINE
    t_qxt_print_html_image_rec TYPE AS
      RECORD
        id       SMALLINT,
        filename VARCHAR(100)
      END RECORD

END GLOBALS

