##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# FUNCTION init_base_module_data()
######################################################################################################### 
FUNCTION init_base_module_data(argMainConfigFile)
  dEFINE 
    local_debug SMALLINT,
    lib_type    CHAR(2)
	DEFINE argMainConfigFile STRING
  LET local_debug = FALSE


  ######################
  # Initialise tools library
  ###################### 
	#DISPLAY "CALL init_tools_data()"
	#CALL fgl_winmessage("CALL init_tools_data()","CALL init_tools_data()","info")
  CALL init_tools_data()


  ############################
  #Connect to the database
  ############################
  IF get_db_name_tool() IS NOT NULL THEN
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL qxt_connect_db_tool()"
    END IF
    CALL qxt_connect_db_tool()
  END IF

  ############################
  # Application String
  ############################

  #File string library needs to import the string file
  IF get_string_app_lib_info() <> "DB" THEN
    CALL qxt_string_app_init(NULL)
  END IF


  ######################
  # Classic 4GL Help - Multilingual
  ###################### 
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_help_classic_cfg_import(NULL)"
  END IF

  #cfg file import
  CALL process_help_classic_cfg_import(NULL)

  #file based (non-db) library needs to import the unl file
  LET lib_type = get_help_classic_lib_info()
  IF lib_type <> "DB" THEN --File type library
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_classic_init(NULL)"
    END IF
    CALL process_help_classic_init(NULL)
  END IF



    ######################
    # Help Html 
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_html_cfg_import(NULL)"
    END IF

    CALL process_help_html_cfg_import(NULL)

    #file based (non-db) library needs to import the unl file
    LET lib_type = get_help_html_url_map_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL init_help_html_url_map(NULL)"
      END IF
      CALL init_help_html_url_map(NULL)
    END IF


    #Import toolbar definitions 
    CALL process_toolbar_init(get_cfg_path(get_toolbar_cfg_filename()),NULL) --!! second argument should be tooltip/label unl file      --Import toolbar configuration
                                                        --initialise common local & server paths

    #HTML HELP
    CALL init_help_html()


	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)

END FUNCTION






