##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# operator/login/password related functions
##################################################################################################################
#
# FUNCTIONS:                                              DESCRIPTION:                                           RETURN:
# operator_main()                                         Main operator Menu                                     NONE
# login()                                                 operator login function for cms                        rv (smallint)
# operator_popup()                                        display operator list and allow to select              l_operator_arr[i].operator_id or NULL
# operator_input(p_operator)                              enter operator details                                 p_operator.*  (p_operator RECORD LIKE operator.*)
# operator_edit(p_operator_id)                            Modify operator details                                NONE
# operator_create(p_type)                                 Create a new operator                                  sqlca.sqlerrd[2]
# operator_delete(p_operator_id)                          delete a operator                                      NONE
# operator_view(p_operator_rec)                           View a operator record using ID                        NONE
# operator_view_by_rec(p_operator_rec)                    View a operator record                                 NONE
# operator_setup()                                        menu to allow operator setup                           NONE
# change_password()                                       change the current password (current operator)         NONE
# logaway_operator()                                      not implemented - log away                             NONE
# admin_operator(p_operator)                              validate if the operator is an admin                   TRUE/FALSE
# get_operator_name(p_operator_id)                        get the operator_id from a operator.name               l_operator_name
# get_operator_id(p_operator_name)                        get the operator_id from a operator.name               l_operator_id
# get_operator_email_address(p_operator_id)               get the email address from the operator (p_operator_id)l_email_address
# get_operator_contact_id_from_operator_id(p_operator_id) get the contact_id from a perator_id                   l_cont_id
# operator_scroll_grid_header()                           Populate grid header                                   NONE
# get_operator_rec(p_operator_id)                         Get the operator record from an id                     l_operator.*
# get_current_operator_id()                               Return currently logged in operator id                 g_operator.operator_id
# get_current_operator_contact_id()                       get the contact_id from the current operator           g_operator.cont_id
# get_current_operator_email_address()                    Return currently logged in operator id                 g_operator.email_address
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



{
##################################################
# FUNCTION operator_main()
#
# operator Management menu
#
# RETURN NONE
##################################################
FUNCTION operator_main(p_operator_id)
  DEFINE p_operator_id LIKE operator.operator_id

  CALL operator_popup(p_operator_id)
END FUNCTION
}

##################################################
# FUNCTION operator_setup()
#
# operator setup menu
#
# RETURN NONE
##################################################
FUNCTION operator_setup()
  DEFINE
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

  CALL fgl_settitle(get_str(1000)) --"Operator's Logaway/password...")
  MENU "Options"
    BEFORE MENU
      CALL set_help_id(901)
      CALL publish_toolbar("operator",0)
      
		CALL ui.Interface.setImage("qx://application/icon16/transportation/operator/people_call_operator.png")
		CALL ui.Interface.setText("Operator")
	      
        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(1001)
        LET menu_str_arr2[1] = get_str(1002)
        LET menu_str_arr1[2] = get_str(1003)
        LET menu_str_arr2[2] = get_str(1004)
        LET menu_str_arr1[3] = get_str(1005)
        LET menu_str_arr2[3] = get_str(1006)
        LET menu_str_arr1[4] = get_str(1007)
        LET menu_str_arr2[4] = get_str(1008)
        LET menu_str_arr1[5] = get_str(1009)
        LET menu_str_arr2[5] = get_str(1010)

    COMMAND KEY(F901) menu_str_arr1[1] menu_str_arr2[1] HELP 801   --"Password" "Change current password" HELP 801
      CALL change_password()
    COMMAND KEY(F109)  menu_str_arr1[2] menu_str_arr2[2] HELP 802   -- "Log away" "Set inactivity period" HELP 802
      CALL logaway_operator()
    COMMAND KEY(F12,Q)  menu_str_arr1[3] menu_str_arr2[3] HELP 803   --"Quit" "Return to previous function" HELP 803
      EXIT MENU
    ON ACTION ("actExit") 
      EXIT MENU      
  END MENU

  #clean up toolbar
  CALL publish_toolbar("operator",1)


END FUNCTION
{
############################################################
#Moved to sepeate file login.4gl/ library member of libLogin
############################################################
##################################################
# FUNCTION login()
##################################################
FUNCTION login()
  DEFINE valid_login  SMALLINT
  DEFINE l_operator       OF t_operator_rec
	DEFINE windowObject WINDOW
	

    
  #display a image window in the background of w_login
  #OPEN WINDOW w_main_window AT 1 , 1 WITH 24 ROWS, 80 COLUMNS

	CALL windowObject.openWithForm("Login",get_form_path("f_login_l2"),1,1,"STYLE=\"full-screen,statusbar_lines5\"")	
 
#    IF NOT fgl_window_open("w_login", 5,5, get_form_path("f_login_2"), TRUE )  THEN
#      CALL fgl_winmessage("Error","login()\nCan not open window w_login","error")
#      RETURN FALSE 
#    END IF
	CALL populate_login_form_labels_g()    
    
#  IF fgl_fglgui() THEN  --gui
#    IF NOT fgl_window_open("w_login", 5,5, get_form_path("f_login_g"), TRUE )  THEN
#      CALL fgl_winmessage("Error","login()\nCan not open window w_login","error")
#      RETURN FALSE 
#    END IF
#
#    CALL populate_login_form_labels_g()    
#  ELSE  --Text
#    IF NOT fgl_window_open("w_login", 5,5, get_form_path("f_login_t"), TRUE )  THEN
#      CALL fgl_winmessage("Error","login()\nCan not open window w_login","error")
#      RETURN FALSE
#    END IF    CALL populate_login_form_labels_t()
#  END IF


  LET int_flag = FALSE

  #to make it easy :-)
  LET l_operator.password = "ALEX"
  LET l_operator.name = "ALEX"
  INPUT BY NAME l_operator.* WITHOUT DEFAULTS HELP 80
    BEFORE INPUT
      CALL hide_dialog_navigation_toolbar(0)
    AFTER INPUT
      IF int_flag THEN   --operator pressed cancel
        LET int_flag = FALSE
        LET valid_login = FALSE
        EXIT INPUT
      ELSE  -- operator pressed OK / Accept
        SELECT *
          INTO g_operator.*
          FROM operator
         WHERE operator.name = l_operator.name
           AND operator.password = l_operator.password
        IF sqlca.sqlcode = 0 THEN   ---if a valid operator for login
          LET valid_login = TRUE
          EXIT INPUT
        ELSE
          LET valid_login = FALSE
          CALL fgl_message_box(get_Str(1030)) --Login screen
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  #CALL fgl_window_close("w_login")
	CALL windowObject.close()
	
#  OPTIONS  --reset form line
#    FORM LINE 2
    
  RETURN valid_login

END FUNCTION
}
##################################################
# FUNCTION operator_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (Cursor) for operator_popup()
#
# RETURN NONE
##################################################
FUNCTION operator_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "operator_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT operator.operator_id, ",
                 "operator.name, ",
                 "operator.password, ",
                 "operator.type, ",
                 "contact.cont_name, ", 
                 "operator.email_address ",
                 "FROM operator, ",
                 "OUTER contact ",
                 "WHERE contact.cont_id = operator.cont_id "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
    #LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ? ?"
  END IF

  PREPARE p_operator FROM sql_stmt
  DECLARE c_operator CURSOR FOR p_operator


END FUNCTION

##################################################
# FUNCTION operator_popup(p_operator_id)
#
# displays a list of all operator (like admin) and returns the selected one
# if the operation is canceled, it will return it's original calling argument
#
# RETURN l_operator_arr[i].operator_id OR p_operator_id
##################################################
FUNCTION operator_popup(p_operator_id,p_order_field,p_accept_action)
  DEFINE 
    p_operator_id                 LIKE operator.operator_id,
    p_accept_action               SMALLINT,
    p_order_field,p_order_field2  VARCHAR(128),
    l_operator_arr                DYNAMIC ARRAY OF t_operator_rec2,
    l_arr_size                    SMALLINT,
    l_operator_id                 LIKE operator.operator_id,
    l_scroll_operator_id          LIKE operator.operator_id,
    l_scroll_operator_line_id     SMALLINT,
    i                             INTEGER,
    err_msg                       VARCHAR(240),
    local_debug                   SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET l_arr_size = 100  -- sizeof(l_operator_arr)
  #Scroll variable init
  IF p_operator_id IS NOT NULL THEN
    LET l_scroll_operator_id = p_operator_id
  ELSE
    LET l_scroll_operator_id = 1
  END IF
  LET l_scroll_operator_line_id = 1


    IF NOT fgl_window_open("w_operator_scroll", 2, 8, get_form_path("f_operator_scroll_l2"), FALSE)  THEN
      CALL fgl_winmessage("Error","operator_popup()\nCan not open window w_operator_scroll","error")
      RETURN p_operator_id
    END IF
    CALL populate_operator_scroll_form_labels_g()
		CALL ui.Interface.setImage("qx://application/icon16/transportation/operator/people_call_operator.png")
		CALL ui.Interface.setText("Operator")

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL operator_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1


    FOREACH c_operator INTO l_operator_arr[i].*
      IF l_operator_arr[i].operator_id = l_scroll_operator_id THEN
        LET l_scroll_operator_line_id = i
      END IF
      LET i = i + 1
      IF i > l_arr_size THEN
        CALL fgl_winmessage("To many rows in the table","There are too many rows in the table for this array size" || l_arr_size || "\nOnly the first " || l_arr_size || "rows will be displayed", "info")
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1


    IF NOT i THEN
      CALL fgl_window_close("w_operator_scroll")
      RETURN NULL
    END IF

		IF i > 0 THEN
			CALL l_operator_arr.resize(i)  --correct the last element of the dynamic array
		END IF
		
    #CALL set_count(i)

    LET int_flag = FALSE


    DISPLAY ARRAY l_operator_arr TO sc_operator_arr.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 910

      BEFORE DISPLAY
        CALL publish_toolbar("OperatorList",0)

        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF l_scroll_operator_line_id IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,l_scroll_operator_line_id)
        END IF

      BEFORE ROW
      	LET i = arr_curr() #set i to current row
        LET l_operator_id = l_operator_arr[i].operator_id
        LET l_scroll_operator_id = l_operator_arr[i].operator_id


      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        CASE p_accept_action
          WHEN 0
            EXIT WHILE
        
          WHEN 1  --view
            CALL operator_view(l_operator_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL operator_edit(l_operator_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL operator_delete(l_operator_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL operator_create(l_operator_id)
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","operator_popup()\noperator Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL operator_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

          OTHERWISE
            LET err_msg = "operator_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("operator_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL operator_create("U")
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL operator_edit(l_operator_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL operator_delete(l_operator_id)
        EXIT DISPLAY

      # Column Sort short cuts
     
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "password"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "type"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "email_address"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      
    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_operator_scroll")

  IF NOT int_flag THEN
    LET i = arr_curr()
    RETURN l_operator_arr[i].operator_id
  ELSE 
    LET int_flag = FALSE
    RETURN p_operator_id
  END IF

END FUNCTION



##################################################
# FUNCTION input_operator(p_operator)
#
# Input operator record details (edit/create)
#
# RETURN p_operator.*
##################################################
FUNCTION operator_input(p_operator)
  DEFINE 
    p_operator     RECORD LIKE operator.*,
    l_operator_rec OF t_operator_rec2,
    local_debug    SMALLINT

  LET local_debug = 0  --0=off 1=on

    IF NOT fgl_window_open("w_operator", 4, 4, get_form_path("f_operator_det_l2"),FALSE)  THEN
      CALL fgl_winmessage("Error","operator_input()\nCannot open window w_operator","error")
      RETURN p_operator.*
    END IF
  
    CALL populate_operator_details_form_labels_g()


  LET l_operator_rec.operator_id = p_operator.operator_id
  LET l_operator_rec.password = p_operator.password
  LET l_operator_rec.name = p_operator.name
  LET l_operator_rec.type = p_operator.type
  LET l_operator_rec.cont_name = get_contact_name(p_operator.cont_id)
  LET l_operator_rec.email_address = p_operator.email_address
	#LET l_operator_rec.session_id = p_operator.session_id
	#LET l_operator_rec.session_counter = p_operator.session_counter
	
	
  LET int_flag = FALSE
   ---allow operator to create a new operator with all fields

  INPUT BY NAME l_operator_rec.* WITHOUT DEFAULTS
    ON KEY (F10)
      IF INFIELD (cont_name) THEN
        LET p_operator.cont_id = contact_popup(p_operator.cont_id,NULL,0)
        LET l_operator_rec.cont_name = get_contact_name(p_operator.cont_id)
        DISPLAY BY NAME l_operator_rec.cont_name
        IF local_debug THEN
          DISPLAY"operator_input() - p_operator.cont_id = ", p_operator.cont_id 
          DISPLAY"operator_input() - l_operator_rec.cont_name = ", l_operator_rec.cont_name
        END IF
      END IF
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_operator_rec.name IS NULL THEN
          #Input Error  -- The operator name field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1070),"error")
          NEXT FIELD name
          CONTINUE INPUT
        END IF
        IF l_operator_rec.password IS NULL THEN
          #Input Error - The operator password field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1071),"error")
          NEXT FIELD password
          CONTINUE INPUT
        END IF
        IF l_operator_rec.type IS NULL THEN
          #Input Error - The operator type field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1072),"error")
          NEXT FIELD type
          CONTINUE INPUT
        END IF
      ELSE
        #Do you really want to abort the new/modified record entry ?
        IF NOT yes_no(get_str(80),get_str(81)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  LET p_operator.operator_id = l_operator_rec.operator_id
  LET p_operator.name = l_operator_rec.name
  LET p_operator.type = l_operator_rec.type
  LET p_operator.password = l_operator_rec.password
  LET p_operator.email_address = l_operator_rec.email_address
  #LET p_operator.session_id = l_operator_rec.session_id
  #LET p_operator.session_counter = l_operator_rec.session_counter
  
  CALL fgl_window_close("w_operator")

  RETURN p_operator.*
END FUNCTION



##################################################
# FUNCTION operator_edit(p_operator_id)
#
# Edit operator record
#
# RETURN NONE
##################################################
FUNCTION operator_edit(p_operator_id)
  DEFINE 
    p_operator_id LIKE operator.operator_id,
    l_operator RECORD LIKE operator.*

  SELECT operator.* 
    INTO l_operator.*
    FROM operator
    WHERE operator.operator_id = p_operator_id   ---checks that the operator does already exist

  CALL operator_input(l_operator.*) 
    RETURNING l_operator.*   ---let operator input new details

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF

  UPDATE operator    ---overwrite old data with new data, matching on ID
    SET name = l_operator.name,
         password = l_operator.password,
         type = l_operator.type,
         cont_id = l_operator.cont_id,
         email_address = l_operator.email_address
         #session_id = l_operator.session_id,
         #session_counter = l_operator.session_counter
      WHERE operator.operator_id = p_operator_id
END FUNCTION

##################################################
# FUNCTION operator_create(p_type)
#
# Create operator record
#
# RETURN sqlca.sqlerrd[2]
##################################################
FUNCTION operator_create(p_type)
  DEFINE 
    l_operator RECORD LIKE operator.*,
    p_type LIKE operator.type

  LET int_flag = FALSE
  LET l_operator.type = p_type
  LET l_operator.operator_id = 0  ---allows DB to control operator_id

  CALL operator_input(l_operator.*) RETURNING l_operator.*

  IF int_flag THEN
    LET int_flag = FALSE
    CALL fgl_winmessage(get_Str(1073),get_Str(1080), "info")
    RETURN NULL
  END IF   ---input new operator details to DB

  INSERT INTO operator VALUES (l_operator.*)

  CALL fgl_winmessage(get_Str(1073),get_str(1075),"info")
  RETURN sqlca.sqlerrd[2]
END FUNCTION





##################################################
# FUNCTION operator_delete(p_operator_id)
#
# Delete operator record
#
# RETURN NONE
##################################################
FUNCTION operator_delete(p_operator_id)
  DEFINE p_operator_id LIKE operator.operator_id
  # Delete - 
  IF yes_no(get_str(817),get_str(1053)) THEN
    DELETE FROM operator WHERE operator.operator_id = p_operator_id
    #"Operator with the operator ID was removed"
    LET tmp_str = get_str(1054) , p_operator_id
    CALL fgl_winmessage(get_str(817), tmp_str ,"info")
  END IF

END FUNCTION

##################################################
# FUNCTION logaway_operator()
#
# Logaway operator from system
#
# RETURN NONE
##################################################
FUNCTION logaway_operator()
  #Log Away
  CALL fgl_winmessage(get_Str(1055),get_str(1056),"info")
END FUNCTION




#############################################
# FUNCTION operator_view(p_operator_rec)
#
# View a operator record using ID
#
# RETURN NONE
#############################################
FUNCTION operator_view(p_operator_id)
  DEFINE 
    p_operator_id      LIKE operator.operator_id,
    l_operator_rec     RECORD LIKE operator.*
  
  CALL get_operator_rec(p_operator_id) RETURNING l_operator_rec.*
  CALL operator_view_by_rec(l_operator_rec.*)


END FUNCTION

#############################################
# FUNCTION operator_view_by_rec(p_operator_rec)
#
# View a operator record
#
# RETURN NONE
#############################################
FUNCTION operator_view_by_rec(p_operator_rec)
  DEFINE 
    p_operator_rec     RECORD LIKE operator.*,
    inp_char       CHAR

    IF NOT fgl_window_open("w_operator",2, 8, get_form_path("f_operator_det_l2"), FALSE)  THEN
      CALL fgl_winmessage("Error","operator_view_by_rec()\nCannot open window w_operator","error")
      RETURN NULL
    END IF

    CALL populate_operator_scroll_form_view_labels_g()


  DISPLAY BY NAME p_operator_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    ENd PROMPT
  END WHILE

  CALL fgl_window_close("w_operator_view")
END FUNCTION



