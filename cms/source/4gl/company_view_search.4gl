##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################
# GLOBALS
###################################################
GLOBALS "globals.4gl"

###################################################
# FUNCTION companySearch(pFieldSearch)
#
# This function was implemented for a general/constantly available search tool located in the top of the window
# Search string will query all major fields of the company record
#
# Return lContactId LIKE contact.cont_id
###################################################
FUNCTION companySearch(pFieldSearch)
	DEFINE pFieldSearch VARCHAR(2000)
	DEFINE lCompanyId LIKE company.comp_id
	DEFINE sqlQuery STRING
	
	LET pFieldSearch = trim(pFieldSearch)
	IF length(pFieldSearch)> 0 THEN
	IF pFieldSearch[length(pFieldSearch)] = "\n" OR pFieldSearch[length(pFieldSearch)] = "\r" THEN
		LET pFieldSearch = pFieldSearch[1,length(pFieldSearch)-1]
	END IF
	
	IF pFieldSearch[length(pFieldSearch)] != get_dbWildcard() THEN
		LET pFieldSearch = pFieldSearch, get_dbWildcard()
	END IF
	END IF
	
	LET sqlQuery = 
		"SELECT ",
#		"cont_id ",
    "company.comp_id, ",
    "company.comp_name, ",
	"company.comp_city, ",
    "company.comp_country, ",
    "contact.cont_fname, ",
	"contact.cont_lname ",


		"FROM company, contact ",
		"WHERE company.comp_main_cont = contact.cont_id " ,
		" AND ( ",
		"			company.comp_name LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr1 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr2 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr3 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_city LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_zone LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_zip LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_country LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_url LIKE \"", pFieldSearch, "\" ",
		
		
		"		OR	contact.cont_title LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_name LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_fname LIKE \"", pFieldSearch, "\" ",		
		"		OR	contact.cont_lname LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr1 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr2 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr3 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_city LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_zone LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_zip LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_country LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_phone LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_fax LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_mobile LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_email LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_dept LIKE \"", pFieldSearch, "\" ",
#		"		OR	contact.cont_org LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_position LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_ipaddr LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_notes LIKE \"", pFieldSearch, "\" ",	
		
		")"
	#WHENEVER ERROR STOP

#	DISPLAY "lContactId=",lContactId 

	LET lCompanyId = companySearchProcess(sqlQuery)
	RETURN lCompanyId 
		
	#RETURN lContactId     
END FUNCTION    


######################################

FUNCTION companySearchProcess(p_sql_stmt)
	DEFINE p_sql_stmt VARCHAR(2000)
	DEFINE comp_arr DYNAMIC ARRAY OF t_comp_arr
	DEFINE i int


  PREPARE p1x FROM p_sql_stmt
  DECLARE c1x CURSOR FOR p1x
  LET i = 1

  FOREACH c1x INTO comp_arr[i].*
    LET i = i + 1
    #IF i > 50 THEN
    #  EXIT FOREACH
    #END IF
  END FOREACH

  IF i = 1 THEN
    CALL fgl_message_box(get_str(851))  --"No matching records found")
    RETURN NULL

  ELSE
    LET i = i - 1
    IF i = 1 THEN
      RETURN comp_arr[1].comp_id
    END IF
  END IF

    IF NOT fgl_window_open("w_comp_scroll", 3,3, get_form_path("f_company_scroll_l2"), FALSE ) THEN
      CALL fgl_winmessage("Error","company_query2()\nCannot open window w_comp_scroll","error")
      RETURN NULL
    END IF 
    CALL populate_company_list_form_labels_g()

		If i > 0 THEN
			CALL comp_arr.resize(i)  --remove the last item which has no data
		END IF 
	

  DISPLAY ARRAY comp_arr TO sa_comp.*  
    ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 51


    ON KEY(F5)
      LET i = arr_curr()
      CALL company_view_short_by_id(comp_arr[i].comp_id)
  END DISPLAY


  CALL fgl_window_close("w_comp_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  ELSE
    LET i = arr_curr()
    RETURN comp_arr[i].comp_id
  END IF 
END FUNCTION

###################################################
# FUNCTION company_view_short_by_id(p_company_id)
#
# Display small/short set of company details to form (fields by name)
#
# RETURN NOTHING
###################################################
FUNCTION company_view_short_by_id(p_company_id)
  DEFINE 
    p_company_id  LIKE company.comp_id,
    l_company_short_rec OF t_company_short_rec,
    inp_char CHAR

  #only proceess if id is not null
  IF p_company_id IS NULL THEN
    RETURN
  END IF

    CALL fgl_window_open("w_comp_short",5,5,get_form_path("f_company_view_short_l2"),FALSE)
    CALL fgl_settitle(get_str(101))

  CALL populate_company_short_form_labels()

  CALL get_company_short_rec(p_company_id)
    RETURNING l_company_short_rec.*

  DISPLAY BY NAME l_company_short_rec.*


  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_comp_short")
END FUNCTION

###################################################
# FUNCTION company_query()
#
# Search Company - uses company_query()
#
# RETURN l_comp_id
###################################################
FUNCTION company_query()
  DEFINE l_comp_id LIKE company.comp_id


    IF NOT fgl_window_open("win_query_company", 5,2, get_form_path("f_company_query_l2"), FALSE)  THEN
      CALL fgl_winmessage("Error","company_query()\nCannot open window win_query_company","error") 
      RETURN NULL
    END IF

    CALL populate_company_query_form_labels_g() 



  #Toolbar
  #CALL publish_toolbar("CompanySearchDetailed",0)

  LET l_comp_id = company_query2()

  #Toolbar
  #CALL publish_toolbar("CompanySearchDetailed",1)

  CALL fgl_window_close("win_query_company")

  RETURN l_comp_id

END FUNCTION

###################################################
# FUNCTION company_query2()
#
# Search Form for company query
# RETURN comp_arr[i].comp_id   OR NULL
###################################################
FUNCTION company_query2()
  DEFINE 
    l_company_id   LIKE company.comp_id,
    l_comp_country LIKE company.comp_country,
    l_company_rec  OF t_company_rec

  DEFINE 
    where_clause CHAR(1024),
    sql_stmt CHAR(2048),
    i INTEGER,
    comp_arr DYNAMIC ARRAY OF t_comp_arr

  CLEAR FORM

  IF fgl_fglgui() THEN --gui
    DISPLAY "!" TO bt_accept  --b_ok
    DISPLAY "!" TO bt_cancel   --b_cancel
  END IF

  LET tmp_str = get_str(850) CLIPPED, " ", get_str(800) CLIPPED , "-", get_str(816) CLIPPED, "   ", get_str(808) CLIPPED,  get_str(820)
  CALL displayKeyGuide(tmp_str)  --"Enter search criteria (wildcards */% are allowed) Escape-search  Ctrl-C - Cancel")
  CALL displayUsageGuide(tmp_str)  --"Enter search criteria (wildcards */% are allowed) Escape-search  Ctrl-C - Cancel")

  LET int_flag = FALSE

  CONSTRUCT BY NAME where_clause 
    ON  company.comp_id, 
        company.comp_name, 
        company.comp_addr1,
	    company.comp_addr2, 
        company.comp_addr3, 
        company.comp_city, 
        company.comp_zone,
	    company.comp_zip, 
        company.comp_country, 	
	    company.comp_priority
    HELP 115
    BEFORE CONSTRUCT
     CALL publish_toolbar("CompanySearchDetailed",0)
     IF fgl_fglgui() THEN --gui
       CALL country_combo_list("comp_country")
     END IF

    ON KEY (F9)
      IF INFIELD (comp_country) THEN
        LET l_comp_country = country_advanced_lookup(NULL)
        IF l_comp_country IS NOT NULL THEN
          DISPLAY l_comp_country TO comp_country
        END IF
      END IF

    ON KEY (F10)
      IF INFIELD (comp_country) THEN
        LET l_comp_country = country_popup(NULL,NULL,0)
        IF l_comp_country IS NOT NULL THEN
          DISPLAY l_comp_country TO comp_country
        END IF
      END IF

   END CONSTRUCT

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  END IF

  LET sql_stmt = "SELECT ",
                   "company.comp_id, ",
                   "company.comp_name, ",
		   "company.comp_city, ",
                   "company.comp_country, ",
                   "contact.cont_name, ",
                   "operator.name ",
		 "FROM ",
                   "company, ",
                   "OUTER contact, ",
                   "OUTER company_type, ", 
                   "OUTER industry_type, ",
                   "OUTER operator ",
                 "WHERE ", 
		   "contact.cont_id = company.comp_main_cont AND ",
		   "operator.operator_id = company.acct_mgr AND ",
		   "industry_type.type_id = company.comp_industry AND ",
		   "company_type.type_id = company.comp_type AND ",
                   where_clause

        #"operator.operator_id = company.acct_mgr AND ",
        #where_clause

  PREPARE p1 FROM sql_stmt
  DECLARE c1 CURSOR FOR p1
  LET i = 1

  FOREACH c1 INTO comp_arr[i].*
    LET i = i + 1
    #IF i > 50 THEN
    #  EXIT FOREACH
    #END IF
  END FOREACH

  IF i = 1 THEN
    CALL fgl_message_box(get_str(851))  --"No matching records found")
    RETURN NULL

  ELSE
    LET i = i - 1
    IF i = 1 THEN
      RETURN comp_arr[1].comp_id
    END IF
  END IF

    IF NOT fgl_window_open("w_comp_scroll", 3,3, get_form_path("f_company_scroll_l2"), FALSE ) THEN
      CALL fgl_winmessage("Error","company_query2()\nCannot open window w_comp_scroll","error")
      RETURN NULL
    END IF 
    CALL populate_company_list_form_labels_g()

		If i > 0 THEN
			CALL comp_arr.resize(i)  --remove the last item which has no data
		END IF 
	

  DISPLAY ARRAY comp_arr TO sa_comp.*  
    ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 51


    ON KEY(F5)
      LET i = arr_curr()
      CALL company_view_short_by_id(comp_arr[i].comp_id)
  END DISPLAY


  CALL fgl_window_close("w_comp_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  ELSE
    LET i = arr_curr()
    RETURN comp_arr[i].comp_id
  END IF 
END FUNCTION

#######################################################
# FUNCTION populate_company_query_form_labels_g()
#
# Populate company query form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_company_query_form_labels_g()
  DISPLAY get_str(130) TO lbTitle
  DISPLAY get_str(101) TO dl_f1
  DISPLAY get_str(102) TO dl_f2
  DISPLAY get_str(103) TO dl_f3
  DISPLAY get_str(104) TO dl_f4
  DISPLAY get_str(105) TO dl_f5
  DISPLAY get_str(106) TO dl_f6
  DISPLAY get_str(107) TO dl_f7
  #DISPLAY get_str(108) TO dl_f8 
  #DISPLAY get_str(109) TO dl_f9
  DISPLAY get_str(110) TO dl_f10
  #DISPLAY get_str(111) TO dl_f11
  #DISPLAY get_str(112) TO dl_f12
  #DISPLAY get_str(113) TO dl_f13
  #DISPLAY get_str(114) TO dl_f14

  DISPLAY get_str(810) TO bt_accept
  DISPLAY get_str(820) TO bt_cancel

  CALL fgl_settitle(get_str(130))

END FUNCTION