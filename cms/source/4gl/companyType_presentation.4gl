##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_company_type_scroll()
####################################################
FUNCTION grid_header_company_type_scroll()
  CALL fgl_grid_header("sa_cont_type","type_id",get_str(1821) ,"right","F13")           --"Type ID:"
  CALL fgl_grid_header("sa_cont_type","ctype_name",get_str(1822),"left","F14")          --"Comp. Type Name:"
  CALL fgl_grid_header("sa_cont_type","user_def",get_str(1823),"left","F15")            --"User Def.:"
  CALL fgl_grid_header("sa_cont_type","base_priority",get_str(1824),"left","F16")       --"Priority:"
END FUNCTION


#######################################################
# FUNCTION populate_copmany_type_list_form_labels_g()
#
# Populate company type list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_copmany_type_list_form_labels_g()
  DISPLAY get_str(1820) TO lbTitle
  #DISPLAY get_str(1821) TO dl_f1
  #DISPLAY get_str(1822) TO dl_f2
  #DISPLAY get_str(1823) TO dl_f3
  #DISPLAY get_str(1824) TO dl_f4
  #DISPLAY get_str(1825) TO dl_f5
  #DISPLAY get_str(1826) TO dl_f6
  #DISPLAY get_str(1827) TO dl_f7
  #DISPLAY get_str(1828) TO dl_f8
  DISPLAY get_str(1829) TO lbInfo1


  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1820))
  CALL grid_header_company_type_scroll()


END FUNCTION

#######################################################
# FUNCTION populate_copmany_type_list_form_labels_t()
#
# Populate company type list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_copmany_type_list_form_labels_t()
  DISPLAY get_str(1820) TO lbTitle
  DISPLAY get_str(1821) TO dl_f1
  DISPLAY get_str(1822) TO dl_f2
  DISPLAY get_str(1823) TO dl_f3
  DISPLAY get_str(1824) TO dl_f4
  #DISPLAY get_str(1825) TO dl_f5
  #DISPLAY get_str(1826) TO dl_f6
  #DISPLAY get_str(1827) TO dl_f7
  #DISPLAY get_str(1828) TO dl_f8
  DISPLAY get_str(1829) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_copmany_type_form_labels_g()
#
# Populate company type form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_copmany_type_form_labels_g()
  DISPLAY get_str(1810) TO lbTitle
  DISPLAY get_str(1811) TO dl_f1
  DISPLAY get_str(1812) TO dl_f2
  #DISPLAY get_str(1813) TO dl_f3
  #DISPLAY get_str(1814) TO dl_f4
  #DISPLAY get_str(1815) TO dl_f5
  #DISPLAY get_str(1816) TO dl_f6
  #DISPLAY get_str(1817) TO dl_f7
  #DISPLAY get_str(1818) TO dl_f8
  DISPLAY get_str(1819) TO lbInfo1

  CALL fgl_settitle(get_str(1810))

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION

#######################################################
# FUNCTION populate_copmany_type_form_labels_t()
#
# Populate company type  form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_copmany_type_form_labels_t()
  DISPLAY get_str(1810) TO lbTitle
  DISPLAY get_str(1811) TO dl_f1
  DISPLAY get_str(1812) TO dl_f2
  #DISPLAY get_str(1813) TO dl_f3
  #DISPLAY get_str(1814) TO dl_f4
  #DISPLAY get_str(1815) TO dl_f5
  #DISPLAY get_str(1816) TO dl_f6
  #DISPLAY get_str(1817) TO dl_f7
  #DISPLAY get_str(1818) TO dl_f8
  DISPLAY get_str(1829) TO lbInfo1

END FUNCTION

