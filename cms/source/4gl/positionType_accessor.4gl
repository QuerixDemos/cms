##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the contact position (in comp) table   (position_type)
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                      RETURN
# position_type_combo_list(cb_field_name)          populates the position combo box                  NONE
# get_position_type(p_type_id)                     get position type name from p_type_id             l_ptype_name
# position_type_popup(p_type_id)                   displays selection list and returns the type_id   l_position_type_arr[i].type_id or p_type_id
# position_type_create(()                          creates a new position_type record                NONE
# position_type_delete(p_type_id)                  deletes a position_type record                    NONE
# position_type_edit(p_type_id)                    edit/modify a position_type record                NONE
# position_type_input(p_position_type)             input data into the position type record          l_position_type
# position_type_view(p_position_type_id)           Display record details in window-form by ID       NONE
# position_type_view_by_rec(p_position_type_rec)   Display record details in window-form by record   NONE
# grid_header_pos_type_scroll()                    Populate grid header of position type             NONE
############################################################################################################


###########################################################
# GLOBALS
###########################################################
GLOBALS "globals.4gl"


###################################################################################
# FUNCTION position_type_combo_list(cb_field_name)
#
# Populates the combo box with contact works positions
#
# RETURN NONE
###################################################################################
FUNCTION position_type_combo_list(cb_field_name)
  DEFINE 
    l_position_type_arr DYNAMIC ARRAY OF t_position_name_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    abort_flag   SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_position_type_scroll2 CURSOR FOR 
    SELECT position_type.ptype_name 
      FROM position_type

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_position_type_scroll2 INTO l_position_type_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_position_type_arr[row_count].ptype_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


###########################################################
# FUNCTION get_position_type_name(p_type_id)
#
# get position type name from p_type_id
#
# RETURN l_ptype_name
###########################################################
FUNCTION get_position_type_name(p_type_id)
  DEFINE 
    p_type_id    LIKE position_type.type_id,
    l_ptype_name LIKE position_type.ptype_name

  SELECT position_type.ptype_name
    INTO l_ptype_name
    FROM position_type
    WHERE position_type.type_id = p_type_id

  RETURN l_ptype_name
END FUNCTION


###########################################################
# FUNCTION get_position_type_rec(p_type_id)
#
# get position type record from p_type_id
#
# RETURN l_ptype_rec
###########################################################
FUNCTION get_position_type_rec(p_type_id)
  DEFINE 
    p_type_id    LIKE position_type.type_id,
    l_ptype_rec  RECORD LIKE position_type.*

  SELECT *
    INTO l_ptype_rec
    FROM position_type
    WHERE position_type.type_id = p_type_id

  RETURN l_ptype_rec.*
END FUNCTION
