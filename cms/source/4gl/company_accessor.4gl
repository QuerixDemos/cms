##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################
# GLOBALS
###################################################
GLOBALS "globals.4gl"

##########################################################################################################
# DATA Access functions
##########################################################################################################



########################################################################
# FUNCTION get_company_rec(p_company_id)
#
# get company record from company_id
#
# RETURN l_company.* (RECORD LIKE company.*)
########################################################################
FUNCTION get_company_rec(p_company_id)
  DEFINE p_company_id LIKE company.comp_id
  DEFINE l_company RECORD LIKE company.*

  SELECT company.* 
    INTO l_company.*
    FROM company
    WHERE company.comp_id = p_company_id

  RETURN l_company.*
END FUNCTION


########################################################################
# FUNCTION get_company_id(p_company)
#
# get company_id  from company name
#
# RETURN l_company.comp_id
########################################################################
FUNCTION get_company_id(p_company)
  DEFINE p_company LIKE company.comp_name
  DEFINE l_company RECORD LIKE company.*

  SELECT company.* 
    INTO l_company.*
    FROM company
    WHERE company.comp_name = p_company
  RETURN l_company.comp_id
END FUNCTION

########################################################################
# FUNCTION get_company_id_from_contact_id(p_contact_id)
#
# get company_id  from contact_id
#
# RETURN l_comp_id
########################################################################
FUNCTION get_company_id_from_contact_id(p_contact_id)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_comp_id LIKE contact.cont_org

  SELECT contact.cont_org 
    INTO l_comp_id
    FROM contact
    WHERE contact.cont_id = p_contact_id
  RETURN l_comp_id
END FUNCTION

###################################################
# FUNCTION get_company_name(p_comp_id)
#
# get the company name
# RETURN l_comp_name
###################################################

FUNCTION get_company_name(p_comp_id)
  DEFINE p_comp_id LIKE company.comp_id
  DEFINE l_comp_name LIKE company.comp_name

  SELECT company.comp_name
    INTO l_comp_name 
    FROM company
    WHERE company.comp_id = p_comp_id

  RETURN l_comp_name
END FUNCTION


###################################################
# FUNCTION get_contact_comp_name(p_contact_id)
#
# get the company name of a contact_id
# RETURN l_comp_name
###################################################

FUNCTION get_contact_comp_name(p_contact_id)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_comp_name  LIKE company.comp_name

  SELECT comp_name 
    INTO l_comp_name 
    FROM company, contact
    WHERE contact.cont_org = company.comp_id
     AND contact.cont_id = p_contact_id

  RETURN l_comp_name
END FUNCTION


###################################################
# FUNCTION get_form_company(p_company_id)
#
# ????
# RETURN l_company_rec.*
###################################################
FUNCTION get_form_company(p_company_id)
  DEFINE 
    p_company_id 	LIKE company.comp_id,
    l_company_rec OF t_company_rec

  SELECT company.comp_name, 
         company.comp_addr1, 
         company.comp_addr2,
         company.comp_addr3, 
         company.comp_city,
	 company.comp_zone, 
         company.comp_zip, 
         company.comp_country, 
         operator.name, 
	 industry_type.itype_name, 
         company.comp_priority, 
         company_type.ctype_name, 
         company.comp_url,
         contact.cont_name,
         company.comp_notes
    INTO l_company_rec.*
    FROM company, OUTER contact, OUTER industry_type, OUTER company_type, OUTER operator
   WHERE company.comp_id = p_company_id
     AND industry_type.type_id = company.comp_industry
     AND company_type.type_id = company.comp_type
     AND company.comp_main_cont = contact.cont_id
     AND company.acct_mgr = operator.operator_id

  RETURN l_company_rec.*
END FUNCTION



###################################################
# FUNCTION get_company_short_rec(p_company_id)
#
# Get the corresponding company name (from ID)
# RETURN l_company_short_rec
###################################################
FUNCTION get_company_short_rec(p_company_id)
  DEFINE
    p_company_id LIKE company.comp_id,
    l_company_short_rec OF t_company_short_rec

  SELECT 
      comp_id,
      comp_name,
      comp_addr1,
      comp_addr2,
      comp_city,
      comp_zone,
      comp_zip,
      comp_country 
    INTO l_company_short_rec
    FROM company
   WHERE company.comp_id = p_company_id

  RETURN l_company_short_rec.*

END FUNCTION

###################################################
# FUNCTION get_comp_name(p_company_id)
#
# Get the corresponding company name (from ID)
# RETURN l_comp_name
###################################################
FUNCTION get_comp_name(p_company_id)
  DEFINE p_company_id LIKE company.comp_id
  DEFINE l_comp_name LIKE company.comp_name

  SELECT comp_name 
    INTO l_comp_name
    FROM company
   WHERE company.comp_id = p_company_id

  RETURN l_comp_name
END FUNCTION


###################################################
# FUNCTION get_comp_id(p_comp_name)
#
# Get the corresponding company ID from name
#
# RETURN l_comp_id
###################################################
FUNCTION get_comp_id(p_comp_name)
  DEFINE p_comp_name LIKE company.comp_name
  DEFINE l_comp_id LIKE company.comp_id

  SELECT comp_id 
    INTO l_comp_id
    FROM company
   WHERE company.comp_name = p_comp_name

  RETURN l_comp_id
END FUNCTION



####################################################
# FUNCTION company_name_count(p_comp_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION company_name_count(p_comp_name)
  DEFINE
    p_comp_name    LIKE company.comp_name,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM company
      WHERE company.comp_name = p_comp_name

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION company_id_count(p_comp_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION company_id_count(p_comp_id)
  DEFINE
    p_comp_id      LIKE company.comp_id,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM company
      WHERE company.comp_id = p_comp_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION




