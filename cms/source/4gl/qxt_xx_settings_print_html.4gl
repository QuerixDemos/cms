##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings print_html
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_print_html_edit()
#
# Edit print_html settings (print_html.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_print_html_edit()
  DEFINE 
    l_settings_print_html_rec  OF t_qxt_settings_print_html_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_print_html",5,5,get_form_path("f_qxt_settings_print_html"),FALSE)
  CALL qxt_settings_print_html_view_form_details()

  #Get the corresponding record
  CALL get_settings_print_html_form_rec()
    RETURNING  l_settings_print_html_rec.*

  #Input
  INPUT BY NAME l_settings_print_html_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    ON ACTION("settingsSave")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_print_html_form_rec(l_settings_print_html_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_print_html_form_rec(l_settings_print_html_rec.*) --Write to memory
      CALL process_print_html_cfg_export(NULL) --Write to disk
      EXIT INPUT

    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD print_html_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_print_html_form_rec(l_settings_print_html_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  CALL publish_toolbar("settingsCfg",1)

  IF NOT int_flag THEN
    CALL copy_settings_print_html_form_rec(l_settings_print_html_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_print_html")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_print_html_view_form_details()

  CALL fgl_settitle(get_str_tool(185))

  DISPLAY get_str_tool(186) TO dl_f86 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(187) TO dl_f87 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(188) TO dl_f88 ATTRIBUTE(BLUE, BOLD)



END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_print_html_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_print_html_form_rec,
    local_debug     SMALLINT
  
  LET local_debug = FALSE

  ##################################
  # print_html.cfg Configuration file
  ##################################
  #[print_html] Section
  LET l_settings_form.print_html_output                 = qxt_settings.print_html_output                 --Help System Type (from app server file or online webserver)
  LET l_settings_form.print_html_template_unl_filename  = qxt_settings.print_html_template_unl_filename  --File name of the html help url map config file
  LET l_settings_form.print_html_image_unl_filename     = qxt_settings.print_html_image_unl_filename     --Table name of the html help url map config information

  IF local_debug THEN
    DISPLAY "get_settings_print_html_form_rec() - qxt_settings.print_html_output =",                 qxt_settings.print_html_output 
    DISPLAY "get_settings_print_html_form_rec() - qxt_settings.print_html_template_unl_filename =",  qxt_settings.print_html_template_unl_filename 
    DISPLAY "get_settings_print_html_form_rec() - qxt_settings.print_html_image_unl_filename =",     qxt_settings.print_html_image_unl_filename 

    DISPLAY "get_settings_print_html_form_rec() - l_settings_form.print_html_output =",                l_settings_form.print_html_output 
    DISPLAY "get_settings_print_html_form_rec() - l_settings_form.print_html_template_unl_filename =", l_settings_form.print_html_template_unl_filename 
    DISPLAY "get_settings_print_html_form_rec() - l_settings_form.print_html_image_unl_filename =",    l_settings_form.print_html_image_unl_filename 
  END IF

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_print_html_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_print_html_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_print_html_form_rec,
    local_debug         SMALLINT

  LET local_debug = FALSE
  
  ##################################
  # print_html.cfg Configuration file
  ##################################


  #[print_html] Section
  LET qxt_settings.print_html_output                 = p_settings_form_rec.print_html_output  --Help System Type (from app server file or online webserver)
  LET qxt_settings.print_html_template_unl_filename  = p_settings_form_rec.print_html_template_unl_filename     --File name of the html help url map config file
  LET qxt_settings.print_html_image_unl_filename     = p_settings_form_rec.print_html_image_unl_filename     --Table name of the html help url map config information

  IF local_debug THEN
    DISPLAY "copy_settings_print_html_form_rec() - qxt_settings.print_html_output =",                qxt_settings.print_html_output 
    DISPLAY "copy_settings_print_html_form_rec() - qxt_settings.print_html_template_unl_filename =", qxt_settings.print_html_template_unl_filename 
    DISPLAY "copy_settings_print_html_form_rec() - qxt_settings.print_html_url_map_tname =",         qxt_settings.print_html_image_unl_filename 

    DISPLAY "copy_settings_print_html_form_rec() - p_settings_form_rec.print_html_output =",                p_settings_form_rec.print_html_output 
    DISPLAY "copy_settings_print_html_form_rec() - p_settings_form_rec.print_html_template_unl_filename =", p_settings_form_rec.print_html_template_unl_filename 
    DISPLAY "copy_settings_print_html_form_rec() - p_settings_form_rec.print_html_url_map_tname =",         p_settings_form_rec.print_html_image_unl_filename 
  END IF


END FUNCTION



####################################################################################################
# EOF
####################################################################################################


