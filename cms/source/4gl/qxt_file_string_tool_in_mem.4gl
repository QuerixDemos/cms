##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Tool String 
#
# Strings are located in a file but copied to memory
#
################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_string_tool_in_mem_globals.4gl"



######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_string_tool_lib_info()
#
# Simply returns libray name
#
# RETURN NONE
###########################################################
FUNCTION get_string_tool_lib_info()
  RETURN "FILE - qxt_file_string_tool_in_mem"
END FUNCTION 




######################################################################################################################
# Init functions
######################################################################################################################

###########################################################
# FUNCTION init_string_tool()
#
# Initialise too string array with blanks ""
#
# RETURN NONE
###########################################################
FUNCTION qxt_string_tool_init(p_string_tool_filename)
  DEFINE
    p_string_tool_filename  VARCHAR(200),
    l_language              SMALLINT,
    l_default_language      SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "qxt_string_tool_init() - p_string_tool_filename=", p_string_tool_filename
  END IF
 
  IF p_string_tool_filename IS NULL THEN
    LET p_string_tool_filename = get_string_tool_filename()
  END IF

  IF local_debug THEN
    DISPLAY "qxt_string_tool_init() - p_string_tool_filename=", p_string_tool_filename
  END IF

  IF p_string_tool_filename IS NULL THEN
    CALL fgl_winmessage("Error in qxt_string_tool_init()","Error in qxt_string_tool_init()\nNo import string_tool_filename was specified in the function call\nand also not found in the string.cfg file","error")
    EXIT PROGRAM
  END IF

  IF local_debug THEN
    DISPLAY "qxt_string_tool_init() - p_string_tool_filename=", p_string_tool_filename
  END IF

  # Note: To ensure, that all strings exist for any language, we use the default language - usually english=1
  # The default language is specified in the config file
  LET l_language = get_language()
  LET l_default_language = get_language_default()



  IF l_language = l_default_language THEN  
    CALL process_string_tool_import_to_memory(get_unl_path(p_string_tool_filename),l_language)
  ELSE
    #If the languge is not English (our default) we will import first english and after the other language
    #This way, strings which are not available in the other languages will be displayed in English
    CALL process_string_tool_import_to_memory(get_unl_path(p_string_tool_filename),l_default_language)
    CALL process_string_tool_import_to_memory(get_unl_path(p_string_tool_filename),l_language)
  END IF

END FUNCTION


###########################################################
# FUNCTION process_string_tool_import_to_memory(p_file_name)
#
# Import string file to memory
#
# RETURN NONE
###########################################################
FUNCTION process_string_tool_import_to_memory(p_filename,p_language)

  #International strings
  DEFINE
    local_debug,str_length        SMALLINT,
    row_count                     SMALLINT,
    string_temp_rec               OF t_qxt_string_tool_rec,
    err_msg                       VARCHAR(200),
    p_filename                    VARCHAR(100),
    p_language                    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_string_tool_import_to_memory() - p_filename=", p_filename
  END IF


  #Check if the file exists
  IF NOT validate_file_server_side_exists(p_filename, NULL,TRUE) THEN
    EXIT PROGRAM
  END IF

  #Open File
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in process_string_tool_import_to_memory()\nCan not open file ", trim(p_filename)
    CALL fgl_winmessage("Error in process_string_tool_import_to_memory()", err_msg, "error")
    RETURN
  END IF

  #Set dellimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in process_string_tool_import_to_memory()\nCan not set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in process_string_tool_import_to_memory()", err_msg, "error")
    RETURN
  END IF

  IF local_debug THEN
    DISPLAY "### start ##### ",p_filename, " ########### process_string_tool_import_to_memory(" , p_filename, ")"  
  END IF


  #Loop to import file
  LET row_count = 0

  WHILE fgl_channel_read("stream",string_temp_rec.*)
    IF  string_temp_rec.string_id IS NOT NULL AND string_temp_rec.string_id > 0 THEN
      LET row_count = row_count + 1

      IF string_temp_rec.language_id = p_language THEN  --Only load strings to memory with current language_id 
        LET qxt_string_tool[string_temp_rec.string_id] = string_temp_rec.string_data

        #Import Debug Help
        IF local_debug THEN
          DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_id:                             ", string_temp_rec.string_id
          DISPLAY "process_string_import_file_to_memory() - string_temp_rec.string_data:                           ", string_temp_rec.string_data
        END IF


      END IF

    ELSE
      EXIT WHILE
    END IF

    LET qxt_string_tool_array_size = row_count

    IF local_debug THEN
      DISPLAY "row_count = ", row_count
    END IF

  END WHILE

  #Close file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in process_string_tool_import_to_memory()\nCan not close file ", trim(p_filename)
    CALL fgl_winmessage("Error in process_string_tool_import_to_memory()", err_msg, "error")
    RETURN
  END IF


  IF row_count < 1 THEN --nothing was imported i.e. file is empty
    LET err_msg = "Error in process_string_tool_import_to_memory()\nThe import file ", trim(p_filename), " is empty or does not exist!\nrow_count = ", row_count
    CALL fgl_winmessage("Error in process_string_tool_import_to_memory()",err_msg,"error")
  END IF


  IF local_debug THEN
    DISPLAY "### END of process_string_tool_import_to_memory - File Name: ", p_filename, "qxt_string_tool_array_size=", qxt_string_tool_array_size
  END IF

END FUNCTION






