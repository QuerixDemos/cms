##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the event_type table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_event_type_id(p_language_id)  get event_type id from p_language_id        l_event_type_id
# get_language_id(category_id)    Get language_id from p_event_type_id        l_language_id
# get_qxt_event_type_rec(p_event_type_id)   Get the event_type record from p_event_type_id  l_qxt_event_type.*
# event_type_popup_data_source()               Data Source (cursor) for event_type_popup              NONE
# event_type_popup                             event_type selection window                            p_event_type_id
# (p_event_type_id,p_order_field,p_accept_action)
# event_type_combo_list(cb_field_name)         Populates event_type combo list from db                NONE
# event_type_create()                          Create a new event_type record                         NULL
# event_type_edit(p_event_type_id)      Edit event_type record                                 NONE
# event_type_input(p_qxt_event_type_rec)    Input event_type details (edit/create)                 l_qxt_event_type.*
# event_type_delete(p_event_type_id)    Delete a event_type record                             NONE
# event_type_view(p_event_type_id)      View event_type record by ID in window-form            NONE
# event_type_view_by_rec(p_qxt_event_type_rec) View event_type record in window-form               NONE
# get_event_type_id_from_language_id(p_language_id)          Get the event_type_id from a file                      l_event_type_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# event_type_id_count(p_event_type_id)  tests if a record with this event_type_id already exists r_count
# copy_qxt_event_type_record_to_form_record        Copy normal event_type record data to type qxt_event_type_form_rec   l_qxt_event_type_form_rec.*
# (p_qxt_event_type_rec)
# copy_qxt_event_type_form_record_to_record        Copy type qxt_event_type_form_rec to normal event_type record data   l_event_type_rec.*
# (p_qxt_event_type_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_event_type_scroll()              Populate event_type grid headers                       NONE
# populate_event_type_form_labels_g()          Populate event_type form labels for gui                NONE
# populate_event_type_form_labels_t()          Populate event_type form labels for text               NONE
# populate_event_type_form_edit_labels_g()     Populate event_type form edit labels for gui           NONE
# populate_event_type_form_edit_labels_t()     Populate event_type form edit labels for text          NONE
# populate_event_type_form_view_labels_g()     Populate event_type form view labels for gui           NONE
# populate_event_type_form_view_labels_t()     Populate event_type form view labels for text          NONE
# populate_event_type_form_create_labels_g()   Populate event_type form create labels for gui         NONE
# populate_event_type_form_create_labels_t()   Populate event_type form create labels for text        NONE
# populate_event_type_list_form_labels_g()     Populate event_type list form labels for gui           NONE
# populate_event_type_list_form_labels_t()     Populate event_type list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"



########################################################################################################
# Data Access Functions
########################################################################################################


#########################################################
# FUNCTION get_event_type_id(p_event_type_name)
#
# get event_type_name from p_event_type_id
#
# RETURN l_event_type_id
#########################################################
FUNCTION get_event_type_id(p_event_type_name)
  DEFINE 
    p_event_type_name          LIKE qxt_tbi_event_type.event_type_name,
    l_event_type_id            LIKE qxt_tbi_event_type.event_type_id,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_event_type_name() - p_event_type_name = ", p_event_type_name
  END IF

  SELECT event_type_id
    INTO l_event_type_id
    FROM qxt_tbi_event_type
    WHERE event_type_name = p_event_type_name
    

  IF local_debug THEN
    DISPLAY "get_event_type_name() - l_event_type_id = ", l_event_type_id
  END IF

  RETURN l_event_type_id
END FUNCTION


#########################################################
# FUNCTION get_event_type_name(p_event_type_id)
#
# get event_type_name from p_event_type_id
#
# RETURN l_event_type_name
#########################################################
FUNCTION get_event_type_name(p_event_type_id)
  DEFINE 
    p_event_type_id          LIKE qxt_tbi_event_type.event_type_id,
    l_event_type_name        LIKE qxt_tbi_event_type.event_type_name,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_event_type_name() - p_event_type_id = ", p_event_type_id
  END IF

  SELECT event_type_name
    INTO l_event_type_name
    FROM qxt_tbi_event_type
    WHERE event_type_id = p_event_type_id


  IF local_debug THEN
    DISPLAY "get_event_type_name() - l_event_type_name = ", l_event_type_name
  END IF

  RETURN l_event_type_name
END FUNCTION




########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION event_type_name_combo_list(p_cb_field_name)
#
# Populates event_type_name_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION event_type_name_combo_list(p_cb_field_name)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    l_event_type_name          DYNAMIC ARRAY OF LIKE qxt_tbi_event_type.event_type_name,
    row_count                 INTEGER,
    current_row               INTEGER,
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "event_type_name_combo_list() - p_cb_field_name=", p_cb_field_name
  END IF

  DECLARE c_event_type_scroll2 CURSOR FOR 
    SELECT event_type_name
      FROM qxt_tbi_event_type
    ORDER BY event_type_name ASC 

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_event_type_scroll2 INTO l_event_type_name[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_event_type_name[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_event_type_name[row_count] CLIPPED, ")"
      DISPLAY "event_type_name_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION

{

FUNCTION get_tbi_event_type_name(p_event_type_id)
  DEFINE
    p_event_type_id  SMALLINT,
    err_msg          VARCHAR(200)

  CASE p_event_type_id
    WHEN 1
      RETURN "Key"
    WHEN 2
      RETURN "Action"
    OTHERWISE
      LET err_msg = "Function get_tbi_event_type_name() called with an invalid argument!\np_event_type_id =", p_event_type_id
      CALL fgl_winmessage("Error in get_tbi_event_type_name()",err_msg,"ERROR")
      RETURN NULL
  END CASE

END FUNCTION


FUNCTION get_tbi_event_type_id(p_event_type_name)
  DEFINE
    p_event_type_name  VARCHAR(20),
    err_msg            VARCHAR(200)

  LET p_event_type_name = downshift(p_event_type_name)

  CASE p_event_type_name
    WHEN "key"
      RETURN 1
    WHEN "action"
      RETURN 2
    OTHERWISE
      LET err_msg = "Function get_tbi_event_type_name() called with an invalid argument!\np_event_type_id =", p_event_type_name
      CALL fgl_winmessage("Error in get_tbi_event_type_name()",err_msg,"ERROR")
      RETURN NULL
  END CASE

END FUNCTION







}