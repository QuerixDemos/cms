##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

  DEFINE t_cont_rec_temp TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      cont_dept      LIKE contact.cont_dept,
      cont_position  LIKE contact.cont_position,
      comp_name      LIKE company.comp_name,
      cont_usemail   LIKE contact.cont_usemail,
      cont_usephone  LIKE contact.cont_usephone,
      cont_notes     LIKE contact.cont_notes
    END RECORD

WEB FUNCTION ws_send_contact_rec(p_contact_rec) RETURNS (contact_id)
  DEFINE contact_id LIKE contact.cont_id
  DEFINE p_contact_rec RECORD
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      #cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
  END RECORD

  RETURN 10
END FUNCTION

FUNCTION ws_get_contact_rec(p_contact_id) RETURNS (l_contact_rec)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_contact_rec RECORD
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      #cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
  END RECORD

  DATABASE cms

  CALL web_get_contact_rec(p_contact_id)
    RETURNING l_contact_rec.*

  RETURN l_contact_rec.*
END FUNCTION

#################################################
# FUNCTION web_get_contact_rec(p_contact_id)
#
# Get the contact record from an id
#
# RETURN l_contact.*
#
# Note: This function is also published as a webservice
#################################################
FUNCTION web_get_contact_rec(p_contact_id) 
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_contact RECORD
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      cont_dept      LIKE contact.cont_dept,
      cont_position  LIKE contact.cont_position,
      comp_name      LIKE company.comp_name,
      cont_usemail   LIKE contact.cont_usemail,
      cont_usephone  LIKE contact.cont_usephone,
      cont_notes     LIKE contact.cont_notes
  END RECORD

  -- LOCATE l_contact.cont_picture IN MEMORY

  SELECT contact.* 
    INTO l_contact.*
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN l_contact.*
END FUNCTION

