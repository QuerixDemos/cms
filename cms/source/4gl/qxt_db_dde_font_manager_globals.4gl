##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS


  DEFINE t_qxt_dde_font_rec TYPE AS
    RECORD
      dde_font_id           LIKE qxt_dde_font.dde_font_id,    --VARCHAR(40),   -- Arial
      font_name             LIKE qxt_dde_font.font_name,      --SMALLINT,      -- false
      font_bold             LIKE qxt_dde_font.font_bold,      --SMALLINT,      --  = 10
      font_size             LIKE qxt_dde_font.font_size,      --SMALLINT,      --  = false
      strike_through        LIKE qxt_dde_font.strike_through, --SMALLINT,      --  = false
      super_script          LIKE qxt_dde_font.super_script,   --SMALLINT,      --  = false
      lower_script          LIKE qxt_dde_font.lower_script,   --SMALLINT,      --  = false
      option_1              LIKE qxt_dde_font.option_1,       --SMALLINT,      --  = false
      option_2              LIKE qxt_dde_font.option_2,       --SMALLINT,      --  = 1
      option_3              LIKE qxt_dde_font.option_3,
      font_color            LIKE qxt_dde_font.font_color      --SMALLINT       --  = 5

    END RECORD


  DEFINE t_qxt_dde_short_rec TYPE AS
    RECORD
      #dde_font_id           LIKE qxt_dde_font.dde_font_id,    
      font_name             LIKE qxt_dde_font.font_name,    --VARCHAR(40),   -- Arial
      font_bold             LIKE qxt_dde_font.font_bold,       --SMALLINT,      -- false
      font_size             LIKE qxt_dde_font.font_size,       --SMALLINT,      --  = 10
      strike_through        LIKE qxt_dde_font.strike_through, --SMA--SMALLINT,   
      super_script          LIKE qxt_dde_font.super_script,      --  = falseLLINT,      --  = false
      lower_script          LIKE qxt_dde_font.lower_script,   --SMALLINT,      --  = false
      option_1              LIKE qxt_dde_font.option_1,      --SMALLINT,      --  = false
      option_2              LIKE qxt_dde_font.option_2,   --SMALLINT,      --  = false
      option_3              LIKE qxt_dde_font.option_3,     --SMALLINT,      --  = 1
      font_color            LIKE qxt_dde_font.font_color      --SMALLINT       --  = 5

    END RECORD


  DEFINE t_qxt_dde_font_form_rec TYPE AS
    RECORD
      dde_font_id           LIKE qxt_dde_font.dde_font_id,    --VARCHAR(40),   -- Arial
      font_name             LIKE qxt_dde_font.font_name,      --SMALLINT,      -- false
      font_bold             LIKE qxt_dde_font.font_bold,      --SMALLINT,      --  = 10
      font_size             LIKE qxt_dde_font.font_size,      --SMALLINT,      --  = false
      strike_through        LIKE qxt_dde_font.strike_through, --SMALLINT,      --  = false
      super_script          LIKE qxt_dde_font.super_script,   --SMALLINT,      --  = false
      lower_script          LIKE qxt_dde_font.lower_script,   --SMALLINT,      --  = false
      option_1              LIKE qxt_dde_font.option_1,       --SMALLINT,      --  = false
      option_2              LIKE qxt_dde_font.option_2,       --SMALLINT,      --  = 1
      option_3              LIKE qxt_dde_font.option_3,
      font_color            LIKE qxt_dde_font.font_color      --SMALLINT       --  = 5

    END RECORD

  DEFINE t_qxt_dde_font_form_rec2 TYPE AS  --used for grid
    RECORD
      dde_font_id           LIKE qxt_dde_font.dde_font_id,    --VARCHAR(40),   -- Arial
      font_name             LIKE qxt_dde_font.font_name,      --SMALLINT,      -- false
      font_bold             LIKE qxt_dde_font.font_bold,      --SMALLINT,      --  = 10
      font_size             LIKE qxt_dde_font.font_size,      --SMALLINT,      --  = false
      strike_through        LIKE qxt_dde_font.strike_through, --SMALLINT,      --  = false
      super_script          LIKE qxt_dde_font.super_script,   --SMALLINT,      --  = false
      lower_script          LIKE qxt_dde_font.lower_script,   --SMALLINT,      --  = false
      option_1              LIKE qxt_dde_font.option_1,       --SMALLINT,      --  = false
      option_2              LIKE qxt_dde_font.option_2,       --SMALLINT,      --  = 1
      option_3              LIKE qxt_dde_font.option_3,
      font_color            LIKE qxt_dde_font.font_color      --SMALLINT       --  = 5

    END RECORD



END GLOBALS

