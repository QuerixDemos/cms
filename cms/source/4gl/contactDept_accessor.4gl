##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the contact_dept  (contact department) table 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                         RETURN
# department_combo_list(cb_field_name)             populates the department combo box                   NONE
# get_contact_dept_id(p_dept_name)                 returns the corresp. id from name                    l_dept_id
# get_contact_dept_name(p_dept_id)                 Get contact dept. name from id                       l_dept_name
# get_contact_dept_rec(p_dept_id)                  get contact department record from id                l_dept_rec.*
# contact_dept_popup_data_source                   Data Source (cursor) for contact_dept_popup          NONE
#   (p_order_field,p_ord_dir)
# contact_dept_popup                               Display contact department list for                  l_contact_dept_arr[i].dept_id or p_dept_id
#   (p_dept_id,p_order_field,p_accept_action)        selection and management
# contact_dept_create()                            Create a contact dept record                         NONE
# contact_dept_delete(p_dept_id)                   Delete contact dept record                           NONE
# contact_dept_view_by_rec(p_contact_dept_rec)            Display cont. department details on form      NONE
# contact_dept_view(p_contact_dept_id)             Display cont. department details on form using id    NONE
# contact_dept_edit(p_dept_id)                     Edit cont. dep. record                               NONE
# contact_dept_input(p_contact_dept)               INPUT contact details from form (edit/create)        l_contact_dept.*
############################################################################################################


#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"

###################################################################################
# FUNCTION department_combo_list(cb_field_name)
###################################################################################
FUNCTION department_combo_list(cb_field_name)
  DEFINE l_contact_dept_arr DYNAMIC ARRAY OF 
    RECORD
     dept_name    LIKE contact_dept.dept_name  
    END RECORD,
    row_count     INTEGER,
    current_row   INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    abort_flag   SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_cont_dept_scroll2 CURSOR FOR 
    SELECT contact_dept.dept_name 
      FROM contact_dept

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_cont_dept_scroll2 INTO l_contact_dept_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_contact_dept_arr[row_count].dept_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

		If row_count > 0 THEN
			CALL l_contact_dept_arr.resize(row_count)  --remove the last item which has no data
		END IF

END FUNCTION


#############################################
# FUNCTION get_contact_dept_rec(p_dept_id)
#
# get contact department record from id
#
# RETURN l_dept_rec.*
#############################################
FUNCTION get_contact_dept_rec(p_dept_id)
  DEFINE 
    l_dept_rec  RECORD LIKE contact_dept.* ,
    p_dept_id LIKE contact_dept.dept_id

  SELECT *
    INTO l_dept_rec
    FROM contact_dept
    WHERE contact_dept.dept_id = p_dept_id

  RETURN l_dept_rec.*
END FUNCTION


#############################################
# FUNCTION get_contact_dept_id(p_dept_name)
#############################################
FUNCTION get_contact_dept_id(p_dept_name)
  DEFINE 
    p_dept_name LIKE contact_dept.dept_name,
    l_dept_id LIKE contact_dept.dept_id

  SELECT contact_dept.dept_id
    INTO l_dept_id
    FROM contact_dept
    WHERE contact_dept.dept_name = p_dept_name

  RETURN l_dept_id
END FUNCTION

#############################################
# FUNCTION get_contact_dept_name(p_dept_id)
#
# Get contact dept. name from id
#
# RETURN l_dept_name
#############################################
FUNCTION get_contact_dept_name(p_dept_id)
  DEFINE 
    p_dept_id LIKE contact_dept.dept_id,
    l_dept_name LIKE contact_dept.dept_name

  SELECT contact_dept.dept_name
    INTO l_dept_name
    FROM contact_dept
    WHERE contact_dept.dept_id = p_dept_id
  RETURN l_dept_name
END FUNCTION

