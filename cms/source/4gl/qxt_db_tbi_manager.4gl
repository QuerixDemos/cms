##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tbi table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_tbi_name(p_obj_name)  get tbi id from p_obj_name        l_tbi_name
# get_obj_name(tbi_name)    Get obj_name from p_tbi_name        l_obj_name
# get_tbi_rec(p_tbi_name)   Get the tbi record from p_tbi_name  l_tbi.*
# tbi_popup_data_source()               Data Source (cursor) for tbi_popup              NONE
# tbi_popup                             tbi selection window                            p_tbi_name
# (p_tbi_name,p_order_field,p_accept_scope)
# tbi_combo_list(cb_field_name)         Populates tbi combo list from db                NONE
# tbi_create()                          Create a new tbi record                         NULL
# tbi_edit(p_tbi_name)      Edit tbi record                                 NONE
# tbi_input(p_tbi_rec)    Input tbi details (edit/create)                 l_tbi.*
# tbi_delete(p_tbi_name)    Delete a tbi record                             NONE
# tbi_view(p_tbi_name)      View tbi record by ID in window-form            NONE
# tbi_view_by_rec(p_tbi_rec) View tbi record in window-form               NONE
# get_tbi_name_from_dir(p_dir)          Get the tbi_name from a file                      l_tbi_name
# obj_name_count(p_dir)                Tests if a record with this dir already exists               r_count
# tbi_name_count(p_tbi_name)  tests if a record with this tbi_name already exists r_count
# copy_tbi_record_to_form_record        Copy normal tbi record data to type tbi_form_rec   l_tbi_form_rec.*
# (p_tbi_rec)
# copy_tbi_form_record_to_record        Copy type tbi_form_rec to normal tbi record data   l_tbi_rec.*
# (p_tbi_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_tbi_scroll()              Populate tbi grid headers                       NONE
# populate_tbi_form_labels_g()          Populate tbi form labels for gui                NONE
# populate_tbi_form_labels_t()          Populate tbi form labels for text               NONE
# populate_tbi_form_edit_labels_g()     Populate tbi form edit labels for gui           NONE
# populate_tbi_form_edit_labels_t()     Populate tbi form edit labels for text          NONE
# populate_tbi_form_view_labels_g()     Populate tbi form view labels for gui           NONE
# populate_tbi_form_view_labels_t()     Populate tbi form view labels for text          NONE
# populate_tbi_form_create_labels_g()   Populate tbi form create labels for gui         NONE
# populate_tbi_form_create_labels_t()   Populate tbi form create labels for text        NONE
# populate_tbi_list_form_labels_g()     Populate tbi list form labels for gui           NONE
# populate_tbi_list_form_labels_t()     Populate tbi list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"





#####################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_tbi_manager_lib_info()
  RETURN "qxt_db_tbi_manager"
END FUNCTION 

######################################################################################################################
# Data Access functins
######################################################################################################################


#########################################################
# FUNCTION get_tbi_scope_id_from_menu_item_name(p_application_id,p_tbi_name)
#
# get tbi_scope_id from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_scope_id_from_menu_item_name(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id                 LIKE qxt_tbi.application_id,
    p_tbi_name         LIKE qxt_tbi.tbi_name,
    l_tbi_scope_id     LIKE qxt_tbi.tbi_scope_id,
    local_debug                      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_name() - p_tbi_name = ", p_tbi_name
    DISPLAY "get_tbi_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_tbi.tbi_scope_id
    INTO l_tbi_scope_id
    FROM qxt_tbi
    WHERE qxt_tbi.tbi_name = p_tbi_name
      AND qxt_tbi.application_id = p_application_id
  IF local_debug THEN
    DISPLAY "get_tbi_name() - l_tbi_scope_id = ", l_tbi_scope_id
  END IF

  RETURN l_tbi_scope_id
END FUNCTION



#########################################################
# FUNCTION get_tbi_event_type_id(p_application_id,p_tbi_name)
#
# get tbi_event_name from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_event_type_id(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id    LIKE qxt_tbi.application_id,
    p_tbi_name          LIKE qxt_tbi.tbi_name,
    l_event_type_id     LIKE qxt_tbi.event_type_id,
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_event_type_id() - p_tbi_name = ", p_tbi_name
    DISPLAY "get_tbi_event_type_id() - p_application_id = ", p_application_id
  END IF

  SELECT event_type_id
    INTO l_event_type_id
    FROM qxt_tbi
    WHERE qxt_tbi.tbi_name = p_tbi_name
      AND qxt_tbi.application_id = p_application_id
  IF local_debug THEN
    DISPLAY "get_tbi_event_type_id() - l_event_type_id = ", l_event_type_id
  END IF

  RETURN l_event_type_id
END FUNCTION



#########################################################
# FUNCTION get_tbi_event_name(p_application_id,p_tbi_name)
#
# get tbi_event_name from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_event_name(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id                LIKE qxt_tbi.application_id,
    p_tbi_name        LIKE qxt_tbi.tbi_name,
    l_tbi_event_name  LIKE qxt_tbi.tbi_event_name,
    local_debug                     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_name() - p_tbi_name = ", p_tbi_name
    DISPLAY "get_tbi_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_tbi.tbi_event_name
    INTO l_tbi_event_name
    FROM qxt_tbi
    WHERE qxt_tbi.tbi_name = p_tbi_name
      AND qxt_tbi.application_id = p_application_id
  IF local_debug THEN
    DISPLAY "get_tbi_name() - l_tbi_event_name = ", l_tbi_event_name
  END IF

  RETURN l_tbi_event_name
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_name(p_application_id,p_tbi_name)
#
# get obj_name from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_obj_name(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id                 LIKE qxt_tbi.application_id,
    p_tbi_name         LIKE qxt_tbi.tbi_name,
    l_tbi_obj_name  LIKE qxt_tbi.tbi_obj_name,
    local_debug                      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_name() - p_tbi_name = ", p_tbi_name
    DISPLAY "get_tbi_name() - p_application_id = ", p_application_id
  END IF

  SELECT tbi_obj_name
    INTO l_tbi_obj_name
    FROM qxt_tbi
    WHERE tbi_name = p_tbi_name
      AND application_id = p_application_id

  IF local_debug THEN
    DISPLAY "get_tbi_name() - l_tbi_obj_name = ", l_tbi_obj_name
  END IF

  RETURN l_tbi_obj_name
END FUNCTION


#########################################################
# FUNCTION get_tbi_static(p_application_id,p_tbi_name)
#
# get static from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_static(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id  LIKE qxt_tbi.application_id,
    p_tbi_name        LIKE qxt_tbi.tbi_name,
    l_tbi_static_id   LIKE qxt_tbi.tbi_static_id,
    local_debug       SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_name() - p_tbi_name = ", p_tbi_name
    DISPLAY "get_tbi_name() - p_application_id = ", p_application_id
  END IF

  SELECT tbi_static_id
    INTO l_tbi_static_id
    FROM qxt_tbi
    WHERE tbi_name = p_tbi_name
      AND application_id = p_application_id

  IF local_debug THEN
    DISPLAY "get_tbi_name() - l_tbi_static_id = ", l_tbi_static_id
  END IF

  RETURN l_tbi_static_id
END FUNCTION


#########################################################
# FUNCTION get_tbi_position(p_application_id,p_tbi_name)
#
# get category1 from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_position(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id                LIKE qxt_tbi.application_id,
    p_tbi_name        LIKE qxt_tbi.tbi_name,
    l_tbi_position    LIKE qxt_tbi.tbi_position,
    local_debug                     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_name() - p_application_id = ", p_application_id
    DISPLAY "get_tbi_name() - p_tbi_name = ", p_tbi_name

  END IF

  SELECT tbi_position
    INTO l_tbi_position
    FROM qxt_tbi
    WHERE tbi_name = p_tbi_name
      AND application_id = p_application_id

  IF local_debug THEN
    DISPLAY "get_tbi_name() - l_tbi_position = ", l_tbi_position
  END IF

  RETURN l_tbi_position
END FUNCTION



######################################################
# FUNCTION get_tbi_rec(p_application_id,p_tbi_name)
#
# Get the tbi record from p_tbi_name
#
# RETURN l_tbi.*
######################################################
FUNCTION get_tbi_rec(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id          LIKE qxt_tbi.application_id,
    p_tbi_name  LIKE qxt_tbi.tbi_name,
    l_tbi       RECORD LIKE qxt_tbi.*

  SELECT qxt_tbi.*
    INTO l_tbi.*
    FROM qxt_tbi
    WHERE tbi_name = p_tbi_name
      AND application_id = p_application_id

  RETURN l_tbi.*
END FUNCTION



#########################################################
# FUNCTION get_tbi_icon_filename(p_application_id,p_tbi_name)
#
# get obj_name from p_tbi_name
#
# RETURN l_tbi_name
#########################################################
FUNCTION get_tbi_icon_filename(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id                   LIKE qxt_tbi.application_id,
    p_tbi_name           LIKE qxt_tbi.tbi_name,
    l_tbi_icon_filename  LIKE qxt_icon.icon_filename,
    local_debug                        SMALLINT
 
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_icon_filename() - p_application_id = ", p_application_id
    DISPLAY "get_tbi_icon_filename() - p_tbi_name = ", p_tbi_name
  END IF

  SELECT qxt_tbi_obj.icon_filename
    INTO l_tbi_icon_filename
    FROM qxt_tbi,qxt_tbi_obj
    WHERE qxt_tbi.tbi_name = p_tbi_name
      AND qxt_tbi.application_id = p_application_id
      AND qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_icon_filename() - l_tbi_icon_filename = ", l_tbi_icon_filename
  END IF

  RETURN l_tbi_icon_filename
END FUNCTION


###################################################################################
# FUNCTION get_tbi_count()
#
# Finds the total number of record rows in the table
#
# RETURN NONE
###################################################################################
FUNCTION get_tbi_count()
  DEFINE
    l_count                  INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tbi

  RETURN l_count
END FUNCTION


###################################################################################
# FUNCTION get_tbi_application_id_and_name_count(p_application_id,p_tbi_name)
#
# Finds the total number of record rows in the table
#
# RETURN NONE
###################################################################################
FUNCTION get_tbi_application_id_and_name_count(p_application_id,p_tbi_name)
  DEFINE
    p_application_id         LIKE qxt_tbi.application_id,
    p_tbi_name               LIKE qxt_tbi.tbi_name,
    l_count                  INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tbi
      WHERE application_id = p_application_id
        AND tbi_name = p_tbi_name

  RETURN l_count
END FUNCTION


###################################################################################
# FUNCTION get_tbi_one_application_count(p_application_id)
#
# Finds the total number of record rows in the table
#
# RETURN NONE
###################################################################################
FUNCTION get_tbi_one_application_count(p_application_id)
  DEFINE
    p_application_id         LIKE qxt_tbi.application_id,
    l_count                  INTEGER,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_one_application_count() - p_application_id=", p_application_id
  END IF

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tbi
      WHERE application_id = p_application_id

  IF local_debug THEN
    DISPLAY "get_tbi_one_application_count() - l_count=", l_count
  END IF

  RETURN l_count
END FUNCTION



####################################################
# FUNCTION tbi_name_count(p_tbi_name)
#
# tests if a record with this tbi_name already exists
#
# RETURN r_count
####################################################
FUNCTION tbi_name_count(p_tbi_name)
  DEFINE
    p_tbi_name    LIKE qxt_tbi.tbi_name,
    r_count                     SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi
      WHERE tbi_name = p_tbi_name

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION tbi_combo_list(cb_field_name)
#
# Populates tbi combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_combo_list(cb_field_name)
  DEFINE 
    l_tbi_name_arr                 DYNAMIC ARRAY OF LIKE qxt_tbi.tbi_name,
    row_count                      INTEGER,
    current_row                    INTEGER,
    cb_field_name                  VARCHAR(35),   --form field name for the country combo list field
    abort_flag                    SMALLINT,
    tmp_str                        VARCHAR(250),
    local_debug                    SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_combo_listtbi_obj_event_name_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_tbi_name_scroll2 CURSOR FOR 
    SELECT tbi_name
      FROM qxt_tbi

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_name_scroll2 INTO l_tbi_name_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_tbi_name_arr[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tbi_name_arr[row_count] CLIPPED, ")"
      DISPLAY "tbi_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


###################################################################################
# FUNCTION tbi_combo_list(cb_field_name)
#
# Populates tbi combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_one_application_combo_list(p_application_id,cb_field_name)
  DEFINE 
    p_application_id               LIKE qxt_tbi.application_id,
    l_tbi_name_arr                 DYNAMIC ARRAY OF LIKE qxt_tbi.tbi_name,
    row_count                      INTEGER,
    current_row                    INTEGER,
    cb_field_name                  VARCHAR(35),   --form field name for the country combo list field
    abort_flag                    SMALLINT,
    tmp_str                        VARCHAR(250),
    local_debug                    SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_combo_listtbi_obj_event_name_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_tbi_name_one_application_scroll2 CURSOR FOR 
    SELECT qxt_tbi.tbi_name
      FROM qxt_tbi
      WHERE qxt_tbi.application_id = p_application_id

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_name_one_application_scroll2 INTO l_tbi_name_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_tbi_name_arr[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tbi_name_arr[row_count] CLIPPED, ")"
      DISPLAY "tbi_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION




######################################################
# FUNCTION tbi_popup_data_source()
#
# Data Source (cursor) for tbi_popup
#
# RETURN NONE
######################################################
FUNCTION tbi_popup_data_source(p_app_filter_switch,p_app_filter_id,p_name_filter_switch,p_name_filter_data,p_cat_filter_switch,p_cat_search1,p_cat_search2,p_cat_search3,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_app_filter_switch  SMALLINT,
    p_app_filter_id      LIKE qxt_application.application_id,
    p_name_filter_switch SMALLINT,
    p_name_filter_data   LIKE qxt_tbi.tbi_name,
    p_application_id     LIKE qxt_tbi.application_id,
    p_language_id        LIKE qxt_language.language_id,
    p_order_field        VARCHAR(128),
    l_sql_stmt           CHAR(2048),
    p_ord_dir            SMALLINT,
    p_ord_dir_str        VARCHAR(4),
    local_debug          SMALLINT,
    #l_language_id        LIKE qxt_language.language_id,
    p_cat_filter_switch  SMALLINT,
    p_cat_search1        LIKE qxt_icon_category.icon_category_id,
    p_cat_search2        LIKE qxt_icon_category.icon_category_id,
    p_cat_search3        LIKE qxt_icon_category.icon_category_id

  LET local_debug = FALSE

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF


  IF local_debug THEN
    DISPLAY "tbi_popup_data_source() - p_app_filter_switch=", p_app_filter_switch
    DISPLAY "tbi_popup_data_source() - p_app_filter_id=", p_app_filter_id
    DISPLAY "tbi_popup_data_source() - p_name_filter_switch=", p_name_filter_switch
    DISPLAY "tbi_popup_data_source() - p_name_filter_data=", p_name_filter_data
    DISPLAY "tbi_popup_data_source() - p_cat_filter_switch=", p_cat_filter_switch
    DISPLAY "tbi_popup_data_source() - p_cat_search1=", p_cat_search1
    DISPLAY "tbi_popup_data_source() - p_cat_search2=", p_cat_search2
    DISPLAY "tbi_popup_data_source() - p_cat_search3=", p_cat_search3
    DISPLAY "tbi_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "tbi_popup_data_source() - p_ord_dir=", p_ord_dir
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "tbi_name"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET l_sql_stmt = "SELECT ",
                 "qxt_application.application_name, ",
                 "qxt_tbi.tbi_name, ",
                 "qxt_tbi.tbi_obj_name, ",
                 "qxt_tbi_event_type.event_type_name, ",
                 "qxt_tbi.tbi_event_name, ",
                 "qxt_tbi_obj_action.tbi_action_name, ",
                 "qxt_tbi_scope.tbi_scope_name, ",
                 "qxt_tbi.tbi_position, ",
                 "qxt_tbi_static.tbi_static_name, ",
                 "qxt_tbi_obj.icon_filename, ",
                 "qxt_tbi_obj.string_id, ",
                 "qxt_tbi_tooltip.label_data, ",
                 "qxt_tbi_tooltip.string_data, ",
                 "ic_cat1.icon_category_data, ",
                 "ic_cat2.icon_category_data, ",
                 "ic_cat3.icon_category_data ",

                 "FROM qxt_tbi, ",
                   "qxt_tbi_obj, ",
                   "qxt_tbi_event_type, ",
                   "qxt_tbi_scope, ",
                   "qxt_tbi_obj_action, ",  
                   "qxt_tbi_tooltip, ",
                   "qxt_tbi_static, ",
                   "qxt_icon_category ic_cat1, ",
                   "qxt_icon_category ic_cat2, ",
                   "qxt_icon_category ic_cat3, ",
                   "qxt_application "


    LET l_sql_stmt = trim(l_sql_stmt), " ",
                 "WHERE qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name  ",
                 "AND ",
                 "qxt_tbi.event_type_id = qxt_tbi_event_type.event_type_id ",
                 "AND ",
                 "qxt_tbi.tbi_scope_id = qxt_tbi_scope.tbi_scope_id ",
                 "AND ",
                 "qxt_tbi.application_id = qxt_application.application_id ",
                 "AND ",
                 "qxt_tbi_obj.tbi_obj_action_id = qxt_tbi_obj_action.tbi_action_id ",
                 "AND ",
                 "qxt_tbi_obj.string_id = qxt_tbi_tooltip.string_id  ",
                 "AND ",
                 "qxt_tbi.tbi_static_id = qxt_tbi_static.tbi_static_id  ",
                 "AND ",
                 "ic_cat1.language_id = ",trim(get_language()), " ",
                 "AND ",
                 "ic_cat2.language_id = ",trim(get_language()), " ",
                 "AND ",
                 "ic_cat3.language_id = ",trim(get_language()), " ",
                 "AND ",
                 "qxt_tbi_obj.icon_category1_id = ic_cat1.icon_category_id ",
                 "AND ",
                 "qxt_tbi_obj.icon_category2_id = ic_cat2.icon_category_id ",
                 "AND ",
                 "qxt_tbi_obj.icon_category3_id = ic_cat3.icon_category_id ",
                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_language_id), " "

                 #"AND ",
                 #"tbi.tbi_static = tbi_static.tbi_static_id "

{
    p_app_filter_switch  SMALLINT,
    p_app_filter_data    LIKE qxt_application.application_name,
    p_name_filter_switch SMALLINT,
    p_name_filter_data   LIKE qxt_tbi.tbi_name,
}

  #Application Filter
  IF p_app_filter_switch THEN
    IF p_app_filter_id IS NOT NULL AND p_app_filter_id > 0 THEN
      LET l_sql_stmt = trim(l_sql_stmt), " ",
                   "AND ",
                   "qxt_tbi.application_id = ", trim(p_app_filter_id) , " "
    END IF
  END IF


  #Application Filter
  IF p_name_filter_switch THEN
    IF p_name_filter_data IS NOT NULL THEN
      LET l_sql_stmt = trim(l_sql_stmt), " ",
                   "AND ",
                   "qxt_tbi.tbi_name LIKE '", trim(p_name_filter_data) , "' "
    END IF
  END IF

{
  IF p_application_id > 0 THEN
    LET l_sql_stmt = trim(l_sql_stmt), " ",
                   "AND ",
                   "qxt_tbi.application_id = ", trim(p_application_id) , " "
  END IF

}
  #Category Filter
  IF p_cat_search1 IS NULL OR p_cat_search1 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search2 IS NULL OR p_cat_search2 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search3 IS NULL OR p_cat_search3 = 0 THEN
    LET p_cat_search1 = 1
  END IF


  IF p_cat_filter_switch THEN
    IF p_cat_search1 > 1 OR p_cat_search2 > 1 OR p_cat_search3 > 1 THEN
      LET l_sql_stmt = trim(l_sql_stmt), " ",
               "AND ",
                 "( "


      IF p_cat_search1 > 1 THEN
        LET l_sql_stmt = trim(l_sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search1), "' ",
                    ") "
      END IF

      IF p_cat_search1 > 1 AND p_cat_search2 > 1 THEN
        LET l_sql_stmt = trim(l_sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search2 > 1 THEN
        LET l_sql_stmt = trim(l_sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search2), "' ",
                    ") "
      END IF

      IF p_cat_search2 > 1 AND p_cat_search3 > 1 THEN
        LET l_sql_stmt = trim(l_sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search3 > 1 THEN
        LET l_sql_stmt = trim(l_sql_stmt), " ",
                    "OR ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search3), "' ",
                    ") "
      END IF                     
 
      LET l_sql_stmt = trim(l_sql_stmt), " ",
                  ") "

    END IF
  END IF




  IF p_order_field IS NOT NULL THEN
    LET l_sql_stmt = l_sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "tbi_popup_data_source()"
    DISPLAY "SQL START"
    DISPLAY l_sql_stmt[1,100]
    DISPLAY l_sql_stmt[101,200]
    DISPLAY l_sql_stmt[201,300]
    DISPLAY l_sql_stmt[301,400]
    DISPLAY l_sql_stmt[401,500]
    DISPLAY l_sql_stmt[501,600]
    DISPLAY l_sql_stmt[601,700]
    DISPLAY l_sql_stmt[701,800]
    DISPLAY l_sql_stmt[801,900]
    DISPLAY l_sql_stmt[901,1000]
    DISPLAY l_sql_stmt[1001,1100]
    DISPLAY l_sql_stmt[1101,1200]
    DISPLAY l_sql_stmt[1201,1300]
    DISPLAY l_sql_stmt[1301,1400]
    DISPLAY l_sql_stmt[1401,1500]
    DISPLAY "SQL END"

  END IF

  PREPARE p_tbi FROM l_sql_stmt
  DECLARE c_tbi CURSOR FOR p_tbi

END FUNCTION


######################################################
# FUNCTION tbi_popup(p_tbi_name,p_order_field,p_accept_scope)
#
# tbi selection window
#
# RETURN p_tbi_name
######################################################
FUNCTION tbi_popup(p_application_id,p_language_id,p_tbi_name,p_order_field,p_accept_scope)
  DEFINE 
    p_application_id        LIKE qxt_tbi.application_id,
    p_language_id           LIKE qxt_language.language_id,
    p_tbi_name              LIKE qxt_tbi.tbi_name,  --default return value if user cancels
    l_tbi_arr               DYNAMIC ARRAY OF t_qxt_tbi_form_rec2,    --RECORD LIKE qxt_tbi.*,  
    l_tbi_arr_size          SMALLINT,
    i                       INTEGER,
    p_accept_scope          SMALLINT,
    err_msg                 VARCHAR(240),
    p_order_field           VARCHAR(128), 
    p_order_field2          VARCHAR(128), 
    l_tbi_name              LIKE qxt_tbi.tbi_name,
    l_application_id        LIKE qxt_tbi.application_id,
    l_application_name      LIKE qxt_application.application_name,
    local_debug             SMALLINT,
    current_row             SMALLINT,  --to jump to last modified array line after edit/input
    l_filter_app_state      SMALLINT,
    l_filter_app_id       LIKE qxt_application.application_id,
    l_filter_app_name       LIKE qxt_application.application_name,
    l_filter_tbi_name_state SMALLINT,
    l_filter_tbi_name_data  LIKE qxt_tbi.tbi_name,
    l_filter_language_name  LIKE qxt_language.language_name,
    l_filter                SMALLINT,
    l_category_data_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_data,
        cs2  LIKE qxt_icon_category.icon_category_data,
        cs3  LIKE qxt_icon_category.icon_category_data
      END RECORD,

    l_category_id_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_id,
        cs2  LIKE qxt_icon_category.icon_category_id,
        cs3  LIKE qxt_icon_category.icon_category_id
      END RECORD


  LET local_debug = FALSE

  #Initialise some variables
  LET l_tbi_arr_size = 300 -- due to dynamic array ...sizeof(l_tbi_arr)

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  IF p_application_id IS NULL THEN
    LET p_application_id = get_current_application_id()
  END IF

  LET l_application_id = p_application_id
  LET l_application_name = get_application_name(l_application_id)
  LET l_tbi_name = p_tbi_name
  LET l_filter_app_state = TRUE  --turn application filter on
  LET l_filter_app_id = l_application_id

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

  #Init go to grid line variable
  LET current_row = 1
  #Initialise filter variables
  LET l_filter = FALSE
  
  LET l_category_id_rec.cs1 = 1
  LET l_category_id_rec.cs2 = 1
  LET l_category_id_rec.cs3 = 1

  LET l_category_data_rec.cs1 = ""
  LET l_category_data_rec.cs2 = ""
  LET l_category_data_rec.cs3 = ""

  IF local_debug THEN
    DISPLAY "tbi_popup() - p_application_id=",p_application_id
    DISPLAY "tbi_popup() - p_tbi_name=",p_tbi_name
    DISPLAY "tbi_popup() - p_order_field=",p_order_field
    DISPLAY "tbi_popup() - p_accept_scope=",p_accept_scope
  END IF


	CALL fgl_window_open("w_tbi_scroll", 2, 2, get_form_path("f_qxt_tbi_scroll_l2"),FALSE) 
	CALL populate_tbi_list_form_labels_g()

  IF p_application_id IS NOT NULL THEN
    DISPLAY get_application_name(p_application_id) TO filter_application_name
  END IF

  IF p_language_id IS NOT NULL THEN
    DISPLAY get_language_name(p_language_id) TO filter_lang_n
  END IF

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0


  #This is about filters in case application_id was not set correctly
  IF p_application_id = 0 OR p_application_id IS NULL THEN
    LET p_application_id = 0  --in case it is NULL
    SELECT COUNT(*)
      INTO i
      FROM qxt_tbi
  ELSE
    SELECT COUNT(*)
      INTO i
      FROM qxt_tbi
        WHERE qxt_tbi.application_id = p_application_id
  END IF

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL tbi_create(NULL,NULL)
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL tbi_popup_data_source(l_filter_app_state,l_filter_app_id,l_filter_tbi_name_state,l_filter_tbi_name_data,l_filter,l_category_id_rec.cs1,l_category_id_rec.cs2,l_category_id_rec.cs3,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_tbi INTO l_tbi_arr[i].*

      #Find current screen line position - go to grid line
      IF l_tbi_arr[i].tbi_name  = l_tbi_name AND l_tbi_arr[i].application_name  = l_application_name THEN
        LET current_row = i
      END IF


      IF local_debug THEN
        DISPLAY "tbi_popup() - i=",i
        DISPLAY "tbi_popup() - l_filter_app_state=",               l_filter_app_state
        DISPLAY "tbi_popup() - l_filter_app_id=",                  l_filter_app_id
        DISPLAY "tbi_popup() - l_filter_tbi_name_state=",          l_filter_tbi_name_state
        DISPLAY "tbi_popup() - l_filter_tbi_name_data=",           l_filter_tbi_name_data
        DISPLAY "tbi_popup() - l_tbi_arr[i].application_name=",    l_tbi_arr[i].application_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_name=",            l_tbi_arr[i].tbi_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_obj_name=",        l_tbi_arr[i].tbi_obj_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_event_name=",      l_tbi_arr[i].tbi_event_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].icon_filename=",       l_tbi_arr[i].icon_filename
        
        DISPLAY "tbi_popup() - l_tbi_arr[i].label_data=",          l_tbi_arr[i].label_data
        DISPLAY "tbi_popup() - l_tbi_arr[i].string_data=",         l_tbi_arr[i].string_data
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_scope_name=",      l_tbi_arr[i].tbi_scope_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_position=",        l_tbi_arr[i].tbi_position
        DISPLAY "tbi_popup() - l_tbi_arr[i].tbi_static_name=",     l_tbi_arr[i].tbi_static_name
        DISPLAY "tbi_popup() - l_tbi_arr[i].icon_category1_data=", l_tbi_arr[i].icon_category1_data
        DISPLAY "tbi_popup() - l_tbi_arr[i].icon_category2_data=", l_tbi_arr[i].icon_category2_data
        DISPLAY "tbi_popup() - l_tbi_arr[i].icon_category3_data=", l_tbi_arr[i].icon_category3_data

      END IF

      LET i = i + 1

      #Array is limited to 500 elements
      IF i > l_tbi_arr_size THEN
        ERROR "Array is limited to ", l_tbi_arr_size, " entries"
        EXIT FOREACH
      END IF

    END FOREACH
    
    LET i = i - 1

		If i > 0 THEN
			CALL l_tbi_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		
    DISPLAY i TO found_records


    IF p_application_id > 0 THEN
      DISPLAY get_tbi_one_application_count(p_application_id) TO total_records
    ELSE
      DISPLAY get_tbi_count() TO total_records
    END IF

    IF local_debug THEN
      DISPLAY "tbi_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_tbi_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "tbi_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_tbi_arr TO sc_tbi.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE DISPLAY
        CALL publish_toolbar("ToolbarTbiList",0)

        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF current_row IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,current_row)
        END IF

      BEFORE ROW
        LET i = arr_curr()
        LET l_tbi_name = l_tbi_arr[i].tbi_name
        LET l_application_id         = get_application_id(l_tbi_arr[i].application_name)

        CALL icon_preview(l_tbi_arr[i].icon_filename)

        DISPLAY l_tbi_name                             TO pv_name
        DISPLAY get_application_name(l_application_id) TO pv_app_name
        DISPLAY l_tbi_arr[i].tbi_obj_name              TO pv_obj_name
        DISPLAY l_tbi_arr[i].event_type_name           TO pv_event_type_name
        DISPLAY l_tbi_arr[i].tbi_event_name            TO pv_event
        DISPLAY l_tbi_arr[i].tbi_scope_name            TO pv_scope
        DISPLAY l_tbi_arr[i].tbi_action_name           TO pv_action
        DISPLAY l_tbi_arr[i].tbi_position              TO pv_position
        DISPLAY l_tbi_arr[i].tbi_static_name           TO pv_static_name
        DISPLAY l_tbi_arr[i].icon_filename             TO pv_icon
        DISPLAY l_tbi_arr[i].label_data                TO pv_label
        DISPLAY l_tbi_arr[i].string_data               TO pv_tooltip
        DISPLAY l_tbi_arr[i].icon_category1_data       TO pv_cat1
        DISPLAY l_tbi_arr[i].icon_category2_data       TO pv_cat2
        DISPLAY l_tbi_arr[i].icon_category3_data       TO pv_cat3


      #KEY EVENTS

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        #LET i = arr_curr()
        #LET l_tbi_name = l_tbi_arr[i].tbi_name

        CASE p_accept_scope
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tbi_view(l_application_id,l_tbi_name)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL tbi_edit(l_application_id,l_tbi_name)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL tbi_delete(l_application_id,l_tbi_name)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL tbi_create(NULL,NULL)
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tbi_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "tbi_popup(p_tbi_name,p_order_field, p_accept_scope = " ,p_accept_scope
            CALL fgl_winmessage("tbi_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL tbi_create(l_application_id,l_tbi_name)  --tbi_create(p_application_id,p_tbi_name)
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL tbi_edit(l_application_id,l_tbi_name)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL tbi_delete(l_application_id,l_tbi_name)
        EXIT DISPLAY

      ON KEY (F7) -- duplicate
        CALL tbi_duplicate(l_application_id,l_tbi_name)
        EXIT DISPLAY


      #Grid Column Sort
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "application_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
    
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_obj_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "event_type_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_event_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_action_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_scope_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F20)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_position"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F21)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_static_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F22)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      

      ON KEY(F23)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F24)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F25)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat1.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F26)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat2.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F27)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat3.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON ACTION("filterIconCategoryOn")
        LET l_filter = TRUE
        EXIT DISPLAY 

      ON ACTION("filterIconCategoryOff")
        LET l_filter = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterIconCategory")  --Specify filter criteria
        INPUT BY NAME l_category_data_rec.* WITHOUT DEFAULTS HELP 1
          ON ACTION("setFilterIconCategory","filterIconCategoryOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
          LET l_filter = FALSE
        ELSE
          LET l_filter = TRUE
          LET l_category_id_rec.cs1 = get_icon_category_id(l_category_data_rec.cs1,get_language())
          LET l_category_id_rec.cs2 = get_icon_category_id(l_category_data_rec.cs2,get_language())
          LET l_category_id_rec.cs3 = get_icon_category_id(l_category_data_rec.cs3,get_language())

          IF local_debug THEN
            DISPLAY "l_category_id_rec.cs1=", l_category_id_rec.cs1
            DISPLAY "l_category_id_rec.cs2=", l_category_id_rec.cs2
            DISPLAY "l_category_id_rec.cs3=", l_category_id_rec.cs3
          END IF

          #Some cleaning up in case the user selects i.e. the first two empty and the last combo has an entry (top to bottom)
          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs2 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs2
            LET l_category_id_rec.cs2 = 1
          END IF

          IF l_category_id_rec.cs2 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs2 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          LET l_category_data_rec.cs1 = get_icon_category_data(l_category_id_rec.cs1,get_language())
          LET l_category_data_rec.cs2 = get_icon_category_data(l_category_id_rec.cs2,get_language())
          LET l_category_data_rec.cs3 = get_icon_category_data(l_category_id_rec.cs3,get_language())

          DISPLAY BY NAME l_category_data_rec.cs1
          DISPLAY BY NAME l_category_data_rec.cs2
          DISPLAY BY NAME l_category_data_rec.cs3

          EXIT DISPLAY
        END IF


      #Application Filter
      ON ACTION("filterApplicationOn")
        LET l_filter_app_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterApplicationOff")
        LET l_filter_app_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterApplicationName")  --Specify filter criteria
        INPUT l_filter_app_name WITHOUT DEFAULTS FROM filter_application_name HELP 1
          ON ACTION("setFilterApplicationName","filterApplicationOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET l_filter_app_id = get_application_id(l_filter_app_name)
          LET l_filter_app_state = TRUE
          EXIT DISPLAY
        END IF


      #Tbi Name Filter
      ON ACTION("filterTbiNameOn")
        LET l_filter_tbi_name_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterTbiNameOff")
        LET l_filter_tbi_name_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterTbiName")  --Specify filter criteria
        INPUT l_filter_tbi_name_data WITHOUT DEFAULTS FROM filter_tbi_name HELP 1
          ON ACTION("setFilterTbiName","filterTbiNameOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
          LET l_filter_tbi_name_state = FALSE
        ELSE
          LET l_filter_tbi_name_state = TRUE
          EXIT DISPLAY
        END IF


      ON ACTION("setFilterLanguageName")  --Specify filter criteria
        INPUT l_filter_language_name WITHOUT DEFAULTS FROM filter_lang_n HELP 1
          ON ACTION("setFilterLanguageName")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE

        ELSE
          LET p_language_id = get_language_id(l_filter_language_name)
          EXIT DISPLAY
        END IF

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_tbi_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_application_id, p_tbi_name 
  ELSE 
    RETURN l_application_id, l_tbi_name 
  END IF

END FUNCTION



###################################################################################################################################
# Edit/Create/Input Functions
###################################################################################################################################


######################################################
# FUNCTION tbi_create()
#
# Create a new tbi record
#
# RETURN NULL
######################################################
FUNCTION tbi_create(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id         LIKE qxt_tbi.application_id,
    p_tbi_name               LIKE qxt_tbi.tbi_name,
    l_tbi_rec    RECORD LIKE qxt_tbi.*,
    local_debug  SMALLINT

  LET local_debug = FALSE

	CALL fgl_window_open("w_tbi", 3, 3, get_form_path("f_qxt_tbi_det_l2"), FALSE) 
	CALL populate_tbi_form_create_labels_g()

  LET int_flag = FALSE
  
  #Initialise some variables
  IF p_application_id IS NULL THEN
    LET l_tbi_rec.application_id = 1
  ELSE
    LET l_tbi_rec.application_id = p_application_id
  END IF

  IF p_tbi_name IS NOT NULL THEN
    LET l_tbi_rec.tbi_name = p_tbi_name
  END IF

  LET l_tbi_rec.tbi_scope_id = 1
  LET l_tbi_rec.tbi_static_id = 0
  LET l_tbi_rec.tbi_position = 1

  # CALL the INPUT
  CALL tbi_input(TRUE,l_tbi_rec.*)
    RETURNING l_tbi_rec.*

  CALL fgl_window_close("w_tbi")

  IF local_debug THEN
    DISPLAY "tbi_create() - l_tbi_rec.application_id=",    l_tbi_rec.application_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_name=",          l_tbi_rec.tbi_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_obj_name=",      l_tbi_rec.tbi_obj_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_event_name=",    l_tbi_rec.tbi_event_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_scope_id=",      l_tbi_rec.tbi_scope_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_position=",      l_tbi_rec.tbi_position
    DISPLAY "tbi_create() - l_tbi_rec.tbi_static_id=",     l_tbi_rec.tbi_static_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category1_id=", l_tbi_rec.icon_category1_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category2_id=", l_tbi_rec.icon_category2_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category3_id=", l_tbi_rec.icon_category3_id

  END IF


  IF NOT int_flag THEN
    INSERT INTO qxt_tbi VALUES (
      l_tbi_rec.application_id,
      l_tbi_rec.tbi_name,
      l_tbi_rec.tbi_obj_name,
      l_tbi_rec.event_type_id,
      l_tbi_rec.tbi_event_name,
      l_tbi_rec.tbi_scope_id,
      l_tbi_rec.tbi_position,
      l_tbi_rec.tbi_static_id,
      l_tbi_rec.icon_category1_id,
      l_tbi_rec.icon_category2_id,
      l_tbi_rec.icon_category3_id
                                       )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_tbi_rec.application_id, l_tbi_rec.tbi_name
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL
  END IF

END FUNCTION



#################################################
# FUNCTION tbi_edit(p_tbi_name)
#
# Edit tbi record
#
# RETURN NONE
#################################################
FUNCTION tbi_edit(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id         LIKE qxt_tbi.application_id,
    p_tbi_name               LIKE qxt_tbi.tbi_name,
    l_key1_application_id    LIKE qxt_tbi.application_id,
    l_key2_tbi_name          LIKE qxt_tbi.tbi_name,
    l_tbi_rec                RECORD LIKE qxt_tbi.*,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_edit() - Function Entry Point"
    DISPLAY "tbi_edit() - p_tbi_name=", p_tbi_name
    DISPLAY "tbi_edit() - p_application_id=", p_application_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_application_id = p_application_id
  LET l_key2_tbi_name = p_tbi_name


  #Get the record (by id)
  CALL get_tbi_rec(p_application_id,p_tbi_name) RETURNING l_tbi_rec.*


	CALL fgl_window_open("w_tbi", 3, 3, get_form_path("f_qxt_tbi_det_l2"), FALSE) 
	CALL populate_tbi_form_edit_labels_g()


  #Call the INPUT
  CALL tbi_input(FALSE,l_tbi_rec.*) 
    RETURNING l_tbi_rec.*

  IF local_debug THEN
    DISPLAY "tbi_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  CALL fgl_window_close("w_tbi")


  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
    DISPLAY "tbi_create() - l_tbi_rec.application_id=",    l_tbi_rec.application_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_name=",          l_tbi_rec.tbi_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_obj_name=",      l_tbi_rec.tbi_obj_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_event_name=",    l_tbi_rec.tbi_event_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_scope_id=",      l_tbi_rec.tbi_scope_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_position=",      l_tbi_rec.tbi_position
    DISPLAY "tbi_create() - l_tbi_rec.tbi_static_id=",     l_tbi_rec.tbi_static_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category1_id=", l_tbi_rec.icon_category1_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category2_id=", l_tbi_rec.icon_category2_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category3_id=", l_tbi_rec.icon_category3_id

    END IF

    IF (l_tbi_rec.application_id <> l_key1_application_id) OR (NOT l_tbi_rec.tbi_name = l_key2_tbi_name) THEN
      #Create a new entry
      INSERT INTO qxt_tbi VALUES (
        l_tbi_rec.application_id,
        l_tbi_rec.tbi_name,
        l_tbi_rec.tbi_obj_name,
        l_tbi_rec.event_type_id,
        l_tbi_rec.tbi_event_name,
        l_tbi_rec.tbi_scope_id,
        l_tbi_rec.tbi_position,
        l_tbi_rec.tbi_static_id,
        l_tbi_rec.icon_category1_id,
        l_tbi_rec.icon_category2_id,
        l_tbi_rec.icon_category3_id
                                       )

      #Update all child table entries with the new key information
      UPDATE qxt_tb
        SET tbi_name       = l_tbi_rec.tbi_name,
            application_id = l_tbi_rec.application_id
      WHERE application_id = l_key1_application_id
        AND tbi_name       = l_key2_tbi_name

      #delete the original row
      DELETE FROM qxt_tbi
        WHERE application_id = l_key1_application_id
          AND tbi_name       = l_key2_tbi_name

    ELSE

      UPDATE qxt_tbi
        SET 
          application_id =     l_tbi_rec.application_id,
          tbi_name =           l_tbi_rec.tbi_name,
          tbi_obj_name =       l_tbi_rec.tbi_obj_name,
          event_type_id =      l_tbi_rec.event_type_id,
          tbi_event_name =     l_tbi_rec.tbi_event_name,
          tbi_scope_id =       l_tbi_rec.tbi_scope_id,
          tbi_position =       l_tbi_rec.tbi_position,
          tbi_static_id =      l_tbi_rec.tbi_static_id,
          icon_category1_id =  l_tbi_rec.icon_category1_id,
          icon_category2_id =  l_tbi_rec.icon_category2_id,
          icon_category3_id =  l_tbi_rec.icon_category3_id

        WHERE qxt_tbi.application_id = l_key1_application_id
         AND qxt_tbi.tbi_name = l_key2_tbi_name

    END IF

    IF local_debug THEN
      DISPLAY "tbi_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(900),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(900),NULL)
      RETURN l_tbi_rec.application_id, l_tbi_rec.tbi_name
    END IF

  ELSE  --User pressed abort
    #CALL tl_msg_input_abort(get_str_tool(900),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL
  END IF

END FUNCTION




#################################################
# FUNCTION tbi_duplicate(p_tbi_name)
#
# Clone tbi record
#
# RETURN NONE
#################################################
FUNCTION tbi_duplicate(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id         LIKE qxt_tbi.application_id,
    p_tbi_name               LIKE qxt_tbi.tbi_name,
    l_key1_application_id    LIKE qxt_tbi.application_id,
    l_key2_tbi_name          LIKE qxt_tbi.tbi_name,
    l_tbi_rec                RECORD LIKE qxt_tbi.*,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_edit() - Function Entry Point"
    DISPLAY "tbi_edit() - p_tbi_name=", p_tbi_name
    DISPLAY "tbi_edit() - p_application_id=", p_application_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_application_id = p_application_id
  LET l_key2_tbi_name = p_tbi_name


  #Get the record (by id)
  CALL get_tbi_rec(p_application_id,p_tbi_name) RETURNING l_tbi_rec.*


	CALL fgl_window_open("w_tbi", 3, 3, get_form_path("f_qxt_tbi_det_l2"), FALSE) 
	CALL populate_tbi_form_edit_labels_g()


  #Call the INPUT
  CALL tbi_input(FALSE,l_tbi_rec.*) 
    RETURNING l_tbi_rec.*

  IF local_debug THEN
    DISPLAY "tbi_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  CALL fgl_window_close("w_tbi")


  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
    DISPLAY "tbi_create() - l_tbi_rec.application_id=",    l_tbi_rec.application_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_name=",          l_tbi_rec.tbi_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_obj_name=",      l_tbi_rec.tbi_obj_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_event_name=",    l_tbi_rec.tbi_event_name
    DISPLAY "tbi_create() - l_tbi_rec.tbi_scope_id=",      l_tbi_rec.tbi_scope_id
    DISPLAY "tbi_create() - l_tbi_rec.tbi_position=",      l_tbi_rec.tbi_position
    DISPLAY "tbi_create() - l_tbi_rec.tbi_static_id=",     l_tbi_rec.tbi_static_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category1_id=", l_tbi_rec.icon_category1_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category2_id=", l_tbi_rec.icon_category2_id
    DISPLAY "tbi_create() - l_tbi_rec.icon_category3_id=", l_tbi_rec.icon_category3_id

    END IF

    #Create a new entry
    INSERT INTO qxt_tbi VALUES (
        l_tbi_rec.application_id,
        l_tbi_rec.tbi_name,
        l_tbi_rec.tbi_obj_name,
        l_tbi_rec.event_type_id,
        l_tbi_rec.tbi_event_name,
        l_tbi_rec.tbi_scope_id,
        l_tbi_rec.tbi_position,
        l_tbi_rec.tbi_static_id,
        l_tbi_rec.icon_category1_id,
        l_tbi_rec.icon_category2_id,
        l_tbi_rec.icon_category3_id
                                       )

    IF local_debug THEN
      DISPLAY "tbi_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(900),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(900),NULL)
      RETURN l_tbi_rec.application_id, l_tbi_rec.tbi_name
    END IF

  ELSE  --User pressed abort
    #CALL tl_msg_input_abort(get_str_tool(900),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL
  END IF

END FUNCTION




#################################################
# FUNCTION tbi_input(p_tbi_rec)
#
# Input tbi details (edit/create)
#
# RETURN l_tbi.*
#################################################
FUNCTION tbi_input(p_new_flag,p_tbi_rec)
  DEFINE 
    p_new_flag                SMALLINT,
    p_tbi_rec                 RECORD LIKE qxt_tbi.*,
    l_tbi_form_rec            OF t_qxt_tbi_form_rec,
    l_orignal_tbi_name        LIKE qxt_tbi.tbi_name,
    l_orignal_application_id  LIKE qxt_tbi.application_id,
    l_orignal_menu_item_name  LIKE qxt_tbi.tbi_name,
    local_debug               SMALLINT,
    tmp_str                   VARCHAR(250),

    tmp_tbi_event_name        LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    tmp_icon_category_id      LIKE qxt_icon_category.icon_category_id,
    tmp_tbi_obj_name          LIKE qxt_tbi_obj.tbi_obj_name,
    tmp_tbi_name              LIKE qxt_tbi.tbi_name


    #tmp_static                              LIKE tbi_tooltip.static,
    #tmp_language_id                            LIKE tbi_tooltip.language_id,
    #tmp_obj_name                          LIKE qxt_icon.obj_name,

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_tbi_name = p_tbi_rec.tbi_name
  LET l_orignal_application_id = p_tbi_rec.application_id

  #copy record data to form_record format record
  CALL copy_tbi_record_to_form_record(p_tbi_rec.*) RETURNING l_tbi_form_rec.*


  INPUT BY NAME l_tbi_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      CALL refresh_det_form_field_contents(l_tbi_form_rec.*)
      NEXT FIELD tbi_obj_name
{
      #Display string data / tooltip
      DISPLAY get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name) TO string_id
      DISPLAY get_tbi_tooltip_data(get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name),get_language()) TO string_data
      #Icon file name and preview update
      DISPLAY get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name) TO icon_filename
      CALL icon_preview(get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name))
      #Action Name - just a view display field
      DISPLAY get_tbi_action_name_from_tbi_obj_name(l_tbi_form_rec.tbi_obj_name) TO tbi_action_name
      NEXT FIELD tbi_obj_name
}
# fgl_dialog_getfieldname() is broken
#
#      #Refresh combo list boxes
#      CALL icon_category_data_combo_list("icon_category1_data",get_language())
#      CALL icon_category_data_combo_list("icon_category2_data",get_language())
#      CALL icon_category_data_combo_list("icon_category3_data",get_language())


#    #Find out, if the cursor is in a category field and update it correspondingly
#    CASE fgl_dialog_getfieldname()

#      WHEN "icon_category1_name"
#        LET tmp_icon_category_id =  icon_category_popup(get_icon_category_id()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category1_data

#      WHEN "icon_category2_name"
#        LET tmp_icon_category_id =  icon_category_popup()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category2_data

#      WHEN "icon_category3_name"
#        LET tmp_icon_category_id =  icon_category_popup()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category3_data
#      OTHERWISE
#        #do nothing
#    ENd CASE
# ##


    ON ACTION("editTbiObj")  --Edit menu_item_obj
      LET tmp_tbi_obj_name = tbi_obj_edit(l_tbi_form_rec.tbi_obj_name)
      IF tmp_tbi_obj_name IS NOT NULL THEN
        LET l_tbi_form_rec.tbi_obj_name = tmp_tbi_obj_name
        DISPLAY tmp_tbi_obj_name TO tbi_obj_name

        #Check if the field buffer has actually changed
        IF FIELD_TOUCHED(tbi_obj_name) THEN 
 
          #Update/Refresh all combo lists (entries may have changed)
          CALL populate_tbi_combo_lists_g()

          IF yes_no("Toolbar Menu Item obj",get_str_tool(861)) THEN
            #Event
            LET l_tbi_form_rec.tbi_event_name  = get_tbi_obj_event_name(l_tbi_form_rec.tbi_obj_name)
            LET l_tbi_form_rec.event_type_name  = get_event_type_name(get_tbi_obj_event_type_id(l_tbi_form_rec.tbi_obj_name))

            #Category
            LET l_tbi_form_rec.icon_category1_data = get_icon_category_data(get_tbi_obj_category1_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category2_data = get_icon_category_data(get_tbi_obj_category2_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category3_data = get_icon_category_data(get_tbi_obj_category3_id(l_tbi_form_rec.tbi_obj_name),get_language())
          END IF

          CALL refresh_det_form_field_contents(l_tbi_form_rec.*)

        END IF

      END IF


    # Action/Key events
    ON ACTION("newTbiObj")  --Create new menu_item_obj
      LET tmp_tbi_obj_name = tbi_obj_create(qxt_g_filter.tbi_obj_name)
      IF tmp_tbi_obj_name IS NOT NULL THEN
        LET l_tbi_form_rec.tbi_obj_name = tmp_tbi_obj_name
        DISPLAY tmp_tbi_obj_name TO tbi_obj_name

        #Check if the field buffer has actually changed
        IF FIELD_TOUCHED(tbi_obj_name) THEN 

#        IF tmp_tbi_obj_name <>l_tbi_form_rec.tbi_obj_name THEN
          #Assign the new file name 
 #         LET l_tbi_form_rec.tbi_obj_name = tmp_tbi_obj_name

  
          #Update/Refresh all combo lists (entries may have changed)
#          DISPLAY l_tbi_form_rec.tbi_obj_name TO tbi_obj_name
          CALL populate_tbi_combo_lists_g()
#          DISPLAY l_tbi_form_rec.tbi_obj_name TO tbi_obj_name
          #CALL tbi_obj_event_name_combo_list("tbi_obj_name")

          #display the new value to the filenae combo field
#          DISPLAY l_tbi_form_rec.tbi_obj_name TO tbi_obj_name

          #Action Name - just a view display field
#          DISPLAY get_tbi_action_name_from_tbi_obj_name(l_tbi_form_rec.tbi_obj_name) TO tbi_action_name

          IF yes_no("Toolbar Menu Item obj",get_str_tool(861)) THEN
            #Event
            LET l_tbi_form_rec.tbi_event_name  = get_tbi_obj_event_name(l_tbi_form_rec.tbi_obj_name)
            LET l_tbi_form_rec.event_type_name  = get_event_type_name(get_tbi_obj_event_type_id(l_tbi_form_rec.tbi_obj_name))

#           DISPLAY l_tbi_form_rec.tbi_event_name TO tbi_event_name

            #Category
            LET l_tbi_form_rec.icon_category1_data = get_icon_category_data(get_tbi_obj_category1_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category2_data = get_icon_category_data(get_tbi_obj_category2_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category3_data = get_icon_category_data(get_tbi_obj_category3_id(l_tbi_form_rec.tbi_obj_name),get_language())
 #           DISPLAY BY NAME l_tbi_form_rec.icon_category1_data TO icon_category1_data
 #           DISPLAY BY NAME l_tbi_form_rec.icon_category2_data TO icon_category2_data
 #           DISPLAY BY NAME l_tbi_form_rec.icon_category3_data TO icon_category3_data
          END IF

          CALL refresh_det_form_field_contents(l_tbi_form_rec.*)

        END IF

      END IF

{
      #Display string data / tooltip
      DISPLAY get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name) TO string_id
      DISPLAY get_tbi_tooltip_data(get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name),get_language()) TO string_data

      #Image name and preview
      DISPLAY get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name) TO icon_filename
      DISPLAY get_icon16_path(get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name)) TO preview1
      DISPLAY get_icon24_path(get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name)) TO preview2
      DISPLAY get_icon32_path(get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name)) TO preview3
}

      
    ON ACTION("manageTbiObj")  --Manage Menu Item objects
      LET tmp_tbi_obj_name = tbi_obj_popup(l_tbi_form_rec.tbi_obj_name,get_data_language_id(),NULL,0) --tbi_obj_popup(p_tbi_obj_name,p_language_id,p_order_field,p_accept_action)
      IF tmp_tbi_obj_name IS NOT NULL THEN

        IF tmp_tbi_obj_name <>l_tbi_form_rec.tbi_obj_name OR l_tbi_form_rec.tbi_obj_name IS NULL THEN

          #Assign the new file name 
          LET l_tbi_form_rec.tbi_obj_name = tmp_tbi_obj_name

          #Update/Refresh all combo lists (entries may have changed)
          CALL populate_tbi_combo_lists_g()

          #display the new value to the filenae combo field
          DISPLAY l_tbi_form_rec.tbi_obj_name TO tbi_obj_name

          #Action Name - just a view display field
          DISPLAY get_tbi_action_name_from_tbi_obj_name(l_tbi_form_rec.tbi_obj_name) TO tbi_action_name


          IF yes_no("Toolbar Menu Item obj",get_str_tool(861)) THEN
            #Event
            LET l_tbi_form_rec.tbi_event_name  = get_tbi_obj_event_name(l_tbi_form_rec.tbi_obj_name)
            LET l_tbi_form_rec.event_type_name  = get_event_type_name(get_tbi_obj_event_type_id(l_tbi_form_rec.tbi_obj_name))

            #Category
            LET l_tbi_form_rec.icon_category1_data = get_icon_category_data(get_tbi_obj_category1_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category2_data = get_icon_category_data(get_tbi_obj_category2_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category3_data = get_icon_category_data(get_tbi_obj_category3_id(l_tbi_form_rec.tbi_obj_name),get_language())
            DISPLAY BY NAME l_tbi_form_rec.icon_category1_data
            DISPLAY BY NAME l_tbi_form_rec.icon_category2_data
            DISPLAY BY NAME l_tbi_form_rec.icon_category3_data
          END IF

        END IF
      END IF

      #Display string data / tooltip
      DISPLAY get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name) TO string_id
      DISPLAY get_tbi_tooltip_data(get_tbi_obj_string_id(l_tbi_form_rec.tbi_obj_name),get_language()) TO string_data

      #Image name and preview
      DISPLAY get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name) TO icon_filename
      CALL icon_preview(get_tbi_obj_icon_filename(l_tbi_form_rec.tbi_obj_name))


    ON ACTION("newTbiObjEvent")   --Create new event obj
      LET tmp_tbi_event_name = tbi_obj_event_create()
      IF tmp_tbi_event_name IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_form_rec.tbi_event_name = tmp_tbi_event_name
        #refresh the combo list
        DISPLAY l_tbi_form_rec.tbi_event_name TO tbi_event_name
        CALL tbi_obj_event_name_combo_list("tbi_event_name")
        #display the new value to the filenae combo field
        DISPLAY l_tbi_form_rec.tbi_event_name TO tbi_event_name
      END IF


    ON ACTION("manageTbiObjEvent")   --Manage Event objs
      LET tmp_tbi_event_name = tbi_obj_event_popup(l_tbi_form_rec.tbi_event_name,NULL,0)
      IF tmp_tbi_event_name IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_form_rec.tbi_event_name = tmp_tbi_event_name
        #refresh the combo list
        DISPLAY l_tbi_form_rec.tbi_event_name TO tbi_event_name
        CALL tbi_obj_event_name_combo_list("tbi_event_name")
        #display the new value to the filenae combo field
        DISPLAY l_tbi_form_rec.tbi_event_name TO tbi_event_name
      END IF


    ON ACTION("newIconCategory")  --Create new category

    ON ACTION("editIconCategory")  --Create new category

    ON ACTION("manageIconCategory")  --Create new category

{
      LET tmp_icon_category_id =  icon_category_create()

      #Refresh combo list boxes
      CALL icon_category_data_combo_list("icon_category1_data",get_language())
      CALL icon_category_data_combo_list("icon_category2_data",get_language())
      CALL icon_category_data_combo_list("icon_category3_data",get_language())


    #Find out, if the cursor is in a category field and update it correspondingly
    CASE fgl_dialog_getfieldname()
      WHEN "icon_category1_name"
        LET l_tbi_form_rec.icon_category1_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_form_rec.icon_category1_data TO icon_category1_data

      WHEN "icon_category2_name"
        LET l_tbi_form_rec.icon_category2_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_form_rec.icon_category2_data TO icon_category2_data

      WHEN "icon_category3_name"
        LET l_tbi_form_rec.icon_category3_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_form_rec.icon_category3_data TO icon_category3_data

      OTHERWISE
        #do nothing
    ENd CASE
}
#    ON KEY(F112)  --Manage category


# fgl_dialog_getfieldname() is broken
#
#      #Refresh combo list boxes
#      CALL icon_category_data_combo_list("icon_category1_data",get_language())
#      CALL icon_category_data_combo_list("icon_category2_data",get_language())
#      CALL icon_category_data_combo_list("icon_category3_data",get_language())


#    #Find out, if the cursor is in a category field and update it correspondingly
#    CASE fgl_dialog_getfieldname()

#      WHEN "icon_category1_name"
#        LET tmp_icon_category_id =  icon_category_popup(get_icon_category_id()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category1_data

#      WHEN "icon_category2_name"
#        LET tmp_icon_category_id =  icon_category_popup()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category2_data

#      WHEN "icon_category3_name"
#        LET tmp_icon_category_id =  icon_category_popup()
#        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category3_data
#      OTHERWISE
#        #do nothing
#    ENd CASE
#



    ##########################
    # After field events
    ##########################

    AFTER FIELD application_name
      #application_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(901), l_tbi_form_rec.application_name,NULL,TRUE)  THEN
        NEXT FIELD application_name
      END IF

      IF p_new_flag THEN  --new records can never have duplicate keys
        IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
          NEXT FIELD application_name
        END IF

      ELSE  --edit records can have duplicate, if they use the original keys
        IF l_orignal_tbi_name <> l_tbi_form_rec.tbi_name OR l_orignal_application_id <> get_application_id(l_tbi_form_rec.application_name) THEN
          IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
            #Constraint only exists for newly created records (not modify)
            CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
            NEXT FIELD application_name
          END IF
        END IF
      END IF



    AFTER FIELD tbi_name
      #tbi_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(902), l_tbi_form_rec.tbi_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_name
      END IF

      IF p_new_flag THEN  --new records can never have duplicate keys
        IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
          NEXT FIELD tbi_name
        END IF

      ELSE  --edit records can have duplicate, if they use the original keys
        IF l_orignal_tbi_name <> l_tbi_form_rec.tbi_name OR l_orignal_application_id <> get_application_id(l_tbi_form_rec.application_name) THEN
          IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
            #Constraint only exists for newly created records (not modify)
            CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
            NEXT FIELD tbi_name
          END IF
        END IF
      END IF


    #Field tbi_obj_name
    BEFORE FIELD tbi_obj_name  --keep a duplicate to see, if the value has changed - only inherite properties for changed entries
      LET tmp_tbi_obj_name = l_tbi_form_rec.tbi_obj_name


    AFTER FIELD tbi_obj_name
      #tbi_obj_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(903), l_tbi_form_rec.tbi_obj_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_name
      END IF

      IF FIELD_TOUCHED(tbi_obj_name)  THEN --Field is different to the orignal input start
        IF l_tbi_form_rec.tbi_obj_name <> tmp_tbi_obj_name OR tmp_tbi_obj_name IS NULL THEN

          #Inherite the Category Information if the field has changed - user can still change it...
          IF yes_no("Toolbar Menu Item obj",get_str_tool(861)) THEN
            #Event
            LET l_tbi_form_rec.event_type_name = get_event_type_name(get_tbi_obj_event_type_id(l_tbi_form_rec.tbi_obj_name))
            LET l_tbi_form_rec.tbi_event_name  = get_tbi_obj_event_name(l_tbi_form_rec.tbi_obj_name)
            #Category
            LET l_tbi_form_rec.icon_category1_data = get_icon_category_data(get_tbi_obj_category1_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category2_data = get_icon_category_data(get_tbi_obj_category2_id(l_tbi_form_rec.tbi_obj_name),get_language())
            LET l_tbi_form_rec.icon_category3_data = get_icon_category_data(get_tbi_obj_category3_id(l_tbi_form_rec.tbi_obj_name),get_language())
            CALL refresh_det_form_field_contents(l_tbi_form_rec.*)
          END IF
        END IF
      END IF

      IF l_tbi_form_rec.tbi_name IS NULL THEN
        LET l_tbi_form_rec.tbi_name = l_tbi_form_rec.tbi_obj_name
        DISPLAY l_tbi_form_rec.tbi_name TO tbi_name
      END IF

    AFTER FIELD event_type_name
      #tbi_event_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(904), l_tbi_form_rec.event_type_name,NULL,TRUE)  THEN
        NEXT FIELD event_type_name
      END IF

    AFTER FIELD tbi_event_name
      #tbi_event_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(905), l_tbi_form_rec.tbi_event_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_event_name
      END IF

    AFTER FIELD tbi_scope_name
      #tbi_scope_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(906), l_tbi_form_rec.tbi_scope_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_scope_name
      END IF

    AFTER FIELD tbi_position
      #tbi_position must not be empty
      IF NOT validate_field_value_numeric_exists(get_str_tool(907), l_tbi_form_rec.tbi_position,NULL,TRUE)  THEN
        NEXT FIELD tbi_position
      END IF

    AFTER FIELD tbi_static_name
      #tbi_static_name must not be empty
      IF NOT validate_field_value_boolean_exists(get_str_tool(908), l_tbi_form_rec.tbi_static_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_static_name
      END IF


    # AFTER INPUT BLOCK ####################################
    AFTER INPUT
      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF


     #application_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(901), l_tbi_form_rec.tbi_name,NULL,TRUE)  THEN
        NEXT FIELD application_name
        CONTINUE INPUT
      END IF

      IF p_new_flag THEN  --new records can never have duplicate keys
        IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
          NEXT FIELD application_name
          CONTINUE INPUT

        END IF

      ELSE  --edit records can have duplicate, if they use the original keys
        IF l_orignal_tbi_name <> l_tbi_form_rec.tbi_name OR l_orignal_application_id <> get_application_id(l_tbi_form_rec.application_name) THEN
          IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
            #Constraint only exists for newly created records (not modify)
            CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
            NEXT FIELD application_name
            CONTINUE INPUT
          END IF
        END IF
      END IF


      #tbi_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(902), l_tbi_form_rec.tbi_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_name
        CONTINUE INPUT
      END IF

      IF p_new_flag THEN  --new records can never have duplicate keys
        IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
          NEXT FIELD tbi_name
          CONTINUE INPUT
        END IF

      ELSE  --edit records can have duplicate, if they use the original keys
        IF l_orignal_tbi_name <> l_tbi_form_rec.tbi_name OR l_orignal_application_id <> get_application_id(l_tbi_form_rec.application_name) THEN
          IF get_tbi_application_id_and_name_count(get_application_id(l_tbi_form_rec.application_name),l_tbi_form_rec.tbi_name) THEN
            #Constraint only exists for newly created records (not modify)
            CALL  tl_msg_duplicate_doublekey(get_str_tool(901),get_str_tool(902),NULL)
            NEXT FIELD tbi_name
            CONTINUE INPUT
          END IF
        END IF
      END IF


      #obj_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(903), l_tbi_form_rec.tbi_obj_name,NULL,TRUE)  THEN
        NEXT FIELD obj_name
        CONTINUE INPUT
      END IF

      #event_type_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(904), l_tbi_form_rec.event_type_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_event_name
        CONTINUE INPUT
      END IF

      #tbi_event_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(905), l_tbi_form_rec.tbi_event_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_event_name
        CONTINUE INPUT
      END IF

      #tbi_scope_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(906), l_tbi_form_rec.tbi_scope_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_scope_name
        CONTINUE INPUT
      END IF

      #position must not be empty
      IF NOT validate_field_value_numeric_exists(get_str_tool(907), l_tbi_form_rec.tbi_position,NULL,TRUE)  THEN
        NEXT FIELD tbi_position
        CONTINUE INPUT
      END IF


      #static must not be empty
      IF NOT validate_field_value_boolean_exists(get_str_tool(908), l_tbi_form_rec.tbi_static_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_static_name
        CONTINUE INPUT
      END IF



  END INPUT
  # END INPUT BLOCK  ##########################

  IF local_debug THEN
    DISPLAY "tbi_input() - l_tbi_form_rec.application_name=",      l_tbi_form_rec.application_name
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_name=",              l_tbi_form_rec.tbi_name
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_obj_name=",          l_tbi_form_rec.tbi_obj_name
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_event_name=",        l_tbi_form_rec.tbi_event_name
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_scope_name=",        l_tbi_form_rec.tbi_scope_name
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_position=",          l_tbi_form_rec.tbi_position
    DISPLAY "tbi_input() - l_tbi_form_rec.tbi_static_name=",       l_tbi_form_rec.tbi_static_name
    DISPLAY "tbi_input() - l_tbi_form_rec.icon_category1_data=",   l_tbi_form_rec.icon_category1_data
    DISPLAY "tbi_input() - l_tbi_form_rec.icon_category2_data=",   l_tbi_form_rec.icon_category2_data
    DISPLAY "tbi_input() - l_tbi_form_rec.icon_category3_data=",   l_tbi_form_rec.icon_category3_data

  END IF


  #Copy the form record data to a normal tbi record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_tbi_form_record_to_record(l_tbi_form_rec.*) RETURNING p_tbi_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "tbi_input() - p_tbi_rec.application_id=",    p_tbi_rec.application_id
    DISPLAY "tbi_input() - p_tbi_rec.tbi_name=",          p_tbi_rec.tbi_name
    DISPLAY "tbi_input() - p_tbi_rec.tbi_obj_name=",      p_tbi_rec.tbi_obj_name
    DISPLAY "tbi_input() - p_tbi_rec.tbi_event_name=",    p_tbi_rec.tbi_event_name
    DISPLAY "tbi_input() - p_tbi_rec.tbi_scope_id=",      p_tbi_rec.tbi_scope_id
    DISPLAY "tbi_input() - p_tbi_rec.tbi_position=",      p_tbi_rec.tbi_position
    DISPLAY "tbi_input() - p_tbi_rec.tbi_static_id=",     p_tbi_rec.tbi_static_id
    DISPLAY "tbi_input() - p_tbi_rec.icon_category1_id=", p_tbi_rec.icon_category1_id
    DISPLAY "tbi_input() - p_tbi_rec.icon_category2_id=", p_tbi_rec.icon_category2_id
    DISPLAY "tbi_input() - p_tbi_rec.icon_category3_id=", p_tbi_rec.icon_category3_id

  END IF

  RETURN p_tbi_rec.*

END FUNCTION



#################################################
# FUNCTION tbi_delete(p_tbi_name)
#
# Delete a tbi record
#
# RETURN NONE
#################################################
FUNCTION tbi_delete(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id               LIKE qxt_tbi.application_id,
    p_tbi_name       LIKE qxt_tbi.tbi_name

  #do you really want to delete...
  IF question_delete_record(get_str_tool(900), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_tbi 
        WHERE application_id = p_application_id
          AND qxt_tbi_name = p_tbi_name
    COMMIT WORK

  END IF

END FUNCTION







######################################################
# FUNCTION copy_tbi_record_to_form_record(p_tbi_rec)  
#
# Copy normal tbi record data to type tbi_form_rec
#
# RETURN l_tbi_form_rec.*
######################################################
FUNCTION copy_tbi_record_to_form_record(p_tbi_rec)  
  DEFINE
    p_tbi_rec       RECORD LIKE qxt_tbi.*,
    l_tbi_form_rec  OF t_qxt_tbi_form_rec,
    local_debug                   SMALLINT

  LET local_debug = FALSE
  LET l_tbi_form_rec.application_name =     get_application_name(p_tbi_rec.application_id)
  LET l_tbi_form_rec.tbi_name =             p_tbi_rec.tbi_name
  LET l_tbi_form_rec.tbi_obj_name =         p_tbi_rec.tbi_obj_name
  LET l_tbi_form_rec.event_type_name =      get_event_type_name(p_tbi_rec.event_type_id)
  LET l_tbi_form_rec.tbi_event_name =       p_tbi_rec.tbi_event_name
  LET l_tbi_form_rec.tbi_scope_name =       get_tbi_scope_name(p_tbi_rec.tbi_scope_id)
  LET l_tbi_form_rec.tbi_position =         p_tbi_rec.tbi_position
  LET l_tbi_form_rec.tbi_static_name =      get_tbi_static_name(p_tbi_rec.tbi_static_id)
  LET l_tbi_form_rec.icon_category1_data =  get_icon_category_data(p_tbi_rec.icon_category1_id,get_language())
  LET l_tbi_form_rec.icon_category2_data =  get_icon_category_data(p_tbi_rec.icon_category2_id,get_language())
  LET l_tbi_form_rec.icon_category3_data =  get_icon_category_data(p_tbi_rec.icon_category3_id,get_language())


  IF local_debug THEN
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.application_id=",          p_tbi_rec.application_id
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_name=",                p_tbi_rec.tbi_name
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_obj_name=",            p_tbi_rec.tbi_obj_name
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_event_name=",          p_tbi_rec.tbi_event_name
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_scope_id=",            p_tbi_rec.tbi_scope_id
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_position=",            p_tbi_rec.tbi_position
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.tbi_static_id=",           p_tbi_rec.tbi_static_id
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.icon_category1_id=",       p_tbi_rec.icon_category1_id
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.icon_category2_id=",       p_tbi_rec.icon_category2_id
    DISPLAY "copy_tbi_record_to_form_record() - p_tbi_rec.icon_category3_id=",       p_tbi_rec.icon_category3_id

    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.application_name=",   l_tbi_form_rec.application_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_name=",           l_tbi_form_rec.tbi_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_obj_name=",       l_tbi_form_rec.tbi_obj_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_event_name=",     l_tbi_form_rec.tbi_event_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_scope_name=",     l_tbi_form_rec.tbi_scope_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_position=",       l_tbi_form_rec.tbi_position
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.tbi_static_name=",    l_tbi_form_rec.tbi_static_name
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.icon_category1_data=",l_tbi_form_rec.icon_category1_data
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.icon_category2_data=",l_tbi_form_rec.icon_category2_data
    DISPLAY "copy_tbi_record_to_form_record() - l_tbi_form_rec.icon_category3_data=",l_tbi_form_rec.icon_category3_data

  END IF

  RETURN l_tbi_form_rec.*

END FUNCTION


######################################################
# FUNCTION copy_tbi_form_record_to_record(p_tbi_form_rec)  
#
# Copy type tbi_form_rec to normal tbi record data
#
# RETURN l_tbi_rec.*
######################################################
FUNCTION copy_tbi_form_record_to_record(p_tbi_form_rec)  
  DEFINE
    l_tbi_rec       RECORD LIKE qxt_tbi.*,
    p_tbi_form_rec  OF t_qxt_tbi_form_rec,
    local_debug     SMALLINT

  LET local_debug = FALSE

  LET l_tbi_rec.application_id =    get_application_id(p_tbi_form_rec.application_name)
  LET l_tbi_rec.tbi_name =          p_tbi_form_rec.tbi_name
  LET l_tbi_rec.tbi_obj_name =      p_tbi_form_rec.tbi_obj_name
  LET l_tbi_rec.event_type_id =     get_event_type_id(p_tbi_form_rec.event_type_name)
  LET l_tbi_rec.tbi_event_name =    p_tbi_form_rec.tbi_event_name
  LET l_tbi_rec.tbi_scope_id =      get_tbi_scope_id(p_tbi_form_rec.tbi_scope_name)
  LET l_tbi_rec.tbi_position =      p_tbi_form_rec.tbi_position
  LET l_tbi_rec.tbi_static_id =     get_tbi_static_id(p_tbi_form_rec.tbi_static_name)
  LET l_tbi_rec.icon_category1_id = get_icon_category_id(p_tbi_form_rec.icon_category1_data,get_language())
  LET l_tbi_rec.icon_category2_id = get_icon_category_id(p_tbi_form_rec.icon_category2_data,get_language())
  LET l_tbi_rec.icon_category3_id = get_icon_category_id(p_tbi_form_rec.icon_category3_data,get_language())

  IF local_debug THEN
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.application_name=",    p_tbi_form_rec.application_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_name=",            p_tbi_form_rec.tbi_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_obj_name=",        p_tbi_form_rec.tbi_obj_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_event_name=",      p_tbi_form_rec.tbi_event_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_scope_name=",      p_tbi_form_rec.tbi_scope_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_position=",        p_tbi_form_rec.tbi_position
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.tbi_static_name=",     p_tbi_form_rec.tbi_static_name
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.icon_category1_data=", p_tbi_form_rec.icon_category1_data
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.icon_category2_data=", p_tbi_form_rec.icon_category2_data
    DISPLAY "copy_tbi_form_record_to_record() - p_tbi_form_rec.icon_category3_data=", p_tbi_form_rec.icon_category3_data

    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.application_id=",           l_tbi_rec.application_id
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_name=",                 l_tbi_rec.tbi_name
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_obj_name=",             l_tbi_rec.tbi_obj_name
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_event_name=",           l_tbi_rec.tbi_event_name
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_scope_id=",             l_tbi_rec.tbi_scope_id
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_position=",             l_tbi_rec.tbi_position
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.tbi_static_id=",            l_tbi_rec.tbi_static_id
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.icon_category1_id=",        l_tbi_rec.icon_category1_id
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.icon_category2_id=",        l_tbi_rec.icon_category2_id
    DISPLAY "copy_tbi_form_record_to_record() - l_tbi_rec.icon_category3_id=",        l_tbi_rec.icon_category3_id

  END IF

  RETURN l_tbi_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

#################################################
# FUNCTION tbi_view(p_tbi_name)
#
# View tbi record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_view(p_application_id,p_tbi_name)
  DEFINE 
    p_application_id  LIKE qxt_tbi.application_id,
    p_tbi_name        LIKE qxt_tbi.tbi_name,
    l_tbi_rec         RECORD LIKE qxt_tbi.*

  CALL get_tbi_rec(p_application_id,p_tbi_name) 
    RETURNING l_tbi_rec.*

  CALL tbi_view_by_rec(l_tbi_rec.*)

END FUNCTION


#################################################
# FUNCTION tbi_view_by_rec(p_tbi_rec)
#
# View tbi record in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_view_by_rec(p_tbi_rec)
  DEFINE 
    p_tbi_rec  RECORD LIKE qxt_tbi.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

	CALL fgl_window_open("w_tbi", 3, 3, get_form_path("f_qxt_tbi_det_l2"), FALSE) 
	CALL populate_tbi_form_view_labels_g()

  #Update icon preview
  CALL icon_preview(get_tbi_icon_filename(get_current_application_id(),p_tbi_rec.tbi_name)) 

  DISPLAY BY NAME p_tbi_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_tbi")
END FUNCTION



####################################################
# FUNCTION grid_header_tbi_scroll()
#
# Populate tbi grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_tbi_scroll()
  CALL fgl_grid_header("sc_tbi","application_name",    get_str_tool(901),"right","F13") 
  CALL fgl_grid_header("sc_tbi","tbi_name",            get_str_tool(902),"right","F14") 
  CALL fgl_grid_header("sc_tbi","tbi_obj_name",        get_str_tool(903),"left" ,"F15")  
  CALL fgl_grid_header("sc_tbi","event_type_name",     get_str_tool(904),"left" ,"F16") 
  CALL fgl_grid_header("sc_tbi","tbi_event_name",      get_str_tool(905),"left" ,"F17") 
  CALL fgl_grid_header("sc_tbi","tbi_action_name",  get_str_tool(906),"left" ,"F18") 
  CALL fgl_grid_header("sc_tbi","tbi_scope_name",      get_str_tool(907),"left" ,"F19")  
  CALL fgl_grid_header("sc_tbi","tbi_position",        get_str_tool(908),"left" ,"F20") 
  CALL fgl_grid_header("sc_tbi","tbi_static_name",     get_str_tool(909),"left" ,"F21") 
  CALL fgl_grid_header("sc_tbi","icon_filename",       get_str_tool(913),"left" ,"F22")  
  CALL fgl_grid_header("sc_tbi","string_id",           get_str_tool(914),"left" ,"F23")  
  CALL fgl_grid_header("sc_tbi","string_data",         get_str_tool(915),"left" ,"F24")  
  CALL fgl_grid_header("sc_tbi","icon_category1_data", get_str_tool(910),"left" ,"F25")  
  CALL fgl_grid_header("sc_tbi","icon_category2_data", get_str_tool(911),"left" ,"F26") 
  CALL fgl_grid_header("sc_tbi","icon_category3_data", get_str_tool(912),"left" ,"F27")  

END FUNCTION


#######################################################
# FUNCTION populate_tbi_combo_lists_g()
#
# Populate all combo lists in the form
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_combo_lists_g()

  #Populate the combo list boxes dynamic from DB
  CALL application_name_combo_list("application_name")
  CALL tbi_combo_list("tbi_name")
  CALL tbi_obj_combo_list("tbi_obj_name")
  CALL event_type_name_combo_list("event_type_name")
  CALL tbi_obj_event_name_combo_list("tbi_event_name")
  CALL toolbar_menu_scope_name_combo_list("tbi_scope_name")
  CALL tbi_static_name_combo_list("tbi_static_name")
  CALL icon_category_data_combo_list("icon_category1_data",get_language())
  CALL icon_category_data_combo_list("icon_category2_data",get_language())
  CALL icon_category_data_combo_list("icon_category3_data",get_language())

END FUNCTION


#######################################################
# FUNCTION refresh_det_form_field_contents(p_tbi_form_rec)
#
# Refresh the field contents
#
# RETURN NONE
#######################################################
FUNCTION refresh_det_form_field_contents(p_tbi_form_rec)
  DEFINE
    p_tbi_form_rec  OF t_qxt_tbi_form_rec

    DISPLAY p_tbi_form_rec.application_name TO application_name
    DISPLAY p_tbi_form_rec.tbi_name TO tbi_name
    DISPLAY p_tbi_form_rec.tbi_obj_name TO tbi_obj_name
    DISPLAY p_tbi_form_rec.event_type_name TO event_type_name
    DISPLAY p_tbi_form_rec.tbi_event_name TO tbi_event_name

    DISPLAY get_tbi_action_name_from_tbi_obj_name(p_tbi_form_rec.tbi_obj_name) TO tbi_action_name
    DISPLAY p_tbi_form_rec.tbi_scope_name TO tbi_scope_name
    DISPLAY p_tbi_form_rec.tbi_position TO tbi_position
    DISPLAY p_tbi_form_rec.tbi_static_name TO tbi_static_name
    DISPLAY BY NAME p_tbi_form_rec.icon_category1_data
    DISPLAY BY NAME p_tbi_form_rec.icon_category2_data
    DISPLAY BY NAME p_tbi_form_rec.icon_category3_data

    DISPLAY get_tbi_obj_string_id(p_tbi_form_rec.tbi_obj_name) TO string_id
    DISPLAY get_tbi_label_data(get_tbi_obj_string_id(p_tbi_form_rec.tbi_obj_name),get_language()) TO label_data    
    DISPLAY get_tbi_tooltip_data(get_tbi_obj_string_id(p_tbi_form_rec.tbi_obj_name),get_language()) TO string_data
    DISPLAY get_tbi_obj_icon_filename(p_tbi_form_rec.tbi_obj_name) TO icon_filename

    CALL icon_preview(get_tbi_obj_icon_filename(p_tbi_form_rec.tbi_obj_name))

END FUNCTION


#######################################################
# FUNCTION populate_tbi_form_labels_g()
#
# Populate tbi form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_labels_g()

  CALL fgl_settitle(get_str_tool(900))

  DISPLAY get_str_tool(900) TO lbTitle

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9

  DISPLAY get_str_tool(910) TO dl_f10  --Category
  DISPLAY get_str_tool(911) TO dl_f11  --tooltip
  DISPLAY get_str_tool(912) TO dl_f12  --Icon
  DISPLAY get_str_tool(913) TO dl_f13  --Icon
  #DISPLAY get_str_tool(914) TO dl_f14  --String id
  #DISPLAY get_str_tool(915) TO dl_f15  --String data
  DISPLAY get_str_tool(916) TO dl_f16  --Tooltip
  DISPLAY get_str_tool(917) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(7) TO bt_edit_obj
  #DISPLAY "!" TO bt_edit_obj

  DISPLAY get_str_tool(18) TO bt_new_obj
  #DISPLAY "!" TO bt_new_obj

  DISPLAY get_str_tool(21) TO bt_manage_obj
  #DISPLAY "!" TO bt_manage_obj


  DISPLAY get_str_tool(7) TO bt_edit_event
  #DISPLAY "!" TO bt_edit_event

  DISPLAY get_str_tool(18) TO bt_new_event
  #DISPLAY "!" TO bt_new_event

  DISPLAY get_str_tool(21) TO bt_manage_event
  #DISPLAY "!" TO bt_manage_event


  DISPLAY get_str_tool(7) TO bt_edit_category
  #DISPLAY "!" TO bt_edit_category

  DISPLAY get_str_tool(18) TO bt_new_category
  #DISPLAY "!" TO bt_new_category

  DISPLAY get_str_tool(21) TO bt_manage_category
  #DISPLAY "!" TO bt_manage_category

  #Populate the combo list boxes dynamic from DB
  CALL populate_tbi_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_form_labels_t()
#
# Populate tbi form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_labels_t()

  DISPLAY get_str_tool(900) TO lbTitle

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10

  DISPLAY get_str_tool(914) TO dl_f11  --tooltip
  DISPLAY get_str_tool(911) TO dl_f12  --Icon
  DISPLAY get_str_tool(915) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_form_edit_labels_g()
#
# Populate tbi form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(900)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9

  DISPLAY get_str_tool(910) TO dl_f10  --Category
  DISPLAY get_str_tool(911) TO dl_f11  --tooltip
  DISPLAY get_str_tool(912) TO dl_f12  --Icon
  DISPLAY get_str_tool(913) TO dl_f13  --Icon
  #DISPLAY get_str_tool(914) TO dl_f14  --String id
  #DISPLAY get_str_tool(915) TO dl_f15  --String data
  DISPLAY get_str_tool(916) TO dl_f16  --Tooltip
  DISPLAY get_str_tool(917) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  ##DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  ##DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(7) TO bt_edit_obj
  ##DISPLAY "!" TO bt_edit_obj

  DISPLAY get_str_tool(18) TO bt_new_obj
  ##DISPLAY "!" TO bt_new_obj

  DISPLAY get_str_tool(21) TO bt_manage_obj
  ##DISPLAY "!" TO bt_manage_obj


  DISPLAY get_str_tool(7) TO bt_edit_event
  ##DISPLAY "!" TO bt_edit_event

  DISPLAY get_str_tool(18) TO bt_new_event
  ##DISPLAY "!" TO bt_new_event

  DISPLAY get_str_tool(21) TO bt_manage_event
  ##DISPLAY "!" TO bt_manage_event


  DISPLAY get_str_tool(7) TO bt_edit_category
  ##DISPLAY "!" TO bt_edit_category

  DISPLAY get_str_tool(18) TO bt_new_category
  ##DISPLAY "!" TO bt_new_category

  DISPLAY get_str_tool(21) TO bt_manage_category
  ##DISPLAY "!" TO bt_manage_category


  #Populate the combo list boxes dynamic from DB
  CALL populate_tbi_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_form_edit_labels_t()
#
# Populate tbi form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_edit_labels_t()

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10

  DISPLAY get_str_tool(914) TO dl_f11  --tooltip
  DISPLAY get_str_tool(911) TO dl_f12  --Icon
  DISPLAY get_str_tool(915) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tbi_form_view_labels_g()
#
# Populate tbi form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(900)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9

  DISPLAY get_str_tool(910) TO dl_f10  --Category
  DISPLAY get_str_tool(911) TO dl_f11  --tooltip
  DISPLAY get_str_tool(912) TO dl_f12  --Icon
  DISPLAY get_str_tool(913) TO dl_f13  --Icon
  #DISPLAY get_str_tool(914) TO dl_f14  --String id
  #DISPLAY get_str_tool(915) TO dl_f15  --String data
  DISPLAY get_str_tool(916) TO dl_f16  --Tooltip
  DISPLAY get_str_tool(917) TO dl_f17  --Preview

  DISPLAY get_str_tool(402) TO lbInfo1

  DISPLAY get_str_tool(3) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(7) TO bt_edit_obj
  #DISPLAY "!" TO bt_edit_obj

  DISPLAY get_str_tool(18) TO bt_new_obj
  #DISPLAY "!" TO bt_new_obj

  DISPLAY get_str_tool(21) TO bt_manage_obj
  #DISPLAY "!" TO bt_manage_obj


  DISPLAY get_str_tool(7) TO bt_edit_event
  #DISPLAY "!" TO bt_edit_event

  DISPLAY get_str_tool(18) TO bt_new_event
  #DISPLAY "!" TO bt_new_event

  DISPLAY get_str_tool(21) TO bt_manage_event
  #DISPLAY "!" TO bt_manage_event


  DISPLAY get_str_tool(7) TO bt_edit_category
  #DISPLAY "!" TO bt_edit_category

  DISPLAY get_str_tool(18) TO bt_new_category
  #DISPLAY "!" TO bt_new_category

  DISPLAY get_str_tool(21) TO bt_manage_category
  #DISPLAY "!" TO bt_manage_category

  #Populate the combo list boxes dynamic from DB
  CALL populate_tbi_combo_lists_g()


  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_form_view_labels_t()
#
# Populate tbi form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_view_labels_t()

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10

  DISPLAY get_str_tool(914) TO dl_f11  --tooltip
  DISPLAY get_str_tool(911) TO dl_f12  --Icon
  DISPLAY get_str_tool(915) TO dl_f13  --Preview

  DISPLAY get_str_tool(402) TO lbInfo1

  #CALL language_combo_list("language_dir")

END FUNCTION


#######################################################
# FUNCTION populate_tbi_form_create_labels_g()
#
# Populate tbi form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(900)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9

  DISPLAY get_str_tool(910) TO dl_f10  --Category
  DISPLAY get_str_tool(911) TO dl_f11  --tooltip
  DISPLAY get_str_tool(912) TO dl_f12  --Icon
  DISPLAY get_str_tool(913) TO dl_f13  --Icon
  #DISPLAY get_str_tool(914) TO dl_f14  --String id
  #DISPLAY get_str_tool(915) TO dl_f15  --String data
  DISPLAY get_str_tool(916) TO dl_f16  --Tooltip
  DISPLAY get_str_tool(917) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(7) TO bt_edit_obj
  #DISPLAY "!" TO bt_edit_obj

  DISPLAY get_str_tool(18) TO bt_new_obj
  #DISPLAY "!" TO bt_new_obj

  DISPLAY get_str_tool(21) TO bt_manage_obj
  #DISPLAY "!" TO bt_manage_obj


  DISPLAY get_str_tool(7) TO bt_edit_event
  #DISPLAY "!" TO bt_edit_event

  DISPLAY get_str_tool(18) TO bt_new_event
  #DISPLAY "!" TO bt_new_event

  DISPLAY get_str_tool(21) TO bt_manage_event
  #DISPLAY "!" TO bt_manage_event


  DISPLAY get_str_tool(7) TO bt_edit_category
  #DISPLAY "!" TO bt_edit_category

  DISPLAY get_str_tool(18) TO bt_new_category
  #DISPLAY "!" TO bt_new_category

  DISPLAY get_str_tool(21) TO bt_manage_category
  #DISPLAY "!" TO bt_manage_category


  #Populate the combo list boxes dynamic from DB
  CALL populate_tbi_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_form_create_labels_t()
#
# Populate tbi form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_form_create_labels_t()

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10

  DISPLAY get_str_tool(914) TO dl_f11  --tooltip
  DISPLAY get_str_tool(911) TO dl_f12  --Icon
  DISPLAY get_str_tool(915) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_list_form_labels_g()
#
# Populate tbi list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_list_form_labels_g()
  CALL updateUILabel("icon_category_filter","Category")
  CALL updateUILabel("application_name_filter","Application")
  CALL updateUILabel("tbi_name_filter","Name")
  CALL fgl_settitle(trim(get_str_tool(900)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_tbi_scroll()

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  #DISPLAY get_str_tool(908) TO dl_f8
  #DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10  --Category 1
  #DISPLAY get_str_tool(911) TO dl_f11 --Category 2
  #DISPLAY get_str_tool(912) TO dl_f12 --Category 3
  DISPLAY get_str_tool(913) TO dl_f13
  #DISPLAY get_str_tool(914) TO dl_f14
  #DISPLAY get_str_tool(915) TO dl_f15
  DISPLAY get_str_tool(916) TO dl_f16  --Tooltip

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  DISPLAY get_str_tool(22) TO bt_dublicate
  #DISPLAY "!" TO bt_dublicate


  #Enable Category Filter check box
  #DISPLAY "!" TO icon_category_filter
  #DISPLAY "!" TO application_name_filter
  #DISPLAY "!" TO tbi_name_filter

  #Enbable button to specify filter criteria
  #DISPLAY "!" TO bt_set_icon_category_filter_criteria
  #DISPLAY "!" TO bt_set_application_filter_criteria
  #DISPLAY "!" TO bt_set_tbi_name_filter_criteria
  #DISPLAY "!" TO bt_set_filter_language_name

  CALL icon_category_data_combo_list("cs1",get_language())
  CALL icon_category_data_combo_list("cs2",get_language())
  CALL icon_category_data_combo_list("cs3",get_language())
  CALL application_name_combo_list("filter_application_name")
  CALL language_combo_list("filter_lang_n")

  #Initialise icon preview
  CALL icon_preview(NULL)


END FUNCTION


#######################################################
# FUNCTION populate_tbi_list_form_labels_t()
#
# Populate tbi list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_list_form_labels_t()

  DISPLAY trim(get_str_tool(900)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(901) TO dl_f1
  DISPLAY get_str_tool(902) TO dl_f2
  DISPLAY get_str_tool(903) TO dl_f3
  DISPLAY get_str_tool(904) TO dl_f4
  DISPLAY get_str_tool(905) TO dl_f5
  DISPLAY get_str_tool(906) TO dl_f6
  DISPLAY get_str_tool(907) TO dl_f7
  DISPLAY get_str_tool(908) TO dl_f8
  DISPLAY get_str_tool(909) TO dl_f9
  DISPLAY get_str_tool(910) TO dl_f10

  DISPLAY get_str_tool(914) TO dl_f12  --tooltip
  DISPLAY get_str_tool(911) TO dl_f13  --Icon
  DISPLAY get_str_tool(915) TO dl_f14  --Preview  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################









