##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################
# 
# FUNCTION:                                     DESCRIPTION:                                 RETURN
# get_xchg_rate(p_currency_id)                  Get the exchange Rate                        l_xchg_rate
# get_xchg_rate_iv(p_invoice_id)                The the exchange Rate from Invoice           l_xchg_rate
# get_currency(p_currency_id)                   The the currency name                        l_currency_name
# get_currency_name(p_currency_id)              Get the currency name  (seems a duplicate)   l_currency_name
# get_currency_id(p_currency_name)              get the currency id form the name            l_currency_id
# get_currency_rec(p_currency_id)               Get the currency record from an id           l_currency.*
# currency_popup_data_source()                  Data Source for currency_popup_data_source() NONE
# currency_popup()                              View currencies and return seletec value     l_currency.*
# currency_create()                             Create a new currency (record)               rv
# currency_delete(p_currency_id)                Delete a currency                            NONE
# currency_edit(p_currency_id)                  Edit a currency                              NONE
# currency_view_by_rec(p_currency_rec)                 View a currency record                       NONE
# currency_input(p_currency)                    Insert new currency (subfunction of create)  l_currency.*
# currency_combo_list(cb_field_name)            Populates combo list with values from DB     NONE
# grid_header_currency_scroll()                 Populate grid array header with labels       NONE
# currency_popup                                Display currency list for selection          l_currency_arr[i].currency_id or p_currency_id
# (p_currency_id,p_order_field,p_accept_action) and management                               
#
##################################################




##################################################
# GLOBALS
##################################################
GLOBALS "globals.4gl"



#############################################
# FUNCTION currency_popup_data_source()
#
# Data Source for currency_popup()
#
# RETURN NONE
#############################################
FUNCTION currency_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "currency_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM currency "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_currency FROM sql_stmt
  DECLARE c_currency CURSOR FOR p_currency


END FUNCTION

#############################################
# FUNCTION currency_popup(p_currency_id,p_order_field,p_accept_action)
#
# Display currency list for selection and management
#
# RETURN l_currency_arr[i].currency_id or p_currency_id
#############################################
FUNCTION currency_popup(p_currency_id,p_order_field,p_accept_action)
  DEFINE 
    p_currency_id                 LIKE currency.currency_id,
    l_currency_arr                DYNAMIC ARRAY OF RECORD LIKE currency.*,
    i                             INTEGER,
    p_accept_action               SMALLINT,
    err_msg                       VARCHAR(240),
    p_order_field,p_order_field2  VARCHAR(128),
    local_debug                   SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

    CALL fgl_window_open("w_currency_scroll", 2, 8, get_form_path("f_currency_scroll_l2"), FALSE)
    CALL populate_currency_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/money/currency_type.png")
	CALL ui.Interface.setText("Currency")
	    
 #   DISPLAY "!" TO bt_ok
 #   DISPLAY "!" TO bt_cancel
 #   DISPLAY "!" TO bt_edit
 #   DISPLAY "!" TO bt_delete
 #   DISPLAY "!" TO bt_new
    CALL grid_header_currency_scroll()
    CALL fgl_settitle(get_str(1900))  --"Currency List")

 
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL currency_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_currency INTO l_currency_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH

    LET i = i - 1

	IF i > 0 THEN
		CALL l_currency_arr.reSize(i)  --correct the last element of the dynamic array
	END IF    
		
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_currency_arr TO sa_currency_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("CurrencyList",0)

      ON KEY(INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_currency_arr[i].currency_id

        CASE p_accept_action

          WHEN 0
            EXIT WHILE
        
          WHEN 1  --view
            CALL currency_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL currency_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL currency_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL currency_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","activity_type_popup()\nActivity Type Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL activity_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

            OTHERWISE
              LET err_msg = "activity_type_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("activity_type_popup() - 4GL Source Error",err_msg, "error") 
          END CASE



      ON KEY (F4) -- add
        CALL currency_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_currency_arr[i].currency_id
        CALL currency_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_currency_arr[i].currency_id
        CALL currency_delete(i)
        EXIT DISPLAY

      ON KEY (F9)
        LET int_flag = TRUE
        EXIT WHILE

      #Column Sort short cuts
      
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "currency_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

     
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "currency_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "xchg_rate"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_currency_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_currency_id
  ELSE 
    LET i = arr_curr()
    RETURN l_currency_arr[i].currency_id
  END IF

END FUNCTION


#############################################
# FUNCTION currency_create()
#
# Create a new currency record
#
# RETURN r_currency_id (integer)
#############################################
FUNCTION currency_create()
  DEFINE 
    l_currency        RECORD LIKE currency.*,
    r_currency_id     INTEGER

  LET r_currency_id = NULL
  LET l_currency.currency_id = 0

  CALL currency_input(l_currency.*)
    RETURNING l_currency.*

  IF NOT int_flag THEN 
    INSERT INTO currency VALUES (l_currency.*)
    LET r_currency_id = sqlca.sqlerrd[2]
  ELSE 
    LET int_flag = FALSE
  END IF

  RETURN r_currency_id
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate


#############################################
# FUNCTION currency_delete(p_currency_id)
#
# Delete a currency record
#
# RETURN NONE
#############################################
FUNCTION currency_delete(p_currency_id)
  DEFINE p_currency_id INTEGER
  #Delete!","Are you sure you want to delete this value?"
  #"Are you sure you want to delete this value?"
  #do you really want to delete...
  IF question_delete_record(get_str(1900), NULL) THEN
    DELETE FROM currency 
      WHERE currency.currency_id = p_currency_id
  END IF

END FUNCTION


#############################################
# FUNCTION currency_edit(p_currency_id)
#
# Edit a currency record
#
# RETURN NONE
#############################################
FUNCTION currency_edit(p_currency_id)
  DEFINE 
    p_currency_id INTEGER,
    l_currency    RECORD LIKE currency.*

  SELECT currency.* 
    INTO l_currency.*
    FROM currency
    WHERE currency.currency_id = p_currency_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL currency_input(l_currency.*) 
    RETURNING l_currency.*

  IF NOT int_flag THEN
    UPDATE currency
      SET currency.currency_name = l_currency.currency_name,
          currency.xchg_rate = l_currency.xchg_rate
      WHERE currency.currency_id = l_currency.currency_id
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION

#############################################
# FUNCTION currency_view(p_currency_rec)
#
# View a currency record
#
# RETURN NONE
#############################################
FUNCTION currency_view(p_currency_id)
  DEFINE 
    p_currency_id LIKE currency.currency_id,
    l_currency_rec RECORD LIKE currency.*

  CALL get_currency_rec(p_currency_id) RETURNING l_currency_rec.*
  CALL currency_view_by_rec(l_currency_rec.*)

END FUNCTION

#############################################
# FUNCTION currency_view_by_rec(p_currency_rec)
#
# View a currency record
#
# RETURN NONE
#############################################
FUNCTION currency_view_by_rec(p_currency_rec)
  DEFINE 
    p_currency_rec RECORD LIKE currency.*,
    inp_char       CHAR

    CALL fgl_window_open("w_currency", 3, 3, get_form_path("f_currency_l2"), FALSE)
    CALL populate_currency_form_labels_g()
    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_str(1900)) --"Currency")


  DISPLAY BY NAME p_currency_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char
      ON KEY(ACCEPT,INTERRUPT)
        EXIT WHILE
    ENd PROMPT
  END WHILE

  CALL fgl_window_close("w_currency_view")
END FUNCTION


#############################################
# FUNCTION currency_input(p_currency)
#
# Input currency record details (edit/create)
#
# RETURN l_currency.*
#############################################
FUNCTION currency_input(p_currency)
  DEFINE 
    p_currency RECORD LIKE currency.*,
    l_currency RECORD LIKE currency.*

  LET l_currency.* = p_currency.*

    CALL fgl_window_open("w_currency", 3, 3, get_form_path("f_currency_l2"), FALSE)
    CALL populate_currency_form_labels_g()
    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_str(1900)) --"Currency")


  LET int_flag = FALSE

  INPUT BY NAME l_currency.currency_name,
                l_currency.xchg_rate WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_currency.currency_name IS NULL THEN
          #The currency (name) field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1930),"error")
          NEXT FIELD currency_name
          CONTINUE INPUT
        END IF
        IF l_currency.xchg_rate IS NULL THEN
          #The XChange rate field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1931),"error")
          NEXT FIELD xchg_rate
          CONTINUE INPUT
        END IF
      ELSE
        #Do you really want to abort the new/modified record entry ?
        IF NOT yes_no(get_str(1932),get_str(1933)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT


  CALL fgl_window_close("w_currency")

  RETURN l_currency.*
END FUNCTION

