##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###########################################################
# Globals
############################################################
GLOBALS "qxt_db_help_html_document_globals.4gl"

###########################################################
# FUNCTION html_help(help_url) 
#
# Displays the url in a help window
#
# RETURN NONE
###########################################################
# Note: Application will call CALL html_help(get_help_url(1)) or CALL html_help(url)
# The app writer can specify a fully qualified url or use the get_help_url() function to prepare the url & file download etc..
# This function will NOT check the existence - otherwise, the user can not specify freely library independent url's
FUNCTION html_help(help_url) 
  DEFINE 
    help_url    VARCHAR(200),
    inp_char    CHAR,
    local_debug SMALLINT,
    tmp_str     VARCHAR(200)



  #don't bother about text client... text clients don't support html widgets.
  CALL fgl_window_open("w_html_help", get_help_html_win_y(),get_help_html_win_x(), get_form_path("f_qxt_html_help_l2"), TRUE)

  LET tmp_str = get_str_tool(600) CLIPPED, " - ", help_url
  CALL fgl_settitle("w_html_help",tmp_str)

  IF local_debug THEN
    DISPLAY "html_help() - help_url = ", help_url
  END IF

  DISPLAY help_url TO f_browser

  
  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 300
      BEFORE PROMPT
        CALL publish_toolbar("HelpHtml",0) 

      ON KEY(F12,INTERRUPT,ACCEPT)
        LET int_flag = FALSE
        EXIT WHILE

    	ON ACTION ("actExit") 
        LET int_flag = FALSE
        EXIT WHILE
      
      AFTER PROMPT
        CALL publish_toolbar("HelpHtml",1) 

    END PROMPT
  END WHILE

  CALL fgl_window_close("w_html_help")    

END FUNCTION


###########################################################
# FUNCTION get_help_html_web_url(p_url_map_id)
#
# Returns the Help WEB Url
#
# RETURN get_help_html_base_url_path(get_help_html_url_map_joint_url(get_language(),p_url_map_id))
###########################################################
FUNCTION get_help_html_web_url(p_url_map_id)
  DEFINE 
    p_url_map_id         SMALLINT,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_html_web_url() - p_url_map_id=", p_url_map_id
    DISPLAY "get_help_html_web_url() - get_help_html_url_map_joint_url(get_language()=", get_help_html_url_map_joint_url(get_language(),p_url_map_id)
    DISPLAY "get_help_html_web_url() - get_help_html_base_url_path(get_help_html_url_map_joint_url(get_language(),p_url_map_id))=", get_help_html_base_url_path(get_help_html_url_map_joint_url(get_language(),p_url_map_id))

  END IF

  RETURN get_help_html_base_url_path(get_help_html_url_map_joint_url(get_language(),p_url_map_id))

END FUNCTION


###########################################################
# FUNCTION get_help_html_app_server_url(p_url_map_id)
#
# Returns the Help Url for the help file located on the app server
#
# RETURN url
###########################################################
FUNCTION get_help_html_app_server_url(p_url_map_id)
  DEFINE 
    p_url_map_id         SMALLINT,
    url                  VARCHAR(200),
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_html_app_server_url() - p_url_map_id=", p_url_map_id
  END IF

  #get the document name
  LET url =  get_help_html_url_map_fname(p_url_map_id,get_language())

  IF local_debug THEN
    DISPLAY "1-get_help_html_app_server_url() - url=", url
  END IF


  #add the base and language url directory
  LET url =  get_help_html_base_dir_path(get_language_url_path(get_language(),url))  

  IF local_debug THEN
    DISPLAY "2-get_help_html_app_server_url() - url=", url
  END IF

  #download the file
  #LET url =  file_download(url,url, FALSE,NULL)
	LET url = "qx://application/", trim(url)
  IF local_debug THEN
    DISPLAY "3-get_help_html_app_server_url() - url=", url
  END IF

  #add the optional url extension string
  LET url = url CLIPPED, get_help_html_url_map_ext(p_url_map_id,get_language()) CLIPPED

  IF local_debug THEN
    DISPLAY "4-get_help_html_app_server_url() - url=", url
  END IF

  RETURN url

END FUNCTION

{

###########################################################
# FUNCTION set_help_id(id)
#
# Set the html help id
#
# RETURN NONE
###########################################################
FUNCTION set_help_id(id)
  DEFINE 
    id SMALLINT
  LET qxt_current_help_id = id

END FUNCTION
}
{
FUNCTION get_help_html(p_id)
  DEFINE
    p_id INTEGER

  RETURN qxt_helphtml[p_id].help_html_file

END FUNCTION
}




