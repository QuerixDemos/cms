##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################
# 
# FUNCTION:                                     DESCRIPTION:                                      RETURN
# delivery_type_combo_list(cb_field_name)       Populates combo list with values from DB          NONE
#
#
#
##################################################




##################################################
# GLOBALS
##################################################
GLOBALS "globals.4gl"

###################################################################################
# FUNCTION delivery_type_combo_list(cb_field_name)
#
# Populates combo list with values from DB
#
# RETURN NONE
###################################################################################
FUNCTION delivery_type_combo_list(cb_field_name)
  DEFINE 
    l_delivery_type_arr DYNAMIC ARRAY OF t_delivery_type_rec,
    row_count           INTEGER,
    current_row         INTEGER,
    cb_field_name       VARCHAR(20)   --form field name for the country combo list field
  
  DECLARE c_delivery_type_scroll2 CURSOR FOR 
    SELECT delivery_type.delivery_type_name 
      FROM delivery_type

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_delivery_type_scroll2 INTO l_delivery_type_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_delivery_type_arr[row_count].delivery_type_name)
    LET row_count = row_count + 1
  END FOREACH
		If row_count > 0 THEN
			CALL l_delivery_type_arr.resize(row_count)  --remove the last item which has no data
		END IF
  LET row_count = row_count - 1


END FUNCTION
