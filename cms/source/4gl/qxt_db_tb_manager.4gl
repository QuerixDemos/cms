##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tb table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_tb_name(p_tbi_name)  get tb id from p_tbi_name        l_tb_name
# get_tbi_name(tb_name)    Get tbi_name from p_tb_name        l_tbi_name
# get_tb_rec(p_tb_name)   Get the tb record from p_tb_name  l_tb.*
# tb_popup_data_source()               Data Source (cursor) for tb_popup              NONE
# tb_popup                             tb selection window                            p_tb_name
# (p_tb_name,p_order_field,p_accept_scope)
# tb_combo_list(cb_field_name)         Populates tb combo list from db                NONE
# tb_create()                          Create a new tb record                         NULL
# tb_edit(p_tb_name)      Edit tb record                                 NONE
# tb_input(p_tb_rec)    Input tb details (edit/create)                 l_tb.*
# tb_delete(p_tb_name)    Delete a tb record                             NONE
# tb_view(p_tb_name)      View tb record by ID in window-form            NONE
# tb_view_by_rec(p_tb_rec) View tb record in window-form               NONE
# get_tb_name_from_dir(p_dir)          Get the tb_name from a file                      l_tb_name
# tbi_name_count(p_dir)                Tests if a record with this dir already exists               r_count
# tb_name_count(p_tb_name)  tests if a record with this tb_name already exists r_count
# copy_tb_record_to_form_record        Copy normal tb record data to type tb_form_rec   l_tb_form_rec.*
# (p_tb_rec)
# copy_tb_form_record_to_record        Copy type tb_form_rec to normal tb record data   l_tb_rec.*
# (p_tb_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_tb_scroll()              Populate tb grid headers                       NONE
# populate_tb_form_labels_g()          Populate tb form labels for gui                NONE
# populate_tb_form_labels_t()          Populate tb form labels for text               NONE
# populate_tb_form_edit_labels_g()     Populate tb form edit labels for gui           NONE
# populate_tb_form_edit_labels_t()     Populate tb form edit labels for text          NONE
# populate_tb_form_view_labels_g()     Populate tb form view labels for gui           NONE
# populate_tb_form_view_labels_t()     Populate tb form view labels for text          NONE
# populate_tb_form_create_labels_g()   Populate tb form create labels for gui         NONE
# populate_tb_form_create_labels_t()   Populate tb form create labels for text        NONE
# populate_tb_list_form_labels_g()     Populate tb list form labels for gui           NONE
# populate_tb_list_form_labels_t()     Populate tb list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"

######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_tb_manager_lib_info()
  RETURN "qxt_db_tb_manager"
END FUNCTION 



FUNCTION tb_clone_entire_toolbar(p_application_id,p_tb_name_org, p_tb_name_new)
  DEFINE
    p_tb_name_org LIKE qxt_tb.tb_name,
    p_tb_name_new LIKE qxt_tb.tb_name,
    p_application_id LIKE qxt_application.application_id

  CALL fgl_window_open("w_tb_clone",5,5,get_form_path("f_qxt_tb_clone_l2"),FALSE)
  CALL populate_tb_clone_entire_toolbar(p_application_id)

  INPUT p_tb_name_org, p_tb_name_new WITHOUT DEFAULTS FROM tb_name_org, tb_name_new HELP 1
    AFTER INPUT
      IF tb_name_and_application_count(p_application_id,p_tb_name_new) THEN
        CALL fgl_winmessage("Menu already exists","A toolbar with this name already exists\nChoose a different target name","error")
        CONTINUE INPUT
    END IF
  END INPUT

  CALL fgl_window_close("w_tb_clone")
  IF p_tb_name_new IS NOT NULL THEN
    CALL tb_clone_entire_toolbar_process(p_application_id,p_tb_name_org, p_tb_name_new)
  END IF
  
  IF int_flag THEN
    LET int_flag = FALSE
  END IF
  
  RETURN p_tb_name_new

END FUNCTION



FUNCTION tb_clone_entire_toolbar_process(p_application_id,p_tb_name_org, p_tb_name_new)
  DEFINE
    p_tb_name_org LIKE qxt_tb.tb_name,
    p_tb_name_new LIKE qxt_tb.tb_name,
    p_application_id LIKE qxt_application.application_id,
    l_tb          RECORD LIKE qxt_tb.*,
    l_tb_array    DYNAMIC ARRAY OF RECORD LIKE qxt_tb.*,
    sql_stmt      CHAR(2000)

  IF p_tb_name_new IS NULL THEN
    CALL fgl_winmessage("Info","Tool Bar Item name p_tb_name_new is empty","info")
  END IF

  LET sql_stmt = "SELECT * ",
                 "FROM qxt_tb ", 
                 "WHERE tb_name = '", trim(p_tb_name_org), "' ",
                 "AND application_id = ", p_application_id



  PREPARE p_tb_clone FROM sql_stmt
  DECLARE c_tb_clone CURSOR FOR p_tb_clone


  FOREACH c_tb_clone INTO l_tb.*
 
    INSERT INTO qxt_tb VALUES (
      l_tb.application_id,
      p_tb_name_new,
      l_tb.tb_instance,
      l_tb.tbi_name
                                       )



  END FOREACH




END FUNCTION

FUNCTION populate_tb_clone_entire_toolbar(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL fgl_settitle("Clone an entire toolbar for an application")
  CALL tb_name_combo_list(p_application_id,"tb_name_org")
  CALL tb_name_combo_list(p_application_id,"tb_name_new") 
 #DISPLAY 


END FUNCTION


######################################################################################################################
# Data Access functins
######################################################################################################################


#########################################################
# FUNCTION get_tb_instance_from_menu_item_name(p_application_id,p_tb_name)
#
# get tb_instance from p_tb_name
#
# RETURN l_tb_name
#########################################################
FUNCTION get_tb_instance_from_menu_item_name(p_application_id,p_tb_name)
  DEFINE 
    p_application_id                 LIKE qxt_tb.application_id,
    p_tb_name         LIKE qxt_tb.tb_name,
    l_tb_instance     LIKE qxt_tb.tb_instance,
    local_debug                      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_name() - p_tb_name = ", p_tb_name
    DISPLAY "get_tb_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_tb.tb_instance
    INTO l_tb_instance
    FROM qxt_tb
    WHERE qxt_tb.tb_name = p_tb_name
      AND qxt_tb.application_id = p_application_id
  IF local_debug THEN
    DISPLAY "get_tb_name() - l_tb_instance = ", l_tb_instance
  END IF

  RETURN l_tb_instance
END FUNCTION


#########################################################
# FUNCTION get_tb_event_name(p_application_id,p_tb_name,p_tb_instance)
#
# get tb_event_name from p_tb_name
#
# RETURN l_tb_name
#########################################################
FUNCTION get_tb_event_name(p_application_id,p_tb_name,p_tb_instance)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_tb_event_name      LIKE qxt_tbi.tbi_event_name,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_name() - p_tb_name = ", p_tb_name
    DISPLAY "get_tb_name() - p_application_id = ", p_application_id
  END IF

  SELECT tbi_event_name
    INTO l_tb_event_name
    FROM qxt_tb,qxt_tbi
    WHERE qxt_tb.application_id = p_application_id
      AND qxt_tb.tb_name = p_tb_name
      AND qxt_tb.tb_instance = p_tb_instance
      AND qxt_tb.tbi_name = qxt_tbi.tbi_name

  IF local_debug THEN
    DISPLAY "get_tb_name() - l_tb_event_name = ", l_tb_event_name
  END IF

  RETURN l_tb_event_name
END FUNCTION


#########################################################
# FUNCTION get_tb_tbi_name(p_application_id,p_tb_name)
#
# get tbi_name from p_tb_name
#
# RETURN l_tb_name
#########################################################
FUNCTION get_tb_tbi_name(p_application_id,p_tb_name,p_tb_instance)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_tb_tbi_name        LIKE qxt_tb.tbi_name,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_name() - p_tb_name = ", p_tb_name
    DISPLAY "get_tb_name() - p_application_id = ", p_application_id
  END IF

  SELECT tbi_name
    INTO l_tb_tbi_name
    FROM qxt_tb
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance

  IF local_debug THEN
    DISPLAY "get_tb_name() - l_tb_tbi_name = ", l_tb_tbi_name
  END IF

  RETURN l_tb_tbi_name
END FUNCTION


#########################################################
# FUNCTION get_tb_static(p_application_id,p_tb_name)
#
# get static from p_tb_name
#
# RETURN l_tb_name
#########################################################
FUNCTION get_tb_static(p_application_id,p_tb_name,p_tb_instance)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_tb_static_id       LIKE qxt_tbi.tbi_static_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_name() - p_tb_name = ", p_tb_name
    DISPLAY "get_tb_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_tbi.tbi_static_id
    INTO l_tb_static_id
    FROM qxt_tb,qxt_tbi
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance
      AND qxt_tb.tbi_name = qxt_tbi.tbi_name

  IF local_debug THEN
    DISPLAY "get_tb_name() - l_tb_static_id = ", l_tb_static_id
  END IF

  RETURN l_tb_static_id
END FUNCTION


#########################################################
# FUNCTION get_tb_position(p_application_id,p_tb_name)
#
# get category1 from p_tb_name
#
# RETURN l_tb_name
#########################################################
FUNCTION get_tb_position(p_application_id,p_tb_name,p_tb_instance)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_tb_position        LIKE qxt_tbi.tbi_position,
    local_debug                     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_name() - p_application_id = ", p_application_id
    DISPLAY "get_tb_name() - p_tb_name = ", p_tb_name

  END IF

  SELECT qxt_tbi.tbi_position
    INTO l_tb_position
    FROM qxt_tb,qxt_tbi
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance
      AND qxt_tb.tbi_name = qxt_tbi.tbi_name

  IF local_debug THEN
    DISPLAY "get_tb_name() - l_tb_position = ", l_tb_position
  END IF

  RETURN l_tb_position
END FUNCTION



######################################################
# FUNCTION get_tb_rec(p_application_id,p_tb_name)
#
# Get the tb record from p_tb_name
#
# RETURN l_tb.*
######################################################
FUNCTION get_tb_rec(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    p_tbi_name           LIKE qxt_tb.tbi_name,
    l_tb                 RECORD LIKE qxt_tb.*,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_rec() - p_application_id=", p_application_id
    DISPLAY "get_tb_rec() - p_tb_name=", p_tb_name
    DISPLAY "get_tb_rec() - p_tb_instance=", p_tb_instance
    DISPLAY "get_tb_rec() - p_tbi_name=", p_tbi_name
  END IF

  SELECT qxt_tb.*
    INTO l_tb.*
    FROM qxt_tb
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance
      AND tbi_name = p_tbi_name

  IF local_debug THEN
    DISPLAY "get_tb_rec() - l_tb.application_id=", l_tb.application_id
    DISPLAY "get_tb_rec() - l_tb.tb_name=", l_tb.tb_name
    DISPLAY "get_tb_rec() - l_tb.tb_instance=", l_tb.tb_instance
    DISPLAY "get_tb_rec() - l_tb.tbi_nam=", l_tb.tbi_name
  END IF

  RETURN l_tb.*
END FUNCTION



#########################################################
# FUNCTION get_tb_icon_filename(p_application_id,p_tb_name)
#
# get tbi_name from p_tb_name
#
# RETURN l_tb_icon_filename
#########################################################
FUNCTION get_tb_icon_filename(p_application_id,p_tb_name,p_tb_instance)
  DEFINE 
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_tb_icon_filename   LIKE qxt_icon.icon_filename,
    local_debug          SMALLINT
 
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_icon_filename() - p_application_id = ", p_application_id
    DISPLAY "get_tb_icon_filename() - p_tb_name = ", p_tb_name
  END IF

  SELECT qxt_tbi_obj.icon_filename
    INTO l_tb_icon_filename
    FROM qxt_tb,qxt_tbi,qxt_tbi_obj
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance
      AND qxt_tb.tbi_name = qxt_tbi.tbi_name
      AND qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tb_icon_filename() - l_tb_icon_filename = ", l_tb_icon_filename
  END IF

  RETURN l_tb_icon_filename
END FUNCTION


###################################################################################
# FUNCTION get_tb_count()
#
# Finds the total number of record rows in the table
#
# RETURN l_count
###################################################################################
FUNCTION get_tb_count()
  DEFINE
    l_count                  INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tb

  RETURN l_count
END FUNCTION


###################################################################################
# FUNCTION get_tb_application_id_and_name_and_instance_count(p_application_id,p_tb_name,p_tb_instance)
#
# Finds the total number of menu item rows for each menu
#
# RETURN l_count
###################################################################################
FUNCTION get_tb_application_id_and_name_and_instance_count(p_application_id,p_tb_name,p_tb_instance)
  DEFINE
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    l_count              INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tb
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance

  RETURN l_count
END FUNCTION


###################################################################################
# FUNCTION get_tb_application_id_and_name_and_instance_and_tbi_count(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
#
# Finds the total number of menu item - Return must always be 0 or 1 (primary key constraint)
#
# RETURN l_count
###################################################################################
FUNCTION get_tb_application_id_and_name_and_instance_and_tbi_count(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    p_tbi_name           LIKE qxt_tb.tbi_name,
    l_count              INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tb
    WHERE application_id = p_application_id
      AND tb_name = p_tb_name
      AND tb_instance = p_tb_instance
      AND tbi_name = p_tbi_name

  RETURN l_count
END FUNCTION


###################################################################################
# FUNCTION get_tb_one_application_count(p_application_id)
#
# Finds the total number of menu item rows in the table for one application
#
# RETURN NONE
###################################################################################
FUNCTION get_tb_one_application_count(p_application_id)
  DEFINE
    p_application_id         LIKE qxt_tb.application_id,
    l_count                  INTEGER,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tb_one_application_count() - p_application_id=", p_application_id
  END IF

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tb
      WHERE qxt_tb.application_id = p_application_id

  IF local_debug THEN
    DISPLAY "get_tb_one_application_count() - l_count=", l_count
  END IF

  RETURN l_count
END FUNCTION



####################################################
# FUNCTION tb_name_and_language_count(p_application_id,p_tb_name)
#
# Returns how many of the same tb_name instances exist for a particular application
#
# RETURN r_count
####################################################
FUNCTION tb_name_and_language_count(p_application_id,p_tb_name)
  DEFINE
    p_application_id         LIKE qxt_tb.application_id,
    p_tb_name                LIKE qxt_tb.tb_name,
    r_count                  SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tb
      WHERE qxt_tb.tb_name = p_tb_name
        AND application_id = p_application_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION tb_name_and_application_count(p_application_id,p_tb_name)
#
# Returns how many of the same tb_name instances exist for a particular application
#
# RETURN r_count
####################################################
FUNCTION tb_name_and_application_count(p_application_id,p_tb_name)
  DEFINE
    p_application_id         LIKE qxt_tb.application_id,
    p_tb_name                LIKE qxt_tb.tb_name,
    r_count                  SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tb
      WHERE qxt_tb.tb_name = p_tb_name
        AND application_id = p_application_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

####################################################
# FUNCTION tb_position_count(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
#
# Returns how many menu items are located on the same position (order id)
#
# RETURN r_count
####################################################
FUNCTION tb_position_count(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE
    p_application_id     LIKE qxt_tb.application_id,
    p_tb_name            LIKE qxt_tb.tb_name,
    p_tb_instance        LIKE qxt_tb.tb_instance,
    p_tbi_name           LIKE qxt_tb.tbi_name,
    l_position           LIKE qxt_tbi.tbi_position,
    l_count              INTEGER

  LET l_position = get_tbi_position(p_application_id,p_tbi_name)

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tb,qxt_tbi
    WHERE qxt_tb.application_id = p_application_id
      AND qxt_tb.tb_name = p_tb_name
      AND qxt_tb.tb_instance = p_tb_instance
      AND qxt_tb.tbi_name = qxt_tbi.tbi_name
      #AND tbi_name = p_tbi_name
      AND qxt_tbi.tbi_position = l_position
  RETURN l_count
END FUNCTION






########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION tb_name_combo_list(p_application_id,cb_field_name)
#
# Populates tb combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tb_name_combo_list(p_application_id,cb_field_name)
  DEFINE 
    p_application_id         LIKE qxt_tb.application_id,
    l_tb_name                LIKE qxt_tb.tb_name,
    row_count                INTEGER,
    current_row              INTEGER,
    cb_field_name            VARCHAR(35),   --form field name for the country combo list field
    abort_flag              SMALLINT,
    tmp_str                  VARCHAR(250),
    local_debug              SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tb_combo_listtb_obj_event_name_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_tb_name_scroll2 CURSOR FOR 
    SELECT UNIQUE qxt_tb.tb_name
      FROM qxt_tb
    ORDER BY tb_name ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tb_name_scroll2 INTO l_tb_name
    CALL fgl_list_set(cb_field_name,row_count, l_tb_name)
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tb_name CLIPPED, ")"
      DISPLAY "tb_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION




######################################################
# FUNCTION tb_popup_data_source()
#
# Data Source (cursor) for tb_popup
#
# RETURN NONE
######################################################
FUNCTION tb_popup_data_source(p_app_filter,p_application_id,p_name_filter,p_tb_name, p_inst_filter,p_tb_instance,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_app_filter        SMALLINT,
    p_application_id    LIKE qxt_tb.application_id,
    p_name_filter       SMALLINT,
    p_tb_name           LIKE qxt_tb.tb_name,
    p_inst_filter       SMALLINT,
    p_tb_instance       LIKE qxt_tb.tb_instance,
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT,
    p_language_id       LIKE qxt_language.language_id

  LET local_debug = FALSE
  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "tb_name"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  IF p_app_filter IS NULL THEN
    LET p_app_filter = 0
  END IF

  IF p_name_filter IS NULL THEN
    LET p_name_filter = 0
  END IF

  IF p_inst_filter IS NULL THEN
    LET p_inst_filter = 0
  END IF
  LET sql_stmt = "SELECT ",
                 "qxt_application.application_name, ",
                 "qxt_tb.tb_name, ",
                 "qxt_tb.tb_instance, ",
                 "qxt_tb.tbi_name, ",
                 "qxt_tbi_event_type.event_type_name, ",
                 "qxt_tbi.tbi_event_name, ",
                 "qxt_tbi_obj_action.tbi_action_name, ",
                 "qxt_tbi_scope.tbi_scope_name, ",
                 "qxt_tbi.tbi_position, ",
                 "qxt_tbi_static.tbi_static_name, ",
                 "qxt_tbi_obj.icon_filename, ",
                 "qxt_tbi_tooltip.label_data, ",
                 "qxt_tbi_tooltip.string_data ",


                 "FROM qxt_tb, ",
                   "qxt_tbi, ",
                   "qxt_tbi_obj, ",
                   "qxt_tbi_event_type, ",
                   "qxt_tbi_scope, ",
                   "qxt_tbi_obj_action, ",  
                   "qxt_tbi_tooltip, ",
                   "qxt_application, ",
                   "qxt_tbi_static "


    LET sql_stmt = trim(sql_stmt), " ",
                 "WHERE qxt_tb.tbi_name = qxt_tbi.tbi_name  ",
                 "AND ",
                 "qxt_tb.application_id = qxt_tbi.application_id  ",
                 "AND ",
                 "qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name  ",
                 "AND ",
                 "qxt_tbi.event_type_id= qxt_tbi_event_type.event_type_id ",
                 "AND ",
                 "qxt_tbi.tbi_scope_id= qxt_tbi_scope.tbi_scope_id ",
                 "AND ",
                 "qxt_tbi_obj.tbi_obj_action_id = qxt_tbi_obj_action.tbi_action_id ",
                 #"AND ",
                 #"qxt_tbi_obj.tbi_obj_action_id = qxt_tbi_obj_action.tbi_obj_action_id ",
                 "AND ",
                 "qxt_tbi_obj.string_id = qxt_tbi_tooltip.string_id ",
                 "AND ",
                 "qxt_tb.application_id = qxt_application.application_id ",
                 "AND ",
                 "qxt_tbi.tbi_static_id = qxt_tbi_static.tbi_static_id  ",
                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_language_id), " "

  IF p_app_filter = TRUE THEN
    IF p_application_id IS NOT NULL THEN
      LET sql_stmt = trim(sql_stmt), " ",
                   "AND ",
                   "qxt_tb.application_id = ", trim(p_application_id) , " "
    END IF
  END IF


  IF p_name_filter = TRUE THEN
    IF p_tb_name IS NOT NULL THEN
      LET sql_stmt = trim(sql_stmt), " ",
                   "AND ",
                   "qxt_tb.tb_name = '", trim(p_tb_name) , "' "
    END IF
  END IF


  IF p_inst_filter = TRUE THEN
    IF p_tb_instance IS NOT NULL THEN
      LET sql_stmt = trim(sql_stmt), " ",
                   "AND ",
                   "qxt_tb.tb_instance = ", trim(p_tb_instance) , " "
    END IF
  END IF


#    END IF
#  END IF




  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "tb_popup_data_source()"
    DISPLAY "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
    DISPLAY sql_stmt[501,600]
    DISPLAY sql_stmt[601,700]
    DISPLAY sql_stmt[701,800]
    DISPLAY sql_stmt[801,900]
    DISPLAY sql_stmt[901,1000]
    DISPLAY sql_stmt[1001,1100]
    DISPLAY sql_stmt[1101,1200]
    DISPLAY sql_stmt[1201,1300]
    DISPLAY sql_stmt[1301,1400]
    DISPLAY "@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  END IF

  PREPARE p_tb FROM sql_stmt
  DECLARE c_tb CURSOR FOR p_tb

END FUNCTION


######################################################
# FUNCTION tb_popup(p_tb_name,p_order_field,p_accept_scope)
#
# tb selection window
#
# RETURN p_tb_name
######################################################
FUNCTION tb_popup(p_application_id,p_tb_name,p_tb_instance,p_tbi_name,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_application_id    LIKE qxt_tb.application_id,
    p_tb_name           LIKE qxt_tb.tb_name,
    p_tb_instance       LIKE qxt_tb.tb_instance,
    p_tbi_name          LIKE qxt_tb.tbi_name,
    p_language_id       LIKE qxt_language.language_id,
    p_order_field       VARCHAR(128), 
    p_order_field2      VARCHAR(128), 
    l_app_filter        SMALLINT,
    l_name_filter       SMALLINT,
    l_inst_filter       SMALLINT,
    l_application_name  LIKE qxt_application.application_name,
    l_tb_arr            DYNAMIC ARRAY OF t_qxt_tb_form_rec2,    --RECORD LIKE qxt_tb.*,
    l_arr_size          SMALLINT,  --size for l_tb_arr
    current_row         SMALLINT,  --to jump to last modified array line after edit/input
    i                   INTEGER,  
    p_accept_action     SMALLINT,
    err_msg             STRING,
    l_application_id    LIKE qxt_tb.application_id,
    l_tb_name           LIKE qxt_tb.tb_name,
    l_tb_instance       LIKE qxt_tb.tb_instance,
    l_tbi_name          LIKE qxt_tbi.tbi_name,
    l_filter_language_name LIKE qxt_language.language_name,
    local_debug         SMALLINT,
    l_language_id       LIKE qxt_language.language_id



  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET l_arr_size = 1000 -- due to dynamic array ...sizeof(l_tb_arr)
  LET l_app_filter = FALSE
  LET l_name_filter = FALSE      
  LET l_inst_filter = FALSE 

  #Initialise filter variables

  #If you want, you could define a default application_id
  IF p_application_id IS NULL THEN
    LET p_application_id = get_current_application_id()
  END IF

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  LET l_application_id = p_language_id

  #If you want, you could define a default menu name
  #IF p_tb_name IS NULL THEN
  #  LET p_tb_name = "GLOBAL"
  #END IF

  #IF p_tb_instance IS NULL THEN
  #  LET p_tb_instance = 1
  #END IF

     


  #If arguments are not empty, assign them to the filter
  #application id
  IF p_application_id IS NOT NULL THEN
    LET qxt_g_filter.application_id = p_application_id
  END IF

  LET l_application_id  = p_application_id
  LET l_application_name  = get_application_name(l_application_id)
  LET l_app_filter = TRUE
  #toolbar name
  IF p_tb_name IS NOT NULL THEN
    LET qxt_g_filter.tb_name = p_tb_name
  END IF

  LET l_tb_name = p_tb_name

  #Instance
  IF p_tb_instance IS NOT NULL THEN
    LET qxt_g_filter.tb_instance = p_tb_instance
  END IF

  LET l_tb_instance     = p_tb_instance

  #Item Name
  LET l_tbi_name     = p_tbi_name


  IF local_debug THEN
    DISPLAY "tb_popup() - p_application_id=",p_application_id
    DISPLAY "tb_popup() - l_application_id=",l_application_id
    DISPLAY "tb_popup() - p_tb_name=",p_tb_name
    DISPLAY "tb_popup() - p_order_field=",p_order_field
    DISPLAY "tb_popup() - p_accept_action=",p_accept_action
  END IF


	CALL fgl_window_open("w_tb_scroll", 2, 2, get_form_path("f_qxt_tb_scroll_l2"),FALSE) 
	CALL populate_tb_list_form_labels_g(p_application_id)

  DISPLAY get_language_name(p_language_id) TO filter_language_name

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0

  IF p_application_id = 0 OR p_application_id IS NULL THEN
    LET p_application_id = 0  --in case it is NULL
    SELECT COUNT(*)
      INTO i
      FROM qxt_tb
  ELSE

    SELECT COUNT(*)
      INTO i
      FROM qxt_tb
        WHERE qxt_tb.application_id = p_application_id
  END IF


  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL tb_create(NULL,NULL,NULL,NULL)
    END IF
  END IF

  IF local_debug THEN
    DISPLAY "tb_popup() - *p_application_id=",p_application_id
    DISPLAY "tb_popup() - *l_application_id=",l_application_id
    DISPLAY "tb_popup() - *p_tb_name=",p_tb_name
    DISPLAY "tb_popup() - *p_order_field=",p_order_field
    DISPLAY "tb_popup() - *p_accept_action=",p_accept_action
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL tb_popup_data_source(l_app_filter,p_application_id,l_name_filter,p_tb_name,l_inst_filter,p_tb_instance,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_tb INTO l_tb_arr[i].*

      #Find current screen line position - go to grid line
      IF l_tb_arr[i].tbi_name = l_tbi_name THEN 
        IF l_tb_arr[i].tb_name  = l_tb_name THEN
          IF l_tb_arr[i].application_name  = l_application_name THEN
            IF l_tb_arr[i].tb_instance       = l_tb_instance THEN
              LET current_row = i
            END IF
          END IF
        END IF
      END IF

      IF local_debug THEN
        DISPLAY "tb_popup() - i=",i
        DISPLAY "tb_popup() - l_tb_arr[i].application_name=",     l_tb_arr[i].application_name
        DISPLAY "tb_popup() - l_tb_arr[i].tb_name=",              l_tb_arr[i].tb_name
        DISPLAY "tb_popup() - l_tb_arr[i].tb_instance=",          l_tb_arr[i].tb_instance
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_name=",             l_tb_arr[i].tbi_name
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_event_name=",       l_tb_arr[i].tbi_event_name
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_action_name=",  l_tb_arr[i].tbi_action_name
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_scope_name=",       l_tb_arr[i].tbi_scope_name
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_position=",         l_tb_arr[i].tbi_position
        DISPLAY "tb_popup() - l_tb_arr[i].tbi_static_name=",      l_tb_arr[i].tbi_static_name
        DISPLAY "tb_popup() - l_tb_arr[i].icon_filename=",        l_tb_arr[i].icon_filename
        DISPLAY "tb_popup() - l_tb_arr[i].label_data=",          l_tb_arr[i].label_data
        DISPLAY "tb_popup() - l_tb_arr[i].string_data=",          l_tb_arr[i].string_data
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        LET err_msg = "Array is limited to ", l_arr_size CLIPPED, " entries"
        CALL fgl_winmessage("Warning","Array Size Limit", err_msg, "exclamation")
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_tb_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		    
    DISPLAY i TO found_records


  IF local_debug THEN
    DISPLAY "tb_popup() - **p_application_id=",p_application_id
    DISPLAY "tb_popup() - **l_application_id=",l_application_id
    DISPLAY "tb_popup() - **p_tb_name=",p_tb_name
    DISPLAY "tb_popup() - **p_order_field=",p_order_field
    DISPLAY "tb_popup() - **p_accept_action=",p_accept_action
  END IF

    IF p_application_id > 0 THEN
      DISPLAY get_tb_one_application_count(p_application_id) TO total_records
    ELSE
      DISPLAY get_tb_count() TO total_records
    END IF

    IF local_debug THEN
      DISPLAY "tb_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage("xxxxxxxx",get_str_tool(62),"error")

      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_tb_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "tb_popup() - set_count(i)=",i
    END IF

      IF local_debug THEN
        DISPLAY "tb_popup() - *i=",i
        IF i > 0 AND i <=  l_arr_size THEN
          DISPLAY "tb_popup() - *l_tb_arr[i].application_name=",     l_tb_arr[i].application_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tb_name=",              l_tb_arr[i].tb_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tb_instance=",          l_tb_arr[i].tb_instance
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_name=",             l_tb_arr[i].tbi_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_event_name=",       l_tb_arr[i].tbi_event_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_action_name=",  l_tb_arr[i].tbi_action_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_scope_name=",       l_tb_arr[i].tbi_scope_name
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_position=",         l_tb_arr[i].tbi_position
          DISPLAY "tb_popup() - *l_tb_arr[i].tbi_static_name=",      l_tb_arr[i].tbi_static_name
          DISPLAY "tb_popup() - *l_tb_arr[i].icon_filename=",        l_tb_arr[i].icon_filename
          DISPLAY "tb_popup() - *l_tb_arr[i].label_data=",          l_tb_arr[i].label_data
          DISPLAY "tb_popup() - *l_tb_arr[i].string_data=",          l_tb_arr[i].string_data
        END IF
      END IF


    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_tb_arr TO sc_tb.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
       BEFORE DISPLAY
        CALL publish_toolbar("ToolbarTbList",0)

        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF current_row IS NULL OR current_row = 0 THEN
          LET current_row = 5
        END IF
        IF local_debug THEN
          DISPLAY "tb_popup() - BEFORE ROW - current_row=", current_row
        END IF

        CALL fgl_dialog_setcurrline(5,current_row)


      BEFORE ROW
        LET i = arr_curr()

        LET l_application_id  = get_application_id(l_tb_arr[i].application_name)
        LET l_application_name= l_tb_arr[i].application_name
        LET l_tb_name         = l_tb_arr[i].tb_name
        LET l_tb_instance     = l_tb_arr[i].tb_instance
        LET l_tbi_name        = l_tb_arr[i].tbi_name

        IF local_debug THEN
          DISPLAY "tb_popup() - BEFORE ROW - i=", i
          DISPLAY "tb_popup() - BEFORE ROW - l_application_id=", l_application_id
          DISPLAY "tb_popup() - BEFORE ROW - l_application_name=", l_application_name
          DISPLAY "tb_popup() - BEFORE ROW - l_tb_name=", l_tb_name
          DISPLAY "tb_popup() - BEFORE ROW - l_tb_instance=", l_tb_instance
          DISPLAY "tb_popup() - BEFORE ROW - l_tbi_name=", l_tbi_name
        END IF


        DISPLAY get_application_name(p_application_id) TO filter_application_name
        DISPLAY p_tb_name TO filter_tb_name
        DISPLAY p_tb_instance TO filter_tb_instance

        CALL populate_tb_preview_scroll_g(l_tb_arr[i].*)


        #Initialise icon preview
        CALL icon_preview(l_tb_arr[i].icon_filename)

        #Update empty Filter with the values of the currently selected row
        IF qxt_g_filter.application_id IS NULL THEN
          LET qxt_g_filter.application_id = l_application_id
         END IF

        IF qxt_g_filter.tb_name IS NULL THEN
          LET qxt_g_filter.tb_name = l_tb_name
         END IF

        IF qxt_g_filter.tb_instance IS NULL THEN
          LET qxt_g_filter.tb_instance = l_tb_instance
         END IF


      #KEY EVENTS

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        #LET i = arr_curr()
        #LET l_tb_name = l_tb_arr[i].tb_name

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tb_view(l_application_id,l_tb_name,l_tb_instance,l_tbi_name)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL tb_edit(l_application_id,l_tb_name,l_tb_instance,l_tbi_name)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL tb_delete(l_application_id,l_tb_name,l_tb_instance,l_tbi_name)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL tb_create(l_application_id,l_tb_name,l_tb_instance,NULL)
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tb_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "tb_popup(p_tb_name,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("tb_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL tb_create(l_application_id,l_tb_name,l_tb_instance,NULL)
        CALL populate_tb_scroll_combo_lists_g(l_application_id)
        EXIT DISPLAY

      ON KEY (F5) -- edit
        IF local_debug THEN
          DISPLAY "tb_popup() - EDIT  l_application_id=", l_application_id
          DISPLAY "tb_popup() - EDIT  l_tb_name=", l_tb_name
          DISPLAY "tb_popup() - EDIT  l_tb_instance=", l_tb_instance
          DISPLAY "tb_popup() - EDIT  l_tbi_name=", l_tbi_name
        END IF

        CALL tb_edit(l_application_id,l_tb_name,l_tb_instance,l_tbi_name)
        CALL populate_tb_scroll_combo_lists_g(l_application_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL tb_delete(l_application_id,l_tb_name,l_tb_instance,l_tbi_name)
        EXIT DISPLAY


      ON KEY(F8)
        LET p_tb_name =  tb_clone_entire_toolbar(l_application_id,l_tb_name, "")

        EXIT DISPLAY

     #GRID Sort 

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "application_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
    
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tb_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tb_instance"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "event_type_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_event_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_action_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F20)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_scope_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F21)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_position"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F22)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_static_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      

      ON KEY(F23)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F24)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ######################
      # Aplication Filter
      ######################
      ON ACTION("filterApplicationOn")
        LET l_app_filter = TRUE
        EXIT DISPLAY 

      ON ACTION("filterApplicationOff")
        LET l_app_filter = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterApplication")  --Specify application filter criteria
        INPUT l_application_name WITHOUT DEFAULTS FROM filter_application_name HELP 1
          ON ACTION("setFilterApplication","filterApplicationOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET p_application_id = get_application_id(l_application_name)

          #remember filer value globally
          LET qxt_g_filter.application_id = p_application_id   
          LET l_app_filter = TRUE
 
        END IF

        EXIT DISPLAY 


      ######################
      # TB Name Filter
      ######################
      ON ACTION("filterTbNameOn")
        LET l_name_filter = TRUE
        EXIT DISPLAY 

      ON ACTION("filterTbNameOff")
        LET l_name_filter = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterTbName")  --Specify application filter criteria
        INPUT p_tb_name WITHOUT DEFAULTS FROM filter_tb_name HELP 1
          ON ACTION("setFilterTbName","filterTbNameOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          #remember filer value globally
          LET qxt_g_filter.tb_name = p_tb_name   
          LET l_name_filter = TRUE
        END IF

        EXIT DISPLAY

      ######################
      # TB Instance Filter
      ######################
      ON ACTION("filterTbInstanceOn")
        LET l_inst_filter = TRUE
        EXIT DISPLAY 

      ON ACTION("filterTbInstanceOff")
        LET l_inst_filter = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterTbInstance")  --Specify instance filter criteria
        INPUT p_tb_instance WITHOUT DEFAULTS FROM filter_tb_instance HELP 1
          ON ACTION("setFilterTbInstance","filterTbInstanceOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          #remember filer value globally
          LET qxt_g_filter.tb_instance = p_tb_instance  
          LET l_inst_filter = TRUE
        END IF
 
        EXIT DISPLAY


      ######################
      # Tooltip Language Filter
      ######################

      ON ACTION("setFilterLanguageName")  --Specify filter criteria
        INPUT l_filter_language_name WITHOUT DEFAULTS FROM filter_language_name HELP 1
          ON ACTION("setFilterLanguageName")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE

        ELSE
          LET p_language_id = get_language_id(l_filter_language_name)
          EXIT DISPLAY
        END IF


    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_tb_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_application_id, p_tb_name 
  ELSE 
    RETURN l_application_id, l_tb_name 
  END IF

END FUNCTION







###################################################################################################################################
# Edit/Create/Input Functions
###################################################################################################################################


######################################################
# FUNCTION tb_create()
#
# Create a new tb record
#
# RETURN NULL
######################################################
FUNCTION tb_create(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE 
    p_application_id      LIKE qxt_tb.application_id,
    p_tb_name             LIKE qxt_tb.tb_name,
    p_tb_instance         LIKE qxt_tb.tb_instance,
    p_tbi_name            LIKE qxt_tbi.tbi_name,
    l_tb_rec              RECORD LIKE qxt_tb.*,
    err_msg               VARCHAR(200),
    local_debug           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tb_create() - p_application_id=",  p_application_id
    DISPLAY "tb_create() - p_tb_name=",         p_tb_name
    DISPLAY "tb_create() - p_tb_instance=",     p_tb_instance
    DISPLAY "tb_create() - p_tbi_name=",        p_tbi_name
  END IF

	CALL fgl_window_open("w_tb", 3, 3, get_form_path("f_qxt_tb_det_l2"), FALSE) 
	CALL populate_tb_form_create_labels_g(p_application_id)

  LET int_flag = FALSE
  
  #Initialise some variables
  IF p_application_id IS NULL THEN
    LET l_tb_rec.application_id = 1
  ELSE
    LET l_tb_rec.application_id = p_application_id
  END IF

  IF p_tb_name IS NOT NULL THEN
    LET l_tb_rec.tb_name = p_tb_name
  END IF

  IF p_tb_instance IS NULL THEN
    LET l_tb_rec.tb_instance = 0
  ELSE
    LET l_tb_rec.tb_instance = p_tb_instance
  END IF


  IF p_tbi_name IS NOT NULL THEN
    LET l_tb_rec.tbi_name = p_tbi_name
  END IF


  IF local_debug THEN
    DISPLAY "tb_create() - After Input CALL - l_tb_rec.application_id=",  l_tb_rec.application_id
    DISPLAY "tb_create() - After Input CALL - l_tb_rec.tb_name=",         l_tb_rec.tb_name
    DISPLAY "tb_create() - After Input CALL - l_tb_rec.tb_instance=",     l_tb_rec.tb_instance
    DISPLAY "tb_create() - After Input CALL - l_tb_rec.tbi_name=",     l_tb_rec.tbi_name
  END IF

  # CALL the INPUT
  CALL tb_input(TRUE,l_tb_rec.*)
    RETURNING l_tb_rec.*

  CALL fgl_window_close("w_tb")

  IF local_debug THEN
    DISPLAY "tb_create() - Before Input CALL - l_tb_rec.application_id=",  l_tb_rec.application_id
    DISPLAY "tb_create() - Before Input CALL - l_tb_rec.tb_name=",         l_tb_rec.tb_name
    DISPLAY "tb_create() - Before Input CALL - l_tb_rec.tb_instance=",     l_tb_rec.tb_instance
    DISPLAY "tb_create() - Before Input CALL - l_tb_rec.tbi_name=",     l_tb_rec.tbi_name
  END IF


  IF NOT int_flag THEN
    INSERT INTO qxt_tb VALUES (
      l_tb_rec.application_id,
      l_tb_rec.tb_name,
      l_tb_rec.tb_instance,
      l_tb_rec.tbi_name
                                       )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL, NULL, NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      
      #Check if the toolbar icon position is already used 
      IF tb_position_count(l_tb_rec.application_id,l_tb_rec.tb_name,l_tb_rec.tb_instance,l_tb_rec.tbi_name) > 1 THEN
        LET err_msg = "You have got more than one icon specified for the same position\n Position:", trim(get_tbi_position(l_tb_rec.application_id,l_tb_rec.tbi_name))
        CALL fgl_winmessage("Icon Position Warning",err_msg,"info")
      END IF

      #Refresh combo boxes for possible new entries
      CALL populate_tb_combo_lists_g(p_application_id)


      RETURN l_tb_rec.application_id, l_tb_rec.tb_name, l_tb_rec.tb_instance, l_tb_rec.tbi_name
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL, NULL, NULL
  END IF

END FUNCTION



#################################################
# FUNCTION tb_edit(p_tb_name)
#
# Edit tb record
#
# RETURN NONE, NONE, NONE or ???
#################################################
FUNCTION tb_edit(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE 
    p_application_id      LIKE qxt_tb.application_id,
    p_tb_name             LIKE qxt_tb.tb_name,
    p_tb_instance         LIKE qxt_tb.tb_instance,
    p_tbi_name            LIKE qxt_tbi.tbi_name,
    l_key1_application_id LIKE qxt_tb.application_id,
    l_key2_tb_name        LIKE qxt_tb.tb_name,
    l_key3_tb_instance    LIKE qxt_tb.tb_instance,
    l_key4_tbi_name       LIKE qxt_tbi.tbi_name,
    l_tb_rec              RECORD LIKE qxt_tb.*,
    err_msg               VARCHAR(200),
    local_debug           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tb_edit() - Function Entry Point"
    DISPLAY "tb_edit() - p_application_id=", p_application_id
    DISPLAY "tb_edit() - p_tb_name=", p_tb_name
    DISPLAY "tb_edit() - p_tb_instance=", p_tb_instance
    DISPLAY "tb_edit() - p_tbi_name=", p_tbi_name
  END IF

  #Store the primary key for the SQL update
  LET l_key1_application_id = p_application_id
  LET l_key2_tb_name        = p_tb_name
  LET l_key3_tb_instance    = p_tb_instance
  LET l_key4_tbi_name       = p_tbi_name

  #Get the record (by primary key)
  CALL get_tb_rec(p_application_id,p_tb_name,p_tb_instance,p_tbi_name) RETURNING l_tb_rec.*

#	call fgl_winmessage("open w_tb","close w_tb","info")
	CALL fgl_window_open("w_tb", 3, 3, get_form_path("f_qxt_tb_det_l2"), TRUE) 
	CALL populate_tb_form_edit_labels_g(p_application_id)

  #Call the INPUT
  CALL tb_input(FALSE,l_tb_rec.*) 
    RETURNING l_tb_rec.*

  IF local_debug THEN
    DISPLAY "tb_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF
	#call fgl_winmessage("close w_tb","close w_tb","info")
  CALL fgl_window_close("w_tb")


  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "tb_create() - l_tb_rec.application_id=",  l_tb_rec.application_id
      DISPLAY "tb_create() - l_tb_rec.tb_name=",         l_tb_rec.tb_name
      DISPLAY "tb_create() - l_tb_rec.tb_instance=",     l_tb_rec.tb_instance
      DISPLAY "tb_create() - l_tb_rec.tbi_name=",     l_tb_rec.tbi_name
    END IF

    UPDATE qxt_tb
      SET 
        application_id =   l_tb_rec.application_id,
        tb_name =          l_tb_rec.tb_name,
        tb_instance =      l_tb_rec.tb_instance,
        tbi_name =         l_tb_rec.tbi_name

      WHERE qxt_tb.application_id = l_key1_application_id
        AND qxt_tb.tb_name        = l_key2_tb_name
        AND qxt_tb.tb_instance    = l_key3_tb_instance
        AND qxt_tb.tbi_name       = l_key4_tbi_name

    IF local_debug THEN
      DISPLAY "tb_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(920),NULL)
      RETURN NULL, NULL, NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(920),NULL)

      #Check if the toolbar icon position is already used 
      IF tb_position_count(l_tb_rec.application_id,l_tb_rec.tb_name,l_tb_rec.tb_instance,l_tb_rec.tbi_name) > 1 THEN
        LET err_msg = "You have got more than one icon specified for the same position\n Position:", trim(get_tbi_position(l_tb_rec.application_id,l_tb_rec.tbi_name))
        CALL fgl_winmessage("Icon Position Warning",err_msg,"info")
      END IF

      #Refresh combo boxes for possible new entries
      CALL populate_tb_combo_lists_g(p_application_id)

      RETURN l_tb_rec.application_id, l_tb_rec.tb_name, l_tb_rec.tb_instance, l_tb_rec.tbi_name
    END IF

  ELSE  --User pressed abort
    #CALL tl_msg_input_abort(get_str_tool(920),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL, NULL, NULL
  END IF

END FUNCTION


#################################################
# FUNCTION tb_input(p_tb_rec)
#
# Input tb details (edit/create)
#
# RETURN l_tb.*
#################################################
FUNCTION tb_input(p_new_flag,p_tb_rec)
  DEFINE 
    p_new_flag                SMALLINT,
    p_tb_rec                  RECORD LIKE qxt_tb.*,
    l_tb_form_rec             OF t_qxt_tb_form_rec,
    l_original_application_id LIKE qxt_tb.application_id,
    l_original_tb_name        LIKE qxt_tb.tb_name,
    l_original_tb_instance    LIKE qxt_tb.tb_instance,
    l_original_tbi_name       LIKE qxt_tb.tbi_name,
    local_debug               SMALLINT,
    tmp_str                   VARCHAR(250),

    tmp_application_id        LIKE qxt_tbi.application_id,
    tmp_tb_event_name         LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    tmp_icon_category_id      LIKE qxt_icon_category.icon_category_id,
    tmp_tb_name               LIKE qxt_tb.tb_name,
    tmp_tbi_name              LIKE qxt_tb.tbi_name


  LET local_debug = FALSE

  LET int_flag = FALSE

  #Some NULL corrections
  IF p_tb_rec.application_id IS NULL THEN
    LET p_tb_rec.application_id = get_current_application_id()
  END IF

  IF p_tb_rec.tb_name IS NULL THEN
    LET p_tb_rec.tb_name = "Global"
  END IF

  IF p_tb_rec.tb_instance IS NULL THEN
    LET p_tb_rec.tb_instance = 1 
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_original_application_id = p_tb_rec.application_id
  LET l_original_tb_name = p_tb_rec.tb_name
  LET l_original_tb_instance = p_tb_rec.tb_instance
  LET l_original_tbi_name = p_tb_rec.tbi_name

  #copy record data to form_record format record
  CALL copy_tb_record_to_form_record(p_tb_rec.*) RETURNING l_tb_form_rec.*


  INPUT BY NAME l_tb_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      IF p_tb_rec.tbi_name IS NOT NULL THEN
        CALL populate_tb_preview_edit_g(l_tb_form_rec.*)
      END IF



    # Action/Key events
    ON ACTION("newTbi")  --Create new menu_item tbi
      CALL tbi_create(NULL,NULL) 
        RETURNING  
          tmp_application_id,
          tmp_tbi_name

      IF tmp_application_id IS NOT NULL THEN
        LET l_tb_form_rec.application_name = get_application_name(tmp_application_id) 
      END IF

      IF tmp_tbi_name IS NOT NULL THEN
        LET l_tb_form_rec.tbi_name = tmp_tbi_name
      END IF

      CALL populate_tb_combo_lists_g(p_tb_rec.application_id )

      DISPLAY BY NAME l_tb_form_rec.application_name
      DISPLAY BY NAME l_tb_form_rec.tbi_name

      CALL populate_tb_preview_edit_g(l_tb_form_rec.*)


    ON ACTION("manageTbi")  --Manage Menu Item
      #tbi_popup(p_application_id,p_language_id,p_tbi_name,p_order_field,p_accept_scope)
      CALL tbi_popup(get_application_id(l_tb_form_rec.application_name),get_data_language_id(),l_tb_form_rec.tbi_name,NULL,0)
        RETURNING  
          tmp_application_id,
          tmp_tbi_name

      IF tmp_application_id IS NOT NULL THEN
        LET l_tb_form_rec.application_name = get_application_name(tmp_application_id) 
      END IF

      IF tmp_tbi_name IS NOT NULL THEN
        LET l_tb_form_rec.tbi_name = tmp_tbi_name
      END IF

      CALL populate_tb_combo_lists_g(p_tb_rec.application_id )

      DISPLAY BY NAME l_tb_form_rec.application_name
      DISPLAY BY NAME l_tb_form_rec.tbi_name
      
      CALL populate_tb_preview_edit_g(l_tb_form_rec.*)


    ON ACTION("editTbi")  --Edit Menu Item
      CALL tbi_edit(get_application_id(l_tb_form_rec.application_name),l_tb_form_rec.tbi_name)
        RETURNING  
          tmp_application_id,
          tmp_tbi_name

      IF tmp_application_id IS NOT NULL THEN
        LET l_tb_form_rec.application_name = get_application_name(tmp_application_id) 
      END IF

      IF tmp_tbi_name IS NOT NULL THEN
        LET l_tb_form_rec.tbi_name = tmp_tbi_name
      END IF

      CALL populate_tb_combo_lists_g(p_tb_rec.application_id )

      DISPLAY BY NAME l_tb_form_rec.application_name
      DISPLAY BY NAME l_tb_form_rec.tbi_name

      CALL populate_tb_preview_edit_g(l_tb_form_rec.*)




    ############################
    #After field events
    ############################

    AFTER FIELD application_name
      #application_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(921), l_tb_form_rec.application_name,NULL,TRUE)  THEN
        NEXT FIELD application_name
      END IF

    AFTER FIELD tb_name
      #tb_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(922), l_tb_form_rec.tb_name,NULL,TRUE)  THEN
        NEXT FIELD tb_name
      END IF

    AFTER FIELD tb_instance
      #tb_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(923), l_tb_form_rec.tb_instance,NULL,TRUE)  THEN
        NEXT FIELD tb_instance
      END IF


    AFTER FIELD tbi_name
      #tb_tbi_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(924), l_tb_form_rec.tbi_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_name
      END IF
      
      IF l_tb_form_rec.tbi_name IS NOT NULL THEN
        CALL populate_tb_preview_edit_g(l_tb_form_rec.*)
      END IF



    # AFTER INPUT BLOCK ####################################
    AFTER INPUT
      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF


     #application_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(921), l_tb_form_rec.application_name,NULL,TRUE)  THEN
        NEXT FIELD application_name
        CONTINUE INPUT
      END IF

      #tb_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(922), l_tb_form_rec.tb_name,NULL,TRUE)  THEN
        NEXT FIELD tb_name
        CONTINUE INPUT
      END IF

      #tb_instance must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(923), l_tb_form_rec.tb_instance,NULL,TRUE)  THEN
        NEXT FIELD tb_instance
        CONTINUE INPUT
      END IF


      #tbi_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(924), l_tb_form_rec.tbi_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_name
        CONTINUE INPUT
      END IF


      #All four fields build the primary key
      #Check for primary key validation
      IF p_new_flag THEN  --new records are never allowed to have duplicate existing primary keys
        IF get_tb_application_id_and_name_and_instance_and_tbi_count(
          get_application_id(l_tb_form_rec.application_name),
          l_tb_form_rec.tb_name,
          l_tb_form_rec.tb_instance,
          l_tb_form_rec.tbi_name) THEN

          #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_fourfoldkey(get_str_tool(921),get_str_tool(922),get_str_tool(923),get_str_tool(924),NULL)
          NEXT FIELD application_name
          CONTINUE INPUT

        END IF

      ELSE  --edit records can have duplicate, if they use the original keys
        IF l_original_application_id <> get_application_id(l_tb_form_rec.application_name) 
          OR l_original_tb_name <> l_tb_form_rec.tb_name 
          OR l_original_tb_instance <> l_tb_form_rec.tb_instance   
          OR l_original_tbi_name <> l_tb_form_rec.tbi_name THEN  

          IF get_tb_application_id_and_name_and_instance_and_tbi_count(
            get_application_id(l_tb_form_rec.application_name),
            l_tb_form_rec.tb_name,
            l_tb_form_rec.tb_instance,
            l_tb_form_rec.tbi_name) THEN

            #Constraint only exists for newly created records (not modify)
            CALL  tl_msg_duplicate_fourfoldkey(get_str_tool(921),get_str_tool(922),get_str_tool(923),get_str_tool(924),NULL)
            NEXT FIELD application_name
            CONTINUE INPUT
          END IF
        END IF
      END IF

  END INPUT
  # END INPUT BLOCK  ##########################

  IF local_debug THEN
    DISPLAY "tb_input() - l_tb_form_rec.application_name=",    l_tb_form_rec.application_name
    DISPLAY "tb_input() - l_tb_form_rec.tb_name=",             l_tb_form_rec.tb_name
    DISPLAY "tb_input() - l_tb_form_rec.tb_instance=",         l_tb_form_rec.tb_instance
    DISPLAY "tb_input() - l_tb_form_rec.tbi_name=",            l_tb_form_rec.tbi_name


  END IF


  #Copy the form record data to a normal tb record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_tb_form_record_to_record(l_tb_form_rec.*) RETURNING p_tb_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "tb_input() - p_tb_rec.application_id=",  p_tb_rec.application_id
    DISPLAY "tb_input() - p_tb_rec.tb_name=",         p_tb_rec.tb_name
    DISPLAY "tb_input() - p_tb_rec.tb_instance=",     p_tb_rec.tb_instance
    DISPLAY "tb_input() - p_tb_rec.tbi_name=",     p_tb_rec.tbi_name

  END IF

  RETURN p_tb_rec.*

END FUNCTION


#################################################
# FUNCTION tb_delete(p_tb_name)
#
# Delete a tb record
#
# RETURN NONE
#################################################
FUNCTION tb_delete(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE 
    p_application_id      LIKE qxt_tb.application_id,
    p_tb_name             LIKE qxt_tb.tb_name,
    p_tb_instance         LIKE qxt_tb.tb_instance,
    p_tbi_name            LIKE qxt_tb.tbi_name

  #do you really want to delete...
  IF question_delete_record(get_str_tool(920), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_tb 
        WHERE application_id = p_application_id
          AND tb_name        = p_tb_name
          AND tb_instance    = p_tb_instance 
          AND tbi_name       = p_tbi_name
    COMMIT WORK

    RETURN TRUE
  ELSE
    RETURN FALSE
  END IF

END FUNCTION


######################################################
# FUNCTION copy_tb_record_to_form_record(p_tb_rec)  
#
# Copy normal tb record data to type tb_form_rec
#
# RETURN l_tb_form_rec.*
######################################################
FUNCTION copy_tb_record_to_form_record(p_tb_rec)  
  DEFINE
    p_tb_rec        RECORD LIKE qxt_tb.*,
    l_tb_form_rec   OF t_qxt_tb_form_rec,
    local_debug     SMALLINT

  LET local_debug = FALSE
  LET l_tb_form_rec.application_name =     get_application_name(p_tb_rec.application_id)
  LET l_tb_form_rec.tb_name =              p_tb_rec.tb_name
  LET l_tb_form_rec.tb_instance =          p_tb_rec.tb_instance
  LET l_tb_form_rec.tbi_name =             p_tb_rec.tbi_name


  IF local_debug THEN
    DISPLAY "copy_tb_record_to_form_record() - p_tb_rec.application_id=",        p_tb_rec.application_id
    DISPLAY "copy_tb_record_to_form_record() - p_tb_rec.tb_name=",               p_tb_rec.tb_name
    DISPLAY "copy_tb_record_to_form_record() - p_tb_rec.tb_instance=",           p_tb_rec.tb_instance
    DISPLAY "copy_tb_record_to_form_record() - p_tb_rec.tbi_name=",              p_tb_rec.tbi_name

    DISPLAY "copy_tb_record_to_form_record() - l_tb_form_rec.application_name=", l_tb_form_rec.application_name
    DISPLAY "copy_tb_record_to_form_record() - l_tb_form_rec.tb_name=",          l_tb_form_rec.tb_name
    DISPLAY "copy_tb_record_to_form_record() - l_tb_form_rec.tb_instance=",      l_tb_form_rec.tb_instance
    DISPLAY "copy_tb_record_to_form_record() - l_tb_form_rec.tbi_name=",         l_tb_form_rec.tbi_name

  END IF

  RETURN l_tb_form_rec.*

END FUNCTION


######################################################
# FUNCTION copy_tb_form_record_to_record(p_tb_form_rec)  
#
# Copy type tb_form_rec to normal tb record data
#
# RETURN l_tb_rec.*
######################################################
FUNCTION copy_tb_form_record_to_record(p_tb_form_rec)  
  DEFINE
    l_tb_rec       RECORD LIKE qxt_tb.*,
    p_tb_form_rec  OF t_qxt_tb_form_rec,
    local_debug     SMALLINT

  LET local_debug = FALSE

  LET l_tb_rec.application_id =   get_application_id(p_tb_form_rec.application_name)
  LET l_tb_rec.tb_name =          p_tb_form_rec.tb_name
  LET l_tb_rec.tb_instance =      p_tb_form_rec.tb_instance
  LET l_tb_rec.tbi_name =         p_tb_form_rec.tbi_name


  IF local_debug THEN
    DISPLAY "copy_tb_form_record_to_record() - p_tb_form_rec.application_name=",  p_tb_form_rec.application_name
    DISPLAY "copy_tb_form_record_to_record() - p_tb_form_rec.tb_name=",           p_tb_form_rec.tb_name
    DISPLAY "copy_tb_form_record_to_record() - p_tb_form_rec.tb_instance=",       p_tb_form_rec.tb_instance
    DISPLAY "copy_tb_form_record_to_record() - p_tb_form_rec.tbi_name=",          p_tb_form_rec.tbi_name

    DISPLAY "copy_tb_form_record_to_record() - l_tb_rec.application_id=",         l_tb_rec.application_id
    DISPLAY "copy_tb_form_record_to_record() - l_tb_rec.tb_name=",                l_tb_rec.tb_name
    DISPLAY "copy_tb_form_record_to_record() - l_tb_rec.tb_instance=",            l_tb_rec.tb_instance
    DISPLAY "copy_tb_form_record_to_record() - l_tb_rec.tbi_name=",               l_tb_rec.tbi_name

  END IF

  RETURN l_tb_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

#################################################
# FUNCTION tb_view(p_tb_name)
#
# View tb record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION tb_view(p_application_id,p_tb_name,p_tb_instance,p_tbi_name)
  DEFINE 
    p_application_id  LIKE qxt_tb.application_id,
    p_tb_name         LIKE qxt_tb.tb_name,
    p_tb_instance     LIKE qxt_tb.tb_instance,
    p_tbi_name        LIKE qxt_tb.tbi_name,
    l_tb_rec          RECORD LIKE qxt_tb.*

  CALL get_tb_rec(p_application_id,p_tb_name,p_tb_instance,p_tbi_name) 
    RETURNING l_tb_rec.*

  CALL tb_view_by_rec(l_tb_rec.*)

END FUNCTION


#################################################
# FUNCTION tb_view_by_rec(p_tb_rec)
#
# View tb record in window-form
#
# RETURN NONE
#################################################
FUNCTION tb_view_by_rec(p_tb_rec)
  DEFINE 
    p_tb_rec  RECORD LIKE qxt_tb.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)
#	call fgl_winmessage("open w_tb","open w_tb","info")
	CALL fgl_window_open("w_tb", 3, 3, get_form_path("f_qxt_tb_det_l2"), FALSE) 
	CALL populate_tb_form_view_labels_g(p_tb_rec.application_id )

  #Update icon preview
  CALL icon_preview(get_tb_icon_filename(NULL,p_tb_rec.tb_name,NULL))

  DISPLAY BY NAME p_tb_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  
	#call fgl_winmessage("close w_tb","close w_tb","info")
  CALL fgl_window_close("w_tb")
END FUNCTION



####################################################
# FUNCTION grid_header_tb_scroll()
#
# Populate tb grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_tb_scroll()
  CALL fgl_grid_header("sc_tb","application_name",             get_str_tool(921),"left","F13") 
  CALL fgl_grid_header("sc_tb","tb_name",                      get_str_tool(922),"left","F14") 
  CALL fgl_grid_header("sc_tb","tb_instance",                  get_str_tool(923),"right" ,"F15")  
  CALL fgl_grid_header("sc_tb","tbi_name",                     get_str_tool(924),"left" ,"F16") 
  CALL fgl_grid_header("sc_tb","event_type_name",              get_str_tool(925),"left" ,"F17")
  CALL fgl_grid_header("sc_tb","tbi_event_name",               get_str_tool(926),"left" ,"F18")
  CALL fgl_grid_header("sc_tb","tbi_action_name",          get_str_tool(927),"left" ,"F19")  
  CALL fgl_grid_header("sc_tb","tbi_scope_name",               get_str_tool(928),"left" ,"F20") 
  CALL fgl_grid_header("sc_tb","tbi_position",                 get_str_tool(929),"right" ,"F21") 
  CALL fgl_grid_header("sc_tb","tbi_static_name",              get_str_tool(930),"left" ,"F22")  
  CALL fgl_grid_header("sc_tb","icon_filename",                get_str_tool(931),"left" ,"F23")  
  CALL fgl_grid_header("sc_tb","string_data",                  get_str_tool(932),"left" ,"F24")  
 
END FUNCTION


#######################################################
# FUNCTION populate_tb_combo_lists_g()
#
# Populate all combo lists in the form
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_combo_lists_g(p_application_id)
  DEFINE
    i                SMALLINT,
    p_application_id LIKE qxt_application.application_id

  #Populate the combo list boxes dynamic from DB
  CALL application_name_combo_list("application_name")
  CALL tb_name_combo_list(get_current_application_id(),"tb_name")

  FOR i = 0 TO 10
    CALL fgl_list_set("tb_instance",i+1, i)
  END FOR

  CALL tbi_one_application_combo_list(p_application_id,"tbi_name")

END FUNCTION



#######################################################
# FUNCTION populate_tb_preview_edit_g(p_tb_form_rec)
#
# Populate tb edit form view fields
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_preview_edit_g(p_tb_form_rec)
  DEFINE p_tb_form_rec OF t_qxt_tb_form_rec

  DISPLAY get_event_type_name(get_tbi_event_type_id(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)) TO event_type_name
  DISPLAY get_tbi_event_name(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name) TO tbi_event_name
  DISPLAY get_tbi_action_name_from_tbi_obj_name(get_tbi_obj_name(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)) TO tbi_action_name
  DISPLAY get_tbi_scope_name(get_tbi_scope_id_from_menu_item_name(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)) TO tbi_scope_name
  DISPLAY get_tbi_position(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name) TO tbi_position
  DISPLAY get_tbi_static_name(get_tbi_static(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)) TO tbi_static_name
  DISPLAY get_tbi_icon_filename(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name) TO icon_filename
  CALL icon_preview(get_tbi_icon_filename(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name))

  DISPLAY get_tbi_label_data(get_tbi_obj_string_id(get_tbi_obj_name(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)),get_language()) TO label_data
  DISPLAY get_tbi_tooltip_data(get_tbi_obj_string_id(get_tbi_obj_name(get_application_id(p_tb_form_rec.application_name),p_tb_form_rec.tbi_name)),get_language()) TO string_data

END FUNCTION



#######################################################
# FUNCTION populate_tb_preview_scroll_g(p_tb_form_rec)
#
# Populate tb scroll form view fields
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_preview_scroll_g(p_tb_form_rec)
  DEFINE 
    p_tb_form_rec OF t_qxt_tb_form_rec2

  DISPLAY p_tb_form_rec.application_name TO pv_app_name
  DISPLAY p_tb_form_rec.tb_name TO pv_tb_name
  DISPLAY p_tb_form_rec.tb_instance TO pv_tb_instance
  DISPLAY p_tb_form_rec.tbi_name TO pv_tbi_name
  DISPLAY p_tb_form_rec.event_type_name TO pv_event_type_name
  DISPLAY p_tb_form_rec.tbi_event_name TO pv_tbi_event_name

  DISPLAY p_tb_form_rec.tbi_action_name TO pv_tbi_action_name
  DISPLAY p_tb_form_rec.tbi_scope_name TO pv_tbi_scope_name
  DISPLAY p_tb_form_rec.tbi_position TO pv_tbi_position
  DISPLAY p_tb_form_rec.tbi_static_name TO pv_tbi_static_name
  DISPLAY p_tb_form_rec.icon_filename TO pv_icon_filename
  DISPLAY p_tb_form_rec.string_data TO pv_string_data

END FUNCTION

#######################################################
# FUNCTION populate_tb_form_labels_g()
#
# Populate tb form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_labels_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL fgl_settitle(get_str_tool(920))

  DISPLAY get_str_tool(920) TO lbTitle

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9

  DISPLAY get_str_tool(930) TO dl_f10  --Category
  DISPLAY get_str_tool(931) TO dl_f11  --tooltip
  DISPLAY get_str_tool(932) TO dl_f12  --Icon
  DISPLAY get_str_tool(933) TO dl_f13  --Icon
  #DISPLAY get_str_tool(934) TO dl_f14  --String id
  #DISPLAY get_str_tool(935) TO dl_f15  --String data
  #DISPLAY get_str_tool(936) TO dl_f16  --Tooltip
  #DISPLAY get_str_tool(937) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(18) TO bt_new_tbi
  #DISPLAY "!" TO bt_new_tbi

  DISPLAY get_str_tool(21) TO bt_manage_tbi
  #DISPLAY "!" TO bt_manage_tbi

  #Populate the combo list boxes dynamic from DB
  CALL populate_tb_combo_lists_g(p_application_id)

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tb_form_labels_t()
#
# Populate tb form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_labels_t(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  DISPLAY get_str_tool(920) TO lbTitle

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9
  DISPLAY get_str_tool(930) TO dl_f10

  DISPLAY get_str_tool(934) TO dl_f11  --tooltip
  DISPLAY get_str_tool(931) TO dl_f12  --Icon
  DISPLAY get_str_tool(935) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tb_form_edit_labels_g()
#
# Populate tb form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_edit_labels_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL fgl_settitle(trim(get_str_tool(920)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9

  DISPLAY get_str_tool(930) TO dl_f10  --Category
  DISPLAY get_str_tool(931) TO dl_f11  --tooltip
  DISPLAY get_str_tool(932) TO dl_f12  --Icon
  DISPLAY get_str_tool(933) TO dl_f13  --Icon
  #DISPLAY get_str_tool(934) TO dl_f14  --String id
  #DISPLAY get_str_tool(935) TO dl_f15  --String data
  #DISPLAY get_str_tool(936) TO dl_f16  --Tooltip
  #DISPLAY get_str_tool(937) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(18) TO bt_new_tbi
  #DISPLAY "!" TO bt_new_tbi

  DISPLAY get_str_tool(21) TO bt_manage_tbi
  #DISPLAY "!" TO bt_manage_tbi

  DISPLAY get_str_tool(7) TO bt_edit_tbi
  #DISPLAY "!" TO bt_edit_tbi

  #Populate the combo list boxes dynamic from DB
  CALL populate_tb_combo_lists_g(p_application_id)

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tb_form_edit_labels_t()
#
# Populate tb form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_edit_labels_t(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9
  DISPLAY get_str_tool(930) TO dl_f10

  DISPLAY get_str_tool(934) TO dl_f11  --tooltip
  DISPLAY get_str_tool(931) TO dl_f12  --Icon
  DISPLAY get_str_tool(935) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tb_form_view_labels_g()
#
# Populate tb form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_view_labels_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL fgl_settitle(trim(get_str_tool(920)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9

  DISPLAY get_str_tool(930) TO dl_f10  --Category
  DISPLAY get_str_tool(931) TO dl_f11  --tooltip
  DISPLAY get_str_tool(932) TO dl_f12  --Icon
  DISPLAY get_str_tool(933) TO dl_f13  --Icon
  #DISPLAY get_str_tool(934) TO dl_f14  --String id
  #DISPLAY get_str_tool(935) TO dl_f15  --String data
  #DISPLAY get_str_tool(936) TO dl_f16  --Tooltip
  #DISPLAY get_str_tool(937) TO dl_f17  --Preview

  DISPLAY get_str_tool(402) TO lbInfo1

  DISPLAY get_str_tool(3) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(18) TO bt_new_tbi
  DISPLAY "!" TO bt_new_tbi

  DISPLAY get_str_tool(21) TO bt_manage_tbi
  DISPLAY "!" TO bt_manage_tbi

  DISPLAY get_str_tool(7) TO bt_edit_tbi
  DISPLAY "!" TO bt_edit_tbi

  #Populate the combo list boxes dynamic from DB
  CALL populate_tb_combo_lists_g(p_application_id)


  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tb_form_view_labels_t()
#
# Populate tb form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_view_labels_t(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9
  DISPLAY get_str_tool(930) TO dl_f10

  DISPLAY get_str_tool(934) TO dl_f11  --tooltip
  DISPLAY get_str_tool(931) TO dl_f12  --Icon
  DISPLAY get_str_tool(935) TO dl_f13  --Preview

  DISPLAY get_str_tool(402) TO lbInfo1

  #CALL language_combo_list("language_dir")

END FUNCTION


#######################################################
# FUNCTION populate_tb_form_create_labels_g()
#
# Populate tb form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_create_labels_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL fgl_settitle(trim(get_str_tool(920)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9

  DISPLAY get_str_tool(930) TO dl_f10  --Category
  DISPLAY get_str_tool(931) TO dl_f11  --tooltip
  DISPLAY get_str_tool(932) TO dl_f12  --Icon
  DISPLAY get_str_tool(933) TO dl_f13  --Icon
  #DISPLAY get_str_tool(934) TO dl_f14  --String id
  #DISPLAY get_str_tool(935) TO dl_f15  --String data
  #DISPLAY get_str_tool(936) TO dl_f16  --Tooltip
  #DISPLAY get_str_tool(937) TO dl_f17  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #New / Manage sub-menu buttons
  DISPLAY get_str_tool(18) TO bt_new_tbi
  #DISPLAY "!" TO bt_new_tbi

  DISPLAY get_str_tool(21) TO bt_manage_tbi
  #DISPLAY "!" TO bt_manage_tbi

  DISPLAY get_str_tool(7) TO bt_edit_tbi
  #DISPLAY "!" TO bt_edit_tbi

  #Populate the combo list boxes dynamic from DB
  CALL populate_tb_combo_lists_g(p_application_id)

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tb_form_create_labels_t()
#
# Populate tb form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_form_create_labels_t(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9
  DISPLAY get_str_tool(930) TO dl_f10

  DISPLAY get_str_tool(934) TO dl_f11  --tooltip
  DISPLAY get_str_tool(931) TO dl_f12  --Icon
  DISPLAY get_str_tool(935) TO dl_f13  --Preview

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tb_list_form_labels_g()
#
# Populate tb list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_list_form_labels_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id
  CALL updateUILabel("filter_application_name_switch","Application")
  CALL updateUILabel("filter_tb_name_switch","Name")
  CALL updateUILabel("filter_tb_instance_switch","Instance")
  CALL fgl_settitle(trim(get_str_tool(920)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_tb_scroll()

  #DISPLAY get_str_tool(921) TO dl_f1
  #DISPLAY get_str_tool(922) TO dl_f2
  #DISPLAY get_str_tool(923) TO dl_f3
  #DISPLAY get_str_tool(924) TO dl_f4
  #DISPLAY get_str_tool(925) TO dl_f5
  #DISPLAY get_str_tool(926) TO dl_f6
  #DISPLAY get_str_tool(927) TO dl_f7
  #DISPLAY get_str_tool(928) TO dl_f8
  #DISPLAY get_str_tool(929) TO dl_f9
  #DISPLAY get_str_tool(930) TO dl_f10  --Category 1
  #DISPLAY get_str_tool(931) TO dl_f11 --Category 2
  #DISPLAY get_str_tool(932) TO dl_f12 --Category 3
  #DISPLAY get_str_tool(933) TO dl_f13
  #DISPLAY get_str_tool(934) TO dl_f14
  #DISPLAY get_str_tool(935) TO dl_f15
  #DISPLAY get_str_tool(936) TO dl_f16  --Tooltip

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete


  DISPLAY get_str_tool(22) TO bt_clone
  #DISPLAY "!" TO bt_clone

  #Enbable filter check box
  #DISPLAY "!" TO filter_application_name_switch
  #DISPLAY "!" TO filter_tb_name_switch
  #DISPLAY "!" TO filter_tb_instance_switch

  #Enable filter combo
  #DISPLAY "!" TO filt_app
  #DISPLAY "!" TO filt_name
  #DISPLAY "!" TO filt_inst

  #Enable filter SET button
  #DISPLAY "!" TO bt_set_filter_criteria_1
  #DISPLAY "!" TO bt_set_filter_criteria_2
  #DISPLAY "!" TO bt_set_filter_criteria_3
  #DISPLAY "!" TO bt_set_filter_language_name

  #Update all combo list boxes in the scroll form
  CALL populate_tb_scroll_combo_lists_g(p_application_id)


  #CALL icon_category_data_combo_list("cs1",get_language())
  #CALL icon_category_data_combo_list("cs2",get_language())
  #CALL icon_category_data_combo_list("cs3",get_language())

  #Initialise icon preview
  CALL icon_preview(NULL)


END FUNCTION

#######################################################
# FUNCTION populate_tb_scroll_combo_lists_g(l_application_id)
#
# Populate all combo lists in the SCROLL form
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_scroll_combo_lists_g(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  CALL application_name_combo_list("filter_application_name")
  CALL tb_name_combo_list(p_application_id,"filter_tb_name")
  CALL language_combo_list("filter_language_name")

END FUNCTION

#######################################################
# FUNCTION populate_tb_list_form_labels_t()
#
# Populate tb list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tb_list_form_labels_t(p_application_id)
  DEFINE
    p_application_id LIKE qxt_application.application_id

  DISPLAY trim(get_str_tool(920)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(921) TO dl_f1
  DISPLAY get_str_tool(922) TO dl_f2
  DISPLAY get_str_tool(923) TO dl_f3
  DISPLAY get_str_tool(924) TO dl_f4
  DISPLAY get_str_tool(925) TO dl_f5
  DISPLAY get_str_tool(926) TO dl_f6
  DISPLAY get_str_tool(927) TO dl_f7
  DISPLAY get_str_tool(928) TO dl_f8
  DISPLAY get_str_tool(929) TO dl_f9
  DISPLAY get_str_tool(930) TO dl_f10

  DISPLAY get_str_tool(934) TO dl_f11  --tooltip
  DISPLAY get_str_tool(931) TO dl_f12  --Icon
  DISPLAY get_str_tool(935) TO dl_f13  --Preview  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION





{
FUNCTION tb_module_test()
  DEFINE
    l_tb_rec RECORD LIKE qxt_tb.*,
    l_tb_key_rec 
      RECORD
        application_id SMALLINT,
        tb_name        VARCHAR(30),
        tb_instance    SMALLINT

      END RECORD

  CALL get_tb_manager_lib_info()
  #CALL get_tb_instance_from_menu_item_name(p_application_id,p_tb_name)
  DISPLAY "get_tb_event_name(1,\"test\",1)=", get_tb_event_name(1,"test",1)
  DISPLAY "get_tb_tbi_name(1,\"test\",1)=", get_tb_tbi_name(1,"test",1)
  DISPLAY "get_tb_static(1,\"test\",1) =", get_tb_static(1,"test",1)
  DISPLAY "get_tb_position(1,\"test\",1) = ", get_tb_position(1,"test",1)

  DISPLAY "get_tb_rec(1,\"test\",1,\"GlobalExitF12\")"
  CALL get_tb_rec(1,"test",1,"GlobalExitF12") RETURNING l_tb_rec.*
  DISPLAY "  -> l_tb_rec.application_id=", l_tb_rec.application_id
  DISPLAY "  -> l_tb_rec.tb_name=", l_tb_rec.tb_name
  DISPLAY "  -> l_tb_rec.tb_instance=", l_tb_rec.tb_instance
  DISPLAY "  -> l_tb_rec.tbi_name=", l_tb_rec.tbi_name

  DISPLAY "get_tb_icon_filename(1,\"test\",1)=", get_tb_icon_filename(1,"test",1)
  DISPLAY "get_tb_count()=", get_tb_count()
  DISPLAY "get_tb_application_id_and_name_and_instance_count(1,\"test\",1) =", get_tb_application_id_and_name_and_instance_count(1,"test",1)
  DISPLAY "get_tb_one_application_count(1)=", get_tb_one_application_count(1)
  DISPLAY "tb_name_and_language_count(1,\"test\")=",tb_name_and_language_count(1,"test")
  DISPLAY "CALL tb_name_combo_list(1,\"test\")"
  CALL tb_name_combo_list(1,"test")
  
  DISPLAY "CALL tb_create(NULL,NULL,NULL,NULL)"
  CALL tb_create(NULL,NULL,NULL,NULL) RETURNING l_tb_key_rec.*
  DISPLAY "  -> l_tb_key_rec.application_id=", l_tb_key_rec.application_id
  DISPLAY "  -> l_tb_key_rec.tb_name=", l_tb_key_rec.tb_name
  DISPLAY "  -> l_tb_key_rec.tb_instance=", l_tb_key_rec.tb_instance

  DISPLAY "CALL tb_edit(1,\"test\",1)"
  CALL tb_edit(1,"test",1) RETURNING l_tb_key_rec.*
  DISPLAY "  -> l_tb_key_rec.application_id=", l_tb_key_rec.application_id
  DISPLAY "  -> l_tb_key_rec.tb_name=", l_tb_key_rec.tb_name
  DISPLAY "  -> l_tb_key_rec.tb_instance=", l_tb_key_rec.tb_instance

  DISPLAY "CALL tb_delete(1,\"test\",1)"
  CALL tb_delete(1,"test",1)

  DISPLAY "CALL tb_popup(NULL,NULL,NULL,NULL,2)"
  CALL tb_popup(NULL,NULL,NULL,NULL,2)
  
  DISPLAY "CALL tb_popup(1,\"test\",1,NULL,2)"
  CALL tb_popup(1,"test",1,NULL,2)

END FUNCTION

}

###########################################################################################################################
# EOF
###########################################################################################################################








































