##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################################################
# This program creates the database schema and test data in the test database cms
###################################################################################

###################################################################################
# DATABASE
###################################################################################
#DATABASE cms

###################################################################################
# GLOBALS
###################################################################################
GLOBALS
  #DEFINE window_open SMALLINT
  DEFINE
    monitor STRING,
    retval SMALLINT,
    db_state_str STRING,
    db_connect_msg STRING,
    db_name STRING,
    db_version VARCHAR(10),
    db_build VARCHAR(10),
    cms_db_state SMALLINT,
    table_name_list       DYNAMIC ARRAY OF STRING, --VARCHAR(18),
    inst_table_count      SMALLINT,
    qxt_table_count       SMALLINT,
    cms_table_count       SMALLINT,
    app_type              SMALLINT

  DEFINE t_cms_info TYPE AS 
    RECORD
      db_version VARCHAR(10),
      db_build   VARCHAR(10),
      db_other   vARCHAR(10)
    END RECORD


END GLOBALS

###################################################################################
# MAIN
###################################################################################
MAIN
  DEFINE
    i INTEGER,
    ret SMALLINT,
    db_driver_str STRING,
    l_cms_info OF t_cms_info

  CALL fgl_putenv("DBDATE=dmy4/")
  CALL fgl_putenv("LINES=24")
  CALL fgl_putenv("COLUMNS=90")  

  OPTIONS
    HELP KEY F1,
    HELP FILE "msg/db_tools.erm"

  WHENEVER ERROR STOP

  DEFER INTERRUPT


#?????? still needs to be converted to cms_2016 modular structure
	#default toolbar buttons (event/key to toolbar button matching)
	#CALL publish_toolbar("Global",0)


  IF arg_val(1) IS NOT NULL THEN
    LET db_name = arg_val(1)
  END IF

  CALL ini_table_name_list()

  #Default is cms
  LET app_type = 0
  LET inst_table_count = cms_table_count


  CALL fgl_settitle("CMS Database Tool")


  CALL fgl_form_open("f_db","form/db_tools_l2")
  CALL fgl_form_display("f_db")
  DISPLAY "!" TO bt_db_set
  DISPLAY "!" TO bt_db_connect


  #DISPLAY HYDRA_DB_DRIVER
  LET db_driver_str = fgl_getenv("HYDRA_DB_DRIVER")
  DISPLAY  db_driver_str TO dl_hydra_db_driver
  LET db_name = "cms"

  #Display DBDATE Status
  DISPLAY fgl_getenv("DBDATE") TO dl_dbdate_format

  LET monitor = add_monitor_str(monitor, "DB Tool",0)
  CALL display_monitor(monitor)

  
  #Try to connect to database
  CALL connect_db_tools(db_name)
    RETURNING  retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*

{

  WHENEVER ERROR CONTINUE
    DISPLAY db_name TO db_name
    CALL qxt_open_db(db_name) RETURNING retval, db_state_str, db_connect_msg
  WHENEVER ERROR STOP
  #WHENEVER ERROR CALL error_func

  #Update Monitor window
  CALL db_connect_monitor_msg(retval)

  #if connection successful, try to read the cms database version & build information
  #and display it
  IF retval >= 0 THEN
    CALL db_info("cms")  
  END IF
}

  MENU "DB-Create"
    COMMAND "(Re-)Create Database"  HELP 100
      CALL all_tables(-1,0)

    COMMAND "All Tables"
      MENU "All Tables"

        COMMAND "Drop ALL Tables"  HELP 100
          CALL all_tables(0,NULL)

        COMMAND "Create ALL Tables"  HELP 100
          CALL all_tables(1,NULL)

        COMMAND "Populate ALL Tables"  HELP 100
          MENU "Populate"
            COMMAND "Populate from UNL"
              CALL all_tables(2,0)
            COMMAND "Populate from BAK"  HELP 100
              CALL all_tables(2,1)
            COMMAND KEY(F23) "Set Database"
              CALL db_setting()

            COMMAND KEY(F24) "Connect"
              CALL connect_db_tools(db_name)
                RETURNING  retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*

            COMMAND "Return"
              EXIT MENU
          END MENU


          

        COMMAND "Unload ALL Tables"  HELP 100
          MENU "Export"
            COMMAND "Unload to .UNL files"
              CALL all_tables(3,0)
            COMMAND "Unload to .BAK files"  HELP 100
              CALL all_tables(3,1)
            COMMAND KEY(F23) "Set Database"
              CALL db_setting()

            COMMAND KEY(F24) "Connect"
              CALL connect_db_tools(db_name)
                RETURNING  retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*

            COMMAND "Return"
              EXIT MENU
          END MENU
        COMMAND "Return"
          EXIT MENU
      END MENU
      
    COMMAND "Individual Tables"
      MENU "Individual Tables"

        COMMAND "Drop"
          CALL table_selection_menu(0,NULL)

        COMMAND "Create"
          CALL table_selection_menu(1,NULL)

        COMMAND "Load UNL"
          CALL table_selection_menu(2,0)

        COMMAND "Load BAK"
          CALL table_selection_menu(2,1)

        COMMAND "Unload UNL"
          CALL table_selection_menu(3,0)

        COMMAND "Unload BAK"
          CALL table_selection_menu(3,1)

        COMMAND "Return"
          EXIT MENU
      END MENU

    COMMAND "Monitor"
      MENU "Monitor"
        COMMAND "Monitor Input"  HELP 100
          INPUT monitor WITHOUT DEFAULTS FROM monitor_progress

        COMMAND "Clear Monitor"  HELP 100
          LET monitor = ""
          CALL display_monitor(monitor)
        COMMAND "Exit"  HELP 100
          EXIT MENU
      END MENU
    COMMAND "DB" HELP 100
      MENU "db_connect"
        COMMAND KEY(F23) "Set Database"
          CALL db_setting()

        COMMAND KEY(F24) "Connect"
          CALL connect_db_tools(db_name)
            RETURNING  retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*

        COMMAND "Return"
          EXIT MENU

      END MENU

    COMMAND "Exit"  HELP 100
      EXIT MENU

    COMMAND "Help"  HELP 100
      CALL showhelp(100)

    COMMAND KEY(F23) 
      CALL db_setting()          


    COMMAND KEY(F24) 
      CALL connect_db_tools(db_name)
        RETURNING  retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*


  END MENU

END MAIN


FUNCTION db_setting()

  INPUT db_name, app_type WITHOUT DEFAULTS FROM db_name, app_type HELP 100
    ON KEY (F23,F24, ACCEPT)
      CALL fgl_dialog_update_data()
      CASE app_type 
        WHEN 0   --cms
          LET  inst_table_count = cms_table_count
        WHEN 1   --just qxt
          LET  inst_table_count = qxt_table_count
        OTHERWISE
          CALL fgl_winmessage("Error in Main()","Invalid Option for Install Type (CMS or Qxt)","Error")
        END CASE

      EXIT INPUT
  END INPUT
END FUNCTION


FUNCTION connect_db_tools(db_name)
  DEFINE 
    db_name    STRING,
    l_cms_info OF t_cms_info 

  #Try to connect to database
  WHENEVER ERROR CONTINUE
    DISPLAY db_name TO db_name
    CALL qxt_open_db(db_name) RETURNING retval, db_state_str, db_connect_msg
  WHENEVER ERROR STOP
  #WHENEVER ERROR CALL error_func

  #Update Monitor window
  CALL db_connect_monitor_msg(retval)

  #if connection successful, try to read the cms database version & build information
  #and display it
  IF retval >= 0 THEN
    CALL db_info("cms") RETURNING cms_db_state, l_cms_info.*
  END IF

  RETURN retval, db_state_str, db_connect_msg, cms_db_state, l_cms_info.*

END FUNCTION

FUNCTION db_connect_monitor_msg(retval)
  DEFINE 
    retval SMALLINT,
    err_msg STRING
    
  IF retval < 0 THEN

    DISPLAY db_connect_msg TO dl_db_state ATTRIBUTE(RED)
    DISPLAY "Connection failed" TO monitor_progress ATTRIBUTE(RED)
    LET err_msg = "Cannot connect to database ", db_name CLIPPED, "\nYou need to create the specified database named ", db_name CLIPPED," before you can continue\nAlternatively, change the database (Set DB) Name and try to reconnect"
    CALL fgl_winmessage("Database Error",err_msg,"error")
    #EXIT PROGRAM
  ELSE
    DISPLAY db_connect_msg TO dl_db_state ATTRIBUTE(GREEN)
    DISPLAY "Connection successful" TO monitor_progress ATTRIBUTE(GREEN)

    CALL db_state_report()
  END IF
END FUNCTION


########################################################
# FUNCTION unload_file_name(str,bak)
########################################################
FUNCTION unload_file_name(file_name,bak)
  DEFINE
    file_name,ret STRING,
    bak SMALLINT,
    dir_name STRING
    
  LET dir_name = "unl"

   IF bak = 0 THEN
    LET ret = trim(dir_name), "/", trim(file_name), ".unl" 
    RETURN ret
  ELSE
    LET ret = trim(dir_name), "/", trim(file_name), ".bak" 
    RETURN ret
  END IF

END FUNCTION


########################################################
# FUNCTION splash(p_type,p_name,p_file_type)
#  CALL splash(p_action,p_table_name,p_file_type)
#    CALL splash(p_action,p_table_name,p_file_type)
########################################################
FUNCTION splash(p_action,p_name,p_file_type)
  DEFINE 
    p_action    SMALLINT,
    p_name      STRING,
    obj         STRING,
    tmp_str     STRING,
    p_file_type SMALLINT,
    file_name   STRING

  IF p_action >1 THEN
    CASE p_file_type
      WHEN 0
        LET file_name = trim(p_name), ".unl"
      WHEN 1
        LET file_name = trim(p_name), ".bak"
    ENd CASE
  END IF

  CASE p_action
    WHEN 0
      LET obj = "Dropping Table: ", trim(p_name), "..."   --  TO monitor  --AT 4,4
    WHEN 1
      LET obj = "Creating Table: ", trim(p_name), "..." 
    WHEN 2 
      LET obj = "Loading Table: ", trim(p_name), " from ", file_name   
    WHEN 3 
      LET obj = "Unloading Table: ", trim(p_name), " from ", file_name
    WHEN NULL
      #do nothing
    OTHERWISE
      LET tmp_str = "Invalid p_type argument in function splash()!\p_file_type = ", p_file_type
      CALL fgl_winmessage("Error in splash()",tmp_str, "error")
  END CASE


  IF fgl_fglgui() THEN
    LET monitor = add_monitor_str(monitor, obj,1)
    CALL display_monitor(monitor)
  ELSE 
    CALL display_monitor(obj)
  END IF

  #SLEEP 1
END FUNCTION


########################################################
# FUNCTION error_func()
########################################################
FUNCTION error_func()
  DEFINE msg STRING

  CALL fgl_winmessage("This is a custom error message fuction error_func()","This is a custom error message fuction error_func()","info")
  IF sqlca.sqlcode = 0 THEN
    RETURN
  END IF
  LET msg = "SQL Error: ", sqlca.sqlcode, "\nobj creation failed"
  CALL fgl_message_box(msg)
  ROLLBACK WORK
  EXIT PROGRAM
END FUNCTION


######################################################
# FUNCTION add_monitor_str(str, str2,level)
######################################################
FUNCTION add_monitor_str(str, str2,level)
  DEFINE str, str2 STRING,
    error_str STRING,
    level SMALLINT

  DISPLAY str2 TO dl_current_action 

  CASE level
    WHEN 0
      LET str = trim(str), "\n", trim(str2)
    WHEN 1
      LET str = trim(str), "\n\t", trim(str2)
    WHEN 2
      LET str = trim(str), "\n\t\t", trim(str2)
    WHEN 3
      LET str = trim(str), "\n\t\t\t", trim(str2)
    WHEN 4
      LET str = trim(str), "\n\t\t\t\t", trim(str2)
    OTHERWISE
      LET error_str = "Invalid level specified in add_monitor_str() level=" , level
      CALL fgl_winmessage("Internal 4gl source error - add_monitor_str(str, str2,level)",error_str,"error")
  END CASE


      
  RETURN trim(str)
END FUNCTION


######################################################
# FUNCTION display_monitor(str)
######################################################
FUNCTION display_monitor(str)
  DEFINE str STRING

  DISPLAY str TO monitor_progress ATTRIBUTE(GREEN)

END FUNCTION


########################################################
# FUNCTION display_db_info(db_version,db_build)
########################################################
FUNCTION display_db_info(l_cms_info)
  DEFINE l_cms_info OF t_cms_info 

  DISPLAY "CMS Database version (db_version): " || l_cms_info.db_version || "DB_Build:" || l_cms_info.db_build || "(Required DB_Build=" || get_required_cms_version() || ")" TO dl_cms_info

END FUNCTION

########################################################
# FUNCTION display_db_info(db_version,db_build)
########################################################
FUNCTION display_db_error(err_str,err_state)
  DEFINE 
    err_str STRING,
    err_state SMALLINT

  IF err_state = 0 THEN
    DISPLAY  err_str TO dl_cms_error ATTRIBUTE(GREEN)
  ELSE
    DISPLAY  err_str TO dl_cms_error ATTRIBUTE(RED)
  END IF

END FUNCTION




########################################################################
# FUNCTION yes_no(msg)
#
# Question dialog box with the options yes and no
#
# RETURN BOOLEAN TRUE/FALSE
########################################################################
FUNCTION yes_no(title_msg,msg)
  DEFINE
    title_msg VARCHAR(100),
    msg VARCHAR(200),
    rv  CHAR

  IF title_msg = "" THEN
    LET title_msg = "CMS-Demo Application"
  END IF

  LET rv = fgl_winquestion (title_msg,msg,1,"Yes|No","question",0)

  
  IF rv = "Y" THEN
    RETURN TRUE
  ELSE 
    RETURN FALSE
  END IF
END FUNCTION


#################################################################
# FUNCTION db_state_report()
#
#
#
# RETURN NONE
#################################################################
FUNCTION db_state_report()
  DEFINE
    l_cms_info OF t_cms_info

  CALL db_info("cms") RETURNING cms_db_state, l_cms_info.*

  CASE cms_db_state
    WHEN -1
      CALL display_db_error("Your CMS database seems to be incomplete or outdated - please reinstall the db",-1)
      CALL display_db_info(l_cms_info.*)
    WHEN -2
      CALL display_db_error("Your CMS database seems to be empty (no tables or too old) - please reinstall the database",-1)
      CALL display_db_info(l_cms_info.*)
    WHEN 0
      CALL display_db_error("Your CMS database seems to be OK",0)
      CALL display_db_info(l_cms_info.*)
  END CASE

END FUNCTION






############################################################################################################



#####################################################
# FUNCTION table_operation()
#
#
#
# RETURN err
#####################################################
FUNCTION table_operation(p_table_name,p_action,p_file_type)
  DEFINE 
    err          SMALLINT,
    p_table_name STRING,
    p_action     SMALLINT,
    p_file_type  SMALLINT,
    tmp_str      STRING,
    file_name    STRING,
    local_debug  SMALLINT

  LET local_debug = FALSE

  LET err = -1  -- -1=error 

  IF p_action > 1 THEN   --0=drop and 1=table create
    LET file_name = unload_file_name(p_table_name,p_file_type)

    #Now done with a sepearte function at import call
    #IF p_action = 2 THEN --Load requires at least the unl or bak file 
      #IF NOT fgl_test("e",file_name) THEN
      #  LET tmp_str = "Following string import file does not exist: \n", file_name
      #  CALL fgl_winmessage("File Error in table_operation()" ,tmp_str, "error")
      #  RETURN -1
      #END IF
    #END IF

  END IF

  IF local_debug THEN
    DISPLAY "table_operation() - p_action=",p_action
    DISPLAY "table_operation() - fgl_find_table(p_table_name)=", fgl_find_table(p_table_name)
    DISPLAY "table_operation() - p_table_name=",p_table_name
  END IF

  IF p_action = 0 THEN   --0=drop and 1=table create 2 =load 3=unload

    IF NOT fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " can not be removed!\nThis table does not exist ! (Create it prior)\nIgnoring your request.."
      CALL fgl_winmessage("Table Drop (Delete) Error",tmp_str,"error")
    RETURN 0

    END IF

  END IF

  IF p_action = 1 THEN   --0=drop and 1=table create 2 =load 3=unload

    IF fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " can not be created!\nThis table already exists ! (Drop it prior)\nIgnoring your request.."
      CALL fgl_winmessage("Table Create Error",tmp_str,"error")
    RETURN 0

    END IF

  END IF


  IF p_action > 1 THEN  --Load/Unload  --0=drop and 1=table create 2 =load 3=unload

    IF NOT fgl_find_table(p_table_name) THEN
      LET tmp_Str = "Error - Table ", trim(p_table_name), " does not exist!\nSystem can not export/import data from a missing table.\nIgnoring your request.."
      CALL fgl_winmessage("Table Data Import/Export Error",tmp_str,"error")
      RETURN 0
    END IF

  END IF

  IF p_action = 2 THEN  --Load unl file into table

    IF NOT server_side_file_exists(file_name) THEN
      RETURN 0
    END IF

    #'s' returns true if the file is non-zero size - false if it is empty
    IF NOT fgl_test("s",file_name) THEN  --file exists but is empty - Load will not like it...
      LET tmp_Str = "Warning - UNL Load File ", trim(file_name), " is empty!\nSystem can not import data from an empty UNL/BAK file.\nIgnoring your request.."
      CALL fgl_winmessage("Table Data Import Problem",tmp_str,"warning")
      RETURN 0
    END IF

  END IF

  IF local_debug THEN
    DISPLAY "table_operation() - Begin Work"
  END IF

  -- BEGIN WORK

  CALL splash(p_action,p_table_name,p_file_type)

  CASE p_table_name




##############################################################################################################
# Tool Libs related tables
#
# Note - Some tables i.e. Document Manager require BLOB support of the database (Standard Engine can't cope with it)
##############################################################################################################


   ############################################
   # Application name <-> ID  Manager
   ############################################

    WHEN table_name_list[1] -- "qxt_application"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_application
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_application(
            application_id                   SERIAL       NOT NULL,
            application_name                 CHAR(30)     NOT NULL UNIQUE,

          PRIMARY KEY (application_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_application
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_application ORDER BY application_id

      END CASE


   ############################################
   # Language name <-> ID  Manager
   ############################################

    WHEN table_name_list[2] -- "qxt_language" (for language)
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_language
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_language (
            language_id       SERIAL NOT NULL,
            language_name     VARCHAR(20) NOT NULL,
            language_dir      CHAR(2) NOT NULL,
            language_url      VARCHAR(10),
            PRIMARY KEY (language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_language
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_language  ORDER BY language_id
      END CASE



   ############################################
   # 4GL Tool Lib string Category (for easier management)
   ############################################

    WHEN table_name_list[3] -- "qxt_str_category"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_str_category
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_str_category (
            category_id     INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            category_data   VARCHAR(30) NOT NULL,  --used to unique, but did not work

            #FOREIGN KEY (language_id) 
            #  REFERENCES qxt_language (language_id) ,


            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id) ,
           PRIMARY KEY (category_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_str_category

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_str_category ORDER BY category_id , language_id

      END CASE



   ############################################
   # 4GL Tool Lib string support (multi language support)
   ############################################

    WHEN table_name_list[4] -- "qxt_string_tool"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_string_tool
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_string_tool (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            string_data     VARCHAR(100),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_string_tool

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_string_tool ORDER BY string_id, language_id 

      END CASE


   ############################################
   # 4GL Application string support (multi language support)
   ############################################

    WHEN table_name_list[5] -- "qxt_string_app"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_string_app
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_string_app (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            string_data     VARCHAR(200),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_string_app
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_string_app  ORDER BY string_id, language_id 

      END CASE


   ############################################
   # Help Classic - multilingual Help file map  Manager
   ############################################

    WHEN table_name_list[6] -- "qxt_help_classic"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_classic
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_help_classic (
            id             SERIAL NOT NULL,
            filename       VARCHAR(100) NOT NULL,
            PRIMARY KEY (id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_help_classic

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_classic ORDER BY id

      END CASE



   ############################################
   # HTML Help (URL)  Manager
   ############################################

    WHEN table_name_list[7] -- "qxt_help_url_map"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_url_map
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_help_url_map (
            map_id         INTEGER NOT NULL,
            language_id    INTEGER NOT NULL,
            map_fname      VARCHAR(100) NOT NULL,
            map_ext        VARCHAR(100),

          PRIMARY KEY (map_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN

            LOAD FROM file_name INSERT INTO qxt_help_url_map

          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_url_map  ORDER BY map_id, language_id 

      END CASE


   ############################################
   # HTML Help (BLOB)  Manager
   ############################################
    WHEN table_name_list[8] -- "qxt_help_html_doc"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_help_html_doc
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_help_html_doc (
            help_html_doc_id     INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            help_html_filename   VARCHAR(100),
            help_html_mod_date   DATE,
            help_html_data       TEXT,
          FOREIGN KEY (language_id) 
            REFERENCES qxt_language (language_id),
            #ON DELETE CASCADE,
          PRIMARY KEY (help_html_doc_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_help_html_doc
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_help_html_doc  ORDER BY help_html_doc_id, language_id 

      END CASE


   ############################################
   # Simple Toolbar  Manager  (For dynamic toolbars)
   ############################################


    WHEN table_name_list[9] --"qxt_toolbar"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_toolbar
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE qxt_toolbar (
          	tb_prog_id VARCHAR(30),
          	tb_menu_id VARCHAR(30),
          	tb_action VARCHAR(30),
          	tb_label VARCHAR(20),
          	tb_icon VARCHAR(100),
          	tb_position INT,
          	tb_static SMALLINT, --BOOLEAN,
          	tb_tooltip VARCHAR(100),
          	
          	tb_type SMALLINT, --0=button 1=divider--BOOLEAN,  --true = seperator pipe (not a button)
          	tb_scope SMALLINT, --0=dialog 1=global --BOOLEAN,  --scope  true is dialog
          	tb_hide SMALLINT, --0=visible 1=hide/remove --BOOLEAN,  --true will delete that button - false will set it		

          	
          	PRIMARY KEY (tb_prog_id, tb_menu_id, tb_action)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_toolbar
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_toolbar ORDER BY tb_prog_id, tb_menu_id, tb_position ASC

      END CASE


   ############################################
   # Toolbar  Manager  (For dynamic toolbars)
   ############################################

    WHEN table_name_list[10] -- "qxt_icon_size"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_size
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_icon_size (
            icon_size_name     CHAR(5)     NOT NULL,

            PRIMARY KEY (icon_size_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_size
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_size  ORDER BY icon_size_name

      END CASE


    WHEN table_name_list[11] -- "qxt_icon_path"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_path
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_icon_path (
            icon_path_name     VARCHAR(20)   NOT NULL,
            icon_path_dir      VARCHAR(50)   NOT NULL UNIQUE, 

            PRIMARY KEY (icon_path_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_path
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_path  ORDER BY icon_path_name

      END CASE



    WHEN table_name_list[12] -- "qxt_icon_property"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_property
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_icon_property (
            icon_property_name        VARCHAR(20)   NOT NULL,
            icon_size_name            CHAR(5)       NOT NULL,
            icon_path_name            VARCHAR(20)   NOT NULL,

            FOREIGN KEY (icon_size_name) 
              REFERENCES qxt_icon_size (icon_size_name),
            FOREIGN KEY (icon_path_name) 
              REFERENCES qxt_icon_path (icon_path_name),
            PRIMARY KEY (icon_property_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_property
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_property  ORDER BY icon_property_name

      END CASE



    WHEN table_name_list[13] -- "qxt_icon_category"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon_category
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_icon_category (
            icon_category_id     INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            icon_category_data   VARCHAR(40) NOT NULL UNIQUE,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id) ,
           PRIMARY KEY (icon_category_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon_category
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon_category  ORDER BY icon_category_id, language_id ASC

      END CASE



    WHEN table_name_list[14] -- "qxt_icon"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_icon
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_icon (
            icon_filename           VARCHAR(100)  NOT NULL,
            #icon_property_name     VARCHAR(20)  NOT NULL,
            icon_category1_id                    INTEGER NOT NULL,
            icon_category2_id                    INTEGER NOT NULL,
            icon_category3_id                    INTEGER NOT NULL,    

          PRIMARY KEY (icon_filename)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_icon
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_icon  ORDER BY icon_filename ASC

      END CASE




    WHEN table_name_list[15] -- "qxt_tbi_tooltip"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_tooltip
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_tooltip (
            string_id       INTEGER NOT NULL,
            language_id     INTEGER NOT NULL,
            label_data     VARCHAR(30),
            string_data     VARCHAR(100),
            category1_id    INTEGER,
            category2_id    INTEGER,
            category3_id    INTEGER,

            FOREIGN KEY (language_id) 
              REFERENCES qxt_language (language_id), 
            PRIMARY KEY (string_id, language_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_tooltip
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_tooltip  ORDER BY string_id , language_id ASC

      END CASE

                                                  
    WHEN table_name_list[16] -- "qxt_tbi_event_type"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_event_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_event_type (
            event_type_id                 SERIAL NOT NULL,
            event_type_name               VARCHAR(10) NOT NULL UNIQUE,

          PRIMARY KEY (event_type_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_event_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_event_type  ORDER BY event_type_id ASC

      END CASE



    WHEN table_name_list[17] -- "qxt_tbi_obj_event"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj_event
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj_event (
            tbi_obj_event_name               VARCHAR(30) NOT NULL,

          PRIMARY KEY (tbi_obj_event_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj_event
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj_event  ORDER BY tbi_obj_event_name ASC

      END CASE



    WHEN table_name_list[18] -- "qxt_tbi_obj_action"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj_action
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj_action (
            tbi_action_id                  SMALLINT    NOT NULL,
            tbi_action_name                VARCHAR(30) NOT NULL UNIQUE,
            string_id                          SMALLINT    NOT NULL,

          PRIMARY KEY (tbi_action_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj_action
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj_action  ORDER BY tbi_action_id ASC

      END CASE



    WHEN table_name_list[19] -- "qxt_tbi_obj"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_obj
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_obj (
            tbi_obj_name             VARCHAR(35) NOT NULL,
            event_type_id            INTEGER NOT NULL,
            tbi_obj_event_name       VARCHAR(30) NOT NULL,
            tbi_obj_action_id        SMALLINT    NOT NULL,
            icon_filename            VARCHAR(100) NOT NULL,
            string_id                INTEGER  NOT NULL,
            icon_category1_id        INTEGER NOT NULL,
            icon_category2_id        INTEGER NOT NULL,
            icon_category3_id        INTEGER NOT NULL,         
 
          FOREIGN KEY (event_type_id) 
            REFERENCES qxt_tbi_event_type (event_type_id),
          FOREIGN KEY (icon_filename) 
            REFERENCES qxt_icon (icon_filename),
          FOREIGN KEY (tbi_obj_action_id) 
            REFERENCES qxt_tbi_obj_action (tbi_action_id),
          PRIMARY KEY (tbi_obj_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_obj
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_obj  ORDER BY tbi_obj_name ASC

      END CASE


    WHEN table_name_list[20] -- "qxt_tbi_scope"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_scope
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_scope (
            tbi_scope_id                SMALLINT    NOT NULL , --CHECK (qxt_tbi_instance BETWEEN 0 AND 9),
            tbi_scope_name              VARCHAR(30) NOT NULL UNIQUE,
            string_id                   INTEGER     NOT NULL UNIQUE,
          PRIMARY KEY (tbi_scope_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_scope
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_scope  ORDER BY tbi_scope_id ASC

      END CASE



    WHEN table_name_list[21] -- "qxt_tbi_static"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi_static
          WHENEVER ERROR STOP


        WHEN 1  --create table
          CREATE TABLE qxt_tbi_static (
            tbi_static_id    SMALLINT    NOT NULL,
            tbi_static_name  VARCHAR(20) NOT NULL,
            string_id        INTEGER     NOT NULL UNIQUE,

            PRIMARY KEY (tbi_static_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi_static
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi_static  ORDER BY tbi_static_id ASC

      END CASE



    WHEN table_name_list[22] -- "qxt_tbi"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tbi
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tbi(
            application_id     INTEGER      NOT NULL,
            tbi_name           VARCHAR(30)  NOT NULL,
#           tbi_instance      SMALLINT     NOT NULL,  --there can be any number of menus for a menu name i.e.you could clean fgl_setkeylabel() icons at the end of a function

            tbi_obj_name       VARCHAR(35)  NOT NULL,  --toolbar menu item
            event_type_id      INTEGER      NOT NULL,
            tbi_event_name     VARCHAR(30)  NOT NULL,
            tbi_scope_id       SMALLINT     NOT NULL,  -- Scope: 0-global fgl_setkeylabel  1-fgl_dialog_setkeylabel()
            tbi_position       SMALLINT     NOT NULL,  --position order  
            tbi_static_id      SMALLINT     NOT NULL,
            icon_category1_id  INTEGER      NOT NULL,
            icon_category2_id  INTEGER      NOT NULL,
            icon_category3_id  INTEGER      NOT NULL, 

          FOREIGN KEY (event_type_id) 
            REFERENCES qxt_tbi_event_type (event_type_id),
          FOREIGN KEY (application_id) 
            REFERENCES qxt_application (application_id),
          FOREIGN KEY (tbi_obj_name) 
            REFERENCES qxt_tbi_obj (tbi_obj_name),
          FOREIGN KEY (tbi_scope_id) 
            REFERENCES qxt_tbi_scope (tbi_scope_id),
          FOREIGN KEY (tbi_static_id) 
            REFERENCES qxt_tbi_static (tbi_static_id),
          PRIMARY KEY (application_id,tbi_name)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_tbi
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tbi  ORDER BY application_id, tbi_name ASC

      END CASE


    WHEN table_name_list[23] -- "qxt_tb"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_tb
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_tb(
            application_id         INTEGER      NOT NULL,
            tb_name                VARCHAR(30)  NOT NULL,
            tb_instance            SMALLINT     NOT NULL,  --there can be any number of menus for a menu name i.e.you could clean fgl_setkeylabel() icons at the end of a function
            tbi_name               VARCHAR(30)  NOT NULL,  --toolbar menu item
            #instance               SMALLINT     NOT NULL,  -- Scope: 0-global fgl_setkeylabel  1-fgl_dialog_setkeylabel()
            #order_position         SMALLINT     NOT NULL,  --position order  
            #static_state           SMALLINT     NOT NULL,
 

          FOREIGN KEY (application_id) 
            REFERENCES qxt_application (application_id),
          #FOREIGN KEY (application_id,tbi_name) 
          #  REFERENCES qxt_tbi (application_id,tbi_name)
          #  ON DELETE CASCADE,

          #FOREIGN KEY (tbi_instance) 
          #  REFERENCES qxt_tbi_scope (tbi_instance),


          PRIMARY KEY (application_id,tb_name,tb_instance,tbi_name)
          )

        WHEN 2  --load table
          LOAD FROM file_name INSERT INTO qxt_tb

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_tb  ORDER BY application_id, tb_name, tb_instance, tbi_name ASC

      END CASE




   ############################################
   # Document (BLOB) Manager
   ############################################

    WHEN table_name_list[24] --"qxt_document"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_document
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_document (
            document_id          SERIAL(1000),
            document_filename    VARCHAR(100) NOT NULL UNIQUE,
            #document_fileext     CHAR(10),
            document_app         CHAR(30),
            document_category1   CHAR(40),
            document_category2   CHAR(40),
            document_category3   CHAR(40),
            document_mod_date    DATE,
            document_desc        TEXT,  --CHAR(1000),  NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field

            PRIMARY KEY (document_id)

            #document_blob        BYTE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_document
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_document  ORDER BY document_id ASC

      END CASE


    WHEN table_name_list[25] --"qxt_document_blob"
      # NOTE: Some DB's like Oracle don't allow to have 2 large objs i.e. 2 byte fields or a byte and a TEXT field

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_document_blob
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_document_blob (
            document_id          INTEGER NOT NULL UNIQUE,
            document_blob        BYTE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_document_blob
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_document_blob  ORDER BY document_id ASC

      END CASE




    WHEN table_name_list[26] --"qxt_dde_font"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_dde_font
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE qxt_dde_font (
            dde_font_id      INTEGER NOT NULL,
            font_name        VARCHAR(40), --VARCHAR(30),   -- Arial
            font_bold        VARCHAR(30), --SMALLINT,      -- false
            font_size        VARCHAR(30), --SMALLINT,      --  = 10
            strike_through   VARCHAR(30), --SMALLINT,      --  = false
            super_script     VARCHAR(30), --SMALLINT,      --  = false
            lower_script     VARCHAR(30), --SMALLINT,      --  = false
            option_1         VARCHAR(30), --SMALLINT,      --  = false
            option_2         VARCHAR(30), --SMALLINT,      --  = false
            option_3         VARCHAR(30), --SMALLINT,      --  = 1
            font_color       VARCHAR(30), --SMALLINT       --  = 5

            PRIMARY KEY (dde_font_id)

          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_dde_font
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_dde_font  ORDER BY dde_font_id ASC

      END CASE



   ############################################
   # HTML Print Template (BLOB)  Manager
   ############################################
    WHEN table_name_list[27] -- "qxt_print_template"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_print_template
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_print_template (
            template_id          INTEGER NOT NULL,
            language_id          INTEGER NOT NULL,
            filename             VARCHAR(100),
            mod_date             DATE,
            template_data        TEXT,
          FOREIGN KEY (language_id) 
            REFERENCES qxt_language (language_id)
            ON DELETE CASCADE,
          PRIMARY KEY (template_id, language_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_print_template
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_print_template ORDER BY template_id, language_id ASC

      END CASE

   ############################################
   # Print Image (BLOB)  Manager (used by i.e. html print)
   ############################################
    WHEN table_name_list[28] -- "qxt_print_image"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_print_image
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_print_image (
            image_id             SERIAL NOT NULL,
            filename             VARCHAR(100),
            mod_date             DATE,
            image_data           BYTE,
          PRIMARY KEY (image_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_print_image
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_print_image

      END CASE


   ############################################
   # qxt_reserve
   ############################################
    WHEN table_name_list[29] -- "qxt_reserve"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_reserve
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_reserve (
            id             INTEGER NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_reserve
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_reserve

      END CASE

   ############################################
   # qxt_reserve
   ############################################
    WHEN table_name_list[30] -- "qxt_reserve2"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE qxt_reserve2
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE qxt_reserve2 (
            id             INTEGER NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO qxt_reserve2
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM qxt_reserve2

      END CASE





#######################################################################################
# CMS Application related tables
#######################################################################################


    WHEN table_name_list[31] --"cms_info"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE cms_info
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE cms_info (
            db_version      VARCHAR(10),
            db_build        VARCHAR(10),
            db_other        VARCHAR(10)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO cms_info
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM cms_info

      END CASE


    WHEN table_name_list[32] --"menus"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE menus
          WHENEVER ERROR STOP

        WHEN 1  --create table

          CREATE TABLE menus (
            menu_id		INTEGER NOT NULL,
            #language_id		INTEGER,
            menu_name	       VARCHAR(30),

            #FOREIGN KEY (language_id) 
            #  REFERENCES qxt_language (language_id), 
            PRIMARY KEY (menu_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO menus
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM menus

      END CASE


    WHEN table_name_list[33] --"menu_options"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE menu_options
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE menu_options (
            option_id	        SERIAL NOT NULL, 
            menu_id		INTEGER NOT NULL,
            option_name_id      INTEGER, --VARCHAR(20),
            option_comment_id   INTEGER, --VARCHAR(60),
            option_keypress	VARCHAR(10),
 
            PRIMARY KEY (option_id,menu_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO menu_options
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM menu_options

      END CASE


    WHEN table_name_list[34] --"contact"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE contact
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        WHEN 1  --create table
          CREATE TABLE contact (
            cont_id 	SERIAL(1000) NOT NULL,
            cont_title	VARCHAR(10),
            cont_name 	VARCHAR(20) NOT NULL UNIQUE,
            cont_fname 	VARCHAR(20),
            cont_lname 	VARCHAR(20),
            cont_addr1	VARCHAR(40),
            cont_addr2	VARCHAR(40),
            cont_addr3	VARCHAR(40),
            cont_city	VARCHAR(20),
            cont_zone	VARCHAR(15),
            cont_zip	VARCHAR(15),
            cont_country	VARCHAR(40),
            cont_phone	VARCHAR(15),
            cont_fax	VARCHAR(15),
            cont_mobile	VARCHAR(15),
            cont_email	VARCHAR(50) NOT NULL UNIQUE,
            cont_dept	VARCHAR(15),	# ref cont_dept_id
            cont_org	INTEGER,	# ref company_id
            cont_position	VARCHAR(15),	# ref position_id
            cont_picture	BYTE,
            cont_password	VARCHAR(15),
            cont_ipaddr	VARCHAR(15),	# IP V4
            cont_usemail	SMALLINT,
            cont_usephone	SMALLINT,
            cont_notes   CHAR(1000),
 
            PRIMARY KEY (cont_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO contact
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM contact

      END CASE



    WHEN table_name_list[35] --"company"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE company (
            comp_id     SERIAL(1000) NOT NULL,
            comp_name	CHAR(100) NOT NULL UNIQUE,
            comp_addr1	CHAR(40),
            comp_addr2	CHAR(40),
            comp_addr3	CHAR(40),
            comp_city	CHAR(20),
            comp_zone	CHAR(15),
            comp_zip	CHAR(15),
            comp_country	CHAR(40),
            acct_mgr	INTEGER,		# ref contact_id
            comp_link	INTEGER,		# future: cross ref link table
            comp_industry	INTEGER,		# ref industry_id
            comp_priority	INTEGER,
            comp_type	INTEGER,		# ref cont_dept_id
            comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
            comp_url	VARCHAR(50),
            comp_notes	CHAR(1000),
 
            PRIMARY KEY (comp_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company

      END CASE




    WHEN table_name_list[36] --"country"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE country
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE country (
            country         VARCHAR(40) NOT NULL unique   --country lookup
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO country
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM country

      END CASE


    WHEN table_name_list[37] --"user_fields"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE user_fields
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE user_fields (
            field_id	SERIAL,
            field_name	VARCHAR(50) NOT NULL UNIQUE,
            field_desc	VARCHAR(50),
            field_type	INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO user_fields
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM user_fields

      END CASE


    WHEN table_name_list[38] --"user_field_data"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE user_field_data
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE user_field_data (
            field_id	INTEGER,
            contact_id	INTEGER,
            field_data	VARCHAR(50)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO user_field_data
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM user_field_data

      END CASE



    WHEN table_name_list[39] --"industry_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE industry_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE industry_type (
            type_id	SERIAL,
            itype_name	VARCHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO industry_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM industry_type

      END CASE


    WHEN table_name_list[40] --"company_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func

        
        WHEN 1  --create table
          CREATE TABLE company_type (
            type_id		SERIAL,
            ctype_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT,
            base_priority	INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company_type

      END CASE


    WHEN table_name_list[41] --"contact_dept"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE contact_dept
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE contact_dept (
            dept_id		SERIAL,
            dept_name	CHAR(25) NOT NULL UNIQUE,
            user_def	SMALLINT
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO contact_dept
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM contact_dept

      END CASE



    WHEN table_name_list[42] --"position_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE position_type
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE position_type (
            type_id     SERIAL,
            ptype_name  CHAR(25) NOT NULL UNIQUE,
            user_def    SMALLINT
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO position_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM position_type

      END CASE

    WHEN table_name_list[43] --"activity_type"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity_type (
            type_id		SERIAL,
            atype_name	CHAR(15) NOT NULL UNIQUE,
            user_def	SMALLINT
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO activity_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM activity_type

      END CASE


    WHEN table_name_list[44] --"company_link"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE company_link
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE company_link (
            parent_comp_id	INTEGER,	# ref comp_id
            child_comp_id	INTEGER		# ref comp_id
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO company_link
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM company_link

      END CASE


    WHEN table_name_list[45] --"activity"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity (
            activity_id	SERIAL UNIQUE,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id     INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            #long_ref	INTEGER,		# ref possible blob
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO activity
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM activity

      END CASE



    WHEN table_name_list[46] --"titles"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE titles
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE titles (
            lang_id		INTEGER,
            title_name	VARCHAR(15)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO titles
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM titles

      END CASE


    WHEN table_name_list[47] -- "delivery_type"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE delivery_type
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE delivery_type (
            delivery_type_id       SERIAL,
            delivery_type_name     CHAR(20)  NOT NULL UNIQUE,
            delivery_rate     MONEY(6,2) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO delivery_type
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM delivery_type

      END CASE


    WHEN table_name_list[48] --"operator"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE operator
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE operator (
            operator_id             SERIAL,
            name                CHAR(10)  NOT NULL UNIQUE,
            password            CHAR(10),
            type                CHAR,
            cont_id             INTEGER, # ref contact_id
            email_address       VARCHAR(100),
            #session_id          VARCHAR(40),
            #session_counter     INTEGER,
            
            PRIMARY KEY (operator_id)
            
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO operator
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM operator

      END CASE


    WHEN table_name_list[49] --"operator_session"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE operator_session
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE operator_session (
          	session_id					VARCHAR(50),
            operator_id         INTEGER,
            session_counter     INTEGER,
            expired							SMALLINT,
            session_created			DATETIME YEAR TO SECOND,
            session_modified  	DATETIME YEAR TO SECOND,
            client_host					VARCHAR(50),
            client_ip						VARCHAR(50),
            reserve1						VARCHAR(50),
            reserve2						VARCHAR(50)--,
            
            --PRIMARY KEY (session_id--),
            
          	--FOREIGN KEY (operator_id) 
            --REFERENCES operator (operator_id)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO operator_session
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM operator_session ORDER BY operator_id, session_created, session_modified ASC

      END CASE

    WHEN table_name_list[50] --"mailbox"

      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE mailbox
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE mailbox (
            mail_id             SERIAL,				# 
            activity_id         INTEGER NOT NULL UNIQUE,		# ref activity
            operator_id         INTEGER NOT NULL, 		# ref operator.operator_id
            from_email_address  VARCHAR(100),
            from_cont_id     INTEGER,			# ref contact.
            to_email_address    VARCHAR(100),
            to_cont_id       INTEGER,
            recv_date           DATE,
            read_flag           SMALLINT,
            urgent_flag         SMALLINT,
            mail_status         SMALLINT			# inbound, outbound, read, deleted, etc.
           # although the activity provides an implicit contact, we need a contact where no activity is involved. 
           # Eventually, I don't want this to reference activity for obvious reasons, but
           # for now it'll be OK.
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO mailbox
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM mailbox

      END CASE


    WHEN table_name_list[51] -- "invoice"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE invoice
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE invoice (
            invoice_id		SERIAL(1000),
            account_id		INTEGER NOT NULL,
            invoice_date	DATE NOT NULL,
            pay_date          	DATE,
            status              INTEGER NOT NULL,
            tax_total		MONEY(8,2) NOT NULL,
            net_total		MONEY(8,2) NOT NULL,
            inv_total		MONEY(8,2) NOT NULL,
            currency_id		INTEGER,
            for_total         	MONEY(8,2),        -- 
            operator_id		INTEGER NOT NULL,
            pay_method_id       INTEGER NOT NULL,
            pay_method_name     CHAR(20),
            pay_method_desc     CHAR(20),
            del_address_dif	INTEGER NOT NULL,
            del_address1 		CHAR(30),
            del_address2 		CHAR(30),
            del_address3 		CHAR(30),
            del_method 		INT,  --CHAR(20),
            invoice_po		CHAR(20)
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO invoice
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM invoice

      END CASE

    WHEN  table_name_list[52] --"currency"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE currency
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE currency (
            currency_id		SERIAL,
            currency_name	CHAR(10)   NOT NULL UNIQUE,
            xchg_rate		DECIMAL(5,3)  NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO currency
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM currency

      END CASE


    WHEN  table_name_list[53] --"invoice_line"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE invoice_line
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE invoice_line (
            invoice_id          INTEGER NOT NULL,    -- references invoice
            quantity            INTEGER,             
            stock_id            CHAR(10) NOT NULL,  -- references stock item
            item_tax            INTEGER NOT NULL     -- references tax rates
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO invoice_line
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM invoice_line

      END CASE



    WHEN table_name_list[54] -- "stock_item"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE stock_item
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE stock_item (
            stock_id            CHAR(10) NOT NULL,
            item_desc           CHAR(80)  NOT NULL UNIQUE,
            item_cost           MONEY(8,2),
            rate_id             INTEGER NOT NULL,
            quantity            INTEGER DEFAULT 0
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO stock_item
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM stock_item

      END CASE



   WHEN table_name_list[55] -- "account"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE account
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table

          CREATE TABLE account (
            account_id          SERIAL,
            comp_id             INTEGER  NOT NULL UNIQUE,
            credit_limit        MONEY(8,2),
            discount            DECIMAL(4,2) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO account
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM account

      END CASE


    WHEN table_name_list[56] -- "tax_rates"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE tax_rates
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE tax_rates (
            rate_id             SERIAL,
            tax_rate            DECIMAL(5,2) NOT NULL,
            tax_desc            CHAR(80)  NOT NULL UNIQUE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO tax_rates
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM tax_rates

      END CASE


    WHEN table_name_list[57] -- "pay_methods"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE pay_methods
          WHENEVER ERROR STOP

        WHEN 1  --create table
          CREATE TABLE pay_methods (
            pay_method_id       SERIAL,
            pay_method_name     CHAR(10)  NOT NULL UNIQUE,
            pay_method_desc     CHAR(20) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO pay_methods
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM pay_methods

      END CASE


    WHEN table_name_list[58] -- "title"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE title
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE title (
            title           CHAR(10) NOT NULL UNIQUE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO title
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM title

      END CASE



    WHEN table_name_list[59] -- "zone"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE zone
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE zone (
            zone_id           SERIAL      NOT NULL UNIQUE,
            zone_name         VARCHAR(40) NOT NULL UNIQUE,
            country_id        INTEGER     NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO zone
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM zone

      END CASE



    WHEN table_name_list[60] -- "status_invoice"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE status_invoice
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE status_invoice
          (
            status_id   SERIAL,
            status_name CHAR(25) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO status_invoice
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM status_invoice

      END CASE



    WHEN table_name_list[61] -- "state_supply"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE state_supply
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE state_supply
          (
            state_id    SERIAL,
            state_name  CHAR(25) NOT NULL
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO state_supply
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM state_supply

      END CASE



    WHEN table_name_list[62] -- "supplies"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE supplies
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE supplies
          (
            suppl_id    SERIAL,
            operator_id INTEGER NOT NULL,
            suppl_date  DATE,
            state       INTEGER NOT NULL,
            account_id  INTEGER NOT NULL,
            exp_date    DATE
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO supplies
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM supplies

      END CASE



    WHEN table_name_list[63] -- "supplies_line"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE supplies_line
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE supplies_line
          (
            suppl_id    INTEGER NOT NULL,
            stock_id    CHAR(10) NOT NULL,
            quantity    INTEGER
          )

        WHEN 2  --load table
          IF server_side_file_exists(file_name) THEN
            LOAD FROM file_name INSERT INTO supplies_line
          END IF

        WHEN 3  --unload table
          UNLOAD TO file_name SELECT * FROM supplies_line

      END CASE

    WHEN table_name_list[64] -- "activity_birt"
      CASE p_action

        WHEN 0  --drop table
          WHENEVER ERROR CONTINUE
          DROP TABLE activity_birt
          WHENEVER ERROR STOP
          ##WHENEVER ERROR CALL error_func


        WHEN 1  --create table
          CREATE TABLE activity_birt (
            sesion_id   INTEGER,
            activity_id	INTEGER,
            open_date	DATE NOT NULL,
            close_date	DATE,
            contact_id	INTEGER NOT NULL,	# ref contact_id
            comp_id		INTEGER,	# ref comp_id
            operator_id INTEGER NOT NULL,	# ref operator_id
            act_type	INTEGER,		# ref activity_type
            long_desc	TEXT,
            short_desc	CHAR(80),
            a_owner		INTEGER,	# ref contact_id
            priority	INTEGER
          )

      END CASE
  
    OTHERWISE
      LET tmp_str = "Invalid table name argument in function table_operation()!\ntable_name =", p_table_name
      CALL fgl_winmessage("Error",tmp_str,"error")

  END CASE

  -- COMMIT WORK

  #LET monitor = add_monitor_str(monitor, "End of Create Table Operation",0)
  CALL display_monitor(monitor)

  #CALL fgl_winmessage("End of Operation", "All tables where created","info")

  LET err = 1  -- -1=error  0 = failed but not error 1=ok

  RETURN err 

END FUNCTION

##################################





FUNCTION ini_table_name_list()
  LET table_name_list[1] = "qxt_application"
  LET table_name_list[2] = "qxt_language"
  LET table_name_list[3] = "qxt_str_category"
  LET table_name_list[4] = "qxt_string_tool"
  LET table_name_list[5] = "qxt_string_app"
  LET table_name_list[6] = "qxt_help_classic"
  LET table_name_list[7] = "qxt_help_url_map"
  LET table_name_list[8] = "qxt_help_html_doc"  
  LET table_name_list[9] = "qxt_toolbar"
  LET table_name_list[10] = "qxt_icon_size"
  LET table_name_list[11] = "qxt_icon_path"
  LET table_name_list[12] = "qxt_icon_property"
  LET table_name_list[13] = "qxt_icon_category"
  LET table_name_list[14] = "qxt_icon"
  LET table_name_list[15] = "qxt_tbi_tooltip"
  LET table_name_list[16] = "qxt_tbi_event_type"
  LET table_name_list[17] = "qxt_tbi_obj_event"
  LET table_name_list[18] = "qxt_tbi_obj_action"
  LET table_name_list[19] = "qxt_tbi_obj"
  LET table_name_list[20] = "qxt_tbi_scope"
  LET table_name_list[21] = "qxt_tbi_static"
  LET table_name_list[22] = "qxt_tbi"
  LET table_name_list[23] = "qxt_tb"
  LET table_name_list[24] = "qxt_document"
  LET table_name_list[25] = "qxt_document_blob"
  LET table_name_list[26] = "qxt_dde_font"
  LET table_name_list[27] = "qxt_print_template"
  LET table_name_list[28] = "qxt_print_image"
  LET table_name_list[29] = "qxt_reserve"
  LET table_name_list[30] = "qxt_reserve2"

  LET qxt_table_count = 30

  # Actual CMS Application 
  LET table_name_list[31] = "cms_info"
  LET table_name_list[32] = "menus"
  LET table_name_list[33] = "menu_options"
  LET table_name_list[34] = "contact"
  LET table_name_list[35] = "company"
  LET table_name_list[36] = "country"
  LET table_name_list[37] = "user_fields"
  LET table_name_list[38] = "user_field_data"
  LET table_name_list[39] = "industry_type"
  LET table_name_list[40] = "company_type"
  LET table_name_list[41] = "contact_dept"
  LET table_name_list[42] = "position_type"
  LET table_name_list[43] = "activity_type"
  LET table_name_list[44] = "company_link"
  LET table_name_list[45] = "activity"
  LET table_name_list[46] = "titles"
  LET table_name_list[47] = "delivery_type"
  LET table_name_list[48] = "operator"
	LET table_name_list[49] = "operator_session"
  LET table_name_list[50] = "mailbox"
  LET table_name_list[51] = "invoice"
  LET table_name_list[52] = "currency"
  LET table_name_list[53] = "invoice_line"
  LET table_name_list[54] = "stock_item"
  LET table_name_list[55] = "account"
  LET table_name_list[56] = "tax_rates"
  LET table_name_list[57] = "pay_methods"
  LET table_name_list[58] = "title"
  LET table_name_list[59] = "zone"
  LET table_name_list[60] = "status_invoice"
  LET table_name_list[61] = "state_supply"
  LET table_name_list[62] = "supplies"
  LET table_name_list[63] = "supplies_line"
  LET table_name_list[64] = "activity_birt" --table for adapting birt to complex query
  LET cms_table_count = 64
END FUNCTION



FUNCTION all_tables(p_action,p_file_type)
  DEFINE
   p_action SMALLINT,
   id       SMALLINT,
   err      SMALLINT,
   tmp_str  STRING,
   tmp_str1 STRING,
   tmp_str2 STRING,
   tmp_str3 STRING,
   p_file_type SMALLINT,
   temp_action  SMALLINT,
   local_debug  SMALLINT


 LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "all_tables() - p_action=", p_action
    DISPLAY "all_tables() - p_file_type=", p_file_type
  END IF

  CASE p_action
    WHEN -1  --Re-create entire database
      LET tmp_str1 = "Re-create (Drop,Create/Populate) ALL tables"
      LET tmp_str2 = "Are you sure you want to re-create (drop) all tables?\nYou can not undo this operation and will lose all tables including their data"
      LET tmp_str3 = "End Of Operation - The entire CMS database was re-created"
    WHEN 0  --drop
      LET tmp_str1 = "Drop ALL tables"
      LET tmp_str2 = "Are you sure you want to drop all tables?\nYou can not undo this operation and will lose all tables including their data"
      LET tmp_str3 = "End Of Operation - All CMS database tables were droped"
    WHEN 1  --create
      LET tmp_str1 = "Create ALL tables"
      LET tmp_str2 = "Are you sure you want to create all tables?\nYou can not undo this operation"
      LET tmp_str3 = "End Of Operation - All CMS database tables were created"

    WHEN 2  --load
      LET tmp_str1 = "Populate (Load) ALL tables"
      LET tmp_str2 = "Are you sure you want to populate all tables?\nYou can not undo this operation"
      LET tmp_str3 = "End Of Operation - All CMS database tables populated (unl load)"

    WHEN 3  --unload
      LET tmp_str1 = "Export (UnLoad) ALL tables"
      LET tmp_str2 = "Are you sure you want to export all tables?\nExisting 'Load/Unload files will be overwritten"
      LET tmp_str3 = "End Of Operation - All CMS database table data were exported"
      
    OTHERWISE
      LET tmp_str = "Error in function all_tables()\nFunction argument p_action is invalid\np_action = ", p_action
      CALL fgl_winmessage("Error in function all_tables()",tmp_str,"error")
      RETURN

  END CASE

  IF local_debug THEN
    DISPLAY "all_tables() - yes/no?="
    #DISPLAY "all_tables() - p_file_type=", p_file_type
  END IF

  #Are you sure ?
  IF yes_no(tmp_str1,tmp_str2) THEN

    LET monitor = "" --initialise monitor progress string
    LET monitor = add_monitor_str(monitor, tmp_str1,0)
    CALL display_monitor(monitor)


    #recreate entire database
    IF p_action = -1  THEN --recreate entire database 

      #drop 
      LET temp_action = 0   --0=drop and 1=table create 2 =load 3=unload
      #Note: must drop table in reversed order
      FOR id = inst_table_count TO 1  STEP -1
        IF fgl_find_table(table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(table_name_list[id],temp_action,p_file_type) RETURNING err
        END IF
      END FOR

      #create
      LET temp_action = 1
      FOR id = 1 TO inst_table_count 
        IF NOT fgl_find_table(table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(table_name_list[id],temp_action,p_file_type) RETURNING err
        END IF
      END FOR

      #populate
      LET temp_action = 2
      FOR id = 1 TO inst_table_count 
        CALL table_operation(table_name_list[id],temp_action,p_file_type) RETURNING err
      END FOR
      #CALL load_russian_translations()

    ELSE
      #Note: must drop table in reversed order
      IF p_action = 0 THEN  --drop all tables
        FOR id = inst_table_count TO 1 STEP -1
        IF fgl_find_table(table_name_list[id]) THEN  --only drop, if the table does exist
          CALL table_operation(table_name_list[id],p_action,p_file_type) RETURNING err
        END IF
        END FOR
      ELSE
        FOR id = 1 TO inst_table_count 
          CALL table_operation(table_name_list[id],p_action,p_file_type) RETURNING err
        END FOR
      END IF

    END IF

    CALL db_state_report()
    LET monitor = add_monitor_str(monitor, tmp_str3,0)
    CALL display_monitor(monitor)
  ELSE
    CALL fgl_winmessage("Operation canceled",trim(tmp_str1) || " operation cancelled at user's request","info")
  END IF

  RETURN err
END FUNCTION


########################################################################
# FUNCTION table_selection_menu(p_action,p_file_type)
#
# Display table list for selection & apply action/operation
#
# RETURN err
########################################################################

FUNCTION table_selection_menu(p_action,p_file_type)
  DEFINE
    p_action   SMALLINT,
    tmp_str    STRING,
    id         SMALLINT,
    err        SMALLINT,
    p_file_type SMALLINT


  CALL fgl_window_open("w_table_popup",5,5,"form/db_tools_form_list_g",TRUE)
  CALL fgl_settitle("Table List")


  CALL set_count(inst_table_count)
  LET int_flag = FALSE

  DISPLAY ARRAY table_name_list TO table_name_list.*
    ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

    AFTER DISPLAY
      LET id = arr_curr()
      
  END DISPLAY  

  CALL fgl_window_close("w_table_popup")

  IF int_flag THEN  --cancel
    #do nothing
    LET int_flag = FALSE
  ELSE
    CALL table_operation(table_name_list[id],p_action,p_file_type) RETURNING err
  END IF

  RETURN err
END FUNCTION


###########################################################
# FUNCTION validate_file_server_side_exists(p_filename, p_optional_str,p_dialog)
#
# FILE - Validate the existence of a file on the SERVER (with optional message)
#
# RETURN BOOLEAN
###########################################################
FUNCTION server_side_file_exists(p_filename)
  DEFINE 
    p_filename     VARCHAR(250),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300),
    msg VARCHAR(200),
    rv  CHAR,
    ret              SMALLINT,
    local_debug      SMALLINT

  LET local_debug = FALSE

  LET rv = "E"
  LET ret = TRUE

  IF local_debug THEN
    DISPLAY "server_side_file_exists() - p_filename=", p_filename
  END IF

  WHILE TRUE 
    #Check if the file exists on the server
    IF fgl_test("e",p_filename) THEN
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - file does exist=", p_filename
      END IF

      LET ret = TRUE
      EXIT WHILE

    ELSE  --file does not exist
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - file does NOT exist=", p_filename
      END IF

      LET tmp_str = "Following file does not exist:\n", trim(p_filename)
      LET rv = fgl_winquestion ("Read/Import UNL/BAK Data",tmp_str,1,"Abort|Retry|Ignore","question",0)
      IF local_debug THEN
        DISPLAY "server_side_file_exists() - User choice ret=", rv
      END IF

      CASE rv
        WHEN "I" 
          LET ret = FALSE
          EXIT WHILE

        WHEN "A"
          EXIT PROGRAM

        WHEN "R" --Retry
          --simply re-run loop

        OTHERWISE
          LET tmp_str = "Error in server_side_file_exists()\nInvalid case rv value\nrv=", rv
          cALL fgl_winmessage("Error",rv,"error")
          EXIT PROGRAM
      END CASE
    END IF
  END WHILE

  IF local_debug THEN
    DISPLAY "server_side_file_exists() - RETURN ret=", ret
  END IF

  RETURN ret


END FUNCTION


###########################################################
# FUNCTION load_russian_translations()
#
# Load russian translations
#
# RETURN TRUE or FALSE
###########################################################
{FUNCTION load_russian_translations()

  DEFINE l_russ_lang_id     SMALLINT,
         l_dir_name         VARCHAR(100),
         l_file_name        VARCHAR(100)

  LET l_russ_lang_id = 8
  LET l_dir_name     = "unl/unl_russian/"
  
WHENEVER ERROR GOTO :lbl_del_lang

  LET l_file_name = l_dir_name, "qxt_language.unl"
  LOAD FROM l_file_name INSERT INTO qxt_language
  LET l_file_name = l_dir_name, "qxt_help_url_map.unl"
  LOAD FROM l_file_name INSERT INTO qxt_help_url_map
  LET l_file_name = l_dir_name, "qxt_icon_category.unl"
  LOAD FROM l_file_name INSERT INTO qxt_icon_category
  LET l_file_name = l_dir_name, "qxt_print_template.unl"
  LOAD FROM l_file_name INSERT INTO qxt_print_template
  LET l_file_name = l_dir_name, "qxt_str_category.unl"
  LOAD FROM l_file_name INSERT INTO qxt_str_category
  LET l_file_name = l_dir_name, "qxt_string_app.unl"
  LOAD FROM l_file_name INSERT INTO qxt_string_app

WHENEVER ERROR STOP

  RETURN TRUE

  LABEL lbl_del_lang:

  DELETE FROM qxt_help_url_map   WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_icon_category  WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_print_template WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_str_category   WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_string_app     WHERE language_id = l_russ_lang_id  
  DELETE FROM qxt_language       WHERE language_id = l_russ_lang_id  

WHENEVER ERROR STOP
  
  RETURN FALSE

END FUNCTION

}




