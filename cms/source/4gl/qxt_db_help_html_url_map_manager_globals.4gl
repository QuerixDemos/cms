##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

GLOBALS

  #new stuff
  DEFINE qxt_glob_int_switch SMALLINT

#############################
## Help_URL_Map Section
############################

  DEFINE t_qxt_help_url_map_rec TYPE AS
    RECORD
      map_id                   LIKE qxt_help_url_map.map_id,
      language_id              LIKE qxt_help_url_map.language_id,
      map_fname                LIKE qxt_help_url_map.map_fname,
      map_ext                  LIKE qxt_help_url_map.map_ext
    END RECORD

  DEFINE t_qxt_help_url_map_form_rec TYPE AS
    RECORD
      map_id            LIKE qxt_help_url_map.map_id,
      language_name     LIKE qxt_language.language_name,
      map_fname         LIKE qxt_help_url_map.map_fname,
      map_ext           LIKE qxt_help_url_map.map_ext
    END RECORD

  DEFINE t_qxt_help_url_map_file TYPE AS
    RECORD
      map_fname         LIKE qxt_help_url_map.map_fname,
      map_ext           LIKE qxt_help_url_map.map_ext
    END RECORD


  DEFINE t_qxt_help_url_map_filename_rec TYPE AS
    RECORD
      map_fname         LIKE qxt_help_url_map.map_fname,  
      map_ext           LIKE qxt_help_url_map.map_ext  
    END RECORD


END GLOBALS
