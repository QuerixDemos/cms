##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage accounts - popup management list/table window
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                          RETURN:
# account_popup_data_source(p_order_field)    Data Source (Cursor) for the account_popup function   NONE
# account_popup(p_account_id,p_order_field)   Display accounts list to select and return id         l_account_arr[i].account_id  (or p_account_id for Cancel)
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

############################################################################################################
# Lookup/Combo/List/Management functions
############################################################################################################

###################################################
# FUNCTION account_popup_data_source(p_order_field)
#
# Data Source (Cursor) for the account_popup function
#
# RETURN NONE
###################################################
FUNCTION account_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "account.account_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT account.account_id, ", 
                 "company.comp_name, ",
                 "contact.cont_name, ",
                 "account.credit_limit, ",
                 "account.discount, ",
                 "operator.name ",
                 "FROM account, company, OUTER contact, OUTER operator ",
                 "WHERE account.comp_id = company.comp_id ",
                 "AND company.comp_main_cont = contact.cont_id ",
                 "AND company.acct_mgr = operator.operator_id "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED

  END IF

  PREPARE p_account FROM sql_stmt
  DECLARE c_account CURSOR FOR p_account

END FUNCTION


###################################################
# FUNCTION account_popup(p_account_id,p_order_field,p_accept_action)
#
# Display accounts list to select and return id
#
# RETURN l_account_arr[i].account_id  (or p_account_id for Cancel)
###################################################
FUNCTION account_popup(p_account_id,p_order_field,p_accept_action)
  DEFINE 
    l_account_arr DYNAMIC ARRAY OF t_acc_rec2,
    p_order_field,p_order_field2           VARCHAR(128), 
    p_account_id            LIKE  account.account_id,
    i                       INTEGER,
    local_debug             SMALLINT,
    col_sort                SMALLINT,
    p_accept_action         SMALLINT,
    err_msg                 VARCHAR(240),
    l_ord_dir               SMALLINT



  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC

  IF NOT p_order_field THEN
    LET p_order_field = "account_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF



  IF local_debug = 1 THEN
    DISPLAY "account_popup() - p_account_id=",p_account_id
    DISPLAY "account_popup() - p_order_field=",p_order_field
  END IF

 
 		CALL ui.Interface.setImage("qx://application/icon16/money/invoice/invoice-to_vendor.png")
		CALL ui.Interface.setText("Account")   
		
	IF NOT fgl_window_open("w_account_scroll", 2, 3, get_form_path("f_account_scroll_l2"),FALSE)  THEN
		CALL fgl_winmessage("Error","account_popup()\nCan not open window w_account_scroll","error")
		RETURN p_account_id
	END IF

    CALL populate_account_list_form_labels_g()



  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL account_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_account INTO l_account_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH

    LET i = i - 1
	
	If i > 0 THEN
		CALL l_account_arr.resize(i)   --remove the last item which has no data
	ELSE
		CALL l_account_arr.clear() 
	END IF   

    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_account_arr TO sa_account.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
      BEFORE DISPLAY
        CALL publish_toolbar("AccountList",0)

      ON KEY (ACCEPT)  --take selected line
        LET i = arr_curr()
        LET i = l_account_arr[i].account_id

        CASE p_accept_action
          WHEN 0
            EXIT WHILE

          WHEN 1  --view
            CALL account_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL account_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            LET i = arr_curr()
            CALL account_delete(i)
            EXIT DISPLAY

          WHEN 4  --print
            LET i = arr_curr()
            CALL fgl_winmessage("Not implemented","account_popup()\nAccount Print is not implemented","info")
            EXIT DISPLAY


          OTHERWISE
            LET err_msg = "account_popup(p_account_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("account_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add account
        CALL account_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_account_arr[i].account_id
        CALL account_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_account_arr[i].account_id
        CALL account_delete(i)
        EXIT DISPLAY


      ON KEY (F7) -- VIEW
        LET i = arr_curr()
        LET i = l_account_arr[i].account_id
        CALL account_view(i)
        EXIT DISPLAY

      #Column sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "account_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "credit_limit"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "discount"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_account_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_account_id
  ELSE
    LET i = arr_curr()
    RETURN l_account_arr[i].account_id
  END IF
END FUNCTION

