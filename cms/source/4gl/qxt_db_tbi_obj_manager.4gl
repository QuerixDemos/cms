##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tbi_obj table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_tbi_obj_name(p_icon_filename)  get tbi_obj id from p_icon_filename        l_tbi_obj_name
# get_icon_filename(tbi_obj_name)    Get icon_filename from p_tbi_obj_name        l_icon_filename
# get_tbi_obj_rec(p_tbi_obj_name)   Get the tbi_obj record from p_tbi_obj_name  l_tbi_obj.*
# tbi_obj_popup_data_source()               Data Source (cursor) for tbi_obj_popup              NONE
# tbi_obj_popup                             tbi_obj selection window                            p_tbi_obj_name
# (p_tbi_obj_name,p_order_field,p_accept_action)
# tbi_obj_combo_list(cb_field_name)         Populates tbi_obj combo list from db                NONE
# tbi_obj_create()                          Create a new tbi_obj record                         NULL
# tbi_obj_edit(p_tbi_obj_name)      Edit tbi_obj record                                 NONE
# tbi_obj_input(p_tbi_obj_rec)    Input tbi_obj details (edit/create)                 l_tbi_obj.*
# tbi_obj_delete(p_tbi_obj_name)    Delete a tbi_obj record                             NONE
# tbi_obj_view(p_tbi_obj_name)      View tbi_obj record by ID in window-form            NONE
# tbi_obj_view_by_rec(p_tbi_obj_rec) View tbi_obj record in window-form               NONE
# get_tbi_obj_name_from_dir(p_dir)          Get the tbi_obj_name from a file                      l_tbi_obj_name
# icon_filename_count(p_dir)                Tests if a record with this dir already exists               r_count
# tbi_obj_name_count(p_tbi_obj_name)  tests if a record with this tbi_obj_name already exists r_count
# copy_tbi_obj_record_to_form_record        Copy normal tbi_obj record data to type tbi_obj_form_rec   l_tbi_obj_form_rec.*
# (p_tbi_obj_rec)
# copy_tbi_obj_form_record_to_record        Copy type tbi_obj_form_rec to normal tbi_obj record data   l_tbi_obj_rec.*
# (p_tbi_obj_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_tbi_obj_scroll()              Populate tbi_obj grid headers                       NONE
# populate_tbi_obj_form_labels_g()          Populate tbi_obj form labels for gui                NONE
# populate_tbi_obj_form_labels_t()          Populate tbi_obj form labels for text               NONE
# populate_tbi_obj_form_edit_labels_g()     Populate tbi_obj form edit labels for gui           NONE
# populate_tbi_obj_form_edit_labels_t()     Populate tbi_obj form edit labels for text          NONE
# populate_tbi_obj_form_view_labels_g()     Populate tbi_obj form view labels for gui           NONE
# populate_tbi_obj_form_view_labels_t()     Populate tbi_obj form view labels for text          NONE
# populate_tbi_obj_form_create_labels_g()   Populate tbi_obj form create labels for gui         NONE
# populate_tbi_obj_form_create_labels_t()   Populate tbi_obj form create labels for text        NONE
# populate_tbi_obj_list_form_labels_g()     Populate tbi_obj list form labels for gui           NONE
# populate_tbi_obj_list_form_labels_t()     Populate tbi_obj list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"



###########################################################################################
# Data Acces Functions
###########################################################################################


#########################################################
# FUNCTION get_tbi_obj_action_id_from_tbi_obj_name(p_tbi_obj_name)
#
# get tbi_obj_action_id from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_action_id_from_tbi_obj_name(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj_action_id  LIKE qxt_tbi_obj.tbi_obj_action_id,
    local_debug                           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.tbi_obj_action_id
    INTO l_tbi_obj_action_id
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_tbi_obj_action_id = ", l_tbi_obj_action_id
  END IF

  RETURN l_tbi_obj_action_id
END FUNCTION



#########################################################
# FUNCTION get_tbi_action_name_from_tbi_obj_name(p_tbi_obj_name)
#
# get tbi_obj_action_id from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_action_name_from_tbi_obj_name(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name         LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_action_name      LIKE qxt_tbi_obj_action.tbi_action_name,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj_action.tbi_action_name
   INTO l_tbi_action_name
    FROM qxt_tbi_obj_action, qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name
      AND qxt_tbi_obj_action.tbi_action_id = qxt_tbi_obj.tbi_obj_action_id
  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_tbi_action_name = ", l_tbi_action_name
  END IF

  RETURN l_tbi_action_name
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_event_name(p_tbi_obj_name)
#
# get tbi_obj_event_name from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_event_name(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj_event_name  LIKE qxt_tbi_obj.tbi_obj_event_name,
    local_debug                     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.tbi_obj_event_name
    INTO l_tbi_obj_event_name
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_tbi_obj_event_name = ", l_tbi_obj_event_name
  END IF

  RETURN l_tbi_obj_event_name
END FUNCTION



#########################################################
# FUNCTION get_tbi_obj_event_type(p_tbi_obj_name)
#
# get tbi_obj_event_name from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_event_type_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name           LIKE qxt_tbi_obj.tbi_obj_name,
    l_event_type_id  LIKE qxt_tbi_obj.event_type_id,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT event_type_id
    INTO l_event_type_id
    FROM qxt_tbi_obj
    WHERE tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_event_type_id = ", l_event_type_id
  END IF

  RETURN l_event_type_id
END FUNCTION





#########################################################
# FUNCTION get_tbi_obj_icon_filename(p_tbi_obj_name)
#
# get icon_filename from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_icon_filename(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_icon_filename  LIKE qxt_tbi_obj.icon_filename,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.icon_filename
    INTO l_icon_filename
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_icon_filename = ", l_icon_filename
  END IF

  RETURN l_icon_filename
END FUNCTION


#########################################################
# FUNCTION get_string_id(p_tbi_obj_name)
#
# get string_id from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_string_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_string_id  LIKE qxt_tbi_obj.string_id,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.string_id
    INTO l_string_id
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_string_id = ", l_string_id
  END IF

  RETURN l_string_id
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_icon_category1_id(p_tbi_obj_name)
#
# get category1 from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_obj_icon_category1_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_category1                        LIKE qxt_tbi_obj.icon_category1_id,
    local_debug                        SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.icon_category1_id
    INTO l_category1
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_category1 = ", l_category1
  END IF

  RETURN l_category1
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_category1_id(p_tbi_obj_name)
#
# get icon_category1_id from p_tbi_obj_name
#
# RETURN l_category1_id
#########################################################
FUNCTION get_tbi_obj_category1_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_category1_id                     LIKE qxt_tbi_obj.icon_category1_id,
    local_debug                        SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_category1_id() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.icon_category1_id
    INTO l_category1_id
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_category1_id = ", l_category1_id
  END IF

  RETURN l_category1_id
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_category2_id(p_tbi_obj_name)
#
# get icon_category2_id from p_tbi_obj_name
#
# RETURN l_category2_id
#########################################################
FUNCTION get_tbi_obj_category2_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_category2_id                     LIKE qxt_tbi_obj.icon_category2_id,
    local_debug                        SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_category2_id() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.icon_category2_id
    INTO l_category2_id
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_category2_id = ", l_category2_id
  END IF

  RETURN l_category2_id
END FUNCTION


#########################################################
# FUNCTION get_tbi_obj_category3_id(p_tbi_obj_name)
#
# get icon_category3_id from p_tbi_obj_name
#
# RETURN l_category3_id
#########################################################
FUNCTION get_tbi_obj_category3_id(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_category3_id                     LIKE qxt_tbi_obj.icon_category3_id,
    local_debug                        SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_category3_id() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.icon_category3_id
    INTO l_category3_id
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_category3_id = ", l_category3_id
  END IF

  RETURN l_category3_id
END FUNCTION


#@@@
#######################################################
# FUNCTION get_current_field_obj_icon_category_id(p_icon_form_rec,p_field_name,p_language_id)
#
# Retrieve icon ide from current category field
#
# RETURN p_icon_form_rec.*
#######################################################
FUNCTION get_current_field_obj_icon_category_id(p_tbi_obj_form_rec,p_field_name,p_language_id)
  DEFINE
    p_tbi_obj_form_rec   OF t_qxt_tbi_obj_form_rec,
    p_field_name         VARCHAR(30),
    p_language_id        LIKE qxt_icon_category.language_id,
    l_icon_category_id   LIKE qxt_icon.icon_category1_id,
    err_msg              VARCHAR(200),
    local_debug          SMALLINT


  LET local_debug = FALSE

      IF local_debug THEN
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.tbi_obj_name=", p_tbi_obj_form_rec.tbi_obj_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.event_type_name=", p_tbi_obj_form_rec.event_type_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_filename=", p_tbi_obj_form_rec.icon_filename
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category1_data=", p_tbi_obj_form_rec.icon_category1_data
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category2_data=", p_tbi_obj_form_rec.icon_category2_data
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category3_data=", p_tbi_obj_form_rec.icon_category3_data
        DISPLAY "get_current_field_obj_icon_category_id() - p_field_name=", p_field_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_language_id=", p_language_id

      END IF

  IF p_language_id = 0 THEN
    LET p_language_id = 1 --default
  END IF


  #Find out, if the cursor is in a category field and update it correspondingly
  CASE p_field_name
    WHEN "icon_category1_data"
      IF local_debug THEN
        DISPLAY "get_current_field_obj_icon_category_id() - p_field_name", p_field_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_language_id", p_language_id
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category1_data", p_tbi_obj_form_rec.icon_category1_data
      END IF

      RETURN get_icon_category_id(p_tbi_obj_form_rec.icon_category1_data,p_language_id)

    WHEN "icon_category2_data"
      IF local_debug THEN
        DISPLAY "get_current_field_obj_icon_category_id() - p_field_name", p_field_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_language_id", p_language_id
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category2_data", p_tbi_obj_form_rec.icon_category2_data
      END IF

      RETURN get_icon_category_id(p_tbi_obj_form_rec.icon_category2_data,p_language_id)

    WHEN "icon_category3_data"
      IF local_debug THEN
        DISPLAY "get_current_field_obj_icon_category_id() - p_field_name", p_field_name
        DISPLAY "get_current_field_obj_icon_category_id() - p_language_id", p_language_id
        DISPLAY "get_current_field_obj_icon_category_id() - p_tbi_obj_form_rec.icon_category3_data", p_tbi_obj_form_rec.icon_category3_data
      END IF

      RETURN get_icon_category_id(p_tbi_obj_form_rec.icon_category3_data,p_language_id)

    OTHERWISE
      LET err_msg = "Invalid field name argument!\np_field_name=",p_field_name
      CALL fgl_winmessage("Error in get_current_field_obj_icon_category_id()",err_msg,"ERROR")
  END CASE

  RETURN NULL

END FUNCTION



###################################################################################
# FUNCTION get_tbi_obj_count()
#
# Finds the total number of record rows in the table
#
# RETURN NONE
###################################################################################
FUNCTION get_tbi_obj_count()
  DEFINE
    l_count                  INTEGER

  SELECT  COUNT(*)
    INTO l_count
    FROM qxt_tbi_obj

  RETURN l_count
END FUNCTION

{
#########################################################
# FUNCTION get_category3(p_tbi_obj_name)
#
# get category3 from p_tbi_obj_name
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_category3(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_category3  LIKE qxt_tbi_obj.category3,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - p_tbi_obj_name = ", p_tbi_obj_name
  END IF

  SELECT qxt_tbi_obj.category3
    INTO l_category3
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  IF local_debug THEN
    DISPLAY "get_tbi_obj_name() - l_category3 = ", l_category3
  END IF

  RETURN l_category3
END FUNCTION
}
{
#########################################################
# FUNCTION get_icon_filename(tbi_obj_name)
#
# Get icon_filename from p_tbi_obj_name
#
# RETURN l_icon_filename
#########################################################
FUNCTION get_icon_filename(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name,
    l_icon_filename      LIKE qxt_tbi_obj.icon_filename

  SELECT qxt_tbi_obj.icon_filename
    INTO l_icon_filename
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  RETURN l_icon_filename
END FUNCTION
}



######################################################
# FUNCTION get_tbi_obj_rec(p_tbi_obj_name)
#
# Get the tbi_obj record from p_tbi_obj_name
#
# RETURN l_tbi_obj.*
######################################################
FUNCTION get_tbi_obj_rec(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name  LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj       RECORD LIKE qxt_tbi_obj.*

  SELECT qxt_tbi_obj.*
    INTO l_tbi_obj.*
    FROM qxt_tbi_obj
    WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  RETURN l_tbi_obj.*
END FUNCTION



########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION tbi_obj_combo_list(cb_field_name)
#
# Populates tbi_obj combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_obj_combo_list(cb_field_name)
  DEFINE 
    l_tbi_obj_name_arr   DYNAMIC ARRAY OF LIKE qxt_tbi_obj.tbi_obj_name,
    row_count                             INTEGER,
    current_row                           INTEGER,
    cb_field_name                         VARCHAR(35),   --form field name for the country combo list field
    abort_flag                           SMALLINT,
    tmp_str                               VARCHAR(250),
    local_debug                           SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_tbi_obj_name_scroll2 CURSOR FOR 
    SELECT qxt_tbi_obj.tbi_obj_name
      FROM qxt_tbi_obj

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_obj_name_scroll2 INTO l_tbi_obj_name_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_tbi_obj_name_arr[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tbi_obj_name_arr[row_count] CLIPPED, ")"
      DISPLAY "tbi_obj_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION




######################################################
# FUNCTION tbi_obj_popup_data_source()
#
# Data Source (cursor) for tbi_obj_popup
#
# RETURN NONE
######################################################
FUNCTION tbi_obj_popup_data_source(p_name_filter,p_name_search,p_category_filter,p_cat_search1,p_cat_search2,p_cat_search3,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_order_field        VARCHAR(128),
    sql_stmt             CHAR(2048),
    p_ord_dir            SMALLINT,
    p_ord_dir_str        VARCHAR(4),
    local_debug          SMALLINT,
    p_name_filter        SMALLINT,
    p_category_filter    SMALLINT,
    p_name_search        LIKE qxt_tbi_obj.tbi_obj_name,
    p_cat_search1        LIKE qxt_icon_category.icon_category_id,
    p_cat_search2        LIKE qxt_icon_category.icon_category_id,
    p_cat_search3        LIKE qxt_icon_category.icon_category_id,
    p_language_id        LIKE qxt_language.language_id

  LET local_debug = FALSE


  #If language was not specified, use current language
  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF


  IF p_order_field IS NULL  THEN
    LET p_order_field = "tbi_obj_name"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF local_debug THEN
    DISPLAY "tbi_obj_popup_data_source() - p_category_filter=",      p_category_filter
    DISPLAY "tbi_obj_popup_data_source() - p_cat_search1=", p_cat_search1
    DISPLAY "tbi_obj_popup_data_source() - p_cat_search2=", p_cat_search2
    DISPLAY "tbi_obj_popup_data_source() - p_cat_search3=", p_cat_search3
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_tbi_obj.tbi_obj_name, ",
                 "qxt_tbi_event_type.event_type_name, ",
                 "qxt_tbi_obj.tbi_obj_event_name, ",
                 "qxt_tbi_obj_action.tbi_action_name, ",
                 "qxt_tbi_obj.icon_filename, ",
                 "qxt_tbi_obj.string_id, ",
                 "qxt_tbi_tooltip.string_data, ",
                 "ic_cat1.icon_category_data, ",
                 "ic_cat2.icon_category_data, ",
                 "ic_cat3.icon_category_data ",
                 "FROM qxt_tbi_obj, ",
                   "qxt_tbi_event_type, ", 
                   "qxt_tbi_obj_action, ", 
                   "qxt_tbi_tooltip, ",
                   "qxt_icon_category ic_cat1, ",
                   "qxt_icon_category ic_cat2, ",
                   "qxt_icon_category ic_cat3 ",

                 "WHERE ",
                 "qxt_tbi_obj.tbi_obj_action_id = qxt_tbi_obj_action.tbi_action_id ",
                 "AND ",
                 "qxt_tbi_obj.event_type_id = qxt_tbi_event_type.event_type_id ",
                 "AND ",
                 "qxt_tbi_obj.string_id = qxt_tbi_tooltip.string_id ",
                 "AND ",
                 "qxt_tbi_obj.icon_category1_id = ic_cat1.icon_category_id ",
                 "AND ",
                 "qxt_tbi_obj.icon_category2_id = ic_cat2.icon_category_id ",
                 "AND ",
                 "qxt_tbi_obj.icon_category3_id = ic_cat3.icon_category_id ",
                 "AND ",
                 "ic_cat1.language_id = ", trim(get_language()), " ",
                 "AND ",
                 "ic_cat2.language_id = ", trim(get_language()), " ",
                 "AND ",
                 "ic_cat3.language_id = ", trim(get_language()), " ",
                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_language_id), " "

  #filter with empty string is ignored
  IF p_name_search IS NULL THEN
    LET p_name_filter = FALSE
  ELSE
    IF p_name_filter = TRUE THEN
      LET sql_stmt = trim(sql_stmt), 
                 "AND ",
                 "qxt_tbi_obj.tbi_obj_name LIKE '", trim(p_name_search), "'"      
    END IF
  END IF



  IF p_cat_search1 IS NULL OR p_cat_search1 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search2 IS NULL OR p_cat_search2 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search3 IS NULL OR p_cat_search3 = 0 THEN
    LET p_cat_search1 = 1
  END IF


  IF p_category_filter THEN
    IF p_cat_search1 > 1 OR p_cat_search2 > 1 OR p_cat_search3 > 1 THEN
      LET sql_stmt = trim(sql_stmt), " ",
               "AND ",
                 "( "


      IF p_cat_search1 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search1), "' ",
                    ") "
      END IF

      IF p_cat_search1 > 1 AND p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search2), "' ",
                    ") "
      END IF

      IF p_cat_search2 > 1 AND p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search3), "' ",
                    ") "
      END IF                     
 
      LET sql_stmt = trim(sql_stmt), " ",
                  ") "

    END IF
  END IF

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "tbi_obj_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
    DISPLAY sql_stmt[501,600]
    DISPLAY sql_stmt[601,700]
    DISPLAY sql_stmt[701,800]
    DISPLAY sql_stmt[801,900]
    DISPLAY sql_stmt[901,1000]
    DISPLAY sql_stmt[1001,1100]
    DISPLAY sql_stmt[1101,1200]
    DISPLAY sql_stmt[1201,1300]
    DISPLAY sql_stmt[1301,1400]
    DISPLAY sql_stmt[1401,1500]
    DISPLAY sql_stmt[1501,1600]
    DISPLAY sql_stmt[1601,1700]
    DISPLAY sql_stmt[1701,1800]
    DISPLAY sql_stmt[1801,1900]
    DISPLAY sql_stmt[1901,2000]
  END IF

  PREPARE p_tbi_obj FROM sql_stmt
  DECLARE c_tbi_obj CURSOR FOR p_tbi_obj

END FUNCTION


######################################################
# FUNCTION tbi_obj_popup(p_tbi_obj_name,p_order_field,p_accept_action)
#
# tbi_obj selection window
#
# RETURN p_tbi_obj_name
######################################################
FUNCTION tbi_obj_popup(p_tbi_obj_name,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    #h OF t_qxt_tbi_obj_form_rec,
    p_tbi_obj_name                  LIKE qxt_tbi_obj.tbi_obj_name,  --default return value if user cancels
    p_language_id                   LIKE qxt_language.language_id,
    l_tbi_obj_arr                   DYNAMIC ARRAY OF t_qxt_tbi_obj_form_rec2,    --RECORD LIKE qxt_tbi_obj.*,  
    i  , row_max                    INTEGER,
    p_accept_action                 SMALLINT,
    err_msg                         VARCHAR(240),
    p_order_field                   VARCHAR(128), 
    p_order_field2                  VARCHAR(128), 
    l_tbi_obj_name                  LIKE qxt_tbi_obj.tbi_obj_name,
    l_temp_tbi_obj_name             LIKE qxt_tbi_obj.tbi_obj_name,
    local_debug                     SMALLINT,
    current_row                     SMALLINT,  --to jump to last modified array line after edit/input
    #current_tbi_obj_name            LIKE qxt_tbi_obj.tbi_obj_name,
    l_filter_icon_category_state    SMALLINT,
    l_filter_tbi_obj_name_state     SMALLINT,
    l_filter_tbi_obj_name           LIKE qxt_tbi_obj.tbi_obj_name,
    l_language_name                 LIKE qxt_language.language_name,
    l_filter_icon_category_data_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_data,
        cs2  LIKE qxt_icon_category.icon_category_data,
        cs3  LIKE qxt_icon_category.icon_category_data
      END RECORD,

    l_category_id_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_id,
        cs2  LIKE qxt_icon_category.icon_category_id,
        cs3  LIKE qxt_icon_category.icon_category_id
      END RECORD

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET row_max = 200 -- due to dynamic array ...sizeof(l_tbi_obj_arr)

  #Initialise filter variables
  LET l_filter_icon_category_state = FALSE
  LET l_filter_tbi_obj_name_state = FALSE

  LET l_category_id_rec.cs1 = 1
  LET l_category_id_rec.cs2 = 1
  LET l_category_id_rec.cs3 = 1

  LET l_filter_icon_category_data_rec.cs1 = ""
  LET l_filter_icon_category_data_rec.cs2 = ""
  LET l_filter_icon_category_data_rec.cs3 = ""



  #go to grid line
  LET current_row = 1                
  LET l_temp_tbi_obj_name  = p_tbi_obj_name    

  IF p_tbi_obj_name IS NOT NULL THEN   
    LET l_filter_tbi_obj_name = p_tbi_obj_name
    LET l_filter_tbi_obj_name_state = TRUE
  END IF

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "tbi_obj_popup() - p_tbi_obj_name=",p_tbi_obj_name
    DISPLAY "tbi_obj_popup() - p_order_field=",p_order_field
    DISPLAY "tbi_obj_popup() - p_accept_action=",p_accept_action
  END IF


	CALL fgl_window_open("w_tbi_obj_scroll", 2, 2, get_form_path("f_qxt_tbi_obj_scroll_l2"),FALSE) 
	CALL populate_tbi_obj_list_form_labels_g()

  #Display some data to the form
  DISPLAY get_language_name(p_language_id) TO filter_lang_n

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_tbi_obj

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL tbi_obj_create(NULL)
    END IF
  END IF



  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

#    CALL tbi_obj_popup_data_source(l_filter,l_cat_search1,l_cat_search2,l_cat_search3,p_order_field,get_toggle_switch())
    CALL tbi_obj_popup_data_source(l_filter_tbi_obj_name_state,l_filter_tbi_obj_name,l_filter_icon_category_state,l_category_id_rec.cs1,l_category_id_rec.cs2,l_category_id_rec.cs3,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_tbi_obj INTO l_tbi_obj_arr[i].*

      #Find current screen line position - go to grid line
      IF l_tbi_obj_arr[i].tbi_obj_name  = l_tbi_obj_name THEN
        LET current_row = i
      END IF


      IF local_debug THEN
        DISPLAY "tbi_obj_popup() - i=",i
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].tbi_obj_name=",        l_tbi_obj_arr[i].tbi_obj_name
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].tbi_action_name=", l_tbi_obj_arr[i].tbi_action_name
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].tbi_obj_event_name=",  l_tbi_obj_arr[i].tbi_obj_event_name

        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].icon_filename=",                        l_tbi_obj_arr[i].icon_filename
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].string_id=",                            l_tbi_obj_arr[i].string_id
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].icon_category1_data=",                  l_tbi_obj_arr[i].icon_category1_data
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].icon_category2_data=",                  l_tbi_obj_arr[i].icon_category2_data
        DISPLAY "tbi_obj_popup() - l_tbi_obj_arr[i].icon_category3_data=",                  l_tbi_obj_arr[i].icon_category3_data

      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > row_max THEN
        ERROR "Array is limited to ", row_max , " entries"
        #EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1
    
		If i > 0 THEN
			CALL l_tbi_obj_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF      
		
    DISPLAY i TO found_records
    DISPLAY get_tbi_obj_count() TO total_records

    IF local_debug THEN
      DISPLAY "tbi_obj_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_tbi_obj_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "tbi_obj_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_tbi_obj_arr TO sc_tbi_obj.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("ToolbarTbiObjectList",0)

        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF current_row IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,current_row)
        END IF


      BEFORE ROW
        LET i = arr_curr()
        LET l_tbi_obj_name = l_tbi_obj_arr[i].tbi_obj_name
        LET l_temp_tbi_obj_name  = l_tbi_obj_arr[i].tbi_obj_name  

        CALL icon_preview(l_tbi_obj_arr[i].icon_filename)

        DISPLAY l_tbi_obj_name TO pv_name
        DISPLAY l_tbi_obj_arr[i].tbi_obj_event_name TO pv_event
        DISPLAY l_tbi_obj_arr[i].tbi_action_name TO pv_action
        DISPLAY l_tbi_obj_arr[i].icon_filename TO pv_icon
        DISPLAY l_tbi_obj_arr[i].string_data TO pv_tooltip
      #On key events
 
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        #LET i = arr_curr()
        #LET l_tbi_obj_name = l_tbi_obj_arr[i].tbi_obj_name

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tbi_obj_view(l_tbi_obj_name)
            #EXIT DISPLAY

          WHEN 2  --edit
            LET l_temp_tbi_obj_name = tbi_obj_edit(l_tbi_obj_name)
            IF l_temp_tbi_obj_name IS NOT NULL THEN
              LET l_tbi_obj_name = l_temp_tbi_obj_name
              EXIT DISPLAY
            END IF

          WHEN 3  --delete
            IF tbi_obj_delete(l_tbi_obj_name) THEN
              LET l_tbi_obj_name = NULL
              EXIT DISPLAY
            END IF

          WHEN 4  --create/add
            LET l_temp_tbi_obj_name = tbi_obj_create(NULL)
            IF l_temp_tbi_obj_name IS NOT NULL THEN
              LET l_tbi_obj_name = l_temp_tbi_obj_name
              EXIT DISPLAY
            END IF

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tbi_obj_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        
          WHEN 6 --duplicate
            LET l_temp_tbi_obj_name = tbi_obj_duplicate(l_tbi_obj_name)
            IF l_temp_tbi_obj_name IS NOT NULL THEN
              LET l_tbi_obj_name = l_temp_tbi_obj_name
              EXIT DISPLAY
            END IF

          OTHERWISE
            LET err_msg = "tbi_obj_popup(p_tbi_obj_name,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("tbi_obj_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        LET l_temp_tbi_obj_name = tbi_obj_create(NULL)
        IF l_temp_tbi_obj_name IS NOT NULL THEN
          LET l_tbi_obj_name = l_temp_tbi_obj_name
          EXIT DISPLAY
        END IF

      ON KEY (F5) -- edit
        LET l_temp_tbi_obj_name = tbi_obj_edit(l_tbi_obj_name)
        IF l_temp_tbi_obj_name IS NOT NULL THEN
          LET l_tbi_obj_name = l_temp_tbi_obj_name
          EXIT DISPLAY
        END IF

      ON KEY (F6) -- delete
        IF tbi_obj_delete(l_tbi_obj_name) THEN
          LET l_tbi_obj_name = NULL
          EXIT DISPLAY
        END IF
    
      ON KEY (F7) -- duplicate
        LET l_temp_tbi_obj_name = tbi_obj_duplicate(l_tbi_obj_name)
        IF l_temp_tbi_obj_name IS NOT NULL THEN
          LET l_tbi_obj_name = l_temp_tbi_obj_name
          EXIT DISPLAY
        END IF


      #Grid Sort 
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_obj_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "event_type_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_obj_event_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tbi_action_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F20)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat1.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      

      ON KEY(F21)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat2.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F22)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat3.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON ACTION("filterIconCategoryOn")
        LET l_filter_icon_category_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterIconCategoryOff")
        LET l_filter_icon_category_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterIconCategory")  --Specify filter criteria
        INPUT BY NAME l_filter_icon_category_data_rec.* WITHOUT DEFAULTS HELP 1
          ON ACTION("setFilterIconCategory","filterIconCategoryOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET l_filter_icon_category_state = TRUE

          LET l_category_id_rec.cs1 = get_icon_category_id(l_filter_icon_category_data_rec.cs1,get_language())
          LET l_category_id_rec.cs2 = get_icon_category_id(l_filter_icon_category_data_rec.cs2,get_language())
          LET l_category_id_rec.cs3 = get_icon_category_id(l_filter_icon_category_data_rec.cs3,get_language())

          IF local_debug THEN
            DISPLAY "l_category_id_rec.cs1=", l_category_id_rec.cs1
            DISPLAY "l_category_id_rec.cs2=", l_category_id_rec.cs2
            DISPLAY "l_category_id_rec.cs3=", l_category_id_rec.cs3
          END IF

          #Some cleaning up in case the user selects i.e. the first two empty and the last combo has an entry (top to bottom)
          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs2 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs2
            LET l_category_id_rec.cs2 = 1
          END IF

          IF l_category_id_rec.cs2 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs2 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          LET l_filter_icon_category_data_rec.cs1 = get_icon_category_data(l_category_id_rec.cs1,get_language())
          LET l_filter_icon_category_data_rec.cs2 = get_icon_category_data(l_category_id_rec.cs2,get_language())
          LET l_filter_icon_category_data_rec.cs3 = get_icon_category_data(l_category_id_rec.cs3,get_language())

          DISPLAY BY NAME l_filter_icon_category_data_rec.cs1
          DISPLAY BY NAME l_filter_icon_category_data_rec.cs2
          DISPLAY BY NAME l_filter_icon_category_data_rec.cs3

        END IF

        EXIT DISPLAY 



      ON ACTION("filterTbiObjNameCategoryOn")
        LET l_filter_tbi_obj_name_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterTbiObjNameCategoryOff")
        LET l_filter_tbi_obj_name_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterTbiObjName")  --Specify filter criteria
        INPUT l_filter_tbi_obj_name WITHOUT DEFAULTS FROM filter_tbi_obj_name HELP 1
          ON ACTION("setFilterTbiObjName","filterTbiObjNameCategoryOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET l_filter_tbi_obj_name_state = TRUE

        END IF

        EXIT DISPLAY 

      ON ACTION ("setFilterLanguageName")
        INPUT l_language_name WITHOUT DEFAULTS FROM filter_lang_n HELP 1
          ON ACTION ("setFilterLanguageName")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT
        IF l_language_name IS NOT NULL THEN
          LET p_language_id = get_language_id(l_language_name)

          CALL set_data_language_id(p_language_id)
          #Update the category combo lists with the new language strings
          CALL icon_category_data_combo_list("cs1",get_data_language_id())
          CALL icon_category_data_combo_list("cs2",get_data_language_id())
          CALL icon_category_data_combo_list("cs3",get_data_language_id())
        END IF
        EXIT DISPLAY

  
    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_tbi_obj_scroll")
  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_tbi_obj_name 
  ELSE 
    RETURN l_tbi_obj_name 
  END IF

END FUNCTION






#################################################################################################
# Edit/Create/Input.. Functions
#################################################################################################

######################################################
# FUNCTION tbi_obj_create(p_tbi_obj_name)
#
# Create a new tbi_obj record
#
# RETURN NULL
######################################################
FUNCTION tbi_obj_create(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj_rec     RECORD LIKE qxt_tbi_obj.*,
    local_debug       SMALLINT

  LET local_debug = FALSE

	CALL fgl_window_open("w_tbi_obj", 3, 3, get_form_path("f_qxt_tbi_obj_det_l2"), TRUE) 
	CALL populate_tbi_obj_form_create_labels_g()

  LET int_flag = FALSE
  
  #Initialise some variables
  IF p_tbi_obj_name IS NOT NULL THEN
    LET l_tbi_obj_rec.tbi_obj_name = p_tbi_obj_name
  END IF

  LET l_tbi_obj_rec.tbi_obj_action_id = 1
  LET l_tbi_obj_rec.string_id = 1

  # CALL the INPUT
  CALL tbi_obj_input(l_tbi_obj_rec.*)
    RETURNING l_tbi_obj_rec.*

  CALL fgl_window_close("w_tbi_obj")

  IF local_debug THEN
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.tbi_obj_name=",       l_tbi_obj_rec.tbi_obj_name
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.tbi_obj_action_id=",  l_tbi_obj_rec.tbi_obj_action_id
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.event_type_id=",      l_tbi_obj_rec.event_type_id
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.tbi_obj_event_name=", l_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.icon_filename=",      l_tbi_obj_rec.icon_filename
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.string_id=",          l_tbi_obj_rec.string_id
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.icon_category1_id=",  l_tbi_obj_rec.icon_category1_id
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.icon_category2_id=",  l_tbi_obj_rec.icon_category2_id
    DISPLAY "tbi_obj_create() - l_tbi_obj_rec.icon_category3_id=",  l_tbi_obj_rec.icon_category3_id
  END IF

  IF NOT int_flag THEN
    INSERT INTO qxt_tbi_obj VALUES (
      l_tbi_obj_rec.tbi_obj_name,
      l_tbi_obj_rec.event_type_id,
      l_tbi_obj_rec.tbi_obj_event_name,
      l_tbi_obj_rec.tbi_obj_action_id,
      l_tbi_obj_rec.icon_filename,
      l_tbi_obj_rec.string_id,
      l_tbi_obj_rec.icon_category1_id,
      l_tbi_obj_rec.icon_category2_id,
      l_tbi_obj_rec.icon_category3_id
                                       )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(880),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(880),NULL)
      RETURN l_tbi_obj_rec.tbi_obj_name

    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(880),NULL)

    RETURN NULL
  END IF

END FUNCTION



#################################################
# FUNCTION tbi_obj_edit(p_tbi_obj_name)
#
# Edit tbi_obj record
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_edit(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name,
    l_key1_tbi_obj_name  LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj_rec        RECORD LIKE qxt_tbi_obj.*,
    local_debug                           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_edit() - Function Entry Point"
    DISPLAY "tbi_obj_edit() - p_tbi_obj_name=", p_tbi_obj_name
  END IF

  #Store the primary key for the SQL update
  LET l_key1_tbi_obj_name = p_tbi_obj_name

  #Get the record (by id)
  CALL get_tbi_obj_rec(p_tbi_obj_name) RETURNING l_tbi_obj_rec.*


	CALL fgl_window_open("w_tbi_obj", 3, 3, get_form_path("f_qxt_tbi_obj_det_l2"), TRUE) 
	CALL populate_tbi_obj_form_edit_labels_g()

  #Call the INPUT
  CALL tbi_obj_input(l_tbi_obj_rec.*) 
    RETURNING l_tbi_obj_rec.*

  CALL fgl_window_close("w_tbi_obj")

  IF local_debug THEN
    DISPLAY "tbi_obj_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.tbi_obj_name=",        l_tbi_obj_rec.tbi_obj_name
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.tbi_obj_action_id=",   l_tbi_obj_rec.tbi_obj_action_id
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.event_type_id=",       l_tbi_obj_rec.event_type_id
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.tbi_obj_event_name=",  l_tbi_obj_rec.tbi_obj_event_name
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.icon_filename=",       l_tbi_obj_rec.icon_filename
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.string_id=",           l_tbi_obj_rec.string_id
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.icon_category1_id=",   l_tbi_obj_rec.icon_category1_id
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.icon_category2_id=",   l_tbi_obj_rec.icon_category2_id
      DISPLAY "tbi_obj_edit() - l_tbi_obj_rec.icon_category3_id=",   l_tbi_obj_rec.icon_category3_id
    END IF


    #Check if the primary key has changed
    IF NOT (l_tbi_obj_rec.tbi_obj_name = l_key1_tbi_obj_name) THEN

      #Create a new entry (key has changed)
      INSERT INTO qxt_tbi_obj VALUES (
        l_tbi_obj_rec.tbi_obj_name,
        l_tbi_obj_rec.event_type_id,
        l_tbi_obj_rec.tbi_obj_event_name,
        l_tbi_obj_rec.tbi_obj_action_id,
        l_tbi_obj_rec.icon_filename,
        l_tbi_obj_rec.string_id,
        l_tbi_obj_rec.icon_category1_id,
        l_tbi_obj_rec.icon_category2_id,
        l_tbi_obj_rec.icon_category3_id
                                       )

      #Update all child table entries with the new key information
      UPDATE qxt_tbi
        SET tbi_obj_name   = l_tbi_obj_rec.tbi_obj_name
      WHERE tbi_obj_name   = l_key1_tbi_obj_name

      #Delete the original row
      DELETE FROM qxt_tbi_obj
      WHERE tbi_obj_name       = l_key1_tbi_obj_name

    ELSE

      UPDATE qxt_tbi_obj
        SET 
          tbi_obj_name =        l_tbi_obj_rec.tbi_obj_name,
          event_type_id =       l_tbi_obj_rec.event_type_id,
          tbi_obj_event_name =  l_tbi_obj_rec.tbi_obj_event_name,
          tbi_obj_action_id =   l_tbi_obj_rec.tbi_obj_action_id,
          icon_filename =       l_tbi_obj_rec.icon_filename,
          string_id =           l_tbi_obj_rec.string_id,
          icon_category1_id =   l_tbi_obj_rec.icon_category1_id,
          icon_category2_id =   l_tbi_obj_rec.icon_category2_id,
          icon_category3_id =   l_tbi_obj_rec.icon_category3_id

        WHERE qxt_tbi_obj.tbi_obj_name = l_key1_tbi_obj_name

    END IF

    IF local_debug THEN
      DISPLAY "tbi_obj_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(880),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(880),NULL)
      RETURN l_tbi_obj_rec.tbi_obj_name
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(880),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF



END FUNCTION




#################################################
# FUNCTION tbi_obj_duplicate(p_tbi_obj_name)
#
# Creates a new tbi_obj based on an existing record
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_duplicate(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name,
    #l_key1_tbi_obj_name  LIKE qxt_tbi_obj.tbi_obj_name,
    source_tbi_obj_rec   RECORD LIKE qxt_tbi_obj.*,
    l_tbi_obj_rec        RECORD LIKE qxt_tbi_obj.*,

    local_debug                           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_obj_duplicate() - Function Entry Point"
    DISPLAY "tbi_obj_duplicate() - p_tbi_obj_name=", p_tbi_obj_name
  END IF

  #Store the primary key for the SQL update
  #LET l_key1_tbi_obj_name = p_tbi_obj_name

  #Get the record (by id)
  CALL get_tbi_obj_rec(p_tbi_obj_name) 
    RETURNING source_tbi_obj_rec.*

  LET l_tbi_obj_rec.* = source_tbi_obj_rec.*
  LET l_tbi_obj_rec.tbi_obj_name = l_tbi_obj_rec.tbi_obj_name, "-!!rename_me!!"

  CALL fgl_window_open("w_tbi_obj", 3, 3, get_form_path("f_qxt_tbi_obj_det_l2"), TRUE) 
  CALL populate_tbi_obj_form_edit_labels_g()

  #Call the INPUT
  CALL tbi_obj_input(l_tbi_obj_rec.*) 
    RETURNING l_tbi_obj_rec.*

  CALL fgl_window_close("w_tbi_obj")

  IF local_debug THEN
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.tbi_obj_name=",       l_tbi_obj_rec.tbi_obj_name
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.event_type_id=",      l_tbi_obj_rec.event_type_id
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.tbi_obj_event_name=", l_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.tbi_obj_action_id=",  l_tbi_obj_rec.tbi_obj_action_id
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.icon_filename=",      l_tbi_obj_rec.icon_filename
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.string_id=",          l_tbi_obj_rec.string_id
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.icon_category1_id=",  l_tbi_obj_rec.icon_category1_id
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.icon_category2_id=",  l_tbi_obj_rec.icon_category2_id
    DISPLAY "tbi_obj_duplicate() - l_tbi_obj_rec.icon_category3_id=",  l_tbi_obj_rec.icon_category3_id
  END IF

  IF NOT int_flag THEN
    INSERT INTO qxt_tbi_obj VALUES (
      l_tbi_obj_rec.tbi_obj_name,
      l_tbi_obj_rec.event_type_id,
      l_tbi_obj_rec.tbi_obj_event_name,
      l_tbi_obj_rec.tbi_obj_action_id,
      l_tbi_obj_rec.icon_filename,
      l_tbi_obj_rec.string_id,
      l_tbi_obj_rec.icon_category1_id,
      l_tbi_obj_rec.icon_category2_id,
      l_tbi_obj_rec.icon_category3_id
                                       )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(880),NULL)
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(880),NULL)
    END IF

    #RETURN sqlca.sqlerrd[2]
    RETURN l_tbi_obj_rec.tbi_obj_name
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(880),NULL)

    RETURN NULL
  END IF


END FUNCTION



#################################################
# FUNCTION tbi_obj_input(p_tbi_obj_rec)
#
# Input tbi_obj details (edit/create)
#
# RETURN l_tbi_obj.*
#################################################
FUNCTION tbi_obj_input(p_tbi_obj_rec)
  DEFINE 
    p_tbi_obj_rec             RECORD LIKE qxt_tbi_obj.*,
    l_tbi_obj_form_rec        OF t_qxt_tbi_obj_form_rec,
    l_orignal_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    l_orignal_icon_filename   LIKE qxt_tbi_obj.icon_filename,
    local_debug               SMALLINT,
    tmp_str                   VARCHAR(250),
    tmp_string_id             LIKE qxt_tbi_tooltip.string_id,
    tmp_language_id           LIKE qxt_tbi_tooltip.language_id,
    tmp_icon_filename         LIKE qxt_icon.icon_filename,
    tmp_icon_category_id      LIKE qxt_icon_category.icon_category_id,
    tmp_tbi_obj_event_name    LIKE qxt_tbi_obj_event.tbi_obj_event_name,
    l_category_data             LIKE qxt_icon_category.icon_category_data,
    l_category_id               LIKE qxt_icon_category.icon_category_id,
    l_language_id               LIKE qxt_icon_category.language_id,
    l_icon_category_rec         RECORD LIKE qxt_icon_category.*,
    l_current_field_name        VARCHAR(30)

  LET local_debug = FALSE

  LET int_flag = FALSE

  LET l_language_id = get_language()

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_tbi_obj_name = p_tbi_obj_rec.tbi_obj_name
  LET l_orignal_icon_filename = p_tbi_obj_rec.icon_filename

  #copy record data to form_record format record
  CALL copy_tbi_obj_record_to_form_record(p_tbi_obj_rec.*) RETURNING l_tbi_obj_form_rec.*




  INPUT BY NAME l_tbi_obj_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      #Display the actual string_data
      DISPLAY get_tbi_label_data(l_tbi_obj_form_rec.string_id,get_language()) TO label_data
      DISPLAY get_tbi_tooltip_data(l_tbi_obj_form_rec.string_id,get_language()) TO string_data

      #Update icon preview
      CALL icon_preview(l_tbi_obj_form_rec.icon_filename)


    ################################
    # Key/Action Section
    ################################

    #Tbi_Obj_Event

    ON ACTION ("editTbiObjEvent")  --Create new event object
      LET tmp_tbi_obj_event_name = tbi_obj_event_edit(l_tbi_obj_form_rec.tbi_obj_event_name)

      IF tmp_tbi_obj_event_name IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.tbi_obj_event_name = tmp_tbi_obj_event_name
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
      END IF

    ON ACTION ("newTbiObjEvent")  --Create new event object
      LET tmp_tbi_obj_event_name = tbi_obj_event_create()

      IF tmp_tbi_obj_event_name IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.tbi_obj_event_name = tmp_tbi_obj_event_name
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
      END IF


    ON ACTION ("manageTbiObjEvent")  --Manage Event objects
      LET tmp_tbi_obj_event_name = tbi_obj_event_popup(l_tbi_obj_form_rec.tbi_obj_event_name,NULL,0)
      IF tmp_tbi_obj_event_name IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.tbi_obj_event_name = tmp_tbi_obj_event_name
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.tbi_obj_event_name TO tbi_obj_event_name
      END IF



    #Icon

    ON ACTION ("editIcon")  --Create new icon object
      LET tmp_icon_filename = icon_edit(l_tbi_obj_form_rec.icon_filename)
      IF tmp_icon_filename IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.icon_filename = tmp_icon_filename
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename

        #Update icon preview
        CALL icon_preview(l_tbi_obj_form_rec.icon_filename)

      END IF

    ON ACTION ("newIcon")  --Create new icon object
      LET tmp_icon_filename = icon_create(NULL,NULL,NULL,NULL)
      IF tmp_icon_filename IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.icon_filename = tmp_icon_filename
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename

        #Update icon preview
        CALL icon_preview(l_tbi_obj_form_rec.icon_filename)

      END IF

    ON ACTION ("manageIcon")  --Manage icon object
      LET tmp_icon_filename = icon_popup(l_tbi_obj_form_rec.icon_filename,get_language(),NULL,0)
      IF tmp_icon_filename IS NOT NULL THEN
        #Assign the new file name 
        LET l_tbi_obj_form_rec.icon_filename = tmp_icon_filename
        #refresh the combo list
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the filenae combo field
        DISPLAY l_tbi_obj_form_rec.icon_filename TO icon_filename

        #Update icon preview
        CALL icon_preview(l_tbi_obj_form_rec.icon_filename)

      END IF


    #Tooltip
    ON ACTION("editTooltip")  --EDIT string object
      CALL tbi_tooltip_edit(l_tbi_obj_form_rec.string_id,get_data_language_id(),NULL,TRUE)
        RETURNING tmp_string_id, tmp_language_id

      #Check for valid return - i.e. user abort returns NULL

      IF tmp_string_id IS NOT NULL OR tmp_string_id >0 THEN
        #get the id and data of the new object
        LET l_tbi_obj_form_rec.string_id = tmp_string_id
        LET l_tbi_obj_form_rec.string_data = get_tbi_tooltip_data(tmp_string_id,get_language())
        #refresh the combo list
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the combo fields
        DISPLAY l_tbi_obj_form_rec.string_id TO string_id
        DISPLAY l_tbi_obj_form_rec.string_data TO string_data
      END IF

		ON ACTION("newTooltip")  --Create new string object
			CALL tbi_tooltip_create(NULL,get_data_language_id(), get_tbi_label_data(l_tbi_obj_form_rec.string_id, get_data_language_id() ), get_tbi_tooltip_data(l_tbi_obj_form_rec.string_id, get_data_language_id() ), TRUE)
			#FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)
        RETURNING tmp_string_id, tmp_language_id
      #Check for valid return - i.e. user abort returns NULL

      IF tmp_string_id IS NOT NULL OR tmp_string_id >0 THEN
        #get the id and data of the new object
        LET l_tbi_obj_form_rec.string_id = tmp_string_id
        LET l_tbi_obj_form_rec.string_data = get_tbi_tooltip_data(tmp_string_id,get_language())
        #refresh the combo list
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the combo fields
        DISPLAY l_tbi_obj_form_rec.string_id TO string_id
        DISPLAY l_tbi_obj_form_rec.string_data TO string_data
      END IF

    ON ACTION("manageTooltip")  --Manage string object
      CALL tbi_tooltip_popup(l_tbi_obj_form_rec.string_id,get_language(),NULL,0)
        RETURNING tmp_string_id, tmp_language_id

      #Check for valid return - i.e. user abort returns NULL

      IF tmp_string_id IS NOT NULL OR tmp_string_id >0 THEN
        #get the id and data of the new object
        LET l_tbi_obj_form_rec.string_id = tmp_string_id
        LET l_tbi_obj_form_rec.string_data = get_tbi_tooltip_data(tmp_string_id,get_language())
        #refresh the combo list
        CALL populate_tbi_obj_combo_lists_g()
        #display the new value to the combo fields
        DISPLAY l_tbi_obj_form_rec.string_id TO string_id
        DISPLAY l_tbi_obj_form_rec.string_data TO string_data
      END IF

#@@@

    #Icon Category
    ON ACTION("editIconCategory")  --EDIT icon category
      LET l_current_field_name = fgl_dialog_getfieldname()
      LET l_category_id = get_current_field_obj_icon_category_id(l_tbi_obj_form_rec.*,l_current_field_name,l_language_id)
      #LET l_category_data = get_icon_category_data(l_category_id,l_language_id)
      CALL icon_category_edit(l_category_id,l_language_id)
        RETURNING l_icon_category_rec.*

      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_icon_combo_list()
        CALL update_obj_category_field(l_tbi_obj_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_tbi_obj_form_rec.*
      END IF


    ON ACTION("newIconCategory")  --Create new category
      LET l_current_field_name = fgl_dialog_getfieldname()
      LET l_category_id = get_current_field_obj_icon_category_id(l_tbi_obj_form_rec.*,l_current_field_name,l_language_id)
      CALL icon_category_create(NULL,l_language_id,NULL)
        RETURNING l_icon_category_rec.*

      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_tbi_obj_combo_lists_g()
        CALL update_obj_category_field(l_tbi_obj_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_tbi_obj_form_rec.*
      END IF


{
      LET tmp_icon_category_id =  icon_category_create()

      #Refresh combo list boxes
      CALL populate_tbi_obj_combo_lists_g()



    #Find out, if the cursor is in a category field and update it correspondingly
    CASE fgl_dialog_getfieldname()
      WHEN "icon_category1_name"
        LET l_tbi_obj_form_rec.icon_category1_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_obj_form_rec.icon_category1_data TO icon_category1_data

      WHEN "icon_category2_name"
        LET l_tbi_obj_form_rec.icon_category2_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_obj_form_rec.icon_category2_data TO icon_category2_data

      WHEN "icon_category3_name"
        LET l_tbi_obj_form_rec.icon_category3_data = get_icon_category_data(tmp_icon_category_id,get_language())
        DISPLAY l_tbi_obj_form_rec.icon_category3_data TO icon_category3_data

      OTHERWISE
        #do nothing
    ENd CASE
}
    ON ACTION("manageIconCategory")  --Manage category
      LET l_current_field_name = fgl_dialog_getfieldname()
      CALL icon_category_popup(get_current_field_obj_icon_category_id(l_tbi_obj_form_rec.*,fgl_dialog_getfieldname(),l_language_id),l_language_id ,NULL,NULL)
        RETURNING l_icon_category_rec.*
      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_tbi_obj_combo_lists_g()
        CALL update_obj_category_field(l_tbi_obj_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_tbi_obj_form_rec.*
      END IF


{ fgl_dialog_getfieldname() is broken

      #Refresh combo list boxes
      CALL populate_tbi_obj_combo_lists_g()



    #Find out, if the cursor is in a category field and update it correspondingly
    CASE fgl_dialog_getfieldname()

      WHEN "icon_category1_name"
        LET tmp_icon_category_id =  icon_category_popup(get_icon_category_id()
        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category1_data

      WHEN "icon_category2_name"
        LET tmp_icon_category_id =  icon_category_popup()
        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category2_data

      WHEN "icon_category3_name"
        LET tmp_icon_category_id =  icon_category_popup()
        DISPLAY get_icon_category_data(tmp_icon_category_id,get_language()) TO icon_category3_data
      OTHERWISE
        #do nothing
    ENd CASE
}

    #########################
    # BEFORE & AFTER Field Section
    #########################

    AFTER FIELD tbi_obj_name
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(881), l_tbi_obj_form_rec.tbi_obj_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_name
      END IF

      IF l_orignal_tbi_obj_name IS NULL OR l_orignal_tbi_obj_name <> l_tbi_obj_form_rec.tbi_obj_name THEN
        IF tbi_obj_name_count(l_tbi_obj_form_rec.tbi_obj_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_key(get_str_tool(881),NULL)
          NEXT FIELD tbi_obj_name
        END IF
      END IF

    AFTER FIELD event_type_name
      #dir must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(882), l_tbi_obj_form_rec.event_type_name,NULL,TRUE)  THEN
        NEXT FIELD event_type_name
      END IF

    AFTER FIELD tbi_obj_event_name
      #dir must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(883), l_tbi_obj_form_rec.tbi_obj_event_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_event_name
      END IF

    AFTER FIELD tbi_action_name
      #dir must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(884), l_tbi_obj_form_rec.tbi_action_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_action_name
      END IF

    BEFORE FIELD icon_filename  --keep a duplicate to see, if the value has changed - only inherite properties for changed entries
      LET tmp_icon_filename = l_tbi_obj_form_rec.icon_filename

    AFTER FIELD icon_filename
      #dir must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(885), l_tbi_obj_form_rec.icon_filename,NULL,TRUE)  THEN
        NEXT FIELD icon_filename
      END IF

      #Update icon preview
      CALL icon_preview(l_tbi_obj_form_rec.icon_filename)

      #Inherite the Category Information if the field has changed - user can still change it...
      IF l_tbi_obj_form_rec.icon_filename <> tmp_icon_filename OR  tmp_icon_filename IS NULL THEN
        IF yes_no("Toolbar Menu Item",get_str_tool(862)) THEN
  
          LET l_tbi_obj_form_rec.icon_category1_data = get_icon_category_data(get_icon_category1_id(l_tbi_obj_form_rec.icon_filename),get_language())
          LET l_tbi_obj_form_rec.icon_category2_data = get_icon_category_data(get_icon_category2_id(l_tbi_obj_form_rec.icon_filename),get_language())
          LET l_tbi_obj_form_rec.icon_category3_data = get_icon_category_data(get_icon_category3_id(l_tbi_obj_form_rec.icon_filename),get_language())

          DISPLAY l_tbi_obj_form_rec.icon_category1_data TO icon_category1_data
          DISPLAY l_tbi_obj_form_rec.icon_category2_data TO icon_category2_data
          DISPLAY l_tbi_obj_form_rec.icon_category3_data TO icon_category3_data
        END IF

    END IF

    AFTER FIELD string_id
      #dir must not be empty
      IF NOT validate_field_value_numeric_exists(get_str_tool(886), l_tbi_obj_form_rec.string_id,NULL,TRUE)  THEN
        NEXT FIELD string_id
      END IF
      LET l_tbi_obj_form_rec.string_data = get_tbi_tooltip_data(l_tbi_obj_form_rec.string_id,get_language())
      DISPLAY get_tbi_tooltip_data(l_tbi_obj_form_rec.string_id,get_language()) TO string_data

    
    AFTER FIELD string_data
      LET  l_tbi_obj_form_rec.string_id = get_tbi_tooltip_id(l_tbi_obj_form_rec.string_data,get_language())
      DISPLAY l_tbi_obj_form_rec.string_id TO string_id



    # AFTER INPUT BLOCK ####################################

    AFTER INPUT
      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF
      #tbi_obj_name must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(881), l_tbi_obj_form_rec.tbi_obj_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_name
        CONTINUE INPUT
      END IF

      #event_type_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(882), l_tbi_obj_form_rec.event_type_name,NULL,TRUE)  THEN
        NEXT FIELD event_type_name
        CONTINUE INPUT
      END IF


      #tbi_obj_event_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(883), l_tbi_obj_form_rec.tbi_obj_event_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_obj_event_name
        CONTINUE INPUT
      END IF

      #tbi_action_name must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(884), l_tbi_obj_form_rec.tbi_action_name,NULL,TRUE)  THEN
        NEXT FIELD tbi_action_name
        CONTINUE INPUT
      END IF

      #icon_filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(885), l_tbi_obj_form_rec.icon_filename,NULL,TRUE)  THEN
        NEXT FIELD icon_filename
        CONTINUE INPUT
      END IF

      #string_id must not be empty
      IF NOT validate_field_value_numeric_exists(get_str_tool(886), l_tbi_obj_form_rec.string_id,NULL,TRUE)  THEN
        NEXT FIELD string_id
        CONTINUE INPUT
      END IF

      #The tbi_obj_name must be unique
      IF l_orignal_tbi_obj_name IS NULL OR l_orignal_tbi_obj_name <> l_tbi_obj_form_rec.tbi_obj_name THEN
        IF tbi_obj_name_count(l_tbi_obj_form_rec.tbi_obj_name) THEN
        #Constraint only exists for newly created records (not modify)
          CALL  tl_msg_duplicate_key(get_str_tool(881),NULL)
          NEXT FIELD tbi_obj_name
          CONTINUE INPUT
        END IF
      END IF

  END INPUT
  # END INPUT BLOCK  ##########################

  IF local_debug THEN
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.tbi_obj_name=",l_tbi_obj_form_rec.tbi_obj_name
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.event_type_name=",l_tbi_obj_form_rec.event_type_name
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.tbi_obj_event_name=",l_tbi_obj_form_rec.tbi_obj_event_name
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.tbi_action_name=",l_tbi_obj_form_rec.tbi_action_name
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.icon_filename=",l_tbi_obj_form_rec.icon_filename
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.string_id=",l_tbi_obj_form_rec.string_id
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.icon_category1_data=",l_tbi_obj_form_rec.icon_category1_data
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.icon_category2_data=",l_tbi_obj_form_rec.icon_category2_data
    DISPLAY "tbi_obj_input() - l_tbi_obj_form_rec.icon_category3_data=",l_tbi_obj_form_rec.icon_category3_data
  END IF


  #Copy the form record data to a normal tbi_obj record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_tbi_obj_form_record_to_record(l_tbi_obj_form_rec.*) RETURNING p_tbi_obj_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.tbi_obj_name=",p_tbi_obj_rec.tbi_obj_name
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.event_type_id=",p_tbi_obj_rec.event_type_id
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.tbi_obj_event_name=",p_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.tbi_obj_action_id=",p_tbi_obj_rec.tbi_obj_action_id
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.tbi_obj_event_name=",p_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.icon_filename=",p_tbi_obj_rec.icon_filename
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.string_id=",p_tbi_obj_rec.string_id
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.icon_category1_id=",p_tbi_obj_rec.icon_category1_id
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.icon_category2_id=",p_tbi_obj_rec.icon_category2_id
    DISPLAY "tbi_obj_input() - p_tbi_obj_rec.icon_category3_id=",p_tbi_obj_rec.icon_category3_id
  END IF

  RETURN p_tbi_obj_rec.*

END FUNCTION



#################################################
# FUNCTION tbi_obj_delete(p_tbi_obj_name)
#
# Delete a tbi_obj record
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_delete(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name

  #do you really want to delete...
  IF question_delete_record(get_str_tool(880), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_tbi_obj 
        WHERE qxt_tbi_obj_name = p_tbi_obj_name
    COMMIT WORK

    RETURN TRUE
  ELSE
    RETURN FALSE
  END IF

END FUNCTION


#################################################################################################
# View Record Functions
#################################################################################################


#################################################
# FUNCTION tbi_obj_view(p_tbi_obj_name)
#
# View tbi_obj record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_view(p_tbi_obj_name)
  DEFINE 
    p_tbi_obj_name     LIKE qxt_tbi_obj.tbi_obj_name,
    l_tbi_obj_rec      RECORD LIKE qxt_tbi_obj.*


  CALL get_tbi_obj_rec(p_tbi_obj_name) RETURNING l_tbi_obj_rec.*
  CALL tbi_obj_view_by_rec(l_tbi_obj_rec.*)

END FUNCTION


#################################################
# FUNCTION tbi_obj_view_by_rec(p_tbi_obj_rec)
#
# View tbi_obj record in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_obj_view_by_rec(p_tbi_obj_rec)
  DEFINE 
    p_tbi_obj_rec  RECORD LIKE qxt_tbi_obj.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

	CALL fgl_window_open("w_tbi_obj", 3, 3, get_form_path("f_qxt_tbi_obj_det_l2"), TRUE) 
	CALL populate_tbi_obj_form_view_labels_g()

  #Display the images (3 sizes) to the preview panel
  #Update the image preview fields - if they don't exist, display a 'missing' image
  #Icon 16x16
  IF validate_file_server_side_exists_advanced(get_icon16_path(p_tbi_obj_rec.icon_filename),"f",NULL) THEN
    IF validate_file_client_side_cache_exists(get_icon16_path(p_tbi_obj_rec.icon_filename), "Verify, if the image is included in your project-media tree (IDE",NULL) THEN
      DISPLAY get_icon16_path(p_tbi_obj_rec.icon_filename) TO preview1
    ELSE
      DISPLAY get_icon16_path("missing_16x16.ico") TO preview1
    END IF
  END IF

  #Icon 24x24
  IF validate_file_server_side_exists_advanced(get_icon24_path(p_tbi_obj_rec.icon_filename),"f",NULL) THEN
    IF validate_file_client_side_cache_exists(get_icon24_path(p_tbi_obj_rec.icon_filename), "Verify, if the image is included in your project-media tree (IDE",NULL) THEN
      DISPLAY get_icon24_path(p_tbi_obj_rec.icon_filename) TO preview2
    ELSE
      DISPLAY get_icon24_path("missing_24x24.ico") TO preview2
    END IF
  END IF

  #Icon 32x32
  IF validate_file_server_side_exists_advanced(get_icon32_path(p_tbi_obj_rec.icon_filename),"f",NULL) THEN
    IF validate_file_client_side_cache_exists(get_icon32_path(p_tbi_obj_rec.icon_filename), "Verify, if the image is included in your project-media tree (IDE",NULL) THEN
      DISPLAY get_icon32_path(p_tbi_obj_rec.icon_filename) TO preview3
    ELSE
      DISPLAY get_icon32_path("missing_32x32.ico") TO preview3
    END IF
  END IF

  DISPLAY BY NAME p_tbi_obj_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_tbi_obj")
END FUNCTION


{
####################################################
# FUNCTION icon_filename_count(p_dir)
#
# tests if a record with this dir already exists
#
# RETURN r_count
####################################################
FUNCTION icon_filename_count(p_filename)
  DEFINE
    p_filename     LIKE qxt_tbi_obj.icon_filename,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_obj
      WHERE qxt_tbi_obj.icon_filename = p_filename

  RETURN r_count   --0 = unique  0> is not

END FUNCTION
}

####################################################
# FFUNCTION tbi_obj_name_count(p_tbi_obj_name)
#
# tests if a record with this tbi_obj_name already exists
#
# RETURN r_count
####################################################
FUNCTION tbi_obj_name_count(p_tbi_obj_name)
  DEFINE
    p_tbi_obj_name    LIKE qxt_tbi_obj.tbi_obj_name,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_obj
      WHERE qxt_tbi_obj.tbi_obj_name = p_tbi_obj_name

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


######################################################
# FUNCTION copy_tbi_obj_record_to_form_record(p_tbi_obj_rec)  
#
# Copy normal tbi_obj record data to type tbi_obj_form_rec
#
# RETURN l_tbi_obj_form_rec.*
######################################################
FUNCTION copy_tbi_obj_record_to_form_record(p_tbi_obj_rec)  
  DEFINE
    p_tbi_obj_rec       RECORD LIKE qxt_tbi_obj.*,
    l_tbi_obj_form_rec  OF t_qxt_tbi_obj_form_rec,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  LET l_tbi_obj_form_rec.tbi_obj_name =         p_tbi_obj_rec.tbi_obj_name
  LET l_tbi_obj_form_rec.event_type_name =      get_event_type_name(p_tbi_obj_rec.event_type_id)
  LET l_tbi_obj_form_rec.tbi_obj_event_name =   p_tbi_obj_rec.tbi_obj_event_name
  LET l_tbi_obj_form_rec.tbi_action_name =  get_tbi_action_name(p_tbi_obj_rec.tbi_obj_action_id)
  LET l_tbi_obj_form_rec.icon_filename =        p_tbi_obj_rec.icon_filename
  LET l_tbi_obj_form_rec.string_id =            p_tbi_obj_rec.string_id
  LET l_tbi_obj_form_rec.icon_category1_data =  get_icon_category_data(p_tbi_obj_rec.icon_category1_id,get_language())
  LET l_tbi_obj_form_rec.icon_category2_data =  get_icon_category_data(p_tbi_obj_rec.icon_category2_id,get_language())
  LET l_tbi_obj_form_rec.icon_category3_data =  get_icon_category_data(p_tbi_obj_rec.icon_category3_id,get_language())

  IF local_debug THEN
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.tbi_obj_name=",      p_tbi_obj_rec.tbi_obj_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.event_type_id=",     p_tbi_obj_rec.event_type_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.tbi_obj_event_name=",p_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.tbi_obj_action_id=", p_tbi_obj_rec.tbi_obj_action_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.icon_filename=",     p_tbi_obj_rec.icon_filename
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.string_id=",         p_tbi_obj_rec.string_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.icon_category1_id=", p_tbi_obj_rec.icon_category1_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.icon_category2_id=", p_tbi_obj_rec.icon_category2_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - p_tbi_obj_rec.icon_category3_id=", p_tbi_obj_rec.icon_category3_id


    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.tbi_obj_name=",       l_tbi_obj_form_rec.tbi_obj_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.event_type_name=",    l_tbi_obj_form_rec.event_type_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.tbi_obj_event_name=", l_tbi_obj_form_rec.tbi_obj_event_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.tbi_action_name=",l_tbi_obj_form_rec.tbi_action_name
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.icon_filename=",      l_tbi_obj_form_rec.icon_filename
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.string_id=",          l_tbi_obj_form_rec.string_id
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.icon_category1_data=",l_tbi_obj_form_rec.icon_category1_data
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.icon_category2_data=",l_tbi_obj_form_rec.icon_category2_data
    DISPLAY "copy_tbi_obj_record_to_form_record() - l_tbi_obj_form_rec.icon_category3_data=",l_tbi_obj_form_rec.icon_category3_data
  END IF

  RETURN l_tbi_obj_form_rec.*

END FUNCTION


######################################################
# FUNCTION copy_tbi_obj_form_record_to_record(p_tbi_obj_form_rec)  
#
# Copy type tbi_obj_form_rec to normal tbi_obj record data
#
# RETURN l_tbi_obj_rec.*
######################################################
FUNCTION copy_tbi_obj_form_record_to_record(p_tbi_obj_form_rec)  
  DEFINE
    l_tbi_obj_rec       RECORD LIKE qxt_tbi_obj.*,
    p_tbi_obj_form_rec  OF t_qxt_tbi_obj_form_rec,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  LET l_tbi_obj_rec.tbi_obj_name =       p_tbi_obj_form_rec.tbi_obj_name
  LET l_tbi_obj_rec.event_type_id =      get_event_type_id(p_tbi_obj_form_rec.event_type_name)
  LET l_tbi_obj_rec.tbi_obj_event_name = p_tbi_obj_form_rec.tbi_obj_event_name
  LET l_tbi_obj_rec.tbi_obj_action_id =  get_tbi_action_id(p_tbi_obj_form_rec.tbi_action_name)
  LET l_tbi_obj_rec.icon_filename =      p_tbi_obj_form_rec.icon_filename
  LET l_tbi_obj_rec.string_id =          p_tbi_obj_form_rec.string_id
  LET l_tbi_obj_rec.icon_category1_id =  get_icon_category_id(p_tbi_obj_form_rec.icon_category1_data,get_language())
  LET l_tbi_obj_rec.icon_category2_id =  get_icon_category_id(p_tbi_obj_form_rec.icon_category2_data,get_language())
  LET l_tbi_obj_rec.icon_category3_id =  get_icon_category_id(p_tbi_obj_form_rec.icon_category3_data,get_language())


  IF local_debug THEN

    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.tbi_obj_name=",        p_tbi_obj_form_rec.tbi_obj_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.event_type_name=",     p_tbi_obj_form_rec.event_type_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.tbi_obj_event_name=",  p_tbi_obj_form_rec.tbi_obj_event_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.tbi_action_name=", p_tbi_obj_form_rec.tbi_action_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.icon_filename=",       p_tbi_obj_form_rec.icon_filename
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.string_id=",           p_tbi_obj_form_rec.string_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.icon_category1_data=", p_tbi_obj_form_rec.icon_category1_data
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.icon_category2_data=", p_tbi_obj_form_rec.icon_category2_data
    DISPLAY "copy_tbi_obj_form_record_to_record() - p_tbi_obj_form_rec.icon_category3_data=", p_tbi_obj_form_rec.icon_category3_data

    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.tbi_obj_name=",             l_tbi_obj_rec.tbi_obj_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.event_type_id=",            l_tbi_obj_rec.event_type_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.tbi_obj_event_name=",       l_tbi_obj_rec.tbi_obj_event_name
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.tbi_obj_action_id=",        l_tbi_obj_rec.tbi_obj_action_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.icon_filename=",            l_tbi_obj_rec.icon_filename
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.string_id=",                l_tbi_obj_rec.string_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.icon_category1_id=",        l_tbi_obj_rec.icon_category1_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.icon_category2_id=",        l_tbi_obj_rec.icon_category2_id
    DISPLAY "copy_tbi_obj_form_record_to_record() - l_tbi_obj_rec.icon_category3_id=",        l_tbi_obj_rec.icon_category3_id

  END IF


  RETURN l_tbi_obj_rec.*

END FUNCTION

######################################################################################################
# Display Object functions
######################################################################################################

####################################################
# FUNCTION grid_header_tbi_obj_scroll()
#
# Populate tbi_obj grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_tbi_obj_scroll()
  CALL fgl_grid_header("sc_tbi_obj","tbi_obj_name",        get_str_tool(881),"left","F13") 
  CALL fgl_grid_header("sc_tbi_obj","event_type_name",     get_str_tool(882),"left", "F14")  
  CALL fgl_grid_header("sc_tbi_obj","tbi_obj_event_name",  get_str_tool(883),"left", "F15")  
  CALL fgl_grid_header("sc_tbi_obj","tbi_action_name", get_str_tool(884),"left", "F16")  
  CALL fgl_grid_header("sc_tbi_obj","icon_filename",       get_str_tool(885),"left", "F17")  
  CALL fgl_grid_header("sc_tbi_obj","string_id",           get_str_tool(886),"right", "F18") 
  CALL fgl_grid_header("sc_tbi_obj","string_data",         get_str_tool(886),"left", "F19")  
  CALL fgl_grid_header("sc_tbi_obj","icon_category1_data", get_str_tool(887),"left", "F20")  
  CALL fgl_grid_header("sc_tbi_obj","icon_category2_data", get_str_tool(888),"left", "F21") 
  CALL fgl_grid_header("sc_tbi_obj","icon_category3_data", get_str_tool(889),"left", "F22")  


END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_combo_lists_g()
#
# Populate all combo lists in the form
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_combo_lists_g()

  CALL tbi_obj_combo_list("tbi_obj_name")

  CALL event_type_name_combo_list("event_type_name")
  CALL tbi_tooltip_id_combo_list("string_id", get_language())
  CALL tbi_tooltip_data_combo_list("string_data", get_language())

  CALL tbi_obj_event_name_combo_list("tbi_obj_event_name")

  CALL toolbar_menu_action_name_combo_list("tbi_action_name")

  CALL icon_filename_combo_list("icon_filename")

  CALL icon_category_data_combo_list("icon_category1_data",get_language())
  CALL icon_category_data_combo_list("icon_category2_data",get_language())
  CALL icon_category_data_combo_list("icon_category3_data",get_language())

END FUNCTION


#######################################################
# FUNCTION update_management_buttons
#
# Populate a list of all ext data management buttons
#
# RETURN NONE
#######################################################
FUNCTION update_management_buttons()
  #Buttons for external Data management calls
  DISPLAY get_str_tool(7) TO bt_edit_event
  #DISPLAY "!" TO bt_edit_event

  DISPLAY get_str_tool(18) TO bt_new_event
  #DISPLAY "!" TO bt_new_event

  DISPLAY get_str_tool(21) TO bt_manage_event
  #DISPLAY "!" TO bt_manage_event


  DISPLAY get_str_tool(7) TO bt_edit_icon
  #DISPLAY "!" TO bt_edit_icon

  DISPLAY get_str_tool(18) TO bt_new_icon
  #DISPLAY "!" TO bt_new_icon

  DISPLAY get_str_tool(21) TO bt_manage_icon
  #DISPLAY "!" TO bt_manage_icon


  DISPLAY get_str_tool(7) TO bt_edit_tooltip
  #DISPLAY "!" TO bt_edit_tooltip

  DISPLAY get_str_tool(18) TO bt_new_tooltip
  #DISPLAY "!" TO bt_new_tooltip

  DISPLAY get_str_tool(21) TO bt_manage_tooltip
  #DISPLAY "!" TO bt_manage_tooltip


  DISPLAY get_str_tool(7) TO bt_edit_category
  #DISPLAY "!" TO bt_edit_category

  DISPLAY get_str_tool(18) TO bt_new_category
  #DISPLAY "!" TO bt_new_category

  DISPLAY get_str_tool(21) TO bt_manage_category
  #DISPLAY "!" TO bt_manage_category

END FUNCTION

#######################################################
# FUNCTION populate_tbi_obj_form_labels_g()
#
# Populate tbi_obj form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_labels_g()

  CALL fgl_settitle(get_str_tool(880))

  DISPLAY get_str_tool(880) TO lbTitle

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel


  #External calls buttons
  CALL update_management_buttons()

  #Populate combo list boxes with data from db
  CALL populate_tbi_obj_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)


END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_form_labels_t()
#
# Populate tbi_obj form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_labels_t()

  DISPLAY get_str_tool(880) TO lbTitle

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_form_edit_labels_g()
#
# Populate tbi_obj form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(880)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel


  #External calls buttons
  CALL update_management_buttons()


  #Populate combo list boxes with data from db
  CALL populate_tbi_obj_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_form_edit_labels_t()
#
# Populate tbi_obj form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_edit_labels_t()

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_form_view_labels_g()
#
# Populate tbi_obj form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(880)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #External calls buttons
  CALL update_management_buttons()

  #Populate combo list boxes with data from db
  CALL populate_tbi_obj_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_form_view_labels_t()
#
# Populate tbi_obj form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_view_labels_t()

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1

  #CALL language_combo_list("language_dir")

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_form_create_labels_g()
#
# Populate tbi_obj form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(880)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #External calls buttons
  CALL update_management_buttons()

  #Populate combo list boxes with data from db
  CALL populate_tbi_obj_combo_lists_g()

  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION



#######################################################
# FUNCTION populate_tbi_obj_form_create_labels_t()
#
# Populate tbi_obj form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_form_create_labels_t()

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_list_form_labels_g()
#
# Populate tbi_obj list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_list_form_labels_g()
  CALL updateUILabel("icon_category_filter_switch","Category")
  CALL updateUILabel("tbi_obj_name_filter_switch","Obj")
  CALL fgl_settitle(trim(get_str_tool(880)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_tbi_obj_scroll()

  #DISPLAY get_str_tool(881) TO dl_f1
  #DISPLAY get_str_tool(889) TO dl_f2
  #DISPLAY get_str_tool(889) TO dl_f3
  #DISPLAY get_str_tool(889) TO dl_f4
  #DISPLAY get_str_tool(889) TO dl_f5
  #DISPLAY get_str_tool(889) TO dl_f6
  #DISPLAY get_str_tool(889) TO dl_f7
  #DISPLAY get_str_tool(889) TO dl_f8
  #DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  DISPLAY get_str_tool(22) TO bt_duplicate
  #DISPLAY "!" TO bt_duplicate

  #Enable Category Filter check box
  #DISPLAY "!" TO icon_category_filter_switch
  #DISPLAY "!" TO tbi_obj_name_filter_switch

  #Enbable button to specify filter criteria
  #DISPLAY "!" TO bt_set_filter_icon_category
  #DISPLAY "!" TO bt_set_filter_tbi_obj_name
  #DISPLAY "!" TO bt_set_filter_language_name

  CALL icon_category_data_combo_list("cs1",get_data_language_id())
  CALL icon_category_data_combo_list("cs2",get_data_language_id())
  CALL icon_category_data_combo_list("cs3",get_data_language_id())

  CALL language_combo_list("filter_lang_n")

  #Initialise icon preview
  CALL icon_preview(NULL)


END FUNCTION


#######################################################
# FUNCTION populate_tbi_obj_list_form_labels_t()
#
# Populate tbi_obj list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_obj_list_form_labels_t()

  DISPLAY trim(get_str_tool(880)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(881) TO dl_f1
  DISPLAY get_str_tool(882) TO dl_f2
  DISPLAY get_str_tool(883) TO dl_f3
  DISPLAY get_str_tool(884) TO dl_f4
  DISPLAY get_str_tool(885) TO dl_f5
  DISPLAY get_str_tool(886) TO dl_f6
  DISPLAY get_str_tool(887) TO dl_f7
  DISPLAY get_str_tool(888) TO dl_f8
  DISPLAY get_str_tool(889) TO dl_f9

  #DISPLAY get_str_tool(889) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION


#@@@
#######################################################
# FUNCTION update_category_field(p_field_name,p_data)
#
# Update/Refresh Data for the corresponding field
#
# RETURN p_icon_form_rec.*
#######################################################
FUNCTION update_obj_category_field(p_tbi_obj_form_rec,p_field_name,p_data)
  DEFINE
    p_tbi_obj_form_rec        OF t_qxt_tbi_obj_form_rec,
    #p_icon_form_rec OF t_qxt_icon_form_rec,
    p_field_name    VARCHAR(30),
    p_data          VARCHAR(40),
    local_debug     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_filename = ", p_tbi_obj_form_rec.icon_filename
    DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category1_data = ", p_tbi_obj_form_rec.icon_category1_data
    DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category2_data = ", p_tbi_obj_form_rec.icon_category2_data
    DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category3_data = ", p_tbi_obj_form_rec.icon_category3_data
    DISPLAY "update_category_field() - p_field_name= ", p_field_name
    DISPLAY "update_category_field() - p_data= ", p_data
  END IF

  #Find out, if the cursor is in a category field and update it correspondingly
  CASE p_field_name
    WHEN "icon_category1_data"
      DISPLAY p_data TO icon_category1_data
      LET p_tbi_obj_form_rec.icon_category1_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category1_data"
        DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category1_data =  ", p_data
      END IF

    WHEN "icon_category2_data"
      DISPLAY p_data TO icon_category2_data
      LET p_tbi_obj_form_rec.icon_category2_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category2_data"
        DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category2_data =  ", p_data
      END IF

    WHEN "icon_category3_data"
      DISPLAY p_data TO icon_category3_data
      LET p_tbi_obj_form_rec.icon_category3_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category3_data"
        DISPLAY "update_category_field() - p_tbi_obj_form_rec.icon_category3_data =  ", p_data
      END IF

    OTHERWISE
      #do nothing
  END CASE

  RETURN p_tbi_obj_form_rec.*

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################














































