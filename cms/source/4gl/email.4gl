##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

########################################################################################################################
# Email functions
########################################################################################################################
#
# FUNCTION:                                             DESCRIPTION:                                   RETURN
# email_write(p_contact_id, l_subject,                  Write an E-Mail                                NONE
#             l_body, actid, p_operator_id2)          
# email_send((p_email, p_subject, p_body,               Send an E-Mail                                 INT i   (i = sqlca.sqlerrd[2])
#            actid, p_operator)
# email_view(p_email_id,direction)                      displays email details in window               NONE
# email_reply (p_mail_id)                               Reply to an email                              NONE
# email_create(p_email_rec)                             Create an mailbox & activity rec               mailbox_id
# rec_copy_t_email_rec_to_activity(p_email_rec)         Copy t_emai_rec data to activity rec           l_activity.*
# rec_copy_t_email_rec_to_mailbox(p_email_rec)          Copy t_emai_rec data to mailbox rec            l_mailbox.*
# rec_copy_t_email_rec_to_t_email_form_rec(p_email_rec) Copy t_email_rec data to t_email_form_rec      l_email_form_rec.*
# rec_copy_mailbox_to_t_email_rec(p_mailbox_rec)        Copy mailbox record data to t_email_rec        l_email_rec.*
# rec_copy_activity_rec_t_email_rec(p_activity_rec)     Copy activity record to t_emai_rec             l_email_rec.*
# rec_copy_mailbox_and_activity_rec_t_email_rec         Copy mailbox & activity record to t_email_rec  l_email_rec.*
#  (p_mailbox_rec,p_activity_rec)
#
# get_email_activity_id(p_mail_id)                      get email activity id from mail_id             r_activity_id LIKE activity.activity_id
# get_t_email_rec_from_mail_id(p_mail_id)               get a email record from the mail id            l_email_rec.*
#
# email_set_read(p_email_id)                            Set the read_flag to true                      NONE
# email_set_unread(p_email_id)                          Set the read_flag to false                     NONE
########################################################################################################################


#################################################
# GLOBALS
#################################################
GLOBALS "globals.4gl"

  DEFINE mail_temp_exists SMALLINT
  #######NB p_contact_id is who it is TO, p_operator_id2 is who it is FROM######




#################################################
# FUNCTION email_view(p_email_id,mail_direction)
#
# displays email details in window
#
# RETURN NONE
#################################################
FUNCTION email_view(p_email_id,mail_direction)
  DEFINE
    p_email_id       LIKE mailbox.mail_id,
    l_email_type     VARCHAR(3),
    l_email_rec      OF t_email_rec,
    l_email_form_rec OF t_email_form_rec,
    local_debug      SMALLINT,
    action           VARCHAR(10),
    inp_char         CHAR,
    mail_direction   SMALLINT  --0=incoming email  1=outgoing email

  LOCATE l_email_rec.long_desc IN MEMORY
  LOCATE l_email_form_rec.long_desc IN MEMORY

  LET local_debug = 0  --0=off 1=on


  #Set email read flag to TRUE
  IF mail_direction = 0 THEN  --0=incoming email  1=outgoing email
    CALL email_set_read(p_email_id)
  END IF

  #generate email record from activity and mailbox record
  CALL get_t_email_rec_from_mail_id(p_email_id) RETURNING l_email_rec.*


  #generate email-form record from email record
  CALL rec_copy_t_email_rec_to_t_email_form_rec(l_email_rec.*,l_email_form_rec.*) RETURNING l_email_form_rec.*

  IF local_debug = 1 THEN   --o=off 1=on
    DISPLAY "email_view(p_email_id=",p_email_id,")"
  END IF


    CALL fgl_window_open("w_email_view",2,1, get_form_path("f_email_det_l2"),FALSE) 
    CALL populate_email_form_view_labels_g()
    CALL fgl_settitle(get_str(437))  --"View E-Mail")
#    DISPLAY get_str(811) TO bt_cancel  --Done
#    DISPLAY "!" TO bt_cancel
#    DISPLAY "*" TO bt_ok



  IF local_debug = 1 THEN
    DISPLAY "email_view() p_email_id=" ,                  p_email_id
    DISPLAY "email_view() email_rec.from_email_address",  l_email_form_rec.from_email_address
    DISPLAY "email_view() email_rec.from_cont_id",        l_email_form_rec.from_cont_id
    DISPLAY "email_view() email_rec.from_cont_fname",     l_email_form_rec.from_cont_fname  
    DISPLAY "email_view() email_rec.from_cont_lname",     l_email_form_rec.from_cont_lname

    DISPLAY "email_view() email_rec.to_email_address",    l_email_form_rec.to_email_address 
    DISPLAY "email_view() email_rec.to_cont_id",          l_email_form_rec.to_cont_id
    DISPLAY "email_view() email_rec.to_cont_fname",       l_email_form_rec.to_cont_fname  
    DISPLAY "email_view() email_rec.to_cont_lname",       l_email_form_rec.to_cont_lname


    DISPLAY "email_view() email_rec.recv_date ",          l_email_form_rec.recv_date  
    DISPLAY "email_view() email_rec.read_flag",           l_email_form_rec.read_flag
    DISPLAY "email_view() email_rec.urgent_flag",         l_email_form_rec.urgent_flag
    DISPLAY "email_view() email_rec.short_desc",          l_email_form_rec.short_desc

  END IF

  DISPLAY BY NAME l_email_form_rec.*

  IF l_email_rec.to_email_address = get_current_operator_email_address() THEN
    LET l_email_type = "in"
  ELSE
    LET l_email_type = "out" 
  END IF

  IF l_email_type = "in" THEN  --Menu for received emails
    WHILE TRUE
      PROMPT "" FOR CHAR inp_char HELP 2
        BEFORE PROMPT
          CALL publish_toolbar("MailInboxView",0)

        ON KEY(ACCEPT,INTERRUPT)
          LET action = "nothing"
          LET int_flag = FALSE
          EXIT WHILE
        ON KEY(F5)
          LET action = "reply"
          EXIT WHILE
        ON KEY(F6)
          LET action = "forward"
          EXIT WHILE
        ON KEY(F7)
          LET action = "delete"
          EXIT WHILE
        ON KEY(F8)
          LET action = "read"
          EXIT WHILE
        ON KEY(F9)
          LET action = "unread"
          EXIT WHILE
        ON KEY(F10)
          LET action = "open"
          EXIT WHILE
        ON KEY(F11)
          LET action = "close"
          EXIT WHILE
      END PROMPT


    END WHILE
    CALL publish_toolbar("MailInboxView",1)
  ELSE  --Email in the Outbox / send email
     WHILE TRUE
      PROMPT "" FOR CHAR inp_char HELP 2
        BEFORE PROMPT
          CALL publish_toolbar("MailOutboxView",0)

        ON KEY(ACCEPT,INTERRUPT)
          LET action = "nothing"
          LET int_flag = FALSE
          EXIT WHILE
        ON KEY(F5)
          LET action = "reply"
          EXIT WHILE
        #ON KEY(F6)
        #  LET action = "forward"
        #  EXIT WHILE
        ON KEY(F7)
          LET action = "delete"
          EXIT WHILE
        #ON KEY(F8)
        #  LET action = "read"
        #  EXIT WHILE
        #ON KEY(F9)
        #  LET action = "unread"
        #  EXIT WHILE
        ON KEY(F10)
          LET action = "open"
          EXIT WHILE
        ON KEY(F11)
          LET action = "close"
          EXIT WHILE
      END PROMPT

    CALL publish_toolbar("MailOutboxView",1)

    END WHILE

  END IF


  CALL fgl_window_close("w_email_view")

  FREE l_email_rec.long_desc 
  FREE l_email_form_rec.long_desc

  CASE action
    WHEN "nothing"
      #Nothing to do
    WHEN "reply"
      CALL email_reply(p_email_id)
    WHEN "forward"
      CALL email_forward(p_email_id)
    WHEN "delete"
      CALL email_delete(p_email_id)
    WHEN "open"
      CALL email_open(p_email_id)
    WHEN "close"
      CALL email_close(p_email_id)
    WHEN "read"
      CALL email_set_read(p_email_id)
    WHEN "unread"
      CALL email_set_unread(p_email_id)

    OTHERWISE
      LET tmp_str1 = get_str(50), " in email_view()"
      LET tmp_str2 = "email_view() - ", get_str(32), " action =", action
      CALL fgl_winmessage(tmp_str1,tmp_str2,"error")
  END CASE

END FUNCTION



#################################################
# FUNCTION email_redirection_module (p_mail_id) 
#
# Email redirection module (reply,forward,redirect)
#
# RETURN NONE
#################################################
FUNCTION email_redirection_module(p_email_id,r_action) 
  DEFINE
    p_email_id                   LIKE mailbox.mail_id,
    r_action                     SMALLINT,  -- 0=Email Reply  1=Email Forward 2=Re-direct
    org_activity_id              LIKE activity.activity_id,
    org_email_rec                OF t_email_rec,
    new_email_rec                OF t_email_rec,
    l_email_form_rec             OF t_email_form_rec,
    local_debug                  SMALLINT

  LET local_debug = FALSE

  LOCATE org_email_rec.long_desc IN MEMORY
  LOCATE new_email_rec.long_desc IN MEMORY
  LOCATE l_email_form_rec.long_desc      IN MEMORY

  #generate email record from activity and mailbox record
  CALL get_t_email_rec_from_mail_id(p_email_id) RETURNING org_email_rec.*

  #copy orignal to new rec
  LET new_email_rec.* = org_email_rec.*

  #Some modifcations to new mail record like RE: and ID
  LET new_email_rec.mail_id = 0
  LET new_email_rec.activity_id = 0
  LET new_email_rec.recv_date = TODAY 
  LET new_email_rec.read_flag = 0
  LET new_email_rec.operator_id = get_current_operator_id()

  IF local_debug THEN
    DISPLAY "email_redirection_module() - r_action=", r_action
  END IF

  CASE r_action

    WHEN 0  -- Email Reply
      LET new_email_rec.from_email_address = get_current_operator_email_address()
      LET new_email_rec.from_cont_id =  get_current_operator_contact_id()

      LET new_email_rec.to_email_address = org_email_rec.from_email_address
      LET new_email_rec.to_cont_id = org_email_rec.from_cont_id

      LET new_email_rec.short_desc = "RE:", org_email_rec.short_desc 

    WHEN 1  -- Email Forward
      LET new_email_rec.from_email_address = get_current_operator_email_address()
      LET new_email_rec.from_cont_id =  get_current_operator_contact_id()

      LET new_email_rec.to_cont_id = advanced_contact_lookup(NULL)
      LET new_email_rec.to_email_address = get_contact_email_address(new_email_rec.to_cont_id)

      LET new_email_rec.short_desc = "FW:", org_email_rec.short_desc 
    WHEN 2  -- Re-direct

      LET new_email_rec.to_cont_id = advanced_contact_lookup(NULL)
      LET new_email_rec.to_email_address = get_contact_email_address(new_email_rec.from_cont_id)

    OTHERWISE
      LET tmp_str1 = get_str(50), " in email_redirection_module()"
      LET tmp_str2 = get_str(32), " r_action= ",  r_action
      CALL fgl_winmessage(tmp_str1,tmp_str2, "error")
  END CASE

  #Final changes for first and last name
  LET new_email_rec.from_cont_fname = get_contact_fname(new_email_rec.from_cont_id)  
  LET new_email_rec.from_cont_lname = get_contact_lname(new_email_rec.from_cont_id) 

  LET new_email_rec.to_cont_fname = get_contact_fname(new_email_rec.to_cont_id)  
  LET new_email_rec.to_cont_lname = get_contact_lname(new_email_rec.to_cont_id) 

  LET new_email_rec.operator_id =  get_current_operator_contact_id()

  IF local_debug THEN
    DISPLAY "email_redirection_module() - new_email_rec.mail_id = ",new_email_rec.mail_id
    DISPLAY "email_redirection_module() - new_email_rec.activity_id = ", new_email_rec.activity_id 
    DISPLAY "email_redirection_module() - new_email_rec.operator_id = ", new_email_rec.operator_id

    DISPLAY "email_redirection_module() - new_email_rec.from_email_address = ", new_email_rec.from_email_address 
    DISPLAY "email_redirection_module() - new_email_rec.from_cont_id = ", new_email_rec.from_cont_id 

    DISPLAY "email_redirection_module() - new_email_rec.to_email_address = ", new_email_rec.to_email_address 
    DISPLAY "email_redirection_module() - new_email_rec.to_cont_id = ", new_email_rec.to_cont_id 

    DISPLAY "email_redirection_module() - new_email_rec.short_desc = ", new_email_rec.short_desc 

    DISPLAY "email_redirection_module() - new_email_rec.recv_date = ", new_email_rec.recv_date 
    DISPLAY "email_redirection_module() - new_email_rec.read_flag = ", new_email_rec.read_flag 

  END IF


    IF NOT fgl_window_open("w_edit_email",2,1, get_form_path("f_email_det_l2"),FALSE)  THEN
      CALL fgl_winmessage("Error","Could not open window w_edit_email","error")
    END IF

    CASE r_action
      WHEN 0  -- Email Reply
        CALL populate_email_form_reply_labels_g()

      WHEN 1  -- Email Forward
        CALL populate_email_form_forward_labels_g()

      WHEN 2  -- Re-direct
        CALL populate_email_form_redirect_labels_g()

    END CASE


  LET int_flag = FALSE

  #convert to form record and display
  CALL rec_copy_t_email_rec_to_t_email_form_rec(new_email_rec.*,l_email_form_rec.*) RETURNING l_email_form_rec.*
  DISPLAY BY NAME l_email_form_rec.*

  INPUT BY NAME new_email_rec.short_desc, new_email_rec.long_desc, new_email_rec.urgent_flag WITHOUT DEFAULTS HELP 411
    BEFORE INPUT
      CALL publish_toolbar("EMailReply",0)
      CALL displayKeyGuide("Escape-Send E-Mail   Ctrl-C Cancel")

    AFTER INPUT 
      IF int_flag THEN  --operator choose cancel
        #IF NOT yes_no("Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?") THEN
        IF NOT yes_no(get_str(421),get_str(422)) THEN
          LET int_flag = FALSE         
          CONTINUE INPUT
        ELSE
          LET int_flag = FALSE 
          CALL email_set_read(p_email_id)
          CALL email_close(p_email_id)
          CALL publish_toolbar("EMailReply",1)	  
        END IF
      ELSE
        LET int_flag = FALSE
        CALL email_create(new_email_rec.*)
        CALL email_set_read(p_email_id)
        CALL email_close(p_email_id)
        CALL publish_toolbar("EMailReply",1)      
    END IF
  END INPUT

  CALL fgl_window_close("w_edit_email")
  FREE org_email_rec.long_desc 
  FREE new_email_rec.long_desc 
  FREE l_email_form_rec.long_desc
END FUNCTION







#################################################
# FUNCTION email_reply (p_mail_id) 
#
# Reply to an email
#
# RETURN NONE
#################################################
FUNCTION email_reply(p_email_id) 
  DEFINE
    p_email_id LIKE mailbox.mail_id

  CALL email_redirection_module(p_email_id,0)

END FUNCTION


#################################################
# FUNCTION email_forward (p_mail_id) 
#
# Reply to an email
#
# RETURN NONE
#################################################
FUNCTION email_forward(p_email_id) 
  DEFINE
    p_email_id LIKE mailbox.mail_id

  CALL email_redirection_module(p_email_id,1)


END FUNCTION


#################################################
# FUNCTION email_redirect (p_mail_id) 
#
# Reply to an email
#
# RETURN NONE
#################################################
FUNCTION email_redirect(p_email_id) 
  DEFINE
    p_email_id LIKE mailbox.mail_id

  CALL email_redirection_module(p_email_id,2)


END FUNCTION

#################################################
# FUNCTION write_email (p_contact_id, l_subject, l_body, actid, p_operator_id2) 
#
# RETURN NONE
#################################################
FUNCTION email_write_by_id(p_from_cont_id, p_to_cont_id) 
  DEFINE 
    p_from_cont_id LIKE contact.cont_id,
    p_to_cont_id LIKE contact.cont_id,
    l_email_rec   OF t_email_rec,
    l_email_form_rec OF t_email_form_rec,
    local_debug SMALLINT

  LET local_debug = 0   --0=off 1=on
  
  LOCATE l_email_rec.long_desc  IN MEMORY
  LOCATE l_email_form_rec.long_desc IN MEMORY

  LET l_email_rec.mail_id = 0
  LET l_email_rec.operator_id = get_current_operator_id()

  LET l_email_rec.from_cont_id = p_from_cont_id
  LET l_email_rec.from_email_address = get_contact_email_address(p_from_cont_id)
  LET l_email_rec.from_cont_fname = get_contact_fname(p_from_cont_id)
  LET l_email_rec.from_cont_lname = get_contact_lname(p_from_cont_id)

  LET l_email_rec.to_cont_id = p_to_cont_id
  LET l_email_rec.to_email_address = get_contact_email_address(p_to_cont_id)
  LET l_email_rec.to_cont_fname = get_contact_fname(p_to_cont_id)
  LET l_email_rec.to_cont_lname = get_contact_lname(p_to_cont_id)

  LET l_email_rec.read_flag = 0
  LET l_email_rec.urgent_flag = 0
  LET l_email_rec.recv_date = TODAY


  #Check if contact record exists
  IF NOT contact_id_count(p_to_cont_id) THEN
    LET tmp_str = get_str(424), "\n",get_str(425)
    CALL fgl_winmessage(get_str(423),tmp_str,"error")
    FREE l_email_rec.long_desc
    FREE l_email_form_rec.long_desc

    RETURN
  END IF 

  IF l_email_rec.to_email_address IS NULL THEN
    #Your contact record has no valid email address!
    LET tmp_str = get_str(426), "\n", get_str(427), "\n", get_str(428)
    CALL fgl_winmessage(get_str(423),tmp_str,"error")
    FREE l_email_rec.long_desc
    FREE l_email_form_rec.long_desc
    RETURN
  END IF


    IF NOT fgl_window_open("w_edit_email",2,1, get_form_path("f_email_det_l2"),FALSE) THEN
      CALL fgl_winmessage("Error","email_write_by_id()\nCould not open window w_edit_email","error")
    END IF

    CALL populate_email_form_labels_g()
    #DISPLAY get_str(825) TO bt_ok
    CALL fgl_settitle(get_str(439)) --"Email Send")



  IF local_debug = 1 THEN
    DISPLAY "email_write_by_id() p_to_cont_id=",p_to_cont_id
    DISPLAY "email_write_by_id() l_email_rec.from_cont_id=",l_email_rec.from_cont_id
    DISPLAY "email_write_by_id() l_email_rec.from_email_address=",l_email_rec.from_email_address
    DISPLAY "email_write_by_id() l_email_rec.to_cont_id =",l_email_rec.to_cont_id
    DISPLAY "email_write_by_id() l_email_rec.to_email_address =",l_email_rec.to_email_address

  END IF

  #CALL email_view_data(l_mail_rec.*)
  CALL rec_copy_t_email_rec_to_t_email_form_rec(l_email_rec.*, l_email_form_rec.*) RETURNING l_email_form_rec.*
  DISPLAY BY NAME l_email_form_rec.*


  LET int_flag = FALSE
       ---get email details

  INPUT BY NAME l_email_rec.short_desc, l_email_rec.long_desc,l_email_rec.urgent_flag 
    WITHOUT DEFAULTS --FROM rec, urgent_flag, subject, body  
    HELP 411
    BEFORE INPUT
      CALL publish_toolbar("EMailSend",0)
      CALL displayKeyGuide("Escape - Send E-Mail   CtrlC-Cancel")

    AFTER INPUT 
      IF int_flag THEN
        #do you really want to abort the new email..
        IF NOT yes_no(get_str(421),get_str(422)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          LET int_flag = FALSE
        END IF 
      ELSE
      CALL email_create(l_email_rec.*)
      END IF
  END INPUT

  CALL publish_toolbar("EMailSend",1)

  CALL fgl_window_close("w_edit_email")

    FREE l_email_rec.long_desc
    FREE l_email_form_rec.long_desc

END FUNCTION





#################################################
# FUNCTION email_create(p_email_rec)
#
# Create an mailbox & activity rec
#
# RETURN mailbox_id
#################################################
FUNCTION email_create(p_email_rec)
  DEFINE
    p_email_rec  OF t_email_rec,
    l_activity RECORD LIKE activity.*,
    l_mailbox RECORD LIKE mailbox.*

  LOCATE l_activity.long_desc IN MEMORY

  CALL rec_copy_t_email_rec_to_activity(p_email_rec.*,l_activity.*) RETURNING l_activity.*
  #LET l_activity.activity_id = 0
  #LET l_activity.act_type = 1  --E-Mail
  INSERT INTO activity VALUES (l_activity.*)

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN NULL
    END IF

  #get the record id from the new activity record
  LET p_email_rec.activity_id = sqlca.sqlerrd[2]

  CALL rec_copy_t_email_rec_to_mailbox(p_email_rec.*, l_mailbox.*) RETURNING l_mailbox.*
  INSERT INTO mailbox VALUES (l_mailbox.*)

  FREE l_activity.long_desc

  RETURN sqlca.sqlerrd[2]

END FUNCTION



{
#################################################
# FUNCTION write_email (p_contact_id, l_subject, l_body, actid, p_operator_id2) 
#
# RETURN NONE
#################################################
FUNCTION email_write(p_from_cont_id, p_to_cont_id,p_l_subject, p_body, actid, p_operator_id2) 
  DEFINE 
    p_contact_id LIKE contact.cont_id,
    l_contact 	RECORD LIKE contact.*,
    l_activity 	RECORD LIKE activity.*,
    l_subject 	VARCHAR(50),
    p_body 	TEXT,
    actid 	LIKE activity.activity_id,
    p_operator_id2 	LIKE operator.operator_id,
    l_act 	LIKE activity.short_desc,
    local_debug SMALLINT,
    from_email_address LIKE mailbox.from_email_address,
    urgent_flag LIKE mailbox.urgent_flag
  

  LET local_debug = 0   --0=off 1=on
  
  LOCATE l_body 	IN MEMORY
  LOCATE l_activity.long_desc IN MEMORY
  CALL web_get_contact_rec(p_contact_id) RETURNING l_contact.*

  #Check if contact record exists
  IF NOT contact_id_count(l_contact.cont_id) THEN
    CALL fgl_winmessage("Error - Can not send an E-Mail","Your contact record is not saved!\nSave your contact record including a valid E-Mail address before sending an E-Mail.","error")
    FREE l_body
    FREE l_activity.long_desc
    RETURN
  END IF 

  IF l_contact.cont_email IS NULL THEN
    CALL fgl_winmessage("Error - Can not send an E-Mail","Your contact record has no valid email address!\nModify your email address in your contact record and retry.\nIt is also possible that you have not saved your record with a valid E-Mail address.","error")
    FREE l_body
    FREE l_activity.long_desc
    RETURN
  END IF

  IF local_debug = 1 THEN
    DISPLAY "email_write() p_contact_id=",p_contact_id
    DISPLAY "email_write() l_contact.contact_id=",l_contact.cont_id
  END IF

  IF fgl_fglgui() THEN  --gui
    OPEN WINDOW w_edit_email 
      AT 2,1 
      WITH FORM "form/f_email_send_g" 
      ATTRIBUTE(BORDER, FORM LINE 2)
    DISPLAY "!" TO bt_send
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle("Email Send")

  ELSE --text client
    OPEN WINDOW w_edit_email 
      AT 2,1 
      WITH FORM "form/f_email_send_t"
  END IF

  CALL fgl_settitle("CMS  Send E-Mail")

  #DISPLAY "Click close (top-right corner X) to stop" AT 2,1
  LET int_flag = FALSE
       ---get email details
  INPUT l_contact.cont_email, urgent_flag, l_subject, l_body 
    WITHOUT DEFAULTS FROM rec, urgent_flag, subject, body  HELP 411
    BEFORE INPUT
      CALL publish_toolbar("EMailSend",0)
      CALL displayKeyGuide("Ctrl-S Send E-Mail   Ctrl-C Cancel")

    ON KEY (F411,CONTROL-S)
      LET l_activity.activity_id = 0     --lets auto increment
      LET l_activity.operator_id =  g_operator.operator_id  --p_contact_id 
      LET l_activity.long_desc = l_body
      LET l_activity.short_desc = l_subject
      LET l_activity.contact_id = p_contact_id   --p_operator_id2
      LET l_activity.act_type = get_activity_type_id("email")
      LET l_activity.open_date = TODAY
      LET l_activity.close_date = NULL
      LET l_activity.comp_id = get_company_id_from_contact_id(p_contact_id)  --new

  IF local_debug = 1 THEN
    DISPLAY "###"
    DISPLAY "email_write() l_activity.activity_id=",l_activity.activity_id
    DISPLAY "email_write() l_activity.operator_id=",l_activity.operator_id
    DISPLAY "email_write() l_activity.long_desc=",l_activity.long_desc
    DISPLAY "email_write() l_activity.short_desc=",l_activity.short_desc
    DISPLAY "email_write() l_activity.contact_id=",l_activity.contact_id
    DISPLAY "email_write() l_activity.act_type=",l_activity.act_type
    DISPLAY "email_write() l_activity.open_date=",l_activity.open_date
    DISPLAY "email_write() l_activity.close_date=",l_activity.close_date
    DISPLAY "email_write() l_activity.comp_id=",l_activity.comp_id
    DISPLAY "get_company_id(p_contact_id)", get_company_id_from_contact_id(p_contact_id)
  END IF


      INSERT INTO activity VALUES (l_activity.*)

      SELECT activity.activity_id
          INTO actid
          FROM activity
          WHERE activity.short_desc = l_subject

  IF local_debug = 1 THEN
    DISPLAY "###"
    DISPLAY "email_write() l_contact.cont_email=",l_contact.cont_email
    DISPLAY "email_write() l_subject=",l_subject
    DISPLAY "email_write() l_body=",l_body
    DISPLAY "email_write() actid=",actid
    DISPLAY "email_write() p_operator_id2=",p_operator_id2
    DISPLAY "email_write() l_activity.contact_id=", l_activity.contact_id
  END IF

      IF email_send(l_contact.cont_email, l_subject, l_body, actid, p_operator_id2, l_activity.contact_id,urgent_flag) THEN
        EXIT INPUT
      END IF
    AFTER INPUT 
      IF int_flag THEN
        IF NOT yes_no("Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?") THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF 
      END IF
  END INPUT

  CALL publish_toolbar("EMailSend",1)

  CLOSE WINDOW w_edit_email


  FREE l_body
  FREE l_activity.long_desc
END FUNCTION

}

#################################################
# FUNCTION email_send((p_email, p_subject, p_body, actid, p_operator)
# from_email_address = operator email address
# p_mail = email address
# p_subject = email header/reference
# p_body = email body text
# actid = activity id  (not activity_type_id)
# p_operator = operator id of the sender (not receipient)
# p_contact_id = contact_id from target (receipient)
#################################################
FUNCTION email_send(p_to_email, p_subject, p_body, actid, p_operator_id,p_contact_id,p_urgent_flag)
   ---this puts an email into the mailbox with ref. to an activity
  DEFINE 
    p_to_email      LIKE contact.cont_email,           -- receipient (target) email address
    p_activity      LIKE activity.activity_id,         -- again acivity_id ????
    p_subject       VARCHAR(50),                       -- email subject
    p_body          TEXT,                              -- email body
    p_operator_id   LIKE operator.operator_id,         -- operator/operator id (sender)
    i               INTEGER,
    l_mailbox       RECORD LIKE mailbox.*,
    actid           LIKE activity.activity_id,         -- activity_id  1=email
    p_contact_id    LIKE contact.cont_id,              -- TO contact_id (receiptien)
    p_urgent_flag   LIKE mailbox.urgent_flag,          -- high priority email if 1
    local_debug     SMALLINT

  LET local_debug = 0   --o=off 1=on
  
--  LOCATE p_body     IN MEMORY
  LET l_mailbox.mail_id = 0
  LET l_mailbox.mail_status = 0
  LET l_mailbox.from_email_address = get_operator_email_address(p_operator_id)
  LET l_mailbox.to_email_address = p_to_email
  LET l_mailbox.from_cont_id = p_contact_id
  LET l_mailbox.activity_id = actid
  LET l_mailbox.operator_id = p_operator_id
  LET l_mailbox.read_flag = 0
  LET l_mailbox.urgent_flag = p_urgent_flag
  LET l_mailbox.recv_date = TODAY

  IF local_debug = 1 THEN
    DISPLAY "###"
    DISPLAY "email_send(() l_mailbox.mail_id=",l_mailbox.mail_id
    DISPLAY "email_send(() l_mailbox.mail_status=",l_mailbox.mail_status
    DISPLAY "email_send(() l_mailbox.from_email_address=",l_mailbox.from_email_address
    DISPLAY "email_send(() l_mailbox.to_email_address=",l_mailbox.to_email_address
    DISPLAY "email_send(() l_mailbox.from_cont_id=",l_mailbox.from_cont_id
    DISPLAY "email_send(() l_mailbox.activity_id=",l_mailbox.activity_id
    DISPLAY "email_send(() l_mailbox.operator_id=",l_mailbox.operator_id
    DISPLAY "email_send(() l_mailbox.read_flag=",l_mailbox.read_flag
    DISPLAY "email_send(() l_mailbox.urgent_flag=",l_mailbox.urgent_flag
    DISPLAY "email_send(() l_mailbox.recv_date=",l_mailbox.recv_date
  END IF

  INSERT INTO mailbox VALUES (l_mailbox.*)

  FREE p_body

  LET i = sqlca.sqlerrd[2]
    RETURN i
END FUNCTION

#################################################
# FUNCTION sent_email(p_operator_id)
#
# Send an email to a contact by operator_id
#
# RETURN NONE
#################################################

#
#
# This seems not to be implemented or someone hacked it.. what's the display array for ?
#
#

FUNCTION sent_email(p_operator_id)
  DEFINE 
    p_operator_id      LIKE operator.operator_id,
    i              INTEGER,
    curr_pa        SMALLINT,
    p_operator_recv_id LIKE contact.cont_name,
    p_operator2        LIKE contact.cont_id,
    p_operator_subject LIKE activity.short_desc,
    mail           LIKE activity.activity_id,
    l_sel_rec OF t_mailbox_rec3,
    l_mail_arr DYNAMIC ARRAY OF t_mailbox_rec4,
    l_mail_arr2 DYNAMIC ARRAY OF t_mailbox_rec5,
    g_data   OF t_act_rec7,
    g_data2  OF t_act_rec7

  LOCATE l_sel_rec.long_desc IN MEMORY  
  LOCATE g_data2.long_desc IN MEMORY
  LOCATE g_data.long_desc  IN MEMORY

  CALL fgl_winmessage("are we using this function","are we using this function - not implemented","error")

  LET i = 1
  DECLARE c_mail2 CURSOR FOR
    SELECT mailbox.mail_id, mailbox.recv_date, mailbox.read_flag, mailbox.urgent_flag,
           activity.short_desc, contact.cont_name, activity.long_desc
     FROM  mailbox, activity, contact, operator
     WHERE mailbox.activity_id = activity.activity_id
       AND mailbox.contact_id = p_operator_id
       AND operator.cont_id = p_operator_id
       AND mailbox.operator_id = contact.cont_id
       AND activity.act_type = 1   --emails are marked as 1
  ORDER BY mailbox.recv_date DESC

    CALL fgl_window_open("w_mailbox_detail",2,1, get_form_path("f_mailbox2_l2"),FALSE) 
    CALL populate_mailbox2_detail_form_labels_g()
    CALL fgl_settitle("CMS Demo - E-Mail Outbox")


  FOREACH c_mail2 INTO l_sel_rec.*
    LET l_mail_arr[i].short_desc = l_sel_rec.short_desc
    LET l_mail_arr[i].cont_name = l_sel_rec.cont_name
    LET l_mail_arr[i].recv_date = l_sel_rec.recv_date
    LET l_mail_arr[i].mail_status = NULL 
    LET l_mail_arr2[i].long_desc = l_sel_rec.long_desc
    LET l_mail_arr2[i].mail_id = l_sel_rec.mail_id
    IF l_sel_rec.read_flag THEN
      LET l_mail_arr[i].read_flag = "R"
    ELSE
      LET l_mail_arr[i].read_flag = "U"
    END IF
    LET i = i + 1
  END FOREACH
  LET i = i - 1
  

	IF i > 0 THEN
		CALL l_mail_arr.resize(i)  --correct the last element of the dynamic array
	END IF   

  CALL set_count(i)

  DISPLAY ARRAY l_mail_arr TO sa_mailbox.*  HELP 310
    BEFORE DISPLAY
      CALL set_help_id(310)
      CALL publish_toolbar("MailOutbox",0)

    ON KEY (F7)  --View Email  ---get more details on email
      LET curr_pa = ARR_CURR()
      LET mail = l_mail_arr2[curr_pa].mail_id  --retrieve current pos's id from list
      DECLARE cur3 CURSOR FOR
        SELECT activity.long_desc
          FROM activity, operator, mailbox
          WHERE mailbox.mail_id = mail
            AND operator.operator_id = p_operator_id
            AND mailbox.activity_id = activity.activity_id

        FOREACH cur3 INTO g_data.*
          LET g_data2.long_desc = g_data.long_desc
        END FOREACH

        DISPLAY g_data2.* TO sa_maildesc.*
  END DISPLAY 

  CALL fgl_window_close("w_mailbox_detail ")

  FREE g_data.long_desc
  FREE g_data2.long_desc

END FUNCTION


#################################################
# FUNCTION email_delete (p_mail_id) 
#
# Reply to an email
#
# RETURN NONE
#################################################
FUNCTION email_delete(p_mail_id) 
  DEFINE
    p_mail_id      LIKE mailbox.mail_id,
    rv             SMALLINT,
    l_activity_id  LIKE activity.activity_id,
    local_debug    BOOLEAN

  LET local_debug = FALSE

  LET l_activity_id =  get_email_activity_id(p_mail_id)
  ---only delete operator once confirmed, to reduce risk of accidental delete
  #Are you sure you want to delete this email?
  LET tmp_str = get_str(430),"\n","\nE-Mail ID=" , p_mail_id , "activity_id = " , l_activity_id 
  IF yes_no(get_str(429),tmp_str) THEN
    DELETE FROM activity
      WHERE activity.activity_id = l_activity_id

    IF sqlca.sqlcode THEN
      #E-Mail related Activity record delete"
      CALL fgl_winmessage(get_str(431), get_str(432),"stop")
      RETURN FALSE
    ELSE
      DELETE FROM mailbox
        WHERE mailbox.mail_id = p_mail_id      
    END IF
  

    IF sqlca.sqlcode THEN
      CALL fgl_winmessage(get_str(433), get_str(434),"stop")
    ELSE
      CALL fgl_winmessage(get_str(433), get_str(435),"info")
    END IF

    LET rv = TRUE
  ELSE 
    CALL fgl_winmessage(get_str(433), get_str(43),"info")
    LET rv = FALSE
  END IF

  RETURN rv
END FUNCTION



