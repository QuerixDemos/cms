##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_contact_dept_scroll()
#
# Populate grid header
#
# RETURN NONE
####################################################
FUNCTION grid_header_contact_dept_scroll()
  CALL fgl_grid_header("sa_cont_dept","dept_id",get_str(1591),"right","F13")     --"Dep. ID:"
  CALL fgl_grid_header("sa_cont_dept","dept_name",get_str(1592),"left","F14")    --"Department:"
  CALL fgl_grid_header("sa_cont_dept","user_def",get_str(1593),"left","F15")     --"User Def.:"
END FUNCTION


#######################################################
# FUNCTION populate_department_form_labels_t()
#
# Populate department form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_department_form_labels_t()
  DISPLAY get_str(1580) TO lbTitle
  DISPLAY get_str(1581) TO dl_f1
  DISPLAY get_str(1582) TO dl_f2
  DISPLAY get_str(1583) TO dl_f3
  #DISPLAY get_str(1584) TO dl_f4
  #DISPLAY get_str(1585) TO dl_f5
  #DISPLAY get_str(1586) TO dl_f6
  #DISPLAY get_str(1587) TO dl_f7
  #DISPLAY get_str(1588) TO dl_f8
  DISPLAY get_str(1589) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_department_form_labels_g()
#
# Populate department form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_department_form_labels_g()
  DISPLAY get_str(1580) TO lbTitle
  DISPLAY get_str(1581) TO dl_f1
  DISPLAY get_str(1582) TO dl_f2
  DISPLAY get_str(1583) TO dl_f3
  #DISPLAY get_str(1584) TO dl_f4
  #DISPLAY get_str(1585) TO dl_f5
  #DISPLAY get_str(1586) TO dl_f6
  #DISPLAY get_str(1587) TO dl_f7
  #DISPLAY get_str(1588) TO dl_f8
  DISPLAY get_str(1589) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(1580))

END FUNCTION


#######################################################
# FUNCTION populate_department_list_form_labels_t()
#
# Populate department list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_department_list_form_labels_t()
  DISPLAY get_str(1590) TO lbTitle
  DISPLAY get_str(1591) TO dl_f1
  DISPLAY get_str(1592) TO dl_f2
  DISPLAY get_str(1593) TO dl_f3
  #DISPLAY get_str(1594) TO dl_f4
  #DISPLAY get_str(1595) TO dl_f5
  #DISPLAY get_str(1596) TO dl_f6
  #DISPLAY get_str(1597) TO dl_f7
  #DISPLAY get_str(1598) TO dl_f8
  DISPLAY get_str(1599) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_department_list_form_labels_g()
#
# Populate department list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_department_list_form_labels_g()
  DISPLAY get_str(1590) TO lbTitle
  #DISPLAY get_str(1591) TO dl_f1
  #DISPLAY get_str(1592) TO dl_f2
  #DISPLAY get_str(1593) TO dl_f3
  #DISPLAY get_str(1594) TO dl_f4
  #DISPLAY get_str(1595) TO dl_f5
  #DISPLAY get_str(1596) TO dl_f6
  #DISPLAY get_str(1597) TO dl_f7
  #DISPLAY get_str(1598) TO dl_f8
  DISPLAY get_str(1599) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1590))
  CALL grid_header_contact_dept_scroll()

END FUNCTION

