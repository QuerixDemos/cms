##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage Invoices
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTON:                                       RETURN:
# get_title_rec(p_title)                      get title record                                  r_title.* / NULL
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


#############################################
# FUNCTION get_title_rec(p_title)
#
# get title record
#
# RETURN NONE
#############################################
FUNCTION get_title_rec(p_title)
  DEFINE p_title LIKE title.title
  DEFINE r_title RECORD LIKE title.*

  SELECT title.* 
    INTO r_title
    FROM title
    WHERE title.title = p_title

  IF sqlca.sqlcode = 100 THEN
    RETURN NULL
  ELSE
    RETURN r_title.*
  END IF

END FUNCTION
