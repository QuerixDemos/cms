##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage stock item
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                        RETURN:
# get_stock_item(p_stock_id)                  Get stock item record from id                       l_stock_item.*
# get_stock_item_tax(p_stock_id)              get tax rate of a stock item                        l_tax_rate
# get_stock_item_desc(p_stock_id)             get stock item name/description of a stock item     l_item_desc
# check_stock_id(p_stock_id)                  Check if stock_id exists                            i  (1 yes  0 not)
# stock_item_popup_data_source(p_order_field) Data Source Cursor for the stock_item_popup array   NONE
# stock_item_popup(p_stock_id,p_order_field)  Display stock item list and return selected it      l_stock_arr[i].stock_id (or p_stock_id for cancel)
# stock_item_create()                         Create new stock item record                        l_stock_item.stock_id
# stock_item_input(p_stock_item)              Form input for new/edit stock item                  p_stock_item.*
# stock_item_edit(p_stock_id)                 Edit stock item record                              NONE
# stock_item_delete(p_stock_id)               Delete stock item                                   NONE
# stock_item_to_form_stock(p_stock_item)      ????                                                l_stock_rec.*
# stock_item_view(p_stock_id)                 View stock item record                              NONE
# grid_header_f_stock_scroll_g()              Poupulates the grid columns                         NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

###################################################
# FUNCTION get_stock_item(p_stock_id)
#
# Get stock item record from id
#
#  RETURN l_stock_item.*
###################################################
FUNCTION get_stock_item(p_stock_id)
  DEFINE 
    p_stock_id LIKE stock_item.stock_id,
    l_stock_item RECORD LIKE stock_item.*

  SELECT stock_item.*
    INTO l_stock_item.*
    FROM stock_item
    WHERE stock_item.stock_id = p_stock_id

  RETURN l_stock_item.*
END FUNCTION

###################################################
# FUNCTION get_stock_item_tax(p_stock_id)
#
# get tax rate of a stock item 
#
# RETURN l_tax_rate
###################################################
FUNCTION get_stock_item_tax(p_stock_id)
  DEFINE 
    p_stock_id LIKE stock_item.stock_id,
    l_tax_rate LIKE tax_rates.tax_rate

  SELECT tax_rates.tax_rate
    INTO l_tax_rate
    FROM tax_rates, stock_item
    WHERE stock_item.rate_id = tax_rates.rate_id
      AND stock_item.stock_id = p_stock_id

  RETURN l_tax_rate
END FUNCTION


###################################################
# FUNCTION get_stock_item_desc(p_stock_id)
#
# get stock item name/description of a stock item 
#
# RETURN l_item_desc
###################################################
FUNCTION get_stock_item_desc(p_stock_id)
  DEFINE 
    p_stock_id LIKE stock_item.stock_id,
    l_item_desc LIKE stock_item.item_desc

  SELECT stock_item.item_desc
    INTO l_item_desc
    FROM stock_item
    WHERE stock_item.stock_id = p_stock_id

  RETURN l_item_desc
END FUNCTION


###################################################
# FUNCTION get_stock_item_cost(p_stock_id)
#
# get stock item cost/price of a stock item 
#
# RETURN l_item_desc
###################################################
FUNCTION get_stock_item_cost(p_stock_id)
  DEFINE 
    p_stock_id LIKE stock_item.stock_id,
    l_item_cost LIKE stock_item.item_cost

  SELECT stock_item.item_cost
    INTO l_item_cost
    FROM stock_item
    WHERE stock_item.stock_id = p_stock_id

  RETURN l_item_cost
END FUNCTION

###################################################
# FUNCTION check_stock_id(p_stock_id)
#
# Check if stock_id exists 
#
# RETURN i  (1 yes  0 not)
###################################################
FUNCTION check_stock_id(p_stock_id)
  DEFINE 
    p_stock_id  LIKE stock_item.stock_id,
    p_stock_id2 LIKE stock_item.stock_id,
    i           INTEGER

  SELECT COUNT(*)
    INTO i
    FROM stock_item
    WHERE stock_item.stock_id = p_stock_id

  RETURN i
END FUNCTION

###################################################
# FUNCTION get_stock_item_info(p_stock_id)
#
# Get stock item aditional info
#
# RETURN NONE
###################################################
FUNCTION get_stock_item_info(p_stock_id)

  DEFINE p_stock_id   LIKE stock_item.stock_id,
         addit_info   RECORD
                        quantity   INTEGER,
                        reserved   INTEGER,
                        available  INTEGER,
                        required   INTEGER,
                        shot_term  INTEGER,
                        long_term  INTEGER
                      END RECORD

  SELECT quantity
    INTO addit_info.quantity
    FROM stock_item
   WHERE stock_id = p_stock_id

  SELECT SUM(il.quantity)
    INTO addit_info.reserved
    FROM Invoice i, Invoice_Line il
   WHERE il.stock_id = p_stock_id
     AND il.invoice_id = i.invoice_id 
     AND i.status in (1, 3, 4)
     AND TODAY < i.invoice_date + 3

  SELECT SUM(sl.quantity)
    INTO addit_info.shot_term
    FROM Supplies s, Supplies_Line sl
   WHERE sl.stock_id  = p_stock_id
     AND s.suppl_id   = sl.suppl_id 
     AND s.suppl_date < TODAY + 4  
     AND s.state      = 1

  SELECT SUM(sl.quantity)
    INTO addit_info.long_term
    FROM Supplies s, Supplies_Line sl
   WHERE sl.stock_id  = p_stock_id
     AND s.suppl_id   = sl.suppl_id 
     AND s.suppl_date > TODAY + 3  
     AND s.state      = 1

  IF addit_info.quantity  IS NULL THEN LET addit_info.quantity  = 0 END IF  
  IF addit_info.reserved  IS NULL THEN LET addit_info.reserved  = 0 END IF
  IF addit_info.shot_term IS NULL THEN LET addit_info.shot_term = 0 END IF
  IF addit_info.long_term IS NULL THEN LET addit_info.long_term = 0 END IF

  LET addit_info.available = addit_info.quantity - addit_info.reserved
  IF addit_info.available < 0 THEN LET addit_info.available = 0 END IF

  LET addit_info.required = addit_info.reserved - addit_info.quantity
  IF addit_info.required < 0 THEN LET addit_info.required = 0 END IF

  RETURN addit_info.* 

END FUNCTION  -- get_stock_item_info --

