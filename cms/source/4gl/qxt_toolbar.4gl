##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Graphical toolbar manager
#
# Created:
# 10.10.06 HH - V3 - This set of functions handles the import of toolbar configurations and
# allow to display and remove complete toolbars with a single function call
#
# It is written in a re-usable way. Feel free to integrate it into your 4gl application
# to have fully dynamic and customizable toolbars.
#
#
# FUNCTION                                        DESCRIPTION                                            RETURN
# process_toolbar_init(filename)                 Import toolbar config file -                           NONE
#                                                 each menu name has one or more menu items 
# publish_toolbar(action_time)                    Publish a group of toolbar menu icons                  NONE
# draw_tb_icon()                                  Draw a single menu item                                NONE
# hide_dialog_navigation_toolbar(choice)          Hide all auto-generated navigation buttons             NONE
# manage_toolbar_list()                           Display a list of all toolbar menu items -             NONE
#                                                 items can also be edited in detail
# copy_t_toolbar_to_t_toolbar_short(tb_rec)       copy t_toolbar data rec to t_toolbar_short             tb_short_rec.* 
# icon_detail_edit(id)                            Edit icon configuration (for a single menu item)       NONE
# icon_browser()                                  Displays the list of all icons (using the lazy way...) icon_list[i]
#
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


###########################################################
# FUNCTION get_toolbar_cfg_filename()
#
# Import/Process toolbar cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.toolbar_cfg_filename
###########################################################
FUNCTION get_toolbar_cfg_filename()
  RETURN qxt_settings.toolbar_cfg_filename
END FUNCTION


###########################################################
# FUNCTION set_toolbar_cfg_filename(p_filename)
#
# Export/Write toolbar cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.toolbar_cfg_filename
###########################################################
FUNCTION set_toolbar_cfg_filename(p_filename)
  dEFINE
    p_filename   VARCHAR(150)

  LET qxt_settings.toolbar_cfg_filename = p_filename
END FUNCTION



###########################################################
# FUNCTION process_toolbar_cfg_import(p_toolbar_cfg_filename)
#
# Import/Process cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_toolbar_cfg_import(p_toolbar_cfg_filename)
DEFINE 
  p_toolbar_cfg_filename  VARCHAR(200),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_import() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF

  IF p_toolbar_cfg_filename IS NULL THEN
    LET p_toolbar_cfg_filename = get_toolbar_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_import() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF

  #Add path
  LET p_toolbar_cfg_filename = get_cfg_path(p_toolbar_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_import() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF


  IF p_toolbar_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_toolbar_cfg_import()","Error in process_toolbar_cfg_import()\nNo import toolbar_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    EXIT PROGRAM
  END IF



############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_toolbar_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[Toolbar]
  #read from Toolbar section
  LET qxt_settings.toolbar_unl_filename   = configGet(ret,    "[Toolbar]", "toolbar_unl_filename")
  LET qxt_settings.tooltip_unl_filename   = configGet(ret,    "[Toolbar]", "tooltip_unl_filename")
  LET qxt_settings.toolbar_icon_size      = configGetInt(ret, "[Toolbar]", "toolbar_icon_size")



  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_toolbar_cfg_filename = ", p_toolbar_cfg_filename CLIPPED, "###############################"

    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [Toolbar] Section               x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"  
    DISPLAY "configInit() - qxt_settings.toolbar_unl_filename=",   qxt_settings.toolbar_unl_filename
    DISPLAY "configInit() - qxt_settings.tooltip_unl_filename=",   qxt_settings.tooltip_unl_filename
    DISPLAY "configInit() - qxt_settings.toolbar_icon_size=",      qxt_settings.toolbar_icon_size

   
  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_toolbar_cfg_export(p_toolbar_cfg_filename)
#
# Export toolbar cfg data
#
# RETURN ret
###########################################################
FUNCTION process_toolbar_cfg_export(p_toolbar_cfg_filename)
  DEFINE 
    p_toolbar_cfg_filename  VARCHAR(100),
    ret                     SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_export() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF

  IF p_toolbar_cfg_filename IS NULL THEN
    LET p_toolbar_cfg_filename = get_toolbar_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_export() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF

  #Add path
  LET p_toolbar_cfg_filename = get_cfg_path(p_toolbar_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_toolbar_cfg_export() - p_toolbar_cfg_filename=", p_toolbar_cfg_filename
  END IF


  IF p_toolbar_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_toolbar_cfg_export()","Error in process_toolbar_cfg_export()\nNo import toolbar_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_toolbar_cfg_filename)  --configInit(filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[Toolbar]
  #Write to Toolbar section
  CALL configWrite(ret, "[Toolbar]", "toolbar_icon_size", qxt_settings.toolbar_icon_size )
  CALL configWrite(ret, "[Toolbar]", "toolbar_unl_filename", qxt_settings.toolbar_unl_filename )
  CALL configWrite(ret, "[Toolbar]", "tooltip_unl_filename", qxt_settings.tooltip_unl_filename )


END FUNCTION



###########################################################
# FUNCTION get_toolbar_unl_filename()
#
# Get the toolbar unl file name
#
# RETURN qxt_settings.toolbar_unl_filename
###########################################################
FUNCTION get_toolbar_unl_filename()
  RETURN qxt_settings.toolbar_unl_filename
END FUNCTION

###########################################################
# FUNCTION set_icon_cfg_filename(p_fname
#
# Set the toolbar unl file name
#
# RETURN NONE
###########################################################
FUNCTION set_toolbar_unl_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.toolbar_unl_filename = p_fname
END FUNCTION


###########################################################
# FUNCTION get_tooltip_unl_filename()
#
# Get the tooltip unl file name
#
# RETURN qxt_settings.tooltip_unl_filename
###########################################################
FUNCTION get_tooltip_unl_filename()
  RETURN qxt_settings.tooltip_unl_filename
END FUNCTION

###########################################################
# FUNCTION set_icon_cfg_filename(p_fname
#
# Set the tooltip unl file name
#
# RETURN NONE
###########################################################
FUNCTION set_tooltip_unl_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.tooltip_unl_filename = p_fname
END FUNCTION



##################################################################################
# FUNCTION get_tb_icon_directory()
#
# Return toolbar icon directory
#
# RETURN qxt_settings.icon10_path
##################################################################################
FUNCTION get_tb_icon_directory()
  DEFINE 
    err_msg VARCHAR(200)
  
  CASE qxt_settings.toolbar_icon_size
    WHEN 16
      RETURN qxt_settings.icon16_path
    WHEN 24
      RETURN qxt_settings.icon24_path
    WHEN 32
      RETURN qxt_settings.icon32_path
    OTHERWISE
      LET err_msg = "The configuration file entry 'toolbar_icon_size'\nkeeps an invalid value\ntoolbar_icon_size = ", qxt_settings.toolbar_icon_size
      CALL fgl_winmessage("get_tb_icon_directory() - Invalid toolbar_icon_size",err_msg,"error")
  END CASE

END FUNCTION


##################################################################################
# FUNCTION get_tb_icon_path(p_icon)
#
# Return the icon file name argument with the current toolbar icon directory path
#
# RETURN qxt_settings.icon10_path
##################################################################################
FUNCTION get_tb_icon_path(p_icon_filename)
  DEFINE
    p_icon_filename    VARCHAR(100),
    ret                 VARCHAR(200),
    err_msg             VARCHAR(200)

  CASE qxt_settings.toolbar_icon_size
    WHEN 16
      LET ret = trim(qxt_settings.icon16_path), "/", trim(p_icon_filename)
    WHEN 24
      LET ret = trim(qxt_settings.icon24_path), "/", trim(p_icon_filename)
    WHEN 32
      LET ret = trim(qxt_settings.icon32_path), "/", trim(p_icon_filename)
    OTHERWISE
      LET err_msg = "The configuration file entry 'toolbar_icon_size'\nkeeps an invalid value\ntoolbar_icon_size = ", qxt_settings.toolbar_icon_size
      CALL fgl_winmessage("get_tb_icon_path() - Invalid toolbar_icon_size",err_msg,"error")
  END CASE

  RETURN trim(ret)

END FUNCTION



##################################################################
# FUNCTION hide_dialog_navigation_toolbar(choice)
#
# Hide all auto-generated navigation buttons
#
# RETURN NONE
##################################################################
FUNCTION hide_dialog_navigation_toolbar(choice)
  DEFINE 
    choice     SMALLINT,  --0=all  1= up/down  2= next/prev page  3= first/last
    err_msg    VARCHAR(200)


  CASE choice
    WHEN 0
      CALL fgl_dialog_setkeylabel("PREVIOUS","")  
      CALL fgl_dialog_setkeylabel("NEXT","") 
      CALL fgl_dialog_setkeylabel("DOWN","") 
      CALL fgl_dialog_setkeylabel("UP","") 
      CALL fgl_dialog_setkeylabel("FIRST","") 
      CALL fgl_dialog_setkeylabel("LAST","") 
    WHEN 1
      CALL fgl_dialog_setkeylabel("PREVIOUS","")  
      CALL fgl_dialog_setkeylabel("NEXT","") 
    WHEN 2
      CALL fgl_dialog_setkeylabel("DOWN","") 
      CALL fgl_dialog_setkeylabel("UP","") 
    WHEN 3
      CALL fgl_dialog_setkeylabel("FIRST","") 
      CALL fgl_dialog_setkeylabel("LAST","") 
    OTHERWISE
      LET err_msg = get_str_tool(802) CLIPPED, " \nhide_dialog_navigation_toolbar(choice)\nchoice=" , choice  
      CALL fgl_winmessage(get_str_tool(30),err_msg,"error")
  END CASE
END FUNCTION


##################################################################################
# FUNCTION draw_tb_icon()
#
# Draw a single menu item
#
# RETURN NONE
##################################################################################
FUNCTION draw_tb_icon(p_tbi_rec)
  DEFINE
    p_tbi_rec			OF t_qxt_toolbar_item_rec,
    lang_string		VARCHAR(100),
    id						SMALLINT,
    local_debug		BOOLEAN,
    err_msg				VARCHAR(200)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "draw_tb_icon() - p_tbi_rec.event_type_id=", p_tbi_rec.event_type_id
    DISPLAY "draw_tb_icon() - p_tbi_rec.tbi_event_name=", p_tbi_rec.tbi_event_name
    DISPLAY "draw_tb_icon() - p_tbi_rec.tbi_obj_action_id=", p_tbi_rec.tbi_obj_action_id
    DISPLAY "draw_tb_icon() - p_tbi_rec.tbi_scope_id=", p_tbi_rec.tbi_scope_id
    DISPLAY "draw_tb_icon() - p_tbi_rec.tbi_position=", p_tbi_rec.tbi_position
    DISPLAY "draw_tb_icon() - p_tbi_rec.tbi_static_id=", p_tbi_rec.tbi_static_id
    DISPLAY "draw_tb_icon() - p_tbi_rec.icon_filename=", p_tbi_rec.icon_filename
    DISPLAY "draw_tb_icon() - p_tbi_rec.label_data=", p_tbi_rec.label_data
    DISPLAY "draw_tb_icon() - p_tbi_rec.string_data=", p_tbi_rec.string_data
  END IF
 

      
  CASE p_tbi_rec.tbi_obj_action_id

    WHEN 1 --fgl_set_keylabel & fgl_dialog_setkeylabel

      CASE p_tbi_rec.event_type_id  --action or key events

        WHEN 1 -- KEY Event
          IF p_tbi_rec.tbi_scope_id = 0 THEN
            CALL fgl_setkeylabel(p_tbi_rec.tbi_event_name,p_tbi_rec.label_data ,get_tb_icon_path(p_tbi_rec.icon_filename)  ,p_tbi_rec.tbi_position,p_tbi_rec.tbi_static_id, p_tbi_rec.string_data)
            IF local_debug  THEN
              DISPLAY "fgl_setkeylabel(",trim(p_tbi_rec.tbi_event_name), ",", trim(p_tbi_rec.label_data),",",trim(get_tb_icon_path(p_tbi_rec.icon_filename)),",",trim(p_tbi_rec.tbi_position) , ",",trim(p_tbi_rec.tbi_static_id) ,",",trim(p_tbi_rec.string_data),")"
            END IF

          ELSE
            CALL fgl_dialog_setkeylabel(p_tbi_rec.tbi_event_name,p_tbi_rec.label_data ,get_tb_icon_path(p_tbi_rec.icon_filename)  ,p_tbi_rec.tbi_position,p_tbi_rec.tbi_static_id, p_tbi_rec.string_data)
            IF local_debug  THEN
              DISPLAY "fgl_dialog_setkeylabel(",trim(p_tbi_rec.tbi_event_name), ",", trim(p_tbi_rec.label_data),",",trim(get_tb_icon_path(p_tbi_rec.icon_filename)),",",trim(p_tbi_rec.tbi_position) , ",",trim(p_tbi_rec.tbi_static_id) ,",",trim(p_tbi_rec.string_data),")"
            END IF

          END IF


        WHEN 2 -- ACTION Event
          IF p_tbi_rec.tbi_scope_id = 0 THEN
            CALL fgl_setactionlabel(p_tbi_rec.tbi_event_name,p_tbi_rec.label_data ,get_tb_icon_path(p_tbi_rec.icon_filename)  ,p_tbi_rec.tbi_position,p_tbi_rec.tbi_static_id, p_tbi_rec.string_data)
            IF local_debug  THEN
              DISPLAY "fgl_setactionlabel(",trim(p_tbi_rec.tbi_event_name), ",", trim(p_tbi_rec.label_data),",",trim(get_tb_icon_path(p_tbi_rec.icon_filename)),",",trim(p_tbi_rec.tbi_position) , ",",trim(p_tbi_rec.tbi_static_id) ,",",trim(p_tbi_rec.string_data),")"
            END IF

          ELSE
            CALL fgl_dialog_setactionlabel(p_tbi_rec.tbi_event_name,p_tbi_rec.label_data ,get_tb_icon_path(p_tbi_rec.icon_filename)  ,p_tbi_rec.tbi_position,p_tbi_rec.tbi_static_id, p_tbi_rec.string_data)
            IF local_debug  THEN
              DISPLAY "fgl_dialog_setactionlabel(",trim(p_tbi_rec.tbi_event_name), ",", trim(p_tbi_rec.label_data),",",trim(get_tb_icon_path(p_tbi_rec.icon_filename)),",",trim(p_tbi_rec.tbi_position) , ",",trim(p_tbi_rec.tbi_static_id) ,",",trim(p_tbi_rec.string_data),")"
            END IF

          END IF

        OTHERWISE
          LET err_msg = "Error in draw_tb_icon()\nInavlid Event Type !\np_tbi_rec.event_type_id =",p_tbi_rec.event_type_id 
          CALL fgl_winmessage("Error in draw_tb_icon()",err_msg,"error")
      END CASE

    WHEN 2 --REMOVE KEY LABELS USING fgl_set_keylabel & fgl_dialog_setkeylabel
      IF p_tbi_rec.tbi_scope_id = 0 THEN

        CALL fgl_setkeylabel(p_tbi_rec.tbi_event_name,"")

        IF local_debug THEN
          DISPLAY "remove global toolbar icon - fgl_setkeylabel(", p_tbi_rec.tbi_event_name, ",\"\""
        END IF

      ELSE

        CALL fgl_dialog_setkeylabel(p_tbi_rec.tbi_event_name,"")

        IF local_debug THEN
          DISPLAY "remove dialog toolbar icon - fgl_dialog_setkeylabel(", p_tbi_rec.tbi_event_name, ",\"\""
        END IF

      END IF

    WHEN 3 -- fgl_keydivider and fgl_dialog_keydivider
      IF p_tbi_rec.tbi_scope_id = 0 THEN
        CALL fgl_keydivider(p_tbi_rec.tbi_position)
        IF local_debug THEN
          DISPLAY "fgl_keydivider(",p_tbi_rec.tbi_position , ")"
        END IF

      ELSE
        CALL fgl_dialog_keydivider(p_tbi_rec.tbi_position)
        IF local_debug THEN
          DISPLAY "fgl_dialog_keydivider(",p_tbi_rec.tbi_position , ")"
        END IF

      END IF


    WHEN 4 -- REMOVE fgl_keydivider and fgl_dialog_keydivider
      IF p_tbi_rec.tbi_scope_id = 0 THEN
        CALL fgl_clearkeydivider(p_tbi_rec.tbi_position)

        IF local_debug THEN
          DISPLAY "fgl_clearkeydivider(",p_tbi_rec.tbi_position , ")"
        END IF

      ELSE
        CALL fgl_dialog_clearkeydivider(p_tbi_rec.tbi_position)

        IF local_debug THEN
          DISPLAY "fgl_dialog_clearkeydivider(",p_tbi_rec.tbi_position , ")"
        END IF

      END IF


    OTHERWISE
      IF local_debug THEN
        LET err_msg = get_str_tool(32), " draw_tb_icon()\n Event->" , p_tbi_rec.tbi_event_name , ".action ->" , p_tbi_rec.tbi_obj_action_id
        CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
      END IF 

  END CASE

END FUNCTION



