##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# apps for the string_app table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_string_id(p_language_id)  get string_app id from p_language_id        l_string_id
# get_language_id(string_id)    Get language_id from p_string_id        l_language_id
# get_string_app_rec(p_string_id)   Get the string_app record from p_string_id  l_string_app.*
# string_app_popup_data_source()               Data Source (cursor) for string_app_popup              NONE
# string_app_popup                             string_app selection window                            p_string_id
# (p_string_id,p_order_field,p_accept_action)
# string_app_combo_list(cb_field_name)         Populates string_app combo list from db                NONE
# string_app_create()                          Create a new string_app record                         NULL
# string_app_edit(p_string_id)      Edit string_app record                                 NONE
# string_app_input(p_string_app_rec)    Input string_app details (edit/create)                 l_string_app.*
# string_app_delete(p_string_id)    Delete a string_app record                             NONE
# string_app_view(p_string_id)      View string_app record by ID in window-form            NONE
# string_app_view_by_rec(p_string_app_rec) View string_app record in window-form               NONE
# get_string_id_from_language_id(p_language_id)          Get the string_id from a file                      l_string_id
# string_app_language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# string_app_id_count(p_string_id)  tests if a record with this string_id already exists r_count
# copy_string_app_record_to_form_record        Copy normal string_app record data to type string_app_form_rec   l_string_app_form_rec.*
# (p_string_app_rec)
# copy_string_app_form_record_to_record        Copy type string_app_form_rec to normal string_app record data   l_string_app_rec.*
# (p_string_app_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_string_app_scroll()              Populate string_app grid headers                       NONE
# populate_string_app_form_labels_g()          Populate string_app form labels for gui                NONE
# populate_string_app_form_labels_t()          Populate string_app form labels for text               NONE
# populate_string_app_form_edit_labels_g()     Populate string_app form edit labels for gui           NONE
# populate_string_app_form_edit_labels_t()     Populate string_app form edit labels for text          NONE
# populate_string_app_form_view_labels_g()     Populate string_app form view labels for gui           NONE
# populate_string_app_form_view_labels_t()     Populate string_app form view labels for text          NONE
# populate_string_app_form_create_labels_g()   Populate string_app form create labels for gui         NONE
# populate_string_app_form_create_labels_t()   Populate string_app form create labels for text        NONE
# populate_string_app_list_form_labels_g()     Populate string_app list form labels for gui           NONE
# populate_string_app_list_form_labels_t()     Populate string_app list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_string_app_manager_globals.4gl"


########################################################################################################################
# Data Access Functions
########################################################################################################################


#########################################################
# FUNCTION get_string_app_string_data(p_string_id,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_string_app_string_data(p_string_id,p_language_id)
  DEFINE 
    p_string_id          LIKE qxt_string_app.string_id,
    p_language_id        LIKE qxt_string_app.language_id,
    l_string_data        LIKE qxt_string_app.string_data,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_string_app_string_data() - p_string_id = ", p_string_id
    DISPLAY "get_string_app_string_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_string_app.string_data
    INTO l_string_data
    FROM qxt_string_app
    WHERE qxt_string_app.string_id = p_string_id
      AND qxt_string_app.language_id = p_language_id

  RETURN l_string_data
END FUNCTION


#########################################################
# FUNCTION get_string_app_string_data(p_string_id,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_string_app_string_id(p_string_data,p_language_id)
  DEFINE 
    p_string_data        LIKE qxt_string_app.string_data,
    p_language_id        LIKE qxt_string_app.language_id,
    l_string_id          LIKE qxt_string_app.string_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_string_app_string_data() - p_string_data = ", p_string_data
    DISPLAY "get_string_app_string_data() - p_language_id = ", p_language_id
  END IF

  #Table entry 1 keeps an empty string
  IF p_string_data IS NULL THEN
    RETURN 1
  END IF

  SELECT qxt_string_app.string_id
    INTO l_string_id
    FROM qxt_string_app
    WHERE qxt_string_app.string_data = p_string_data
      AND qxt_string_app.language_id = p_language_id

  IF local_debug THEN
    DISPLAY "get_string_app_string_data() - l_string_id = ", l_string_id
  END IF

  RETURN l_string_id
END FUNCTION



#########################################################
# FUNCTION get_string_app_rec(p_string_id,p_language_id)
#
# get string record from p_language_id and p_string_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_string_app_rec(p_string_id,p_language_id)
  DEFINE 
    p_string_id          LIKE qxt_string_app.string_id,
    p_language_id        LIKE qxt_string_app.language_id,
    l_string_rec         RECORD LIKE qxt_string_app.*,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_string_app_string_data() - p_string_id = ", p_string_id
    DISPLAY "get_string_app_string_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_string_app.*
    INTO l_string_rec.*
    FROM qxt_string_app
    WHERE qxt_string_app.string_id = p_string_id
      AND qxt_string_app.language_id = p_language_id

  RETURN l_string_rec.*
END FUNCTION


###################################################################################
# FUNCTION get_string_app_new_string_id(p_language_id)
#
# Finds MAX string_app_id and Returns it +1  (SERIAL emulation) 
#
# RETURN NONE
###################################################################################
FUNCTION get_string_app_new_string_id(p_language_id)
  DEFINE
    p_language_id            LIKE qxt_language.language_id,
    l_max_string_app_id  LIKE qxt_string_app.string_id

  SELECT  MAX (qxt_string_app.string_id)
    INTO l_max_string_app_id
    FROM qxt_string_app
    WHERE qxt_string_app.language_id = p_language_id

  RETURN l_max_string_app_id + 1
END FUNCTION


########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION string_app_data_combo_list(cb_field_name)
#
# Populates string_app_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION string_app_data_combo_list(p_cb_field_name, p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_string_app.language_id,
    l_string_data             DYNAMIC ARRAY OF LIKE qxt_string_app.string_data,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   STRING,
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_data_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "string_app_data_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_string_app_scroll2 CURSOR FOR 
    SELECT qxt_string_app.string_data
      FROM qxt_string_app
      WHERE qxt_string_app.language_id = p_language_id
    ORDER BY string_data ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_string_app_scroll2 INTO l_string_data[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_string_data[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_string_data[row_count] CLIPPED, ")"
      DISPLAY "string_app_data_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION

###################################################################################
# FUNCTION string_app_string_id_combo_list(cb_field_name)
#
# Populates string_app_id_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION string_app_string_id_combo_list(p_cb_field_name, p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_string_app.language_id,
    l_string_id               DYNAMIC ARRAY OF LIKE qxt_string_app.string_id,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   STRING,
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "string_app_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_string_app_id_scroll2 CURSOR FOR 
    SELECT qxt_string_app.string_id
      FROM qxt_string_app
      WHERE qxt_string_app.language_id = p_language_id
    ORDER BY string_id ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_string_app_id_scroll2 INTO l_string_id[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_string_id[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_string_id[row_count] CLIPPED, ")"
      DISPLAY "string_app_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION



######################################################
# FUNCTION string_app_popup_data_source()
#
# Data Source (cursor) for string_app_popup
#
# RETURN NONE
######################################################
FUNCTION string_app_popup_data_source(p_f_lang_switch, p_f_lang_data,p_search_filter_switch,p_search_filter_data,p_order_field,p_ord_dir)
  DEFINE 
    p_f_lang_switch        SMALLINT,
    p_f_lang_data          LIKE qxt_string_app.language_id,
    p_search_filter_switch SMALLINT,
    p_search_filter_data   LIKE qxt_string_app.string_data,
    p_order_field          STRING,
    sql_stmt               STRING,
    p_ord_dir              SMALLINT,
    p_ord_dir_str          VARCHAR(4),
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_Debug THEN
    DISPLAY "string_app_popup_data_source() - p_f_lang_switch=",           p_f_lang_switch
    DISPLAY "string_app_popup_data_source() - p_f_lang_data=",             p_f_lang_data
    DISPLAY "string_app_popup_data_source() - p_search_filter_switch=",    p_search_filter_switch
    DISPLAY "string_app_popup_data_source() - p_search_filter_data=",      p_search_filter_data
    DISPLAY "string_app_popup_data_source() - p_order_field=",             p_order_field
    DISPLAY "string_app_popup_data_source() - p_ord_dir=",                 p_ord_dir
  END IF

  IF p_search_filter_data IS NULL THEN
    LET p_search_filter_switch = FALSE
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "string_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_string_app.string_id, ",
                 "qxt_language.language_name, ",
                 "qxt_string_app.string_data, ",
                 "str_cat1.category_data, ",
                 "str_cat2.category_data, ",
                 "str_cat3.category_data ",

                 "FROM ",
                 "qxt_string_app, ",
                 "qxt_language, ",
                 "qxt_str_category str_cat1, ",
                 "qxt_str_category str_cat2, ",
                 "qxt_str_category str_cat3 ",

                 "WHERE ",
                 "qxt_string_app.language_id = qxt_language.language_id ",

                 "AND ",
                 "qxt_string_app.category1_id = str_cat1.category_id ",
                 "AND ", 
                 "qxt_string_app.language_id = str_cat1.language_id ", 
                 "AND ",
                 "qxt_string_app.category2_id = str_cat2.category_id ",
                 "AND ",
                 "qxt_string_app.language_id = str_cat2.language_id ",
                 "AND ",
                 "qxt_string_app.category3_id = str_cat3.category_id ",
                 "AND ",
                 "qxt_string_app.language_id = str_cat3.language_id "


  IF p_f_lang_data IS NOT NULL THEN
    IF p_f_lang_switch THEN
      LET sql_stmt = trim(sql_stmt), " ",
                 " AND ",
                 " qxt_string_app.language_id = '", trim(p_f_lang_data), "' "
    END IF
  END IF


  IF p_search_filter_data IS NOT NULL THEN
    IF p_search_filter_switch THEN
      LET sql_stmt = trim(sql_stmt), " ",
                 "AND ",
                 "UPPER(qxt_string_app.string_data) LIKE '", trim((p_search_filter_data)), "' "
    END IF
  END IF


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED, "  "
  END IF


  IF local_debug THEN
    DISPLAY "string_app_popup_data_source()"
    DISPLAY sql_stmt[001,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
  END IF

  PREPARE p_string_app FROM sql_stmt
  DECLARE c_string_app CURSOR FOR p_string_app

END FUNCTION


######################################################
# FUNCTION string_app_popup(p_string_id,p_language_id, p_order_field,p_accept_action)
#
# string_app Main Management Function
#
# Arguments:
#   p_string_id     - String ID  (Prim. Key)
#   p_language_id   - Language ID (Prim. Key)
#   p_order_field   - The field name of the default order
#   p_accept_action - The action which should take place when the user pressed OK/Accept/Enter
#
# RETURN p_string_id
######################################################
FUNCTION string_app_popup(p_string_id,p_language_id, p_order_field,p_accept_action)
  DEFINE 
    p_string_id                  LIKE qxt_string_app.string_id,  --default return value if user cancels
    p_language_id                LIKE qxt_string_app.language_id,  --default return value if user cancels
    l_lang_filter_switch         SMALLINT,
    #l_string_app_arr             DYNAMIC ARRAY OF t_qxt_string_app_form_rec,    --RECORD LIKE qxt_string_app.*,  
    l_string_app_arr             DYNAMIC ARRAY OF t_qxt_string_app_form_rec,    --RECORD LIKE qxt_string_app.*,  
    l_arr_size                   SMALLINT,
    l_search_filter_data         LIKE qxt_string_app.string_data,
    l_search_filter_switch       SMALLINT,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      STRING,
    p_order_field,p_order_field2 STRING, 
    l_string_id                  LIKE qxt_string_app.string_id,
    l_language_id                LIKE qxt_string_app.language_id,
    l_language_name              LIKE qxt_language.language_name,
    l_t_lang_id_1, l_t_lang_id_2 LIKE qxt_language.language_id,
    l_t_lang_n_1, l_t_lang_n_2   LIKE qxt_language.language_name,
    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    current_string_id            LIKE  qxt_string_app.string_id,
    local_debug                  SMALLINT

  LET local_debug = FALSE
  #LET l_arr_size =sizeof(l_string_app_arr)
  LET l_arr_size = 5000  --we limit it to 5000 elements sizeof() does not make sense because we work with a dynamic array --sizeof(l_string_app_arr)
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1

  IF p_string_id IS NULL THEN
    LET l_string_id = 1
  ELSE
    LET l_string_id = p_string_id
  END IF

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  ELSE
    LET l_lang_filter_switch = TRUE
  END IF

  IF local_debug THEN
    DISPLAY "string_app_popup() - p_string_id=",p_string_id
    DISPLAY "string_app_popup() - p_order_field=",p_order_field
    DISPLAY "string_app_popup() - p_accept_action=",p_accept_action
  END IF


#  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_string_app_scroll", 2, 8, get_form_path("f_qxt_string_app_scroll_l2"),FALSE) 
    CALL populate_string_app_list_form_labels_g()
#  ELSE  --text
#    CALL fgl_window_open("w_string_app_scroll", 2, 8, get_form_path("f_qxt_string_app_scroll_t"),FALSE) 
#    CALL populate_string_app_list_form_labels_t()
#  END IF

  DISPLAY get_language_name(p_language_id) TO cb_f_lang
  LET l_t_lang_id_1 = 3
  LET l_t_lang_id_2 = 2
  LET l_t_lang_n_1 = get_language_name(l_t_lang_id_1)
  LET l_t_lang_n_2 = get_language_name(l_t_lang_id_2)
  DISPLAY l_t_lang_n_1 TO trans_lang_1
  DISPLAY l_t_lang_n_2 TO trans_lang_2


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_string_app

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL string_app_create(NULL,p_language_id,NULL,TRUE)
    END IF
  END IF


  #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
  LET current_row = 1
  LET current_string_id = 1

  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL string_app_popup_data_source(l_lang_filter_switch,p_language_id,l_search_filter_switch,l_search_filter_data,p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_string_app INTO l_string_app_arr[i].*
      IF local_debug THEN
        DISPLAY "string_app_popup() - i=",i
        DISPLAY "string_app_popup() - l_string_app_arr[i].string_id=",l_string_app_arr[i].string_id
        DISPLAY "string_app_popup() - l_string_app_arr[i].language_name=",l_string_app_arr[i].language_name
        DISPLAY "string_app_popup() - l_string_app_arr[i].string_data=",l_string_app_arr[i].string_data

      END IF

      IF l_string_app_arr[i].string_id = current_string_id THEN
        LET current_row = i
      END IF

      LET i = i + 1
			CALL l_string_app_arr.resize(i)
			
      #Display Array is limited to l_arr_size elements
      IF i > l_arr_size THEN
        LET err_msg = "The string list is limited to ", trim(l_arr_size), " Rows!\nOnly the first ", trim(l_arr_size), " rows will be displayed."
        CALL fgl_winmessage("Too many rows",err_msg, "info")
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_string_app_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		

    IF local_debug THEN
      DISPLAY "string_app_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_string_app_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "string_app_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_string_app_arr TO sc_string_app.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
       #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        CALL publish_toolbar("StringAppList",0)

        CALL fgl_dialog_setcurrline(5,current_row)

      BEFORE ROW
        LET i = arr_curr()
        LET current_string_id = l_string_app_arr[i].string_id
        LET l_string_id = current_string_id
        LET l_language_id = get_language_id(l_string_app_arr[i].language_name)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL string_app_view(l_string_id, l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL string_app_edit(l_string_id,l_language_id,NULL,TRUE)
              RETURNING current_string_id, l_language_id
            EXIT DISPLAY

          WHEN 3  --delete
            CALL string_app_delete(l_string_id,l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL string_app_create(NULL,l_language_id,NULL,TRUE)
              RETURNING current_string_id, l_language_id

            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","string_app_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)

          WHEN 6  --duplicate
            CALL string_app_duplicate(l_string_id,l_language_id)
            EXIT DISPLAY        

          OTHERWISE
            LET err_msg = "string_app_popup(p_string_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("string_app_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL string_app_create(NULL,l_language_id,NULL,TRUE)
          RETURNING current_string_id, l_language_id

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          EXIT DISPLAY
        END IF

      ON KEY (F5) -- edit
        CALL string_app_edit(l_string_id, l_language_id,NULL,TRUE)
          RETURNING current_string_id, l_language_id

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          EXIT DISPLAY
        END IF

      ON KEY (F6) -- delete
        CALL string_app_delete(l_string_id, l_language_id)

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          EXIT DISPLAY
        END IF

      ON KEY (F7) -- duplicate
        CALL string_app_duplicate(l_string_id, l_language_id)
          RETURNING current_string_id, l_language_id

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          EXIT DISPLAY
        END IF    

      #Grid Column Sort
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      


      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat1.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  


      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat2.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  


      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat3.category_data"

        IF p_order_field = p_order_field THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  

      #Goto Line Number
      ON ACTION ("gotoLine")
        LET current_row = fgl_winprompt(5,5,"Goto Line Number:",1,4,1)
        CALL fgl_winmessage("line",current_row,"info")
        CALL fgl_dialog_setcurrline(5,current_row)

      #Goto Line Number
      ON ACTION ("gotoId")
        LET current_string_id = fgl_winprompt(5,5,"Goto Line Number:",1,4,1)

        EXIT DISPLAY


      #Language Filter
      ON ACTION ("filterLanguageOn")
        LET l_lang_filter_switch = TRUE
        EXIT DISPLAY

      ON ACTION ("filterLanguageOff")
        LET l_lang_filter_switch = FALSE
        EXIT DISPLAY

      ON ACTION ("setFilterLanguage")
        INPUT l_language_name WITHOUT DEFAULTS FROM cb_f_lang HELP 1
          ON ACTION ("setFilterLanguage","filterLanguageOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF l_language_name IS NOT NULL THEN
          LET  p_language_id = get_language_id(l_language_name)
          LET l_lang_filter_switch = TRUE
        ELSE
          LET l_lang_filter_switch = FALSE
        END IF
        EXIT DISPLAY

      #Search String feature
      ON ACTION ("filterSearchOn")
        LET l_search_filter_switch = TRUE
        EXIT DISPLAY

      ON ACTION ("filterSearchOff")
        LET l_search_filter_switch = FALSE
        EXIT DISPLAY

      ON ACTION ("setFilterSearch","F24")
        INPUT l_search_filter_data WITHOUT DEFAULTS FROM search_filter_data HELP 1
          ON ACTION ("setFilterSearch","filterSearchOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF l_search_filter_data IS NOT NULL THEN
          LET l_search_filter_switch = TRUE
        ELSE
          LET l_search_filter_switch = FALSE
        END IF

        EXIT DISPLAY

     #Translation feature
      ON ACTION("setTranslation")
        INPUT l_t_lang_n_1, l_t_lang_n_2 WITHOUT DEFAULTS FROM trans_lang_1,trans_lang_2 HELP 1  
          ON ACTION("setTranslation")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        LET l_t_lang_id_1 = get_language_id(l_t_lang_n_1)
        LET l_t_lang_id_2 = get_language_id(l_t_lang_n_2)

      ON ACTION("translate")
        CALL string_app_translate(l_string_id, l_language_id, l_t_lang_id_1, l_t_lang_id_2)
        EXIT DISPLAY



    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_string_app_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_string_id, p_language_id 
  ELSE 
    RETURN l_string_id, p_language_id 
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION string_app_create(p_string_id,p_language_id,p_string_data,p_dialog)
#
# Create a new string_app record
#
# RETURN NULL
######################################################
FUNCTION string_app_create(p_string_id,p_language_id,p_string_data,p_dialog)
  DEFINE 
    p_string_id       LIKE qxt_string_app.string_id,
    p_string_data     LIKE qxt_string_app.string_data,
    p_language_id     LIKE qxt_string_app.language_id,
    p_dialog          SMALLINT,
    l_string_app_rec  RECORD LIKE qxt_string_app.*,
    local_debug       SMALLINT

  LET int_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_create() - p_string_id = ",   p_string_id
    DISPLAY "string_app_create() - p_language_id = ", p_language_id
    DISPLAY "string_app_create() - p_string_data = ", p_string_data
    DISPLAY "string_app_create() - p_dialog = ",      p_dialog
  END IF

  #Initialise some variables
  IF p_string_id IS NOT NULL THEN
    LET l_string_app_rec.string_id = p_string_id --default language is english
  ELSE
    LET l_string_app_rec.string_id = get_string_app_new_string_id(get_language())
  END IF

  IF p_string_data IS NOT NULL THEN
    LET l_string_app_rec.string_data = p_string_data --default language is english
  END IF

  IF p_language_id IS NOT NULL THEN
    LET l_string_app_rec.language_id = p_language_id
  ELSE
    LET l_string_app_rec.language_id = get_data_language_id() 
  END IF


  IF l_string_app_rec.category1_id IS NULL OR l_string_app_rec.category1_id = 0 THEN
    LET l_string_app_rec.category1_id = 1
  END IF

  IF l_string_app_rec.category2_id IS NULL OR l_string_app_rec.category2_id = 0 THEN
    LET l_string_app_rec.category1_id = 1
  END IF

  IF l_string_app_rec.category3_id IS NULL OR l_string_app_rec.category3_id = 0 THEN
    LET l_string_app_rec.category1_id = 1
  END IF

  #Only for p_dialog Section (graphical input
  IF p_dialog THEN

#    IF fgl_fglgui() THEN --gui
      CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_l2"), TRUE) 
      CALL populate_string_app_form_create_labels_g()

#    ELSE
#      CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_t"), TRUE) 
#      CALL populate_string_app_form_create_labels_t()
#    END IF

  
    # CALL the INPUT
    CALL string_app_input(l_string_app_rec.*,TRUE)
      RETURNING l_string_app_rec.*

    CALL fgl_window_close("w_string_app")

  END IF


  #Insert for with or without p_dialog

  IF l_string_app_rec.string_id   IS NOT NULL AND
     l_string_app_rec.language_id IS NOT NULL AND
     l_string_app_rec.string_data IS NOT NULL AND 
     NOT int_flag                  THEN

    INSERT INTO qxt_string_app VALUES (
      l_string_app_rec.string_id,
      l_string_app_rec.language_id,
      l_string_app_rec.string_data,
      l_string_app_rec.category1_id,
      l_string_app_rec.category2_id,
      l_string_app_rec.category3_id

                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(790),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(790),NULL)
      RETURN l_string_app_rec.string_id, l_string_app_rec.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]

  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL

  END IF

END FUNCTION


#################################################
# FUNCTION string_app_edit(p_string_id,p_language_id,p_string_data,p_dialog)
#
# Edit string_app record
#
# RETURN NONE
#################################################
FUNCTION string_app_edit(p_string_id,p_language_id,p_string_data,p_dialog)
  DEFINE 
    p_string_id            LIKE qxt_string_app.string_id,
    p_language_id          LIKE qxt_string_app.language_id,
    p_string_data          LIKE qxt_string_app.string_data,
    p_dialog               SMALLINT,
    l_key1_string_id       LIKE qxt_string_app.string_id,
    l_key2_language_id     LIKE qxt_string_app.language_id,
    l_string_app_rec   RECORD LIKE qxt_string_app.*,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_edit() - Function Entry Point"
    DISPLAY "string_app_edit() - p_string_id=", p_string_id
    DISPLAY "string_app_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_string_id = p_string_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_string_app_rec(p_string_id,p_language_id) 
    RETURNING l_string_app_rec.*

  IF p_string_data IS NOT NULL THEN
    LET l_string_app_rec.string_data = p_string_data
  END IF

  IF p_dialog THEN

#    IF fgl_fglgui() THEN --gui
      CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_l2"), TRUE) 
      CALL populate_string_app_form_edit_labels_g()
#    ELSE
#      CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_t"), TRUE) 
#      CALL populate_string_app_form_edit_labels_t()
#    END IF


    #Call the INPUT
    CALL string_app_input(l_string_app_rec.*,FALSE) 
      RETURNING l_string_app_rec.*


    CALL fgl_window_close("w_string_app")

    IF local_debug THEN
      DISPLAY "string_app_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF

  END IF


  #Check if user canceled and if all arguments are NOT NULL
  IF l_string_app_rec.string_id   IS NOT NULL AND
     l_string_app_rec.language_id IS NOT NULL AND
     l_string_app_rec.string_data IS NOT NULL AND 
     NOT int_flag                  THEN

    IF local_debug THEN
      DISPLAY "string_app_edit() - l_string_app_rec.string_id=",l_string_app_rec.string_id
      DISPLAY "string_app_edit() - l_string_app_rec.language_id=",l_string_app_rec.language_id
      DISPLAY "string_app_edit() - l_string_app_rec.string_data=",l_string_app_rec.string_data
    END IF

    UPDATE qxt_string_app
      SET 
          string_id =                l_string_app_rec.string_id,
          language_id =              l_string_app_rec.language_id,
          string_data =              l_string_app_rec.string_data,
          category1_id =             l_string_app_rec.category1_id,
          category2_id =             l_string_app_rec.category2_id,
          category3_id =             l_string_app_rec.category3_id
      WHERE qxt_string_app.string_id = l_key1_string_id
        AND qxt_string_app.language_id = l_key2_language_id


    IF local_debug THEN
      DISPLAY "string_app_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(780),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(780),NULL)
      RETURN l_string_app_rec.string_id , l_string_app_rec.language_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(780),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL
  END IF



END FUNCTION



#################################################
# FUNCTION string_app_duplicate(p_string_id,p_language_id)
#
# Crate a new record with clonded data from an existing record
#
# RETURN NONE
#################################################
FUNCTION string_app_duplicate(p_string_id,p_language_id)
  DEFINE 
    p_string_id             LIKE qxt_string_app.string_id,
    p_language_id           LIKE qxt_string_app.language_id,
    l_key1_string_id        LIKE qxt_string_app.string_id,
    l_key2_language_id      LIKE qxt_string_app.language_id,
    l_string_app_rec        RECORD LIKE qxt_string_app.*,
    source_string_app_rec   RECORD LIKE qxt_string_app.*,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_edit() - Function Entry Point"
    DISPLAY "string_app_edit() - p_string_id=", p_string_id
    DISPLAY "string_app_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_string_id = p_string_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_string_app_rec(p_string_id,p_language_id) 
    RETURNING source_string_app_rec.*

  LET l_string_app_rec.* = source_string_app_rec.*
  #LET l_string_app_rec.string_id = get_string_app_new_string_id(p_language_id)

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_l2"), TRUE) 
    CALL populate_string_app_form_edit_labels_g()

#  ELSE
#    CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_t"), TRUE) 
#    CALL populate_string_app_form_edit_labels_t()
#  END IF


  #Call the INPUT
  CALL string_app_input(l_string_app_rec.*,TRUE) 
    RETURNING l_string_app_rec.*

  CALL fgl_window_close("w_string_app")

  IF NOT int_flag THEN
    INSERT INTO qxt_string_app VALUES (
      l_string_app_rec.string_id,
      l_string_app_rec.language_id,
      l_string_app_rec.string_data,
      l_string_app_rec.category1_id,
      l_string_app_rec.category2_id,
      l_string_app_rec.category3_id

                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(790),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(790),NULL)
      RETURN l_string_app_rec.string_id, l_string_app_rec.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]

  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL, NULL
  END IF


END FUNCTION



#################################################
# FUNCTION string_app_input(p_string_app_rec)
#
# Input string_app details (edit/create)
#
# RETURN l_string_app.*
#################################################
FUNCTION string_app_input(p_string_app_rec,p_new_rec_flag)
  DEFINE 
    p_string_app_rec            RECORD LIKE qxt_string_app.*,
    l_string_app_form_rec          OF t_qxt_string_app_form_rec,
    l_orignal_string_id             LIKE qxt_string_app.string_id,
    l_orignal_language_id           LIKE qxt_string_app.language_id,
    local_debug                     SMALLINT,
    tmp_str                         STRING,
    p_new_rec_flag                  SMALLINT

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_string_id = p_string_app_rec.string_id
  LET l_orignal_language_id = p_string_app_rec.language_id

  #copy record data to form_record format record
  CALL copy_string_app_record_to_form_record(p_string_app_rec.*) RETURNING l_string_app_form_rec.*


  ############################
  # INPUT 
  ############################
  INPUT BY NAME l_string_app_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      NEXT FIELD string_data

    ON KEY (F100)  -- get next id (MAX id for this language)
      CALL fgl_dialog_update_data()
      LET l_string_app_form_rec.string_id = get_string_app_new_string_id(get_language_id(l_string_app_form_rec.language_name))
      DISPLAY BY NAME l_string_app_form_rec.string_id


    ############################
    # AFTER FIELD BLOCK 
    ############################

    #String ID
    AFTER FIELD string_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(791), l_string_app_form_rec.string_id,NULL,TRUE)  THEN
        NEXT FIELD string_id
      END IF

    #Language
    AFTER FIELD language_name
      #language_id must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(792), l_string_app_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD language_name
      END IF




    ############################
    # AFTER INPUT BLOCK 
    ############################

    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #string_id must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(791), l_string_app_form_rec.string_id,NULL,TRUE)  THEN
          NEXT FIELD string_id
          CONTINUE INPUT
        END IF

      #language_ID must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(792), l_string_app_form_rec.language_name,NULL,TRUE)  THEN

          NEXT FIELD language_id
          CONTINUE INPUT
        END IF


      #The string_id and language_id must be unique - DOUBLE KEY
      IF string_id_and_string_app_language_id_count(l_string_app_form_rec.string_id,get_language_id(l_string_app_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
 
       IF p_new_rec_flag OR (NOT ((l_orignal_string_id = l_string_app_form_rec.string_id) AND (l_orignal_language_id = get_language_id(l_string_app_form_rec.language_name))))THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_doublekey(get_str_tool(791),get_str_tool(792),NULL)
          NEXT FIELD string_id
          CONTINUE INPUT
        END IF
      END IF




  END INPUT

  ############################
  # INPUT END 
  ############################


  IF local_debug THEN
    DISPLAY "string_app_input() - l_string_app_form_rec.string_id=",l_string_app_form_rec.string_id
    DISPLAY "string_app_input() - l_string_app_form_rec.language_name=",l_string_app_form_rec.language_name
    DISPLAY "string_app_input() - l_string_app_form_rec.string_data=",l_string_app_form_rec.string_data
  END IF


  #Copy the form record data to a normal string_app record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_string_app_form_record_to_record(l_string_app_form_rec.*) RETURNING p_string_app_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "string_app_input() - p_string_app_rec.string_id=",p_string_app_rec.string_id
    DISPLAY "string_app_input() - p_string_app_rec.language_id=",p_string_app_rec.language_id
    DISPLAY "string_app_input() - p_string_app_rec.string_data=",p_string_app_rec.string_data
  END IF

  RETURN p_string_app_rec.*

END FUNCTION


#################################################
# FUNCTION string_app_translate(l_string_id, l_language_id, l_t_lang_id_1, l_t_lang_id_2)
#
# Translate String - Upper Function
#
# RETURN NONE
#################################################
FUNCTION string_app_translate(p_string_id, p_language_id, p_t_lang_id_1, p_t_lang_id_2)
  DEFINE
    p_string_id     LIKE qxt_string_app.string_id,
    p_language_id   LIKE qxt_string_app.language_id,
    p_t_lang_id_1   LIKE qxt_string_app.language_id,
    p_t_lang_id_2   LIKE qxt_string_app.language_id,
    local_debug     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "string_app_translate(() - p_string_id=", p_string_id
    DISPLAY "string_app_translate(() - p_language_id=", p_language_id
    DISPLAY "string_app_translate(() - p_t_lang_id_1=", p_t_lang_id_1
    DISPLAY "string_app_translate(() - p_t_lang_id_2=", p_t_lang_id_2
  END IF

  #At least one additional language for the translation must be specified
  IF (p_language_id = p_t_lang_id_1) OR (p_language_id = p_t_lang_id_2) OR (p_t_lang_id_1 = p_t_lang_id_2) THEN
    CALL fgl_winmessage("Translation Language Option Error","You cannot translate from and to the same language !\nSpecify the correct languages!","error")
    RETURN
  END IF

  
  CALL string_app_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)


END FUNCTION


#################################################
# FUNCTION string_app_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)
#
# Translate String - Lower Function
#
# RETURN NONE
#################################################
FUNCTION string_app_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)
  DEFINE
    p_string_id          LIKE qxt_string_app.string_id,
    p_language_id        LIKE qxt_string_app.language_id,
    l_language_name      LIKE qxt_language.language_name,
    l_string_data_0      LIKE qxt_string_app.string_data,
    p_t_lang_id_1        LIKE qxt_string_app.language_id,
    l_t_lang_name_1      LIKE qxt_language.language_name,
    l_string_data_1      LIKE qxt_string_app.string_data,
    p_t_lang_id_2        LIKE qxt_string_app.language_id,
    l_t_lang_name_2      LIKE qxt_language.language_name,
    l_string_data_2      LIKE qxt_string_app.string_data,
    l_original_lang_id_1 LIKE qxt_string_app.language_id,
    l_original_lang_id_2 LIKE qxt_string_app.language_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

  LET l_original_lang_id_1 = p_t_lang_id_1
  LET l_original_lang_id_2 = p_t_lang_id_2

  CALL fgl_window_open("w_translate_double",3,3,get_form_path("f_qxt_string_translate_l2"),FALSE)

  CALL populate_string_app_translate_double_form_labels_g()

  #Display some static data
  DISPLAY p_string_id TO string_id
  DISPLAY get_language_name(p_language_id) TO language_0

  LET l_language_name = get_language_name(p_language_id)
  LET l_t_lang_name_1 = get_language_name(p_t_lang_id_1)
  LET l_t_lang_name_2 = get_language_name(p_t_lang_id_2)

  LET l_string_data_0 = get_string_app_string_data(p_string_id,p_language_id)
  LET l_string_data_1 = get_string_app_string_data(p_string_id,p_t_lang_id_1)
  LET l_string_data_2 = get_string_app_string_data(p_string_id,p_t_lang_id_2)


  IF local_debug THEN
    DISPLAY "string_app_translate_double() - p_string_id=", p_string_id
    DISPLAY "string_app_translate_double() - p_language_id=", p_language_id
    DISPLAY "string_app_translate_double() - p_t_lang_id_1=", p_t_lang_id_1
    DISPLAY "string_app_translate_double() - p_t_lang_id_2=", p_t_lang_id_2
    DISPLAY "string_app_translate_double() - l_string_data_0=", l_string_data_0
    DISPLAY "string_app_translate_double() - l_string_data_1=", l_string_data_1
  END IF





  INPUT l_string_data_0, l_t_lang_name_1, l_string_data_1,l_t_lang_name_2,l_string_data_2 WITHOUT DEFAULTS 
    FROM string_0,language_1, string_1, language_2, string_2    HELP 1

    AFTER FIELD language_1
      IF get_language_id(l_t_lang_name_1) <> l_original_lang_id_1 THEN
        #When the language changes, we have to update the current data set
        #Check if this is an edit or create operation
        IF string_id_and_string_app_language_id_count(p_string_id, l_original_lang_id_1) THEN
          CALL string_app_edit(p_string_id,l_original_lang_id_1,l_string_data_1,FALSE)
        ELSE
          CALL string_app_create(p_string_id,l_original_lang_id_1,l_string_data_1,FALSE)
        END IF

        LET p_t_lang_id_1 = get_language_id(l_t_lang_name_1)
        LET l_original_lang_id_1 = p_t_lang_id_1

        #Now, we have to fetch the corresponding string data for the new language_id

        LET l_string_data_1 = get_string_app_string_data(p_string_id,p_t_lang_id_1)
        DISPLAY l_string_data_1 TO string_1
      END IF

    AFTER FIELD language_2
      IF get_language_id(l_t_lang_name_2) <> l_original_lang_id_2 THEN
        #When the language changes, we have to update the current data set
        #Check if this is an edit or create operation
        IF string_id_and_string_app_language_id_count(p_string_id, l_original_lang_id_2) THEN
          CALL string_app_edit(p_string_id,l_original_lang_id_2,l_string_data_2,FALSE)
        ELSE
          CALL string_app_create(p_string_id,l_original_lang_id_2,l_string_data_2,FALSE)
        END IF

        LET p_t_lang_id_2 = get_language_id(l_t_lang_name_2)
        LET l_original_lang_id_2 = p_t_lang_id_2

        #Now, we have to fetch the corresponding string data for the new language_id

        LET l_string_data_2 = get_string_app_string_data(p_string_id,p_t_lang_id_2)
        DISPLAY l_string_data_2 TO string_2
      END IF


 
    AFTER INPUT
      #LET p_language_id   = get_language_id(l_language_name)
      LET p_t_lang_id_1 = get_language_id(l_t_lang_name_1)
      LET p_t_lang_id_2 = get_language_id(l_t_lang_name_2)
  END INPUT


  IF NOT int_flag THEN

    #Original Master String (selected string language)

    CALL string_app_edit(p_string_id,p_language_id,l_string_data_0,FALSE)

    #First translation String (selected string language)

    IF string_id_and_string_app_language_id_count(p_string_id, p_t_lang_id_1) THEN
      CALL string_app_edit(p_string_id,p_t_lang_id_1,l_string_data_1,FALSE)
    ELSE
      CALL string_app_create(p_string_id,p_t_lang_id_1,l_string_data_1,FALSE)
    END IF

    #Second Translation String
    IF string_id_and_string_app_language_id_count(p_string_id, p_t_lang_id_2) THEN
      #CALL fgl_winmessage("2nd string - edit ", "p_string_id = " || p_string_id || " p_t_lang_id_2 = " || p_t_lang_id_2 || " l_string_data_2=" || l_string_data_2,"info")
      CALL string_app_edit(p_string_id,p_t_lang_id_2,l_string_data_2,FALSE)
    ELSE
      #CALL fgl_winmessage("2nd string - create ", "p_string_id = " || p_string_id || " p_t_lang_id_2 = " || p_t_lang_id_2 || " l_string_data_2=" || l_string_data_2,"info")
      CALL string_app_create(p_string_id,p_t_lang_id_2,l_string_data_2,FALSE)

    END IF
  ELSE
    LET int_flag = FALSE
  END IF

  
  CALL fgl_window_close("w_translate_double")


END FUNCTION



#################################################
# FUNCTION string_app_delete(p_string_id)
#
# Delete a string_app record
#
# RETURN NONE
#################################################
FUNCTION string_app_delete(p_string_id, p_language_id)
  DEFINE 
    p_string_id       LIKE qxt_string_app.string_id,
    p_language_id     LIKE qxt_string_app.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(780), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_string_app 
        WHERE string_id = p_string_id
          AND language_id = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION string_app_view(p_string_id)
#
# View string_app record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION string_app_view(p_string_id,p_language_id)
  DEFINE 
    p_string_id             LIKE qxt_string_app.string_id,
    p_language_id           LIKE qxt_string_app.language_id,
    l_string_app_rec    RECORD LIKE qxt_string_app.*


  CALL get_string_app_rec(p_string_id, p_language_id) RETURNING l_string_app_rec.*
  CALL string_app_view_by_rec(l_string_app_rec.*)

END FUNCTION


#################################################
# FUNCTION string_app_view_by_rec(p_string_app_rec)
#
# View string_app record in window-form
#
# RETURN NONE
#################################################
FUNCTION string_app_view_by_rec(p_string_app_rec)
  DEFINE 
    p_string_app_rec     RECORD LIKE qxt_string_app.*,
    inp_char                 CHAR,
    tmp_str                  STRING

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_l2"), TRUE) 
    CALL populate_string_app_form_view_labels_g()
#  ELSE
#    CALL fgl_window_open("w_string_app", 3, 3, get_form_path("f_qxt_string_app_det_t"), TRUE) 
#    CALL populate_string_app_form_view_labels_t()
#  END IF

  DISPLAY BY NAME p_string_app_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_string_app")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################

####################################################
# FFUNCTION string_id_and_string_app_language_id_count(p_string_id, p_language_id)
#
# tests if a record with this string_id already exists
#
# RETURN r_count
####################################################
FUNCTION string_id_and_string_app_language_id_count(p_string_id, p_language_id)
  DEFINE
    p_string_id      LIKE qxt_string_app.string_id,
    p_language_id    LIKE qxt_string_app.language_id,
    r_count          SMALLINT,
    local_debug      SMALLINT

  LET local_debug = FALSE

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_string_app
      WHERE qxt_string_app.string_id = p_string_id
        AND qxt_string_app.language_id = p_language_id

  IF local_debug THEN
    DISPLAY "string_id_and_string_app_language_id_count() - r_count=", r_count
  END IF

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

####################################################
# FUNCTION string_app_language_id_count(p_language_id)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION string_app_language_id_count(p_language_id)
  DEFINE
    p_language_id         LIKE qxt_string_app.language_id,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_string_app
      WHERE qxt_string_app.language_id = p_language_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION string_app_id_count(p_string_id)
#
# tests if a record with this string_id already exists
#
# RETURN r_count
####################################################
FUNCTION string_app_id_count(p_string_id)
  DEFINE
    p_string_id    LIKE qxt_string_app.string_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_string_app
      WHERE qxt_string_app.string_id = p_string_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_string_app_record_to_form_record(p_string_app_rec)  
#
# Copy normal string_app record data to type string_app_form_rec
#
# RETURN l_string_app_form_rec.*
######################################################
FUNCTION copy_string_app_record_to_form_record(p_string_app_rec)  
  DEFINE
    p_string_app_rec       RECORD LIKE qxt_string_app.*,
    l_string_app_form_rec  OF t_qxt_string_app_form_rec

  LET l_string_app_form_rec.string_id =      p_string_app_rec.string_id
  LET l_string_app_form_rec.language_name =  get_language_name(p_string_app_rec.language_id)
  LET l_string_app_form_rec.string_data =    p_string_app_rec.string_data

  LET l_string_app_form_rec.category1_data = get_str_category_data(p_string_app_rec.category1_id, get_language())
  LET l_string_app_form_rec.category2_data = get_str_category_data(p_string_app_rec.category2_id, get_language())
  LET l_string_app_form_rec.category3_data = get_str_category_data(p_string_app_rec.category3_id, get_language())


  RETURN l_string_app_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_string_app_form_record_to_record(p_string_app_form_rec)  
#
# Copy type string_app_form_rec to normal string_app record data
#
# RETURN l_string_app_rec.*
######################################################
FUNCTION copy_string_app_form_record_to_record(p_string_app_form_rec)  
  DEFINE
    l_string_app_rec       RECORD LIKE qxt_string_app.*,
    p_string_app_form_rec  OF t_qxt_string_app_form_rec

  LET l_string_app_rec.string_id = p_string_app_form_rec.string_id
  LET l_string_app_rec.language_id = get_language_id(p_string_app_form_rec.language_name)
  LET l_string_app_rec.string_data = p_string_app_form_rec.string_data

  LET l_string_app_rec.category1_id = get_str_category_id(p_string_app_form_rec.category1_data, get_language())
  LET l_string_app_rec.category2_id = get_str_category_id(p_string_app_form_rec.category2_data, get_language())
  LET l_string_app_rec.category3_id = get_str_category_id(p_string_app_form_rec.category3_data, get_language())

  RETURN l_string_app_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_string_app_scroll()
#
# Populate string_app grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_string_app_scroll()
  CALL fgl_grid_header("sc_string_app","string_id",      get_str_tool(791),"right","F13")  --string_app
  CALL fgl_grid_header("sc_string_app","language_name",  get_str_tool(792),"left", "F14")  --file language_id
  CALL fgl_grid_header("sc_string_app","string_data",    get_str_tool(793),"left", "F15")  --string_app_data
  CALL fgl_grid_header("sc_string_app","category1_data", get_str_tool(794),"left", "F16")  --string_tool_data
  CALL fgl_grid_header("sc_string_app","category2_data", get_str_tool(795),"left", "F17")  --string_tool_data
  CALL fgl_grid_header("sc_string_app","category3_data", get_str_tool(796),"left", "F18")  --string_tool_data


END FUNCTION


#######################################################
# FUNCTION str_app_combo_population()
#
# Populate combo lists
#
# RETURN NONE
#######################################################
FUNCTION str_app_combo_population()
  DEFINE l_language_id LIKE qxt_language.language_id

  LET l_language_id = get_language()

  CALL str_category_data_combo_list("category1_data",l_language_id)
  CALL str_category_data_combo_list("category2_data",l_language_id)
  CALL str_category_data_combo_list("category3_data",l_language_id)
  CALL language_combo_list("language_name")

END FUNCTION


#######################################################
# FUNCTION populate_string_app_form_labels_g()
#
# Populate string_tool form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_translate_double_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail2GroupBox","1st Language Translation")
  CALL updateUILabel("cntDetail3GroupBox","2nd Language Translation")
  CALL fgl_settitle(get_str_tool(782) || " - String Translator")

  #DISPLAY get_str_tool(780) TO lbTitle

  #DISPLAY get_str_tool(791) TO dl_f1
  #DISPLAY get_str_tool(792) TO dl_f2
  #DISPLAY get_str_tool(793) TO dl_f3
  #DISPLAY get_str_tool(794) TO dl_f4
  #DISPLAY get_str_tool(795) TO dl_f5
  #DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  #CALL language_combo_list("language_0")
  CALL language_combo_list("language_1")
  CALL language_combo_list("language_2")
  
  #DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  #DISPLAY get_str_tool(23) TO bt_get_new_id
  #DISPLAY "!" TO bt_get_new_id

  #CALL language_combo_list("language_name")

END FUNCTION


#######################################################
# FUNCTION populate_string_app_form_labels_g()
#
# Populate string_app form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_labels_g()

  CALL fgl_settitle(get_str_tool(782))

  DISPLAY get_str_tool(782) TO lbTitle

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_app_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_string_app_form_labels_t()
#
# Populate string_app form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_labels_t()

  DISPLAY get_str_tool(782) TO lbTitle

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_string_app_form_edit_labels_g()
#
# Populate string_app form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_edit_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","String Properties")  
  CALL updateUILabel("cntDetail2GroupBox","Actual String Text")
  CALL updateUILabel("cntDetail3GroupBox","Categories (used for searching/filtering)")
  CALL fgl_settitle(trim(get_str_tool(782)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_app_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_string_app_form_edit_labels_t()
#
# Populate string_app form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_edit_labels_t()

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_string_app_form_view_labels_g()
#
# Populate string_app form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_view_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","String Properties")  
  CALL updateUILabel("cntDetail2GroupBox","Actual String Text")
  CALL updateUILabel("cntDetail3GroupBox","Categories (used for searching/filtering)")
  CALL fgl_settitle(trim(get_str_tool(782)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_app_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_string_app_form_view_labels_t()
#
# Populate string_app form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_view_labels_t()

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_string_app_form_create_labels_g()
#
# Populate string_app form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_create_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","String Properties")  
  CALL updateUILabel("cntDetail2GroupBox","Actual String Text")
  CALL updateUILabel("cntDetail3GroupBox","Categories (used for searching/filtering)")
  CALL fgl_settitle(trim(get_str_tool(782)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_app_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_string_app_form_create_labels_t()
#
# Populate string_app form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_form_create_labels_t()

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_string_app_list_form_labels_g()
#
# Populate string_app list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_list_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details / Filter")
  CALL updateUILabel("switch_filter_language","Filter")
  CALL updateUILabel("switch_filter_search","Filter")
  CALL fgl_settitle(trim(get_str_tool(782)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_string_app_scroll()

  #DISPLAY get_str_tool(791) TO dl_f1
  #DISPLAY get_str_tool(792) TO dl_f2
  #DISPLAY get_str_tool(793) TO dl_f3
  #DISPLAY get_str_tool(794) TO dl_f4
  #DISPLAY get_str_tool(795) TO dl_f5
  #DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  DISPLAY get_str_tool(22) TO bt_duplicate
  #DISPLAY "!" TO bt_duplicate


  #Filter check boxex and set buttons
  #DISPLAY "!" TO bt_filter_language_set
  #DISPLAY "!" TO switch_filter_language

  CALL language_combo_list("cb_f_lang")

  #DISPLAY "!" TO bt_search_filter_set
  #DISPLAY "!" TO switch_filter_search

  #Goto Line Numer button
  #DISPLAY "!" TO bt_goto_line
  #DISPLAY "!" TO bt_goto_id


  #Translate
  #DISPLAY "!" TO bt_translate
  #DISPLAY get_str_tool(22) TO bt_translate

  #DISPLAY "!" TO bt_set_trans
  #DISPLAY get_str_tool(22) TO bt_set_translation


  CALL language_combo_list("trans_lang_1")
  CALL language_combo_list("trans_lang_2")

END FUNCTION


#######################################################
# FUNCTION populate_string_app_list_form_labels_t()
#
# Populate string_app list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_string_app_list_form_labels_t()

  DISPLAY trim(get_str_tool(782)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################





