##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


######################################################
# REPORT warehouse_report(p_warehouse_rec)  
######################################################
REPORT warehouse_report(p_warehouse_rec) 

  DEFINE p_warehouse_rec OF t_warehouse_rec

  OUTPUT
    BOTTOM MARGIN 1
    TOP MARGIN 1
    PAGE LENGTH 64

  ORDER BY p_warehouse_rec.stock_id

  FORMAT

    FIRST PAGE HEADER
      SKIP 2 LINES
      PRINT COLUMN 25, get_str(2511) -- "WAREHOUSE REPORT"
      SKIP 1 LINE
      PRINT COLUMN 20, get_str(2512), TODAY USING "dd mmm. yyyy" -- "Executing date: "
      SKIP 2 LINES
      PRINT COLUMN 15, get_str(2513), COUNT(*) USING ": ##" -- "Total Quantity of Presented Items"
      SKIP 4 LINES

    ON EVERY ROW 
      PRINT "-----------------------------------------"
      SKIP 1 LINE
      PRINT COLUMN 5,  get_str(2514), -- "Stock ID: "
            COLUMN 20, p_warehouse_rec.stock_id
      PRINT COLUMN 5,  get_str(2515), -- "Description: "
            COLUMN 20, p_warehouse_rec.item_desc
      SKIP 1 LINE
      PRINT COLUMN 12, get_str(2516), -- "Total amount: "
            COLUMN 35, p_warehouse_rec.quantity
      PRINT COLUMN 12, get_str(2517), -- "Reserved: "
            COLUMN 35, p_warehouse_rec.reserved
      PRINT COLUMN 12, get_str(2518), -- "Supply on shot term: "
            COLUMN 35, p_warehouse_rec.shot_term
      PRINT COLUMN 12, get_str(2519), -- "Supply on long term: "
            COLUMN 35, p_warehouse_rec.long_term
      PRINT COLUMN 12, get_str(2520), -- "Required: "
            COLUMN 35, p_warehouse_rec.required
      SKIP 2 LINES

    PAGE TRAILER
      SKIP 3 LINES
      PRINT COLUMN 45, get_str(2521), PAGENO USING "## /", (COUNT(*) + 1 + (5 - ((COUNT(*) + 1) mod 5))) / 5 USING "##" -- "Page ## / ##"

END REPORT  -- warehouse_report_main --
























