##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the str_category table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_category_id(p_language_id)  get str_category id from p_language_id        l_category_id
# get_language_id(category_id)    Get language_id from p_category_id        l_language_id
# get_str_category_rec(p_category_id)   Get the str_category record from p_category_id  l_str_category.*
# str_category_popup_data_source()               Data Source (cursor) for str_category_popup              NONE
# str_category_popup                             str_category selection window                            p_category_id
# (p_category_id,p_order_field,p_accept_action)
# str_category_combo_list(cb_field_name)         Populates str_category combo list from db                NONE
# str_category_create()                          Create a new str_category record                         NULL
# str_category_edit(p_category_id)      Edit str_category record                                 NONE
# str_category_input(p_str_category_rec)    Input str_category details (edit/create)                 l_str_category.*
# str_category_delete(p_category_id)    Delete a str_category record                             NONE
# str_category_view(p_category_id)      View str_category record by ID in window-form            NONE
# str_category_view_by_rec(p_str_category_rec) View str_category record in window-form               NONE
# get_category_id_from_language_id(p_language_id)          Get the category_id from a file                      l_category_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# category_id_count(p_category_id)  tests if a record with this category_id already exists r_count
# copy_str_category_record_to_form_record        Copy normal str_category record data to type str_category_form_rec   l_str_category_form_rec.*
# (p_str_category_rec)
# copy_str_category_form_record_to_record        Copy type str_category_form_rec to normal str_category record data   l_str_category_rec.*
# (p_str_category_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_str_category_scroll()              Populate str_category grid headers                       NONE
# populate_str_category_form_labels_g()          Populate str_category form labels for gui                NONE
# populate_str_category_form_labels_t()          Populate str_category form labels for text               NONE
# populate_str_category_form_edit_labels_g()     Populate str_category form edit labels for gui           NONE
# populate_str_category_form_edit_labels_t()     Populate str_category form edit labels for text          NONE
# populate_str_category_form_view_labels_g()     Populate str_category form view labels for gui           NONE
# populate_str_category_form_view_labels_t()     Populate str_category form view labels for text          NONE
# populate_str_category_form_create_labels_g()   Populate str_category form create labels for gui         NONE
# populate_str_category_form_create_labels_t()   Populate str_category form create labels for text        NONE
# populate_str_category_list_form_labels_g()     Populate str_category list form labels for gui           NONE
# populate_str_category_list_form_labels_t()     Populate str_category list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_string_category_manager_globals.4gl"


#########################################################
# FUNCTION get_str_category_id(p_category_data, p_language_id)
#
# get category_data from p_category_id
#
# RETURN l_category_id
#########################################################
FUNCTION get_str_category_id(p_category_data, p_language_id)
  DEFINE 
    p_category_data          LIKE qxt_str_category.category_data,
    p_language_id                 LIKE qxt_language.language_id,
    l_category_id            LIKE qxt_str_category.category_id,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_category_data() - p_category_data = ", p_category_data
    DISPLAY "get_category_data() - p_language_id = ", p_language_id
  END IF

  #CHECK FOR NULL
  IF p_category_data IS NULL THEN
    RETURN 1  --The first record entry will be used for the empty string / no category
  END IF

  SELECT category_id
    INTO l_category_id
    FROM qxt_str_category
    WHERE category_data = p_category_data
      AND language_id = p_language_id
    

  IF local_debug THEN
    DISPLAY "get_category_data() - l_category_id = ", l_category_id
  END IF

  RETURN l_category_id
END FUNCTION


#########################################################
# FUNCTION get_str_category_data(p_category_id,p_language_id)
#
# get category_data from p_category_id
#
# RETURN l_category_data
#########################################################
FUNCTION get_str_category_data(p_category_id,p_language_id)
  DEFINE 
    p_category_id          LIKE qxt_str_category.category_id,
    p_language_id               LIKE qxt_language.language_id,
    l_category_data        LIKE qxt_str_category.category_data,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_category_data() - p_category_id = ", p_category_id
    DISPLAY "get_category_data() - p_language_id = ", p_language_id
  END IF

  SELECT category_data
    INTO l_category_data
    FROM qxt_str_category
    WHERE category_id = p_category_id
      AND language_id = p_language_id


  IF local_debug THEN
    DISPLAY "get_category_data() - l_category_data = ", l_category_data
  END IF

  RETURN l_category_data
END FUNCTION



#########################################################
# FUNCTION get_str_category_rec(p_category_id,p_language_id)
#
# get string record from p_language_id and p_category_id
#
# RETURN l_str_category_rec.*
#########################################################
FUNCTION get_str_category_rec(p_category_id,p_language_id)
  DEFINE 
    p_category_id          LIKE qxt_str_category.category_id,
    p_language_id               LIKE qxt_language.language_id,
    l_str_category_rec         RECORD LIKE qxt_str_category.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_category_data() - p_category_id = ", p_category_id
    DISPLAY "get_category_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_str_category.*
    INTO l_str_category_rec.*
    FROM qxt_str_category
    WHERE category_id = p_category_id
      AND language_id = p_language_id

  RETURN l_str_category_rec.*
END FUNCTION


###################################################################################
# FUNCTION get_str_category_new_id(p_language_id)
#
# Finds MAX category_id and Returns it +1  (SERIAL emulation) 
#
# RETURN NONE
###################################################################################
FUNCTION get_str_category_new_id(p_language_id)
  DEFINE
    p_language_id            LIKE qxt_language.language_id,
    l_max_category_id   LIKE qxt_str_category.category_id

  SELECT  MAX (category_id)
    INTO l_max_category_id
    FROM qxt_str_category
    WHERE qxt_str_category.language_id = p_language_id

  RETURN l_max_category_id + 1
END FUNCTION


########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION str_category_data_combo_list(p_cb_field_name,p_language_id)
#
# Populates str_category_data_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION str_category_data_combo_list(p_cb_field_name,p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id               LIKE qxt_language.language_id,
    l_category_data             DYNAMIC ARRAY OF LIKE qxt_str_category.category_data,
    row_count                 INTEGER,
    current_row               INTEGER,
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "str_category_data_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "str_category_data_combo_list() - p_language_id=", p_language_id

  END IF

  DECLARE c_str_category_scroll2 CURSOR FOR 
  SELECT  category_data
    FROM  qxt_str_category
    WHERE language_id = p_language_id
    ORDER BY category_data ASC 

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_str_category_scroll2 INTO l_category_data[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_category_data[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_category_data[row_count] CLIPPED, ")"
      DISPLAY "str_category_data_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


###################################################################################
# FUNCTION toolbar_category_id_combo_list(p_cb_field_name,p_language_id)
#
# Populates toolbar_category_id_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION toolbar_category_id_combo_list(p_cb_field_name,p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_language.language_id,
    l_category_id             DYNAMIC ARRAY OF LIKE qxt_str_category.category_id,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "toolbar_category_id_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "toolbar_category_id_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_toolbar_category_id_scroll2 CURSOR FOR 
    SELECT category_id
      FROM qxt_str_category
      WHERE language_id = p_language_id 
      ORDER BY category_id ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_toolbar_category_id_scroll2 INTO l_category_id[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_category_id[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_category_id[row_count] CLIPPED, ")"
      DISPLAY "toolbar_category_id_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION



######################################################
# FUNCTION str_category_popup_data_source()
#
# Data Source (cursor) for str_category_popup
#
# RETURN NONE
######################################################
FUNCTION str_category_popup_data_source(p_filter,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       STRING,
    p_language_id       LIKE qxt_language.language_id,
    sql_stmt            VARCHAR(600),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    p_filter            SMALLINT,
    local_debug         SMALLINT

  LET local_debug = FALSE
  
  IF local_debug THEN
    DISPLAY "str_category_popup_data_source() - p_filter=", p_filter
    DISPLAY "str_category_popup_data_source() - p_language_id=", p_language_id
    DISPLAY "str_category_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "str_category_popup_data_source() - p_ord_dir=", p_ord_dir

  ENd IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "category_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  IF p_filter IS NULL THEN
    LET p_filter = FALSE
  END IF

  LET sql_stmt = "SELECT ",
                 "category_id, ",
                 "language_name, ",
                 "category_data ",

                 "FROM ",
                 "qxt_str_category, ",
                 "qxt_language ",
                 "WHERE ",
                 "qxt_str_category.language_id = qxt_language.language_id "


  IF p_filter AND p_language_id IS NOT NULL AND p_language_id > 0 THEN
    LET sql_stmt = trim(sql_stmt), " ", 
                 "AND ",
                 "qxt_str_category.language_id = ", trim(p_language_id) , " "
  END IF
                    


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ,qxt_str_category.language_id ", p_ord_dir_str --CLIPPED,  " ,language_id "
  END IF

  IF local_debug THEN
    DISPLAY "str_category_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
    DISPLAY sql_stmt[501,600]
  END IF

  PREPARE p_str_category FROM sql_stmt
  DECLARE c_str_category CURSOR FOR p_str_category

END FUNCTION


######################################################
# FUNCTION str_category_popup(p_category_id,p_order_field,p_accept_action)
#
# str_category selection window
#
# RETURN p_category_id
######################################################
FUNCTION str_category_popup(p_category_id, p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_category_id                LIKE qxt_str_category.category_id,  --default return value if user cancels
    p_language_id                LIKE qxt_language.language_id,
    l_str_category_arr           DYNAMIC ARRAY OF t_qxt_str_category_form_rec,    --RECORD LIKE qxt_str_category.*, 
    l_arr_size                   SMALLINT, 
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      STRING,
    p_order_field,p_order_field2 STRING, 
    l_category_id                LIKE qxt_str_category.category_id,
    l_language_id                LIKE qxt_language.language_id,
    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                  SMALLINT,
    l_lang_name                  STRING,
    l_filter                     SMALLINT


  LET local_debug = FALSE

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1
  LET l_arr_size = 200 -- due to dynamic array ...sizeof(l_str_category_arr)
  IF p_language_id IS NULL OR p_language_id = 0 THEN
    LET l_filter = FALSE
    #LET p_language_id = get_language()
  END IF


  IF local_debug THEN
    DISPLAY "str_category_popup() - p_category_id=",  p_category_id
    DISPLAY "str_category_popup() - p_language_id=",  p_language_id
    DISPLAY "str_category_popup() - p_order_field=",  p_order_field
    DISPLAY "str_category_popup() - p_accept_action=",p_accept_action
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_str_category_scroll", 2, 8, get_form_path("f_qxt_str_category_scroll_g"),FALSE) 
    CALL populate_str_category_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_str_category_scroll", 2, 8, get_form_path("f_qxt_str_category_scroll_t"),FALSE) 
    CALL populate_str_category_list_form_labels_t()
  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_str_category

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL str_category_create(NULL,NULL,NULL)
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL str_category_popup_data_source(l_filter,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_str_category INTO l_str_category_arr[i].*
      IF local_debug THEN
        DISPLAY "str_category_popup() - i=",i
        DISPLAY "str_category_popup() - l_str_category_arr[i].category_id=",l_str_category_arr[i].category_id
        DISPLAY "str_category_popup() - l_str_category_arr[i].language_name=",l_str_category_arr[i].language_name
        DISPLAY "str_category_popup() - l_str_category_arr[i].category_data=",l_str_category_arr[i].category_data

      END IF

      IF l_str_category_arr[i].category_id = l_category_id THEN
        LET current_row = i
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        ERROR "Too many rows found to display! Only the first ", trim(l_arr_size), " rows will be displayed"
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_str_category_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF    
    
		CALL l_str_category_arr.resize(i)

    IF local_debug THEN
      DISPLAY "str_category_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")  --info - no rows found
      #CALL fgl_window_close("w_str_category_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "str_category_popup() - set_count(i)=",i
    END IF

    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_str_category_arr TO sc_str_category.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL fgl_dialog_setcurrline(5,current_row)

      BEFORE ROW
        LET i = arr_curr()
        LET current_row = i
        LET l_category_id = l_str_category_arr[i].category_id
        LET l_language_id = get_language_id(l_str_category_arr[i].language_name)


      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL str_category_view(l_category_id)  --(l_category_id,l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL str_category_edit(l_category_id,l_language_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL str_category_delete(l_category_id,l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL str_category_create(NULL,NULL,NULL)
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","str_category_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "str_category_popup(p_category_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("str_category_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL str_category_create(NULL,NULL,NULL)
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL str_category_edit(l_category_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL str_category_delete(l_category_id,l_language_id)
        EXIT DISPLAY



      #Grid column sort    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "category_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      #Language filter
      ON KEY(F120)
        LET l_filter = TRUE
        EXIT DISPLAY 

      ON KEY(F121)
        LET l_filter = FALSE
        EXIT DISPLAY 

      ON KEY(F122)  --Specify filter criteria
        INPUT l_lang_name WITHOUT DEFAULTS FROM combo_lang HELP 1
          ON KEY(F122)
            CALL fgl_dialog_update_data()
            LET p_language_id = get_language_id(l_lang_name)
            LET l_filter = TRUE
            EXIT INPUT
          AFTER INPUT
            LET p_language_id = get_language_id(l_lang_name)
            LET l_filter = TRUE

        END INPUT

        EXIT DISPLAY 

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_str_category_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_category_id, p_language_id 
  ELSE 
    RETURN l_category_id , l_language_id
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION str_category_create(p_id,p_language_id,p_data)
#
# Create a new str_category record
#
# RETURN NULL
######################################################
FUNCTION str_category_create(p_id,p_language_id,p_data)
  DEFINE 
    p_id            LIKE qxt_str_category.category_id,
    p_language_id   LIKE qxt_str_category.language_id,
    p_data          LIKE qxt_str_category.category_data,
    l_str_category RECORD LIKE qxt_str_category.*,
    local_debug     SMALLINT

  LET local_debug = FALSE

  #Initialise some variables
  IF p_id IS NOT NULL THEN
    LET l_str_category.category_id = p_id
  ELSE
    LET l_str_category.category_id = get_str_category_new_id(get_language())
  END IF

  IF p_language_id IS NOT NULL THEN
    LET l_str_category.language_id = p_language_id
  ELSE
    LET l_str_category.language_id = get_language()
  END IF

  IF p_data IS NOT NULL THEN
    LET l_str_category.category_data = p_data
  END IF


#select max column from table
#informix guide to sql

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_g"), TRUE) 
    CALL populate_str_category_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_t"), TRUE) 
    CALL populate_str_category_form_create_labels_t()
  END IF



  LET int_flag = FALSE
  

  IF local_debug THEN
    DISPLAY "str_category_create() - l_str_category.category_id=", l_str_category.category_id
    DISPLAY "str_category_create() - get_language()=", get_language()
  END IF

  # CALL the INPUT
  CALL str_category_input(l_str_category.*)
    RETURNING l_str_category.*

  CALL fgl_window_close("w_str_category")

  IF NOT int_flag THEN
    INSERT INTO qxt_str_category VALUES (
      l_str_category.category_id,
      l_str_category.language_id,
      l_str_category.category_data
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_str_category.category_id, l_str_category.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]
  
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION str_category_edit(p_category_id)
#
# Edit str_category record
#
# RETURN NONE
#################################################
FUNCTION str_category_edit(p_category_id,p_language_id)
  DEFINE 
    p_category_id            LIKE qxt_str_category.category_id,
    p_language_id                 LIKE qxt_str_category.language_id,
    l_key1_category_id       LIKE qxt_str_category.category_id,
    l_key2_language_id            LIKE qxt_str_category.language_id,

    l_str_category_rec           RECORD LIKE qxt_str_category.*,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "str_category_edit() - Function Entry Point"
    DISPLAY "str_category_edit() - p_category_id=", p_category_id
    DISPLAY "str_category_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_category_id = p_category_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_str_category_rec(p_category_id,p_language_id) RETURNING l_str_category_rec.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_g"), TRUE) 
    CALL populate_str_category_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_t"), TRUE) 
    CALL populate_str_category_form_edit_labels_t()
  END IF


  #Call the INPUT
  CALL str_category_input(l_str_category_rec.*) 
    RETURNING l_str_category_rec.*

  CALL fgl_window_close("w_str_category")

  IF local_debug THEN
    DISPLAY "str_category_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF


  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "str_category_edit() - l_str_category_rec.category_id=",l_str_category_rec.category_id
      DISPLAY "str_category_edit() - l_str_category_rec.language_id=",l_str_category_rec.language_id
      DISPLAY "str_category_edit() - l_str_category_rec.category_data=",l_str_category_rec.category_data
      DISPLAY "str_category_edit() - l_key1_category_id=",l_key1_category_id
      DISPLAY "str_category_edit() - l_key2_language_id=",l_key2_language_id

    END IF

    UPDATE qxt_str_category
      SET 
          #category_id   = l_str_category_rec.category_id,
          language_id   = l_str_category_rec.language_id,
          category_data = l_str_category_rec.category_data
      WHERE category_id = l_key1_category_id
        AND language_id = l_key2_language_id


    IF local_debug THEN
      DISPLAY "str_category_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(839),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(839),NULL)
      RETURN l_str_category_rec.category_id, l_str_category_rec.language_id, l_str_category_rec.category_data 
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(839),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION str_category_input(p_str_category_rec)
#
# Input str_category details (edit/create)
#
# RETURN l_str_category.*
#################################################
FUNCTION str_category_input(p_str_category_rec)
  DEFINE 
    p_str_category_rec            RECORD LIKE qxt_str_category.*,
    l_str_category_form_rec       OF t_qxt_str_category_form_rec,
    l_orignal_category_id     LIKE qxt_str_category.category_id,
    l_orignal_language_id          LIKE qxt_str_category.language_id,
    local_debug                    SMALLINT,
    tmp_str                        VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "str_category_input() - p_str_category_rec.category_id=",p_str_category_rec.category_id
    DISPLAY "str_category_input() - p_str_category_rec.language_id=",p_str_category_rec.language_id
    DISPLAY "str_category_input() - p_str_category_rec.category_data=",p_str_category_rec.category_data
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_category_id = p_str_category_rec.category_id
  LET l_orignal_language_id = p_str_category_rec.language_id

  #copy record data to form_record format record
  CALL copy_str_category_record_to_form_record(p_str_category_rec.*) RETURNING l_str_category_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_str_category_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      NEXT FIELD category_data

    AFTER FIELD category_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(836), l_str_category_form_rec.category_id,NULL,TRUE)  THEN
        NEXT FIELD category_id
      END IF

    AFTER FIELD language_name
      #language_name must not be empty / NULL 
      IF NOT validate_field_value_string_exists(get_str_tool(837), l_str_category_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD category_id
      END IF

    #AFTER FIELD category_data
    #  #language_id must not be empty
    #  IF NOT validate_field_value_string_exists(get_str_tool(838), l_str_category_form_rec.category_data,NULL,TRUE)  THEN
    #    NEXT FIELD category_data
    #  END IF




    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #category_id must not be empty
        IF NOT validate_field_value_numeric_exists(get_str_tool(836), l_str_category_form_rec.category_id,NULL,TRUE)  THEN
          NEXT FIELD category_id
          CONTINUE INPUT
        END IF

      #language_name must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(837), l_str_category_form_rec.language_name,NULL,TRUE)  THEN
          NEXT FIELD category_id
          CONTINUE INPUT
        END IF

      #category_data must not be empty
      #  IF NOT validate_field_value_string_exists(get_str_tool(838), l_str_category_form_rec.category_data,NULL,TRUE)  THEN
      #
      #    NEXT FIELD category_data
      #    CONTINUE INPUT
      #  END IF


      #The category_id and language_id must be unique - Double Primary KEY
      IF category_id_and_language_id_count(l_str_category_form_rec.category_id,get_language_id(l_str_category_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
 
       IF NOT (l_orignal_category_id = l_str_category_form_rec.category_id AND l_orignal_language_id = get_language_id(l_str_category_form_rec.language_name) )THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_doublekey(get_str_tool(836),get_str_tool(837),NULL)
          NEXT FIELD category_id
          CONTINUE INPUT
        END IF
      END IF




  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "str_category_input() - l_str_category_form_rec.category_id=",l_str_category_form_rec.category_id
    DISPLAY "str_category_input() - l_str_category_form_rec.language_name=",l_str_category_form_rec.language_name
    DISPLAY "str_category_input() - l_str_category_form_rec.category_data=",l_str_category_form_rec.category_data
  END IF


  #Copy the form record data to a normal str_category record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_str_category_form_record_to_record(l_str_category_form_rec.*) RETURNING p_str_category_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "str_category_input() - p_str_category_rec.category_id=",p_str_category_rec.category_id
    DISPLAY "str_category_input() - p_str_category_rec.language_id=",p_str_category_rec.language_id
    DISPLAY "str_category_input() - p_str_category_rec.category_data=",p_str_category_rec.category_data
  END IF

  RETURN p_str_category_rec.*

END FUNCTION


#################################################
# FUNCTION str_category_delete(p_category_id)
#
# Delete a str_category record
#
# RETURN NONE
#################################################
FUNCTION str_category_delete(p_category_id,p_language_id)
  DEFINE 
    p_category_id       LIKE qxt_str_category.category_id,
    p_language_id            LIKE qxt_str_category.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(839), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_str_category 
        WHERE category_id = p_category_id
        AND language_id = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION str_category_view(p_category_id)
#
# View str_category record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION str_category_view(p_category_id)
  DEFINE 
    p_category_id             LIKE qxt_str_category.category_id,
    l_str_category_rec    RECORD LIKE qxt_str_category.*


  CALL get_str_category_rec(p_category_id,get_language()) RETURNING l_str_category_rec.*
  CALL str_category_view_by_rec(l_str_category_rec.*)

END FUNCTION


#################################################
# FUNCTION str_category_view_by_rec(p_str_category_rec)
#
# View str_category record in window-form
#
# RETURN NONE
#################################################
FUNCTION str_category_view_by_rec(p_str_category_rec)
  DEFINE 
    p_str_category_rec     RECORD LIKE qxt_str_category.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_g"), TRUE) 
    CALL populate_str_category_form_view_labels_g()
  ELSE
    CALL fgl_window_open("w_str_category", 3, 3, get_form_path("f_qxt_str_category_det_t"), TRUE) 
    CALL populate_str_category_form_view_labels_t()
  END IF

  DISPLAY BY NAME p_str_category_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_str_category")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################


####################################################
# FUNCTION category_id_and_language_id_count(p_category_id, p_language_id)
#
# tests if a record with this category_id and language_id already exists
#
# RETURN r_count
####################################################
FUNCTION category_id_and_language_id_count(p_category_id, p_language_id)
  DEFINE
    p_category_id        LIKE qxt_str_category.category_id,
    p_language_id             LIKE qxt_str_category.language_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_str_category
      WHERE qxt_str_category.category_id = p_category_id
        AND qxt_str_category.language_id = p_language_id
  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION category_data_count(p_category_data)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION category_data_count(p_category_data)
  DEFINE
    p_category_data  LIKE qxt_str_category.category_data,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_str_category
      WHERE qxt_str_category.category_data = p_category_data

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION category_id_count(p_category_id)
#
# tests if a record with this category_id already exists
#
# RETURN r_count
####################################################
FUNCTION category_id_count(p_category_id)
  DEFINE
    p_category_id    LIKE qxt_str_category.category_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_str_category
      WHERE qxt_str_category.category_id = p_category_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_str_category_record_to_form_record(p_str_category_rec)  
#
# Copy normal str_category record data to type str_category_form_rec
#
# RETURN l_str_category_form_rec.*
######################################################
FUNCTION copy_str_category_record_to_form_record(p_str_category_rec)  
  DEFINE
    p_str_category_rec       RECORD LIKE qxt_str_category.*,
    l_str_category_form_rec  OF t_qxt_str_category_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_str_category_form_rec.category_id   = p_str_category_rec.category_id
  LET l_str_category_form_rec.language_name      = get_language_name(p_str_category_rec.language_id)
  LET l_str_category_form_rec.category_data = p_str_category_rec.category_data

  IF local_debug THEN
    DISPLAY "copy_str_category_record_to_form_record() - p_str_category_rec.category_id=",   p_str_category_rec.category_id
    DISPLAY "copy_str_category_record_to_form_record() - p_str_category_rec.language_id=",        p_str_category_rec.language_id
    DISPLAY "copy_str_category_record_to_form_record() - p_str_category_rec.category_data=", p_str_category_rec.category_data

    DISPLAY "copy_str_category_record_to_form_record() - l_str_category_form_rec.category_id=", l_str_category_form_rec.category_id
    DISPLAY "copy_str_category_record_to_form_record() - l_str_category_form_rec.category_id=", l_str_category_form_rec.language_name
    DISPLAY "copy_str_category_record_to_form_record() - l_str_category_form_rec.category_id=", l_str_category_form_rec.category_data
  END IF

  RETURN l_str_category_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_str_category_form_record_to_record(p_str_category_form_rec)  
#
# Copy type str_category_form_rec to normal str_category record data
#
# RETURN l_str_category_rec.*
######################################################
FUNCTION copy_str_category_form_record_to_record(p_str_category_form_rec)  
  DEFINE
    l_str_category_rec       RECORD LIKE qxt_str_category.*,
    p_str_category_form_rec  OF t_qxt_str_category_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_str_category_rec.category_id   = p_str_category_form_rec.category_id
  LET l_str_category_rec.language_id        = get_language_id(p_str_category_form_rec.language_name)
  LET l_str_category_rec.category_data = p_str_category_form_rec.category_data

  IF local_debug THEN
    DISPLAY "copy_str_category_form_record_to_record() - p_str_category_form_rec.category_id=",   p_str_category_form_rec.category_id
    DISPLAY "copy_str_category_form_record_to_record() - p_str_category_form_rec.language_id=",        p_str_category_form_rec.language_name
    DISPLAY "copy_str_category_form_record_to_record() - p_str_category_form_rec.category_data=", p_str_category_form_rec.category_data

    DISPLAY "copy_str_category_form_record_to_record() - l_str_category_rec.category_id=", l_str_category_rec.category_id
    DISPLAY "copy_str_category_form_record_to_record() - l_str_category_rec.category_id=", l_str_category_rec.language_id
    DISPLAY "copy_str_category_form_record_to_record() - l_str_category_rec.category_id=", l_str_category_rec.category_data
  END IF


  RETURN l_str_category_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_str_category_scroll()
#
# Populate str_category grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_str_category_scroll()
  CALL fgl_grid_header("sc_str_category","category_id",  get_str_tool(836),"center","F13")  --str_category
  CALL fgl_grid_header("sc_str_category","language_name",     get_str_tool(837),"left", "F14")  --file language_id
  CALL fgl_grid_header("sc_str_category","category_data",get_str_tool(838),"left","F15")  --category_data

END FUNCTION



#######################################################
# FUNCTION populate_str_category_form_labels_g()
#
# Populate str_category form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_labels_g()

  CALL fgl_settitle(get_str_tool(839))

  DISPLAY get_str_tool(839) TO lbTitle

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_str_category_form_labels_t()
#
# Populate str_category form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_labels_t()

  DISPLAY get_str_tool(839) TO lbTitle

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_str_category_form_edit_labels_g()
#
# Populate str_category form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(839)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_name")



END FUNCTION



#######################################################
# FUNCTION populate_str_category_form_edit_labels_t()
#
# Populate str_category form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_edit_labels_t()

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_str_category_form_view_labels_g()
#
# Populate str_category form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(839)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_str_category_form_view_labels_t()
#
# Populate str_category form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_view_labels_t()

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_language_id")

END FUNCTION


#######################################################
# FUNCTION populate_str_category_form_create_labels_g()
#
# Populate str_category form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(839)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_str_category_form_create_labels_t()
#
# Populate str_category form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_form_create_labels_t()

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_str_category_list_form_labels_g()
#
# Populate str_category list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(839)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_str_category_scroll()
  CALL updateUILabel("language_filter","Filter")
  #DISPLAY get_str_tool(836) TO dl_f1
  #DISPLAY get_str_tool(837) TO dl_f2
  #DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  #DISPLAY "!" TO language_filter
  #DISPLAY "!" TO bt_set_filter_criteria

  CALL language_combo_list("combo_lang")

END FUNCTION


#######################################################
# FUNCTION populate_str_category_list_form_labels_t()
#
# Populate str_category list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_str_category_list_form_labels_t()

  DISPLAY trim(get_str_tool(839)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################






