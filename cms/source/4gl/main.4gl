##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# MAIN module of the CMS demo application
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                      RETURN
# MAIN                                        MAIN   (inc. main ring menu)                      NONE
# add_report_types()                          Prepares the different report formats             NONE
# check_cms_db_version()                      Validates the CMS database to ensure the version  NONE
# db_error_information(err_msg, l_cms_info)   displays cms database error information (version) NONE
#
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# MAIN
#
# NONE
######################################################
MAIN
  DEFINE 
    a_config_filename  STRING,
    logoff_txt         STRING,
    value              LIKE operator.operator_id,
    menu_refresh       SMALLINT,
    menu_str_arr1      DYNAMIC ARRAY OF STRING, --i.e. used for rinmenu strings
    menu_str_arr2      DYNAMIC ARRAY OF STRING, --i.e. used for rinmenu strings
    l_language         LIKE qxt_language.language_id


	#read arguments, config files and initialise
	CALL prepareModuleStart()
	CALL prepareModuleStartExtended()
	
	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)

  # set help file '1' define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(1)
  
{
 CALL setEnvAndOptions(1)


  #######################################################################################
  # QXT Library Init Section
  #######################################################################################
  ##################################################################
  # Init Main QXT Configuration CFG
  # Can be specified as a program call argument
  ################################################################## 

  LET a_config_filename = ARG_VAL(1)
  LET a_config_filename = trim(a_config_filename)

  IF a_config_filename IS NULL THEN
     CALL set_main_cfg_filename("cfg/cms.cfg")
  ELSE
     CALL set_main_cfg_filename(a_config_filename)
  END IF

  CALL init_data()  --initialise some variables and the QXT libraries

  # set help file '1' define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(1)

  CALL check_cms_db_version()  --check if it exists and if it is the correct version
  CALL add_report_types()    -- add three types of reports (screen, print text, html)

#  IF fgl_fglgui() THEN -- gui
    CALL publish_toolbar("Global",0)
    CALL fgl_settitle("CMS-Demo Lycia 2 Application by Querix")
#  ELSE  --text client

#  END IF


  LET menu_refresh = FALSE



  WHILE TRUE


    IF menu_refresh THEN  --no re-login if we only want a menu refresh... this is required if the user changes the language
      LET menu_refresh = FALSE
    ELSE
      CLEAR SCREEN

      IF NOT login() THEN
        EXIT PROGRAM
      END IF

      LET logoff_txt = "Logoff operator ", g_operator.name CLIPPED

      SELECT operator_id
        INTO value
        FROM operator
        WHERE operator.name = g_operator.name

      LET current_operator_id = value

    END IF
}
    MENU "Main"
      BEFORE MENU
        IF NOT admin_operator(g_operator.name) THEN
          HIDE OPTION main_menu[6].option_name
        END IF

        CALL load_main_menu() -- main menu uses strings from a different db table (for demo/testing purpose)


        CALL set_help_id(1)
        CALL publish_toolbar("CmsMain",0)
        LET l_language = get_language()


			CALL ui.Interface.setImage("qx://application/icon16/business/vendor.png")
			CALL ui.Interface.setText("CMS ALL")


      COMMAND KEY (F101) main_menu[1].option_name main_menu[1].option_comment  HELP 20 --company
        CALL company_main(NULL)
        CLEAR SCREEN

      COMMAND KEY (F102) main_menu[2].option_name main_menu[2].option_comment  HELP 21  --contact
	CALL contact_main(value,NULL)
	CLEAR SCREEN

      COMMAND KEY (F103) main_menu[3].option_name main_menu[3].option_comment  HELP 22
        CALL mailbox(value)
        CLEAR SCREEN

      COMMAND KEY (F104) main_menu[4].option_name main_menu[4].option_comment  HELP 23
        CALL activity_main(value)
        CLEAR SCREEN

      COMMAND KEY (F105) main_menu[5].option_name main_menu[5].option_comment  HELP 24
        CALL invoice_main()
        CLEAR SCREEN

      COMMAND KEY ("S") main_menu[13].option_name main_menu[13].option_comment HELP 29
        CALL supplies_main()
        CLEAR SCREEN

      COMMAND KEY (F106) main_menu[7].option_name main_menu[7].option_comment  HELP 25
        CALL report_main(FALSE)

      #COMMAND KEY (F107) main_menu[6].option_name main_menu[6].option_comment  HELP 26
        #CALL admin_main()

      #COMMAND KEY (F108) main_menu[8].option_name main_menu[8].option_comment  HELP 27
        #CALL operator_setup()
	#CLEAR SCREEN

      COMMAND KEY (F108) main_menu[8].option_name main_menu[8].option_comment  HELP 27
        CALL settings_menu(30)
        LET menu_refresh = TRUE  --otherwise, Login appears again
        EXIT MENU --need to refresh menu for possible language changes

      COMMAND KEY (F109) main_menu[9].option_name main_menu[9].option_comment  HELP 28  --logout
				CALL disable_current_session_record()
        LET menu_refresh = FALSE  --otherwise, Login appears again
        CLEAR SCREEN
        EXIT MENU

      COMMAND KEY (F12,F9) main_menu[10].option_name main_menu[10].option_comment  HELP 51
        CALL publish_toolbar("CmsMain",1)
        CLEAR SCREEN
        EXIT MENU --WHILE

    	ON ACTION ("actExit") 
        CALL publish_toolbar("CmsMain",1)
        CLEAR SCREEN
      	EXIT MENU

      COMMAND KEY(HELP)  main_menu[11].option_name main_menu[11].option_comment  HELP 1
        #"Help" "Classic 4GL Text Help window"
        CALL showhelp(1)

      COMMAND KEY(F1024)  main_menu[12].option_name main_menu[12].option_comment  HELP 1
        #"Html-Help" "Modern 4GL HTML Help Window"
        CALL html_help(get_help_url(1))
 

    END MENU


#  END WHILE

  #Clean applications temporary file on the server
  CALL clean_app_server_temp_files()

END MAIN
{
###########################################################
# FUNCTION add_report_types()
#
# RETURN NONE
###########################################################
FUNCTION add_report_types()
  CALL q4gl_add_user_report_type("printer","print")
  CALL q4gl_add_user_report_type("screen","text")
  CALL q4gl_add_user_report_type("browser","html")
END FUNCTION


}
{
###########################################################
# FUNCTION  check_cms_db_version()
#
# RETURN NONE
###########################################################
FUNCTION  check_cms_db_version()
  DEFINE 
    db_name VARCHAR(40),
    retval SMALLINT,
    db_state_str,err_msg VARCHAR(100),
    cms_db_state SMALLINT,
    l_cms_info OF t_cms_info 
  LET db_name = "cms"

  #Try to connect to database
  WHENEVER ERROR CONTINUE
    CALL open_db(db_name) RETURNING retval, db_state_str
  WHENEVER ERROR STOP

  IF retval < 0 THEN

    DISPLAY db_state_str TO dl_db_state ATTRIBUTE(RED)
    CALL fgl_winmessage("Database Error","Cannot connect to database CMS\nYou need to create the database named 'cms' before you can continue\nThe application will now close","error")
    #EXIT PROGRAM
  ELSE

    #if connection successful, try to read the cms database version & build information
    CALL db_info("cms") RETURNING cms_db_state, l_cms_info.*

    CASE cms_db_state
      WHEN -1
        LET err_msg = "Your CMS database seems to be incomplete or outdated - please reinstall the db"
        CALL db_error_information(err_msg, l_cms_info.*)
        EXIT PROGRAM
      WHEN -2
        LET err_msg = "Your CMS database seems to be empty (no tables or too old) - please reinstall the database"
        CALL db_error_information(err_msg, l_cms_info.*)
        EXIT PROGRAM
      WHEN 0
        #LET err_msg = "Your CMS database seems to be OK"
        #CALL db_error_information(err_msg, l_cms_info.*)
    END CASE
  END IF

END FUNCTION

}
{
###########################################################
# FUNCTION db_error_information(err_msg, l_cms_info)
#
# RETURN NONE
###########################################################
FUNCTION db_error_information(err_msg, l_cms_info)
  DEFINE 
    err_msg VARCHAR(300),
    l_cms_info OF t_cms_info 


  LET err_msg = trim(err_msg) , "\n", "Current CMS DB Version:", l_cms_info.db_version, " Build:", l_cms_info.db_build, "\n", "Required CMS DB Version:",  get_required_cms_version()
  CALL fgl_winmessage("Error in DMS Database",err_msg,"error")

END FUNCTION

}

###Web service test


