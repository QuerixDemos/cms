##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the string category manager
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


GLOBALS

#icon_category TYPE
  DEFINE t_qxt_str_category_rec TYPE AS
    RECORD
      category_id      LIKE qxt_str_category.category_id,
      language_id      LIKE qxt_str_category.language_id,
      category_data    LIKE qxt_str_category.category_data
    END RECORD

  DEFINE t_qxt_str_category_form_rec TYPE AS
    RECORD
      category_id      LIKE qxt_str_category.category_id,
      language_name    LIKE qxt_language.language_name,
      category_data    LIKE qxt_str_category.category_data

    END RECORD


END GLOBALS





