##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage acounts
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                          RETURN:
# get_account_credit_balance(p_account_id)    Retrieve the credit balance for the account           credit_limit - t_inv_balance 
# get_account_balance(p_account_id)           Get the account balance of an account                 l_total_balance
# get_account_rec(p_account_id)               Get the account record                                l_account.* (LIKE account.*)
# get_form_account(p_account_id)              ????                                                  l_account_rec.*  (cust record)
# account_input(p_account)                    input form for new & edit account                     p_account.*
# account_edit(p_account_id)                  Edit account details                                  NONE
# account_create()                            Create a new account                                  l_account.account_id                                
# account_delete(p_account_id)                Delete account record                                 NONE
# account_view(p_account_id)                  View account details in window                        NONE
# account_view_by_rec(p_account_rec)          Display account details on form using record argument NONE
# account_popup_data_source(p_order_field)    Data Source (Cursor) for the account_popup function   NONE
# account_popup(p_account_id,p_order_field)   Display accounts list to select and return id         l_account_arr[i].account_id  (or p_account_id for Cancel)
# account_to_form_account(p_account)          ????                                                  l_account_rec.*
# account_display(p_account_id)               DISPLAY account record                                NONE
# form_account_to_account(p_account_rec)      ????                                                  l_account.*
# disp_account_comp(p_comp_id)                Display extracted company details (shorter version)   l_comp_rec.*
# grid_header_f_account_scroll_g()            Populate grid header details                          NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



############################################################################################################
# Record Edit/Create/Delete/Input functions
############################################################################################################

###################################################
# FUNCTION account_delete(p_account_id)
#
# Delete account record
#
# RETURN NONE
###################################################
FUNCTION account_delete(p_account_id)
  DEFINE p_account_id LIKE account.account_id

  #"Are you sure you want to delete this account ?"
  IF yes_no(get_str(817),get_str(1150)) THEN
    DELETE FROM account 
      WHERE account.account_id = p_account_id
  END IF

END FUNCTION


###################################################
# FUNCTION account_input(p_account)
#
# input form for new & edit account
#
# RETURN p_account.*
###################################################
FUNCTION account_input(p_account,p_edit_mode)
  DEFINE  
    p_account     RECORD LIKE account.*,
    l_account_rec OF t_acc_rec,
    l_comp_rec    OF t_company_short_rec,
    p_edit_mode		BOOLEAN
    
    IF NOT fgl_window_open("w_account_input", 2, 2,get_form_path("f_account_l2"),FALSE)  THEN
      CALL fgl_winmessage("Error","account_input()\nCannot open window w_account_input","error")
      RETURN p_account.*
    END IF

    CALL populate_account_form_input_labels_g()


  CALL account_to_form_account(p_account.*)
    RETURNING l_account_rec.*

  DISPLAY p_account.comp_id TO comp_id

  INPUT BY NAME l_account_rec.* WITHOUT DEFAULTS
    BEFORE INPUT
      CALL publish_toolbar("AccountEdit",0)
    
    BEFORE FIELD comp_name
      IF NOT exist(p_account.comp_id) THEN  --empty company_id field pops-up company query/search window
        CALL company_query()  
          RETURNING p_account.comp_id

        CALL disp_account_comp(p_account.comp_id)
          RETURNING l_comp_rec.*

        LET l_account_rec.comp_name    = l_comp_rec.comp_name 
        LET l_account_rec.comp_addr1   = l_comp_rec.comp_addr1 
        LET l_account_rec.comp_addr2   = l_comp_rec.comp_addr2 
        LET l_account_rec.comp_city    = l_comp_rec.comp_city 
        LET l_account_rec.comp_zone    = l_comp_rec.comp_zone 
        LET l_account_rec.comp_zip     = l_comp_rec.comp_zip 
        LET l_account_rec.comp_country = l_comp_rec.comp_country 

      END IF

    ON KEY (F10)
      IF INFIELD (comp_name) THEN
        CALL company_query()
          RETURNING p_account.comp_id

        CALL disp_account_comp(p_account.comp_id)
          RETURNING l_comp_rec.*

        LET l_account_rec.comp_name    = l_comp_rec.comp_name 
        LET l_account_rec.comp_addr1   = l_comp_rec.comp_addr1 
        LET l_account_rec.comp_addr2   = l_comp_rec.comp_addr2 
        LET l_account_rec.comp_city    = l_comp_rec.comp_city 
        LET l_account_rec.comp_zone    = l_comp_rec.comp_zone 
        LET l_account_rec.comp_zip     = l_comp_rec.comp_zip 
        LET l_account_rec.comp_country = l_comp_rec.comp_country

      END IF

    AFTER INPUT
      IF NOT int_flag THEN
      	IF p_edit_mode = FALSE THEN --new record creation
					IF account_count(p_account.account_id) THEN
          	#The company_id field must not be empty !
          	CALL fgl_winmessage("Duplicate","An account with this id already exists!","error")
          	CONTINUE INPUT						
					END IF

					IF account_company_count(p_account.comp_id) THEN
          	#The company_id field must not be empty !
          	CALL fgl_winmessage("Duplicate","An account for this company\n(with this id)\n already exists!","error")
          	CONTINUE INPUT						
					END IF



				END IF
      
        IF p_account.comp_id  IS NULL THEN
          #The company_id field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(162),"error")
          CONTINUE INPUT
        END IF
        IF l_account_rec.account_id  IS NULL THEN
          #The 'Account ID' field must not be empty !!
          CALL fgl_winmessage(get_str(48),get_str(1462),"error")
          CONTINUE INPUT
        END IF
        IF l_account_rec.credit_limit  IS NULL THEN
          #The credit_limit field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1161),"error")
          CONTINUE INPUT
        END IF
        IF l_account_rec.discount  IS NULL THEN
          #The discount field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1162),"error")
          CONTINUE INPUT
        END IF
      ELSE
        IF NOT yes_no(get_str(1163),get_str(1164)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF

  #Possible toolbar clean up
  CALL publish_toolbar("AccountEdit",1)

  END INPUT

  CALL fgl_window_close("w_account_input")

  LET p_account.credit_limit = l_account_rec.credit_limit
  LET p_account.discount = l_account_rec.discount

  RETURN p_account.*
END FUNCTION


###################################################
# FUNCTION account_view(account_view_by_rec)
#
# Display account details on a form using record argument
#
# RETURN NONE
###################################################
FUNCTION account_view_by_rec(p_account_rec)
  DEFINE  
    p_account_rec RECORD LIKE account.*,
    inp_char      CHAR
    
    IF NOT fgl_window_open("w_account_view", 2, 2, get_form_path("f_account_l2"),FALSE)  THEN
      CALL fgl_winmessage("Error","account_view_by_rec()\nCannot open window w_account_view","error")
      RETURN p_account_rec.*
    END IF
    CALL populate_account_form_view_labels_g()


  DISPLAY BY NAME p_account_rec.* 

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
      EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_account_view")

END FUNCTION


###################################################
# FUNCTION disp_account_comp(p_comp_id)
#
# Display extracted company details (shorter version)
#
# RETURN l_comp_rec
###################################################
FUNCTION disp_account_comp(p_comp_id)
  DEFINE 
    p_comp_id LIKE company.comp_id,
    l_comp_rec OF t_company_short_rec


  SELECT company.comp_id,
         company.comp_name,
         company.comp_addr1,
         company.comp_addr2,
         company.comp_city,
         company.comp_zone,
         company.comp_zip,
         company.comp_country
    INTO l_comp_rec.*
    FROM company
    WHERE company.comp_id = p_comp_id

  DISPLAY BY NAME l_comp_rec.*
  RETURN l_comp_rec.*
END FUNCTION


###################################################
# FUNCTION account_create() 
#
# RETURN l_account.account_id
#
# Create a new account
###################################################
FUNCTION account_create() 
  DEFINE l_account RECORD LIKE account.*
  
  LET l_account.account_id = 0
  LET l_account.discount = 0.00
  LET l_account.credit_limit = 0.00

  CALL account_input(l_account.*,FALSE)  --2nd argument is edit_mode
    RETURNING l_account.*

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  ELSE
    #DISPLAY "account_count(l_account.account_id)=", account_count(l_account.account_id)
    IF account_count(l_account.account_id) = 0 THEN  --check if an account with this company exists
      INSERT INTO account VALUES (l_account.*)
      LET l_account.account_id = sqlca.sqlerrd[2]
    ELSE
      CALL fgl_winmessage("Error","Account for this company already exists","error")
      RETURN 0
    END IF
  END IF
  
  RETURN l_account.account_id
END FUNCTION


###################################################
# FUNCTION account_edit(p_account_id)
#
# Edit account details
#
# RETURN NONE
###################################################
FUNCTION account_edit(p_account_id)
  DEFINE p_account_id LIKE account.account_id
  DEFINE l_account RECORD LIKE account.*

  CALL get_account_rec(p_account_id)
    RETURNING l_account.*

  CALL account_input(l_account.*,TRUE)  --2nd argument is edit_mode
    RETURNING l_account.*

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  ELSE
    UPDATE account
      SET comp_id = l_account.comp_id,
          credit_limit = l_account.credit_limit,
          discount = l_account.discount
      WHERE account.account_id = l_account.account_id
    RETURN l_account.account_id
  END IF
END FUNCTION


###################################################
# FUNCTION account_view(p_account_id)
#
# View account details
#
# RETURN NONE
###################################################
FUNCTION account_view(p_account_id)
  DEFINE 
    p_account_id LIKE account.account_id,
    l_account RECORD LIKE account.*,
    inp_char CHAR    

  CALL get_account_rec(p_account_id)
    RETURNING l_account.*

  CALL account_view_by_rec(l_account.*)


END FUNCTION


###################################################
# FUNCTION account_display(p_account_id)
#
# DISPLAY account record
#
# RETURN NONE
###################################################
FUNCTION account_display(p_account_id)
  DEFINE 
    p_account_id LIKE account.account_id,
    l_account_rec OF t_acc_rec
    
  CALL get_form_account(p_account_id)
    RETURNING l_account_rec.*


  DISPLAY get_comp_id(l_account_rec.comp_name) TO comp_id
  DISPLAY BY NAME l_account_rec.*
END FUNCTION








