##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS

  DEFINE
    qxt_data_language_id   LIKE qxt_language.language_id   --is required, if the user works in a different language but want to view 'another' language related data


END GLOBALS


