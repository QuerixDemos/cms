##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_ind_type_scroll()
#
# Populates grid header labels
#
# RETURN NONE
####################################################
FUNCTION grid_header_ind_type_scroll()
  CALL fgl_grid_header("sa_ind_type","type_id",get_str(1781),"right","F13")    --Type ID
  CALL fgl_grid_header("sa_ind_type","itype_name",get_str(1782),"left","F14")  --"Ind. Type Name:"
  CALL fgl_grid_header("sa_ind_type","user_def",get_str(1783),"left","F15")    --"User Def.:"
END FUNCTION


#######################################################
# FUNCTION populate_contact_form_labels_t()
#
# Populate industry type details form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_industry_type_form_labels_t()
  DISPLAY get_str(1770) TO lbTitle
  DISPLAY get_str(1771) TO dl_f1
  #DISPLAY get_str(1772) TO dl_f2
  #DISPLAY get_str(1773) TO dl_f3
  #DISPLAY get_str(1774) TO dl_f4
  #DISPLAY get_str(1775) TO dl_f5
  #DISPLAY get_str(1776) TO dl_f6
  #DISPLAY get_str(1777) TO dl_f7
  #DISPLAY get_str(1778) TO dl_f8 
  DISPLAY get_str(1779) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_contact_form_labels_g()
#
# Populate industry type details form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_industry_type_form_labels_g()
  DISPLAY get_str(1770) TO lbTitle
  DISPLAY get_str(1771) TO dl_f1
  #DISPLAY get_str(1772) TO dl_f2
  #DISPLAY get_str(1773) TO dl_f3
  #DISPLAY get_str(1774) TO dl_f4
  #DISPLAY get_str(1775) TO dl_f5
  #DISPLAY get_str(1776) TO dl_f6
  #DISPLAY get_str(1777) TO dl_f7
  #DISPLAY get_str(1778) TO dl_f8 
  DISPLAY get_str(1779) TO lbInfo1


  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(151))  --"Industry Type List")

END FUNCTION

#######################################################
# FUNCTION populate_industry_type_scroll_form_labels_t()
#
# Populate industry type details form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_industry_type_scroll_form_labels_t()
  DISPLAY get_str(1780) TO lbTitle
  DISPLAY get_str(1781) TO dl_f1
  DISPLAY get_str(1782) TO dl_f2
  DISPLAY get_str(1783) TO dl_f3
  #DISPLAY get_str(1784) TO dl_f4
  #DISPLAY get_str(1785) TO dl_f5
  #DISPLAY get_str(1786) TO dl_f6
  #DISPLAY get_str(1787) TO dl_f7
  #DISPLAY get_str(1788) TO dl_f8 
  DISPLAY get_str(1789) TO lbInfo1

END FUNCTION 


#######################################################
# FUNCTION populate_industry_type_scroll_form_labels_g()
#
# Populate industry type details form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_industry_type_scroll_form_labels_g()
  DISPLAY get_str(1780) TO lbTitle
  #DISPLAY get_str(1781) TO dl_f1
  #DISPLAY get_str(1782) TO dl_f2
  #DISPLAY get_str(1783) TO dl_f3
  #DISPLAY get_str(1784) TO dl_f4
  #DISPLAY get_str(1785) TO dl_f5
  #DISPLAY get_str(1786) TO dl_f6
  #DISPLAY get_str(1787) TO dl_f7
  #DISPLAY get_str(1788) TO dl_f8 
  DISPLAY get_str(1789) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1780))
  CALL grid_header_ind_type_scroll()

END FUNCTION

