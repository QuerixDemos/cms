##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tbi_tooltip table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_string_id(p_language_id)  get tbi_tooltip id from p_language_id        l_string_id
# get_language_id(string_id)    Get language_id from p_string_id        l_language_id
# get_tbi_tooltip_rec(p_string_id)   Get the tbi_tooltip record from p_string_id  l_tbi_tooltip.*
# tbi_tooltip_popup_data_source()               Data Source (cursor) for tbi_tooltip_popup              NONE
# tbi_tooltip_popup                             tbi_tooltip selection window                            p_string_id
# (p_string_id,p_order_field,p_accept_action)
# tbi_tooltip_combo_list(cb_field_name)         Populates tbi_tooltip combo list from db                NONE
# tbi_tooltip_create()                          Create a new tbi_tooltip record                         NULL
# tbi_tooltip_edit(p_string_id)      Edit tbi_tooltip record                                 NONE
# tbi_tooltip_input(p_tbi_tooltip_rec)    Input tbi_tooltip details (edit/create)                 l_tbi_tooltip.*
# tbi_tooltip_delete(p_string_id)    Delete a tbi_tooltip record                             NONE
# tbi_tooltip_view(p_string_id)      View tbi_tooltip record by ID in window-form            NONE
# tbi_tooltip_view_by_rec(p_tbi_tooltip_rec) View tbi_tooltip record in window-form               NONE
# get_string_id_from_language_id(p_language_id)          Get the string_id from a file                      l_string_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# string_id_count(p_string_id)  tests if a record with this string_id already exists r_count
# copy_tbi_tooltip_record_to_form_record        Copy normal tbi_tooltip record data to type tbi_tooltip_form_rec   l_tbi_tooltip_form_rec.*
# (p_tbi_tooltip_rec)
# copy_tbi_tooltip_form_record_to_record        Copy type tbi_tooltip_form_rec to normal tbi_tooltip record data   l_tbi_tooltip_rec.*
# (p_tbi_tooltip_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_tbi_tooltip_scroll()              Populate tbi_tooltip grid headers                       NONE
# populate_tbi_tooltip_form_labels_g()          Populate tbi_tooltip form labels for gui                NONE
# populate_tbi_tooltip_form_labels_t()          Populate tbi_tooltip form labels for text               NONE
# populate_tbi_tooltip_form_edit_labels_g()     Populate tbi_tooltip form edit labels for gui           NONE
# populate_tbi_tooltip_form_edit_labels_t()     Populate tbi_tooltip form edit labels for text          NONE
# populate_tbi_tooltip_form_view_labels_g()     Populate tbi_tooltip form view labels for gui           NONE
# populate_tbi_tooltip_form_view_labels_t()     Populate tbi_tooltip form view labels for text          NONE
# populate_tbi_tooltip_form_create_labels_g()   Populate tbi_tooltip form create labels for gui         NONE
# populate_tbi_tooltip_form_create_labels_t()   Populate tbi_tooltip form create labels for text        NONE
# populate_tbi_tooltip_list_form_labels_g()     Populate tbi_tooltip list form labels for gui           NONE
# populate_tbi_tooltip_list_form_labels_t()     Populate tbi_tooltip list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


#########################################################
# FUNCTION get_tbi_label_data(p_string_id,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_tbi_label_data(p_string_id,p_language_id)
  DEFINE 
    p_string_id          LIKE qxt_tbi_tooltip.string_id,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_label_data       LIKE qxt_tbi_tooltip.label_data,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_label_data() - p_string_id = ", p_string_id
    DISPLAY "get_tbi_label_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_tbi_tooltip.label_data
    INTO l_label_data
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.string_id = p_string_id
      AND qxt_tbi_tooltip.language_id = p_language_id

  RETURN l_label_data
END FUNCTION

#########################################################
# FUNCTION get_tbi_tooltip_data(p_string_id,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_tbi_tooltip_data(p_string_id,p_language_id)
  DEFINE 
    p_string_id          LIKE qxt_tbi_tooltip.string_id,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_string_data        LIKE qxt_tbi_tooltip.string_data,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_tooltip_data() - p_string_id = ", p_string_id
    DISPLAY "get_tbi_tooltip_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_tbi_tooltip.string_data
    INTO l_string_data
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.string_id = p_string_id
      AND qxt_tbi_tooltip.language_id = p_language_id

  RETURN l_string_data
END FUNCTION

#########################################################
# get_tbi_tooltip_id(p_string_data,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_tbi_label_id(p_label_data,p_language_id)
  DEFINE 
    p_label_data         LIKE qxt_tbi_tooltip.label_data,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_string_id          LIKE qxt_tbi_tooltip.string_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_label_data() - p_string_data = ", p_label_data
    DISPLAY "get_tbi_label_data() - p_language_id = ", p_language_id
  END IF

  #Table entry 1 keeps an empty string
  IF p_label_data IS NULL THEN
    RETURN 1
  END IF

  SELECT qxt_tbi_tooltip.string_id
    INTO l_string_id
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.label_data = p_label_data
      AND qxt_tbi_tooltip.language_id = p_language_id

  IF local_debug THEN
    DISPLAY "get_tbi_label_data() - l_string_id = ", l_string_id
  END IF

  RETURN l_string_id
END FUNCTION

#########################################################
# get_tbi_tooltip_id(p_string_data,p_language_id)
#
# get string_id from p_language_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_tbi_tooltip_id(p_string_data,p_language_id)
  DEFINE 
    p_string_data        LIKE qxt_tbi_tooltip.string_data,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_string_id          LIKE qxt_tbi_tooltip.string_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_tooltip_data() - p_string_data = ", p_string_data
    DISPLAY "get_tbi_tooltip_data() - p_language_id = ", p_language_id
  END IF

  #Table entry 1 keeps an empty string
  IF p_string_data IS NULL THEN
    RETURN 1
  END IF

  SELECT qxt_tbi_tooltip.string_id
    INTO l_string_id
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.string_data = p_string_data
      AND qxt_tbi_tooltip.language_id = p_language_id

  IF local_debug THEN
    DISPLAY "get_tbi_tooltip_data() - l_string_id = ", l_string_id
  END IF

  RETURN l_string_id
END FUNCTION



#########################################################
# FUNCTION get_tbi_tooltip_rec(p_string_id,p_language_id)
#
# get string record from p_language_id and p_string_id
#
# RETURN l_string_id
#########################################################
FUNCTION get_tbi_tooltip_rec(p_string_id,p_language_id)
  DEFINE 
    p_string_id          LIKE qxt_tbi_tooltip.string_id,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_string_rec         RECORD LIKE qxt_tbi_tooltip.*,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_tooltip_data() - p_string_id = ", p_string_id
    DISPLAY "get_tbi_tooltip_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_tbi_tooltip.*
    INTO l_string_rec.*
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.string_id = p_string_id
      AND qxt_tbi_tooltip.language_id = p_language_id

  RETURN l_string_rec.*
END FUNCTION


###################################################################################
# FUNCTION get_tbi_tooltip_new_id(p_language_id)
#
# Finds MAX tbi_tooltip_id and Returns it +1  (SERIAL emulation) 
#
# RETURN NONE
###################################################################################
FUNCTION get_tbi_tooltip_new_id(p_language_id)
  DEFINE
    p_language_id            LIKE qxt_language.language_id,
    l_max_tbi_tooltip_id  LIKE qxt_tbi_tooltip.string_id

  SELECT  MAX (qxt_tbi_tooltip.string_id)
    INTO l_max_tbi_tooltip_id
    FROM qxt_tbi_tooltip
    WHERE qxt_tbi_tooltip.language_id = p_language_id

  RETURN l_max_tbi_tooltip_id + 1
END FUNCTION


########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION tbi_tooltip_data_combo_list(cb_field_name)
#
# Populates tbi_tooltip_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_tooltip_data_combo_list(p_cb_field_name, p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_tbi_tooltip.language_id,
    l_string_data             DYNAMIC ARRAY OF LIKE qxt_tbi_tooltip.string_data,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_tooltip_data_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "tbi_tooltip_data_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_tbi_tooltip_scroll2 CURSOR FOR 
    SELECT qxt_tbi_tooltip.string_data
      FROM qxt_tbi_tooltip
      WHERE qxt_tbi_tooltip.language_id = p_language_id
    ORDER BY string_data ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_tooltip_scroll2 INTO l_string_data[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_string_data[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_string_data[row_count] CLIPPED, ")"
      DISPLAY "tbi_tooltip_data_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION

###################################################################################
# FUNCTION tbi_tooltip_id_combo_list(cb_field_name)
#
# Populates tbi_tooltip_id_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_tooltip_id_combo_list(p_cb_field_name, p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_tbi_tooltip.language_id,
    l_string_id               DYNAMIC ARRAY OF LIKE qxt_tbi_tooltip.string_id,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_tooltip_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "tbi_tooltip_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_tbi_tooltip_id_scroll2 CURSOR FOR 
    SELECT qxt_tbi_tooltip.string_id
      FROM qxt_tbi_tooltip
      WHERE qxt_tbi_tooltip.language_id = p_language_id
    ORDER BY string_id ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_tooltip_id_scroll2 INTO l_string_id[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_string_id[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_string_id[row_count] CLIPPED, ")"
      DISPLAY "tbi_tooltip_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION



######################################################
# FUNCTION tbi_tooltip_popup_data_source()
#
# Data Source (cursor) for tbi_tooltip_popup
#
# RETURN NONE
######################################################
FUNCTION tbi_tooltip_popup_data_source(p_f_lang_switch, p_f_lang_data,p_cat_filter_switch,p_cat_search1,p_cat_search2,p_cat_search3,p_order_field,p_ord_dir)
  DEFINE 
    p_f_lang_switch     SMALLINT,
    p_f_lang_data       LIKE qxt_tbi_tooltip.language_id,
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT,
    p_cat_filter_switch  SMALLINT,
    p_cat_search1        LIKE qxt_str_category.category_id,
    p_cat_search2        LIKE qxt_str_category.category_id,
    p_cat_search3        LIKE qxt_str_category.category_id

  LET local_debug = FALSE

  IF local_Debug THEN
    DISPLAY "tbi_tooltip_popup_data_source() - p_f_lang_switch=",  p_f_lang_switch
    DISPLAY "tbi_tooltip_popup_data_source() - p_f_lang_data=",    p_f_lang_data

    DISPLAY "tbi_tooltip_popup_data_source() - p_cat_filter_switch=",    p_cat_filter_switch
    DISPLAY "tbi_tooltip_popup_data_source() - p_cat_search1=",    p_cat_search1
    DISPLAY "tbi_tooltip_popup_data_source() - p_cat_search2=",    p_cat_search2
    DISPLAY "tbi_tooltip_popup_data_source() - p_cat_search3=",    p_cat_search3

    DISPLAY "tbi_tooltip_popup_data_source() - p_order_field=",    p_order_field
    DISPLAY "tbi_tooltip_popup_data_source() - p_ord_dir=",        p_ord_dir
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "string_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_tbi_tooltip.string_id, ",
                 "qxt_language.language_name, ",
                 "qxt_tbi_tooltip.label_data, ",
                 "qxt_tbi_tooltip.string_data, ",
                 "str_cat1.category_data, ",
                 "str_cat2.category_data, ",
                 "str_cat3.category_data ",

                 "FROM ",
                 "qxt_tbi_tooltip, ",
                 "qxt_language, ",
                 "qxt_str_category str_cat1, ",
                 "qxt_str_category str_cat2, ",
                 "qxt_str_category str_cat3 ",

                 "WHERE ",
                 "qxt_tbi_tooltip.language_id = qxt_language.language_id ",

                 "AND ",
                 "qxt_tbi_tooltip.category1_id = str_cat1.category_id ",
                 "AND ",
                 "qxt_tbi_tooltip.category2_id = str_cat2.category_id ",
                 "AND ",
                 "qxt_tbi_tooltip.category3_id = str_cat3.category_id "


  IF p_f_lang_data IS NOT NULL THEN
    IF p_f_lang_switch THEN
      LET sql_stmt = trim(sql_stmt), " ",

                 "AND ", 
                 "qxt_tbi_tooltip.language_id = ", trim(p_f_lang_data), " " 

{
                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_f_lang_data), " ",


                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_f_lang_data), " "


                 "AND ",
                 "qxt_tbi_tooltip.language_id = ", trim(p_f_lang_data), " "
}
    END IF
  END IF


  ###################
  #Category Filter - each category search is validated in all three category columns
  ###################
  IF p_cat_search1 IS NULL OR p_cat_search1 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search2 IS NULL OR p_cat_search2 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search3 IS NULL OR p_cat_search3 = 0 THEN
    LET p_cat_search1 = 1
  END IF


  IF p_cat_filter_switch THEN
    IF p_cat_search1 > 1 OR p_cat_search2 > 1 OR p_cat_search3 > 1 THEN
      LET sql_stmt = trim(sql_stmt), " ",
               "AND ",
                 "( "


      IF p_cat_search1 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "str_cat1.category_id = ", trim(p_cat_search1), " ",
                      "OR ",
                      "str_cat2.category_id = ", trim(p_cat_search1), " ",
                      "OR ",
                      "str_cat3.category_id = ", trim(p_cat_search1), " ",
                    ") "
      END IF

      IF p_cat_search1 > 1 AND p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "str_cat1.category_id = ", trim(p_cat_search2), " ",
                      "OR ",
                      "str_cat2.category_id = ", trim(p_cat_search2), " ",
                      "OR ",
                      "str_cat3.category_id = ", trim(p_cat_search2), " ",
                    ") "
      END IF

      IF p_cat_search2 > 1 AND p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR ",
                    "( ",
                      "str_cat1.category_id = ", trim(p_cat_search3), " ",
                      "OR ",
                      "str_cat2.category_id = ", trim(p_cat_search3), " ",
                      "OR ",
                      "str_cat3.category_id = ", trim(p_cat_search3), " ",
                    ") "
      END IF                     
 
      LET sql_stmt = trim(sql_stmt), " ",
                  ") "

    END IF
  END IF




  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED, "  "
  END IF


  IF local_debug THEN
    DISPLAY "START of SQL @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
    DISPLAY "tbi_tooltip_popup_data_source()"
    DISPLAY sql_stmt[001,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
    DISPLAY sql_stmt[501,600]
    DISPLAY sql_stmt[601,700]
    DISPLAY sql_stmt[701,800]
    DISPLAY sql_stmt[801,900]
    DISPLAY sql_stmt[901,1000]
    DISPLAY sql_stmt[1001,1100]
    DISPLAY sql_stmt[1101,1200]
    DISPLAY sql_stmt[1201,1300]
    DISPLAY "End of SQL @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"
  END IF

  PREPARE p_tbi_tooltip FROM sql_stmt
  DECLARE c_tbi_tooltip CURSOR FOR p_tbi_tooltip

END FUNCTION


######################################################
# FUNCTION tbi_tooltip_popup(p_string_id,p_language_id, p_order_field,p_accept_action)
#
# tbi_tooltip Main Management Function
#
# Arguments:
#   p_string_id     - String ID  (Prim. Key)
#   p_language_id   - Language ID (Prim. Key)
#   p_order_field   - The field name of the default order
#   p_accept_action - The action which should take place when the user pressed OK/Accept/Enter
#
# RETURN p_string_id
######################################################
FUNCTION tbi_tooltip_popup(p_string_id,p_language_id, p_order_field,p_accept_action)
  DEFINE 
    p_string_id                  LIKE qxt_tbi_tooltip.string_id,  --default return value if user cancels
    p_language_id                LIKE qxt_tbi_tooltip.language_id,  --default return value if user cancels
    l_lang_filter_switch         SMALLINT,
    l_tbi_tooltip_arr            DYNAMIC ARRAY OF t_qxt_tbi_tooltip_form_rec,    --RECORD LIKE qxt_tbi_tooltip.*,  
    l_arr_size                   SMALLINT,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_string_id                  LIKE qxt_tbi_tooltip.string_id,
    l_language_id                LIKE qxt_tbi_tooltip.language_id,
    l_language_name              LIKE qxt_language.language_name,
    l_t_lang_id_1, l_t_lang_id_2 LIKE qxt_language.language_id,
    l_t_lang_n_1, l_t_lang_n_2   LIKE qxt_language.language_name,

    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                  SMALLINT,
    l_filter                SMALLINT,
    l_category_data_rec 
      RECORD
        cs1  LIKE qxt_str_category.category_data,
        cs2  LIKE qxt_str_category.category_data,
        cs3  LIKE qxt_str_category.category_data
      END RECORD,

    l_category_id_rec 
      RECORD
        cs1  LIKE qxt_str_category.category_id,
        cs2  LIKE qxt_str_category.category_id,
        cs3  LIKE qxt_str_category.category_id
      END RECORD


  LET local_debug = FALSE
  LET l_arr_size = 500 -- due to dynamic array ...sizeof(l_tbi_tooltip_arr)

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  ELSE
    LET l_lang_filter_switch = TRUE
  END IF

  LET l_language_id = p_language_id
  LET l_language_name = get_language_name(l_language_id)

  #Initialise filter variables
  LET l_filter = FALSE
  
  LET l_category_id_rec.cs1 = 1
  LET l_category_id_rec.cs2 = 1
  LET l_category_id_rec.cs3 = 1

  LET l_category_data_rec.cs1 = ""
  LET l_category_data_rec.cs2 = ""
  LET l_category_data_rec.cs3 = ""

  IF local_debug THEN
    DISPLAY "tbi_tooltip_popup() - p_string_id=",p_string_id
    DISPLAY "tbi_tooltip_popup() - p_order_field=",p_order_field
    DISPLAY "tbi_tooltip_popup() - p_accept_action=",p_accept_action
  END IF


    CALL fgl_window_open("w_tbi_tooltip_scroll", 2, 8, get_form_path("f_qxt_tbi_tooltip_scroll_l2"),FALSE) 
    CALL populate_tbi_tooltip_list_form_labels_g()

    DISPLAY get_language_name(p_language_id) TO cb_f_lang

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_tbi_tooltip

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL tbi_tooltip_create(NULL,p_language_id,NULL,NULL,0)
      	#FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)
   #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL tbi_tooltip_popup_data_source(
      l_lang_filter_switch,
      p_language_id,
      l_filter,
      l_category_id_rec.cs1,
      l_category_id_rec.cs2,
      l_category_id_rec.cs3,
      p_order_field,
      get_toggle_switch())


    LET i = 1
    FOREACH c_tbi_tooltip INTO l_tbi_tooltip_arr[i].*

      #Find current screen line position - go to grid line
      IF l_tbi_tooltip_arr[i].string_id  = l_string_id  THEN
        IF l_tbi_tooltip_arr[i].language_name  = l_language_name THEN
          LET current_row = i
        END IF
      END IF

      IF local_debug THEN
        DISPLAY "tbi_tooltip_popup() - i=",i
        DISPLAY "tbi_tooltip_popup() - l_tbi_tooltip_arr[i].string_id=",l_tbi_tooltip_arr[i].string_id
        DISPLAY "tbi_tooltip_popup() - l_tbi_tooltip_arr[i].language_name=",l_tbi_tooltip_arr[i].language_name
        DISPLAY "tbi_tooltip_popup() - l_tbi_tooltip_arr[i].string_data=",l_tbi_tooltip_arr[i].string_data

      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1
    
		If i > 0 THEN
			CALL l_tbi_tooltip_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF       

    IF local_debug THEN
      DISPLAY "tbi_tooltip_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_tbi_tooltip_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "tbi_tooltip_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_tbi_tooltip_arr TO sc_tbi_tooltip.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL publish_toolbar("ToolbarTbiTooltipList",0)

        IF current_row IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,current_row)
        END IF


      BEFORE ROW
        LET i = arr_curr()
        LET current_row = i
        LET l_string_id = l_tbi_tooltip_arr[i].string_id
        LET l_language_id = get_language_id(l_tbi_tooltip_arr[i].language_name)
        LET l_language_name = l_tbi_tooltip_arr[i].language_name

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tbi_tooltip_view(l_string_id, l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL tbi_tooltip_edit(l_string_id,l_language_id,NULL,TRUE)

            EXIT DISPLAY

          WHEN 3  --delete
            CALL tbi_tooltip_delete(l_string_id,l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL tbi_tooltip_create(NULL,l_language_id,NULL,NULL,TRUE)
						#FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)            
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tbi_tooltip_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)

          WHEN 6  --duplicate
            CALL tbi_tooltip_duplicate(l_string_id,l_language_id)
            EXIT DISPLAY        

          OTHERWISE
            LET err_msg = "tbi_tooltip_popup(p_string_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("tbi_tooltip_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL tbi_tooltip_create(NULL,l_language_id,NULL,NULL,TRUE)
        #FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)  
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL tbi_tooltip_edit(l_string_id, l_language_id,NULL,TRUE)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL tbi_tooltip_delete(l_string_id, l_language_id)
        EXIT DISPLAY

      ON KEY (F7) -- duplicate
        CALL tbi_tooltip_duplicate(l_string_id, l_language_id)
        EXIT DISPLAY
    

      #Grid Column Header
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      #Grid column sort      

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "string_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      



      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat1.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  


      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat2.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  


      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat3.category_data"

        IF p_order_field = p_order_field THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY  


      #Filter language

      ON ACTION ("filterLanguageOn")
        LET l_lang_filter_switch = TRUE
        EXIT DISPLAY

      ON ACTION ("filterLanguageOff")
        LET l_lang_filter_switch = FALSE
        EXIT DISPLAY

      ON ACTION ("setFilterLanguage")
        INPUT l_language_name WITHOUT DEFAULTS FROM cb_f_lang HELP 1
          ON ACTION ("setFilterLanguage","filterLanguageOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF l_language_name IS NOT NULL THEN
          LET  p_language_id = get_language_id(l_language_name)
          LET l_lang_filter_switch = TRUE

          EXIT DISPLAY  --Refresh query and re-populate grid array
 
        ELSE
          LET l_lang_filter_switch = FALSE
        END IF


      #Set string category id
      ON KEY(F25)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat1.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F26)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat2.category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON KEY(F27)
        LET p_order_field2 = p_order_field
        LET p_order_field = "str_cat3.str_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY 

      ON ACTION("filterStringCategoryOn")
        LET l_filter = TRUE
        EXIT DISPLAY 

      ON ACTION("filterStringCategoryOff")
        LET l_filter = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterStringCategory")  --Specify filter criteria
        INPUT BY NAME l_category_data_rec.* WITHOUT DEFAULTS HELP 1
          ON ACTION("setFilterStringCategory","filterStringCategoryOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
          LET l_filter = FALSE
        ELSE
          LET l_filter = TRUE
          LET l_category_id_rec.cs1 = get_str_category_id(l_category_data_rec.cs1,get_language())
          LET l_category_id_rec.cs2 = get_str_category_id(l_category_data_rec.cs2,get_language())
          LET l_category_id_rec.cs3 = get_str_category_id(l_category_data_rec.cs3,get_language())

          IF local_debug THEN
            DISPLAY "l_category_id_rec.cs1=", l_category_id_rec.cs1
            DISPLAY "l_category_id_rec.cs2=", l_category_id_rec.cs2
            DISPLAY "l_category_id_rec.cs3=", l_category_id_rec.cs3
          END IF

          #Some cleaning up in case the user selects i.e. the first two empty and the last combo has an entry (top to bottom)
          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs2 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs2
            LET l_category_id_rec.cs2 = 1
          END IF

          IF l_category_id_rec.cs2 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs2 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          LET l_category_data_rec.cs1 = get_str_category_data(l_category_id_rec.cs1,get_language())
          LET l_category_data_rec.cs2 = get_str_category_data(l_category_id_rec.cs2,get_language())
          LET l_category_data_rec.cs3 = get_str_category_data(l_category_id_rec.cs3,get_language())

          DISPLAY BY NAME l_category_data_rec.cs1
          DISPLAY BY NAME l_category_data_rec.cs2
          DISPLAY BY NAME l_category_data_rec.cs3

          EXIT DISPLAY

        END IF

     #Translation feature
      ON ACTION("setTranslation")
        INPUT l_t_lang_n_1, l_t_lang_n_2 WITHOUT DEFAULTS FROM trans_lang_1,trans_lang_2 HELP 1  
          ON ACTION("setTranslation")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        LET l_t_lang_id_1 = get_language_id(l_t_lang_n_1)
        LET l_t_lang_id_2 = get_language_id(l_t_lang_n_2)

      ON ACTION("translate")

        CALL tbi_tooltip_translate(l_string_id, l_language_id, l_t_lang_id_1, l_t_lang_id_2)
        EXIT DISPLAY


      #Goto Line Number
      ON ACTION ("gotoLine")
        LET current_row = fgl_winprompt(5,5,"Goto Line Number:",1,4,1)
        #CALL fgl_winmessage("line",current_row,"info")
        CALL fgl_dialog_setcurrline(5,current_row)


      #Goto ID
      ON ACTION ("gotoId")
        LET l_string_id = fgl_winprompt(5,5,"Goto String ID Number:",1,4,1)

        EXIT DISPLAY



    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE


  CALL fgl_window_close("w_tbi_tooltip_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_string_id, p_language_id 
  ELSE 
    RETURN l_string_id, p_language_id 
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION tbi_tooltip_create()
#
# Create a new tbi_tooltip record
#
# RETURN NULL
######################################################
FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)
  DEFINE 
    p_string_id       LIKE qxt_tbi_tooltip.string_id,
    p_language_id     LIKE qxt_tbi_tooltip.language_id,
    p_label_data      LIKE qxt_tbi_tooltip.label_data,
    p_string_data     LIKE qxt_tbi_tooltip.string_data,
    p_dialog          SMALLINT,
    l_tbi_tooltip_rec RECORD LIKE qxt_tbi_tooltip.*

  #Initialise some variables

  IF p_language_id IS NOT NULL THEN
    LET l_tbi_tooltip_rec.language_id = p_language_id
  ELSE
    LET l_tbi_tooltip_rec.language_id = get_data_language_id() 
  END IF

  IF p_string_id IS NOT NULL THEN
    LET l_tbi_tooltip_rec.string_id = p_string_id --default language is english
  ELSE
    LET l_tbi_tooltip_rec.string_id = get_tbi_tooltip_new_id(l_tbi_tooltip_rec.language_id)
  END IF

  IF p_label_data IS NOT NULL THEN
    LET l_tbi_tooltip_rec.label_data = p_label_data --default language is english
  END IF


  IF p_string_data IS NOT NULL THEN
    LET l_tbi_tooltip_rec.string_data = p_string_data --default language is english
  END IF


  IF l_tbi_tooltip_rec.category1_id IS NULL OR l_tbi_tooltip_rec.category1_id = 0 THEN
    LET l_tbi_tooltip_rec.category1_id = 1
  END IF

  IF l_tbi_tooltip_rec.category2_id IS NULL OR l_tbi_tooltip_rec.category2_id = 0 THEN
    LET l_tbi_tooltip_rec.category1_id = 1
  END IF

  IF l_tbi_tooltip_rec.category3_id IS NULL OR l_tbi_tooltip_rec.category3_id = 0 THEN
    LET l_tbi_tooltip_rec.category1_id = 1
  END IF


  #Only for p_dialog Section (graphical input
  IF p_dialog THEN

    CALL fgl_window_open("w_tbi_tooltip", 3, 3, get_form_path("f_qxt_tbi_tooltip_det_l2"), TRUE) 
    CALL populate_tbi_tooltip_form_create_labels_g()

    LET int_flag = FALSE

    # CALL the INPUT
    CALL tbi_tooltip_input(l_tbi_tooltip_rec.*,TRUE)
      RETURNING l_tbi_tooltip_rec.*

    CALL fgl_window_close("w_tbi_tooltip")
  END IF




  IF NOT int_flag THEN
    INSERT INTO qxt_tbi_tooltip VALUES (
      l_tbi_tooltip_rec.string_id,
      l_tbi_tooltip_rec.language_id,
      l_tbi_tooltip_rec.label_data,
      l_tbi_tooltip_rec.string_data,
      l_tbi_tooltip_rec.category1_id,
      l_tbi_tooltip_rec.category2_id,
      l_tbi_tooltip_rec.category3_id
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(790),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(790),NULL)
      RETURN l_tbi_tooltip_rec.string_id, l_tbi_tooltip_rec.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]

  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL

  END IF

END FUNCTION


#################################################
# FUNCTION tbi_tooltip_edit(p_string_id,p_language_id)
#
# Edit tbi_tooltip record
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_edit(p_string_id,p_language_id,p_string_data,p_dialog)
  DEFINE 
    p_string_id            LIKE qxt_tbi_tooltip.string_id,
    p_language_id          LIKE qxt_tbi_tooltip.language_id,
    p_string_data          LIKE qxt_tbi_tooltip.string_data,
    p_dialog               SMALLINT,
    l_key1_string_id       LIKE qxt_tbi_tooltip.string_id,
    l_key2_language_id     LIKE qxt_tbi_tooltip.language_id,
    l_tbi_tooltip_rec   RECORD LIKE qxt_tbi_tooltip.*,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_tooltip_edit() - Function Entry Point"
    DISPLAY "tbi_tooltip_edit() - p_string_id=", p_string_id
    DISPLAY "tbi_tooltip_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_string_id = p_string_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_tbi_tooltip_rec(p_string_id,p_language_id) 
    RETURNING l_tbi_tooltip_rec.*


  IF p_string_data IS NOT NULL THEN
    LET l_tbi_tooltip_rec.string_data = p_string_data
  END IF







  #input form is only displayed in dialog mode
  IF p_dialog THEN

    CALL fgl_window_open("w_tbi_tooltip", 3, 3, get_form_path("f_qxt_tbi_tooltip_det_l2"), TRUE) 
    CALL populate_tbi_tooltip_form_edit_labels_g()

    #Call the INPUT
    CALL tbi_tooltip_input(l_tbi_tooltip_rec.*,FALSE) 
      RETURNING l_tbi_tooltip_rec.*

    CALL fgl_window_close("w_tbi_tooltip")

    IF local_debug THEN
      DISPLAY "tbi_tooltip_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF
  END IF



  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "tbi_tooltip_edit() - l_tbi_tooltip_rec.string_id=",l_tbi_tooltip_rec.string_id
      DISPLAY "tbi_tooltip_edit() - l_tbi_tooltip_rec.language_id=",l_tbi_tooltip_rec.language_id
      DISPLAY "tbi_tooltip_edit() - l_tbi_tooltip_rec.string_data=",l_tbi_tooltip_rec.string_data
    END IF

    UPDATE qxt_tbi_tooltip
      SET 
          string_id =                l_tbi_tooltip_rec.string_id,
          language_id =              l_tbi_tooltip_rec.language_id,
          label_data =               l_tbi_tooltip_rec.label_data,
          string_data =              l_tbi_tooltip_rec.string_data,
          category1_id =             l_tbi_tooltip_rec.category1_id,
          category2_id =             l_tbi_tooltip_rec.category2_id,
          category3_id =             l_tbi_tooltip_rec.category3_id
      WHERE qxt_tbi_tooltip.string_id = l_key1_string_id
        AND qxt_tbi_tooltip.language_id = l_key2_language_id


    IF local_debug THEN
      DISPLAY "tbi_tooltip_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(780),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(780),NULL)
      RETURN l_tbi_tooltip_rec.string_id , l_tbi_tooltip_rec.language_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(780),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL
  END IF


END FUNCTION



#################################################
# FUNCTION tbi_tooltip_duplicate(p_string_id,p_language_id)
#
# Crate a new record with clonded data from an existing record
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_duplicate(p_string_id,p_language_id)
  DEFINE 
    p_string_id                 LIKE qxt_tbi_tooltip.string_id,
    p_language_id               LIKE qxt_tbi_tooltip.language_id,
    l_key1_string_id            LIKE qxt_tbi_tooltip.string_id,
    l_key2_language_id          LIKE qxt_tbi_tooltip.language_id,
    l_tbi_tooltip_rec           RECORD LIKE qxt_tbi_tooltip.*,
    source_tbi_tooltip_rec      RECORD LIKE qxt_tbi_tooltip.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_tooltip_edit() - Function Entry Point"
    DISPLAY "tbi_tooltip_edit() - p_string_id=", p_string_id
    DISPLAY "tbi_tooltip_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_string_id = p_string_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_tbi_tooltip_rec(p_string_id,p_language_id) 
    RETURNING source_tbi_tooltip_rec.*

  LET l_tbi_tooltip_rec.* = source_tbi_tooltip_rec.*
  #LET l_tbi_tooltip_rec.string_id = get_tbi_tooltip_new_id(p_language_id)

  CALL fgl_window_open("w_tbi_tooltip", 3, 3, get_form_path("f_qxt_tbi_tooltip_det_l2"), TRUE) 
  CALL populate_tbi_tooltip_form_edit_labels_g()

  #Call the INPUT
  CALL tbi_tooltip_input(l_tbi_tooltip_rec.*,TRUE) 
    RETURNING l_tbi_tooltip_rec.*

  CALL fgl_window_close("w_tbi_tooltip")

  IF NOT int_flag THEN
    INSERT INTO qxt_tbi_tooltip VALUES (
      l_tbi_tooltip_rec.string_id,
      l_tbi_tooltip_rec.language_id,
      l_tbi_tooltip_rec.string_data,
      l_tbi_tooltip_rec.category1_id,
      l_tbi_tooltip_rec.category2_id,
      l_tbi_tooltip_rec.category3_id
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(790),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(790),NULL)
      RETURN l_tbi_tooltip_rec.string_id
    END IF

    #RETURN sqlca.sqlerrd[2]

  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL
  END IF


END FUNCTION



#################################################
# FUNCTION tbi_tooltip_input(p_tbi_tooltip_rec)
#
# Input tbi_tooltip details (edit/create)
#
# RETURN l_tbi_tooltip.*
#################################################
FUNCTION tbi_tooltip_input(p_tbi_tooltip_rec,p_new_rec_flag)
  DEFINE 
    p_tbi_tooltip_rec            RECORD LIKE qxt_tbi_tooltip.*,
    l_tbi_tooltip_form_rec          OF t_qxt_tbi_tooltip_form_rec,
    l_orignal_string_id             LIKE qxt_tbi_tooltip.string_id,
    l_orignal_language_id           LIKE qxt_tbi_tooltip.language_id,
    local_debug                     SMALLINT,
    tmp_str                         VARCHAR(250),
    p_new_rec_flag                  SMALLINT

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_string_id = p_tbi_tooltip_rec.string_id
  LET l_orignal_language_id = p_tbi_tooltip_rec.language_id

  #copy record data to form_record format record
  CALL copy_tbi_tooltip_record_to_form_record(p_tbi_tooltip_rec.*) RETURNING l_tbi_tooltip_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_tbi_tooltip_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      NEXT FIELD string_data

    ON KEY (F100)  -- get next id (MAX id for this language)
      CALL fgl_dialog_update_data()
      LET l_tbi_tooltip_form_rec.string_id = get_tbi_tooltip_new_id(get_language_id(l_tbi_tooltip_form_rec.language_name))
      DISPLAY BY NAME l_tbi_tooltip_form_rec.string_id

    AFTER FIELD string_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(791), l_tbi_tooltip_form_rec.string_id,NULL,TRUE)  THEN
        NEXT FIELD string_id
      END IF


    AFTER FIELD language_name
      #language_id must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(792), l_tbi_tooltip_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD language_id
      END IF




    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #string_id must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(791), l_tbi_tooltip_form_rec.string_id,NULL,TRUE)  THEN
          NEXT FIELD string_id
          CONTINUE INPUT
        END IF

      #language_ID must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(792), l_tbi_tooltip_form_rec.language_name,NULL,TRUE)  THEN

          NEXT FIELD language_id
          CONTINUE INPUT
        END IF


      #The string_id and language_id must be unique - DOUBLE KEY
      IF string_id_and_language_id_count(l_tbi_tooltip_form_rec.string_id,get_language_id(l_tbi_tooltip_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
 
       IF p_new_rec_flag OR (NOT ((l_orignal_string_id = l_tbi_tooltip_form_rec.string_id) AND (l_orignal_language_id = get_language_id(l_tbi_tooltip_form_rec.language_name))))THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_doublekey(get_str_tool(791),get_str_tool(792),NULL)
          NEXT FIELD string_id
          CONTINUE INPUT
        END IF
      END IF




  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "tbi_tooltip_input() - l_tbi_tooltip_form_rec.string_id=",l_tbi_tooltip_form_rec.string_id
    DISPLAY "tbi_tooltip_input() - l_tbi_tooltip_form_rec.language_name=",l_tbi_tooltip_form_rec.language_name
    DISPLAY "tbi_tooltip_input() - l_tbi_tooltip_form_rec.string_data=",l_tbi_tooltip_form_rec.string_data
  END IF


  #Copy the form record data to a normal tbi_tooltip record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_tbi_tooltip_form_record_to_record(l_tbi_tooltip_form_rec.*) RETURNING p_tbi_tooltip_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "tbi_tooltip_input() - p_tbi_tooltip_rec.string_id=",p_tbi_tooltip_rec.string_id
    DISPLAY "tbi_tooltip_input() - p_tbi_tooltip_rec.language_id=",p_tbi_tooltip_rec.language_id
    DISPLAY "tbi_tooltip_input() - p_tbi_tooltip_rec.string_data=",p_tbi_tooltip_rec.string_data
  END IF

  RETURN p_tbi_tooltip_rec.*

END FUNCTION


#################################################
# FUNCTION tbi_tooltip_translate(l_string_id, l_language_id, l_t_lang_id_1, l_t_lang_id_2)
#
# Translate String - Upper Function
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_translate(p_string_id, p_language_id, p_t_lang_id_1, p_t_lang_id_2)
  DEFINE
    p_string_id     LIKE qxt_string_tool.string_id,
    p_language_id   LIKE qxt_string_tool.language_id,
    p_t_lang_id_1   LIKE qxt_string_tool.language_id,
    p_t_lang_id_2   LIKE qxt_string_tool.language_id,
    local_debug     SMALLINT

  LET local_debug = FALSE


  IF local_debug THEN
    DISPLAY "tbi_tooltip_translate(() - p_string_id=", p_string_id
    DISPLAY "tbi_tooltip_translate(() - p_language_id=", p_language_id
    DISPLAY "tbi_tooltip_translate(() - p_t_lang_id_1=", p_t_lang_id_1
    DISPLAY "tbi_tooltip_translate(() - p_t_lang_id_2=", p_t_lang_id_2
  END IF

  #At least one additional language for the translation must be specified
  IF (p_language_id = p_t_lang_id_1) OR (p_language_id = p_t_lang_id_2) OR (p_t_lang_id_1 = p_t_lang_id_2) THEN
    CALL fgl_winmessage("Translation Language Option Error","You cannot translate from and to the same language !\nSpecify the correct languages!","error")
    RETURN
  END IF

  CALL tbi_tooltip_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)


END FUNCTION



#################################################
# FUNCTION tbi_tooltip_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)
#
# Translate String - Lower Function
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_translate_double(p_string_id, p_language_id, p_t_lang_id_1,p_t_lang_id_2)
  DEFINE
    p_string_id          LIKE qxt_tbi_tooltip.string_id,
    p_language_id        LIKE qxt_tbi_tooltip.language_id,
    l_language_name      LIKE qxt_language.language_name,
    l_label_data_0       LIKE qxt_tbi_tooltip.label_data,
    l_string_data_0      LIKE qxt_tbi_tooltip.string_data,
    p_t_lang_id_1        LIKE qxt_tbi_tooltip.language_id,
    l_t_lang_name_1      LIKE qxt_language.language_name,
    l_label_data_1       LIKE qxt_tbi_tooltip.label_data,
    l_string_data_1      LIKE qxt_tbi_tooltip.string_data,
    p_t_lang_id_2        LIKE qxt_tbi_tooltip.language_id,
    l_t_lang_name_2      LIKE qxt_language.language_name,
    l_label_data_2       LIKE qxt_tbi_tooltip.label_data,
    l_string_data_2      LIKE qxt_tbi_tooltip.string_data,
    l_original_lang_id_1 LIKE qxt_tbi_tooltip.language_id,
    l_original_lang_id_2 LIKE qxt_tbi_tooltip.language_id,
    local_debug          SMALLINT

  LET local_debug = FALSE
  LET l_original_lang_id_1 = p_t_lang_id_1
  LET l_original_lang_id_2 = p_t_lang_id_2

  CALL fgl_window_open("w_translate_double",3,3,get_form_path("f_qxt_string_translate_g"),FALSE)

  CALL populate_tbi_tooltip_translate_double_form_labels_g()

  #Display some static data
  DISPLAY p_string_id TO string_id
  DISPLAY get_language_name(p_language_id) TO language_0

  LET l_language_name = get_language_name(p_language_id)
  LET l_t_lang_name_1 = get_language_name(p_t_lang_id_1)
  LET l_t_lang_name_2 = get_language_name(p_t_lang_id_2)

  LET l_label_data_0 = get_tbi_label_data(p_string_id,p_language_id)
  LET l_label_data_1 = get_tbi_label_data(p_string_id,p_t_lang_id_1)
  LET l_label_data_2 = get_tbi_label_data(p_string_id,p_t_lang_id_2)


  LET l_string_data_0 = get_tbi_tooltip_data(p_string_id,p_language_id)
  LET l_string_data_1 = get_tbi_tooltip_data(p_string_id,p_t_lang_id_1)
  LET l_string_data_2 = get_tbi_tooltip_data(p_string_id,p_t_lang_id_2)


  IF local_debug THEN
    DISPLAY "tbi_tooltip_translate_double() - p_string_id=", p_string_id
    DISPLAY "tbi_tooltip_translate_double() - p_language_id=", p_language_id
    DISPLAY "tbi_tooltip_translate_double() - p_t_lang_id_1=", p_t_lang_id_1
    DISPLAY "tbi_tooltip_translate_double() - p_t_lang_id_2=", p_t_lang_id_2
    DISPLAY "tbi_tooltip_translate_double() - l_string_data_0=", l_string_data_0
    DISPLAY "tbi_tooltip_translate_double() - l_string_data_1=", l_string_data_1
  END IF





  INPUT l_string_data_0, l_t_lang_name_1, l_string_data_1,l_t_lang_name_2,l_string_data_2 WITHOUT DEFAULTS 
    FROM string_0,language_1, string_1, language_2, string_2    HELP 1

    AFTER FIELD language_1
      IF get_language_id(l_t_lang_name_1) <> l_original_lang_id_1 THEN
        #When the language changes, we have to update the current data set
        #Check if this is an edit or create operation
        IF string_id_and_language_id_count(p_string_id, l_original_lang_id_1) THEN
          CALL tbi_tooltip_edit(p_string_id,l_original_lang_id_1,l_string_data_1,FALSE)
        ELSE
          CALL tbi_tooltip_create(p_string_id,l_original_lang_id_1,l_label_data_1, l_string_data_1,FALSE)
          #FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)  
        END IF

        LET p_t_lang_id_1 = get_language_id(l_t_lang_name_1)
        LET l_original_lang_id_1 = p_t_lang_id_1

        #Now, we have to fetch the corresponding string data for the new language_id

        LET l_string_data_1 = get_tbi_tooltip_data(p_string_id,p_t_lang_id_1)
        DISPLAY l_string_data_1 TO string_1
      END IF

    AFTER FIELD language_2
      IF get_language_id(l_t_lang_name_2) <> l_original_lang_id_2 THEN
        #When the language changes, we have to update the current data set
        #Check if this is an edit or create operation
        IF string_id_and_language_id_count(p_string_id, l_original_lang_id_2) THEN
          CALL tbi_tooltip_edit(p_string_id,l_original_lang_id_2,l_string_data_2,FALSE)
        ELSE
          CALL tbi_tooltip_create(p_string_id,l_original_lang_id_2,l_label_data_2,l_string_data_2,FALSE)
          #FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)
        END IF

        LET p_t_lang_id_2 = get_language_id(l_t_lang_name_2)
        LET l_original_lang_id_2 = p_t_lang_id_2

        #Now, we have to fetch the corresponding string data for the new language_id

        LET l_string_data_2 = get_tbi_tooltip_data(p_string_id,p_t_lang_id_2)
        DISPLAY l_string_data_2 TO string_2
      END IF


 
    AFTER INPUT
      #LET p_language_id   = get_language_id(l_language_name)
      LET p_t_lang_id_1 = get_language_id(l_t_lang_name_1)
      LET p_t_lang_id_2 = get_language_id(l_t_lang_name_2)
  END INPUT


  IF NOT int_flag THEN

    #Original Master String (selected string language)

    CALL tbi_tooltip_edit(p_string_id,p_language_id,l_string_data_0,FALSE)

    #First translation String (selected string language)

    IF string_id_and_language_id_count(p_string_id, p_t_lang_id_1) THEN
      CALL tbi_tooltip_edit(p_string_id,p_t_lang_id_1,l_string_data_1,FALSE)
    ELSE
      CALL tbi_tooltip_create(p_string_id,p_t_lang_id_1,l_label_data_1, l_string_data_1,FALSE)
			#FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)      
 
   END IF

    #Second Translation String
    IF string_id_and_language_id_count(p_string_id, p_t_lang_id_2) THEN
      #CALL fgl_winmessage("2nd string - edit ", "p_string_id = " || p_string_id || " p_t_lang_id_2 = " || p_t_lang_id_2 || " l_string_data_2=" || l_string_data_2,"info")
      CALL tbi_tooltip_edit(p_string_id,p_t_lang_id_2,l_string_data_2,FALSE)
    ELSE
      #CALL fgl_winmessage("2nd string - create ", "p_string_id = " || p_string_id || " p_t_lang_id_2 = " || p_t_lang_id_2 || " l_string_data_2=" || l_string_data_2,"info")
      CALL tbi_tooltip_create(p_string_id,p_t_lang_id_2,l_label_data_2, l_string_data_2,FALSE)
      #FUNCTION tbi_tooltip_create(p_string_id,p_language_id,p_label_data,p_string_data,p_dialog)

    END IF
  ELSE
    LET int_flag = FALSE
  END IF

  
  CALL fgl_window_close("w_translate_double")


END FUNCTION






#################################################
# FUNCTION tbi_tooltip_delete(p_string_id)
#
# Delete a tbi_tooltip record
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_delete(p_string_id, p_language_id)
  DEFINE 
    p_string_id       LIKE qxt_tbi_tooltip.string_id,
    p_language_id     LIKE qxt_tbi_tooltip.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(780), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_tbi_tooltip 
        WHERE string_id = p_string_id
          AND language_id = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION tbi_tooltip_view(p_string_id)
#
# View tbi_tooltip record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_view(p_string_id,p_language_id)
  DEFINE 
    p_string_id             LIKE qxt_tbi_tooltip.string_id,
    p_language_id           LIKE qxt_tbi_tooltip.language_id,
    l_tbi_tooltip_rec    RECORD LIKE qxt_tbi_tooltip.*


  CALL get_tbi_tooltip_rec(p_string_id, p_language_id) RETURNING l_tbi_tooltip_rec.*
  CALL tbi_tooltip_view_by_rec(l_tbi_tooltip_rec.*)

END FUNCTION


#################################################
# FUNCTION tbi_tooltip_view_by_rec(p_tbi_tooltip_rec)
#
# View tbi_tooltip record in window-form
#
# RETURN NONE
#################################################
FUNCTION tbi_tooltip_view_by_rec(p_tbi_tooltip_rec)
  DEFINE 
    p_tbi_tooltip_rec     RECORD LIKE qxt_tbi_tooltip.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

  CALL fgl_window_open("w_tbi_tooltip", 3, 3, get_form_path("f_qxt_tbi_tooltip_det_l2"), TRUE) 
  CALL populate_tbi_tooltip_form_view_labels_g()

  DISPLAY BY NAME p_tbi_tooltip_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_tbi_tooltip")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################

####################################################
# FFUNCTION string_id_and_language_id_count(p_string_id, p_language_id)
#
# tests if a record with this string_id already exists
#
# RETURN r_count
####################################################

FUNCTION string_id_and_language_id_count(p_string_id, p_language_id)
  DEFINE
    p_string_id      LIKE qxt_tbi_tooltip.string_id,
    p_language_id    LIKE qxt_tbi_tooltip.language_id,
    r_count          SMALLINT,
    local_debug      SMALLINT

  LET local_debug = FALSE

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_tooltip
      WHERE qxt_tbi_tooltip.string_id = p_string_id
        AND qxt_tbi_tooltip.language_id = p_language_id

  IF local_debug THEN
    DISPLAY "string_id_and_language_id_count() - r_count=", r_count
  END IF

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

####################################################
# FUNCTION language_id_count(p_language_id)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION language_id_count(p_language_id)
  DEFINE
    p_language_id         LIKE qxt_tbi_tooltip.language_id,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_tooltip
      WHERE qxt_tbi_tooltip.language_id = p_language_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION string_id_count(p_string_id)
#
# tests if a record with this string_id already exists
#
# RETURN r_count
####################################################
FUNCTION string_id_count(p_string_id)
  DEFINE
    p_string_id    LIKE qxt_tbi_tooltip.string_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_tbi_tooltip
      WHERE qxt_tbi_tooltip.string_id = p_string_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_tbi_tooltip_record_to_form_record(p_tbi_tooltip_rec)  
#
# Copy normal tbi_tooltip record data to type tbi_tooltip_form_rec
#
# RETURN l_tbi_tooltip_form_rec.*
######################################################
FUNCTION copy_tbi_tooltip_record_to_form_record(p_tbi_tooltip_rec)  
  DEFINE
    p_tbi_tooltip_rec       RECORD LIKE qxt_tbi_tooltip.*,
    l_tbi_tooltip_form_rec  OF t_qxt_tbi_tooltip_form_rec

  LET l_tbi_tooltip_form_rec.string_id =      p_tbi_tooltip_rec.string_id
  LET l_tbi_tooltip_form_rec.language_name =  get_language_name(p_tbi_tooltip_rec.language_id)
  LET l_tbi_tooltip_form_rec.label_data =    p_tbi_tooltip_rec.label_data
  LET l_tbi_tooltip_form_rec.string_data =    p_tbi_tooltip_rec.string_data

  LET l_tbi_tooltip_form_rec.category1_data = get_str_category_data(p_tbi_tooltip_rec.category1_id, get_language())
  LET l_tbi_tooltip_form_rec.category2_data = get_str_category_data(p_tbi_tooltip_rec.category2_id, get_language())
  LET l_tbi_tooltip_form_rec.category3_data = get_str_category_data(p_tbi_tooltip_rec.category3_id, get_language())

  RETURN l_tbi_tooltip_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_tbi_tooltip_form_record_to_record(p_tbi_tooltip_form_rec)  
#
# Copy type tbi_tooltip_form_rec to normal tbi_tooltip record data
#
# RETURN l_tbi_tooltip_rec.*
######################################################
FUNCTION copy_tbi_tooltip_form_record_to_record(p_tbi_tooltip_form_rec)  
  DEFINE
    l_tbi_tooltip_rec       RECORD LIKE qxt_tbi_tooltip.*,
    p_tbi_tooltip_form_rec  OF t_qxt_tbi_tooltip_form_rec

  LET l_tbi_tooltip_rec.string_id =    p_tbi_tooltip_form_rec.string_id
  LET l_tbi_tooltip_rec.language_id =  get_language_id(p_tbi_tooltip_form_rec.language_name)
  LET l_tbi_tooltip_rec.label_data =  p_tbi_tooltip_form_rec.label_data
  LET l_tbi_tooltip_rec.string_data =  p_tbi_tooltip_form_rec.string_data

  LET l_tbi_tooltip_rec.category1_id = get_str_category_id(p_tbi_tooltip_form_rec.category1_data, get_language())
  LET l_tbi_tooltip_rec.category2_id = get_str_category_id(p_tbi_tooltip_form_rec.category2_data, get_language())
  LET l_tbi_tooltip_rec.category3_id = get_str_category_id(p_tbi_tooltip_form_rec.category3_data, get_language())


  RETURN l_tbi_tooltip_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_tbi_tooltip_scroll()
#
# Populate tbi_tooltip grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_tbi_tooltip_scroll()
  CALL fgl_grid_header("sc_tbi_tooltip","string_id",     get_str_tool(791),"right","F13")  --tbi_tooltip
  CALL fgl_grid_header("sc_tbi_tooltip","language_name", get_str_tool(792),"left", "F14")  --file language_id
  CALL fgl_grid_header("sc_tbi_tooltip","string_data",   get_str_tool(793),"left", "F15")  --tbi_tooltip_data
  CALL fgl_grid_header("sc_tbi_tooltip","category1_data",get_str_tool(794),"left", "F16")  --tbi_tooltip_data
  CALL fgl_grid_header("sc_tbi_tooltip","category2_data",get_str_tool(795),"left", "F17")  --string_tool_data
  CALL fgl_grid_header("sc_tbi_tooltip","category3_data",get_str_tool(796),"left", "F18")  --string_tool_data

END FUNCTION

#######################################################
# FUNCTION str_app_combo_population()
#
# Populate combo lists
#
# RETURN NONE
#######################################################
FUNCTION str_tbi_tooltip_combo_population()
  DEFINE l_language_id LIKE qxt_language.language_id

  LET l_language_id = get_language()

  CALL str_category_data_combo_list("category1_data",l_language_id)
  CALL str_category_data_combo_list("category2_data",l_language_id)
  CALL str_category_data_combo_list("category3_data",l_language_id)
  CALL language_combo_list("language_name")

END FUNCTION


#######################################################
# FUNCTION populate_tbi_tooltip_form_labels_g()
#
# Populate tbi_tooltip form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(790) TO lbTitle

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_tbi_tooltip_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_tbi_tooltip_form_labels_t()
#
# Populate tbi_tooltip form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_labels_t()

  DISPLAY get_str_tool(790) TO lbTitle

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tbi_tooltip_form_edit_labels_g()
#
# Populate tbi_tooltip form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(790)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(797) TO lbButtonLabel  
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1
  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  #DISPLAY "!" TO bt_get_new_id

  CALL str_tbi_tooltip_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_tbi_tooltip_form_edit_labels_t()
#
# Populate tbi_tooltip form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_edit_labels_t()

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tbi_tooltip_form_view_labels_g()
#
# Populate tbi_tooltip form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(790)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_tbi_tooltip_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_tbi_tooltip_form_view_labels_t()
#
# Populate tbi_tooltip form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_view_labels_t()

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_tbi_tooltip_form_create_labels_g()
#
# Populate tbi_tooltip form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(790)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(23) TO bt_get_new_id
  DISPLAY "!" TO bt_get_new_id

  CALL str_tbi_tooltip_combo_population()

END FUNCTION



#######################################################
# FUNCTION populate_tbi_tooltip_form_create_labels_t()
#
# Populate tbi_tooltip form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_form_create_labels_t()

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION




#######################################################
# FUNCTION populate_tbi_tooltip_translate_double_form_labels_g()
#
# Populate tbi_tooltip form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_translate_double_form_labels_g()

  CALL fgl_settitle(get_str_tool(781) || "String Translator")

  #DISPLAY get_str_tool(781) TO lbTitle

  #DISPLAY get_str_tool(791) TO dl_f1
  #DISPLAY get_str_tool(792) TO dl_f2
  #DISPLAY get_str_tool(793) TO dl_f3
  #DISPLAY get_str_tool(794) TO dl_f4
  #DISPLAY get_str_tool(795) TO dl_f5
  #DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  #CALL language_combo_list("language_0")
  CALL language_combo_list("language_1")
  CALL language_combo_list("language_2")
  
  #DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  #DISPLAY get_str_tool(23) TO bt_get_new_id
  #DISPLAY "!" TO bt_get_new_id

  #CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_tbi_tooltip_list_form_labels_g()
#
# Populate tbi_tooltip list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(790)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_tbi_tooltip_scroll()

  #DISPLAY get_str_tool(791) TO dl_f1
  #DISPLAY get_str_tool(792) TO dl_f2
  #DISPLAY get_str_tool(793) TO dl_f3
  #DISPLAY get_str_tool(794) TO dl_f4
  #DISPLAY get_str_tool(795) TO dl_f5
  #DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  DISPLAY get_str_tool(22) TO bt_duplicate
  #DISPLAY "!" TO bt_duplicate


  #DISPLAY "!" TO bt_filter_language_set
  #DISPLAY "!" TO switch_filter_language

  #DISPLAY "!" TO bt_set_trans
  #DISPLAY get_str_tool(22) TO bt_set_translation

  #Goto Line Numer button
  #DISPLAY "!" TO bt_goto_line
  #DISPLAY "!" TO bt_goto_id

  #Translate
  #DISPLAY "!" TO bt_translate
  #DISPLAY get_str_tool(22) TO bt_translate

  #Enable Category Filter check box
  #DISPLAY "!" TO str_category_filter

  #Enbable button and combo to specify filter criteria
  #DISPLAY "!" TO bt_set_str_category_filter_criteria
  CALL str_category_data_combo_list("cs1",get_language())
  CALL str_category_data_combo_list("cs2",get_language())
  CALL str_category_data_combo_list("cs3",get_language())

  CALL language_combo_list("cb_f_lang")
  CALL language_combo_list("trans_lang_1")
  CALL language_combo_list("trans_lang_2")

END FUNCTION


#######################################################
# FUNCTION populate_tbi_tooltip_list_form_labels_t()
#
# Populate tbi_tooltip list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tbi_tooltip_list_form_labels_t()

  DISPLAY trim(get_str_tool(790)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(791) TO dl_f1
  DISPLAY get_str_tool(792) TO dl_f2
  DISPLAY get_str_tool(793) TO dl_f3
  DISPLAY get_str_tool(794) TO dl_f4
  DISPLAY get_str_tool(795) TO dl_f5
  DISPLAY get_str_tool(796) TO dl_f6
  #DISPLAY get_str_tool(797) TO dl_f7
  #DISPLAY get_str_tool(798) TO dl_f8
  #DISPLAY get_str_tool(799) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################













