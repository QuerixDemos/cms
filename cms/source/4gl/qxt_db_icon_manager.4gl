##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the icon table
#
#
# FUNCTION                                               DESCRIPTION                                                   RETURN
# get_icon_id(p_icon_filename)  get icon id from p_icon_filename        l_icon_property_name
# get_icon_filename(icon_property_name)    Get icon_filename from p_icon_property_name        l_icon_filename
# get_icon_rec(p_icon_property_name)       Get the icon record from p_icon_property_name  l_icon.*
# icon_popup_data_source()                       Data Source (cursor) for icon_popup              NONE
# icon_popup                                     icon selection window                            p_icon_property_name
# (p_icon_property_name,p_order_field,p_accept_action)
# icon_create()                                  Create a new icon record                         NULL
# icon_edit(p_icon_property_name)          Edit icon record                                 NONE
# icon_input(p_icon_rec)                 Input icon details (edit/create)                 l_icon.*
# icon_delete(p_icon_property_name)        Delete a icon record                             NONE
# icon_view(p_icon_property_name)          View icon record by ID in window-form            NONE
# icon_view_by_rec(p_icon_rec)           View icon record in window-form               NONE
# get_icon_id_from_filename(p_filename) Get the icon_property_name from a file                      l_icon_property_name
# icon_filename_count(p_filename)                Tests if a record with this name already exists               r_count
# icon_property_name_count(p_icon_property_name)  tests if a record with this icon_property_name already exists r_count
# copy_icon_record_to_form_record                Copy normal icon record data to type icon_form_rec   l_icon_form_rec.*
# (p_icon_rec)
# copy_icon_form_record_to_record                Copy type icon_form_rec to normal icon record data   l_icon_rec.*
# (p_icon_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_icon_scroll()              Populate icon grid headers                       NONE
# populate_icon_form_labels_g()          Populate icon form labels for gui                NONE
# populate_icon_form_labels_t()          Populate icon form labels for text               NONE
# populate_icon_form_edit_labels_g()     Populate icon form edit labels for gui           NONE
# populate_icon_form_edit_labels_t()     Populate icon form edit labels for text          NONE
# populate_icon_form_view_labels_g()     Populate icon form view labels for gui           NONE
# populate_icon_form_view_labels_t()     Populate icon form view labels for text          NONE
# populate_icon_form_create_labels_g()   Populate icon form create labels for gui         NONE
# populate_icon_form_create_labels_t()   Populate icon form create labels for text        NONE
# populate_icon_list_form_labels_g()     Populate icon list form labels for gui           NONE
# populate_icon_list_form_labels_t()     Populate icon list form labels for text          NONE
#
####################################################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"



#########################################################
# FUNCTION get_icon_id(p_icon_filename,p_icon_property_name)
#
# get icon id from p_icon_filename and p_icon_property_name
#
# RETURN l_icon_property_name
#########################################################
FUNCTION get_icon_category1_id(p_icon_filename)
  DEFINE 
    p_icon_filename        LIKE qxt_icon.icon_filename,
    l_icon_category1_id    LIKE qxt_icon.icon_category1_id,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_id() - p_icon_filename = ", p_icon_filename
  END IF

  SELECT qxt_icon.icon_category1_id
    INTO l_icon_category1_id
    FROM qxt_icon
    WHERE qxt_icon.icon_filename = p_icon_filename

  RETURN l_icon_category1_id
END FUNCTION



#########################################################
# FUNCTION get_icon_category2_id(p_icon_filename)
#
# get icon category id  FROM qxt_icon object
#
# RETURN l_icon_property_name
#########################################################
FUNCTION get_icon_category2_id(p_icon_filename)
  DEFINE 
    p_icon_filename        LIKE qxt_icon.icon_filename,
    l_icon_category2_id    LIKE qxt_icon.icon_category2_id,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_id() - p_icon_filename = ", p_icon_filename
  END IF

  SELECT qxt_icon.icon_category2_id
    INTO l_icon_category2_id
    FROM qxt_icon
    WHERE qxt_icon.icon_filename = p_icon_filename

  RETURN l_icon_category2_id
END FUNCTION


#########################################################
# FUNCTION get_icon_category3_id(p_icon_filename)
#
# get icon category id  FROM qxt_icon object
#
# RETURN l_icon_property_name
#########################################################
FUNCTION get_icon_category3_id(p_icon_filename)
  DEFINE 
    p_icon_filename        LIKE qxt_icon.icon_filename,
    l_icon_category3_id    LIKE qxt_icon.icon_category3_id,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_id() - p_icon_filename = ", p_icon_filename
  END IF

  SELECT qxt_icon.icon_category3_id
    INTO l_icon_category3_id
    FROM qxt_icon
    WHERE qxt_icon.icon_filename = p_icon_filename

  RETURN l_icon_category3_id
END FUNCTION

{
#########################################################
# FUNCTION get_icon_filename(p_icon_id)
#
# Get icon_filename from p_icon_id
#
# RETURN l_icon_filename
#########################################################
FUNCTION get_icon_filename(p_icon_id)
  DEFINE 
    p_icon_id                LIKE qxt_icon.icon_id,
    l_icon_filename          LIKE qxt_icon.icon_filename

  SELECT qxt_icon.icon_filename
    INTO l_icon_filename
    FROM qxt_icon
    WHERE qxt_icon.icon_id = p_icon_id

  RETURN l_icon_filename
END FUNCTION
}

######################################################
# FUNCTION get_icon_rec(p_icon_id)
#
# Get the icon record from p_icon_id
#
# RETURN l_icon.*
######################################################
FUNCTION get_icon_rec(p_icon_filename)
  DEFINE 
    p_icon_filename     LIKE qxt_icon.icon_filename,
    #p_icon_property_name  LIKE qxt_icon.icon_property_name,
    l_icon              RECORD LIKE qxt_icon.*

  SELECT qxt_icon.*
    INTO l_icon.*
    FROM qxt_icon
    WHERE qxt_icon.icon_filename = p_icon_filename

  RETURN l_icon.*
END FUNCTION


#################################################################################################################
# List / Combo functions
#################################################################################################################



###################################################################################
# FUNCTION icon_combo_list(cb_field_name)
#
# Populates toolbar icon combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION icon_filename_combo_list(cb_field_name)
  DEFINE 
    l_icon_filename_arr                DYNAMIC ARRAY OF LIKE qxt_icon.icon_filename,
    row_count                          INTEGER,
    current_row                        INTEGER,
    cb_field_name                      VARCHAR(30),   --form field name for the country combo list field
    abort_flag                        SMALLINT,
    tmp_str                            VARCHAR(250),
    local_debug                        SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_filename_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_icon_filename_scroll2 CURSOR FOR 
    SELECT qxt_icon.icon_filename
      FROM qxt_icon
    ORDER BY icon_filename ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_icon_filename_scroll2 INTO l_icon_filename_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_icon_filename_arr[row_count])

    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_icon_filename_arr[row_count] CLIPPED, ")"
      DISPLAY "toolbar_menu_action_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

END FUNCTION


######################################################
# FUNCTION icon_popup_data_source()
#
# Data Source (cursor) for icon_popup
#
# RETURN NONE
######################################################
FUNCTION icon_popup_data_source(p_name_filter,p_name_search,p_filter,p_cat_search1,p_cat_search2,p_cat_search3,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT,
    p_filter            SMALLINT,
    p_cat_search1        LIKE qxt_icon_category.icon_category_id,
    p_cat_search2        LIKE qxt_icon_category.icon_category_id,
    p_cat_search3        LIKE qxt_icon_category.icon_category_id,
    p_name_filter        SMALLINT,
    p_name_search        LIKE qxt_icon.icon_filename,
    p_language_id        LIKE qxt_language.language_id

  LET local_debug = FALSE


  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "icon_filename"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  LET sql_stmt = "SELECT ",
                 "qxt_icon.icon_filename, ",
                 "ic_cat1.icon_category_data, ",
                 "ic_cat2.icon_category_data, ",
                 "ic_cat3.icon_category_data ",


                 "FROM ",
                   "qxt_icon, ",
                   "qxt_icon_category ic_cat1, ",
                   "qxt_icon_category ic_cat2, ",
                   "qxt_icon_category ic_cat3 ",

                 "WHERE ",
                 "qxt_icon.icon_category1_id = ic_cat1.icon_category_id ",
                 "AND ",
                 "qxt_icon.icon_category2_id = ic_cat2.icon_category_id ",
                 "AND ",
                 "qxt_icon.icon_category3_id = ic_cat3.icon_category_id ",
                 "AND ",
                 "ic_cat1.language_id = ", p_language_id, " ",
                 "AND ",
                 "ic_cat2.language_id = ", p_language_id, " ",
                 "AND ",
                 "ic_cat3.language_id = ", p_language_id, " "

  #filter with empty string is ignored
  IF p_name_search IS NULL THEN
    LET p_name_filter = FALSE
  ELSE
    IF p_name_filter = TRUE THEN
      LET sql_stmt = trim(sql_stmt), 
                 "AND ",
                 "qxt_icon.icon_filename LIKE '", trim(p_name_search), "'"      
    END IF
  END IF



  IF p_cat_search1 IS NULL OR p_cat_search1 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search2 IS NULL OR p_cat_search2 = 0 THEN
    LET p_cat_search1 = 1
  END IF

  IF p_cat_search3 IS NULL OR p_cat_search3 = 0 THEN
    LET p_cat_search1 = 1
  END IF


  IF p_filter THEN
    IF p_cat_search1 > 1 OR p_cat_search2 > 1 OR p_cat_search3 > 1 THEN
      LET sql_stmt = trim(sql_stmt), " ",
               "AND ",
                 "( "


      IF p_cat_search1 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search1), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search1), "' ",
                    ") "
      END IF

      IF p_cat_search1 > 1 AND p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search2 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search2), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search2), "' ",
                    ") "
      END IF

      IF p_cat_search2 > 1 AND p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR "
      END IF

      IF p_cat_search3 > 1 THEN
        LET sql_stmt = trim(sql_stmt), " ",
                    "OR ",
                    "( ",
                      "ic_cat1.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat2.icon_category_id = '", trim(p_cat_search3), "' ",
                      "OR ",
                      "ic_cat3.icon_category_id = '", trim(p_cat_search3), "' ",
                    ") "
      END IF                     
 
      LET sql_stmt = trim(sql_stmt), " ",
                  ") "

    END IF
  END IF


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "icon_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_icon FROM sql_stmt
  DECLARE c_icon CURSOR FOR p_icon

END FUNCTION


######################################################
# FUNCTION icon_popup(p_icon_id,p_order_field,p_accept_action)
#
# icon selection window
#
# RETURN l_icon_filename
######################################################
FUNCTION icon_popup(p_icon_filename,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_icon_filename              LIKE qxt_icon.icon_filename,  --default return value if user cancels
    p_accept_action              SMALLINT,
    p_language_id                LIKE qxt_language.language_id,
    p_order_field                VARCHAR(128), 
    p_order_field2               VARCHAR(128), 
    l_icon_arr                   DYNAMIC ARRAY OF t_qxt_icon_form_rec,    --RECORD LIKE qxt_icon.*,  
    i,row_max                    INTEGER,
    err_msg                      VARCHAR(240),
    l_icon_filename              LIKE qxt_icon.icon_filename,
    l_temp_icon_filename         LIKE qxt_icon.icon_filename,
    local_debug                  BOOLEAN,
    l_filter_icon_name           LIKE qxt_icon.icon_filename,
    l_filter_icon_name_state     SMALLINT,
    l_filter_icon_category_state SMALLINT,  
    l_language_name              LIKE qxt_language.language_name,
    current_row                  SMALLINT,  --current row for auto grid scroll - fgl_dialog_setcurline()
    l_category_data_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_data,
        cs2  LIKE qxt_icon_category.icon_category_data,
        cs3  LIKE qxt_icon_category.icon_category_data
      END RECORD,

    l_category_id_rec 
      RECORD
        cs1  LIKE qxt_icon_category.icon_category_id,
        cs2  LIKE qxt_icon_category.icon_category_id,
        cs3  LIKE qxt_icon_category.icon_category_id
      END RECORD

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET row_max = 500 -- due to dynamic array ...sizeof(l_icon_arr)

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  LET p_order_field2 = ""

  #Initialise filter variables
  LET l_filter_icon_category_state = FALSE
  LET l_filter_icon_name_state = FALSE
  LET l_category_id_rec.cs1 = 1
  LET l_category_id_rec.cs2 = 1
  LET l_category_id_rec.cs3 = 1

  LET l_category_data_rec.cs1 = ""
  LET l_category_data_rec.cs2 = ""
  LET l_category_data_rec.cs3 = ""



  IF local_debug THEN
    DISPLAY "icon_popup() - p_icon_filename=",p_icon_filename
    DISPLAY "icon_popup() - p_order_field=",  p_order_field
    DISPLAY "icon_popup() - p_accept_action=",p_accept_action
  END IF




  CALL fgl_window_open("w_icon_scroll", 2, 8, get_form_path("f_qxt_icon_scroll_l2"),FALSE) 
  CALL populate_icon_list_form_labels_g()


  #Display some dynamic data (filter)
  DISPLAY get_language_name(p_language_id) TO filter_lang_n

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_icon

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL icon_create(NULL,NULL,NULL,NULL)
    END IF
  END IF



  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL icon_popup_data_source(l_filter_icon_name_state,l_filter_icon_name,l_filter_icon_category_state,l_category_id_rec.cs1,l_category_id_rec.cs2,l_category_id_rec.cs3,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_icon INTO l_icon_arr[i].*
      IF local_debug THEN
        DISPLAY "icon_popup() - i=",i
        DISPLAY "icon_popup() - l_icon_arr[i].icon_filename=",     l_icon_arr[i].icon_filename
        DISPLAY "icon_popup() - l_icon_arr[i].icon_category1_data=",    l_icon_arr[i].icon_category1_data
        DISPLAY "icon_popup() - l_icon_arr[i].icon_category2_data=",    l_icon_arr[i].icon_category2_data
        DISPLAY "icon_popup() - l_icon_arr[i].icon_category3_data=",    l_icon_arr[i].icon_category3_data
      END IF

      IF l_icon_arr[i].icon_filename = l_icon_filename THEN
        LET current_row = i
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > row_max THEN
        ERROR "Array is limited to ", row_max, " entries"
        EXIT FOREACH
      END IF

    END FOREACH
    
    
		If i > 0 THEN
			CALL l_icon_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		    
    LET i = i - 1
		IF current_row = 0 THEN
			LET current_row = 1
		END IF
    DISPLAY i TO found_records
    DISPLAY icon_count() TO total_records

    IF local_debug THEN
      DISPLAY "icon_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_icon_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "icon_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_icon_arr TO sc_icon.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL fgl_dialog_setcurrline(5,current_row)

      BEFORE ROW
        LET i = arr_curr()
        LET l_icon_filename = l_icon_arr[i].icon_filename

        CALL icon_preview(l_icon_filename)
        DISPLAY l_icon_filename TO pv_icon
        DISPLAY l_icon_arr[i].icon_category1_data TO pv_category_1
        DISPLAY l_icon_arr[i].icon_category2_data TO pv_category_2
        DISPLAY l_icon_arr[i].icon_category3_data TO pv_category_3
       

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL icon_view(l_icon_filename)
            #EXIT DISPLAY

          WHEN 2  --edit
            LET l_temp_icon_filename = icon_edit(l_icon_filename)
            IF l_temp_icon_filename IS NOT NULL THEN
              LET l_icon_filename = l_temp_icon_filename
              EXIT DISPLAY
            END IF

          WHEN 3  --delete
            IF icon_delete(l_icon_filename) THEN
              LET l_icon_filename = NULL
              EXIT DISPLAY
            END IF

          WHEN 4  --create/add
            LET l_temp_icon_filename = icon_create(NULL,NULL,NULL,NULL)
            IF l_temp_icon_filename IS NOT NULL THEN
              LET l_icon_filename = l_temp_icon_filename
              EXIT DISPLAY
            END IF


          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","icon_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "icon_popup(", p_order_field, " p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("icon_popup() - 4GL Source Error",err_msg, "error") 
        END CASE


      ON KEY (F4) -- add
        LET l_temp_icon_filename = icon_create(NULL,NULL,NULL,NULL)
        IF l_temp_icon_filename IS NOT NULL THEN
          LET l_icon_filename = l_temp_icon_filename
          EXIT DISPLAY
        END IF


      ON KEY (F5) -- edit
        LET l_temp_icon_filename = icon_edit(l_icon_filename)
        IF l_temp_icon_filename IS NOT NULL THEN
          LET l_icon_filename = l_temp_icon_filename
          EXIT DISPLAY
        END IF

      ON KEY (F6) -- delete
        IF icon_delete(l_icon_filename) THEN
          LET l_icon_filename = NULL
          EXIT DISPLAY
        END IF
         


      #Grid Column Header
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat1.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat2.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ic_cat3.icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON ACTION("filterIconCategoryOn")
        LET l_filter_icon_category_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterIconCategoryOff")
        LET l_filter_icon_category_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterIconCategory")  --Specify filter criteria
        INPUT BY NAME l_category_data_rec.* WITHOUT DEFAULTS HELP 1
          ON ACTION("setFilterIconCategory","filterIconCategoryOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET l_filter_icon_category_state = TRUE

          LET l_category_id_rec.cs1 = get_icon_category_id(l_category_data_rec.cs1,get_language())
          LET l_category_id_rec.cs2 = get_icon_category_id(l_category_data_rec.cs2,get_language())
          LET l_category_id_rec.cs3 = get_icon_category_id(l_category_data_rec.cs3,get_language())

          IF local_debug THEN
            DISPLAY "l_category_id_rec.cs1=", l_category_id_rec.cs1
            DISPLAY "l_category_id_rec.cs2=", l_category_id_rec.cs2
            DISPLAY "l_category_id_rec.cs3=", l_category_id_rec.cs3
          END IF

          #Some cleaning up in case the user selects i.e. the first two empty and the last combo has an entry (top to bottom)
          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs2 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs2
            LET l_category_id_rec.cs2 = 1
          END IF

          IF l_category_id_rec.cs2 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs2 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          IF l_category_id_rec.cs1 < 2 AND l_category_id_rec.cs3 > 1 THEN
            LET l_category_id_rec.cs1 = l_category_id_rec.cs3
            LET l_category_id_rec.cs3 = 1
          END IF

          LET l_category_data_rec.cs1 = get_icon_category_data(l_category_id_rec.cs1,get_language())
          LET l_category_data_rec.cs2 = get_icon_category_data(l_category_id_rec.cs2,get_language())
          LET l_category_data_rec.cs3 = get_icon_category_data(l_category_id_rec.cs3,get_language())

          DISPLAY BY NAME l_category_data_rec.cs1
          DISPLAY BY NAME l_category_data_rec.cs2
          DISPLAY BY NAME l_category_data_rec.cs3


          EXIT DISPLAY 
        END IF

      ON ACTION("filterIconNameOn")
        LET l_filter_icon_name_state = TRUE
        EXIT DISPLAY 

      ON ACTION("filterIconNameOff")
        LET l_filter_icon_name_state = FALSE
        EXIT DISPLAY 

      ON ACTION("setFilterIconName")  --Specify filter criteria
        INPUT l_filter_icon_name WITHOUT DEFAULTS FROM filter_icon_name HELP 1
          ON ACTION("setFilterIconName","filterIconNameOn")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT

        IF int_flag THEN
          LET int_flag = FALSE
        ELSE
          LET l_filter_icon_name_state = TRUE
        END IF

        EXIT DISPLAY


      ON ACTION ("setFilterLanguageName")
        INPUT l_language_name WITHOUT DEFAULTS FROM filter_lang_n HELP 1
          ON ACTION ("setFilterLanguageName")
            CALL fgl_dialog_update_data()
            EXIT INPUT
        END INPUT
        IF l_language_name IS NOT NULL THEN
          LET p_language_id = get_language_id(l_language_name)

          CALL set_data_language_id(p_language_id)
          #Update the category combo lists with the new language strings
          CALL icon_category_data_combo_list("cs1",get_data_language_id())
          CALL icon_category_data_combo_list("cs2",get_data_language_id())
          CALL icon_category_data_combo_list("cs3",get_data_language_id())
        END IF
        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_icon_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_icon_filename 
  ELSE 
    RETURN l_icon_filename
  END IF

END FUNCTION






#############################################################################################################
# Record Manipulation & Create/Delete
#############################################################################################################


######################################################
# FUNCTION icon_create(p_icon_filename,p_cat1,p_cat2,p_cat3)
#
# Create a new icon record
#
# RETURN NULL
######################################################
FUNCTION icon_create(p_icon_filename,p_cat1,p_cat2,p_cat3)
  DEFINE 
    p_icon_filename         LIKE qxt_icon.icon_filename,
    p_cat1                  LIKE qxt_icon.icon_category1_id,
    p_cat2                  LIKE qxt_icon.icon_category2_id,
    p_cat3                  LIKE qxt_icon.icon_category3_id,
    l_icon_rec              RECORD LIKE qxt_icon.*,
    local_debug             SMALLINT

  LET local_debug = FALSE

  #Initialise some variables
  IF p_icon_filename IS NOT NULL THEN
    LET l_icon_rec.icon_filename = p_icon_filename
  END IF

  IF p_cat1 IS NOT NULL THEN
    LET l_icon_rec.icon_category1_id = p_cat1
  ELSE
    LET l_icon_rec.icon_category1_id = 1
  END IF

  IF p_cat2 IS NOT NULL THEN
    LET l_icon_rec.icon_category2_id = p_cat2
  ELSE
    LET l_icon_rec.icon_category2_id = 1
  END IF    

  IF p_cat3 IS NOT NULL THEN
    LET l_icon_rec.icon_category3_id = p_cat3
  ELSE
    LET l_icon_rec.icon_category3_id = 1
  END IF    


	CALL fgl_window_open("w_icon", 3, 3, get_form_path("f_qxt_icon_det_l2"), FALSE) 
	CALL populate_icon_form_create_labels_g()


  LET int_flag = FALSE
  

  # CALL the INPUT
  CALL icon_input(l_icon_rec.*)
    RETURNING l_icon_rec.*

  CALL fgl_window_close("w_icon")


  IF local_debug THEN
      DISPLAY "icon_create() - l_icon.icon_filename=",       l_icon_rec.icon_filename
      DISPLAY "icon_create() - l_icon.icon_category1_id=",   l_icon_rec.icon_category1_id
      DISPLAY "icon_create() - l_icon.icon_category2_id=",   l_icon_rec.icon_category2_id
      DISPLAY "icon_create() - l_icon.icon_category3_id=",   l_icon_rec.icon_category3_id

  END IF

  IF NOT int_flag THEN
    INSERT INTO qxt_icon VALUES (
      l_icon_rec.icon_filename,
      l_icon_rec.icon_category1_id,
      l_icon_rec.icon_category2_id,
      l_icon_rec.icon_category3_id
                                 )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(870),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(870),NULL)
      RETURN l_icon_rec.icon_filename  --sqlca.sqlerrd[2]
    END IF

  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(870),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION icon_edit(p_icon_filename)
#
# Edit icon record
#
# RETURN NONE
#################################################
FUNCTION icon_edit(p_icon_filename)
  DEFINE 
    p_icon_filename          LIKE qxt_icon.icon_filename,  --key1
    l_key1_icon_filename     LIKE qxt_icon.icon_filename,
    l_icon_rec               RECORD LIKE qxt_icon.*,
    local_debug              SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_edit() - Function Entry Point"
    DISPLAY "icon_edit() - p_icon_filename=", p_icon_filename
  END IF

  #Store the primary key for the SQL update
  LET l_key1_icon_filename = p_icon_filename

  #Get the record (by id)
  CALL get_icon_rec(p_icon_filename) RETURNING l_icon_rec.*


    CALL fgl_window_open("w_icon", 3, 3, get_form_path("f_qxt_icon_det_l2"), FALSE) 
    CALL populate_icon_form_edit_labels_g()
    CALL icon_preview(l_icon_rec.icon_filename)


  #Call the INPUT
  CALL icon_input(l_icon_rec.*) 
    RETURNING l_icon_rec.*

  CALL fgl_window_close("w_icon")


  IF local_debug THEN
    DISPLAY "icon_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "icon_create() - l_icon_rec.icon_filename=",       l_icon_rec.icon_filename
      DISPLAY "icon_create() - l_icon_rec.icon_category1_id=",   l_icon_rec.icon_category1_id
      DISPLAY "icon_create() - l_icon_rec.icon_category2_id=",   l_icon_rec.icon_category2_id
      DISPLAY "icon_create() - l_icon_rec.icon_category3_id=",   l_icon_rec.icon_category3_id

    END IF


    #Check if the primary key has changed
    IF NOT (l_icon_rec.icon_filename = l_key1_icon_filename) THEN

      #Create a new entry (key has changed)
      INSERT INTO qxt_icon VALUES (
        l_icon_rec.icon_filename,
        l_icon_rec.icon_category1_id,
        l_icon_rec.icon_category2_id,
        l_icon_rec.icon_category3_id
                                 )
      #Update all child table entries with the new key information
      UPDATE qxt_tbi_obj
        SET icon_filename   = l_icon_rec.icon_filename
      WHERE icon_filename   = l_key1_icon_filename

      #Delete the original row
      DELETE FROM qxt_icon
      WHERE icon_filename       = l_key1_icon_filename

    ELSE

      UPDATE qxt_icon
        SET 
          icon_filename =         l_icon_rec.icon_filename,
          icon_category1_id =     l_icon_rec.icon_category1_id,
          icon_category2_id =     l_icon_rec.icon_category2_id,
          icon_category3_id =     l_icon_rec.icon_category3_id

        WHERE qxt_icon.icon_filename = l_key1_icon_filename

    END IF

    IF local_debug THEN
      DISPLAY "icon_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(870),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(870),NULL)
      RETURN l_icon_rec.icon_filename
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(870),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF



END FUNCTION



#################################################
# FUNCTION icon_input(p_icon_rec)
#
# Input icon details (edit/create)
#
# RETURN l_icon.*
#################################################
FUNCTION icon_input(p_icon_rec)
  DEFINE 
    p_icon_rec                  RECORD LIKE qxt_icon.*,
    l_icon_form_rec             OF t_qxt_icon_form_rec,
    l_orignal_icon_filename     LIKE qxt_icon.icon_filename,
    l_tmp_filename              VARCHAR(250),
    local_debug                 SMALLINT,
    tmp_str                     VARCHAR(250),
    l_category_data             LIKE qxt_icon_category.icon_category_data,
    l_category_id               LIKE qxt_icon_category.icon_category_id,
    l_language_id               LIKE qxt_icon_category.language_id,
    l_icon_category_rec         RECORD LIKE qxt_icon_category.*,
    l_current_field_name        VARCHAR(30)


  LET local_debug = FALSE

  LET int_flag = FALSE

  #Initialise some variables
  LET l_language_id = get_language()

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_icon_filename = p_icon_rec.icon_filename

  #copy record data to form_record format record
  CALL copy_icon_record_to_form_record(p_icon_rec.*) RETURNING l_icon_form_rec.*

  ####################
  #Start actual INPUT
  INPUT BY NAME l_icon_form_rec.* WITHOUT DEFAULTS HELP 1   
    BEFORE INPUT

      IF l_icon_form_rec.icon_filename IS NULL THEN
        #"select the file"
        CALL fgl_file_dialog("open", 0, get_str(273), l_icon_form_rec.icon_filename, l_icon_form_rec.icon_filename, "Ico (*.ico)|*.ico|Jpg (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
          RETURNING l_tmp_filename
        LET l_icon_form_rec.icon_filename = fgl_basename(l_tmp_filename)
        DISPLAY BY NAME l_icon_form_rec.icon_filename
      END IF

      #Refresh icon preview
      CALL icon_preview(l_icon_form_rec.icon_filename)
  

    #######################################
    # Action / Key Section
    #######################################

    #Icon Filename Section
    ON ACTION("selectIconFileName")
      #"select the file"
      CALL fgl_file_dialog("open", 0, get_str(273), l_icon_form_rec.icon_filename, l_icon_form_rec.icon_filename, "Ico (*.ico)|*.ico|Jpg (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
        RETURNING l_tmp_filename
      IF l_tmp_filename IS NOT NULL THEN  --user pressed cancel
        LET l_icon_form_rec.icon_filename = fgl_basename(l_tmp_filename)
        DISPLAY BY NAME l_icon_form_rec.icon_filename
      END IF

      #Refresh icon preview
      CALL icon_preview(l_icon_form_rec.icon_filename)
  

    ON ACTION ("editIconCategory") --edit an icon category and add the entry to the combo list
      LET l_current_field_name = fgl_dialog_getfieldname()
      LET l_category_id = get_current_field_icon_category_id(l_icon_form_rec.*,l_current_field_name,l_language_id)
      #LET l_category_data = get_icon_category_data(l_category_id,l_language_id)
      CALL icon_category_edit(l_category_id,l_language_id)
        RETURNING l_icon_category_rec.*

      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_icon_combo_list()
        CALL update_category_field(l_icon_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_icon_form_rec.*
      END IF

      
    ON ACTION ("newIconCategory") --create a new icon category and add the entry to the combo list

      LET l_current_field_name = fgl_dialog_getfieldname()
      LET l_category_id = get_current_field_icon_category_id(l_icon_form_rec.*,l_current_field_name,l_language_id)
      CALL icon_category_create(NULL,l_language_id,NULL)
        RETURNING l_icon_category_rec.*

      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_icon_combo_list()
        CALL update_category_field(l_icon_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_icon_form_rec.*
      END IF



    ON ACTION ("manageIconCategory") --edit an icon category and add the entry to the combo list
      LET l_current_field_name = fgl_dialog_getfieldname()
      CALL icon_category_popup(get_current_field_icon_category_id(l_icon_form_rec.*,fgl_dialog_getfieldname(),l_language_id),l_language_id ,NULL,0)
        RETURNING l_icon_category_rec.*
      IF l_icon_category_rec.icon_category_id IS NOT NULL THEN
        CALL populate_icon_combo_list()
        CALL update_category_field(l_icon_form_rec.*,l_current_field_name,l_icon_category_rec.icon_category_data)
          RETURNING l_icon_form_rec.*
      END IF

     # CALL @@@ (icon_category_edit(get_current_field_icon_category_id(l_icon_form_rec.*,fgl_dialog_getfieldname()),get_language()),get_language())  
   #   CALL populate_icon_combo_list()

      #Find out, if the cursor is in a category field and update it correspondingly
    #  CALL update_category_field(l_icon_form_rec.*,fgl_dialog_getfieldname(),l_category_data)
     #   RETURNING l_icon_form_rec.*



    


    #######################################
    # BEFORE/AFTER Field Section
    #######################################
    AFTER FIELD icon_filename
      #icon_filename must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(871), l_icon_form_rec.icon_filename,NULL,TRUE)  THEN
        NEXT FIELD icon_filename
      END IF

    BEFORE FIELD icon_category1_data
      DISPLAY "!" TO bt_edit_category
      DISPLAY "!" TO bt_new_category
      DISPLAY "!" TO bt_manage_category

    AFTER FIELD icon_category1_data
      DISPLAY "*" TO bt_edit_category
      DISPLAY "*" TO bt_new_category
      DISPLAY "*" TO bt_manage_category

    BEFORE FIELD icon_category2_data
      DISPLAY "!" TO bt_edit_category
      DISPLAY "!" TO bt_new_category
      DISPLAY "!" TO bt_manage_category

    AFTER FIELD icon_category2_data
      DISPLAY "*" TO bt_edit_category
      DISPLAY "*" TO bt_new_category
      DISPLAY "*" TO bt_manage_category

    BEFORE FIELD icon_category3_data
      DISPLAY "!" TO bt_edit_category
      DISPLAY "!" TO bt_new_category
      DISPLAY "!" TO bt_manage_category

    AFTER FIELD icon_category3_data
      DISPLAY "*" TO bt_edit_category
      DISPLAY "*" TO bt_new_category
      DISPLAY "*" TO bt_manage_category

    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF


      #filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(871), l_icon_form_rec.icon_filename,NULL,TRUE)  THEN
          NEXT FIELD icon_filename
          CONTINUE INPUT
        END IF


      #Primary key contraint validation
      #Check if EDIT (on edit - ignore duplication)

      #IF l_orignal_icon_filename IS NULL THEN --looks like a newly created record
      IF l_orignal_icon_filename IS NULL OR (l_orignal_icon_filename <> l_icon_form_rec.icon_filename) THEN --true = new

        #Double key contraint validation
        IF icon_filename_count(l_icon_form_rec.icon_filename) THEN

          CALL tl_msg_duplicate_key(get_str_tool(871),NULL)
          NEXT FIELD icon_filename
          CONTINUE INPUT
        END IF
      END IF



#      #The icon_filename must be unique
#      IF icon_filename_count(l_icon_form_rec.icon_filename) THEN
#        #Constraint only exists for newly created records (not modify)
#        IF l_orignal_icon_filename = "" OR l_orignal_icon_filename IS NULL THEN  --it is not an edit operation
#          CALL  tl_msg_duplicate(get_str_tool(872),NULL)
#          NEXT FIELD icon_filename
#          CONTINUE INPUT
#        END IF
#      END IF


  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "icon_input() - l_icon_form_rec.icon_filename=",     l_icon_form_rec.icon_filename
    DISPLAY "icon_input() - l_icon_form_rec.icon_category1_data=",    l_icon_form_rec.icon_category1_data
    DISPLAY "icon_input() - l_icon_form_rec.icon_category2_data=",    l_icon_form_rec.icon_category2_data
    DISPLAY "icon_input() - l_icon_form_rec.icon_category3_data=",    l_icon_form_rec.icon_category3_data
  END IF


  #Copy the form record data to a normal icon record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_icon_form_record_to_record(l_icon_form_rec.*) RETURNING p_icon_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "icon_input() - p_icon_rec.icon_filename=",   p_icon_rec.icon_filename

    DISPLAY "icon_input() - p_icon_rec.icon_category1_id=",  p_icon_rec.icon_category1_id
    DISPLAY "icon_input() - p_icon_rec.icon_category2_id=",  p_icon_rec.icon_category2_id
    DISPLAY "icon_input() - p_icon_rec.icon_category3_id=",  p_icon_rec.icon_category3_id

  END IF

  RETURN p_icon_rec.*

END FUNCTION



#################################################
# FUNCTION icon_delete(p_icon_id)
#
# Delete a icon record
#
# RETURN NONE
#################################################
FUNCTION icon_delete(p_icon_filename)
  DEFINE 
    p_icon_filename          LIKE qxt_icon.icon_filename  --key1


  #do you really want to delete...
  IF question_delete_record(get_str_tool(870), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_icon 
        WHERE icon_filename = p_icon_filename
    COMMIT WORK

    RETURN TRUE
  ELSE
    RETURN FALSE
  END IF

END FUNCTION


#################################################
# FUNCTION icon_view(p_icon_filename,p_icon_property_name)
#
# View icon record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_view(p_icon_filename)
  DEFINE 
    p_icon_filename          LIKE qxt_icon.icon_filename,  --key1
    l_icon_rec               RECORD LIKE qxt_icon.*

  CALL get_icon_rec(p_icon_filename) RETURNING l_icon_rec.*
  CALL icon_view_by_rec(l_icon_rec.*)

END FUNCTION


#################################################
# FUNCTION icon_view_by_rec(p_icon_rec)
#
# View icon record in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_view_by_rec(p_icon_rec)
  DEFINE 
    p_icon_rec       RECORD LIKE qxt_icon.*,
    l_icon_form_rec  OF t_qxt_icon_form_rec,
    inp_char         CHAR,
    tmp_str          VARCHAR(250)

	CALL fgl_window_open("w_icon", 3, 3, get_form_path("f_qxt_icon_det_l2"), FALSE) 
	CALL populate_icon_form_view_labels_g()

  CALL copy_icon_record_to_form_record(p_icon_rec.*)  RETURNING l_icon_form_rec.*

  DISPLAY BY NAME p_icon_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_icon")
END FUNCTION



#####################################################################################################
# COUNT functions  (for duplicate / existence checking
#####################################################################################################


####################################################
# FUNCTION icon_count(p_filename)
#
# returns the total number of icon rows
#
# RETURN r_count
####################################################
FUNCTION icon_count()
  DEFINE
    r_count            SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION icon_filename_count(p_filename)
#
# tests if a record with this filename already exists
#
# RETURN r_count
####################################################
FUNCTION icon_filename_count(p_filename)
  DEFINE
    p_filename         LIKE qxt_icon.icon_filename,
    r_count            SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon
      WHERE icon_filename = p_filename

  RETURN r_count   --0 = unique  0> is not

END FUNCTION





#############################################################################################################
# Record / Form Record copy functions
#############################################################################################################

######################################################
# FUNCTION copy_icon_record_to_form_record(p_icon_rec)  
#
# Copy normal icon record data to type icon_form_rec
#
# RETURN l_icon_form_rec.*
######################################################
FUNCTION copy_icon_record_to_form_record(p_icon_rec)  
  DEFINE
    p_icon_rec           RECORD LIKE qxt_icon.*,
    l_icon_form_rec      OF t_qxt_icon_form_rec,
    local_debug          SMALLINT

  LET local_debug = FALSE

  LET l_icon_form_rec.icon_filename           = p_icon_rec.icon_filename
  LET l_icon_form_rec.icon_category1_data     = get_icon_category_data(p_icon_rec.icon_category1_id,get_language())
  LET l_icon_form_rec.icon_category2_data     = get_icon_category_data(p_icon_rec.icon_category2_id,get_language())
  LET l_icon_form_rec.icon_category3_data     = get_icon_category_data(p_icon_rec.icon_category3_id,get_language())


  IF local_debug THEN
    DISPLAY "copy_icon_record_to_form_record() - p_icon_rec.icon_filename=",p_icon_rec.icon_filename
    DISPLAY "copy_icon_record_to_form_record() - p_icon_rec.icon_category1_id",p_icon_rec.icon_category1_id
    DISPLAY "copy_icon_record_to_form_record() - p_icon_rec.icon_category2_id",p_icon_rec.icon_category2_id
    DISPLAY "copy_icon_record_to_form_record() - p_icon_rec.icon_category3_id",p_icon_rec.icon_category3_id

    DISPLAY "copy_icon_record_to_form_record() - l_icon_form_rec.icon_filename=",l_icon_form_rec.icon_filename
    DISPLAY "copy_icon_record_to_form_record() - l_icon_form_rec.icon_category1_data=",l_icon_form_rec.icon_category1_data
    DISPLAY "copy_icon_record_to_form_record() - l_icon_form_rec.icon_category2_data=",l_icon_form_rec.icon_category2_data
    DISPLAY "copy_icon_record_to_form_record() - l_icon_form_rec.icon_category3_data=",l_icon_form_rec.icon_category3_data

  END IF


  RETURN l_icon_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_icon_form_record_to_record(p_icon_form_rec)  
#
# Copy type icon_form_rec to normal icon record data
#
# RETURN l_icon_rec.*
######################################################
FUNCTION copy_icon_form_record_to_record(p_icon_form_rec)  
  DEFINE
    l_icon_rec       RECORD LIKE qxt_icon.*,
    p_icon_form_rec  OF t_qxt_icon_form_rec,
    local_debug      SMALLINT

  LET local_debug = FALSE
  LET l_icon_rec.icon_filename    = p_icon_form_rec.icon_filename
  LET l_icon_rec.icon_category1_id   = get_icon_category_id(p_icon_form_rec.icon_category1_data,get_language())
  LET l_icon_rec.icon_category2_id   = get_icon_category_id(p_icon_form_rec.icon_category2_data,get_language())
  LET l_icon_rec.icon_category3_id   = get_icon_category_id(p_icon_form_rec.icon_category3_data,get_language())


  IF local_debug THEN
    DISPLAY "copy_icon_form_record_to_record() - p_icon_form_rec.icon_filename=",      p_icon_form_rec.icon_filename
    DISPLAY "copy_icon_form_record_to_record() - p_icon_form_rec.icon_category1_data=",     p_icon_form_rec.icon_category1_data
    DISPLAY "copy_icon_form_record_to_record() - p_icon_form_rec.icon_category2_data=",     p_icon_form_rec.icon_category2_data
    DISPLAY "copy_icon_form_record_to_record() - p_icon_form_rec.icon_category3_data=",     p_icon_form_rec.icon_category3_data

    DISPLAY "copy_icon_form_record_to_record() - l_icon_rec.icon_filename=",   l_icon_rec.icon_filename
    DISPLAY "copy_icon_form_record_to_record() - l_icon_rec.icon_category1_id",   l_icon_rec.icon_category1_id
    DISPLAY "copy_icon_form_record_to_record() - l_icon_rec.icon_category2_id",   l_icon_rec.icon_category2_id
    DISPLAY "copy_icon_form_record_to_record() - l_icon_rec.icon_category3_id",   l_icon_rec.icon_category3_id

  END IF

  RETURN l_icon_rec.*

END FUNCTION


######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_icon_scroll()
#
# Populate icon grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_icon_scroll()
  CALL fgl_grid_header("sc_icon","icon_filename",          get_str_tool(871),"left", "F13")  --file filename
  CALL fgl_grid_header("sc_icon","icon_category1_data",    get_str_tool(872),"left", "F14")  --category1
  CALL fgl_grid_header("sc_icon","icon_category2_data",    get_str_tool(873),"left", "F15")  --category2
  CALL fgl_grid_header("sc_icon","icon_category3_data",    get_str_tool(874),"left", "F16")  --category3
END FUNCTION

#######################################################
# FUNCTION populate_icon_combo_list()
#
# Populate all combo list boxes
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_combo_list()

  CALL icon_property_combo_list("icon_property_name")
  CALL icon_category_data_combo_list("icon_category1_data",get_language())
  CALL icon_category_data_combo_list("icon_category2_data",get_language())
  CALL icon_category_data_combo_list("icon_category3_data",get_language())

END FUNCTION


#######################################################
# FUNCTION update_all_category_fields(p_icon_form_rec)
#
# Update/Refresh Data for the corresponding field
#
# RETURN p_icon_form_rec.*
#######################################################
FUNCTION update_all_category_fields(p_icon_form_rec)
  DEFINE
    p_icon_form_rec   OF t_qxt_icon_form_rec

  DISPLAY p_icon_form_rec.icon_category1_data TO icon_category1_name
  DISPLAY p_icon_form_rec.icon_category2_data TO icon_category2_name
  DISPLAY p_icon_form_rec.icon_category3_data TO icon_category3_name

END FUNCTION



#######################################################
# FUNCTION update_category_field(p_field_name,p_data)
#
# Update/Refresh Data for the corresponding field
#
# RETURN p_icon_form_rec.*
#######################################################
FUNCTION update_category_field(p_icon_form_rec,p_field_name,p_data)
  DEFINE
    p_icon_form_rec OF t_qxt_icon_form_rec,
    p_field_name    VARCHAR(30),
    p_data          VARCHAR(40),
    local_debug     SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "update_category_field() - p_icon_form_rec.icon_filename = ", p_icon_form_rec.icon_filename
    DISPLAY "update_category_field() - p_icon_form_rec.icon_category1_data = ", p_icon_form_rec.icon_category1_data
    DISPLAY "update_category_field() - p_icon_form_rec.icon_category2_data = ", p_icon_form_rec.icon_category2_data
    DISPLAY "update_category_field() - p_icon_form_rec.icon_category3_data = ", p_icon_form_rec.icon_category3_data
    DISPLAY "update_category_field() - p_field_name= ", p_field_name
    DISPLAY "update_category_field() - p_data= ", p_data
  END IF

  #Find out, if the cursor is in a category field and update it correspondingly
  CASE p_field_name
    WHEN "icon_category1_data"
      DISPLAY p_data TO icon_category1_data
      LET p_icon_form_rec.icon_category1_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category1_data"
        DISPLAY "update_category_field() - p_icon_form_rec.icon_category1_data =  ", p_data
      END IF

    WHEN "icon_category2_data"
      DISPLAY p_data TO icon_category2_data
      LET p_icon_form_rec.icon_category2_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category2_data"
        DISPLAY "update_category_field() - p_icon_form_rec.icon_category2_data =  ", p_data
      END IF

    WHEN "icon_category3_data"
      DISPLAY p_data TO icon_category3_data
      LET p_icon_form_rec.icon_category3_data = p_data

      IF local_debug THEN
        DISPLAY "update_category_field() - DISPLAY ", p_data , " TO icon_category3_data"
        DISPLAY "update_category_field() - p_icon_form_rec.icon_category3_data =  ", p_data
      END IF

    OTHERWISE
      #do nothing
  END CASE

  RETURN p_icon_form_rec.*

END FUNCTION



#######################################################
# FUNCTION get_current_field_icon_category_id(p_icon_form_rec,p_field_name,p_language_id)
#
# Retrieve icon ide from current category field
#
# RETURN p_icon_form_rec.*
#######################################################
FUNCTION get_current_field_icon_category_id(p_icon_form_rec,p_field_name,p_language_id)
  DEFINE
    p_icon_form_rec      OF t_qxt_icon_form_rec,
    p_field_name         VARCHAR(30),
    p_language_id        LIKE qxt_icon_category.language_id,
    l_icon_category_id   LIKE qxt_icon.icon_category1_id,
    err_msg              VARCHAR(200)

  #Find out, if the cursor is in a category field and update it correspondingly
  CASE p_field_name
    WHEN "icon_category1_data"
      RETURN get_icon_category_id(p_icon_form_rec.icon_category1_data,p_language_id)

    WHEN "icon_category2_data"
      RETURN get_icon_category_id(p_icon_form_rec.icon_category2_data,p_language_id)

    WHEN "icon_category3_data"
      RETURN get_icon_category_id(p_icon_form_rec.icon_category3_data,p_language_id)

    OTHERWISE
      LET err_msg = "Invalid field name argument!\np_field_name=",p_field_name
      CALL fgl_winmessage("Error in get_current_field_icon_category_id()",err_msg,"ERROR")
  END CASE

  RETURN NULL

END FUNCTION




#######################################################
# FUNCTION populate_icon_form_labels_g()
#
# Populate icon form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(870) TO lbTitle

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  #Buttons for ext. calls
  #DISPLAY "!" TO bt_edit_category
  #DISPLAY "!" TO bt_new_category
  #DISPLAY "!" TO bt_manage_category
 

  #Initialise icon preview
  CALL icon_preview(NULL)
  CALL populate_icon_combo_list()



END FUNCTION


{
#######################################################
# FUNCTION populate_icon_form_labels_t()
#
# Populate icon form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_labels_t()

  DISPLAY get_str_tool(870) TO lbTitle

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION

}
#######################################################
# FUNCTION populate_icon_form_edit_labels_g()
#
# Populate icon form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(870)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  #Buttons for ext. calls
  #DISPLAY "!" TO bt_edit_category
  #DISPLAY "!" TO bt_new_category
  #DISPLAY "!" TO bt_manage_category
 

  #Initialise icon preview
  CALL icon_preview(NULL)
  CALL populate_icon_combo_list()

END FUNCTION


{
#######################################################
# FUNCTION populate_icon_form_edit_labels_t()
#
# Populate icon form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_edit_labels_t()

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION
}

#######################################################
# FUNCTION populate_icon_form_view_labels_g()
#
# Populate icon form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(870)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  #Buttons for ext. calls
  #DISPLAY "!" TO bt_edit_category
  #DISPLAY "!" TO bt_new_category
  #DISPLAY "!" TO bt_manage_category
 

  #Initialise icon preview
  CALL icon_preview(NULL)
  CALL populate_icon_combo_list()

END FUNCTION


{
#######################################################
# FUNCTION populate_icon_form_view_labels_t()
#
# Populate icon form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_view_labels_t()

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION

}
#######################################################
# FUNCTION populate_icon_form_create_labels_g()
#
# Populate icon form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(870)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  #Buttons for ext. calls
  #DISPLAY "!" TO bt_edit_category
  #DISPLAY "!" TO bt_new_category
  #DISPLAY "!" TO bt_manage_category
 

  #Initialise icon preview
  CALL icon_preview(NULL)
  CALL populate_icon_combo_list()

END FUNCTION

{

#######################################################
# FUNCTION populate_icon_form_create_labels_t()
#
# Populate icon form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_form_create_labels_t()

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  DISPLAY get_str_tool(875) TO dl_f5
  DISPLAY get_str_tool(876) TO dl_f6
  DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION

}
#######################################################
# FUNCTION populate_icon_list_form_labels_g()
#
# Populate icon list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_list_form_labels_g()
  CALL updateUILabel("icon_category_filter_switch","Category")
  CALL updateUILabel("icon_name_filter_switch","File Name")
  CALL fgl_settitle(trim(get_str_tool(870)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_icon_scroll()

  #DISPLAY get_str_tool(871) TO dl_f1
  #DISPLAY get_str_tool(872) TO dl_f2
  #DISPLAY get_str_tool(873) TO dl_f3
  #DISPLAY get_str_tool(874) TO dl_f4
  #DISPLAY get_str_tool(875) TO dl_f5
  #DISPLAY get_str_tool(876) TO dl_f6
  #DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  #Enable Category Filter check box
  #DISPLAY "!" TO icon_category_filter_switch
  #DISPLAY "!" TO icon_name_filter_switch

  #Enbable button to specify filter criteria
  #DISPLAY "!" TO bt_set_filter_icon_category
  #DISPLAY "!" TO bt_set_filter_icon_name
  #DISPLAY "!" TO bt_set_filter_language_name

  CALL icon_category_data_combo_list("cs1",get_data_language_id())
  CALL icon_category_data_combo_list("cs2",get_data_language_id())
  CALL icon_category_data_combo_list("cs3",get_data_language_id())
  CALL language_combo_list("filter_lang_n")
  #Initialise icon preview
  CALL icon_preview(NULL)

END FUNCTION

{
#######################################################
# FUNCTION populate_icon_list_form_labels_t()
#
# Populate icon list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_list_form_labels_t()

  DISPLAY trim(get_str_tool(870)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(871) TO dl_f1
  DISPLAY get_str_tool(872) TO dl_f2
  DISPLAY get_str_tool(873) TO dl_f3
  DISPLAY get_str_tool(874) TO dl_f4
  #DISPLAY get_str_tool(875) TO dl_f5
  #DISPLAY get_str_tool(876) TO dl_f6
  #DISPLAY get_str_tool(877) TO dl_f7
  #DISPLAY get_str_tool(878) TO dl_f8
  #DISPLAY get_str_tool(879) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION
}

#######################################################
# FUNCTION icon_preview(p_ico_filename)
#
# Displays icons to preview 
#
# RETURN NONE
#######################################################
FUNCTION icon_preview(p_ico_filename)
  DEFINE
    p_ico_filename      VARCHAR(200),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_preview() - p_ico_filename=", p_ico_filename
    DISPLAY "icon_preview() - get_icon16_path(p_ico_filename)", get_icon16_path(p_ico_filename)
    DISPLAY "icon_preview() - get_icon24_path(p_ico_filename)", get_icon24_path(p_ico_filename)
    DISPLAY "icon_preview() - get_icon32_path(p_ico_filename)", get_icon32_path(p_ico_filename)
    DISPLAY "icon_preview() - validate_file_client_side_cache_exists(get_icon16_path(p_ico_filename), NULL, FALSE)", validate_file_client_side_cache_exists(get_icon16_path(p_ico_filename), NULL, FALSE)
    DISPLAY "icon_preview() - validate_file_client_side_cache_exists(get_icon24_path(p_ico_filename), NULL, FALSE)", validate_file_client_side_cache_exists(get_icon24_path(p_ico_filename), NULL, FALSE)
    DISPLAY "icon_preview() - validate_file_client_side_cache_exists(get_icon32_path(p_ico_filename), NULL, FALSE)", validate_file_client_side_cache_exists(get_icon32_path(p_ico_filename), NULL, FALSE)
 
  END IF

  #Enable image preview
  #DISPLAY "!" TO preview1
  #DISPLAY "!" TO preview2
  #DISPLAY "!" TO preview3

  #Check for NULL filename
  IF p_ico_filename IS NULL THEN
    DISPLAY get_icon16_path("missing_16x16.ico") TO preview1
    DISPLAY get_icon24_path("missing_24x24.ico") TO preview2
    DISPLAY get_icon32_path("missing_32x32.ico") TO preview3

    IF local_debug THEN
      DISPLAY "icon_preview() - NULL-1 DISPLAY 16 Pixel ICON: ", get_icon16_path("missing_16x16.ico")
      DISPLAY "icon_preview() - NULL-1 DISPLAY 24 Pixel ICON: ", get_icon24_path("missing_24x24.ico")
      DISPLAY "icon_preview() - NULL-1 DISPLAY 32 Pixel ICON: ", get_icon32_path("missing_32x32.ico")
    END IF

    RETURN
  END IF


  #icon 16x16
  IF validate_file_client_side_cache_exists(get_icon16_path(p_ico_filename), NULL, FALSE) THEN
    DISPLAY get_icon16_path(p_ico_filename) TO preview1
    IF local_debug THEN
      DISPLAY "icon_preview() - DISPLAY 16 Pixel ICON: ", get_icon16_path(p_ico_filename)
    END IF

  ELSE
    DISPLAY get_icon16_path("missing_16x16.ico") TO preview1
    IF local_debug THEN
      DISPLAY "icon_preview() - NULL-2 DISPLAY 16 Pixel ICON: ", get_icon16_path("missing_16x16.ico")
    END IF
  END IF

  #icon 24x24
  IF validate_file_client_side_cache_exists(get_icon24_path(p_ico_filename), NULL, FALSE) THEN
    DISPLAY get_icon24_path(p_ico_filename) TO preview2
    IF local_debug THEN
      DISPLAY "icon_preview() - DISPLAY 24 Pixel ICON: ", get_icon24_path(p_ico_filename)
    END IF
  ELSE
    DISPLAY get_icon24_path("missing_24x24.ico") TO preview2
    IF local_debug THEN
      DISPLAY "icon_preview() - NULL-2 DISPLAY 24 Pixel ICON: ", get_icon24_path("missing_24x24.ico")
    END IF
  END IF

  #icon 32x32
  IF validate_file_client_side_cache_exists(get_icon32_path(p_ico_filename), NULL, FALSE) THEN
    DISPLAY get_icon32_path(p_ico_filename) TO preview3
    IF local_debug THEN
      DISPLAY "icon_preview() - DISPLAY 32 Pixel ICON: ", get_icon32_path(p_ico_filename)
    END IF
  ELSE
    DISPLAY get_icon32_path("missing_32x32.ico") TO preview3
    IF local_debug THEN
      DISPLAY "icon_preview() - NULL-2 DISPLAY 32 Pixel ICON: ", get_icon32_path("missing_32x32.ico")
    END IF
  END IF


END FUNCTION
###########################################################################################################################
# EOF
###########################################################################################################################















