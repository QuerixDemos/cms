##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_html_url_map_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_help_url_map_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_html_url_map_lib_info()
  RETURN "DB - qxt_db_help_html_url_map"
END FUNCTION 


##################################################################################################
# Init & Data import functions
##################################################################################################


############################################################
# FUNCTION init_help_url_maps()
#
# Initialises help_url_map settings
#
# RETURN NONE
############################################################
FUNCTION init_help_html_url_map(p_filename)
  DEFINE
    p_filename VARCHAR(100)

  #Just a dummy function
  # only required to be compatible with the 'file' help_url_map library
  #THis DB library does not need to import anything...

END FUNCTION

###########################################################################################
# Data Acces Functions
###########################################################################################


###########################################################
# FUNCTION get_help_html_url_map_joint_url(p_language_id,p_help_url_map_id)
#
# get the main url with extension
#
# RETURN url
###########################################################

FUNCTION get_help_html_url_map_joint_url(p_language_id,p_help_url_map_id)
  DEFINE
    p_language_id         LIKE qxt_help_url_map.language_id,
    p_help_url_map_id     LIKE qxt_help_url_map.map_id,
    l_help_url_map_rec    RECORD LIKE qxt_help_url_map.*,
    url                   VARCHAR(200)

  SELECT *
  INTO   l_help_url_map_rec
  FROM   qxt_help_url_map
  WHERE language_id = p_language_id
    AND map_id = p_help_url_map_id

  LET url = l_help_url_map_rec.map_fname, l_help_url_map_rec.map_ext


  RETURN url

END FUNCTION

#############################################################
# FUNCTION get_help_html_url_map_id(p_language_id,p_help_url_map_fname)
#
# Returns the help_url_map_id from a help_url_map (name)
#
# RETURN qxt_help_url_map_rec[id].help_url_map_id
#############################################################
FUNCTION get_help_html_url_map_id(p_language_id,p_help_url_map_fname)
  DEFINE 
    p_language_id         LIKE qxt_help_url_map.language_id,
    p_help_url_map_fname  LIKE qxt_help_url_map.map_fname,
    l_help_url_map_id     LIKE qxt_help_url_map.map_id


  SELECT map_id
    INTO l_help_url_map_id
    FROM qxt_help_url_map
    WHERE language_id = p_language_id

  RETURN l_help_url_map_id
END FUNCTION


###########################################################
# FUNCTION get_help_html_url_language_id(p_help_url_map_id,p_help_url_map_fname)
#
# get l_language_id from url map row
#
# RETURN l_language_id
###########################################################
FUNCTION get_help_html_url_language_id(p_help_url_map_id,p_help_url_map_fname)
  DEFINE 
    p_help_url_map_id     LIKE qxt_help_url_map.map_id,
    p_help_url_map_fname  LIKE qxt_help_url_map.map_fname,
    l_language_id         LIKE qxt_help_url_map.language_id

  SELECT language_id
    INTO l_language_id
    FROM qxt_help_url_map
    WHERE map_id = p_help_url_map_id
    AND map_fname = p_help_url_map_fname

  RETURN l_language_id
END FUNCTION




###########################################################
# FUNCTION get_help_html_url_map_fname(p_language_id,p_help_url_map_id)
#
# get help_url_map fnameectory from p_help_url_map_id i.e. de,sp,fr,...
#
# RETURN l_help_url_map_fname
###########################################################
FUNCTION get_help_html_url_map_fname(p_language_id,p_help_url_map_id)
  DEFINE 
    p_help_url_map_id      LIKE qxt_help_url_map.map_id,
    p_language_id          LIKE qxt_help_url_map.language_id,
    l_help_url_map_fname   LIKE qxt_help_url_map.map_fname

  SELECT map_fname
    INTO l_help_url_map_fname
    FROM qxt_help_url_map
    WHERE map_id = p_help_url_map_id
      AND language_id = p_language_id

  RETURN l_help_url_map_fname
END FUNCTION


###########################################################
# FUNCTION get_help_html_url_map_ext(p_language_id,p_help_url_map_id)
#
# get help_url_map url extension from p_help_url_map_id (ie. for a goto jump with in a html file) 
#
# RETURN l_help_url_map_ext
###########################################################
FUNCTION get_help_html_url_map_ext(p_language_id,p_help_url_map_id)
  DEFINE 
    p_help_url_map_id      LIKE qxt_help_url_map.map_id,
    p_language_id          LIKE qxt_help_url_map.language_id,
    l_help_url_map_ext     LIKE qxt_help_url_map.map_ext

  SELECT map_ext
    INTO l_help_url_map_ext
    FROM qxt_help_url_map
    WHERE map_id = p_help_url_map_id
      AND language_id = p_language_id

  RETURN l_help_url_map_ext
END FUNCTION


###########################################################
# FUNCTION get_help_html_url_map_rec(p_language_id,p_help_url_map_id)
#
# get help_url_map record from p_help_url_map_id
#
# RETURN l_help_url_map_rec
###########################################################
FUNCTION get_help_html_url_map_rec(p_language_id,p_help_url_map_id)
  DEFINE 
    p_help_url_map_id      LIKE qxt_help_url_map.map_id,
    p_language_id          LIKE qxt_help_url_map.language_id,
    l_help_url_map_rec     RECORD LIKE qxt_help_url_map.*

  SELECT *
    INTO l_help_url_map_rec
    FROM qxt_help_url_map
    WHERE map_id = p_help_url_map_id
      AND language_id = p_language_id

  RETURN l_help_url_map_rec.*
END FUNCTION



########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION help_html_url_map_combo_list(cb_field_name)
#
# Populates the combo box with help_url_maps
#
# RETURN NONE
###################################################################################
FUNCTION help_html_url_map_combo_list(p_language_id,cb_field_name)
  DEFINE 
    p_language_id           LIKE qxt_help_url_map.language_id,
    cb_field_name           VARCHAR(20),   --form field name for the country combo list field
    l_help_html_url_map_rec RECORD LIKE qxt_help_url_map.*, 
    #l_help_url_map_arr     DYNAMIC ARRAY OF t_qxt_help_url_map_rec,
    row_count               INTEGER,
    current_row             INTEGER,
    abort_flag             SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "help_url_map_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_help_url_map_scroll2 CURSOR FOR 
    SELECT  *
      FROM  qxt_help_url_map
      WHERE language_id = p_language_id

  LET int_flag = FALSE
  LET row_count = 0

  FOREACH c_help_url_map_scroll2 INTO l_help_html_url_map_rec.*
    CALL fgl_list_set(cb_field_name,row_count, l_help_html_url_map_rec.map_fname)
    IF local_debug THEN
      DISPLAY "help_url_map_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_help_html_url_map_rec.map_fname), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

	LET row_count = row_count - 1
	
  RETURN row_count

END FUNCTION


###################################################################################
# FUNCTION help_html_url_map_id_combo_list(cb_field_name)
#
# Populates the combo box with help_url_map ids
#
# RETURN NONE
###################################################################################
FUNCTION help_html_url_map_id_combo_list(p_language_id,cb_field_name)
  DEFINE 
    p_language_id          LIKE qxt_help_url_map.language_id,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    l_help_html_url_map_rec RECORD LIKE qxt_help_url_map.*, 
    #l_help_url_map_arr DYNAMIC ARRAY OF t_qxt_help_url_map_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    abort_flag   SMALLINT,
    local_debug   SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "help_url_map_id_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_help_url_map_scroll4 CURSOR FOR 
    SELECT * 
      FROM qxt_help_url_map
      WHERE language_id = p_language_id

  LET int_flag = FALSE
  LET row_count = 0

  FOREACH c_help_url_map_scroll4 INTO l_help_html_url_map_rec.*
    CALL fgl_list_set(cb_field_name,row_count, l_help_html_url_map_rec.map_id)
    IF local_debug THEN
      DISPLAY "help_url_map_id_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_help_html_url_map_rec.map_id), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

	LET row_count = row_count - 1
	
  RETURN row_count

END FUNCTION




