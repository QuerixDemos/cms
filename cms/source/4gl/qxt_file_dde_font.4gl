##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_file_dde_font_globals.4gl"



###########################################################
# FUNCTION get_dde_font_lib_info()
#
# Simply returns libray type /  name
#
# RETURN "FILE - qxt_file_dde_font"
###########################################################
FUNCTION get_dde_font_lib_info()
  RETURN "FILE - qxt_file_dde_font"
END FUNCTION 


#####################################################################################
# Init/Config/Import Functions
#####################################################################################

###########################################################
# FUNCTION process_dde_font_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_dde_font_init(p_filename)
  DEFINE 
    p_filename     VARCHAR(100),
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF p_filename IS NULL THEN
    LET p_filename = get_dde_font_excel_unl_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_dde_font_init() - p_filename=", p_filename
  END IF

  CALL qxt_dde_font_import_file(get_unl_path(p_filename))
  #This function must be compatible with the file toolbar library
END FUNCTION


###########################################################
# FUNCTION qxt_dde_font_import_file(p_filename)
#
# Import the Excel dde font file
#
# RETURN NONE
###########################################################
FUNCTION qxt_dde_font_import_file(p_filename)
  DEFINE 
    p_filename              VARCHAR(150),
    temp_font_rec           OF t_qxt_dde_font_rec,
    sql_stmt                CHAR(1000),
    local_debug             SMALLINT,
    err_msg                 VARCHAR(200),
    i                       SMALLINT

  LET local_debug = FALSE

  IF local_Debug THEN
    DISPLAY "1-qxt_dde_font_import_file() - p_filename = ", p_filename
  END IF

  IF p_filename IS NULL THEN
    LET p_filename = get_dde_font_excel_unl_filename()
  END IF

  IF local_Debug THEN
    DISPLAY "2-qxt_dde_font_import_file() - p_filename = ", p_filename
  END IF

  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_dde_font_import_file()\nCan not open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_dde_font_import_file()", err_msg, "error")
    RETURN
  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_dde_font_import_file()\nCan not set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_dde_font_import_file()", err_msg, "error")
    RETURN
  END IF


  LET i = 1
  WHILE fgl_channel_read("stream",temp_font_rec.*) 

    LET i = temp_font_rec.dde_font_id

    LET qxt_dde_short_rec_arr[i].font_name           = temp_font_rec.font_name
    LET qxt_dde_short_rec_arr[i].font_bold           = temp_font_rec.font_bold
    LET qxt_dde_short_rec_arr[i].font_size           = temp_font_rec.font_size
    LET qxt_dde_short_rec_arr[i].strike_through      = temp_font_rec.strike_through
    LET qxt_dde_short_rec_arr[i].super_script        = temp_font_rec.super_script
    LET qxt_dde_short_rec_arr[i].lower_script        = temp_font_rec.lower_script
    LET qxt_dde_short_rec_arr[i].option_1            = temp_font_rec.option_1
    LET qxt_dde_short_rec_arr[i].option_2            = temp_font_rec.option_2
    LET qxt_dde_short_rec_arr[i].option_3            = temp_font_rec.option_3
    LET qxt_dde_short_rec_arr[i].font_color          = temp_font_rec.font_color
    LET qxt_dde_short_rec_arr[i].font_name           = temp_font_rec.font_name


    IF local_debug THEN
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].font_name=",       qxt_dde_short_rec_arr[i].font_name 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].font_bold=",       qxt_dde_short_rec_arr[i].font_bold 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].font_size=",       qxt_dde_short_rec_arr[i].font_size 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].strike_through=",  qxt_dde_short_rec_arr[i].strike_through 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].super_script=",    qxt_dde_short_rec_arr[i].super_script 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].lower_script=",    qxt_dde_short_rec_arr[i].lower_script 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].option_1=",        qxt_dde_short_rec_arr[i].option_1 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].option_2=",        qxt_dde_short_rec_arr[i].option_2 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].option_3=",        qxt_dde_short_rec_arr[i].option_3 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].font_color=",      qxt_dde_short_rec_arr[i].font_color 
      DISPLAY "qxt_dde_font_import_file() - qxt_dde_short_rec_arr[", trim(i), "].font_name=",       qxt_dde_short_rec_arr[i].font_name 
    END IF 

  END WHILE

  #Close the file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_dde_font_import_file()\nCan not close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_dde_font_import_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION



############################################################################################
# Data Access Functions
############################################################################################


###########################################################
# FUNCTION get_excel_dde_font_rec(p_dde_font_id)
#
# Get the excel dde font record
#
# RETURN NONE
###########################################################
FUNCTION get_excel_dde_font_rec(p_dde_font_id)
  DEFINE
    p_dde_font_id SMALLINT

  RETURN qxt_dde_short_rec_arr[p_dde_font_id].*

END FUNCTION


#####################################################################################
# EOF
#####################################################################################



