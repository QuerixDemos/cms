##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the company_type 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                  RETURN
# company_type_combo_list(cb_field_name)           populates the combo box                       NONE
# get_company_type_id(p_ctype_name)                return type_id based on type_name             l_type_id  (LIKE company_type.type_id)
# get_company_type(p_type_id)                      return type_name based on type_id             l_ctype_name (LIKE company_type.ctype_name)
# get_company_type_rec(p_type_id)                  get company_type record from id               l_ctype_rec
# company_type_popup(p_company_type_id)            popup window for type selection               l_company_type_arr[i].type_id OR RETURN p_company_type_id
# company_type_create()                            create new company type record                NONE
# company_type_delete(p_type_id)                   delete company type                           NONE
# company_type_edit(p_type_id)                     edit a company type record                    NONE
# company_type_view_by_rec(p_company_type_rec)            DISPLAY company_type details on a form        NONE
# company_type_input(p_company_type)               enter company type details                    l_company_type.*
# company_type_choose(p_type_id)                   Displays all companies for selection and      company_type_id  type_array[i].type_id OR p_type_id (cancel)
#                                                  returns the selected records 
#
#
############################################################################################################

#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"

###################################################################################
# FUNCTION company_type_combo_list(cb_field_name)
###################################################################################
FUNCTION company_type_combo_list(cb_field_name)

  DEFINE 
    l_company_type_arr DYNAMIC ARRAY OF 
      RECORD
       ctype_name  LIKE company_type.ctype_name  
      END RECORD,
    rv             LIKE company_type.ctype_name,  --return value = found company_type
    row_count      INTEGER,
    current_row    INTEGER,
    cb_field_name  VARCHAR(20),   --form field name for the country combo list field
    abort_flag    SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_cont_type_scroll2 CURSOR FOR 
    SELECT company_type.ctype_name 
      FROM company_type

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_cont_type_scroll2 INTO l_company_type_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_company_type_arr[row_count].ctype_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

		If row_count > 0 THEN
			CALL l_company_type_arr.resize(row_count)  --remove the last item which has no data
		END IF

END FUNCTION

#############################################
# FUNCTION get_company_type_id(p_ctype_name)
#############################################
FUNCTION get_company_type_id(p_ctype_name)
  DEFINE 
    p_ctype_name  LIKE company_type.ctype_name,
    l_type_id     LIKE company_type.type_id

  SELECT company_type.type_id
    INTO l_type_id
    FROM company_type
    WHERE company_type.ctype_name = p_ctype_name

  RETURN l_type_id
END FUNCTION

#############################################
# FUNCTION get_company_type_name(p_type_id)
#############################################
FUNCTION get_company_type_name(p_type_id)
  DEFINE 
    p_type_id    LIKE company_type.type_id,
    l_ctype_name LIKE company_type.ctype_name

  SELECT company_type.ctype_name
    INTO l_ctype_name
    FROM company_type
    WHERE company_type.type_id = p_type_id

  RETURN l_ctype_name

END FUNCTION

#############################################
# FUNCTION get_company_type_rec(p_type_id)
#
# get company_type record from id
#
# RETURN l_ctype_rec
#############################################
FUNCTION get_company_type_rec(p_type_id)
  DEFINE 
    p_type_id    LIKE company_type.type_id,
    l_ctype_rec  RECORD LIKE company_type.*

  SELECT *
    INTO l_ctype_rec
    FROM company_type
    WHERE company_type.type_id = p_type_id

  RETURN l_ctype_rec.*

END FUNCTION


