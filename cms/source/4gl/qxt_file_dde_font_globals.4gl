##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the dde font file library
#
#
#
################################################################################


################################################################################
# GLOBALS
################################################################################
GLOBALS


  DEFINE t_qxt_dde_font_rec TYPE AS
    RECORD
      dde_font_id           SMALLINT,    
      font_name             VARCHAR(40),   -- Arial
      font_bold             VARCHAR(30),      -- false
      font_size             VARCHAR(30),      --  = 10
      strike_through        VARCHAR(30),   
      super_script          VARCHAR(30),      --  = false
      lower_script          VARCHAR(30),   --SMALLINT,      --  = false
      option_1              VARCHAR(30),      --SMALLINT,      --  = false
      option_2              VARCHAR(30),   --SMALLINT,      --  = false
      option_3              VARCHAR(30),     --SMALLINT,      --  = 1
      font_color            VARCHAR(30)      --SMALLINT       --  = 5
    END RECORD


  DEFINE t_qxt_dde_font_short_rec TYPE AS
    RECORD
      #dde_font_id           LIKE qxt_dde_font.dde_font_id,    
      font_name             VARCHAR(40),   -- Arial
      font_bold             VARCHAR(30),      -- false
      font_size             VARCHAR(30),      --  = 10
      strike_through        VARCHAR(30),   
      super_script          VARCHAR(30),      --  = false
      lower_script          VARCHAR(30),   --SMALLINT,      --  = false
      option_1              VARCHAR(30),      --SMALLINT,      --  = false
      option_2              VARCHAR(30),   --SMALLINT,      --  = false
      option_3              VARCHAR(30),     --SMALLINT,      --  = 1
      font_color            VARCHAR(30)      --SMALLINT       --  = 5
    END RECORD

  DEFINE qxt_dde_short_rec_arr DYNAMIC ARRAY OF  t_qxt_dde_font_short_rec

END GLOBALS
