##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the company_type 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                  RETURN
# company_type_combo_list(cb_field_name)           populates the combo box                       NONE
# get_company_type_id(p_ctype_name)                return type_id based on type_name             l_type_id  (LIKE company_type.type_id)
# get_company_type(p_type_id)                      return type_name based on type_id             l_ctype_name (LIKE company_type.ctype_name)
# get_company_type_rec(p_type_id)                  get company_type record from id               l_ctype_rec
# company_type_popup(p_company_type_id)            popup window for type selection               l_company_type_arr[i].type_id OR RETURN p_company_type_id
# company_type_create()                            create new company type record                NONE
# company_type_delete(p_type_id)                   delete company type                           NONE
# company_type_edit(p_type_id)                     edit a company type record                    NONE
# company_type_view_by_rec(p_company_type_rec)            DISPLAY company_type details on a form        NONE
# company_type_input(p_company_type)               enter company type details                    l_company_type.*
# company_type_choose(p_type_id)                   Displays all companies for selection and      company_type_id  type_array[i].type_id OR p_type_id (cancel)
#                                                  returns the selected records 
#
#
############################################################################################################

#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"


########################################################################
# FUNCTION company_type_choose(p_type_id)
#
# Displays all companies for selection and returns the selected records company_type_id
#
# RETURN type_array[i].type_id OR p_type_id (cancel)
########################################################################
FUNCTION company_type_choose(p_type_id)
  DEFINE 
    i                   INTEGER,
    l_type_id,p_type_id LIKE company_type.type_id,
    l_type_name	        LIKE company_type.ctype_name,
    type_array          DYNAMIC ARRAY OF RECORD LIKE company_type.*

  DECLARE c2 CURSOR FOR 
    SELECT * 
    FROM company_type
    ORDER BY user_def, type_id ASC

  LET i = 1

  FOREACH c2 INTO type_array[i].*
    LET i = i + 1
  END FOREACH

    CALL fgl_window_open("w_company_type", 2,2, get_form_path("f_company_type_l2"),FALSE)
    CALL populate_copmany_type_form_labels_g()

		LET i = i - 1
		If i > 0 THEN
			CALL type_array.resize(i)  --remove the last item which has no data
		END IF

  DISPLAY ARRAY type_array TO sa_cont_type.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1

  CALL fgl_window_close("w_company_type")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_type_id   --LET l_type_id = NULL
  ELSE
    LET i = arr_curr()
    RETURN type_array[i].type_id
  END IF

END FUNCTION



#############################################
# FUNCTION company_type_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for company_type_popup()
#
# RETURN NONE
#############################################
FUNCTION company_type_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "type_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM company_type "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_comp_type FROM sql_stmt
  DECLARE c_comp_type CURSOR FOR p_comp_type

END FUNCTION


#############################################
# FUNCTION company_type_popup(p_company_type_id,p_order_field,p_accept_action)
#
# Display company type list for selection and management
#
# RETURN l_company_type_arr[i].type_id OR p_company_type_id
#############################################
FUNCTION company_type_popup(p_company_type_id,p_order_field,p_accept_action)
  DEFINE 
    p_company_type_id              LIKE company_type.type_id,
    l_company_type_arr             DYNAMIC ARRAY OF RECORD LIKE company_type.*,
    i                              INTEGER,
    p_order_field,p_order_field2   VARCHAR(128), 
    local_debug                    SMALLINT,
    p_accept_action                SMALLINT,
    err_msg                        VARCHAR(240),
    l_ord_dir                      SMALLINT

  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC

  IF NOT p_order_field THEN
    LET p_order_field = "type_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF

    CALL fgl_window_open("w_comp_type_scroll", 2, 8, get_form_path("f_company_type_scroll_l2"), FALSE) 
    CALL populate_copmany_type_list_form_labels_g()

 		CALL ui.Interface.setImage("qx://application/icon16/business/company/company-type.png")
		CALL ui.Interface.setText("Company Type") 


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL company_type_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_comp_type INTO l_company_type_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1
    
		IF i > 1 THEN
			CALL l_company_type_arr.resize(i)   --correct the last element of the dynamic array
		END IF    
		
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_company_type_arr TO sa_cont_type.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("CompanyTypeList",0)

      ON KEY(INTERRUPT)  --on cancel, we return the calling argument
        EXIT WHILE

      ON KEY(ACCEPT)  --ON OK events can be customized

        LET i = arr_curr()
        LET i = l_company_type_arr[i].type_id

        CASE p_accept_action

          WHEN 0
            EXIT WHILE
        
          WHEN 1  --view
            CALL company_type_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL company_type_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL company_type_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL company_type_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","activity_type_popup()\nActivity Type Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL activity_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

            OTHERWISE
              LET err_msg = "company_type_popup(p_company_type_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("company_type_popup() - 4GL Source Error",err_msg, "error") 
          END CASE


      ON KEY (F4) -- add
        CALL company_type_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_company_type_arr[i].type_id
        CALL company_type_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_company_type_arr[i].type_id
        CALL company_type_delete(i)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "type_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "ctype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "user_def"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "base_priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_comp_type_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_company_type_id  -- if canceld, return the orignal argument / id
  ELSE 
    LET i = arr_curr()
    RETURN l_company_type_arr[i].type_id
  END IF


END FUNCTION


#############################################
# FUNCTION company_type_create()
#############################################
FUNCTION company_type_create()
  DEFINE 
    l_company_type     RECORD LIKE company_type.*,
    local_debug        SMALLINT

  LET local_debug = 0  --0=off 1=on

  CALL company_type_input(l_company_type.*)
    RETURNING l_company_type.*

    IF local_debug THEN
      DISPLAY "company_type_create() - l_company_type.type_id = ", l_company_type.type_id
      DISPLAY "company_type_create() - l_company_type.ctype_name = ", l_company_type.ctype_name
      DISPLAY "company_type_create() - l_company_type.user_def = ", l_company_type.user_def
      DISPLAY "company_type_create() - l_company_type.base_priority = ", l_company_type.base_priority
    END IF

  IF NOT int_flag THEN 
    LET l_company_type.type_id = 0
    INSERT INTO company_type VALUES (l_company_type.*)
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate


#############################################
# FUNCTION company_type_delete(p_type_id)
#############################################
FUNCTION company_type_delete(p_type_id)
  DEFINE p_type_id INTEGER
  #"Delete!","Are you sure you want to delete this record?
  IF yes_no(get_str(817),get_str(1830)) THEN
    DELETE FROM company_type 
      WHERE type_id = p_type_id
    #Following company type  was deleted:
    LET tmp_str = get_str(1831) CLIPPED, p_type_id
    CALL fgl_winmessage(get_str(817),tmp_str,"info")

  END IF
END FUNCTION


#############################################
# FUNCTION company_type_edit(p_type_id)
#############################################
FUNCTION company_type_edit(p_type_id)
  DEFINE 
    p_type_id      INTEGER,
    l_company_type RECORD LIKE company_type.*

  SELECT company_type.* 
    INTO l_company_type
    FROM company_type
    WHERE company_type.type_id = p_type_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL company_type_input(l_company_type.*) 
    RETURNING l_company_type.*

  IF NOT int_flag THEN
    UPDATE company_type
       SET ctype_name = l_company_type.ctype_name,
           user_def = l_company_type.user_def
    WHERE company_type.type_id = l_company_type.type_id
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION

#############################################
# FUNCTION company_type_input(p_company_type)
#
# Input company type details from form (edit/create)
#
# RETURN company_type.* or 
#############################################
FUNCTION company_type_input(p_company_type)
  DEFINE 
    p_company_type RECORD LIKE company_type.*,
    l_company_type RECORD LIKE company_type.*

  LET l_company_type.* = p_company_type.*

    CALL fgl_window_open("w_company_type", 3, 3, get_form_path("f_company_type_l2"), TRUE)
    CALL populate_copmany_type_form_labels_g()

  LET int_flag = FALSE

  INPUT BY NAME l_company_type.ctype_name,
                l_company_type.base_priority WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_company_type.ctype_name IS NULL THEN
          #"Input Error","The company type name field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(1832),"error")
          CONTINUE INPUT
        END IF
      ELSE
        IF NOT yes_no(get_str(821),get_str(1833)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  CALL fgl_window_close("w_company_type")
  RETURN l_company_type.*
END FUNCTION

#############################################
# FUNCTION company_type_view_by_rec(p_company_type_rec)
#
# DISPLAY company_type details on a form
#
# RETURN NONE
#############################################
FUNCTION company_type_view(p_company_type_id)
  DEFINE 
    l_company_type_rec RECORD LIKE company_type.*,
    p_company_type_id LIKE company_type.type_id

  CALL get_company_type_rec(p_company_type_id) RETURNING l_company_type_rec.*
  CALL company_type_view_by_rec(l_company_type_rec.*)

END FUNCTION


#############################################
# FUNCTION company_type_view_by_rec(p_company_type_rec)
#
# DISPLAY company_type details on a form
#
# RETURN NONE
#############################################
FUNCTION company_type_view_by_rec(p_company_type_rec)
  DEFINE 
    p_company_type_rec RECORD LIKE company_type.*,
    inp_char           CHAR


    CALL fgl_window_open("w_company_type", 3, 3, get_form_path("f_company_type_l2"), TRUE)
    CALL populate_copmany_type_form_labels_g()

    DISPLAY get_str(811) TO bt_ok
    DISPLAY "*" TO bt_cancel


  DISPLAY BY NAME p_company_type_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_company_type")

END FUNCTION

