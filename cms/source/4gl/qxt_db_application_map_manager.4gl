##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the application table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_application_id(p_language_id)  get application id from p_language_id        l_application_id
# get_language_id(category_id)    Get language_id from p_application_id        l_language_id
# get_qxt_application_rec(p_application_id)   Get the application record from p_application_id  l_qxt_application.*
# application_popup_data_source()               Data Source (cursor) for application_popup              NONE
# application_popup                             application selection window                            p_application_id
# (p_application_id,p_order_field,p_accept_action)
# application_combo_list(cb_field_name)         Populates application combo list from db                NONE
# application_create()                          Create a new application record                         NULL
# application_edit(p_application_id)      Edit application record                                 NONE
# application_input(p_qxt_application_rec)    Input application details (edit/create)                 l_qxt_application.*
# application_delete(p_application_id)    Delete a application record                             NONE
# application_view(p_application_id)      View application record by ID in window-form            NONE
# application_view_by_rec(p_qxt_application_rec) View application record in window-form               NONE
# get_application_id_from_language_id(p_language_id)          Get the application_id from a file                      l_application_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# application_id_count(p_application_id)  tests if a record with this application_id already exists r_count
# copy_qxt_application_record_to_form_record        Copy normal application record data to type qxt_application_form_rec   l_qxt_application_form_rec.*
# (p_qxt_application_rec)
# copy_qxt_application_form_record_to_record        Copy type qxt_application_form_rec to normal application record data   l_application_rec.*
# (p_qxt_application_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_application_scroll()              Populate application grid headers                       NONE
# populate_application_form_labels_g()          Populate application form labels for gui                NONE
# populate_application_form_labels_t()          Populate application form labels for text               NONE
# populate_application_form_edit_labels_g()     Populate application form edit labels for gui           NONE
# populate_application_form_edit_labels_t()     Populate application form edit labels for text          NONE
# populate_application_form_view_labels_g()     Populate application form view labels for gui           NONE
# populate_application_form_view_labels_t()     Populate application form view labels for text          NONE
# populate_application_form_create_labels_g()   Populate application form create labels for gui         NONE
# populate_application_form_create_labels_t()   Populate application form create labels for text        NONE
# populate_application_list_form_labels_g()     Populate application list form labels for gui           NONE
# populate_application_list_form_labels_t()     Populate application list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_application_map_globals.4gl"


########################################################################################################
# Data Access Functions
########################################################################################################

#########################################################
# FUNCTION get_qxt_application_rec(p_application_id)
#
# get string record from p_language_id and p_application_id
#
# RETURN l_application_rec.*
#########################################################
FUNCTION get_qxt_application_rec(p_application_id)
  DEFINE 
    p_application_id          LIKE qxt_application.application_id,
    l_application_rec         RECORD LIKE qxt_application.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_application_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_application.*
    INTO l_application_rec.*
    FROM qxt_application
    WHERE qxt_application.application_id = p_application_id

  RETURN l_application_rec.*
END FUNCTION


###################################################################################
# FUNCTION get_application_new_id()
#
# Finds MAX category_id and Returns it +1  (SERIAL emulation) 
#
# RETURN NONE
###################################################################################
FUNCTION get_application_new_id()
  DEFINE
    p_language_id            LIKE qxt_language.language_id,
    l_max_application_id     LIKE qxt_application.application_id

  SELECT  MAX (qxt_application.application_id)
    INTO l_max_application_id
    FROM qxt_application

  RETURN l_max_application_id + 1
END FUNCTION





##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################


####################################################
# FUNCTION application_id_and_language_id_count(p_application_id, p_language_id)
#
# tests if a record with this application_id and language_id already exists
#
# RETURN r_count
####################################################
FUNCTION application_id_and_language_id_count(p_application_id, p_application_name)
  DEFINE
    p_application_id        LIKE qxt_application.application_id,
    p_application_name         LIKE qxt_application.application_name,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_application
      WHERE qxt_application.application_id = p_application_id
        AND qxt_application.application_name = p_application_name
  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION application_name_count(p_application_name)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION application_name_count(p_application_name)
  DEFINE
    p_application_name  LIKE qxt_application.application_name,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_application
      WHERE qxt_application.application_name = p_application_name

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION application_id_count(p_application_id)
#
# tests if a record with this application_id already exists
#
# RETURN r_count
####################################################
FUNCTION application_id_count(p_application_id)
  DEFINE
    p_application_id    LIKE qxt_application.application_id,
    r_count             SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_application
      WHERE qxt_application.application_id = p_application_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION





########################################################################################################
# Combo list and selection list-management functions
########################################################################################################




######################################################
# FUNCTION application_popup_data_source()
#
# Data Source (cursor) for application_popup
#
# RETURN NONE
######################################################
FUNCTION application_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "application_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "application_popup_data_source() - p_ord_dir=", p_ord_dir

  ENd IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "application_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_application.application_id, ",
                 "qxt_application.application_name ",

                 "FROM qxt_application"
                    


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED,  " "
  END IF

  IF local_debug THEN
    DISPLAY "application_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_application FROM sql_stmt
  DECLARE c_application CURSOR FOR p_application

END FUNCTION


######################################################
# FUNCTION application_popup(p_application_id,p_order_field,p_accept_action)
#
# application selection window
#
# RETURN p_application_id
######################################################
FUNCTION application_popup(p_application_id,p_order_field,p_accept_action)
  DEFINE 
    p_application_id             LIKE qxt_application.application_id,  --default return value if user cancels
    l_application_arr            DYNAMIC ARRAY OF t_qxt_application_form_rec,    --RECORD LIKE qxt_application.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_application_id             LIKE qxt_application.application_id,
    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1


  IF local_debug THEN
    DISPLAY "application_popup() - p_application_id=",p_application_id
    DISPLAY "application_popup() - p_order_field=",p_order_field
    DISPLAY "application_popup() - p_accept_action=",p_accept_action
  END IF


#  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_application_scroll", 2, 8, get_form_path("f_qxt_application_scroll_l2"),FALSE) 
    CALL populate_application_list_form_labels_g()
#  ELSE  --text
#    CALL fgl_window_open("w_application_scroll", 2, 8, get_form_path("f_qxt_application_scroll_t"),FALSE) 
#    CALL populate_application_list_form_labels_t()
#  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_application

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL application_create()
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL application_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_application INTO l_application_arr[i].*
      IF local_debug THEN
        DISPLAY "application_popup() - i=",i
        DISPLAY "application_popup() - l_application_arr[i].application_id=",l_application_arr[i].application_id
        DISPLAY "application_popup() - l_application_arr[i].application_name=",l_application_arr[i].application_name

      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 20 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		IF i > 0 THEN
			CALL l_application_arr.resize(i)  --correct the last element of the dynamic array
		END IF
		

    IF local_debug THEN
      DISPLAY "application_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      CALL fgl_window_close("w_application_scroll")
      RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "application_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_application_arr TO sc_application.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL fgl_dialog_setcurrline(1,current_row)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET current_row = i
        LET l_application_id = l_application_arr[i].application_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL application_view(l_application_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL application_edit(l_application_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL application_delete(l_application_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL application_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","application_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "application_popup(p_application_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("application_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL application_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_application_id = l_application_arr[i].application_id

        CALL application_edit(l_application_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_application_id = l_application_arr[i].application_id

        CALL application_delete(l_application_id)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "application_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

   
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "application_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_application_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_application_id 
  ELSE 
    RETURN l_application_id
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION application_create()
#
# Create a new application record
#
# RETURN NULL
######################################################
FUNCTION application_create()
  DEFINE 
    l_application_rec     RECORD LIKE qxt_application.*,
    local_debug           SMALLINT

  LET local_debug = FALSE


#select max column from table
#informix guide to sql

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_l2"), FALSE) 
    CALL populate_application_form_create_labels_g()

#  ELSE
#    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_t"), TRUE) 
#    CALL populate_application_form_create_labels_t()
#  END IF



  LET int_flag = FALSE
  
  #Initialise some variables
  LET l_application_rec.application_id = get_application_new_id()

  IF local_debug THEN
    DISPLAY "application_create() - l_application_rec.application_id=", l_application_rec.application_id
  END IF

  # CALL the INPUT
  CALL application_input(l_application_rec.*)
    RETURNING l_application_rec.*

  CALL fgl_window_close("w_application")

  IF NOT int_flag THEN
    INSERT INTO qxt_application VALUES (
      l_application_rec.application_id,
      l_application_rec.application_name
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_application_rec.application_id
    END IF

    --sqlca.sqlerrd[2]
  
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION application_edit(p_application_id)
#
# Edit application record
#
# RETURN NONE
#################################################
FUNCTION application_edit(p_application_id)
  DEFINE 
    p_application_id            LIKE qxt_application.application_id,
    l_key1_application_id       LIKE qxt_application.application_id,

    l_application_rec           RECORD LIKE qxt_application.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "application_edit() - Function Entry Point"
    DISPLAY "application_edit() - p_application_id=", p_application_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_application_id = p_application_id

  #Get the record (by id)
  CALL get_qxt_application_rec(p_application_id) RETURNING l_application_rec.*


#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_l2"), FALSE) 
    CALL populate_application_form_edit_labels_g()

#  ELSE
#    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_t"), TRUE) 
#    CALL populate_application_form_edit_labels_t()
#  END IF


  #Call the INPUT
  CALL application_input(l_application_rec.*) 
    RETURNING l_application_rec.*

  CALL fgl_window_close("w_application")


  IF local_debug THEN
    DISPLAY "application_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF



  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "application_edit() - l_application_rec.application_id=",l_application_rec.application_id
      DISPLAY "application_edit() - l_application_rec.application_name=",l_application_rec.application_name
      DISPLAY "application_edit() - l_key1_application_id=",l_key1_application_id

    END IF

    #Check if the primary key has changed
    IF l_application_rec.application_id <> l_key1_application_id THEN
      #Key has changed - create new record

      #Create a new entry (key has changed)
      INSERT INTO qxt_application VALUES (
                    l_application_rec.application_id,
                    "temp_@@@_qxt_value"   --Needs a temp value - otherwise, unique & NULL constraint will bang - l_application_rec.application_name
                                        )

      #Update all child table entries with the new key information
      UPDATE qxt_tbi
        SET application_id   = l_application_rec.application_id
      WHERE application_id   = l_key1_application_id

      UPDATE qxt_tb
        SET application_id   = l_application_rec.application_id
      WHERE application_id   = l_key1_application_id

      #Correct the Name which keeps a temp value
      UPDATE qxt_application
        SET application_name   = l_application_rec.application_name
      WHERE application_id     = l_application_rec.application_id


      #Delete the original row
      DELETE FROM qxt_application
      WHERE application_id   = l_key1_application_id

    ELSE

    UPDATE qxt_application
      SET 
            application_name =               l_application_rec.application_name
      WHERE qxt_application.application_id = l_key1_application_id

    END IF

    IF local_debug THEN
      DISPLAY "application_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(990),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(990),NULL)
      RETURN l_application_rec.application_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(990),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION application_input(p_qxt_application_rec)
#
# Input application details (edit/create)
#
# RETURN l_qxt_application.*
#################################################
FUNCTION application_input(p_qxt_application_rec)
  DEFINE 
    p_qxt_application_rec            RECORD LIKE qxt_application.*,
    l_qxt_application_form_rec       OF t_qxt_application_form_rec,
    l_orignal_application_id     LIKE qxt_application.application_id,
    l_orignal_application_name   LIKE qxt_application.application_name,
    local_debug                  SMALLINT,
    tmp_str                      VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "application_input() - p_qxt_application_rec.application_id=",p_qxt_application_rec.application_id
    DISPLAY "application_input() - p_qxt_application_rec.application_name=",p_qxt_application_rec.application_name
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_application_id = p_qxt_application_rec.application_id
  LET l_orignal_application_name = p_qxt_application_rec.application_name

  #copy record data to form_record format record
  CALL copy_qxt_application_record_to_form_record(p_qxt_application_rec.*) RETURNING l_qxt_application_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_qxt_application_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      NEXT FIELD application_name

    AFTER FIELD application_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(991), l_qxt_application_form_rec.application_id,NULL,TRUE)  THEN
        NEXT FIELD application_id
      END IF

      IF l_orignal_application_id IS NULL THEN
        IF application_id_count(l_qxt_application_form_rec.application_id) THEN
          CALL  tl_msg_duplicate_key(get_str_tool(991),NULL)
          NEXT FIELD application_id
        END IF

      ELSE
        IF l_orignal_application_id <> l_qxt_application_form_rec.application_id OR 
           l_orignal_application_name <> l_qxt_application_form_rec.application_name THEN
          IF application_id_count(l_qxt_application_form_rec.application_id) THEN
            CALL  tl_msg_duplicate_key(get_str_tool(991),NULL)
            NEXT FIELD application_id
          END IF
        END IF
      END IF



    AFTER FIELD application_name
      #language_id must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(993), l_qxt_application_form_rec.application_name,NULL,TRUE)  THEN
        NEXT FIELD application_name
      END IF

      IF l_orignal_application_name IS NULL THEN
        IF application_name_count(l_qxt_application_form_rec.application_name) THEN
          CALL  tl_msg_duplicate_key(get_str_tool(992),NULL)
          NEXT FIELD application_name
        END IF

      ELSE
        IF l_orignal_application_id <> l_qxt_application_form_rec.application_id OR
           l_orignal_application_name <> l_qxt_application_form_rec.application_name THEN

          IF application_name_count(l_qxt_application_form_rec.application_name) THEN
            CALL  tl_msg_duplicate_key(get_str_tool(992),NULL)
            NEXT FIELD application_name
          END IF
        END IF
      END IF

    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #application_id must not be empty
        IF NOT validate_field_value_numeric_exists(get_str_tool(991), l_qxt_application_form_rec.application_id,NULL,TRUE)  THEN
          NEXT FIELD application_id
          CONTINUE INPUT
        END IF

      #application_name must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(993), l_qxt_application_form_rec.application_name,NULL,TRUE)  THEN

          NEXT FIELD application_name
          CONTINUE INPUT
        END IF


      #The application_id  must be unique - Primary KEY
      IF application_id_count(l_qxt_application_form_rec.application_id) THEN
        #Constraint only exists for newly created records (not modify)
 
       IF NOT (l_orignal_application_id = l_qxt_application_form_rec.application_id )THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(991),NULL)
          NEXT FIELD application_id
          CONTINUE INPUT
        END IF
      END IF

      #The application name must also be unique
      IF l_orignal_application_name IS NULL THEN
        IF application_name_count(l_qxt_application_form_rec.application_name) THEN
          CALL  tl_msg_duplicate_key(get_str_tool(992),NULL)
          NEXT FIELD application_name
          CONTINUE INPUT
        END IF

      ELSE
        IF l_orignal_application_id <> l_qxt_application_form_rec.application_id OR
           l_orignal_application_name <> l_qxt_application_form_rec.application_name THEN

          IF application_name_count(l_qxt_application_form_rec.application_name) THEN
            CALL  tl_msg_duplicate_key(get_str_tool(992),NULL)
            NEXT FIELD application_name
          END IF
        END IF
      END IF



  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "application_input() - l_qxt_application_form_rec.application_id=",l_qxt_application_form_rec.application_id
    DISPLAY "application_input() - l_qxt_application_form_rec.application_name=",l_qxt_application_form_rec.application_name
  END IF


  #Copy the form record data to a normal application record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_qxt_application_form_record_to_record(l_qxt_application_form_rec.*) RETURNING p_qxt_application_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "application_input() - p_qxt_application_rec.application_id=",p_qxt_application_rec.application_id
    DISPLAY "application_input() - p_qxt_application_rec.application_name=",p_qxt_application_rec.application_name
  END IF

  RETURN p_qxt_application_rec.*

END FUNCTION


#################################################
# FUNCTION application_delete(p_application_id)
#
# Delete a application record
#
# RETURN NONE
#################################################
FUNCTION application_delete(p_application_id)
  DEFINE 
    p_application_id       LIKE qxt_application.application_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(990), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_application 
        WHERE application_id = p_application_id
    COMMIT WORK

  END IF

END FUNCTION


########################################################################################################
# View functions
########################################################################################################


#################################################
# FUNCTION application_view(p_application_id)
#
# View application record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION application_view(p_application_id)
  DEFINE 
    p_application_id         LIKE qxt_application.application_id,
    l_application_rec    RECORD LIKE qxt_application.*


  CALL get_qxt_application_rec(p_application_id) RETURNING l_application_rec.*
  CALL application_view_by_rec(l_application_rec.*)

END FUNCTION


#################################################
# FUNCTION application_view_by_rec(p_qxt_application_rec)
#
# View application record in window-form
#
# RETURN NONE
#################################################
FUNCTION application_view_by_rec(p_qxt_application_rec)
  DEFINE 
    p_qxt_application_rec     RECORD LIKE qxt_application.*,
    inp_char              CHAR,
    tmp_str               VARCHAR(250)

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_l2"), FALSE) 
    CALL populate_application_form_view_labels_g()
#  ELSE
#    CALL fgl_window_open("w_application", 3, 3, get_form_path("f_qxt_application_det_t"), TRUE) 
#    CALL populate_application_form_view_labels_t()
#  END IF

  DISPLAY BY NAME p_qxt_application_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_application")
END FUNCTION




###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_qxt_application_record_to_form_record(p_qxt_application_rec)  
#
# Copy normal application record data to type qxt_application_form_rec
#
# RETURN l_qxt_application_form_rec.*
######################################################
FUNCTION copy_qxt_application_record_to_form_record(p_qxt_application_rec)  
  DEFINE
    p_qxt_application_rec       RECORD LIKE qxt_application.*,
    l_qxt_application_form_rec  OF t_qxt_application_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_qxt_application_form_rec.application_id   = p_qxt_application_rec.application_id
  LET l_qxt_application_form_rec.application_name = p_qxt_application_rec.application_name

  IF local_debug THEN
    DISPLAY "copy_qxt_application_record_to_form_record() - p_qxt_application_rec.application_id=",   p_qxt_application_rec.application_id
    DISPLAY "copy_qxt_application_record_to_form_record() - p_qxt_application_rec.application_name=", p_qxt_application_rec.application_name

    DISPLAY "copy_qxt_application_record_to_form_record() - l_qxt_application_form_rec.application_id=", l_qxt_application_form_rec.application_id
    DISPLAY "copy_qxt_application_record_to_form_record() - l_qxt_application_form_rec.application_name=", l_qxt_application_form_rec.application_name
  END IF

  RETURN l_qxt_application_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_qxt_application_form_record_to_record(p_qxt_application_form_rec)  
#
# Copy type qxt_application_form_rec to normal application record data
#
# RETURN l_application_rec.*
######################################################
FUNCTION copy_qxt_application_form_record_to_record(p_qxt_application_form_rec)  
  DEFINE
    l_application_rec       RECORD LIKE qxt_application.*,
    p_qxt_application_form_rec  OF t_qxt_application_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_application_rec.application_id   = p_qxt_application_form_rec.application_id
  LET l_application_rec.application_name = p_qxt_application_form_rec.application_name

  IF local_debug THEN
    DISPLAY "copy_qxt_application_form_record_to_record() - p_qxt_application_form_rec.application_id=",   p_qxt_application_form_rec.application_id
    DISPLAY "copy_qxt_application_form_record_to_record() - p_qxt_application_form_rec.application_name=", p_qxt_application_form_rec.application_name

    DISPLAY "copy_qxt_application_form_record_to_record() - l_application_rec.application_id=", l_application_rec.application_id
    DISPLAY "copy_qxt_application_form_record_to_record() - l_application_rec.application_name=", l_application_rec.application_name
  END IF

  RETURN l_application_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_application_scroll()
#
# Populate application grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_application_scroll()
  CALL fgl_grid_header("sc_application","application_id",  get_str_tool(991),"center","F13")  --application
  CALL fgl_grid_header("sc_application","application_name",get_str_tool(992),"left","F15")  --application_name

END FUNCTION



#######################################################
# FUNCTION populate_application_form_labels_g()
#
# Populate application form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(990) TO lbTitle

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_application_form_labels_t()
#
# Populate application form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_labels_t()

  DISPLAY get_str_tool(990) TO lbTitle

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_application_form_edit_labels_g()
#
# Populate application form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(990)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION



#######################################################
# FUNCTION populate_application_form_edit_labels_t()
#
# Populate application form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_edit_labels_t()

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_application_form_view_labels_g()
#
# Populate application form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(990)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_application_form_view_labels_t()
#
# Populate application form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_view_labels_t()

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4

  DISPLAY get_str_tool(402) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_application_form_create_labels_g()
#
# Populate application form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(990)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_application_form_create_labels_t()
#
# Populate application form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_application_form_create_labels_t()

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_application_list_form_labels_g()
#
# Populate application list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_application_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(990)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_application_scroll()

  #DISPLAY get_str_tool(991) TO dl_f1
  #DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4


  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete


END FUNCTION


#######################################################
# FUNCTION populate_application_list_form_labels_t()
#
# Populate application list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_application_list_form_labels_t()

  DISPLAY trim(get_str_tool(990)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(991) TO dl_f1
  DISPLAY get_str_tool(992) TO dl_f2
  #DISPLAY get_str_tool(993) TO dl_f3
  #DISPLAY get_str_tool(994) TO dl_f4

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################





