##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

#################################################
# FUNCTION get_contact_name(p_contact_id)
#
# Get the contact name from an id
#
# RETURN r_contact_name
#################################################
FUNCTION get_contact_name(p_contact_id)
  DEFINE p_contact_id   LIKE contact.cont_id
  DEFINE r_contact_name LIKE contact.cont_name

  SELECT cont_name 
    INTO r_contact_name
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN r_contact_name
END FUNCTION


#################################################
# FUNCTION get_contact_fname(p_contact_id)
#
# Get the contact fname from an id
#
# RETURN r_contact_name
#################################################
FUNCTION get_contact_fname(p_contact_id)
  DEFINE p_contact_id   LIKE contact.cont_id
  DEFINE r_contact_fname LIKE contact.cont_fname

  SELECT cont_fname 
    INTO r_contact_fname
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN r_contact_fname
END FUNCTION


#################################################
# FUNCTION get_contact_lname(p_contact_id)
#
# Get the contact lname from an id
#
# RETURN r_contact_name
#################################################
FUNCTION get_contact_lname(p_contact_id)
  DEFINE p_contact_id   LIKE contact.cont_id
  DEFINE r_contact_lname LIKE contact.cont_lname

  SELECT cont_lname 
    INTO r_contact_lname
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN r_contact_lname
END FUNCTION

#################################################
# FUNCTION get_contact_company_id(p_contact_id)
#
# Get the corresponding company_id from a contact_id 
#
# RETURN r_contact_name
#################################################
FUNCTION get_contact_company_id(p_contact_id)
  DEFINE p_contact_id   LIKE contact.cont_id
  DEFINE r_company_id LIKE contact.cont_org

  SELECT cont_org 
    INTO r_company_id
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN r_company_id
END FUNCTION


#################################################
# FUNCTION get_contact_email_address(p_contact_id)
#
# Get the corresponding email address from a contact_id 
#
# RETURN r_contact_name
#################################################
FUNCTION get_contact_email_address(p_contact_id)
  DEFINE p_contact_id   LIKE contact.cont_id
  DEFINE r_email_address LIKE contact.cont_email

  SELECT cont_email 
    INTO r_email_address
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN r_email_address
END FUNCTION


####################################################
# FUNCTION contact_name_count(p_cont_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION contact_name_count(p_cont_name)
  DEFINE
    p_cont_name    LIKE contact.cont_name,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM contact
      WHERE contact.cont_name = p_cont_name

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION contact_id_count(p_cont_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION contact_id_count(p_cont_id)
  DEFINE
    p_cont_id    LIKE contact.cont_id,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM contact
      WHERE contact.cont_id = p_cont_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION


###############################################################################################################
# Contact Record/Data Access Functions
###############################################################################################################



#########################################################
#
# FUNCTION get_contact_short_rec(p_contact_id)
#
# RETURN l_contact_short_rec.*
#
#########################################################

FUNCTION get_contact_short_rec(p_contact_id)
  DEFINE
    p_contact_id          LIKE contact.cont_id,
    l_contact_short_rec   OF t_contact_short_rec

  LOCATE l_contact_short_rec.cont_picture IN MEMORY


  SELECT 
      cont_name, 
      cont_title,
      cont_fname, 
      cont_lname, 
      cont_addr1, 
      cont_addr2,  
      cont_addr3,   
      cont_city,  
      cont_zone,   
      cont_zip,     
      cont_country, 
      cont_phone,
      cont_mobile, 
      cont_fax, 
      cont_email,  
      comp_name,  
      cont_picture,
      cont_notes  
    INTO l_contact_short_rec.*
    FROM contact,company
   WHERE contact.cont_id = p_contact_id
     AND contact.cont_org = company.comp_id



  RETURN l_contact_short_rec.*

END FUNCTION



#########################################################
#
# FUNCTION get_contact_ws_rec(p_contact_id)
#
# RETURN l_contact_ws_rec.*
#
#########################################################
FUNCTION get_contact_ws_rec(p_contact_id)
  DEFINE
    p_contact_id          LIKE contact.cont_id,
    l_contact_ws_rec      OF t_contact_ws_rec

  #LOCATE l_contact_ws_rec.cont_picture IN MEMORY


  SELECT 
      cont_name, 
      cont_title,
      cont_fname, 
      cont_lname, 
      cont_addr1, 
      cont_addr2,  
      cont_addr3,   
      cont_city,  
      cont_zone,   
      cont_zip,     
      cont_country, 
      cont_phone,
      cont_mobile, 
      cont_fax, 
      cont_email,  
      comp_name,  
      #cont_picture,
      cont_notes  
    INTO l_contact_ws_rec.*
    FROM contact,company
   WHERE contact.cont_id = p_contact_id
     AND contact.cont_org = company.comp_id



  RETURN l_contact_ws_rec.*

END FUNCTION



#################################################
# FUNCTION get_contact_id(p_cont_name)
#
# get the contact_id of a contact_name
#
# RETURN l_cont_id
#################################################
FUNCTION get_contact_id(p_cont_name)
  DEFINE p_cont_name LIKE contact.cont_name
  DEFINE l_cont_id LIKE contact.cont_id

  SELECT contact.cont_id
    INTO l_cont_id
    FROM contact
    WHERE contact.cont_name = p_cont_name

  RETURN l_cont_id
END FUNCTION


#################################################
# FUNCTION get_contact_id(p_cont_name)
#
# get the contact_id of a contact_name
#
# RETURN l_cont_id
#################################################
FUNCTION get_contact_id_from_email_address(p_email_address)
  DEFINE 
    p_email_address LIKE contact.cont_email,
    l_cont_id       LIKE contact.cont_id

  SELECT contact.cont_id
    INTO l_cont_id
    FROM contact
    WHERE contact.cont_email = p_email_address

  RETURN l_cont_id
END FUNCTION

{
#################################################
# FUNCTION get_contact_id2(p_name)
#
# Seems to be an identical duplicate... to get_contact_id()
#
# RETURN l_operator
#################################################
FUNCTION get_contact_id2(p_name)
   DEFINE p_name LIKE contact.cont_name
   DEFINE l_operator LIKE mailbox.contact_id

   SELECT contact.cont_id
     INTO l_operator
     FROM contact
     WHERE contact.cont_name = p_name

   RETURN l_operator
END FUNCTION
}



#################################################
# FUNCTION web_get_contact_rec(p_contact_id)
#
# Get the contact record from an id
#
# RETURN l_contact.*
#
# Note: This function is also published as a webservice
#################################################
 FUNCTION web_get_contact_rec(p_contact_id) RETURNS (l_contact)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_contact RECORD LIKE contact.*

  LOCATE l_contact.cont_picture IN MEMORY

  SELECT contact.* 
    INTO l_contact.*
    FROM contact
    WHERE contact.cont_id = p_contact_id

  RETURN l_contact.*
END FUNCTION


#################################################
# FUNCTION web_get_contact_address_rec(p_contact_id)
#
# Get the contact address record from an id
#
# RETURN l_contact.*
#
# Note: This function is also published as a webservice
#################################################
FUNCTION web_get_contact_address_rec(p_contact_id) RETURNS (l_contact)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_contact OF t_contact_address_rec

  SELECT 
    contact.cont_title,
    cont_fname,
    cont_lname,
    cont_addr1,
    cont_addr2,
    cont_addr3,
    cont_city,
    cont_zone,
    cont_zip,
    cont_country,
    cont_phone,
    cont_mobile,
    cont_fax,
    cont_email,
    comp_name  

    INTO l_contact.*
    FROM contact, company
    WHERE contact.cont_id = p_contact_id
      AND contact.cont_org = company.comp_id

  RETURN l_contact.*
END FUNCTION

{
  DEFINE t_contact_address_rec TYPE AS
    RECORD 
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name
    END RECORD
}



