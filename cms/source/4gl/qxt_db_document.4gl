##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_document_manager_globals.4gl"

######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_document_lib_info()
  RETURN "qxt_db_document"
END FUNCTION 



######################################################################################################################
# Data Access Functions
######################################################################################################################

#########################################################
# FUNCTION get_document_filename(document_id)
#
# Get document_id from name
#
# RETURN l_document_filename
#########################################################
FUNCTION get_document_filename(p_document_id)
  DEFINE p_document_id LIKE qxt_document.document_id
  DEFINE l_document_filename LIKE qxt_document.document_filename

  SELECT document_filename
    INTO l_document_filename
    FROM qxt_document
    WHERE document_id = p_document_id

  RETURN l_document_filename
END FUNCTION



######################################################
# FUNCTION get_document_id_from_filename(p_filename)
#
# Get the document_id from a file
#
# RETURN l_document_id
######################################################
FUNCTION get_document_id_from_filename(p_filename)
  DEFINE 
    p_filename    LIKE qxt_document.document_filename,
    l_document_id LIKE qxt_document.document_id,
    local_debug   SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_document_id() - document_filename = ", p_filename
  END IF

    SELECT document_id
      INTO l_document_id
      FROM qxt_document
      WHERE document_filename = p_filename

  IF local_debug THEN
    DISPLAY "get_document_id() - l_document_id = ", l_document_id
  END IF

  RETURN l_document_id

END FUNCTION



######################################################################################################################
# BLOB download/upload functions
######################################################################################################################


######################################################
# FUNCTION download_blob_document_to_client(p_document_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION download_blob_document_to_client(p_document_id,p_client_file_path,p_dialog)
  DEFINE 
    p_document_id       LIKE qxt_document.document_id,
    #l_document_file     OF t_qxt_document_file,
    local_debug         SMALLINT,
    p_client_file_path  VARCHAR(250),
    l_server_file_path  VARCHAR(250),
    p_dialog            SMALLINT,
    l_server_file_blob  BYTE

  LET local_debug = FALSE

  LET l_server_file_path = get_server_blob_temp_path(get_document_filename(p_document_id))  

  #CALL get_blob_document(p_document_id) RETURNING l_document_file.*


  #LET default_file_name = p_image_name CLIPPED, ".jpg"
  #LET local_file_name = default_file_name

  #If argument has NULL, use file dialog to choose target file name
  IF p_dialog THEN
    CALL fgl_file_dialog("save", 0, get_str_tool(65), p_client_file_path, p_client_file_path, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file_path
  ELSE
    LET p_client_file_path = p_client_file_path
  END IF

    IF local_debug THEN
      DISPLAY "download_blob_document_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_document_to_client() - l_server_file_path=", l_server_file_path
    END IF


  IF p_client_file_path IS NOT NULL THEN
    LOCATE l_server_file_blob IN FILE l_server_file_path

    SELECT qxt_document_blob.document_blob
      INTO l_server_file_blob
      FROM qxt_document_blob
      WHERE qxt_document_blob.document_id = p_document_id

    LET p_client_file_path = fgl_download(l_server_file_path, p_client_file_path)

    IF local_debug THEN
      DISPLAY "download_blob_document_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "***************download_blob_document_to_client() - p_client_file_path=", p_client_file_path
      DISPLAY "download_blob_document_to_client() - l_server_file_path=", l_server_file_path


    END IF

    FREE l_server_file_blob
    #FREE l_document_file.document_blob

    RETURN p_client_file_path
  ELSE
    RETURN NULL
  END IF
END FUNCTION



######################################################
# FUNCTION upload_blob_document(p_contact_id, p_contact_name,remote_image_path)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION upload_blob_document(p_client_file,p_dialog)
  DEFINE 
    p_document_id       LIKE qxt_document.document_id,
    l_document_file     OF t_qxt_document_file,
    local_debug         SMALLINT,
    p_client_file       VARCHAR(250),
    l_server_file       VARCHAR(250),
    p_dialog            SMALLINT,
    p_server_file_blob  BYTE

  LET local_debug = FALSE

  IF p_dialog THEN
    #"Please select the file"
    CALL fgl_file_dialog("open", 0, get_str_tool(65), p_client_file, p_client_file, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file
  END IF

  IF p_client_file IS NOT NULL THEN
    # This will copy the file to the CWD. Hubert can tidy this up later
    LET l_server_file = get_server_blob_temp_path(fgl_basename(p_client_file))

    IF NOT fgl_upload(p_client_file, l_server_file) OR NOT fgl_test("e", l_server_file) THEN
      LET l_server_file = ""
    END IF
  END IF


  IF local_debug THEN
    DISPLAY "upload_blob_document() - p_client_file_path=", p_client_file
    DISPLAY "upload_blob_document() - l_server_file_path=", l_server_file
  END IF


  RETURN l_server_file


END FUNCTION


