##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the application table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_application_id(p_language_id)  get application id from p_language_id        l_application_id
# get_language_id(category_id)    Get language_id from p_application_id        l_language_id
# get_qxt_application_rec(p_application_id)   Get the application record from p_application_id  l_qxt_application.*
# application_popup_data_source()               Data Source (cursor) for application_popup              NONE
# application_popup                             application selection window                            p_application_id
# (p_application_id,p_order_field,p_accept_action)
# application_combo_list(cb_field_name)         Populates application combo list from db                NONE
# application_create()                          Create a new application record                         NULL
# application_edit(p_application_id)      Edit application record                                 NONE
# application_input(p_qxt_application_rec)    Input application details (edit/create)                 l_qxt_application.*
# application_delete(p_application_id)    Delete a application record                             NONE
# application_view(p_application_id)      View application record by ID in window-form            NONE
# application_view_by_rec(p_qxt_application_rec) View application record in window-form               NONE
# get_application_id_from_language_id(p_language_id)          Get the application_id from a file                      l_application_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# application_id_count(p_application_id)  tests if a record with this application_id already exists r_count
# copy_qxt_application_record_to_form_record        Copy normal application record data to type qxt_application_form_rec   l_qxt_application_form_rec.*
# (p_qxt_application_rec)
# copy_qxt_application_form_record_to_record        Copy type qxt_application_form_rec to normal application record data   l_application_rec.*
# (p_qxt_application_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_application_scroll()              Populate application grid headers                       NONE
# populate_application_form_labels_g()          Populate application form labels for gui                NONE
# populate_application_form_labels_t()          Populate application form labels for text               NONE
# populate_application_form_edit_labels_g()     Populate application form edit labels for gui           NONE
# populate_application_form_edit_labels_t()     Populate application form edit labels for text          NONE
# populate_application_form_view_labels_g()     Populate application form view labels for gui           NONE
# populate_application_form_view_labels_t()     Populate application form view labels for text          NONE
# populate_application_form_create_labels_g()   Populate application form create labels for gui         NONE
# populate_application_form_create_labels_t()   Populate application form create labels for text        NONE
# populate_application_list_form_labels_g()     Populate application list form labels for gui           NONE
# populate_application_list_form_labels_t()     Populate application list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_application_map_globals.4gl"


########################################################################################################
# Data Access Functions
########################################################################################################


#########################################################
# FUNCTION get_application_id(p_application_name)
#
# get application_name from p_application_id
#
# RETURN l_application_id
#########################################################
FUNCTION get_application_id(p_application_name)
  DEFINE 
    p_application_name          LIKE qxt_application.application_name,
    l_application_id            LIKE qxt_application.application_id,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_application_name() - p_application_name = ", p_application_name
  END IF

  SELECT qxt_application.application_id
    INTO l_application_id
    FROM qxt_application
    WHERE qxt_application.application_name = p_application_name
    

  IF local_debug THEN
    DISPLAY "get_application_name() - l_application_id = ", l_application_id
  END IF

  RETURN l_application_id
END FUNCTION


#########################################################
# FUNCTION get_application_name(p_application_id)
#
# get application_name from p_application_id
#
# RETURN l_application_name
#########################################################
FUNCTION get_application_name(p_application_id)
  DEFINE 
    p_application_id          LIKE qxt_application.application_id,
    l_application_name        LIKE qxt_application.application_name,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_application_name() - p_application_id = ", p_application_id
  END IF

  SELECT qxt_application.application_name
    INTO l_application_name
    FROM qxt_application
    WHERE qxt_application.application_id = p_application_id


  IF local_debug THEN
    DISPLAY "get_application_name() - l_application_name = ", l_application_name
  END IF

  RETURN l_application_name
END FUNCTION




########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION application_name_combo_list(p_cb_field_name)
#
# Populates application_name_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION application_name_combo_list(p_cb_field_name)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    l_application_name        DYNAMIC ARRAY OF LIKE qxt_application.application_name,
    row_count                 INTEGER,
    current_row               INTEGER,
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "application_name_combo_list() - p_cb_field_name=", p_cb_field_name
  END IF
  
  DECLARE c_application_scroll2 CURSOR FOR 
    SELECT qxt_application.application_name
      FROM qxt_application
    ORDER BY qxt_application.application_name ASC 

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_application_scroll2 INTO l_application_name[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_application_name[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_application_name[row_count] CLIPPED, ")"
      DISPLAY "application_name_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


###########################################################################################################################
# EOF
###########################################################################################################################





