##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################


################################################################################
# GLOBALS
################################################################################
GLOBALS

  DEFINE t_qxt_toolbar_item_rec TYPE AS
    RECORD
      #application_id           LIKE qxt_tbi.application_id,
      #tb_name                  LIKE qxt_tb.tb_name,
      #tb_instance              LIKE qxt_tb.tb_instance,
      event_type_id            SMALLINT,
      tbi_event_name           VARCHAR(25),
      tbi_obj_action_id        SMALLINT,
      tbi_scope_id             SMALLINT,
      tbi_position             SMALLINT,
      tbi_static_id            SMALLINT,
      icon_filename            VARCHAR(100),
      string_data              VARCHAR(100)
    END RECORD

  DEFINE t_qxt_toolbar_file_rec TYPE AS
    RECORD
      #application_id           LIKE qxt_tbi.application_id,
      tb_name                  VARCHAR(35),
      tb_instance              SMALLINT,
      event_type_id            SMALLINT,
      tbi_event_name           VARCHAR(25),
      tbi_obj_action_id        SMALLINT,
      tbi_scope_id             SMALLINT,
      tbi_position             SMALLINT,
      tbi_static_id            SMALLINT,
      icon_filename            VARCHAR(100),
      string_id                SMALLINT
    END RECORD

  DEFINE t_qxt_toolbar_tooltip_file_rec TYPE AS
    RECORD
      string_id                SMALLINT,
      language_id              SMALLINT,
      string_data              VARCHAR(100)
    END RECORD


  DEFINE qxt_toolbar_file_rec DYNAMIC ARRAY OF t_qxt_toolbar_file_rec
  DEFINE qxt_toolbar_tooltip_file_rec DYNAMIC ARRAY OF t_qxt_toolbar_tooltip_file_rec


END GLOBALS

