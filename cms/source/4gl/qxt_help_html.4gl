##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

#################################################################################################
# Init/Config functions
#################################################################################################


###########################################################
# FUNCTION process_language_cfg_import(filename)
#
# Import/Process help_html cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_help_html_cfg_import(p_cfg_filename)

DEFINE 
  p_cfg_filename  VARCHAR(150),
  ret SMALLINT,
  local_debug SMALLINT


  LET local_debug = FALSE  --0=off 1=on

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_help_html_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    RETURN NULL
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[Help-Html]
  #read from html help section
  LET qxt_settings.help_html_system_type    = configGetInt(ret, "[Help-Html]", "help_html_system_type")
  LET qxt_settings.help_html_url_map_fname  = configGet(ret,    "[Help-Html]", "help_html_url_map_fname")
  LET qxt_settings.help_html_url_map_tname  = configGet(ret,    "[Help-Html]", "help_html_url_map_tname")
  LET qxt_settings.help_html_base_dir       = configGet(ret,    "[Help-Html]", "help_html_base_dir")
  LET qxt_settings.help_html_base_url       = configGet(ret,    "[Help-Html]", "help_html_base_url")
  LET qxt_settings.help_html_win_x          = configGet(ret,    "[Help-Html]", "help_html_win_x")
  LET qxt_settings.help_html_win_y          = configGet(ret,    "[Help-Html]", "help_html_win_y")
  LET qxt_settings.help_html_win_width      = configGet(ret,    "[Help-Html]", "help_html_win_width")
  LET qxt_settings.help_html_win_height     = configGet(ret,    "[Help-Html]", "help_html_win_height")


  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_icon_cfg_filename = ", p_cfg_filename CLIPPED, "###############################"

    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [Help-Html Section]          x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    DISPLAY "configInit() - qxt_settings.help_html_system_type=",       qxt_settings.help_html_system_type
    DISPLAY "configInit() - qxt_settings.help_html_url_map_fname=",     qxt_settings.help_html_url_map_fname
    DISPLAY "configInit() - qxt_settings.help_html_url_map_tname=",     qxt_settings.help_html_url_map_tname
    DISPLAY "configInit() - qxt_settings.help_html_base_dir=",          qxt_settings.help_html_base_dir
    DISPLAY "configInit() - qxt_settings.help_html_base_url=",          qxt_settings.help_html_base_url
    DISPLAY "configInit() - qxt_settings.help_html_win_x=",             qxt_settings.help_html_win_x
    DISPLAY "configInit() - qxt_settings.help_html_win_y=",             qxt_settings.help_html_win_y
    DISPLAY "configInit() - qxt_settings.help_html_win_width=",         qxt_settings.help_html_win_width
    DISPLAY "configInit() - qxt_settings.help_html_win_height=",        qxt_settings.help_html_win_height

   
  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_help_html_cfg_export(filename)
#
# Export help_html cfg data
#
# RETURN ret
###########################################################
FUNCTION process_help_html_cfg_export(p_cfg_filename)
  DEFINE 
    ret SMALLINT,
    p_cfg_filename VARCHAR(100)

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_help_html_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_help_html_cfg_export()","No cfg file name was specified or found","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_cfg_filename)

  #CALL fgl_channel_set_delimiter(filename,"")

  #[Help-Html]
  #write to html-help section

  CALL configWrite(ret, "[Help-Html]", "help_html_system_type",   qxt_settings.help_html_system_type )
  CALL configWrite(ret, "[Help-Html]", "help_html_url_map_fname", qxt_settings.help_html_url_map_fname )
  CALL configWrite(ret, "[Help-Html]", "help_html_url_map_tname", qxt_settings.help_html_url_map_tname )
  CALL configWrite(ret, "[Help-Html]", "help_html_base_dir",      qxt_settings.help_html_base_dir )
  CALL configWrite(ret, "[Help-Html]", "help_html_base_url",      qxt_settings.help_html_base_url )
  CALL configWrite(ret, "[Help-Html]", "help_html_win_x",         qxt_settings.help_html_win_x )
  CALL configWrite(ret, "[Help-Html]", "help_html_win_y",         qxt_settings.help_html_win_y )
  CALL configWrite(ret, "[Help-Html]", "help_html_win_width",     qxt_settings.help_html_win_width )
  CALL configWrite(ret, "[Help-Html]", "help_html_win_height",    qxt_settings.help_html_win_height )

END FUNCTION


######################################################################################
# Data Access Functions
######################################################################################


###########################################################
# FUNCTION get_help_html_cfg_filename()
#
# Get the icon (i.e. toolbar) configuration file name
#
# RETURN qxt_settings.icon_cfg_filename
###########################################################
FUNCTION get_help_html_cfg_filename()
  RETURN qxt_settings.help_html_cfg_filename
END FUNCTION

###########################################################
# FUNCTION set_icon_help_html_filename(p_fname
#
# Set the help html library configuration file name
#
# RETURN NONE
###########################################################
FUNCTION set_help_html_cfg_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.help_html_cfg_filename = p_fname
END FUNCTION




{
FUNCTION get_help_html(p_id)
  DEFINE
    p_id INTEGER

  RETURN qxt_helphtml[p_id].help_html_file

END FUNCTION
}

############################################################
# FUNCTION get_help_system_type()
#
# Get the qxt_settings.help_system_type
#
# RETURN qxt_settings.help_system_type
############################################################
FUNCTION get_help_html_system_type()
  RETURN qxt_settings.help_html_system_type
END FUNCTION


############################################################
# FUNCTION get_help_html_base_dir()
#
# Get the qxt_settings.help_html_base_dir
#
# RETURN qxt_settings.help_html_base_dir
############################################################
FUNCTION get_help_html_base_dir()
  RETURN qxt_settings.help_html_base_dir
END FUNCTION

############################################################
# FUNCTION get_help_html_base_dir_path(filename)
#
# Get the path for qxt_settings.help_html_base_dir  and the file argument
#
# RETURN ret
############################################################
FUNCTION get_help_html_base_dir_path(filename)
  DEFINE
    filename VARCHAR(100),
    ret VARCHAR(200)

  LET ret = trim(qxt_settings.help_html_base_dir), "/", trim(filename)

  RETURN trim(ret)
END FUNCTION



############################################################
# FUNCTION get_html_base_url()
#
# Get the qxt_settings.help_html_base_url
#
# RETURN qxt_settings.help_html_base_url
############################################################
FUNCTION get_html_base_url()
  RETURN qxt_settings.help_html_base_url
END FUNCTION


############################################################
# FUNCTION get_help_html_base_url_path(filename)
#
# Get the path for qxt_settings.help_html_base_url and the file argument
#
# RETURN ret
############################################################
FUNCTION get_help_html_base_url_path(filename)
  DEFINE
    filename VARCHAR(100),
    ret VARCHAR(200)

  LET ret = trim(qxt_settings.help_html_base_url), "/", trim(filename)

  RETURN trim(ret)
END FUNCTION


############################################################
# FUNCTION get_help_html_win_x()
#
# Get the x position of the help html window
#
# RETURN qxt_settings.help_html_win_x
############################################################
FUNCTION get_help_html_win_x()
  RETURN qxt_settings.help_html_win_x
END FUNCTION


############################################################
# FUNCTION get_help_html_win_y()
#
# Get the y position of the help html window
#
# RETURN qxt_settings.help_html_win_y
############################################################
FUNCTION get_help_html_win_y()
  RETURN qxt_settings.help_html_win_y
END FUNCTION

############################################################
# FUNCTION get_help_html_win_width()
#
# Get the width of the help html window
#
# RETURN qxt_settings.help_html_win_width
############################################################
FUNCTION get_help_html_win_width()
  RETURN qxt_settings.help_html_win_width
END FUNCTION

############################################################
# FUNCTION get_help_html_win_height()
#
# Get the height of the help html window
#
# RETURN qxt_settings.help_html_win_height
############################################################
FUNCTION get_help_html_win_height()
  RETURN qxt_settings.help_html_win_height
END FUNCTION





FUNCTION get_help_system_type_name(p_id)
  DEFINE 
    p_id     SMALLINT,
    err_msg  VARCHAR(200)

  CASE p_id
    WHEN 1
      RETURN "AppServer"
 
    WHEN 2
      RETURN "WebServer"

    OTHERWISE
      LET err_msg = "Error in get_help_system_type_name()\nInvalid boolean value in argument\np_id = ", p_id
      CALL fgl_winmessage("Error in get_help_system_type_name()",err_msg, "error")

  END CASE

  RETURN NULL
END FUNCTION


FUNCTION get_help_system_type_id(p_name)
  DEFINE 
    p_name   VARCHAR(20),
    err_msg  VARCHAR(200)

  LET p_name = downshift(p_name)
  CASE p_name
    WHEN "appserver"
      RETURN 1
 
    WHEN "webserver"
      RETURN 2 

    OTHERWISE
      LET err_msg = "Error in get_help_system_type_id()\nInvalid boolean value in argument\p_name = ", p_name
      CALL fgl_winmessage("Error in get_help_system_type_id()",err_msg, "error")

  END CASE

  RETURN NULL
END FUNCTION


###################################################################################
# FUNCTION help_system_type_combo_list(cb_field_name)
#
# Populates the combo box with enabled/disabled
# Note: uses/requires tool_lib_db OR tool_lib_txt
#
# RETURN NONE
###################################################################################
FUNCTION help_system_type_combo_list(cb_field_name)
  DEFINE 
    cb_field_name VARCHAR(35),   --form field name for the country combo list field
    local_debug   SMALLINT

  LET local_debug = FALSE
  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "help_system_type_combo_list() - cb_field_name =", cb_field_name
  END IF

  CALL fgl_list_set(cb_field_name,1,"AppServer")
  CALL fgl_list_set(cb_field_name,2,"WebServer")

END FUNCTION


