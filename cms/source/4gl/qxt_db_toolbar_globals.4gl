##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS



  DEFINE t_qxt_toolbar_rec TYPE AS
    RECORD
      application_id           LIKE qxt_tbi.application_id,
      tb_name                  LIKE qxt_tb.tb_name,
      tb_instance              LIKE qxt_tb.tb_instance,
      tbi_name                 LIKE qxt_tbi.tbi_name,
      tbi_position             LIKE qxt_tbi.tbi_position
    END RECORD


  DEFINE t_qxt_toolbar_item_rec TYPE AS
    RECORD
      #application_id           LIKE qxt_tbi.application_id,
      #tb_name                  LIKE qxt_tb.tb_name,
      #tb_instance              LIKE qxt_tb.tb_instance,
      event_type_id            LIKE qxt_tbi.event_type_id,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_obj_action_id        LIKE qxt_tbi_obj.tbi_obj_action_id,
      tbi_scope_id             LIKE qxt_tbi.tbi_scope_id,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_id            LIKE qxt_tbi.tbi_static_id,
      icon_filename            LIKE qxt_tbi_obj.icon_filename,
      label_data               LIKE qxt_tbi_tooltip.label_data,
      string_data              LIKE qxt_tbi_tooltip.string_data
    END RECORD




END GLOBALS


