##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# MAIN module of the CMS demo application
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                      RETURN
# MAIN                                        MAIN   (inc. main ring menu)                      NONE
# add_report_types()                          Prepares the different report formats             NONE
# check_cms_db_version()                      Validates the CMS database to ensure the version  NONE
# db_error_information(err_msg, l_cms_info)   displays cms database error information (version) NONE
#
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# MAIN
#
# NONE
######################################################
MAIN
	#read arguments, config files and initialise
	CALL prepareModuleStart()

	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)
  # set help file '1' define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(1)



	############################################################
	#Launch actual child app
	CALL change_password()
	############################################################

  #Clean applications temporary file on the server
  CALL clean_app_server_temp_files()

END MAIN

