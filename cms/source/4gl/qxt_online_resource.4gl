##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Online Resource Functions to access path / file access / file location
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION  get_online_resource_cfg_filename()
#
# Import/Process online resource cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.online_resource_cfg_filename
###########################################################
FUNCTION get_online_resource_cfg_filename()
  RETURN qxt_settings.online_resource_cfg_filename
END FUNCTION


###########################################################
# FUNCTION set_online_resource_cfg_filename(p_filename)
#
# Export/Write online resource cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.online_resource_cfg_filename
###########################################################
FUNCTION set_online_resource_cfg_filename(p_filename)
  dEFINE
    p_filename   VARCHAR(150)

  LET qxt_settings.online_resource_cfg_filename = p_filename
END FUNCTION


###########################################################
# FUNCTION process_online_resource_cfg_import(p_online_resource_cfg_filename)
#
# Import/Process online resource cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_online_resource_cfg_import(p_online_resource_cfg_filename)
DEFINE 
  p_online_resource_cfg_filename  VARCHAR(200),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_import() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF

  IF p_online_resource_cfg_filename IS NULL THEN
    LET p_online_resource_cfg_filename = get_online_resource_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_import() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF

  #Add path
  LET p_online_resource_cfg_filename = get_cfg_path(p_online_resource_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_import() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF


  IF p_online_resource_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_online_resource_cfg_import()","Error in process_online_resource_cfg_import()\nNo import online_resource_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    EXIT PROGRAM
  END IF



############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_online_resource_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[OnlineResource]
  #read from OnlineResource section
  LET qxt_settings.online_demo_path    = configGet(ret, "[OnlineResource]", "online_demo_path")


  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_online_resource_cfg_filename = ", p_online_resource_cfg_filename CLIPPED, "###############################"


    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [OnlineResource] Section               x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    
    DISPLAY "configInit() - qxt_settings.online_demo_path=",         qxt_settings.online_demo_path
   
  END IF

  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_online_resource_cfg_export(p_online_resource_cfg_filename)
#
# Export online resource cfg data
#
# RETURN ret
###########################################################
FUNCTION process_online_resource_cfg_export(p_online_resource_cfg_filename)
  DEFINE 
    p_online_resource_cfg_filename     VARCHAR(100),
    ret                     SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_export() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF

  IF p_online_resource_cfg_filename IS NULL THEN
    LET p_online_resource_cfg_filename = get_online_resource_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_export() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF

  #Add path
  LET p_online_resource_cfg_filename = get_cfg_path(p_online_resource_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_online_resource_cfg_export() - p_online_resource_cfg_filename=", p_online_resource_cfg_filename
  END IF


  IF p_online_resource_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_online_resource_cfg_export()","Error in process_online_resource_cfg_export()\nNo import online_resource_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_online_resource_cfg_filename)  --configInit(filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[OnlineResource]
  #Write to OnlineResource section
  CALL configWrite(ret, "[OnlineResource]", "online_demo_path",       qxt_settings.online_demo_path )


END FUNCTION



