##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


###################################################
# FUNCTION get_supplies_rec(p_suppl_id)
#
# Returns the suply record
#
# RETURN r_supplies.*
###################################################
FUNCTION get_supplies_rec(p_suppl_id)
  DEFINE 
    p_suppl_id   LIKE supplies.suppl_id,
    r_supplies      RECORD LIKE supplies.*

  SELECT supplies.*
    INTO r_supplies.*
    FROM supplies
    WHERE suppl_id = p_suppl_id

  RETURN r_supplies.*

END FUNCTION

###################################################################################
# FUNCTION supply_state_combo_list(cb_field_name, p_supply_id)
#
# Populate supply_state combo list from database
#
# RETURN NONE
###################################################################################
FUNCTION supply_state_combo_list(cb_field_name, p_supply_id)

  DEFINE  
    p_supply_id      INTEGER,
    l_state_id       LIKE state_supply.state_id,
    l_sup_state_id   INTEGER,
    row_count        INTEGER,
    current_row      INTEGER,
    cb_field_name    VARCHAR(20)   --form field name for the supply state combo list field

  LET status_msg[1]  = get_str(2556)
  LET status_msg[2]  = get_str(2557)
  LET status_msg[3]  = get_str(2558)

  IF p_supply_id IS NOT NULL THEN
    SELECT supplies.state
      INTO l_state_id
      FROM supplies
     WHERE supplies.suppl_id = p_supply_id
  ELSE
    LET l_state_id = 0
  END IF

  DECLARE c_sup_state_scroll CURSOR FOR 
   SELECT state_supply.state_id
     FROM state_supply
    WHERE state_id = l_state_id
   UNION
   SELECT state_supply.state_id
     FROM state_supply
    WHERE state_id = l_state_id + 1 
      AND l_state_id <> 2
   UNION
   SELECT state_supply.state_id
     FROM state_supply
    WHERE state_id = 3
      AND l_state_id = 1

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_sup_state_scroll INTO l_sup_state_id
    CALL fgl_list_set(cb_field_name, row_count, status_msg[l_sup_state_id])
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

END FUNCTION



#########################################################
# FUNCTION get_sup_state_id(sup_state_name)
#
# Get supply state id from name
#
# RETURN l_sup_state_id
#########################################################
FUNCTION get_sup_state_id(sup_state_name)
  DEFINE l_sup_stat        LIKE supplies.state
  DEFINE sup_state_name LIKE state_supply.state_name,
         i  INTEGER

  LET status_msg[1]  = get_str(2556)
  LET status_msg[2]  = get_str(2557)
  LET status_msg[3]  = get_str(2558)

  FOR i=1 TO 3
    IF status_msg[i] = sup_state_name THEN
      RETURN i
    END IF
  END FOR

  RETURN l_sup_stat

END FUNCTION




#########################################################
# FUNCTION get_sup_state_name(sup_state_id)
#
# Get supply state name from id
#
# RETURN l_sup_state_name
#########################################################
FUNCTION get_sup_state_name(sup_state_id)

  DEFINE sup_state_id     LIKE state_supply.state_id

  LET status_msg[1]  = get_str(2556)
  LET status_msg[2]  = get_str(2557)
  LET status_msg[3]  = get_str(2558)

  RETURN status_msg[sup_state_id]

END FUNCTION



###################################################
# FUNCTION verify_sup_data(p_supplies)
#
# Verify input data of supply
#
# RETURN TRUE or FALSE
###################################################
FUNCTION verify_sup_data(p_supplies, init_state)

  DEFINE p_supplies RECORD LIKE supplies.*,
    init_state SMALLINT

  IF NOT int_flag THEN

    IF init_state == 2 THEN
      CALL fgl_winmessage(get_str(48),get_str(2561),"error")
      RETURN FALSE
    END IF

    IF init_state == 3 THEN
      CALL fgl_winmessage(get_str(48),get_str(2562),"error")
      RETURN FALSE
    END IF

    IF p_supplies.suppl_date > p_supplies.exp_date THEN
      CALL fgl_winmessage(get_str(48),get_str(2547),"error")
      RETURN FALSE
    END IF

    IF TODAY > p_supplies.exp_date THEN
      CALL fgl_winmessage(get_str(48),get_str(2559),"error")
      RETURN FALSE
    END IF

  END IF

  RETURN TRUE

END FUNCTION



###################################################
# FUNCTION verify_sup_lines(p_row_count)
#
# Verify input stock items of supply
#
# RETURN TRUE or FALSE
###################################################
FUNCTION verify_sup_lines(p_row_count)

  DEFINE i, p_row_count  SMALLINT,
         l_stock_id      LIKE stock_item.stock_id,
         l_err_msg       VARCHAR(250)
	DEFINE l_verified_sup_lines SMALLINT

  CALL fgl_dialog_update_data()

  FOR i=1 TO p_row_count
    LET l_stock_id = NULL

    SELECT stock_id
      INTO l_stock_id
      FROM stock_item
     WHERE stock_id = g_suppl_lines[i].stock_id

    IF l_stock_id IS NULL AND g_suppl_lines[i].stock_id IS NOT NULL THEN 
      LET l_err_msg = get_str(2560), " ", g_suppl_lines[i].stock_id 
      CALL fgl_winmessage(get_str(48),l_err_msg,"error")
      LET l_verified_sup_lines = FALSE
      RETURN l_verified_sup_lines
    END IF
  END FOR

  LET l_verified_sup_lines = TRUE
  RETURN TRUE

END FUNCTION

