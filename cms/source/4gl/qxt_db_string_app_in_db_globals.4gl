##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Application String
#
# Globals file for the string libary 
# Strings are located and accessed from the database
#
################################################################################


################################################################################
# DATABASE
################################################################################
DATABASE cms

################################################################################
# GLOBALS
################################################################################
GLOBALS

  DEFINE
    t_qxt_string_app_rec TYPE AS  
      RECORD
        string_id      LIKE qxt_string_app.string_id,
        language_id    LIKE qxt_string_app.language_id,
        string_data    LIKE qxt_string_app.string_data
      END RECORD

END GLOBALS
