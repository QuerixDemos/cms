##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage Invoices
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTON:                                       RETURN:
# title_lookup()                              Title lookup window for selection                 l_title_arr[current_row].title or p_title
# title_edit(p_title)                         Edit title record                                 NONE
# title_view(p_title_rec)                     Display title rec on form                         NONE
# title_input(p_title)                        Input title record details (edit/create)          l_title.*# title_delete(p_title)                       delete title record                               NONE
# title_create()                              Create new title record                           NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



###################################################################################
# FUNCTION title_popup_data_source()
#
# Data Source (cursor) for title_lookup()
#
# RETURN  NONE
###################################################################################
FUNCTION title_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "title"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM title "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_title_scroll FROM sql_stmt
  DECLARE c_title_scroll CURSOR FOR p_title_scroll




END FUNCTION

###################################################################################
# FUNCTION title_lookup(p_title,p_order_field,p_accept_action)
#
# Title lookup window for selection
#
# RETURN  l_title_arr[current_row].title or p_title
###################################################################################
FUNCTION title_popup(p_title,p_order_field,p_accept_action)
  DEFINE 
    l_title_arr                  DYNAMIC ARRAY OF t_title_rec,
    p_title                      LIKE title.title,  --return value = found invoice_id
   # row_count,row_id             INTEGER,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT
 
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

  #LET rv = NULL

  CALL fgl_window_open("w_title_popup", 3, 3, get_form_path("f_title_scroll_l2"), FALSE)
  CALL populate_title_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/business/employee/employee-type.png")
	CALL ui.Interface.setText("Title")
	
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL title_popup_data_source(p_order_field,get_toggle_switch())

    LET int_flag = FALSE
    LET i = 1

    FOREACH c_title_scroll INTO l_title_arr[i]
      LET i = i + 1
    END FOREACH

    LET i = i - 1

		If i > 0 THEN
			CALL l_title_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF  
		

    DISPLAY ARRAY l_title_arr TO sc_title_scroll.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 1

      BEFORE DISPLAY
        CALL publish_toolbar("TitleList",0)

      ON KEY (ACCEPT)
        LET i = arr_curr()
        #LET i = l_title_arr[i].title

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL title_view(l_title_arr[i].title)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL title_edit(l_title_arr[i].title)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL title_delete(l_title_arr[i].title)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL title_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","industry_type_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        
          OTHERWISE
            LET err_msg = "industry_type_popup(p_ind_type_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("industry_type_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL title_create()
        EXIT DISPLAY

      ON KEY(F5)  --edit
        LET i = arr_curr()
        CALL title_edit(l_title_arr[i].title)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        CALL title_delete(l_title_arr[i].title)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "title"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY
    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_title_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_title
  ELSE
    LET i = arr_curr()
    RETURN l_title_arr[i].title
  END IF

END FUNCTION


#############################################
# FUNCTION title_edit(p_title)
#
# Edit title record
#
# RETURN NONE
#############################################
FUNCTION title_edit(p_title)
  DEFINE p_title LIKE title.title
  DEFINE l_title RECORD LIKE title.*

  SELECT title.* 
    INTO l_title
    FROM title
    WHERE title.title = p_title

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL title_input(l_title.*) 
    RETURNING l_title.*

  IF NOT int_flag THEN
    UPDATE title
       SET title = l_title.title
          # user_def = l_company_type.user_def
    WHERE title.title = p_title
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


#############################################
# FUNCTION title_input(p_title)
#
# Input title record details (edit/create)
#
# RETURN l_title.*
#############################################
FUNCTION title_input(p_title)
  DEFINE p_title RECORD LIKE title.*
  DEFINE l_title RECORD LIKE title.*

  LET l_title.* = p_title.*

	CALL fgl_window_open("w_title", 3, 3, get_form_path("f_title_l2"), FALSE)
	CALL populate_title_form_labels_g()


  INPUT BY NAME l_title.title WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_title.title IS NULL THEN
          #"The 'Title' (i.e. 'Mr.') field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(1260),"error")
          CONTINUE INPUT
        END IF
      ELSE
        #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_str(53),get_str(54)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  CALL fgl_window_close("w_title")

  #LET int_flag = FALSE

  RETURN l_title.*
END FUNCTION


#############################################
# FUNCTION title_view(p_title_rec)
#
# Display title rec on form
#
# RETURN NONE
#############################################
FUNCTION title_view(p_title_rec)
  DEFINE 
    p_title_rec  RECORD LIKE title.*,
    inp_char     CHAR

	CALL fgl_window_open("w_title", 3, 3, get_form_path("f_title_l2"), FALSE)
	CALL populate_title_form_labels_g()

    DISPLAY "*" TO bt_cancel
    DISPLAY get_str(811) TO bt_ok
    CALL fgl_settitle(get_str(1252))


  DISPLAY BY NAME p_title_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_title")

END FUNCTION


#############################################
# FUNCTION title_delete(p_title)
#
# delete title record
#
# RETURN NONE
#############################################
FUNCTION title_delete(p_title)
  DEFINE p_title LIKE title.title

  #"Delete!","Are you sure you want to delete this value?:" || p_title 
  LET tmp_str = get_str(56), " ", p_title
  IF yes_no(get_str(55),tmp_str) THEN
    DELETE FROM title 
      WHERE title.title = p_title
  END IF
END FUNCTION


#############################################
# FUNCTION title_create()
#
# Create new title record
#
# RETURN NONE
#############################################
FUNCTION title_create()
  DEFINE l_title RECORD LIKE title.*

  CALL title_input(l_title.*)
    RETURNING l_title.*

  IF NOT int_flag THEN 
    #LET l_comp.type_id = 0
    INSERT INTO title VALUES (l_title.*)
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


