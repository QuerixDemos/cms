##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_language_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_language_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_language_lib_info()
  RETURN "DB - qxt_db_language"
END FUNCTION 


##################################################################################################
# Init & Data import functions
##################################################################################################


############################################################
# FUNCTION init_languages()
#
# Initialises Language settings
#
# RETURN NONE
############################################################
FUNCTION init_language_list(p_file_name)
  DEFINE 
    p_file_name VARCHAR(50)
  #Just a dummy function
  # only required to be compatible with the 'file' language library

END FUNCTION

###########################################################################################
# Data Acces Functions
###########################################################################################


#############################################################
# FUNCTION get_language_id(p_lang)
#
# Returns the language_id from a language (name)
#
# RETURN qxt_language_rec[id].language_id
#############################################################
FUNCTION get_language_id(p_language_name)
  DEFINE 
    p_language_name LIKE qxt_language.language_name,
    l_language_id   LIKE qxt_language.language_id


  SELECT language_id
    INTO l_language_id
    FROM qxt_language
    WHERE language_name = p_language_name

  RETURN l_language_id
END FUNCTION


###########################################################
# FUNCTION get_language_name(p_language_id)
#
# get language name from p_language_id
#
# RETURN l_language_name
###########################################################
FUNCTION get_language_name(p_language_id)
  DEFINE 
    p_language_id    LIKE qxt_language.language_id,
    l_language_name  LIKE qxt_language.language_name

  SELECT language_name
    INTO l_language_name
    FROM qxt_language
    WHERE language_id = p_language_id

  RETURN l_language_name
END FUNCTION




###########################################################
# FUNCTION get_language_dir(p_language_id)
#
# get language directory from p_language_id i.e. de,sp,fr,...
#
# RETURN l_language_dir
###########################################################
FUNCTION get_language_dir(p_language_id)
  DEFINE 
    p_language_id    LIKE qxt_language.language_id,
    l_language_dir   LIKE qxt_language.language_dir

  SELECT language_dir
    INTO l_language_dir
    FROM qxt_language
    WHERE language_id = p_language_id

  RETURN l_language_dir
END FUNCTION

###########################################################
# FUNCTION get_language_dir_path(p_language_id,p_filename)
#
# get language directory from p_language_id i.e. de,sp,fr,...
#
# RETURN l_language_dir
###########################################################
FUNCTION get_language_dir_path(p_language_id,p_filename)
  DEFINE
    p_language_id    LIKE qxt_language.language_id,
    p_filename       VARCHAR(200),
    l_language_dir   LIKE qxt_language.language_dir,
    ret              VARCHAR(200)
    
  LET ret = get_language_dir(p_language_id) CLIPPED, "/",  p_filename CLIPPED
  RETURN ret

END FUNCTION

###########################################################
# FUNCTION get_language_url(p_language_id)
#
# get language directory from p_language_id i.e. de,sp,fr,...
#
# RETURN l_language_url
###########################################################
FUNCTION get_language_url(p_language_id)
  DEFINE 
    p_language_id    LIKE qxt_language.language_id,
    l_language_url   LIKE qxt_language.language_url

  SELECT language_url
    INTO l_language_url
    FROM qxt_language
    WHERE language_id = p_language_id

  RETURN l_language_url
END FUNCTION


###########################################################
# FUNCTION get_language_url_path(p_language_id,p_filename)
#
# get language url & file name argument path
#
# RETURN l_language_dir
###########################################################
FUNCTION get_language_url_path(p_language_id,p_filename)
  DEFINE
    p_language_id    LIKE qxt_language.language_id,
    p_filename       VARCHAR(200),
    l_language_dir   LIKE qxt_language.language_dir,
    ret              VARCHAR(200)
    
  LET ret = get_language_url(p_language_id) CLIPPED, "/",  p_filename CLIPPED
  RETURN ret

END FUNCTION

###########################################################
# FUNCTION get_language_rec(p_language_id)
#
# get language record from p_language_id
#
# RETURN l_language_rec
###########################################################
FUNCTION get_language_rec(p_language_id)
  DEFINE 
    p_language_id    LIKE qxt_language.language_id,
    l_language_rec   RECORD LIKE qxt_language.*

  SELECT *
    INTO l_language_rec
    FROM qxt_language
    WHERE lang.language_id = p_language_id

  RETURN l_language_rec.*
END FUNCTION



########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION language_combo_list(cb_field_name)
#
# Populates the combo box with languages
#
# RETURN NONE
###################################################################################
FUNCTION language_combo_list(cb_field_name)
  DEFINE 
    l_language_arr DYNAMIC ARRAY OF t_qxt_language_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    cb_field_name VARCHAR(35),   --form field name for the country combo list field
    abort_flag   SMALLINT,
    local_debug   SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "language_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_language_scroll2 CURSOR FOR 
    SELECT qxt_language.* 
      FROM qxt_language

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_language_scroll2 INTO l_language_arr[row_count].*
    CALL fgl_list_set(cb_field_name,row_count, l_language_arr[row_count].language_name)
    IF local_debug THEN
      DISPLAY "language_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_language_arr[row_count].language_name), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

  RETURN row_count

END FUNCTION


###################################################################################
# FUNCTION language_id_combo_list(cb_field_name)
#
# Populates the combo box with language ids
#
# RETURN NONE
###################################################################################
FUNCTION language_id_combo_list(cb_field_name)
  DEFINE 
    l_language_arr DYNAMIC ARRAY OF t_qxt_language_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    abort_flag   SMALLINT,
    local_debug   SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "language_id_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_language_scroll4 CURSOR FOR 
    SELECT qxt_language.* 
      FROM qxt_language

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_language_scroll4 INTO l_language_arr[row_count].*
    CALL fgl_list_set(cb_field_name,row_count, l_language_arr[row_count].language_id)
    IF local_debug THEN
      DISPLAY "language_id_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_language_arr[row_count].language_id), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

END FUNCTION





