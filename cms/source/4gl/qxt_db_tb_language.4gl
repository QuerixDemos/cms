##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


FUNCTION get_data_language_id()
  RETURN qxt_data_language_id
END FUNCTION

FUNCTION set_data_language_id(p_lang_id)
  DEFINE 
    p_lang_id LIKE qxt_language.language_id

  LET qxt_data_language_id = p_lang_id

END FUNCTION



