##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to access help_html_map system variables
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# get_language()                                  Get the current language choice                            language
# set_language(lang)                              Set the current language                                   NONE
# lang_menu()                                     The lang_menu() function allows to specify the language.   ret 
#                                                 Note: language is a simple global SMALLINT which represents 
#                                                 the language 1-english 2-spanisch 3-german 4-french 
#                                                 5-arabic 6-italian, 7-other
#
# 
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


############################################################
# Data Access functions
############################################################



############################################################
# FUNCTION get_help_html_url_map_fname()
#
# Get the qxt_settings.help_html_url_map_fname
#
# RETURN qxt_settings.help_html_url_map_fname
############################################################
FUNCTION get_help_html_url_map_config_fname()
  RETURN qxt_settings.help_html_url_map_fname
END FUNCTION 


############################################################
# FUNCTION get_help_html_url_map_tname()
#
# Get the qxt_settings.help_html_url_map_tname
#
# RETURN qxt_settings.help_html_url_map_tname
############################################################
FUNCTION get_help_html_url_map_tname()
  RETURN qxt_settings.help_html_url_map_tname
END FUNCTION 






