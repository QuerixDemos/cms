##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###############################################
#Function updateUILabel(widget_id, new_text)
#changes text in Radiobutton, Checkbox, Button, Label, TextArea,Calendar,Textfield, Timeeditfield, GroupBox
###############################################
FUNCTION updateUILabel(id,txt)
DEFINE id CHAR(100)
DEFINE txt CHAR(100)
DEFINE TabPage ui.TabPage --setTitle()
DEFINE GrpbBox ui.GroupBox --setTitle()
DEFINE AbstractBool ui.AbstractBoolField --setTitle()
DEFINE AbstractString ui.AbstractStringField --setText()

LET AbstractBool = ui.AbstractBoolField.ForName(id)
LET AbstractString = ui.AbstractStringField.ForName(id)
LET GrpbBox = ui.GroupBox.ForName(id)
LET TabPage = ui.TabPage.ForName(id)
CASE
  WHEN AbstractBool   IS NOT NULL   CALL AbstractBool.setTitle(txt)
  WHEN AbstractString IS NOT NULL   CALL AbstractString.setText(txt)
  WHEN GrpbBox        IS NOT NULL   CALL GrpbBox.SetTitle(txt)
  WHEN TabPage        IS NOT NULL   CALL TabPage.SetTitle(txt)
END CASE
END FUNCTION

#######################################################
#FUNCTION UIRadButListItemText(RadButList_id,RadButListItem_Number,Item's_newText) 
#changes text for RadioButtonListItem
#######################################################
FUNCTION UIRadButListItemText(ListId,ItemNum,txt) 
DEFINE ListId VARCHAR(100)
DEFINE ItemNum INTEGER
DEFINE txt CHAR(100)
DEFINE rbl ui.Radiobuttonlist
DEFINE rbli DYNAMIC ARRAY OF ui.RadioButtonListItem

LET rbl=ui.RadioButtonList.ForName(ListId)
LET rbli=rbl.GetRadioButtonListItems()     
CALL rbli[ItemNum].SetTitle(txt)
END FUNCTION