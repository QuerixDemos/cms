##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

GLOBALS

  DEFINE qxt_glob_int_switch SMALLINT

#-------------------------------------
######## Document Types TYPE ###
#Document Manager DB Required
#------------------------------------- 
  DEFINE t_qxt_document_complete_rec TYPE AS
    RECORD
     document_id LIKE qxt_document.document_id,
     document_filename    LIKE qxt_document.document_filename,
     document_app         LIKE qxt_document.document_app,
     document_category1   LIKE qxt_document.document_category1,
     document_category2   LIKE qxt_document.document_category2,
     document_category3   LIKE qxt_document.document_category3,
     document_mod_date    LIKE qxt_document.document_mod_date,
     document_desc        LIKE qxt_document.document_desc,  
     document_blob        LIKE qxt_document_blob.document_blob        
    END RECORD

  DEFINE t_qxt_document_no_blob_rec TYPE AS
    RECORD
     document_id LIKE qxt_document.document_id,
     document_filename    LIKE qxt_document.document_filename,
     document_app         LIKE qxt_document.document_app,
     document_category1    LIKE qxt_document.document_category1,
     document_category2    LIKE qxt_document.document_category2,
     document_category3    LIKE qxt_document.document_category3,
     document_mod_date    LIKE qxt_document.document_mod_date      
    END RECORD

  DEFINE t_qxt_document_file TYPE AS
    RECORD
     document_filename    LIKE qxt_document.document_filename,
     document_blob        LIKE qxt_document_blob.document_blob
    END RECORD


  DEFINE t_qxt_document_filename_rec TYPE AS
    RECORD
     document_filename    LIKE qxt_document.document_filename  
    END RECORD



END GLOBALS
