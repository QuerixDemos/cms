##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


GLOBALS

  DEFINE 
    qxt_help_html_url_map_array DYNAMIC ARRAY OF
      RECORD
        map_fname    VARCHAR(100),
        map_ext      VARCHAR(100)
      END RECORD


#icon_category TYPE

  DEFINE 
    t_qxt_help_url_map_rec TYPE AS
      RECORD
        map_id       SMALLINT,
        language_id           SMALLINT,
        map_fname    VARCHAR(100),
        map_ext      VARCHAR(100)
      END RECORD




END GLOBALS


