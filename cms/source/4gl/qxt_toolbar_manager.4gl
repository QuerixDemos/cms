##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Graphical toolbar manager
#
# Created:
# 10.10.06 HH - V3 - This set of functions handles the import of toolbar configurations and
# allow to display and remove complete toolbars with a single function call
#
# It is written in a re-usable way. Feel free to integrate it into your 4gl application
# to have fully dynamic and customizable toolbars.
#
#
# FUNCTION                                        DESCRIPTION                                            RETURN
# process_toolbar_init(file_name)                 Import toolbar config file -                           NONE
#                                                 each menu name has one or more menu items 
# publish_toolbar(action_time)                    Publish a group of toolbar menu icons                  NONE
# draw_tb_icon()                                  Draw a single menu item                                NONE
# hide_dialog_navigation_toolbar(choice)          Hide all auto-generated navigation buttons             NONE
# manage_toolbar_list()                           Display a list of all toolbar menu items -             NONE
#                                                 items can also be edited in detail
# copy_t_toolbar_to_t_toolbar_short(tb_rec)       copy t_toolbar data rec to t_toolbar_short             tb_short_rec.* 
# icon_detail_edit(id)                            Edit icon configuration (for a single menu item)       NONE
# icon_browser()                                  Displays the list of all icons (using the lazy way...) icon_list[i]
#
#########################################################################################################

############################################################
# Globals
############################################################
GLOBA LS "qxt_globals.4gl"


###########################################################
# FUNCTION process_toolbar_init(file_name)
#
# Import toolbar config file - each menu name has one or more menu items 
#
# RETURN NONE
###########################################################
FUNCTION process_toolbar_init(file_name)

  #International Toolbar Icons / key labels
  DEFINE
    file_name, tempstr VARCHAR(40),
    local_debug,id,str_length SMALLINT,
    err_msg             VARCHAR(200)

  IF NOT fgl_test("e",file_name) THEN
    LET err_msg = "File Error in process_toolbar_init()\n", get_str_tool(763), " ", file_name, "\n", get_str_tool(49)
    CALL fgl_winmessage(get_str_tool(30) ,err_msg, "error")
    EXIT PROGRAM
  END IF


  LET local_debug = FALSE
  LET id = 1

  CALL fgl_channel_open_file("stream",file_name, "r")
  CALL fgl_channel_set_delimiter("stream","|")

  IF local_debug THEN
    DISPLAY "### start ##### ",file_name, " ########### process_toolbar_init(" , file_name, ")"  
  END IF

  WHILE fgl_channel_read("stream",qxt_toolbar[id].*) 


    #Import Debug Help
    IF local_debug THEN

      DISPLAY "### In Loop ##### ########### process_toolbar_init(" , file_name, ")" 
      DISPLAY "qxt_toolbar[", id, "].m_name:      ", qxt_toolbar[id].m_name
      DISPLAY "qxt_toolbar[", id, "].m_item:      ", qxt_toolbar[id].m_item
      DISPLAY "qxt_toolbar[", id, "].action_time: ", qxt_toolbar[id].action_time
      DISPLAY "qxt_toolbar[", id, "].action:      ", qxt_toolbar[id].action
      DISPLAY "qxt_toolbar[", id, "].dialog:      ", qxt_toolbar[id].dialog
      DISPLAY "qxt_toolbar[", id, "].event:       ", qxt_toolbar[id].event
      DISPLAY "qxt_toolbar[", id, "].txt_en:      ", qxt_toolbar[id].txt_en
      DISPLAY "qxt_toolbar[", id, "].txt_sp:      ", qxt_toolbar[id].txt_sp
      DISPLAY "qxt_toolbar[", id, "].txt_de:      ", qxt_toolbar[id].txt_de
      DISPLAY "qxt_toolbar[", id, "].txt_fr:      ", qxt_toolbar[id].txt_fr
      DISPLAY "qxt_toolbar[", id, "].txt_ar:      ", qxt_toolbar[id].txt_ar
      DISPLAY "qxt_toolbar[", id, "].txt_it:      ", qxt_toolbar[id].txt_it
      DISPLAY "qxt_toolbar[", id, "].txt_ot:      ", qxt_toolbar[id].txt_ot
      DISPLAY "qxt_toolbar[", id, "].image:       ", qxt_toolbar[id].image
      DISPLAY "qxt_toolbar[", id, "].ord:         ", qxt_toolbar[id].ord
      DISPLAY "qxt_toolbar[", id, "].stat:        ", qxt_toolbar[id].stat
    END IF


    IF qxt_toolbar[id].m_name <> "EOF" AND qxt_toolbar[id].m_name IS NOT NULL THEN   --Terminating on EOF or empty menu name
      IF qxt_toolbar[id].action = 1 THEN
        IF NOT fgl_test("e",qxt_toolbar[id].image) THEN   --check if the icon exists on the server
          LET err_msg = get_str_tool(851) CLIPPED, " ID ", id CLIPPED, "\n", get_str_tool(854) CLIPPED, "  ", qxt_toolbar[id].image  CLIPPED, "\n", get_str_tool(852) CLIPPED  ,": ", qxt_toolbar[id].m_name  CLIPPED, " ", get_str_tool(853)  ," : ", qxt_toolbar[id].m_item
          CALL fgl_winmessage(get_str_tool(851),err_msg, "error")
          IF local_debug THEN
            DISPLAY "Error - Could not find toolbar icon [",id CLIPPED,"] image:>", qxt_toolbar[id].image  CLIPPED, "< men-name: >", qxt_toolbar[id].m_name  CLIPPED, "< men-item: >", qxt_toolbar[id].m_item  CLIPPED , "<"
          END IF
        END IF
      END IF
      LET id = id + 1
    ELSE
      EXIT WHILE
    END IF
  END WHILE

  LET qxt_toolbar_array_size = id - 1


  IF local_debug THEN
    DISPLAY "### END of file import. used array size = " , qxt_toolbar_array_size, "  ##### ",file_name, " ###########"   
  END IF


  CALL fgl_channel_close("stream")

END FUNCTION


###########################################################
# FUNCTION export_toolbar_init(file_name)
#
# Export toolbar config file - each menu name has one or more menu items 
#
# RETURN NONE
###########################################################
FUNCTION export_toolbar_init(file_name)

  #International Toolbar Icons / key labels
  DEFINE
    file_name, tempstr VARCHAR(40),
    local_debug,id,str_length SMALLINT,
    err_msg             VARCHAR(200)


  IF NOT fgl_test("e",file_name) THEN
    LET err_msg = "File Error in export_toolbar_init()\n", get_str_tool(763), " ", file_name, "\n", get_str_tool(49)
    CALL fgl_winmessage(get_str_tool(30) ,err_msg, "error")
    EXIT PROGRAM
  END IF


  LET local_debug = FALSE
  LET id = 1

  CALL fgl_channel_open_file("stream",file_name, "w")
  CALL fgl_channel_set_delimiter("stream","|")

  IF local_debug THEN
    DISPLAY "### start ##### ",file_name, " ########### export_toolbar_init(" , file_name, ")"  
  END IF

  WHILE fgl_channel_write("stream",qxt_toolbar[id].*) 


    #Import Debug Help
    IF local_debug THEN

      DISPLAY "### In Loop ##### ########### process_toolbar_init(" , file_name, ")" 
      DISPLAY "export_toolbar_init[", id, "].m_name:      ", qxt_toolbar[id].m_name
      DISPLAY "export_toolbar_init[", id, "].m_item:      ", qxt_toolbar[id].m_item
      DISPLAY "export_toolbar_init[", id, "].action_time: ", qxt_toolbar[id].action_time
      DISPLAY "export_toolbar_init[", id, "].action:      ", qxt_toolbar[id].action
      DISPLAY "export_toolbar_init[", id, "].dialog:      ", qxt_toolbar[id].dialog
      DISPLAY "export_toolbar_init[", id, "].event:       ", qxt_toolbar[id].event
      DISPLAY "export_toolbar_init[", id, "].txt_en:      ", qxt_toolbar[id].txt_en
      DISPLAY "export_toolbar_init[", id, "].txt_sp:      ", qxt_toolbar[id].txt_sp
      DISPLAY "export_toolbar_init[", id, "].txt_de:      ", qxt_toolbar[id].txt_de
      DISPLAY "export_toolbar_init[", id, "].txt_fr:      ", qxt_toolbar[id].txt_fr
      DISPLAY "export_toolbar_init[", id, "].txt_ar:      ", qxt_toolbar[id].txt_ar
      DISPLAY "export_toolbar_init[", id, "].txt_it:      ", qxt_toolbar[id].txt_it
      DISPLAY "export_toolbar_init[", id, "].txt_ot:      ", qxt_toolbar[id].txt_ot
      DISPLAY "export_toolbar_init[", id, "].image:       ", qxt_toolbar[id].image
      DISPLAY "export_toolbar_init[", id, "].ord:         ", qxt_toolbar[id].ord
      DISPLAY "export_toolbar_init[", id, "].stat:        ", qxt_toolbar[id].stat
    END IF


    IF qxt_toolbar[id].m_name = "EOF" OR qxt_toolbar[id].m_name = "" OR qxt_toolbar[id].m_name IS NULL THEN
      EXIT WHILE
    END IF

    IF id >= qxt_toolbar_array_size THEN
      EXIT WHILE
    END IF 

    LET id = id + 1

  END WHILE


  #LET toolbar_array_size = id -- 1

  IF local_debug THEN
    DISPLAY "### END of file export. used array size = " , id, " qxt_toolbar_array_size= ", qxt_toolbar_array_size, "  ##### ",file_name, " ###########"   
  END IF


  CALL fgl_channel_close("stream")

END FUNCTION



###########################################################
# FUNCTION publish_toolbar(menu_name,action_time)
#
# Publish a group of toolbar menu icons
#
# RETURN NONE
###########################################################
FUNCTION publish_toolbar(menu_name,action_time)
  DEFINE
    menu_name VARCHAR(20),
    action_time, id SMALLINT,
    local_debug SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "Start: publish_toolbar_guidemo_global(action_time)"
  END IF

  FOR id = 1 TO qxt_toolbar_array_size

    IF  qxt_toolbar[id].m_name = menu_name AND qxt_toolbar[id].action_time = action_time THEN
      CALL draw_tb_icon(id)
    END IF
  END FOR

  IF local_debug THEN
    DISPLAY "End: publish_toolbar() array_size = ", id , "id = ", id
  END IF


END FUNCTION



##################################################################################
# FUNCTION draw_tb_icon()
#
# Draw a single menu item
#
# RETURN NONE
##################################################################################
FUNCTION draw_tb_icon(id)
  DEFINE
    lang_string		VARCHAR(100),
    id						SMALLINT,
    local_debug		BOOLEAN,
    err_msg				VARCHAR(200)


  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "draw_tb_icon(id)","id = ", id, "info"
  END IF

  CASE qxt_settings.language
    WHEN 1  --english
      LET lang_string = qxt_toolbar[id].txt_en
    WHEN 2  --spanish
      LET lang_string = qxt_toolbar[id].txt_sp
    WHEN 3  --german
      LET lang_string = qxt_toolbar[id].txt_de
    WHEN 4  --french
      LET lang_string = qxt_toolbar[id].txt_fr
    WHEN 5  --arabic
      LET lang_string = qxt_toolbar[id].txt_ar
    WHEN 6  --italian
      LET lang_string = qxt_toolbar[id].txt_it
    WHEN 7  --other
      LET lang_string = qxt_toolbar[id].txt_ot
    OTHERWISE
      LET err_msg = get_str_tool(32), "draw_tb_icon(id)\n", get_str_tool(804) CLIPPED, " - " , qxt_settings.language
      CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
  END CASE


  CASE qxt_toolbar[id].action

    WHEN 1 --fgl_set_keylabel & fgl_dialog_setkeylabel
      IF qxt_toolbar[id].dialog = 0 THEN
        CALL fgl_setkeylabel(qxt_toolbar[id].event,lang_string,qxt_toolbar[id].image,qxt_toolbar[id].ord,qxt_toolbar[id].stat)
        IF local_debug  THEN
          DISPLAY "fgl_setkeylabel(",qxt_toolbar[id].event, "-",lang_string,qxt_toolbar[id].image,"-",qxt_toolbar[id].ord,"-",qxt_toolbar[id].stat ,")"
        END IF

      ELSE

        CALL fgl_dialog_setkeylabel(qxt_toolbar[id].event,lang_string,qxt_toolbar[id].image,qxt_toolbar[id].ord,qxt_toolbar[id].stat)

        IF local_debug THEN
          DISPLAY "fgl_dialog_setkeylabel(",qxt_toolbar[id].event, "-",lang_string,qxt_toolbar[id].image,"-",qxt_toolbar[id].ord,"-",qxt_toolbar[id].stat ,")"
        END IF

      END IF

    WHEN 2 --REMOVE KEY LABELS USING fgl_set_keylabel & fgl_dialog_setkeylabel
      IF qxt_toolbar[id].dialog = 0 THEN

        CALL fgl_setkeylabel(qxt_toolbar[id].event,"")

        IF local_debug THEN
          DISPLAY "remove global toolbar icon - fgl_setkeylabel(", qxt_toolbar[id].event, ",\"\""
        END IF

      ELSE

        CALL fgl_dialog_setkeylabel(qxt_toolbar[id].event,"")

        IF local_debug THEN
          DISPLAY "remove dialog toolbar icon - fgl_dialog_setkeylabel(", qxt_toolbar[id].event, ",\"\""
        END IF

      END IF

    WHEN 3 -- fgl_keydivider and fgl_dialog_keydivider
      IF qxt_toolbar[id].dialog = 0 THEN
        CALL fgl_keydivider(qxt_toolbar[id].ord)
        IF local_debug THEN
          DISPLAY "fgl_keydivider(",qxt_toolbar[id].ord , ")"
        END IF

      ELSE
        CALL fgl_dialog_keydivider(qxt_toolbar[id].ord)
        IF local_debug THEN
          DISPLAY "fgl_dialog_keydivider(",qxt_toolbar[id].ord , ")"
        END IF

      END IF


    WHEN 4 -- REMOVE fgl_keydivider and fgl_dialog_keydivider
      IF qxt_toolbar[id].dialog = 0 THEN
        CALL fgl_clearkeydivider(qxt_toolbar[id].ord)

        IF local_debug THEN
          DISPLAY "fgl_clearkeydivider(",qxt_toolbar[id].ord , ")"
        END IF

      ELSE
        CALL fgl_dialog_clearkeydivider(qxt_toolbar[id].ord)

        IF local_debug THEN
          DISPLAY "fgl_dialog_clearkeydivider(",qxt_toolbar[id].ord , ")"
        END IF

      END IF


    OTHERWISE
      LET err_msg = get_str_tool(32), " draw_tb_icon()\nqxt_toolbar[" , id , ".action ->" , qxt_toolbar[id].action
      CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
  END CASE
END FUNCTION


##################################################################
# FUNCTION hide_dialog_navigation_toolbar(choice)
#
# Hide all auto-generated navigation buttons
#
# RETURN NONE
##################################################################
FUNCTION hide_dialog_navigation_toolbar(choice)
  DEFINE 
    choice     SMALLINT,  --0=all  1= up/down  2= next/prev page  3= first/last
    err_msg    VARCHAR(200)


  CASE choice
    WHEN 0
      CALL fgl_dialog_setkeylabel("PREVIOUS","")  
      CALL fgl_dialog_setkeylabel("NEXT","") 
      CALL fgl_dialog_setkeylabel("DOWN","") 
      CALL fgl_dialog_setkeylabel("UP","") 
      CALL fgl_dialog_setkeylabel("FIRST","") 
      CALL fgl_dialog_setkeylabel("LAST","") 
    WHEN 1
      CALL fgl_dialog_setkeylabel("PREVIOUS","")  
      CALL fgl_dialog_setkeylabel("NEXT","") 
    WHEN 2
      CALL fgl_dialog_setkeylabel("DOWN","") 
      CALL fgl_dialog_setkeylabel("UP","") 
    WHEN 3
      CALL fgl_dialog_setkeylabel("FIRST","") 
      CALL fgl_dialog_setkeylabel("LAST","") 
    OTHERWISE
      LET err_msg = get_str_tool(802) CLIPPED, " \nhide_dialog_navigation_toolbar(choice)\nchoice=" , choice  
      CALL fgl_winmessage(get_str_tool(30),err_msg,"error")
  END CASE
END FUNCTION


###########################################################################################
# FUNCTION manage_toolbar_list()
#
# Display a list of all toolbar menu items - items can also be edited in detail
#
# RETURN NONE
###########################################################################################
FUNCTION manage_toolbar_list()
  DEFINE 
    tmp_tb_arr DYNAMIC ARRAY OF 
      RECORD
        m_name       VARCHAR(20),
        m_item       VARCHAR(20),
        event        VARCHAR(10),
        stat         SMALLINT,
        action       SMALLINT,
        ord          SMALLINT,
        txt_en       VARCHAR(80),
        dialog       SMALLINT,
        action_time  SMALLINT,
        image        VARCHAR(50)
      END RECORD,
    r                SMALLINT,
    arr_idx          INTEGER,
    scr_idx          INTEGER,
    i                INTEGER,
    local_debug      SMALLINT,
    icon_file        VARCHAR(100),
    previous_help_file_id SMALLINT

  LET local_debug = FALSE

  LET previous_help_file_id = get_current_classic_help()
  # set help file '10' (10 is used for this qxt library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(10)

  CALL fgl_window_open("w_toolbar_list",5,3,get_form_path("f_qxt_toolbar_list_g"),1)


  DISPLAY get_str_tool(850) TO lbTitle
  CALL fgl_settitle(get_str_tool(850))

  CALL set_count(qxt_toolbar_array_size)

  WHILE TRUE  --needs to be in while loop to refresh array list after updates

    #copy a set of the toolbar record for the screen array
    FOR i = 1 TO qxt_toolbar_array_size
      CALL copy_t_toolbar_to_t_toolbar_short(qxt_toolbar[i].*) RETURNING tmp_tb_arr[i].*

      IF local_debug THEN
        DISPLAY "manage_toolbar_list() - tmp_tb_arr[i].image=", tmp_tb_arr[i].image
      END IF
    END FOR

    INPUT ARRAY tmp_tb_arr WITHOUT DEFAULTS FROM tb_list_ar.*  ATTRIBUTE(maxcount=qxt_toolbar_array_size) HELP 500
      BEFORE INPUT
        CALL publish_toolbar("tb_man_arr",0)

      ON KEY(F2)
        CALL export_toolbar_init(get_cfg_path(qxt_settings.toolbar_cfg_file_name))

      ON KEY (F5)
        LET arr_idx = arr_curr()
        LET scr_idx = scr_line()

        #edit current icon line in detailed view form
        CALL icon_detail_edit(arr_idx)

      ON KEY(F9)  
      
        LET arr_idx = arr_curr()
        LET icon_file = icon_browser()
 
        IF icon_file IS NOT NULL THEN
          LET qxt_toolbar[arr_idx].image = icon_file
          EXIT INPUT
        END IF
        #DISPLAY qxt_toolbar[arr_idx].image TO image

      #ON KEY(F12)
      #  EXIT INPUT
    END INPUT

    IF int_flag THEN
      LET int_flag = TRUE
      EXIT WHILE
    ELSE
      EXIT WHILE
    ENd IF

  END WHILE

  CALL fgl_window_close("w_toolbar_list")

  CALL set_classic_help_file(previous_help_file_id)


END FUNCTION


############################################################
#FUNCTION copy_t_toolbar_to_t_toolbar_short(tb_rec)
#
# copy t_toolbar data rec to t_toolbar_short
#
# RETURN qxt_tb_short_rec.* 
############################################################
FUNCTION copy_t_toolbar_to_t_toolbar_short(tb_rec)
  DEFINE
    tb_rec           OF t_qxt_toolbar,
    tb_short_rec     OF t_qxt_toolbar_short,
    local_debug      SMALLINT

    LET local_debug = FALSE

    LET tb_short_rec.m_name = tb_rec.m_name
    LET tb_short_rec.m_item = tb_rec.m_item
    LET tb_short_rec.event = tb_rec.event
    LET tb_short_rec.stat = tb_rec.stat
    LET tb_short_rec.action = tb_rec.action
    LET tb_short_rec.ord = tb_rec.ord
    LET tb_short_rec.txt_en = tb_rec.txt_en  
    LET tb_short_rec.dialog = tb_rec.dialog  
    LET tb_short_rec.action_time = tb_rec.action_time  
    LET tb_short_rec.image = tb_rec.image

  IF local_debug THEN
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.m_name=",      tb_short_rec.m_name
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.m_item=",      tb_short_rec.m_item
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.event=" ,      tb_short_rec.event
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.stat="  ,      tb_short_rec.stat
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.action=",      tb_short_rec.action
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.ord="   ,      tb_short_rec.ord
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.txt_en=",      tb_short_rec.txt_en
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.dialog=",      tb_short_rec.dialog
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.action_time=", tb_short_rec.action_time
    DISPLAY "copy_t_toolbar_to_t_toolbar_short() - qxt_tb_short_rec.image=",       tb_short_rec.image

  END IF

  RETURN tb_short_rec.*

END FUNCTION


############################################################
#FUNCTION copy_t_toolbar_to_t_toolbar_short(tb_rec)
#
# copy t_toolbar_short data rec to t_toolbar
#
# RETURN tb_small_rec.* 
############################################################
FUNCTION copy_t_toolbar_short_to_t_toolbar(tb_short_rec,tb_rec)
  DEFINE
    tb_rec           OF t_qxt_toolbar,
    tb_short_rec     OF t_qxt_toolbar_short

    LET tb_rec.m_name = tb_short_rec.m_name
    LET tb_rec.m_item = tb_short_rec.m_item
    LET tb_rec.event = tb_short_rec.event
    LET tb_rec.stat = tb_short_rec.stat
    LET tb_rec.action = tb_short_rec.action
    LET tb_rec.ord = tb_short_rec.ord
    LET tb_rec.txt_en = tb_short_rec.txt_en  
    LET tb_rec.dialog = tb_short_rec.dialog  
    LET tb_rec.action_time = tb_short_rec.action_time  
    LET tb_rec.image = tb_short_rec.image

  RETURN tb_rec.*

END FUNCTION


###########################################################################################
# FUNCTION icon_detail_edit(id)
#
# Edit icon configuration (for a single menu item)
#
# RETURN NONE
###########################################################################################
FUNCTION icon_detail_edit(id)
  DEFINE
    id SMALLINT

  #OPTIONS
  #  FORM LINE 2

  CALL fgl_window_open("w_icon_details",9,6,get_form_path("f_qxt_toolbar_icon_detail_g"),1)

  DISPLAY get_str_tool(860) TO lbTitle
  CALL fgl_settitle(get_str_tool(860))
  DISPLAY get_str_tool(852) to dl_grp_nam
  DISPLAY get_str_tool(853) to dl_item

  INPUT BY NAME
    qxt_toolbar[id].* WITHOUT DEFAULTS HELP 1 

    BEFORE INPUT
      CALL publish_toolbar("tb_man_det",0)

    #ON KEY(F12)
    #  EXIT INPUT

    #ON KEY(F2)
    #  CALL fgl_dialog_update_data()
    #  EXIT INPUT

    ON KEY(F9)
      LET qxt_toolbar[id].image = icon_browser()
      DISPLAY qxt_toolbar[id].image TO image

  END INPUT

  --sleep 5
  CALL fgl_window_close("w_icon_details")

  #OPTIONS
  #  FORM LINE 3

END FUNCTION


##################################################################
# FUNCTION icon_browser()
#
# Displays the list of all icons (using the lazy way...)
#
# RETURN icon_list[i]
##################################################################
FUNCTION icon_browser()
  DEFINE 
    icon_name         varchar(50),
    i,id,change_image SMALLINT,
    local_debug       SMALLINT

  # DEFINE icon_list ARRAY[toolbar_array_size] OF VARCHAR(50)  
  DEFINE icon_list DYNAMIC ARRAY OF VARCHAR(50) 

  LET local_debug = FALSE

  # set help file '10' (10 is used for this qxt library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(10)

  FOR i = 1 TO qxt_toolbar_array_size
    LET icon_list[i] = qxt_toolbar[i].image
    IF local_debug THEN
      DISPLAY "icon_browser() - i=",i, "  image=", qxt_toolbar[i].image
    END IF
  END FOR

  CALL fgl_window_open("w_ic_browser",3,60,get_form_path("f_qxt_toolbar_icon_browser_g"),1)
  CALL fgl_settitle("Icon")

  #CALL set_count(i)


  DISPLAY ARRAY icon_list TO ic_array.* HELP  500 
    BEFORE DISPLAY
      CALL publish_toolbar("img_brows",0)

    #ON KEY(F2)
    #  LET change_image = 1
    #  EXIT DISPLAY

    #ON KEY(F12)   -- cancel
    #  LET change_image = 0
    #  EXIT DISPLAY

  END DISPLAY

  CALL fgl_window_close("w_ic_browser")

  IF NOT int_flag THEN  --change_image = 1 AND int_flag IS NOT NULL THEN
    LET i = arr_curr()
    RETURN icon_list[i]
  ELSE
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION








