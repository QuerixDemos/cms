##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

###################################################################
#
#
#
#
###################################################################
FUNCTION prepareModuleStart()
	#DEFINE argOperatorIdState BOOLEAN
	DEFINE lConfigFilename STRING
	DEFINE argOperatorId LIKE operator.operator_id
 	DEFINE lSessionId INTEGER

	CALL setEnvAndOptions(1)
	CALL processArguments()  -- read & process the url arguments for i.e. style id, menu id

 

####  huho - had to put it here.. no client interaction is allowed BEFORE the css/js are imported
  #############################
  # GUI Path
  #############################
  #IF local_debug THEN
  #  DISPLAY "init_tools_data() - CALL init_gui_path()"
  #END IF

#  IF fgl_fglgui() THEN
#    CALL init_gui_path()                                                             --initialise common local & server paths
#  END IF
####  huho - had to put it here.. no client interaction is allowed BEFORE the css/js are imported  
  

	#process main configuration file (usually cfg/cms.cfg
	LET lConfigFilename = getConfigFilename()  --get config file name from the url argument line

  IF lConfigFilename IS NULL THEN
     CALL set_main_cfg_filename("cfg/cms.cfg")  --if argument was not specified, use DEFAULT path/file
  ELSE
     CALL set_main_cfg_filename(lConfigFilename)
  END IF

  CALL init_base_module_data(lConfigFilename)  --initialise some variables and the QXT libraries

	IF get_cssState() = TRUE THEN
		CALL importHtml5Resources(get_cssId())  --load css and js stuff depending on the argument cssId
	END IF

	IF get_themeState() = TRUE THEN
		CALL loadThemeResources(get_themeId())  --load specified theme file
	END IF
	
	CALL loadMenuAndBaseUiStyles()  
	
  CALL check_cms_db_version()  --check if it exists and if it is the correct version
  CALL add_report_types()    -- add three types of reports (screen, print text, html)

 	IF NOT login() THEN
		EXIT PROGRAM
 	END IF 
	#display "fgl_getenv(\"qx_child\")=",fgl_getenv("qx_child")
	#IF fgl_getenv("qx_child") IS NULL THEN -- only load stuff if it is not a child process	
		CALL update_mdi_statusbar()
	#END IF
END FUNCTION


FUNCTION prepareModuleStartExtended()
	DEFINE local_debug BOOLEAN
  DEFINE lib_type    CHAR(2)
  
  ######################
  # Initialise tools library
  ###################### 
 # CALL init_tools_data()

  ############################
  #Connect to the database
  ############################
  IF get_db_name_tool() IS NOT NULL THEN
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL qxt_connect_db_tool()"
    END IF
    CALL qxt_connect_db_tool()
  END IF

  ############################
  # Application String
  ############################
  #File string library needs to import the string file
  IF get_string_app_lib_info() <> "DB" THEN
    CALL qxt_string_app_init(NULL)
  END IF

  ######################
  # Classic 4GL Help - Multilingual
  ###################### 
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_help_classic_cfg_import(NULL)"
  END IF

  CALL process_help_classic_cfg_import(NULL)   #cfg file import

  ######################
  # INIT Classic 4GL Help - Multilingual
  ###################### 
  LET lib_type = get_help_classic_lib_info()   #file based (non-db) library needs to import the unl file
  IF lib_type <> "DB" THEN --File type library
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_classic_init(NULL)"
    END IF
    CALL process_help_classic_init(NULL)
  END IF

  ######################
  # Web Service
  ###################### 
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_webservice_cfg_import(NULL)"
  END IF
  CALL process_webservice_cfg_import(NULL)

  #########GUI client only section ##########################
  IF fgl_fglgui() THEN

    ######################
    # Help Html 
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_html_cfg_import(NULL)"
    END IF

    CALL process_help_html_cfg_import(NULL)

    #file based (non-db) library needs to import the unl file
    LET lib_type = get_help_html_url_map_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL init_help_html_url_map(NULL)"
      END IF
      CALL init_help_html_url_map(NULL)
    END IF



    ######################
    # DDE
    ######################  
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_dde_cfg_import(NULL)"
    END IF

    CALL process_dde_cfg_import(NULL)

    #file based (non-db) library needs to import the unl file
    LET lib_type = get_dde_font_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_dde_font_init(NULL)"
      END IF
      CALL process_dde_font_init(NULL)
    END IF



    ######################
    # Print Html
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_print_html_cfg_import(NULL)"
    END IF
    CALL process_print_html_cfg_import(NULL)

    #html print template list
    LET lib_type = get_print_html_template_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_print_html_template_init(NULL)"
      END IF
      CALL process_print_html_template_init(NULL)
    END IF

    #html print image list
    LET lib_type = get_print_html_image_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_print_html_image_init(NULL)"
      END IF

      CALL process_print_html_image_init(NULL)
    END IF



    ######################
    # Online Resource (i.e. self running demo from webserver
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_online_resource_cfg_import(NULL)"
    END IF
    CALL process_online_resource_cfg_import(NULL)


  END IF


END FUNCTION


###########################################################
# FUNCTION setAppTitleBarCaption(argCustLabel)
#
# RETURN NONE
###########################################################
FUNCTION setAppTitleBarCaption(argCustLabel)
	DEFINE argCustLabel STRING
	
	LET argCustLabel = trim (argCustLabel), " - CMS-Demo Lycia 3 Demo App" 
	CALL fgl_settitle(argCustLabel)

END FUNCTION	

###########################################################
# FUNCTION add_report_types()
#
# RETURN NONE
###########################################################
FUNCTION add_report_types()
  CALL q4gl_add_user_report_type("printer","print")
  CALL q4gl_add_user_report_type("screen","text")
  CALL q4gl_add_user_report_type("browser","html")
END FUNCTION



###########################################################
# FUNCTION check_cms_db_version()
#
# RETURN NONE
###########################################################
FUNCTION check_cms_db_version()
  DEFINE 
    db_name VARCHAR(40),
    retval SMALLINT,
    db_state_str,err_msg VARCHAR(100),
    cms_db_state SMALLINT,
    l_cms_info OF t_cms_info 
  LET db_name = "cms"

  #Try to connect to database
  WHENEVER ERROR CONTINUE
    CALL open_db(db_name) RETURNING retval, db_state_str
  WHENEVER ERROR STOP

  IF retval < 0 THEN

    DISPLAY db_state_str TO dl_db_state ATTRIBUTE(RED)
    CALL fgl_winmessage("Database Error","Cannot connect to database CMS\nYou need to create the database named 'cms' before you can continue\nThe application will now close","error")
    #EXIT PROGRAM
  ELSE

    #if connection successful, try to read the cms database version & build information
    CALL db_info("cms") RETURNING cms_db_state, l_cms_info.*

    CASE cms_db_state
      WHEN -1
        LET err_msg = "Your CMS database seems to be incomplete or outdated - please reinstall the db"
        CALL db_error_information(err_msg, l_cms_info.*)
        EXIT PROGRAM
      WHEN -2
        LET err_msg = "Your CMS database seems to be empty (no tables or too old) - please reinstall the database"
        CALL db_error_information(err_msg, l_cms_info.*)
        EXIT PROGRAM
      WHEN 0
        #LET err_msg = "Your CMS database seems to be OK"
        #CALL db_error_information(err_msg, l_cms_info.*)
    END CASE
  END IF

END FUNCTION



###########################################################
# FUNCTION db_error_information(err_msg, l_cms_info)
#
# RETURN NONE
###########################################################
FUNCTION db_error_information(err_msg, l_cms_info)
  DEFINE 
    err_msg VARCHAR(300),
    l_cms_info OF t_cms_info 


  LET err_msg = trim(err_msg) , "\n", "Current CMS DB Version:", l_cms_info.db_version, " Build:", l_cms_info.db_build, "\n", "Required CMS DB Version:",  get_required_cms_version()
  CALL fgl_winmessage("Error in DMS Database",err_msg,"error")

END FUNCTION




  
#####################################################################
# FUNCTION setEnvAndOptions(p_choice)
#
# Load/Apply common options, environments and compiler directives
#####################################################################
FUNCTION setEnvAndOptions(p_choice)
	DEFINE p_choice SMALLINT
	
	CASE p_choice
		WHEN 0
			#don't load anything
		WHEN 1
			DEFER INTERRUPT  
			# Escape for Accept is a legacy sin...
			OPTIONS ACCEPT KEY RETURN  
			OPTIONS HELP KEY F1 
			OPTIONS FIELD ORDER UNCONSTRAINED
			OPTIONS INPUT WRAP
			OPTIONS NEXT KEY F41
			OPTIONS PREVIOUS KEY F42
			
			OPTIONS ON CLOSE APPLICATION KEY F12
#	would be nice to have this option too			OPTIONS ON CLOSE APPLICATION ACTION actExit
			OPTIONS ON CLOSE APPLICATION CALL cleanup
    
			CALL fgl_putenv("DBDATE=dmy4/")  --Set date in the environment
		
	END CASE
	
END FUNCTION
