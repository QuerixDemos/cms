##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Webservice Functions to access path / file access / file location
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION  get_webservice_cfg_filename()
#
# Import/Process Webservice cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.webservice_cfg_filename
###########################################################
FUNCTION get_webservice_cfg_filename()
  RETURN qxt_settings.webservice_cfg_filename
END FUNCTION


###########################################################
# FUNCTION set_webservice_cfg_filename(p_filename)
#
# Export/Write Webservice cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.webservice_cfg_filename
###########################################################
FUNCTION set_webservice_cfg_filename(p_filename)
  dEFINE
    p_filename   VARCHAR(150)

  LET qxt_settings.webservice_cfg_filename = p_filename
END FUNCTION


###########################################################
# FUNCTION process_webservice_cfg_import(p_webservice_cfg_filename)
#
# Import/Process Webservice cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_webservice_cfg_import(p_webservice_cfg_filename)
DEFINE 
  p_webservice_cfg_filename  VARCHAR(200),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_import() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF

  IF p_webservice_cfg_filename IS NULL THEN
    LET p_webservice_cfg_filename = get_webservice_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_import() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF

  #Add path
  LET p_webservice_cfg_filename = get_cfg_path(p_webservice_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_import() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF


  IF p_webservice_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_webservice_cfg_import()","Error in process_webservice_cfg_import()\nNo import webservice_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    EXIT PROGRAM
  END IF



############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_webservice_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[WebService]
  #read from WebService section
  LET qxt_settings.ws_name    = configGet(ret, "[WebService]", "ws_name")
  LET qxt_settings.ws_port    = configGet(ret, "[WebService]", "ws_port")

  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_webservice_cfg_filename = ", p_webservice_cfg_filename CLIPPED, "###############################"


    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [WebService] Section               x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    
    DISPLAY "configInit() - qxt_settings.ws_name=",         qxt_settings.ws_name
    DISPLAY "configInit() - qxt_settings.ws_port=",         qxt_settings.ws_port

   
  END IF

  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_webservice_cfg_export(p_webservice_cfg_filename)
#
# Export Webservice cfg data
#
# RETURN ret
###########################################################
FUNCTION process_webservice_cfg_export(p_webservice_cfg_filename)
  DEFINE 
    p_webservice_cfg_filename     VARCHAR(100),
    ret                     SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_export() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF

  IF p_webservice_cfg_filename IS NULL THEN
    LET p_webservice_cfg_filename = get_webservice_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_export() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF

  #Add path
  LET p_webservice_cfg_filename = get_cfg_path(p_webservice_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_webservice_cfg_export() - p_webservice_cfg_filename=", p_webservice_cfg_filename
  END IF


  IF p_webservice_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_webservice_cfg_export()","Error in process_webservice_cfg_export()\nNo import webservice_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_webservice_cfg_filename)  --configInit(filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[WebService]
  #Write to WebService section
  CALL configWrite(ret, "[WebService]", "ws_name",                qxt_settings.ws_name )
  CALL configWrite(ret, "[WebService]", "ws_port",                qxt_settings.ws_port )


END FUNCTION



###Web service test
{
########################################################################
# FUNCTION request_web_service_data()
########################################################################
FUNCTION request_web_service_data()
  DEFINE cms_cont_rec OF t_contact_ws_rec
  DEFINE ws_service VARCHAR(255)
  DEFINE ws_port VARCHAR(255)
  DEFINE ws_operation VARCHAR(255)
  DEFINE err_string CHAR(1024)
  DEFINE local_debug SMALLINT

  LET local_debug = FALSE

  CALL get_contact_ws_rec(1)
      RETURNING cms_cont_rec.*


  IF local_debug THEN
    DISPLAY "cms_cont_rec.cont_name = " ,  cms_cont_rec.cont_name         -- CHAR(20) NOT NULL,
    DISPLAY "cms_cont_rec.cont_title = ",  cms_cont_rec.cont_title       -- CHAR(5),
    DISPLAY "cms_cont_rec.cont_fname = ",  cms_cont_rec.cont_fname       -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_lname = ",  cms_cont_rec.cont_lname       -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_addr1 = ",  cms_cont_rec.cont_addr1       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_addr2 = ",  cms_cont_rec.cont_addr2       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_addr3 = ",  cms_cont_rec.cont_addr3       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_city = ",   cms_cont_rec.cont_city        -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_zone = ",   cms_cont_rec.cont_zone        -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_zip = ",    cms_cont_rec.cont_zip         -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_country = ",cms_cont_rec.cont_country     -- CHAR(30),
    DISPLAY "cms_cont_rec.cont_phone = ",  cms_cont_rec.cont_phone       -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_mobile = ", cms_cont_rec.cont_mobile      -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_fax = ",    cms_cont_rec.cont_fax         -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_email = ",  cms_cont_rec.cont_email       -- CHAR(50),
    DISPLAY "cms_cont_rec.comp_name = ",   cms_cont_rec.comp_name        -- CHAR(50),
    DISPLAY "cms_cont_rec.cont_notes = ",  cms_cont_rec.cont_notes       -- CHAR(50),
  END IF

END FUNCTION

}

