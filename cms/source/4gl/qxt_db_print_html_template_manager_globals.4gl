##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the DB print html template library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

GLOBALS

  DEFINE qxt_glob_int_switch SMALLINT

  DEFINE t_qxt_print_template_rec TYPE AS
    RECORD
     template_id          LIKE qxt_print_template.template_id,
     language_id          LIKE qxt_print_template.language_id,
     filename             LIKE qxt_print_template.filename,
     mod_date             LIKE qxt_print_template.mod_date,
     template_data        LIKE qxt_print_template.template_data
 
    END RECORD

  DEFINE t_qxt_print_template_no_blob_rec TYPE AS
    RECORD
     template_id          LIKE qxt_print_template.template_id,
     language_id          LIKE qxt_print_template.language_id,
     filename             LIKE qxt_print_template.filename,
     mod_date             LIKE qxt_print_template.mod_date
   END RECORD

  DEFINE t_qxt_print_template_no_blob_form_rec TYPE AS
    RECORD
     template_id          LIKE qxt_print_template.template_id,
     language_name        LIKE qxt_language.language_name,
     filename             LIKE qxt_print_template.filename,
     mod_date             LIKE qxt_print_template.mod_date
   END RECORD


  DEFINE t_qxt_print_template_form_rec TYPE AS
    RECORD
     template_id          LIKE qxt_print_template.template_id,
     language_name        LIKE qxt_language.language_name,
     filename             LIKE qxt_print_template.filename,
     mod_date             LIKE qxt_print_template.mod_date,
     template_data        LIKE qxt_print_template.template_data
 
    END RECORD

  DEFINE t_qxt_print_template_file TYPE AS
    RECORD
     filename             LIKE qxt_print_template.filename,
     template_data        LIKE qxt_print_template.template_data
    END RECORD


END GLOBALS
