##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# String intialisation and handling for international support
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_string_tool_in_mem_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_string_tool_lib_info()
#
# Simply returns libray name
#
# RETURN NONE
###########################################################
FUNCTION get_string_tool_lib_info()
  RETURN "DB - qxt_db_string_tool_in_mem"
END FUNCTION 



######################################################################################################################
# Init functions
######################################################################################################################

###########################################################
# FUNCTION qxt_string_tool_init()
#
# Initialise too string array with blanks ""
#
# RETURN NONE
###########################################################
FUNCTION qxt_string_tool_init(p_string_tool_table_name)
  DEFINE
    p_string_tool_table_name  VARCHAR(200),
    l_language                SMALLINT,
    l_default_language        SMALLINT
 
  IF p_string_tool_table_name IS NULL THEN
    LET p_string_tool_table_name = get_string_tool_table_name()
  END IF

  IF p_string_tool_table_name IS NULL THEN
    CALL fgl_winmessage("Error in qxt_string_tool_init()","Error in qxt_string_tool_init()\nNo import string_tool_table_name was specified in the function call\nand also not found in the string.cfg file","error")
    EXIT PROGRAM
  ELSE 
    CALL set_string_tool_table_name(p_string_tool_table_name)
  END IF


  # Note: To ensure, that all strings exist for any language, we use the default language - usually english=1
  # The default language is specified in the config file
  LET l_language = get_language()
  LET l_default_language = get_language_default()



  IF l_language = l_default_language THEN  
    CALL process_string_tool_import_to_memory(p_string_tool_table_name,l_language)
  ELSE
    CALL process_string_tool_import_to_memory(p_string_tool_table_name,l_default_language)
    CALL process_string_tool_import_to_memory(p_string_tool_table_name,l_language)
  END IF

END FUNCTION


###################################################################################
# FUNCTION string_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for the string table
#
# RETURN NONE
###################################################################################
FUNCTION string_tool_data_source(p_table_name,p_language,p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    p_table_name        VARCHAR(18),
    p_language          LIKE qxt_language.language_id


  IF p_table_name IS NULL THEN
    LET p_table_name = "qxt_string_tool"
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "string_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * ",
                 "FROM ", trim(p_table_name), " ",
                 "WHERE language_id = ", trim(p_language), " "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED

  END IF

display sql_stmt
  PREPARE p_string FROM sql_stmt
  DECLARE c_string CURSOR FOR p_string


END FUNCTION



###################################################################################
# FUNCTION process_string_import_to_memory(p_category)
#
# copy string data to memory (for faster access)
#
# RETURN NONE
###################################################################################
FUNCTION process_string_tool_import_to_memory(p_table_name,p_language)
  DEFINE 
    p_category        SMALLINT,
    tmp_string        RECORD LIKE qxt_string_tool.*,
    table_category    VARCHAR(30),
    id                SMALLINT,
    p_table_name      VARCHAR(18),
    p_language        SMALLINT,
    local_debug       SMALLINT
 

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_string_tool_import_to_memory() - p_table_name=", p_table_name
    DISPLAY "process_string_tool_import_to_memory() - p_language=", p_language

  END IF



  # Move any strings from the current language set to memory
  #table_cateogry allows different string arrays
  #Language_id 0 stands for mono/single language strings

  LET table_category = get_string_tool_table_name()
  CALL string_tool_data_source(p_table_name,p_language,"string_id",get_toggle_switch())


  FOREACH c_string INTO tmp_string
    LET qxt_string_tool[tmp_string.string_id] = tmp_string.string_data
    IF local_debug THEN
      DISPLAY "process_string_tool_import_to_memory() - tmp_string.string_id=", tmp_string.string_id
      DISPLAY "process_string_tool_import_to_memory() - tmp_string.string_data=", tmp_string.string_data
      DISPLAY "process_string_tool_import_to_memory() - qxt_string_tool[tmp_string.string_id]=", qxt_string_tool[tmp_string.string_id]
    END IF

  END FOREACH


END FUNCTION






