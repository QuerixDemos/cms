##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the tax_rates table
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                  RETURN
# get_tax_rate_rec(p_rate_id)                      get tax rate record from tax_id               l_tax_rate.*
# get_tax_rate(p_rate_id)                          get tax from rate_id                          l_tax_rate.tax_rate
# tax_rate_create()                                create tax rate record                        l_tax_rate.rate_id
# tax_rate_edit(p_rate_id)                         edit tax rate record                          NONE
# tax_rate_input(p_tax_rate)                       input tax rate record data                    p_tax_rate.*
# tax_rate_delete(p_rate_id)                       delete tax rate record                        NONE
# tax_rate_view_by_rec(p_tax_rate_rec)             display tax rate details on form              NONE
# tax_rate_view(p_tax_rate_id)                     display tax rate details in window-form by ID NONE
# tax_rate_popup(p_rate_id)                        display tax rate selection window             l_tax_arr[i].rate_id or p_rate_id
# grid_header_tax_scroll                           populate grid array with labels               NONE
#
############################################################################################################
####################################################
#
####################################################
GLOBALS "globals.4gl"



####################################################
# FUNCTION get_tax_rate_rec(p_rate_id)
#
# get tax rate record from id
#
# RETURN  l_tax_rate.*
####################################################
FUNCTION get_tax_rate_rec(p_rate_id)
  DEFINE 
    p_rate_id  LIKE tax_rates.rate_id,
    l_tax_rate RECORD LIKE tax_rates.*

  SELECT tax_rates.*
    INTO l_tax_rate.*
    FROM tax_rates
    WHERE tax_rates.rate_id = p_rate_id

  RETURN l_tax_rate.*
END FUNCTION


####################################################
# FUNCTION get_tax_rate(p_rate_id)
#
# get tax rate from id
#
# RTURN  l_tax_rate.tax_rate
####################################################
FUNCTION get_tax_rate(p_rate_id)
  DEFINE p_rate_id LIKE tax_rates.rate_id
  DEFINE l_tax_rate RECORD LIKE tax_rates.*
  
  CALL get_tax_rate_rec(p_rate_id)
    RETURNING l_tax_rate.*

  RETURN l_tax_rate.tax_rate
END FUNCTION

