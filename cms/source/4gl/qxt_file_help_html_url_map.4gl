##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################




############################################################
# Globals
############################################################
GLOBALS "qxt_file_help_html_url_map_globals.4gl"

######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_help_url_map_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_html_url_map_lib_info()
  RETURN "FILE - qxt_file_help_html_url_map"
END FUNCTION 

###########################################################
# FUNCTION init_help_html_url_map()
#
# Initialises the html_help libraries / must be called before anything else is called
#
# RETURN p_client_file_path
###########################################################
FUNCTION init_help_html_url_map(p_filename)
  DEFINE 
    local_debug SMALLINT,
    p_filename VARCHAR(100)

  LET local_debug = FALSE

  IF p_filename IS NULL THEN
    LET p_filename = get_help_html_url_map_config_fname()
  END IF


  IF local_debug THEN
    DISPLAY "init_help_html_url_map() - p_filename =", p_filename
    DISPLAY "init_help_html_url_map() - get_help_html_url_map_config_fname() =", get_help_html_url_map_config_fname()
    DISPLAY "init_help_html_url_map() - get_unl_path(p_filename) =", get_unl_path(p_filename)
  END IF

  IF fgl_fglgui() THEN
    IF validate_file_server_side_exists(get_unl_path(p_filename), NULL,1) THEN
      CALL process_help_html_url_map_import(get_unl_path(p_filename))                --import html help urls
    END IF
  END IF

END FUNCTION


###########################################################
# FUNCTION process_help_html_url_map_import(p_file_name)
#
# Import/Process the html help url mapping file
#
# RETURN NONE
###########################################################
FUNCTION process_help_html_url_map_import(p_file_name)

  #International strings
  DEFINE
    p_file_name, tempstr            VARCHAR(200),
    local_debug,id,id2,str_length SMALLINT,
    err_msg                       VARCHAR(200),
    tmp_rec                       OF t_qxt_help_url_map_rec,
    l_language_id                 SMALLINT,
    while_count                   SMALLINT

  LET local_debug = FALSE
  LET l_language_id = get_language()
  LET while_count = 0

  IF NOT fgl_test("e",p_file_name) THEN
    LET err_msg = "process_html_help_url_map_import(file_name)\n", get_str_tool(727), " ",  p_file_name
    CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
  END IF
 
  #Open File
  IF NOT fgl_channel_open_file("stream",p_file_name, "r") THEN
    LET err_msg = "Error in process_html_help_url_map_import()\nCannot open file ", trim(p_file_name)
    CALL fgl_winmessage("Error in process_html_help_url_map_import()", err_msg, "error")
    RETURN
  END IF

  #Set dellimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in process_html_help_url_map_import()\nCannot set delimiter for file ", trim(p_file_name)
    CALL fgl_winmessage("Error in process_html_help_url_map_import()", err_msg, "error")
    RETURN
  END IF


  IF local_debug THEN
    DISPLAY "process_html_help_url_map_import() - ### start ##### ",p_file_name, " ########### process_html_help_url_map_import(" , p_file_name, ")"  
  END IF


  #LET xxx = 1
  WHILE fgl_channel_read("stream",tmp_rec.*)
    #Language filter
    IF tmp_rec.language_id = l_language_id THEN  --only import entries corresponding to current language

      IF  tmp_rec.map_id IS NOT NULL THEN
        LET while_count = while_count + 1
        LET qxt_help_html_url_map_array[tmp_rec.map_id].map_fname = tmp_rec.map_fname
        LET qxt_help_html_url_map_array[tmp_rec.map_id].map_ext   = tmp_rec.map_ext

        IF local_debug THEN

          DISPLAY "process_html_help_url_map_import() - tmp_rec.map_id= ", tmp_rec.map_id
          DISPLAY "process_html_help_url_map_import() - qxt_help_html_url_map_array[tmp_rec.map_id].help_url_map_fname= ", qxt_help_html_url_map_array[tmp_rec.map_id].map_fname
          DISPLAY "process_html_help_url_map_import() - qxt_help_html_url_map_array[tmp_rec.map_id].help_url_map_ext= ", qxt_help_html_url_map_array[tmp_rec.map_id].map_ext
        END IF
      ELSE
        EXIT WHILE  -- NULL terminates input
      END IF

    END IF

  END WHILE

  #Close file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in process_html_help_url_map_import()\nCannot close file ", trim(p_file_name)
    CALL fgl_winmessage("Error in process_html_help_url_map_import()", err_msg, "error")
    RETURN
  END IF


  IF while_count <= 0 THEN
    LET err_msg = "The html help configuration file >",p_file_name ,"<\nis empty or does not exist.\nIt is also possible that there are no entries for your current language ", trim(l_language_id) 
    CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
  END IF

  IF local_debug THEN
    DISPLAY "process_html_help_url_map_import() - ### END of help_html file import. found entries = ", trim(while_count), "file=", trim(p_file_name), "language=", trim(l_language_id), " ###########"   
    DISPLAY "process_html_help_url_map_import() - ### END of help_html file import"   
  END IF


END FUNCTION


###############################################################################
# Data Access functions
###############################################################################

###########################################################
# FUNCTION get_help_html_url_map_fname(p_help_url_map_id,p_language_id)
#
# get the main url
#
# RETURN l_help_url_map_fname
###########################################################
FUNCTION get_help_html_url_map_fname(p_help_url_map_id,p_language_id)
  DEFINE 
    p_help_url_map_id   SMALLINT,
    p_language_id       SMALLINT,
    c                   SMALLINT,
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_html_url_map_fname() - p_help_url_map_id=", p_help_url_map_id
    DISPLAY "get_help_html_url_map_fname() - p_language_id=",     p_language_id
    DISPLAY "get_help_html_url_map_fname() - qxt_help_html_url_map_array[p_help_url_map_id].map_fname=",     qxt_help_html_url_map_array[p_help_url_map_id].map_fname
  END IF


  RETURN qxt_help_html_url_map_array[p_help_url_map_id].map_fname
{
  FOR c = 1 TO size_of(qxt_help_html_url_map_array)
    IF qxt_help_html_url_map_array[c] = p_help_url_map_id THEN
      RETURN qxt_help_html_url_map_array[c].help_url_map_fname
  END FOR
}

END FUNCTION


###########################################################
# FUNCTION get_help_html_url_map_ext(p_help_url_map_id,p_language_id)
#
# get the url extension
#
# RETURN l_help_url_map_fname
###########################################################
FUNCTION get_help_html_url_map_ext(p_help_url_map_id,p_language_id)
  DEFINE 
    p_help_url_map_id   SMALLINT,
    p_language_id       SMALLINT,
    c                   SMALLINT


  RETURN qxt_help_html_url_map_array[p_help_url_map_id].map_ext
{
  FOR c = 1 TO size_of(qxt_help_html_url_map_array)
    IF qxt_help_html_url_map_array[c] = p_help_url_map_id THEN
      RETURN qxt_help_html_url_map_array[c].help_url_map_fname
  END FOR
}

END FUNCTION


###########################################################
# FUNCTION get_help_html_url_map_joint_url(p_language_id,p_help_url_map_id)
#
# get the main url with extension
#
# RETURN url
###########################################################
FUNCTION get_help_html_url_map_joint_url(p_language_id,p_help_url_map_id)
  DEFINE
    p_help_url_map_id   SMALLINT,
    p_language_id       SMALLINT,
    url                 VARCHAR(200)

  LET url =  qxt_help_html_url_map_array[p_help_url_map_id].map_fname, qxt_help_html_url_map_array[p_help_url_map_id].map_ext
  RETURN url

END FUNCTION

{

FUNCTION get_help_url_map_rec(p_help_url_map_id,p_language_id)
  DEFINE 
    p_help_url_map_id  SMALLINT,
    p_language_id      SMALLINT,
    c                  SMALLINT


  #Search through array to find the corresponding help entry (help_id)
  FOR c = 1 TO qxt_help_html_url_map_array_size
    IF local_debug THEN
      DISPLAY "2-get_help_url(", id, ")  - B-help_target_current=", qxt_help_target_current
      DISPLAY "2-get_help_url() - for loop - invalid_id =", invalid_id
      DISPLAY "2-get_help_url() - for loop - c =", c
      DISPLAY "2-get_help_url() - for loop - qxt_helphtml[[c].help_id =", qxt_helphtml[c].help_id
    END IF


    #When found, read values ,set invalid_id = 0  (success) and exit for loop
    IF qxt_helphtml[c].help_id = id THEN

      IF local_debug THEN
        DISPLAY "get_help_url() - id = ", id
        DISPLAY "get_help_url() - qxt_helphtml[c].help_html_file = ", qxt_helphtml[c].help_html_file
      END IF
       
      #Depending on the settings application server help or online help we get the fully qualified URL (and may download a file)
      CASE get_help_html_system_type()
        WHEN 0 --application server help file resource
          RETURN get_help_url_file(id,qxt_helphtml[c].help_html_file)
        WHEN 1 --online
          RETURN get_help_html_base_url_path(get_help_html_file(c))
        OTHERWISE
          LET tmp_str = "Invalid help_system_type value in get_help_url()\nget_help_html_system_type() =", get_help_html_system_type()
          CALL fgl_winmessage("Error in help_system_type - get_help_url()",tmp_str,"error")
      END CASE

    END IF

  END FOR

}


















