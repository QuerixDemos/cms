##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

###################################################
# FUNCTION populate_supplies_form_g()
#
# 
#
# RETURN NONE
###################################################
FUNCTION populate_supplies_form_g()
  CALL fgl_settitle("Supplies List")
  CALL fgl_grid_header("sc_suppl_scroll" , "suppl_id"   , get_str(2537), "Left", "F81")
  CALL fgl_grid_header("sc_suppl_scroll" , "name"       , get_str(2538), "Left", "F82")
  CALL fgl_grid_header("sc_suppl_scroll" , "suppl_date" , get_str(2539), "Left", "F83")
  CALL fgl_grid_header("sc_suppl_scroll" , "exp_date"   , get_str(2540), "Left", "F84")
  CALL fgl_grid_header("sc_suppl_scroll" , "status_name", get_str(2541), "Left", "F85")
  CALL fgl_grid_header("sc_suppl_scroll" , "comp_name"  , get_str(2542), "Left", "F86")
  CALL UIRadButListItemText("rb_state_filter",1,get_str(2477))
  CALL UIRadButListItemText("rb_state_filter",2,get_str(2566))
  CALL UIRadButListItemText("rb_state_filter",3,get_str(2481))
  CALL UIRadButListItemText("rb_state_filter",4,get_str(2465))
  #DISPLAY get_str(2477) TO rb_st1
  #DISPLAY get_str(2566) TO rb_st2
  #DISPLAY get_str(2481) TO rb_st3
  #DISPLAY get_str(2465) TO rb_st4

  DISPLAY get_str(2544) TO lbTitle
  DISPLAY get_str(818) TO bt_edit
  #DISPLAY "!" TO  bt_edit
  DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO  bt_cancel
  DISPLAY get_str(817) TO bt_delete
  #DISPLAY "!" TO  bt_delete
  DISPLAY get_str(10) TO bt_print
  #DISPLAY "!" TO  bt_print
  DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO  bt_ok
  DISPLAY get_str(819) TO bt_new
  #DISPLAY "!" TO  bt_new
  #DISPLAY "!" TO  rb_state_filter
  DISPLAY "F121" TO rb_state_filter
END FUNCTION



###################################################
# FUNCTION populate_supplies_form_t()
#
# 
#
# RETURN NONE
###################################################
FUNCTION populate_supplies_form_t()

  DISPLAY get_str(2543) TO dl_f1
  DISPLAY get_str(2538) TO dl_f3
  DISPLAY get_str(2539) TO dl_f4
  DISPLAY get_str(2540) TO dl_exp
  DISPLAY get_str(2541) TO dl_f7
  DISPLAY get_str(2542) TO dl_f2
  DISPLAY get_str(2544) TO lbTitle
  DISPLAY get_str(2545) TO lbInfo1

END FUNCTION

###################################################
# FUNCTION show_supplies_header(p_supplies)
#
# 
#
# RETURN NONE
###################################################
FUNCTION show_supplies_header(p_supplies)
  DEFINE 
    p_supplies   RECORD LIKE supplies.*,
    l_account    RECORD LIKE account.*,
    l_company    RECORD LIKE company.*,
    l_contact    RECORD LIKE contact.*,
    l_operator_name LIKE operator.name

  CALL get_account_rec(p_supplies.account_id)
    RETURNING l_account.*

  CALL get_company_rec(l_account.comp_id)
    RETURNING l_company.*

  CALL web_get_contact_rec(l_company.comp_main_cont)
    RETURNING l_contact.*

  SELECT operator.name
    INTO l_operator_name
    FROM operator  
    WHERE operator.operator_id = p_supplies.operator_id

  DISPLAY BY NAME l_company.comp_id
  DISPLAY BY NAME l_company.comp_name
  DISPLAY BY NAME l_contact.cont_name
  DISPLAY BY NAME l_contact.cont_phone

  DISPLAY l_operator_name TO operator.name
  DISPLAY l_operator_name TO operator.name
  DISPLAY p_supplies.suppl_date TO supplies.suppl_date
  DISPLAY p_supplies.exp_date TO supplies.exp_date

  DISPLAY get_str(2526) TO dl_f1
  DISPLAY get_str(2527) TO dl_f2
  DISPLAY get_str(2528) TO dl_f3
  DISPLAY get_str(2529) TO dl_f4
  DISPLAY get_str(2530) TO dl_f7
  DISPLAY get_str(2531) TO dl_f8
  DISPLAY get_str(2532) TO dl_exp
  DISPLAY get_str(2533) TO dl_st
  DISPLAY get_str(2534) TO dl_f14
  DISPLAY get_str(2535) TO lbTitle
  
  IF fgl_fglgui() THEN --gui client
    CALL fgl_settitle("Supply Details")
    CALL fgl_grid_header("sc_suppl_lines" , "stock_id" , get_str(2522), "Left", "F71")
    CALL fgl_grid_header("sc_suppl_lines" , "quantity" , get_str(2523), "Left", "F72")
    CALL fgl_grid_header("sc_suppl_lines" , "item_cost", get_str(2524), "Left", "F73")
    CALL fgl_grid_header("sc_suppl_lines" , "item_desc", get_str(2525), "Left", "F74")
    CALL supply_state_combo_list("status_name", p_supplies.suppl_id)
    DISPLAY get_str(810) TO bt_ok
    DISPLAY "!" TO bt_ok
    DISPLAY get_str(820) TO bt_cancel
    DISPLAY "!" TO bt_cancel
  ELSE
    DISPLAY get_str(2522) TO dl_f15
    DISPLAY get_str(2523) TO dl_a
    DISPLAY get_str(2524) TO dl_f17
    DISPLAY get_str(2525) TO dl_f18
    DISPLAY get_str(2536) TO lbInfo1
  END IF

  DISPLAY get_supplies_status(p_supplies.state) TO status_name

END FUNCTION