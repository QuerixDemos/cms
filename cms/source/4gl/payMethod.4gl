##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the payment method 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                      RETURN
# get_pay_method_id(pay_method_name)               get payment method id from name                   l_pay_method_id
# get_pay_method_name(pay_method_id)               Get pay_method_id from name                       l_pay_method_name
# get_pay_method_desc(p_pay_method_id)             Get Pay method description from id                l_pay_method_rec.pay_method_desc
# get_pay_method_rec(p_pay_method_id)              Get the pay_method record from pa_method_id       l_pay_method.*
# payment_method_combo_list(cb_field_name)         Populate payment_method combo list from database  NONE
# pay_method_popup()                               Payment Method selection window                   p_pay_method_id
# pay_method_create()                              Create a new payment method record                NULL
# pay_method_delete(p_pay_method_id)               Delete a payment method record                    NONE
# pay_method_edit(p_pay_method_id)                 Edit Payment method record                        NONE
# pay_method_input(p_pay_method)                   Input payment method details (edit/create)        l_pay_method.*
# pay_method_view(p_pay_method_rec)                View payment method record by ID in window-form   NONE
# pay_method_view_by_rec(p_pay_method_rec)         View payment method record in window-form         NONE
# grid_header_pay_method_scroll()                  Populate payment method grid headers              NONE
############################################################################################################


######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


######################################################
# FUNCTION pay_method_popup_data_source()
#
# Data Source (cursor) for pay_method_popup
#
# RETURN NONE
######################################################
FUNCTION pay_method_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "pay_method_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM pay_methods "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_pay_type FROM sql_stmt
  DECLARE c_pay_type CURSOR FOR p_pay_type




END FUNCTION


######################################################
# FUNCTION pay_method_popup(p_pay_method_id,p_order_field,p_accept_action)
#
# Payment Method selection window
#
# RETURN p_pay_method_id
######################################################
FUNCTION pay_method_popup(p_pay_method_id,p_order_field,p_accept_action)
  DEFINE 
    p_pay_method_id              LIKE pay_methods.pay_method_id,
    l_pay_method_arr             DYNAMIC ARRAY OF RECORD LIKE pay_methods.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


    CALL fgl_window_open("w_pay_type_scroll", 2, 8, get_form_path("f_pay_method_scroll_l2"),FALSE) 
    CALL populate_payment_method_list_form_labels_g()
    
		CALL ui.Interface.setImage("qx://application/icon16/money/payment_type.png")
		CALL ui.Interface.setText("Payment Method")
		    
    #DISPLAY "!" TO fb_ok
    #DISPLAY "!" TO fb_cancel
    #DISPLAY "!" TO fb_create
    #DISPLAY "!" TO fb_edit
    #DISPLAY "!" TO fb_delete
    CALL fgl_settitle(get_str(1951))  --"Payment Method List")
    CALL grid_header_pay_method_scroll()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL pay_method_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_pay_type INTO l_pay_method_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1
    IF NOT i THEN
      CALL fgl_window_close("w_pay_type_scroll")
      RETURN NULL
    END IF

		IF i > 0 THEN
			CALL l_pay_method_arr.resize(i)  --correct the last element of the dynamic array
		END IF
		    
		
    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_pay_method_arr TO sc_pay_method.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE DISPLAY
        CALL publish_toolbar("PayMethodList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_pay_method_arr[i].pay_method_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL pay_method_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL pay_method_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL pay_method_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL pay_method_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","pay_method_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "pay_method_popup(p_pay_method_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("pay_method_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL pay_method_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_pay_method_arr[i].pay_method_id
        CALL pay_method_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_pay_method_arr[i].pay_method_id
        CALL pay_method_delete(i)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "pay_method_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "pay_method_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "pay_method_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_pay_type_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_pay_method_id
  ELSE 
    LET i = arr_curr()
    RETURN l_pay_method_arr[i].pay_method_id
  END IF

END FUNCTION


######################################################
# FUNCTION pay_method_create()
#
# Create a new payment method record
#
# RETURN NULL
######################################################
FUNCTION pay_method_create()
  DEFINE l_pay_method RECORD LIKE pay_methods.*

  LET l_pay_method.pay_method_id = 0
  
  CALL pay_method_input(l_pay_method.*)
    RETURNING l_pay_method.*

  IF NOT int_flag THEN
    INSERT INTO pay_methods VALUES (l_pay_method.*)
    RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
  END IF

  RETURN NULL
END FUNCTION


#################################################
# FUNCTION pay_method_delete(p_pay_method_id)
#
# Delete a payment method record
#
# RETURN NONE
#################################################
FUNCTION pay_method_delete(p_pay_method_id)
  DEFINE p_pay_method_id LIKE pay_methods.pay_method_id
  #do you really want to delete...
  IF yes_no(get_str(817),get_str(1980)) THEN
    DELETE FROM pay_methods 
      WHERE pay_method_id = p_pay_method_id
  END IF
END FUNCTION


#################################################
# FUNCTION pay_method_edit(p_pay_method_id)
#
# Edit Payment method record
#
# RETURN NONE
#################################################
FUNCTION pay_method_edit(p_pay_method_id)
  DEFINE 
    p_pay_method_id LIKE pay_methods.pay_method_id,
    l_pay_method    RECORD LIKE pay_methods.*

  SELECT pay_methods.* 
    INTO l_pay_method
    FROM pay_methods
    WHERE pay_methods.pay_method_id = p_pay_method_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL pay_method_input(l_pay_method.*) 
    RETURNING l_pay_method.*

  IF NOT int_flag THEN
    UPDATE pay_methods
      SET pay_method_name = l_pay_method.pay_method_name,
          pay_method_desc = l_pay_method.pay_method_desc
      WHERE pay_methods.pay_method_id = l_pay_method.pay_method_id
  END IF

END FUNCTION


#################################################
# FUNCTION pay_method_input(p_pay_method)
#
# Input payment method details (edit/create)
#
# RETURN l_pay_method.*
#################################################
FUNCTION pay_method_input(p_pay_method)
  DEFINE 
    p_pay_method RECORD LIKE pay_methods.*,
    l_pay_method RECORD LIKE pay_methods.*

  LET l_pay_method.* = p_pay_method.*

    CALL fgl_window_open("w_pay_method", 3, 3, get_form_path("f_pay_method_det_l2"), FALSE) 
    CALL populate_payment_method_form_labels_g()

    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_str(1950))  --"Payment Method")


  LET int_flag = FALSE

  INPUT BY NAME l_pay_method.pay_method_name,
                l_pay_method.pay_method_desc WITHOUT DEFAULTS

    AFTER FIELD pay_method_desc

    AFTER INPUT
      IF int_flag = FALSE THEN --user pressed OK
        IF l_pay_method.pay_method_name IS NULL THEN  --both fields must be filled
          ERROR get_str(1981)  --enter data for both fields
          NEXT FIELD pay_method_name
          CONTINUE INPUT
        END IF
        IF l_pay_method.pay_method_desc IS NULL THEN  --both fields must be filled
          ERROR get_str(1982)  --enter data for both fields
          NEXT FIELD pay_method_desc
          CONTINUE INPUT
        END IF
      ELSE  --user pressed cancel
        #"Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_str(53),get_str(54)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF 
      END IF  

  END INPUT

  call fgl_window_close("w_pay_method")

  RETURN l_pay_method.*
END FUNCTION


#################################################
# FUNCTION pay_method_view(p_pay_method_rec)
#
# View payment method record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION pay_method_view(p_pay_method_id)
  DEFINE 
    p_pay_method_id LIKE pay_methods.pay_method_id,
    l_pay_method_rec RECORD LIKE pay_methods.*

    CALL get_pay_method_rec(p_pay_method_id) RETURNING l_pay_method_rec.*
    CALL pay_method_view_by_rec(l_pay_method_rec.*)

END FUNCTION


#################################################
# FUNCTION pay_method_view_by_rec(p_pay_method_rec)
#
# View payment method record in window-form
#
# RETURN NONE
#################################################
FUNCTION pay_method_view_by_rec(p_pay_method_rec)
  DEFINE 
    p_pay_method_rec RECORD LIKE pay_methods.*,
    inp_char         CHAR

    CALL fgl_window_open("w_pay_method", 3, 3, get_form_path("f_pay_method_det_l2"), FALSE) 
    CALL populate_payment_method_form_labels_g()
    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_str(1952)) --payment method view


  DISPLAY BY NAME p_pay_method_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_pay_method")
END FUNCTION


