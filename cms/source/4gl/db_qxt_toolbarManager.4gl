##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"

MAIN
define
    a_config_filename    VARCHAR(50),
    l_database_app       VARCHAR(30),
    l_database_tool      VARCHAR(30),
    l_application_name   VARCHAR(30),
    l_language_name      LIKE qxt_language.language_name,
    l_language_id        LIKE qxt_language.language_id,
    l_tb_language_name   LIKE qxt_language.language_name,
    l_tb_language_id     LIKE qxt_language.language_id,
    local_debug          SMALLINT

  LET local_debug = FALSE

	#read arguments, config files and initialise
	CALL prepareModuleStart()
	
	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)

	
  ######################
  # Initialise QXT Base library qxt.lib
  ###################### 
  #CALL init_tools_data()

  ######################
  # Initialise String-Application libray - QXT Base library qxt.lib
  ###################### 
  CALL qxt_string_app_init(NULL)

  ######################
  # Icon/Toolbar Icon
  ######################    
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_icon_cfg_import()"
  END IF

  CALL process_icon_cfg_import(NULL)    --Note: Will only be processed if 
                                      -- a) the config file is specified in the main configuration ilfe
                                      -- b) If the specified configuration file exists



  LET qxt_g_filter.application_id = get_current_application_id()
  LET qxt_g_filter.tb_name = NULL
  LET qxt_g_filter.tb_instance = 1
  LET qxt_g_filter.tbi_name = NULL
  LET qxt_g_filter.tbi_obj_name = NULL
  LET qxt_g_filter.icon_category1_id = NULL
  LET qxt_g_filter.icon_category2_id = NULL
  LET qxt_g_filter.icon_category3_id = NULL


  #########GUI client only section ##########################

  IF fgl_fglgui() THEN
    CALL init_gui_path()                                                             --initialise common local & server paths
  END IF



    IF NOT fgl_window_open("w_tb_manager", 1,1, get_form_path("f_tb_manager_main_l2"), FALSE )  THEN
      CALL fgl_winmessage("Error","login()\nCannot open window w_tb_manager","error")
      RETURN FALSE 
    END IF
  CALL fgl_settitle("QXT DB Library Data Manager")    	
  #CALL fgl_form_open("manager",get_form_path("f_tb_manager_main"))
  #CALL fgl_form_display("manager")

  #DISPLAY "!" TO bt_tooltip_manager
  #DISPLAY "!" TO bt_icon
  #DISPLAY "!" TO bt_tbi_object_manager
  #DISPLAY "!" TO bt_tbi_manager
  #DISPLAY "!" TO bt_tb_manager
  #DISPLAY "!" TO bt_export_toolbar_file
  #DISPLAY "!" TO bt_category_mng
  #DISPLAY "!" TO bt_validate_icon_cache
  #DISPLAY "!" TO bt_validate_icon_server

  #DISPLAY "!" TO bt_exit
  #DISPLAY "!" TO bt_refresh


  LET l_language_name = get_language_name(get_language())  --English
  LET l_language_id = get_language()
  CALL set_data_language_id(l_language_id)
  LET l_tb_language_name = get_language_name(get_language())  --English
  LET l_tb_language_id = get_data_language_id()


  LET l_database_app = get_db_name_app()   --This was for future extensions and is currently not used   --cms
  LET l_database_tool = get_db_name_tool() --This was for future extensions and is currently not used  --cms
  LET l_application_name = get_application_name(get_current_application_id())

  WHILE TRUE


    CALL language_combo_list("l_language_name")
    CALL language_combo_list("cb_tb_language_name")
    #DISPLAY "English" TO cb_language_name  ATTRIBUTE (RED)
    #DISPLAY "English" TO cb_tb_language_name  ATTRIBUTE (GREEN)


    CALL application_name_combo_list("cb_application")
    CALL application_name_combo_list("database_tool")
    CALL application_name_combo_list("database_app")

    INPUT
      l_database_tool,
      l_database_app,
      l_language_name,
      l_tb_language_name,
      l_application_name   

      WITHOUT DEFAULTS

      FROM
        database_tool,
        database_app,
        l_language_name,
        cb_tb_language_name,
        cb_application
   #   HELP 1

      BEFORE INPUT
        CALL fgl_dialog_update_data()
      AFTER FIELD l_language_name
        CALL fgl_dialog_update_data()
        LET l_language_id = get_language_id(l_language_name)
        CALL set_language(l_language_id)

      AFTER FIELD cb_tb_language_name
        CALL fgl_dialog_update_data()
        LET l_tb_language_id = get_language_id(l_tb_language_name)
        CALL set_data_language_id(l_tb_language_id)

      ON KEY (INTERRUPT)
        EXIT WHILE


      ON ACTION ("iconCategoryManager")
        CALL fgl_dialog_update_data()
        CALL icon_category_popup(NULL, l_tb_language_id,NULL,2)

      ON ACTION ("actEventManager")
        CALL fgl_dialog_update_data()
        CALL tbi_obj_event_popup(NULL,NULL,2)
     

      ON ACTION ("tooltipManager")
        CALL fgl_dialog_update_data()
        CALL tbi_tooltip_popup(NULL,l_tb_language_id, "string_id",2)

      ON ACTION ("actIconManager")
        CALL fgl_dialog_update_data()
        CALL icon_popup(NULL,l_language_id,NULL,2)

      ON ACTION ("tbi_object_manager")
        CALL fgl_dialog_update_data()
        CALL tbi_obj_popup(NULL,l_tb_language_id,NULL,2)

      ON ACTION ("tbi_manager")
        CALL fgl_dialog_update_data()
        CALL tbi_popup(get_application_id(l_application_name),l_tb_language_id,NULL,NULL,2)
        #FUNCTION tbi_popup(p_application_id,p_language_id,p_tbi_name,p_order_field,p_accept_scope)

      ON ACTION ("tb_manager")
        CALL fgl_dialog_update_data()
        CALL tb_popup(get_application_id(l_application_name),NULL,NULL,NULL,l_tb_language_id,NULL,2)
        #tb_popup(p_application_id,p_tb_name,p_tb_instance,p_tbi_name,p_language_id,p_order_field,p_accept_action)

      ON ACTION ("exportToolbarFile")
        CALL qxt_tb_export_file(get_application_id(l_application_name),NULL,NULL,l_tb_language_id)

      ON ACTION ("validateIconCache")
        IF validate_client_icon_file_existence(get_application_id(l_application_name),1) THEN  --1=client
          CALL fgl_winmessage("Icon Client Cache validation failed","One or more program icons are missing in the client cache","info")
        ELSE
          CALL fgl_winmessage("Icon Client Cache validation successful","All program icons are located in the client cache","info")
        END IF

      ON ACTION ("validateIconServer")
        IF validate_client_icon_file_existence(get_application_id(l_application_name),0) THEN --0=Server
          CALL fgl_winmessage("Icon validation (located on server)  failed","One or more program icons are missing on the application server","info")
        ELSE
          CALL fgl_winmessage("Icon validation (located on server) successful","All program icons are located on the server","info")
        END IF


      ON ACTION ("exitInput")
        CALL fgl_dialog_update_data()
        EXIT INPUT

      ON ACTION ("exitProgram")
        EXIT WHILE
    END INPUT

  END WHILE

{
  MENU "Toolbar Manager"
   command "file import"
     CALL fgl_file_dialog("open", 1, "string a", "bbbbbb", "cccccc", "Ico (*.ico)|*.ico|Jpg (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
       RETURNING l_tmp_filename
    DISPLAY l_tmp_filename[001,100]
    DISPLAY l_tmp_filename[101,200]
    DISPLAY l_tmp_filename[201,300]
    DISPLAY l_tmp_filename[301,400]
    DISPLAY l_tmp_filename[401,500]

    COMMAND "tb module test"
      CALL tb_module_test()
    COMMAND "icon size"
      CALL icon_size_popup(NULL,NULL,2)

    COMMAND "icon path"
      CALL icon_path_popup(NULL,NULL,2)

    COMMAND "icon category"
      CALL icon_category_popup(NULL,NULL,NULL,2)

    COMMAND "icon property"
      CALL icon_property_popup(NULL,NULL,2)

    COMMAND "icon"
      CALL icon_popup(NULL,NULL,2)

    COMMAND "toolbar string"
      CALL tbi_tooltip_popup(NULL,NULL, NULL,2)

    COMMAND "Toolbar menu item Object Event"
      CALL tbi_obj_event_popup(NULL,NULL,2)

    COMMAND "Toolbar Menu Item Object"
      CALL tbi_obj_popup(NULL,NULL,2)

    COMMAND "Toolbar Menu Item"
      CALL tbi_popup(NULL,NULL,NULL,2)

    COMMAND "Toolbar Menu"
      CALL tb_popup(get_current_application_id(),NULL,1,NULL,2)

    COMMAND "Exit"
      EXIT MENU

  END MENU

}

END MAIN






