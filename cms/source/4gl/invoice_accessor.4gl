##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################################################
# Functions to manage Invoices
##################################################################################################################################################
#
# FUNCTION:                                                            DESCRIPTON:                                       RETURN:
# invoice_main()                                                       Main Invoice Menu                                 NONE
# get_account_id_from_invoice_id(p_invoice_id)                         Get acoount ID from Invoice                       rv_account_id
# get_invoice_rec(p_invoice_id)                                        Get the invoice record (by id)                    l_invoice.*
# load_invoice_temp_detail()                                           Load invoice temporary details                    i   (i=integer)
# invoice_delete(p_invoice_id)                                         Delete an invoice                                 NONE
# invoice_create()                                                     Create a new invoice                              l_invoice.invoice_id
# invoice_input(p_invoice)                                             Invoice Input (invoice create sub-module)         p_invoice.*
# create_invoice_temp(p_invoice_id)                                    Create temporary invoice record                   NONE
# copy_invoice_temp(p_invoice_id,p_process_items)                      Creates a copy record of an invoice               NONE
# update_status_name(p_invoice, p_status_name)                         Update status                                     NONE
# update_processible_invoice(p_invoice)                                Update status in "Wait processing"                NONE
# mass_update_processible_invoices()                                   Update processible invoices                       p_invoice.*, input_status
# invoice_input_header(p_invoice)                                      Input the invoice header details                  p_invoice.*, input_status
# invoice_input_detail(p_invoice,w_name)                               Input invoice detail                              input_status  (integer)
# update_line_total()                                                  Update the invoice line totals                    NONE
# update_totals(cnt)                                                   Updates all line totals (individual)              NONE
# invoice_update_totals(p_invoice_id)                                  Calculates invoice total and stores it to the DB  NONE
# invoice_edit(p_invoice_id)                                           Edit an invoice                                   NONE
# invoice_query(p_status)                                              Calls popup and views returned invoice            NONE
# invoice_popup_data_source(p_order_field,p_ord_dir,p_status)          Show invoice list & choose view/edit/delete       NULL or rv_invoice_id
# invoice_popup(p_invoice_id,p_order_field,p_accept_action,p_status)   Show invoice list & choose view/edit/delete       NULL or rv_invoice_id  (integer)
# select_invoices()                                                    called for print                                  NONE
# show_invoice_header(p_invoice)                                       displays the invoice header details to form       NONE
# show_invoice_detail(p_invoice_id)                                    Display invoice details                           NONE
# invoice_show_temp_detail()                                           Show temporary invoice detail                     NONE
# show_invoice(p_invoice_id)                                           Display invoice fields by name                    NONE
# invoice_view(p_invoice_id)                                           View Invoice Record                               NONE
# past_due_val()                                                       Return day interval                               day_interval 
# get_invoice_status(p_status_id,p_invoice_date)                       Get invoice status                                status or err_msg         
# is_past_due(p_status_id,p_invoice_date)                              Check if invoice is past due                      TRUE or FALSE
# display_credit_note(p_balance)                                       Display credit note                               NONE
# verify_inv_data(p_invoice)                                           Verify input data of invoice                      TRUE or FALSE
# verify_inv_lines(p_invoice_lines)                                    Verify input stock items of invoice               TRUE or FALSE
# invoice_status_combo_list(cb_field_name, p_invoice_id)               Populate invoice_status combo list from database  NONE
# get_inv_status_id(inv_status_name)                                   Get invoice status id from name                   l_inv_status_id
# get_inv_status_name(inv_status_id)                                   Get invoice status name from id                   l_inv_status_name
# grid_header_f_invoice_scroll_g()                                     Populate grid headers on form for gui mode        NONE
# populate_invoice_form_labels_t()                                     Populate invoice form for text mode               NONE
# populate_invoice_form_labels_g()                                     Populate invoice form for gui mode                NONE
# populate_invoice_form_view_labels_t()                                Populate invoice view form for text mode          NONE
# populate_invoice_form_view_labels_g()                                Populate invoice view form for gui mode           NONE
# populate_invoice_form_input_labels_t()                               Populate invoice input form for text mode         NONE
# populate_invoice_form_input_labels_g()                               Populate invoice input form for gui mode          NONE
# populate_invoice_form_query_labels_t()                               Populate invoice query form for text mode         NONE
# populate_invoice_form_query_labels_g()                               Populate invoice query form for gui mode          NONE
# populate_invoice_scroll_form_labels_t()                              Populate invoice scroll form for text mode        NONE
# populate_invoice_scroll_form_labels_g()                              Populate invoice scroll form for gui mode         NONE
#
##################################################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


############################################################################################################################
# Data Access functions
############################################################################################################################

###################################################
# FUNCTION get_account_id_from_invoice_id(p_invoice_id)
#
# Returns the account_id from an invoice_id
#
# RETURN rv_account_id
###################################################
FUNCTION get_account_id_from_invoice_id(p_invoice_id)
  DEFINE 
    p_invoice_id  LIKE invoice.invoice_id,
    rv_account_id LIKE account.account_id

  SELECT invoice.account_id
    INTO rv_account_id
    FROM invoice
    WHERE invoice.invoice_id = p_invoice_id
 
  RETURN rv_account_id

END FUNCTION



###################################################
# FUNCTION get_invoice_rec(p_invoice_id)
#
# Get invoice record from id
#
# RETURN l_invoice.* (LIKE invoice.*)
###################################################
FUNCTION get_invoice_rec(p_invoice_id)
  DEFINE 
    p_invoice_id   LIKE invoice.invoice_id,
    r_invoice      RECORD LIKE invoice.*

  SELECT invoice.*
    INTO r_invoice.*
    FROM invoice
    WHERE invoice.invoice_id = p_invoice_id

  RETURN r_invoice.*

END FUNCTION


