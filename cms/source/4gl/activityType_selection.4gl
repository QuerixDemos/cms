##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


######################################################
# FUNCTION activity_type_popup_data_source(p_order_field)
#
# Data Source (Curosr) for activity_type_popup
#
# RETURN NONE
######################################################
FUNCTION activity_type_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "type_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM activity_type "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_act_type FROM sql_stmt
  DECLARE c_act_type CURSOR FOR p_act_type


END FUNCTION


######################################################
# FUNCTION activity_type_popup(p_type_id,p_order_field,p_accept_action)
#
# Display all activity types for selection & management
#
# RETURN  l_act_arr[i].type_id OR p_type_id
######################################################
FUNCTION activity_type_popup(p_type_id,p_order_field,p_accept_action)
  DEFINE 
    p_type_id               LIKE activity_type.type_id,
    l_act_arr               DYNAMIC ARRAY OF RECORD LIKE activity_type.*,
    i                       INTEGER,
    p_order_field,p_order_field2           VARCHAR(128), 
    local_debug             SMALLINT,
    p_accept_action         SMALLINT,
    err_msg                 VARCHAR(240),
    l_ord_dir               SMALLINT

  LET local_debug = FALSE  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC

  IF local_Debug THEN
    DISPLAY "activity_type_popup() - p_type_id =", p_type_id
    DISPLAY "activity_type_popup() - p_order_field =", p_order_field
    DISPLAY "activity_type_popup() - p_accept_action =", p_accept_action
  END IF

  IF NOT p_order_field THEN
    LET p_order_field = "type_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF

    CALL fgl_window_open("w_act_type_popup", 4, 4, get_form_path("f_activity_type_scroll_l2"), FALSE)

 		CALL ui.Interface.setImage("qx://application/icon16/basic/pin/pin_blue-type.png")
		CALL ui.Interface.setText("Activity Type")      

    CALL populate_act_type_list_form_labels_g()


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL activity_type_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_act_type INTO l_act_arr[i].*
      LET i = i + 1
      IF i > 50 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_act_arr.resize(i)   --remove the last item which has no data
		END IF 
		    
    
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_act_arr TO s_act_arr.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 504

      BEFORE DISPLAY
        CALL publish_toolbar("ActivityTypeList",0)

      ON KEY(ACCEPT)  --ON OK events can be customized

        LET i = arr_curr()
        LET i = l_act_arr[i].type_id

        CASE p_accept_action

          WHEN 0
            EXIT WHILE

        
          WHEN 1  --view
            CALL activity_type_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL activity_type_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL activity_type_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL activity_type_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","activity_type_popup()\nActivity Type Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL activity_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

            OTHERWISE
              LET err_msg = "activity_type_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("activity_type_popup() - 4GL Source Error",err_msg, "error") 
          END CASE

      ON KEY (F4) -- add 
        CURRENT WINDOW IS SCREEN
        LET i = arr_curr()
        CALL activity_type_create()
        CALL fgl_window_current("w_act_type_popup")    
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CURRENT WINDOW IS SCREEN
        LET i = arr_curr()
        CALL activity_type_edit(l_act_arr[i].type_id)
        CALL fgl_window_current("w_act_type_popup")    
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CURRENT WINDOW IS SCREEN
        LET i = arr_curr()
        CALL activity_type_delete(l_act_arr[i].type_id)
        CALL fgl_window_current("w_act_type_popup")     
        EXIT DISPLAY


      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "type_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "user_def"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE


  CALL fgl_window_close("w_act_type_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_type_id
  ELSE 
    LET i = arr_curr()
    RETURN l_act_arr[i].type_id
  END IF

END FUNCTION

