##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings icon
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_icon_edit()
#
# Edit icon settings (icon.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_icon_edit()
  DEFINE 
    l_settings_icon_rec  OF t_qxt_settings_icon_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_icon",5,5,get_form_path("f_qxt_settings_icon"),FALSE)
  CALL qxt_settings_icon_view_form_details()

  #Get the corresponding record
  CALL get_settings_icon_form_rec()
    RETURNING  l_settings_icon_rec.*

  #Input
  INPUT BY NAME l_settings_icon_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    ON ACTION("settingsSave")  --Save to disk
      CALL copy_settings_icon_form_rec(l_settings_icon_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL copy_settings_icon_form_rec(l_settings_icon_rec.*) --Write to memory
      CALL process_main_cfg_export(NULL) --Write to disk
      EXIT INPUT

    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD icon_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_icon_form_rec(l_settings_icon_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  CALL publish_toolbar("settingsCfg",1)

  IF NOT int_flag THEN
    CALL copy_settings_icon_form_rec(l_settings_icon_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_icon")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_icon_view_form_details()

  CALL fgl_settitle(get_str_tool(200))

  DISPLAY get_str_tool(201) TO dl_f106 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(202) TO dl_f107 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(203) TO dl_f108 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(204) TO dl_f109 ATTRIBUTE(BLUE, BOLD)


END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_icon_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_icon_form_rec,
    local_debug     SMALLINT
  
  LET local_debug = FALSE

  ##################################
  # icon.cfg Configuration file
  ##################################
  #[icon] Section
  LET l_settings_form.icon10_path     = qxt_settings.icon10_path  --Help System Type (from app server file or online webserver)
  LET l_settings_form.icon16_path     = qxt_settings.icon16_path     --File name of the html help url map config file
  LET l_settings_form.icon24_path     = qxt_settings.icon24_path     --Table name of the html help url map config information
  LET l_settings_form.icon32_path     = qxt_settings.icon32_path          --Base DIR for the help file url ie. help/help-html

  IF local_debug THEN
    DISPLAY "get_settings_icon_form_rec() - qxt_settings.icon10_path =",        qxt_settings.icon10_path 
    DISPLAY "get_settings_icon_form_rec() - qxt_settings.icon16_path =",        qxt_settings.icon16_path 
    DISPLAY "get_settings_icon_form_rec() - qxt_settings.icon_url_map_tname =", qxt_settings.icon24_path 
    DISPLAY "get_settings_icon_form_rec() - qxt_settings.icon_base_dir =",      qxt_settings.icon32_path 

    DISPLAY "get_settings_icon_form_rec() - l_settings_form.icon_system_type_name =", l_settings_form.icon10_path 
    DISPLAY "get_settings_icon_form_rec() - l_settings_form.icon_url_map_fname =",    l_settings_form.icon16_path 
    DISPLAY "get_settings_icon_form_rec() - l_settings_form.icon_url_map_tname =",    l_settings_form.icon24_path 
    DISPLAY "get_settings_icon_form_rec() - l_settings_form.icon_base_dir =",         l_settings_form.icon32_path 
  END IF

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_icon_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_icon_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_icon_form_rec,
    local_debug         SMALLINT

  LET local_debug = FALSE
  
  ##################################
  # icon.cfg Configuration file
  ##################################
  #[icon] Section
  LET qxt_settings.icon10_path     = p_settings_form_rec.icon10_path  --Help System Type (from app server file or online webserver)
  LET qxt_settings.icon16_path     = p_settings_form_rec.icon16_path     --File name of the html help url map config file
  LET qxt_settings.icon24_path     = p_settings_form_rec.icon24_path     --Table name of the html help url map config information
  LET qxt_settings.icon32_path     = p_settings_form_rec.icon32_path          --Base DIR for the help file url ie. help/help-html

  IF local_debug THEN
    DISPLAY "copy_settings_icon_form_rec() - qxt_settings.icon10_path =",        qxt_settings.icon10_path 
    DISPLAY "copy_settings_icon_form_rec() - qxt_settings.icon16_path =",        qxt_settings.icon16_path 
    DISPLAY "copy_settings_icon_form_rec() - qxt_settings.icon_url_map_tname =", qxt_settings.icon24_path 
    DISPLAY "copy_settings_icon_form_rec() - qxt_settings.icon_base_dir =",      qxt_settings.icon32_path 

    DISPLAY "copy_settings_icon_form_rec() - p_settings_form_rec.icon10_path =",        p_settings_form_rec.icon10_path 
    DISPLAY "copy_settings_icon_form_rec() - p_settings_form_rec.icon16_path =",        p_settings_form_rec.icon16_path 
    DISPLAY "copy_settings_icon_form_rec() - p_settings_form_rec.icon_url_map_tname =", p_settings_form_rec.icon24_path 
    DISPLAY "copy_settings_icon_form_rec() - p_settings_form_rec.icon_base_dir =",      p_settings_form_rec.icon32_path 
  END IF


END FUNCTION



####################################################################################################
# EOF
####################################################################################################









