##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage activities
##################################################################################################################
#
# FUNCTION:                                      DESCRIPTION:                                            RETURN:
# country_combo_list()                           Populate the combo list box from DB                     NONE
# country_popup_data_source()                    Data Source (cursor) for the country_popup() function   NONE
# country_popup()                                popup a country lookup window with table management t.  p_country   (p_country LIKE country.country  )
# country_edit(p_country)                        Edit country record                                     NONE
# country_view(p_country_rec)                    DISPLAY country details in a form                       NONE
# country_input(p_country)                       Input country details from form                         l_country.*
# country_delete(p_country)                      Delete Country record                                   NONE
# country_create()                               Create new country record                               NONE
# country_advanced_lookup()                      Allows to search by entering the first letters          rv_comp_id
# country_advanced_lookup_update                 sub-routine of country_advanced_lookup to               rv_comp_id  (LIKE contact.cont_id)
# (query_filter, field_filter, do_choose)        update display
# get_country_rec(p_country)                     get country record from country                         r_country_rec.*
# 
#
# 
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

###################################################################################
# FUNCTION country_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for the country_popup() function
#
# RETURN NONE
###################################################################################
FUNCTION country_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "country"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM country "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED

  END IF

  PREPARE p_country_scroll FROM sql_stmt
  DECLARE c_country_scroll CURSOR FOR p_country_scroll

END FUNCTION


###################################################################################
# FUNCTION country_popup()
#
# popup a country lookup window with table management tools
#
# RETURN p_country   (p_country LIKE country.country  )
###################################################################################
FUNCTION country_popup(p_country,p_order_field,p_accept_action)
  DEFINE l_country_arr DYNAMIC ARRAY OF t_country_rec,
    #row_id             SMALLINT,
    p_country          LIKE country.country,  --return value = found invoice_id
    i                  INTEGER,
    #current_row        INTEGER,
    abort_flag        SMALLINT,
    p_order_field,p_order_field2      VARCHAR(128), 
    local_debug        SMALLINT,
    p_accept_action    SMALLINT,
    err_msg            VARCHAR(240),
    l_ord_dir          SMALLINT

  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC

	CALL fgl_window_open("w_country_popup", 3, 3, get_form_path("f_country_scroll_l2"), FALSE)
	CALL populate_country_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/map/map.png")
	CALL ui.Interface.setText("Country")
	


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF
    CALL country_popup_data_source(p_order_field,get_toggle_switch())

    LET int_flag = FALSE
    LET i = 1

    FOREACH c_country_scroll INTO l_country_arr[i]
      LET i = i + 1
    END FOREACH

    LET i = i - 1

		IF i > 0 THEN
			CALL l_country_arr.reSize(i)   --correct the last element of the dynamic array
		END IF
		
    #CALL set_count(i)

    DISPLAY ARRAY l_country_arr TO sc_country_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("CountryList",0)
      ON KEY (ACCEPT)

        LET i = arr_curr()
        CASE p_accept_action

          WHEN 0  --just return id/country
            EXIT WHILE
        
          WHEN 1  --view
            CALL country_view(get_country_rec(i))
            EXIT DISPLAY

          WHEN 2  --edit
            CALL country_edit(l_country_arr[i].country)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL country_delete(l_country_arr[i].country)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL country_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","activity_type_popup()\nActivity Type Print is not implemented","info")

            #CALL country_print(i)
            

            OTHERWISE
              LET err_msg = "activity_type_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("activity_type_popup() - 4GL Source Error",err_msg, "error") 
          END CASE

      ON KEY (F4) -- add
        CALL country_create()
        EXIT DISPLAY

      ON KEY(F5)  --edit
        LET i = arr_curr()
        CALL country_edit(l_country_arr[i].country)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        CALL country_delete(l_country_arr[i].country)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "country"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_country_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_country
  ELSE
    LET i = arr_curr()
    RETURN l_country_arr[i].country
  END IF

END FUNCTION


#############################################
# FUNCTION country_edit(p_country)
#
# Edit country record
#
# RETURN NONE
#############################################
FUNCTION country_edit(p_country)
  DEFINE 
    p_country   LIKE country.country,
    l_country   RECORD LIKE country.*

  SELECT country.* 
    INTO l_country
    FROM country
    WHERE country.country = p_country

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL country_input(l_country.*) 
    RETURNING l_country.*

  IF NOT int_flag THEN
    UPDATE country
       SET country = l_country.country
          # user_def = l_company_type.user_def
    WHERE country.country = p_country
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


#############################################
# FUNCTION country_input(p_country)
#
# Input country details from form
#
# RETURN l_country.*
#############################################
FUNCTION country_input(p_country)
  DEFINE 
    p_country RECORD LIKE country.*,
    l_country RECORD LIKE country.*

  LET l_country.* = p_country.*

    CALL fgl_window_open("w_country", 3, 3, get_form_path("f_country_l2"), FALSE)
    CALL populate_country_form_labels_g()

  INPUT BY NAME l_country.country WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_country.country  IS NULL THEN
          #"The Country field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(903),"error")
          CONTINUE INPUT
        END IF
      ELSE
        #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_str(53), get_str(54)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  CALL fgl_window_close("w_country")

  RETURN l_country.*
END FUNCTION


#############################################
# FUNCTION country_view(p_country_rec)
#
# DISPLAY country details in a form
#
# RETURN NONE
#############################################
FUNCTION country_view(p_country_rec)
  DEFINE 
    p_country_rec RECORD LIKE country.*,
    inp_char      CHAR

    CALL fgl_window_open("w_country", 3, 3, get_form_path("f_country_l2"), FALSE)
    CALL populate_country_form_labels_g()

    DISPLAY get_str(811) TO bt_ok
    DISPLAY "*" TO bt_cancel


  DISPLAY BY NAME p_country_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_country_view")

END FUNCTION


#############################################
# FUNCTION country_delete(p_country)
#
# Delete Country record
#
# RETURN NONE
#############################################
FUNCTION country_delete(p_country)
  DEFINE p_country LIKE country.country

  #"Delete!","Are you sure you want to delete this value? "
  LET tmp_str = get_str(56), " - ", p_country
  IF yes_no(get_str(55),tmp_str) THEN
    DELETE FROM country 
      WHERE country.country = p_country
  END IF

END FUNCTION


#############################################
# FUNCTION country_create()
#
# Create new country record
#
# RETURN NONE
#############################################
FUNCTION country_create()
  DEFINE l_country RECORD LIKE country.*

  CALL country_input(l_country.*)
    RETURNING l_country.*

  IF NOT int_flag THEN 
    #LET l_comp.type_id = 0
    INSERT INTO country VALUES (l_country.*)
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION



####################################################
# FUNCTION  country_advanced_lookup()
#
# Allows to search by entering the first letters
#
# RETURN rv_comp_id
####################################################
FUNCTION country_advanced_lookup(p_country)
  DEFINE 
    f_filter       VARCHAR(255),
    l_filter       VARCHAR(255),
    l_key          SMALLINT,
    len            INTEGER,
    do_choose      SMALLINT,
    rv_country,p_country LIKE country.country,
    local_debug    SMALLINT

  LET local_debug = FALSE   --0=off 1=on

  IF local_debug THEN
    DISPLAY "country_advanced_lookup(p_country) - p_country=", p_country
  END IF
  # f_filter is what we display to the lookup field
  # l_filter is what we use in the query (ie, f_filter, "%")

  LET rv_country = ""

  LET l_filter = "%"
  LET f_filter = ""
  LET do_choose = FALSE

    CALL fgl_window_open("w_advanced_lookup_country", 2, 2, get_form_path("f_country_scroll_advanced_l2"), FALSE)
    CALL populate_country_quick_search_form_labels_g()

    CALL grid_header_country_scroll_advanced()
    #Advanced Country Lookup - Press the first letters
    CALL fgl_settitle(get_str(902))


  WHILE TRUE
    CALL country_advanced_lookup_update(l_filter, f_filter, do_choose)
      RETURNING rv_country

    IF local_debug THEN
      DISPLAY "country_advanced_lookup() rv_country= ", rv_country
    END IF

    IF rv_country IS NOT NULL THEN
      EXIT WHILE
    END IF

    LET do_choose = FALSE

    CALL fgl_getkey()
      RETURNING l_key

    CASE 
      WHEN l_key = fgl_keyval("HELP")
        CALL showhelp(114)

      WHEN l_key = fgl_keyval("CANCEL")
        LET int_flag = TRUE
        EXIT WHILE

      WHEN l_key = fgl_keyval("accept")
        LET do_choose = TRUE

      WHEN l_key = fgl_keyval("backspace")
        LET len = LENGTH(f_filter) - 1
        IF len >= 1 THEN
          LET f_filter = f_filter[1,len]
          LET l_filter = f_filter, "%"
        ELSE 
          LET f_filter = ""
          LET l_filter = f_filter, "%"
        END IF

      WHEN (l_key >= ORD("0") AND l_key <= ORD("9"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("a") AND l_key <= ORD("z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("A") AND l_key <= ORD("Z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

    END CASE
  IF local_debug THEN
    DISPLAY "in case - country_advanced_lookup(p_country) - int_flag=", int_flag
    DISPLAY "in case - country_advanced_lookup(p_country) - (abort)  p_country=", p_country
    DISPLAY "in case - country_advanced_lookup(p_country) - (success) rv_country=", rv_country
  END IF

  END WHILE

  CALL fgl_window_close("w_advanced_lookup_country")

  #return found country - on cancel, return calling argument

  IF local_debug THEN
    DISPLAY "country_advanced_lookup(p_country) - int_flag=", int_flag
    DISPLAY "country_advanced_lookup(p_country) - (abort)  p_country=", p_country
    DISPLAY "country_advanced_lookup(p_country) - (success) rv_country=", rv_country
  END IF

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_country
  ELSE
    RETURN rv_country
  END IF

END FUNCTION


####################################################
# FUNCTION  country_advanced_lookup_update(query_filter, field_filter, do_choose)
#
# sub-routine of country_advanced_lookup to update display
#
# RETURN rv_comp_id  (LIKE contact.cont_id)
####################################################
FUNCTION country_advanced_lookup_update(query_filter, field_filter, do_choose)
  DEFINE 
    query_filter, field_filter VARCHAR(255),   
    i             INTEGER,
    l_name_arr    DYNAMIC ARRAY OF t_country_rec,
    do_choose     SMALLINT,
    rv_country    LIKE country.country,
    local_debug   SMALLINT

  LET local_debug = FALSE  --0=off 1=on
  LET rv_country = ""

  DECLARE c_test_lookup CURSOR FOR 
    SELECT country.country
      FROM country
      WHERE UPPER(country.country) LIKE UPPER(query_filter)
      ORDER BY country.country ASC


  DISPLAY field_filter TO f_lookup

  LET i = 0

  FOREACH c_test_lookup INTO l_name_arr[i + 1].*
    LET i = i + 1
  END FOREACH

	LET i = i-1
	
	IF i > 0 THEN
		CALL l_name_arr.resize(i)   --correct the last element of the dynamic array
	END IF

	
  #CALL set_count(i)
  
  #LET int_flag = FALSE

  IF do_choose THEN
    DISPLAY ARRAY l_name_arr TO advanced_lookup_arr.*  
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")   HELP 1


    IF NOT int_flag THEN
      LET i = arr_curr()
      LET rv_country = l_name_arr[i].country
    END IF
  ELSE
    DISPLAY ARRAY l_name_arr TO advanced_lookup_arr.* WITHOUT SCROLL
  END IF

  IF local_debug THEN
    DISPLAY "country_advanced_lookup_update() int_flag= ", int_flag
    DISPLAY "country_advanced_lookup_update() query_filter= ", query_filter
    DISPLAY "country_advanced_lookup_update() field_filter= ", field_filter
    DISPLAY "country_advanced_lookup_update() do_choose= ", do_choose
    DISPLAY "country_advanced_lookup_update() rv_country= ",rv_country
  END IF

  RETURN rv_country
END FUNCTION


