##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage Invoices
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTON:                                       RETURN:
# title_combo_list(cb_field_name)             Populates titel combo list from db                NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


###################################################################################
# FUNCTION title_combo_list(cb_field_name)
#
# Populates titel combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION title_combo_list(cb_field_name)
  DEFINE 
    l_title_arr   DYNAMIC ARRAY OF t_title_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    abort_flag   SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_title_scroll2 CURSOR FOR 
    SELECT title.title
      FROM title

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_title_scroll2 INTO l_title_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_title_arr[row_count].title)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1
		If row_count > 0 THEN
			CALL l_title_arr.resize(row_count)  --remove the last item which has no data
		END IF


END FUNCTION