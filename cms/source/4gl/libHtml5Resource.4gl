##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


#####################################################################################
# FUNCTION importHtml5Resources(argCssId)
#
# Function to import html5 resources (css styles and js javaScript
#####################################################################################
FUNCTION importHtml5Resources(argCssId)
	DEFINE argCssId SMALLINT
	DEFINE msgStr STRING


			IF fgl_getenv("qx_child") IS NULL THEN -- only load stuff if it is not a child process

		CASE argCssId
			WHEN 100
				#display "load NO css/js argCssId=", argCssId  --"load master no css"
			#	CALL apply_theme("theme/_master_theme_no_css_blue.qxtheme")				
			WHEN 0
				#DISPLAy "load NO css/js argCssId=", argCssId
				#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector-blue.css"],[]) 			
				#this option is required to use the programName.css process (not to do a dynamic.-css import)
				#CALL fgl_winmessage("do nothing..","do nothing","info")
			WHEN 1  --SDI Classic CMS
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/_master.css"],[])
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js","nowait"],[])  -- i.e. add additonal header and satusbar elements	
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/scripts/messages.js","nowait"],[])		
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js","nowait"],[])  --import wrapper script to alter the style of an widget
				CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/inflair-tooltips.css"],[])    
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair-tooltips.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/jquery-tooltipster.css"],[])    
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/jquery-tooltipster.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar

				#DISPLAY "Load cms classic SDI&MDI CSS/JS  argCssId=", argCssId
					
			WHEN 2   --MDI Classic CMS
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/_master.css"],[])
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/custom-v3.js","nowait"],[])  -- i.e. add additonal header and satusbar elements	
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/scripts/messages.js","nowait"],[])		
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/alterClass.js","nowait"],[])  --import wrapper script to alter the style of an widget
				CALL ui.interface.frontcall("sample","changeFrameTemplate",[],[])  --change the viewports/template i.e. header area and statusbar attachment

				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/inflair-tooltips.css"],[])    
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/inflair-tooltips.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/jquery-tooltipster.css"],[])    
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/jquery-tooltipster.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/menu.css"],[])  --menu style options i.e. treemenu width 300px and lineWrap scroll for toolbar
				
			#display "load FACEBOOK   argCssId=", argCssId
				
			WHEN 3  
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/cmsfb/mutabor.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/cmsfb/transformers.css"],[])
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/cmsfb/transformers.js","nowait"],[])

			#display "load - FACEBOOK SDI   argCssId=", argCssId

			WHEN 4
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/cmsfb/mutabor.js","nowait"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/cmsfb/transformers.css"],[])
				CALL ui.interface.frontcall("html5","scriptImport",["qx://application/extensions/cmsfb/transformers.js","nowait"],[])

			#display "load - FACEBOOK MDI   argCssId=", argCssId

			WHEN 5
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/cms_mdi_grey.css"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/smoothness/custom-smoothness.css"],[])
				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/smoothness/form_class_selector-smoothness.css"],[])  --to apply style to elements with classes defined in the form
			#display "load theme  GREY"  

#			WHEN 5  
#				CALL ui.interface.frontcall("html5","styleImport",["qx://application/cms_mdi_orng.css"],[])
#				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])
#				CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector.css"],[])  --to apply style to elements with classes defined in the form


			#css was specified, but a none-,matching css id was specified (Error/Warning message)
			OTHERWISE 
				#CALL ui.interface.frontcall("html5","styleImport",["qx://application/cms_mdi_blue.css"],[])
				#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/custom.css"],[])
				#CALL ui.interface.frontcall("html5","styleImport",["qx://application/extensions/form_class_selector-blue.css"],[]) 
				LET msgStr = "The specified css id was not found\nID=", trim(get_cssId())  --, "\nLoading the defaut css..."
				CALL fgl_winmessage("Invalid css id was found in argument",msgStr,"error")
		END CASE

	END IF

	

END FUNCTION