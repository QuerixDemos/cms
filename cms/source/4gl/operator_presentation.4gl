##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# operator/login/password related functions
##################################################################################################################
#
# FUNCTIONS:                                              DESCRIPTION:                                           RETURN:
# operator_main()                                         Main operator Menu                                     NONE
# login()                                                 operator login function for cms                        rv (smallint)
# operator_popup()                                        display operator list and allow to select              l_operator_arr[i].operator_id or NULL
# operator_input(p_operator)                              enter operator details                                 p_operator.*  (p_operator RECORD LIKE operator.*)
# operator_edit(p_operator_id)                            Modify operator details                                NONE
# operator_create(p_type)                                 Create a new operator                                  sqlca.sqlerrd[2]
# operator_delete(p_operator_id)                          delete a operator                                      NONE
# operator_view(p_operator_rec)                           View a operator record using ID                        NONE
# operator_view_by_rec(p_operator_rec)                    View a operator record                                 NONE
# operator_setup()                                        menu to allow operator setup                           NONE
# change_password()                                       change the current password (current operator)         NONE
# logaway_operator()                                      not implemented - log away                             NONE
# admin_operator(p_operator)                              validate if the operator is an admin                   TRUE/FALSE
# get_operator_name(p_operator_id)                        get the operator_id from a operator.name               l_operator_name
# get_operator_id(p_operator_name)                        get the operator_id from a operator.name               l_operator_id
# get_operator_email_address(p_operator_id)               get the email address from the operator (p_operator_id)l_email_address
# get_operator_contact_id_from_operator_id(p_operator_id) get the contact_id from a perator_id                   l_cont_id
# operator_scroll_grid_header()                           Populate grid header                                   NONE
# get_operator_rec(p_operator_id)                         Get the operator record from an id                     l_operator.*
# get_current_operator_id()                               Return currently logged in operator id                 g_operator.operator_id
# get_current_operator_contact_id()                       get the contact_id from the current operator           g_operator.cont_id
# get_current_operator_email_address()                    Return currently logged in operator id                 g_operator.email_address
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

####################################################
# FUNCTION operator_scroll_grid_header()
#
# Populate grid header
#
# RETURN NONE
####################################################
FUNCTION operator_scroll_grid_header()
  CALL fgl_grid_header("sc_operator_arr","operator_id",get_str(1041),"right","F13")   --ID
  CALL fgl_grid_header("sc_operator_arr","name",get_str(1042),"left","F14")           --Name
  CALL fgl_grid_header("sc_operator_arr","password",get_str(1043),"left","F15")       --Password
  CALL fgl_grid_header("sc_operator_arr","type",get_str(1044),"left","F16")           --Type
  CALL fgl_grid_header("sc_operator_arr","cont_name",get_str(1045),"left","F17")      --Cont. Name
  CALL fgl_grid_header("sc_operator_arr","email_address",get_str(1046),"left","F18")  --E-Mail Addr.
END FUNCTION


#######################################################
# FUNCTION populate_login_form_labels_t()
#
# Populate operator details form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_login_form_labels_t()
  DISPLAY get_str(1010) TO lbTitle
  DISPLAY get_str(1011) TO dl_f1
  DISPLAY get_str(1012) TO dl_f2
  DISPLAY get_str(1013) TO dl_f3
  DISPLAY get_str(1014) TO dl_f4
  DISPLAY get_str(1015) TO dl_f5
  DISPLAY get_str(1016) TO dl_f6
  #DISPLAY get_str(1017) TO dl_f7
  #DISPLAY get_str(1018) TO dl_f8
  DISPLAY get_str(1019) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_operator_scroll_form_labels_g()
#
# Populate operator scroll form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_operator_scroll_form_labels_g()
  DISPLAY get_str(1040) TO lbTitle
  #DISPLAY get_str(1041) TO dl_f1
  #DISPLAY get_str(1042) TO dl_f2
  #DISPLAY get_str(1043) TO dl_f3
  #DISPLAY get_str(1044) TO dl_f4
  #DISPLAY get_str(1045) TO dl_f5
  #DISPLAY get_str(1046) TO dl_f6
  #DISPLAY get_str(1047) TO dl_f7
  #DISPLAY get_str(1048) TO dl_f8
  DISPLAY get_str(1049) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(823) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1040))  --"Operator List")
  CALL operator_scroll_grid_header()

END FUNCTION


#######################################################
# FUNCTION populate_operator_scroll_form_view_labels_t()
#
# Populate operator scroll form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_operator_scroll_form_view_labels_t()
  DISPLAY get_str(1040) TO lbTitle
  DISPLAY get_str(1041) TO dl_f1
  DISPLAY get_str(1042) TO dl_f2
  DISPLAY get_str(1043) TO dl_f3
  DISPLAY get_str(1044) TO dl_f4
  DISPLAY get_str(1045) TO dl_f5
  DISPLAY get_str(1046) TO dl_f6
  #DISPLAY get_str(1047) TO dl_f7
  #DISPLAY get_str(1048) TO dl_f8
  DISPLAY get_str(1049) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_operator_scroll_form_view_labels_g()
#
# Populate operator scroll form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_operator_scroll_form_view_labels_g()
  DISPLAY get_str(1040) TO lbTitle
  #DISPLAY get_str(1041) TO dl_f1
  #DISPLAY get_str(1042) TO dl_f2
  #DISPLAY get_str(1043) TO dl_f3
  #DISPLAY get_str(1044) TO dl_f4
  #DISPLAY get_str(1045) TO dl_f5
  #DISPLAY get_str(1046) TO dl_f6
  #DISPLAY get_str(1047) TO dl_f7
  #DISPLAY get_str(1048) TO dl_f8
  DISPLAY get_str(1049) TO lbInfo1


  DISPLAY get_str(823) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(1040))  --"Operator List")
  CALL operator_scroll_grid_header()


  DISPLAY get_str(811) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "*" TO bt_cancel

END FUNCTION


#######################################################
# FUNCTION populate_operator_details_form_labels_g()
#
# Populate operator details form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_operator_details_form_labels_g()
  DISPLAY get_str(1060) TO lbTitle
  DISPLAY get_str(1061) TO dl_f1
  DISPLAY get_str(1062) TO dl_f2
  DISPLAY get_str(1063) TO dl_f3
  DISPLAY get_str(1064) TO dl_f4
  DISPLAY get_str(1065) TO dl_f5
  DISPLAY get_str(1066) TO dl_f6
  #DISPLAY get_str(1067) TO dl_f7
  #DISPLAY get_str(1068) TO dl_f8
  DISPLAY get_str(1069) TO lbInfo1

  CALL fgl_settitle(get_str(1020))  --"Operator")

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION

#######################################################
# FUNCTION populate_operator_details_form_labels_t()
#
# Populate operator details form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_operator_details_form_labels_t()
  DISPLAY get_str(1060) TO lbTitle
  DISPLAY get_str(1061) TO dl_f1
  DISPLAY get_str(1062) TO dl_f2
  DISPLAY get_str(1063) TO dl_f3
  DISPLAY get_str(1064) TO dl_f4
  DISPLAY get_str(1065) TO dl_f5
  DISPLAY get_str(1066) TO dl_f6
  #DISPLAY get_str(1067) TO dl_f7
  #DISPLAY get_str(1068) TO dl_f8
  DISPLAY get_str(1069) TO lbInfo1

END FUNCTION




