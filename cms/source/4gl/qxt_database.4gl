##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings
#
# This 4gl module includes data access functions for tool and application database name
#
# Modification History:
#
#########################################################################################################
#
#
# FUNCTION                    DESCRIPTION                                                      RETURN
# set_db_name_tool(p_db_name)      Set the main database name required by the qxt tools libraries   NONE
# set_db_name_app(p_db_name)       Set the main database name required by the application           NONE
# get_db_name_tool()               Get the main database name required by the qxt tools libraries   qxt_settings.db_name_tool
# get_db_name_app()                Get the main database name required by the application           qxt_settings.db_name_app
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

##############################################################
# FUNCTION set_db_name_tool(p_db_name)
#
# Set the main database name required by the qxt tools libraries
#
# RETURN NONE
##############################################################
FUNCTION set_db_name_tool(p_db_name)
  DEFINE
    p_db_name VARCHAR(18)

  LET qxt_settings.db_name_tool = p_db_name

END FUNCTION


##############################################################
# FUNCTION set_db_name_app(p_db_name)
#
# Set the main database name required by the application
#
# RETURN NONE
##############################################################
FUNCTION set_db_name_app(p_db_name)
  DEFINE
    p_db_name VARCHAR(18)

  LET qxt_settings.db_name_app = p_db_name

END FUNCTION


##############################################################
# FUNCTION get_db_name_tool()
#
# Get the main database name required by the qxt tools libraries
#
# RETURN qxt_settings.db_name_tool
##############################################################
FUNCTION get_db_name_tool()

  RETURN qxt_settings.db_name_tool

END FUNCTION



##############################################################
# FUNCTION get_db_name_app()
#
# Get the main database name required by the application
#
# RETURN qxt_settings.db_name_app
##############################################################
FUNCTION get_db_name_app()

  RETURN qxt_settings.db_name_app

END FUNCTION


Function get_dbWildcard()
	RETURN qxt_settings.dbWildcard
END FUNCTION	

# open_db function duplicate 
# db_schema_tools_functions  line 78
# alch
{
FUNCTION open_db(db_name)
  DEFINE db_name CHAR(64),
         sqlr    SMALLINT,
         retval  SMALLINT

  WHENEVER ERROR CONTINUE
    DATABASE db_name
    LET retval = status
    LET sqlr = SQLCA.SQLCODE

  WHENEVER ERROR STOP
  CASE
    WHEN (sqlr = -329 OR sqlr = -827)
      ERROR db_name CLIPPED,
        ": Database not found or no system permission."
    WHEN (sqlr = -349)
      ERROR db_name CLIPPED,
        " not opened, you do not have Connect privilege"
    WHEN (sqlr = -354)
      ERROR db_name CLIPPED,
        ": Incorrect database name format."
    WHEN (sqlr = -377)
      ERROR "open_db() called with a transaction still incomplete"
    WHEN (sqlr = -512)
      ERROR "Unable to open in exclusive mode, db is probably in use."
  END CASE
  RETURN retval
END FUNCTION
}