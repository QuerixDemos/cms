##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################

DATABASE CMS

############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_classic_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_classic_manager_lib_info()
  RETURN "DB - qxt_help_classic_manager"
END FUNCTION 



######################################################################################################################
# Data Access functins
######################################################################################################################


#########################################################
# FUNCTION get_id(p_filename)
#
# get help_classic id from name
#
# RETURN l_id
#########################################################
FUNCTION get_help_classic_id(p_filename)
  DEFINE 
    p_filename       LIKE qxt_help_classic.filename,
    l_id             LIKE qxt_help_classic.id,
    local_debug      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_id() - p_filename = ", p_filename
  END IF

  SELECT id
    INTO l_id
    FROM qxt_help_classic
    WHERE filename = p_filename

  RETURN l_id
END FUNCTION



######################################################
# FUNCTION get_id_from_filename(p_filename)
#
# Get the id from a file
#
# RETURN l_id
######################################################
FUNCTION get_id_from_filename(p_filename)
  DEFINE 
    p_filename     LIKE qxt_help_classic.filename,
    l_id           LIKE qxt_help_classic.id,
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_id() - filename = ", p_filename
  END IF

    SELECT qxt_help_classic.id
      INTO l_id
      FROM qxt_help_classic
      WHERE qxt_help_classic.filename = p_filename

  IF local_debug THEN
    DISPLAY "get_id() - l_id = ", l_id
  END IF

  RETURN l_id

END FUNCTION

####################################################
# FUNCTION help_classic_id_count(p_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION help_classic_id_count(p_id)
  DEFINE
    p_id    LIKE qxt_help_classic.id,
    r_count           SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_help_classic
      WHERE qxt_help_classic.id = p_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION



######################################################
# FUNCTION get_help_classic_rec(p_id)
#
# Get the help_classic record from pa_method_id
#
# RETURN l_help_classic.*
######################################################
FUNCTION get_help_classic_rec(p_id)
  DEFINE 
    p_id               LIKE qxt_help_classic.id,
    l_help_classic     RECORD LIKE qxt_help_classic.*

  SELECT *
    INTO l_help_classic.*
    FROM qxt_help_classic
    WHERE qxt_help_classic.id = p_id

  RETURN l_help_classic.*

END FUNCTION



######################################################################################################################
# List Management and Combo List functins
######################################################################################################################

######################################################
# FUNCTION help_classic_popup_data_source()
#
# Data Source (cursor) for help_classic_popup
#
# RETURN NONE
######################################################
FUNCTION help_classic_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_help_classic.id, ",
                 "qxt_help_classic.filename ",
                 "FROM qxt_help_classic "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "help_classic_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_help_classic FROM sql_stmt
  DECLARE c_help_classic CURSOR FOR p_help_classic

END FUNCTION


######################################################
# FUNCTION help_classic_popup(p_id,p_order_field,p_accept_action)
#
# help_classic selection window
#
# RETURN p_id
######################################################
FUNCTION help_classic_popup(p_id,p_order_field,p_accept_action)
  DEFINE 
    p_id                LIKE qxt_help_classic.id,  --default return value if user cancels
    l_help_classic_arr           DYNAMIC ARRAY OF t_qxt_help_classic_form_rec,    --RECORD LIKE qxt_help_classic.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_id               LIKE qxt_help_classic.id,
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "help_classic_popup() - p_id=",p_id
    DISPLAY "help_classic_popup() - p_order_field=",p_order_field
    DISPLAY "help_classic_popup() - p_accept_action=",p_accept_action
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_help_classic_scroll", 2, 8, get_form_path("f_qxt_help_classic_scroll_g"),FALSE) 
    CALL populate_help_classic_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_help_classic_scroll", 2, 8, get_form_path("f_qxt_help_classic_scroll_t"),FALSE) 
    CALL populate_help_classic_list_form_labels_t()
  END IF


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL help_classic_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_help_classic INTO l_help_classic_arr[i].*
      IF local_debug THEN
        DISPLAY "help_classic_popup() - i=",i
        DISPLAY "help_classic_popup() - l_help_classic_arr[i].id=",l_help_classic_arr[i].id
        DISPLAY "help_classic_popup() - l_help_classic_arr[i].filename=",l_help_classic_arr[i].filename
    END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 100 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_help_classic_arr.resize(i)  -- resize dynamic array to remove last dirty element
		END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_help_classic_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "help_classic_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)

    LET int_flag = FALSE
    DISPLAY ARRAY l_help_classic_arr TO sc_help_classic.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET l_id = l_help_classic_arr[i].id
        #LET i = l_help_classic_arr[i].id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL help_classic_view(l_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL help_classic_edit(l_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL help_classic_delete(l_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL help_classic_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","help_classic_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "help_classic_popup(p_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("help_classic_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL help_classic_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_id = l_help_classic_arr[i].id
        CALL help_classic_edit(l_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_id = l_help_classic_arr[i].id
        CALL help_classic_delete(l_id)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_help_classic_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_id 
  ELSE 
    RETURN l_id 
  END IF

END FUNCTION


######################################################################################################################
# Edit/Create/Delete/Input Functions
######################################################################################################################


######################################################
# FUNCTION help_classic_create()
#
# Create a new help_classic record
#
# RETURN NULL
######################################################
FUNCTION help_classic_create()
  DEFINE 
    l_help_classic RECORD LIKE qxt_help_classic.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_g"), TRUE) 
    CALL populate_help_classic_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_t"), TRUE) 
    CALL populate_help_classic_form_create_labels_t()
  END IF

  LET l_help_classic.id = 0

  LET int_flag = FALSE
  
  #Initialise some variables

  # CALL the INPUT
  CALL help_classic_input(l_help_classic.*)
    RETURNING l_help_classic.*

  CALL fgl_window_close("w_help_classic")

  IF NOT int_flag THEN
    INSERT INTO qxt_help_classic VALUES (
      l_help_classic.id,
      l_help_classic.filename
                                         )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_help_classic.id
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION help_classic_edit(p_id)
#
# Edit help_classic record
#
# RETURN NONE
#################################################
FUNCTION help_classic_edit(p_id)
  DEFINE 
    p_id       LIKE qxt_help_classic.id,
    l_key1_id  LIKE qxt_help_classic.id,
    #l_help_classic_form_rec OF t_help_classic_form_rec,
    l_help_classic_rec      RECORD LIKE qxt_help_classic.*,
    local_debug          SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "help_classic_edit() - Function Entry Point"
    DISPLAY "help_classic_edit() - p_id=", p_id
  END IF

  #Store the primary key combination for the SQL update
  LET l_key1_id = p_id



  #Get the record (by id)
  CALL get_help_classic_rec(p_id) RETURNING l_help_classic_rec.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_g"), TRUE) 
    CALL populate_help_classic_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_t"), TRUE) 
    CALL populate_help_classic_form_edit_labels_t()
  END IF

  #Update the modification date

  #Call the INPUT
  CALL help_classic_input(l_help_classic_rec.*) 
    RETURNING l_help_classic_rec.*

    IF local_debug THEN
      DISPLAY "help_classic_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF

    CALL fgl_window_close("w_help_classic")

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "help_classic_edit() - l_help_classic_rec.id=",l_help_classic_rec.id
      DISPLAY "help_classic_edit() - l_help_classic_rec.filename=",l_help_classic_rec.filename
      #DISPLAY "help_classic_edit() - l_help_classic_rec.image_data=",l_help_classic_rec.image_data
    END IF

    UPDATE qxt_help_classic
      SET 
          id =             l_help_classic_rec.id,
          filename =       l_help_classic_rec.filename
      WHERE qxt_help_classic.id = l_key1_id


    IF local_debug THEN
      DISPLAY "help_classic_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_help_classic_rec.id
    END IF
  
  ELSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF



END FUNCTION


#################################################
# FUNCTION help_classic_input(p_help_classic)
#
# Input help_classic details (edit/create)
#
# RETURN l_help_classic.*
#################################################
FUNCTION help_classic_input(p_help_classic_rec)
  DEFINE 
    p_help_classic_rec            RECORD LIKE qxt_help_classic.*,
    l_help_classic_form_rec       OF t_qxt_help_classic_form_rec,
    l_server_blob_file         VARCHAR(300),  --,
    l_orignal_filename         LIKE qxt_help_classic.filename,
    l_orignal_id     LIKE qxt_help_classic.id,
    local_debug                SMALLINT

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_filename = p_help_classic_rec.filename
  LET l_orignal_id = p_help_classic_rec.id

  #copy record data to form_record format record
  CALL copy_help_classic_record_to_form_record(p_help_classic_rec.*) RETURNING l_help_classic_form_rec.*

  ####################
  #Start actual INPUT
  INPUT BY NAME l_help_classic_form_rec.* WITHOUT DEFAULTS HELP 1

    AFTER FIELD filename
      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_classic_form_rec.filename,NULL,TRUE)  THEN
        NEXT FIELD filename
      END IF



    # AFTER INPUT BLOCK
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_classic_form_rec.filename,NULL,TRUE)  THEN

          NEXT FIELD filename
          CONTINUE INPUT
        END IF

      #The  id must be unique
      IF help_classic_id_count(l_help_classic_form_rec.id) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_id <> l_help_classic_form_rec.id THEN  --it is not an edit operation
          CALL fgl_winmessage(get_str_tool(63),get_str_tool(64),"error")
          NEXT FIELD id
          CONTINUE INPUT
        END IF
      END IF


      #BLOB Data must be suplied, otherwise - the help system will not work correctly
  
  END INPUT

  IF local_debug THEN
    DISPLAY "help_classic_input() - l_help_classic_form_rec.id=",l_help_classic_form_rec.id
    DISPLAY "help_classic_input() - l_help_classic_form_rec.filename=",l_help_classic_form_rec.filename
    #DISPLAY "help_classic_edit() - l_help_classic_form_rec.image_data=",l_help_classic_form_rec.image_data
  END IF


  #Copy the form record data to a normal help_classic record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_help_classic_form_record_to_record(l_help_classic_form_rec.*) RETURNING p_help_classic_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "help_classic_input() - p_help_classic_rec.id=",p_help_classic_rec.id
    DISPLAY "help_classic_input() - p_help_classic_rec.filename=",p_help_classic_rec.filename
    #DISPLAY "help_classic_edit() - p_help_classic_rec.image_data=",p_help_classic_rec.image_data
  END IF

  RETURN p_help_classic_rec.*

END FUNCTION


#################################################
# FUNCTION help_classic_delete(p_id)
#
# Delete a help_classic record
#
# RETURN NONE
#################################################
FUNCTION help_classic_delete(p_id)
  DEFINE 
    p_id       LIKE qxt_help_classic.id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(980), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_help_classic 
        WHERE id = p_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION help_classic_view(p_id)
#
# View help_classic record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION help_classic_view(p_id)
  DEFINE 
    p_id              LIKE qxt_help_classic.id,
    l_help_classic_rec       RECORD LIKE qxt_help_classic.*

  CALL get_help_classic_rec(p_id) RETURNING l_help_classic_rec.*
  CALL help_classic_view_by_rec(l_help_classic_rec.*)

END FUNCTION


#################################################
# FUNCTION help_classic_view_by_rec(p_help_classic_rec)
#
# View help_classic record in window-form
#
# RETURN NONE
#################################################
FUNCTION help_classic_view_by_rec(p_help_classic_rec)
  DEFINE 
    p_help_classic_rec  RECORD LIKE qxt_help_classic.*,
    inp_char         CHAR

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_g"), TRUE) 
    CALL populate_help_classic_form_view_labels_g()

  ELSE
    CALL fgl_window_open("w_help_classic", 3, 3, get_form_path("f_qxt_help_classic_det_t"), TRUE) 
    CALL populate_help_classic_form_view_labels_t()

  END IF

  DISPLAY BY NAME p_help_classic_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_help_classic")
END FUNCTION



######################################################
# FUNCTION copy_help_classic_record_to_form_record(p_help_classic_rec)  
#
# Copy normal help_classic record data to type help_classic_form_rec
#
# RETURN l_help_classic_form_rec.*
######################################################
FUNCTION copy_help_classic_record_to_form_record(p_help_classic_rec)  
  DEFINE
    p_help_classic_rec       RECORD LIKE qxt_help_classic.*,
    l_help_classic_form_rec  OF t_qxt_help_classic_form_rec

  LET l_help_classic_form_rec.id = p_help_classic_rec.id
  LET l_help_classic_form_rec.filename = p_help_classic_rec.filename

  RETURN l_help_classic_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_help_classic_form_record_to_record(p_help_classic_form_rec)  
#
# Copy type help_classic_form_rec to normal help_classic record data
#
# RETURN l_help_classic_rec.*
######################################################
FUNCTION copy_help_classic_form_record_to_record(p_help_classic_form_rec)  
  DEFINE
    l_help_classic_rec       RECORD LIKE qxt_help_classic.*,
    p_help_classic_form_rec  OF t_qxt_help_classic_form_rec

  LET l_help_classic_rec.id = p_help_classic_form_rec.id
  LET l_help_classic_rec.filename = p_help_classic_form_rec.filename

  RETURN l_help_classic_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_help_classic_scroll()
#
# Populate help_classic grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_help_classic_scroll()
  CALL fgl_grid_header("sc_help_classic","id",get_str_tool(971),"right","F13")  --Help ID
  CALL fgl_grid_header("sc_help_classic","filename",get_str_tool(973),"left","F14")  --file name


END FUNCTION

#######################################################
# FUNCTION populate_help_classic_form_labels_g()
#
# Populate help_classic form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(970) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION



#######################################################
# FUNCTION populate_help_classic_form_labels_t()
#
# Populate help_classic form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_labels_t()

  DISPLAY get_str_tool(970) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_help_classic_form_edit_labels_g()
#
# Populate help_classic form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION



#######################################################
# FUNCTION populate_help_classic_form_edit_labels_t()
#
# Populate help_classic form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_edit_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_help_classic_form_view_labels_g()
#
# Populate help_classic form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_help_classic_form_view_labels_t()
#
# Populate help_classic form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_view_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_help_classic_form_create_labels_g()
#
# Populate help_classic form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel



END FUNCTION



#######################################################
# FUNCTION populate_help_classic_form_create_labels_t()
#
# Populate help_classic form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_form_create_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_help_classic_list_form_labels_g()
#
# Populate help_classic list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_help_classic_scroll()

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete

END FUNCTION


#######################################################
# FUNCTION populate_help_classic_list_form_labels_t()
#
# Populate help_classic list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_help_classic_list_form_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(16) TO lbTitle  --List

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION































