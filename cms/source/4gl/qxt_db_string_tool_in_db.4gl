##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Tool String
#
# Globals file for the string libary 
# Strings are located and accessed from the database
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_string_tool_lib_info()
#
# Simply returns libray name
#
# RETURN NONE
###########################################################
FUNCTION get_string_tool_lib_info()
  RETURN "DB - qxt_db_string_tool_in_db"
END FUNCTION 


######################################################################################################################
# Init functions
######################################################################################################################

###########################################################
# FUNCTION qxt_string_tool_init()
#
# Initialise app (application)
#
# RETURN NONE
###########################################################
FUNCTION qxt_string_tool_init(p_string_tool_table_name)
  DEFINE
    p_string_tool_table_name  VARCHAR(200)

  IF p_string_tool_table_name IS NULL THEN
    LET p_string_tool_table_name = get_string_tool_table_name()
  END IF

  IF p_string_tool_table_name IS NULL THEN
    CALL fgl_winmessage("Error in qxt_string_tool_init()","Error in qxt_string_tool_init()\nNo import string_tool_table_name was specified in the function call\nand also not found in the string.cfg file","error")
    EXIT PROGRAM
  ELSE 
    CALL set_string_tool_table_name(p_string_tool_table_name)
  END IF


  #must stay compatible
  # Nothing other than the table name  needs to be initialised if the strings are accessed directly from the database
END FUNCTION


###########################################################
# FUNCTION get_str_tool(p_string_id)
#
# Returns the string_tool string_data (corresponding to the id)
#
# RETURN l_string_data
###########################################################
FUNCTION get_str_tool(p_string_id)
  DEFINE
    p_string_id           LIKE qxt_string_tool.string_id,
    l_language_id         LIKE qxt_string_tool.language_id,
    l_default_language_id LIKE qxt_string_tool.language_id,
    l_string_data         LIKE qxt_string_tool.string_data


  LET l_language_id = get_language()
  LET l_default_language_id = get_language_default()

  SELECT string_data
  INTO l_string_data
  FROM qxt_string_tool
  WHERE string_id = p_string_id
  AND language_id = l_language_id


  #if no string was found for the specified language, re-query using the default language
  IF l_string_data IS NULL THEN

    SELECT string_data
    INTO l_string_data
    FROM qxt_string_tool
    WHERE string_id = p_string_id
    AND language_id = l_default_language_id

  END IF

  RETURN l_string_data

END FUNCTION







