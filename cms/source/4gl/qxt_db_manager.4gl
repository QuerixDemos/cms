##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_manager_globals.4gl"

MAIN
define
    a_config_filename    VARCHAR(50),
    l_database_app       VARCHAR(30),
    l_database_tool      VARCHAR(30),
    l_application_name   VARCHAR(30),
    l_application_id     VARCHAR(30),
    l_language_name      LIKE qxt_language.language_name,
    l_language_id        LIKE qxt_language.language_id,
    l_data_language_name LIKE qxt_language.language_name,
    l_data_language_id   LIKE qxt_language.language_id,
    local_debug          SMALLINT
  DEFER INTERRUPT

  LET local_debug = FALSE

	#read arguments, config files and initialise
	CALL prepareModuleStart()

	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)

	
{	
 # Make Keys windows like
  IF fgl_fglgui() THEN
    OPTIONS ACCEPT KEY RETURN  
  END IF

  OPTIONS 
    HELP KEY F1, 
#    FORM LINE 2,
    FIELD ORDER UNCONSTRAINED,
    INPUT WRAP,
    NEXT KEY F41,
    PREVIOUS KEY F42 --,

#    PROMPT LINE 4



  ##################################################################
  # Init Main QXT Configuration CFG
  # Can be specified as a program call argument
  ################################################################## 

  LET a_config_filename = ARG_VAL(1)
  LET a_config_filename = trim(a_config_filename)

  IF a_config_filename IS NULL THEN
     CALL set_main_cfg_filename("cfg/cms.cfg")
  ELSE
     CALL set_main_cfg_filename(a_config_filename)
  END IF

  ######################
  # Initialise QXT Base library qxt.lib
  ###################### 
  CALL init_tools_data()


  ######################
  # Classic Help 
  ###################### 
  CALL process_help_classic_cfg_import(NULL)

  ######################
  # Help Html 
  ###################### 
  CALL process_help_html_cfg_import(NULL)

  ######################
  # Print Html 
  ###################### 
  CALL process_print_html_cfg_import(NULL)

  ######################
  # Web Service
  ###################### 
  CALL process_webservice_cfg_import(NULL)

  ######################
  # DDE
  ###################### 
  CALL process_dde_cfg_import(NULL)

  ######################
  # Online Resource (i.e. self running demo from webserver
  ###################### 
  CALL process_online_resource_cfg_import(NULL)

  ######################
  # Web Service
  ###################### 
  CALL process_webservice_cfg_import(NULL)



  #########GUI client only section ##########################

  IF fgl_fglgui() THEN
    CALL init_gui_path()                                                             --initialise common local & server paths
  END IF
}
  CALL fgl_settitle("QXT Lib Toolbar Manager")

  CALL fgl_form_open("qxt_db_manager",get_form_path("f_qxt_db_manager_l2"))
  CALL fgl_form_display("qxt_db_manager")

{
  DISPLAY "!" TO bt_application_map_manager
  DISPLAY "!" TO bt_excel_dde_font_manager
  DISPLAY "!" TO bt_string_tool_manager
  DISPLAY "!" TO bt_string_app_manager
  DISPLAY "!" TO bt_string_cat_mng

  DISPLAY "!" TO bt_print_template_manager
  DISPLAY "!" TO bt_print_image_manager
  DISPLAY "!" TO bt_help_classic_manager
  DISPLAY "!" TO bt_document_manager
  DISPLAY "!" TO bt_help_url_map_manager
  DISPLAY "!" TO bt_help_html_document_manager
  DISPLAY "!" TO bt_language_manager
  DISPLAY "!" TO bt_edit_cfg_main
  DISPLAY "!" TO bt_edit_cfg_lang
  DISPLAY "!" TO bt_edit_cfg_string
  DISPLAY "!" TO bt_edit_cfg_help_classic
  DISPLAY "!" TO bt_edit_cfg_help_html
  DISPLAY "!" TO bt_edit_cfg_toolbar
  DISPLAY "!" TO bt_edit_cfg_icon
  DISPLAY "!" TO bt_edit_cfg_print_html
  DISPLAY "!" TO bt_edit_cfg_dde
  DISPLAY "!" TO bt_edit_cfg_online_resource
  DISPLAY "!" TO bt_edit_cfg_webservice

  DISPLAY "!" TO bt_exit
  DISPLAY "!" TO bt_refresh
}

{

  DISPLAY "!" TO bt_tbi_object_manager
  DISPLAY "!" TO bt_tbi_manager
  DISPLAY "!" TO bt_tb_manager

}

  LET l_language_name = get_language_name(get_language())
  LET l_language_id = get_language()
  CALL set_data_language_id(l_language_id)
  LET l_data_language_name = get_language_name(get_data_language_id())
  LET l_data_language_id = get_data_language_id()

  LET l_database_app = get_db_name_app()
  LET l_database_tool = get_db_name_tool()
  LET l_application_name = get_application_name(get_current_application_id())
  LET l_application_id = get_current_application_id()
  WHILE TRUE


    CALL language_combo_list("cb_language_name")
    CALL language_combo_list("cb_data_language_name")

    CALL application_name_combo_list("cb_application_name")

    INPUT
      l_database_tool,
      l_database_app,
      l_language_name,
      l_data_language_name,
      l_application_name   

      WITHOUT DEFAULTS

      FROM
        database_tool,
        database_app,
        cb_language_name,
        cb_data_language_name,
        cb_application_name
   #   HELP 1

     BEFORE INPUT
       #Set toolbar
       CALL publish_toolbar("managerMenu",0)

     AFTER FIELD cb_application_name
        CALL fgl_dialog_update_data()
        LET l_application_id = get_application_id(l_application_name) 

      AFTER FIELD cb_language_name
        CALL fgl_dialog_update_data()
        LET l_language_id = get_language_id(l_language_name)
        CALL set_language(l_language_id)

      AFTER FIELD cb_data_language_name
        CALL fgl_dialog_update_data()
        LET l_data_language_id = get_language_id(l_data_language_name)
        CALL set_data_language_id(l_data_language_id)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON ACTION ("applicationMapManager")
        CALL fgl_dialog_update_data()
        CALL application_popup(l_application_id,NULL,2)


      ON ACTION ("excelDdeFontManager")
        CALL fgl_dialog_update_data()
        CALL dde_font_popup(NULL,NULL,NULL)

      ON ACTION ("stringToolManager")
        CALL fgl_dialog_update_data()
        CALL string_tool_popup(NULL,get_data_language_id(), NULL,2)

      ON ACTION ("stringApplicationManager")
        CALL fgl_dialog_update_data()
        CALL string_app_popup(NULL,get_data_language_id(), NULL,2)

      ON ACTION ("stringCategoryManager")
        CALL fgl_dialog_update_data()
        CALL str_category_popup(NULL,get_data_language_id(), NULL,2)

      ON ACTION ("printTemplateManager")
        CALL fgl_dialog_update_data()
        CALL print_template_popup(NULL,get_data_language_id(),NULL,2)

      ON ACTION ("printImageManager")
        CALL fgl_dialog_update_data()
        CALL print_image_popup(NULL,NULL,2)

      ON ACTION ("helpClassicManager")
        CALL fgl_dialog_update_data()
        CALL help_classic_popup(NULL,NULL,2)

      ON ACTION ("documentManager")
        CALL fgl_dialog_update_data()
        CALL document_popup(NULL,NULL,2)
      
      ON ACTION ("helpUrlMapManager")
        CALL fgl_dialog_update_data()
        CALL help_url_map_popup(NULL,get_data_language_id(),NULL,2)
      
      ON ACTION ("helpHtmlDocManager")
        CALL fgl_dialog_update_data()
        CALL help_html_doc_popup(NULL,get_data_language_id(),NULL,2)
      
      ON ACTION ("languageManager")
        CALL fgl_dialog_update_data()
        CALL language_popup(get_data_language_id(),NULL,2)


      #Cfg Section
      ON ACTION ("editMainConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_main_edit()

      ON ACTION ("editLangConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_language_edit()

      ON ACTION ("editStringConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_string_edit()

      ON ACTION ("editHelpClassicConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_help_classic_edit()

      ON ACTION ("editHelpHtmlConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_help_html_edit()

      ON ACTION ("editToolbarConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_toolbar_edit()

      ON ACTION ("editIconConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_icon_edit()

      ON ACTION ("editPrintHtmlConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_print_html_edit()

      ON ACTION ("editDDEConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_dde_edit()

      ON ACTION ("editOnlineResourceConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_online_resource_edit()

      ON ACTION ("editWebServiceConfiguration")
        CALL fgl_dialog_update_data()
        CALL qxt_settings_webservice_edit()


       # CALL tbi_tooltip_popup(NULL,l_data_language_id, "string_id",2)
{
      ON ACTION ("iconManager")
        CALL fgl_dialog_update_data()
        CALL icon_popup(NULL,l_language_id,NULL,2)

      ON ACTION ("tbi_object_manager")
        CALL fgl_dialog_update_data()
        CALL tbi_obj_popup(NULL,l_tb_language_id,NULL,2)

      ON ACTION ("tbi_manager")
        CALL fgl_dialog_update_data()
        CALL tbi_popup(get_application_id(l_application_name),l_tb_language_id,NULL,NULL,2)
        #FUNCTION tbi_popup(p_application_id,p_language_id,p_tbi_name,p_order_field,p_accept_scope)

      ON ACTION ("tb_manager")
        CALL fgl_dialog_update_data()
        CALL tb_popup(get_current_application_id(),NULL,NULL,l_tb_language_id,NULL,2)
        #tb_popup(p_application_id,p_tb_name,p_tb_instance,p_language_id,p_order_field,p_accept_action)
 
}
     ON ACTION ("exitInput")
        CALL fgl_dialog_update_data()
        EXIT INPUT

      ON ACTION ("exitProgram")
        EXIT WHILE
    END INPUT

  END WHILE

  #Possible toolbar clean up
  CALL publish_toolbar("managerMenu",1)

{
  MENU "Toolbar Manager"
   command "file import"
     CALL fgl_file_dialog("open", 1, "string a", "bbbbbb", "cccccc", "Ico (*.ico)|*.ico|Jpg (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
       RETURNING l_tmp_filename
    DISPLAY l_tmp_filename[001,100]
    DISPLAY l_tmp_filename[101,200]
    DISPLAY l_tmp_filename[201,300]
    DISPLAY l_tmp_filename[301,400]
    DISPLAY l_tmp_filename[401,500]

    COMMAND "tb module test"
      CALL tb_module_test()
    COMMAND "icon size"
      CALL icon_size_popup(NULL,NULL,2)

    COMMAND "icon path"
      CALL icon_path_popup(NULL,NULL,2)

    COMMAND "icon category"
      CALL icon_category_popup(NULL,NULL,NULL,2)

    COMMAND "icon property"
      CALL icon_property_popup(NULL,NULL,2)

    COMMAND "icon"
      CALL icon_popup(NULL,NULL,2)

    COMMAND "toolbar string"
      CALL tbi_tooltip_popup(NULL,NULL, NULL,2)

    COMMAND "Toolbar menu item Object Event"
      CALL tbi_obj_event_popup(NULL,NULL,2)

    COMMAND "Toolbar Menu Item Object"
      CALL tbi_obj_popup(NULL,NULL,2)

    COMMAND "Toolbar Menu Item"
      CALL tbi_popup(NULL,NULL,NULL,2)

    COMMAND "Toolbar Menu"
      CALL tb_popup(get_current_application_id(),NULL,1,NULL,2)

    COMMAND "Exit"
      EXIT MENU

  END MENU
}


END MAIN









