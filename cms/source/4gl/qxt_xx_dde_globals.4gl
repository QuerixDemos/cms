##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the DDE tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

#######################################################################
# GLOBALS
#######################################################################
GLOBALS
  DEFINE
    qxt_dde
      RECORD
        debug      SMALLINT,
        app_name   VARCHAR(30),
        file_name  VARCHAR(200),
        success    SMALLINT,
        timeout    SMALLINT
      END RECORD

  DEFINE
    t_qxt_dde_excel_font_rec TYPE AS 
      RECORD
        font_name        VARCHAR(30), --VARCHAR(30),   -- Arial
        font_bold        VARCHAR(30), --SMALLINT,      -- false
        font_size        VARCHAR(30), --SMALLINT,      --  = 10
        strike_through   VARCHAR(30), --SMALLINT,      --  = false
        super_script     VARCHAR(30), --SMALLINT,      --  = false
        lower_script     VARCHAR(30), --SMALLINT,      --  = false
        option_1         VARCHAR(30), --SMALLINT,      --  = false
        option_2         VARCHAR(30), --SMALLINT,      --  = false
        option_3         VARCHAR(30), --SMALLINT,      --  = 1
        font_color       VARCHAR(30) --SMALLINT       --  = 5
      END RECORD
      
  DEFINE 
    qxt_dde_excel_font_rec DYNAMIC ARRAY OF 
      RECORD
        font             VARCHAR(30), --VARCHAR(30),   -- Arial
        bold             VARCHAR(30), --SMALLINT,      -- false
        size             VARCHAR(30), --SMALLINT,      --  = 10
        strike_through   VARCHAR(30), --SMALLINT,      --  = false
        super_script     VARCHAR(30), --SMALLINT,      --  = false
        lower_script     VARCHAR(30), --SMALLINT,      --  = false
        option_1         VARCHAR(30), --SMALLINT,      --  = false
        option_2         VARCHAR(30), --SMALLINT,      --  = false
        option_3         VARCHAR(30), --SMALLINT,      --  = 1
        color            VARCHAR(30) --SMALLINT       --  = 5
      END RECORD

END GLOBALS
