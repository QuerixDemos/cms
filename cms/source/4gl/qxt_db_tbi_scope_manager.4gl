##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the icon / menu_item_obj categories (lookup tables)
#
#
# FUNCTION                                            DESCRIPTION                                              RETURN
# toolbar_menu_action_combo_list(cb_field_name)            Populates toolbar_menu_action combo list from db              NONE
# tbi_scope2_combo_list(cb_field_name)            Populates toolbar_menu_action combo list from db              NONE
# tbi_scope3_combo_list(cb_field_name)            Populates toolbar_menu_action combo list from db              NONE
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"




#########################################################
# FUNCTION get_tbi_scope_name(p_tbi_scope_id)
#
# get tbi_scope_name from p_tbi_scope_id
#
# RETURN l_tbi_obj_name
#########################################################
FUNCTION get_tbi_scope_name(p_tbi_scope_id)
  DEFINE 
    p_tbi_scope_id    LIKE qxt_tbi_scope.tbi_scope_id,
    l_tbi_scope_name  LIKE qxt_tbi_scope.tbi_scope_name,
    local_debug                      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_scope_name() - p_tbi_scope_id = ", p_tbi_scope_id
  END IF

  SELECT qxt_tbi_scope.tbi_scope_name
    INTO l_tbi_scope_name
    FROM qxt_tbi_scope
    WHERE qxt_tbi_scope.tbi_scope_id = p_tbi_scope_id

  IF local_debug THEN
    DISPLAY "get_tbi_scope_name() - l_tbi_scope_name = ", l_tbi_scope_name
  END IF

  RETURN l_tbi_scope_name
END FUNCTION

#########################################################
# FUNCTION get_tbi_scope_id(p_tbi_scope_name)
#
# get tbi_scope_id from p_tbi_scope_id
#
# RETURN l_tbi_obj_id
#########################################################
FUNCTION get_tbi_scope_id(p_tbi_scope_name)
  DEFINE 
    p_tbi_scope_name    LIKE qxt_tbi_scope.tbi_scope_name,
    l_tbi_scope_id      LIKE qxt_tbi_scope.tbi_scope_id,
    local_debug                        SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_tbi_scope_id() - p_tbi_scope_name = ", p_tbi_scope_name
  END IF

  SELECT qxt_tbi_scope.tbi_scope_id
    INTO l_tbi_scope_id
    FROM qxt_tbi_scope
    WHERE qxt_tbi_scope.tbi_scope_name = p_tbi_scope_name

  IF local_debug THEN
    DISPLAY "get_tbi_scope_id() - l_tbi_scope_id = ", l_tbi_scope_id
  END IF

  RETURN l_tbi_scope_id
END FUNCTION

###################################################################################
# FUNCTION toolbar_menu_scope_name_combo_list(cb_field_name)
#
# Populates toolbar_menu_action combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION toolbar_menu_scope_name_combo_list(cb_field_name)
  DEFINE 
    l_tbi_scope_name_arr      DYNAMIC ARRAY OF LIKE qxt_tbi_scope.tbi_scope_name,
    row_count                 INTEGER,
    current_row               INTEGER,
    cb_field_name             VARCHAR(40),   --form field name for the country combo list field
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "toolbar_menu_action_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_tbi_scope_name_scroll2 CURSOR FOR 
    SELECT qxt_tbi_scope.tbi_scope_name
      FROM qxt_tbi_scope

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_tbi_scope_name_scroll2 INTO l_tbi_scope_name_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_tbi_scope_name_arr[row_count])

    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_tbi_scope_name_arr[row_count] CLIPPED, ")"
      DISPLAY "toolbar_menu_action_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

END FUNCTION











