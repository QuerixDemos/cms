##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Some simple functions add html tags to strings, exporting them to a file and displaying them
# with an external html browser (i.e. MS-IE) or displaying the html information
# using a html widget embedded in a form.
#
#
#
# FUNCTION                                        DESCRIPTION                                              RETURN
# process_html_help_import(file_name)             Import/Process the html help url mapping file            NONE
# set_help_id(id)                                 Set the html help id                                     NONE
# html_help(help_url)                             Call the html help feature (with a fully qualified url)  NONE
# get_help_url(id)                                get the fully qualified help url by id                   qxt_help_target_current
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"



{

!!! This has moved to tool_lib_help_environment.4gl   !!!

###########################################################
# FUNCTION set_help_id(id)
#
# Set the html help id
#
# RETURN NONE
###########################################################
FUNCTION set_help_id(id)
  DEFINE 
    id SMALLINT
  LET qxt_current_help_id = id

END FUNCTION


}


