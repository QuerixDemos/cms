##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################
# 
# FUNCTION:                                     DESCRIPTION:                                      RETURN
# get_delivery_rate(p_delivery_type_id)         Get the Delivery Rate                             l_delivery_rate
# get_delivery_rate_iv(p_invoice_id)            The the Delivery Rate from Invoice                l_delivery_rate
# get_delivery_type(p_delivery_type_id)         The the delivery_type name                        l_delivery_type_name
# get_delivery_type_name(p_delivery_type_id)    Get the delivery_type name  (seems a duplicate)   l_delivery_type_name
# delivery_type_popup()                         View currencies and return seletec value          l_delivery_type.*
# delivery_type_create()                        Create a new delivery_type (record)               rv
# delivery_type_delete(p_delivery_type_id)      Delete a delivery_type                            NONE
# delivery_type_edit(p_delivery_type_id)        Edit a delivery_type                              NONE
# delivery_type_view_by_rec(p_delivery_type)    VIEW delivery type data onform                    NONE
# delivery_type_view(p_delivery_type_id)        VIEW delivery type data onform with ID            NONE
# delivery_type_input(p_delivery_type)          Insert new delivery_type (subfunction of create)  l_delivery_type.*
# delivery_type_combo_list(cb_field_name)       Populates combo list with values from DB          NONE
# get_delivery_type_id(p_delivery_type_name)    get the delivery_type id form the name            l_delivery_type_id
#
#
#
##################################################




##################################################
# GLOBALS
##################################################
GLOBALS "globals.4gl"


##################################################
# FUNCTION get_delivery_rate(p_delivery_type_id)
#
# Get the delivery Rate                       
# RETURN l_delivery_rate
##################################################
FUNCTION get_delivery_rate(p_delivery_type_id)
  DEFINE 
    p_delivery_type_id LIKE delivery_type.delivery_type_id,
    l_delivery_rate    LIKE delivery_type.delivery_rate

  SELECT delivery_type.delivery_rate
    INTO l_delivery_rate
    FROM delivery_type
    WHERE delivery_type.delivery_type_id = p_delivery_type_id

  RETURN l_delivery_rate

END FUNCTION


##################################################
# FUNCTION get_delivery_rate_iv(p_invoice_id)
#
# The the delivery Rate from Invoice          
# RETURN l_delivery_rate
##################################################
FUNCTION get_delivery_rate_iv(p_invoice_id)
  DEFINE 
    p_invoice_id    LIKE invoice.invoice_id,
    l_delivery_rate LIKE delivery_type.delivery_rate

  SELECT delivery_type.delivery_rate
    INTO l_delivery_rate
    FROM delivery_type, invoice
    WHERE delivery_type.delivery_type_id = invoice.delivery_type_id
      AND invoice.invoice_id = p_invoice_id

  RETURN l_delivery_rate

END FUNCTION


##################################################
# FUNCTION get_delivery_type_name(p_delivery_type_id)
##################################################
FUNCTION get_delivery_type_name(p_delivery_type_id)
  DEFINE 
    p_delivery_type_id   LIKE delivery_type.delivery_type_id,
    l_delivery_type_name LIKE delivery_type.delivery_type_name

  SELECT delivery_type.delivery_type_name
    INTO l_delivery_type_name
    FROM delivery_type
    WHERE delivery_type.delivery_type_id = p_delivery_type_id

  RETURN l_delivery_type_name

END FUNCTION


##################################################
# FUNCTION get_delivery_type_id(p_delivery_type_name)
#
# get the delivery_type id form the name
#
# RETURN l_delivery_type_id
##################################################
FUNCTION get_delivery_type_id(p_delivery_type_name)
  DEFINE 
    l_delivery_type_id   LIKE delivery_type.delivery_type_id,
    p_delivery_type_name LIKE delivery_type.delivery_type_name

  SELECT delivery_type.delivery_type_id
    INTO l_delivery_type_id
    FROM delivery_type
    WHERE delivery_type.delivery_type_name = p_delivery_type_name

  RETURN l_delivery_type_id
END FUNCTION


##################################################
# FUNCTION get_delivery_type_rec(p_delivery_type_id)
##################################################
FUNCTION get_delivery_type_rec(p_delivery_type_id)
  DEFINE 
    p_delivery_type_id LIKE delivery_type.delivery_type_id,
    l_delivery_type    RECORD LIKE delivery_type.*

  SELECT delivery_type.*
    INTO l_delivery_type.*
    FROM delivery_type
    WHERE delivery_type.delivery_type_id = p_delivery_type_id

  RETURN l_delivery_type.*
END FUNCTION


#############################################
# FUNCTION delivery_type_popup_data_source(p_order_field,p_ord_dir)
#############################################
FUNCTION delivery_type_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "delivery_type_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM delivery_type "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED

  END IF

  PREPARE p_del_type FROM sql_stmt
  DECLARE c_del_type CURSOR FOR p_del_type


END FUNCTION


#############################################
# FUNCTION delivery_type_popup(p_delivery_type_id,p_order_field,p_accept_action)
#############################################
FUNCTION delivery_type_popup(p_delivery_type_id,p_order_field,p_accept_action)
  DEFINE 
    p_delivery_type_id      LIKE delivery_type.delivery_type_id,
    l_delivery_type_arr     DYNAMIC ARRAY OF RECORD LIKE delivery_type.*,
    i                       INTEGER,
    p_order_field,p_order_field2            VARCHAR(128), 
    local_debug             SMALLINT,
    p_accept_action         SMALLINT,
    err_msg                 VARCHAR(240),
    l_ord_dir               SMALLINT

  LET local_debug = 0  ---0=off   1=on
  LET l_ord_dir = 1  --ASC
  IF NOT p_order_field THEN
    LET p_order_field = "delivery_type_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF

    CALL fgl_window_open("w_delivery_type_scroll", 2, 8, get_form_path("f_delivery_type_scroll_l2"),FALSE) 
    CALL populate_delivery_type_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/transportation/delivery_type.png")
	CALL ui.Interface.setText("Delivery Type")

  WHILE TRUE
    CALL delivery_type_popup_data_source(p_order_field,l_ord_dir)

    LET i = 1

    FOREACH c_del_type INTO l_delivery_type_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH

    LET i = i - 1

	IF i > 0 THEN
		CALL l_delivery_type_arr.resize(i)  --correct the last element of the dynamic array
	END IF 
	
		
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_delivery_type_arr TO sa_delivery_type_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("DeliveryTypeList",0)


      ON KEY(INTERRUPT)  --on cancel, we return the calling argument
        EXIT WHILE

      ON KEY(ACCEPT)  --ON OK events can be customized

        LET i = arr_curr()
        LET i = l_delivery_type_arr[i].delivery_type_id

        CASE p_accept_action

          WHEN 0
            EXIT WHILE
        
          WHEN 1  --view
            CALL delivery_type_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL delivery_type_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL delivery_type_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL delivery_type_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","activity_type_popup()\nActivity Type Print is not implemented","info")

            #CURRENT WINDOW IS SCREEN
            #CALL activity_print(i)
            #CURRENT WINDOW IS w_act_type_popup             

            OTHERWISE
              LET err_msg = "delivery_type_popup(p_delivery_type_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("delivery_type_popup() - 4GL Source Error",err_msg, "error") 
          END CASE


      ON KEY (F4) -- add
        CALL delivery_type_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_delivery_type_arr[i].delivery_type_id
        CALL delivery_type_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_delivery_type_arr[i].delivery_type_id
        CALL delivery_type_delete(i)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "delivery_type_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "delivery_type_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "delivery_rate"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_delivery_type_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_delivery_type_id
  ELSE 
    LET i = arr_curr()
    RETURN l_delivery_type_arr[i].delivery_type_id
  END IF

END FUNCTION


#############################################
# FUNCTION delivery_type_create()
#
# Create delivery_type record
#
# RETURN r_delivery_type_id 
#############################################
FUNCTION delivery_type_create()
  DEFINE 
    l_delivery_type      RECORD LIKE delivery_type.*,
    r_delivery_type_id   LIKE delivery_type.delivery_type_id

  LET r_delivery_type_id = NULL
  LET l_delivery_type.delivery_type_id = 0

  CALL delivery_type_input(l_delivery_type.*)
    RETURNING l_delivery_type.*

  IF NOT int_flag THEN 
    INSERT INTO delivery_type VALUES (l_delivery_type.*)
    LET r_delivery_type_id = sqlca.sqlerrd[2]
  ELSE 
    LET int_flag = FALSE
  END IF

  RETURN r_delivery_type_id
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate


#############################################
# FUNCTION delivery_type_delete(p_delivery_type_id)
#############################################
FUNCTION delivery_type_delete(p_delivery_type_id)
  DEFINE p_delivery_type_id INTEGER

  #Are you sure you want to delete this value?
  IF yes_no(get_str(55),get_str(56)) THEN
    DELETE FROM delivery_type 
      WHERE delivery_type.delivery_type_id = p_delivery_type_id
  END IF
END FUNCTION


#############################################
# FUNCTION delivery_type_edit(p_delivery_type_id)
#############################################
FUNCTION delivery_type_edit(p_delivery_type_id)
  DEFINE 
    p_delivery_type_id INTEGER,
    l_delivery_type    RECORD LIKE delivery_type.*

  SELECT delivery_type.* 
    INTO l_delivery_type.*
    FROM delivery_type
    WHERE delivery_type.delivery_type_id = p_delivery_type_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL delivery_type_input(l_delivery_type.*) 
    RETURNING l_delivery_type.*

  IF NOT int_flag THEN
    UPDATE delivery_type
       SET delivery_type.delivery_type_name = l_delivery_type.delivery_type_name,
           delivery_type.delivery_rate = l_delivery_type.delivery_rate

    WHERE delivery_type.delivery_type_id = l_delivery_type.delivery_type_id
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


#############################################
# FUNCTION delivery_type_input(p_delivery_type)
#
# Input delivery type data from form (edit/create)
#
# RETURN
#############################################
FUNCTION delivery_type_input(p_delivery_type)
  DEFINE 
    p_delivery_type RECORD LIKE delivery_type.*,
    l_delivery_type RECORD LIKE delivery_type.*

  LET l_delivery_type.* = p_delivery_type.*

    CALL fgl_window_open("w_delivery_type", 3, 3, get_form_path("f_delivery_type_l2"),FALSE) 
    CALL populate_delivery_type_form_labels_g()

  LET int_flag = FALSE

  INPUT BY NAME l_delivery_type.delivery_type_name,
                l_delivery_type.delivery_rate 
    WITHOUT DEFAULTS
    HELP 3


    AFTER INPUT
      IF NOT int_flag THEN
        IF l_delivery_type.delivery_type_name IS NULL THEN
          CALL fgl_winmessage(get_str(48),get_str(1681),"error")
          CONTINUE INPUT
        END IF
        IF l_delivery_type.delivery_rate IS NULL THEN
          #"The delivery type 'Rate' field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(1680),"error")
          CONTINUE INPUT
        END IF
      ELSE
        #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_str(60),get_str(61)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF 
      END IF
  END INPUT

  CALL fgl_window_close("w_delivery_type")

  #IF int_flag THEN
  #  LET int_flag = FALSE
  #END IF

  RETURN l_delivery_type.*
END FUNCTION


#############################################
# FUNCTION delivery_type_view(p_delivery_type_id)
#
# VIEW delivery type data onform with ID
#
# RETURN NONE
#############################################
FUNCTION delivery_type_view(p_delivery_type_id)
  DEFINE 
    p_delivery_type_id LIKE delivery_type.delivery_type_id,
    l_delivery_type_rec RECORD LIKE delivery_type.*

  CALL get_delivery_type_rec(p_delivery_type_id) RETURNING l_delivery_type_rec.*
  CALL delivery_type_view_by_rec(l_delivery_type_rec.*)

END FUNCTION

#############################################
# FUNCTION delivery_type_view_by_rec(p_delivery_type)
#
# VIEW delivery type data onform
#
# RETURN
#############################################
FUNCTION delivery_type_view_by_rec(p_delivery_type_rec)
  DEFINE 
    p_delivery_type_rec RECORD LIKE delivery_type.*,
    inp_char            CHAR

    CALL fgl_window_open("w_delivery_type", 3, 3, get_form_path("f_delivery_type_l2"),FALSE) 
    CALL populate_delivery_type_form_labels_g()

    DISPLAY get_str(811) TO bt_ok  --Done
    DISPLAY "!" TO bt_ok

    DISPLAY "*" TO bt_cancel


  LET int_flag = FALSE

  DISPLAY BY NAME p_delivery_type_rec.*

  WHILE TRUE
    PROMPT "" FOR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
     ENd PROMPT

  END WHILE

  CALL fgl_window_close("w_delivery_type")

END FUNCTION

