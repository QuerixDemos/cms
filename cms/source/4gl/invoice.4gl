##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################################################
# Functions to manage Invoices
##################################################################################################################################################
#
# FUNCTION:                                                            DESCRIPTON:                                       RETURN:
# invoice_main()                                                       Main Invoice Menu                                 NONE
# get_account_id_from_invoice_id(p_invoice_id)                         Get acoount ID from Invoice                       rv_account_id
# get_invoice_rec(p_invoice_id)                                        Get the invoice record (by id)                    l_invoice.*
# load_invoice_temp_detail()                                           Load invoice temporary details                    i   (i=integer)
# invoice_delete(p_invoice_id)                                         Delete an invoice                                 NONE
# invoice_create()                                                     Create a new invoice                              l_invoice.invoice_id
# invoice_input(p_invoice)                                             Invoice Input (invoice create sub-module)         p_invoice.*
# create_invoice_temp(p_invoice_id)                                    Create temporary invoice record                   NONE
# copy_invoice_temp(p_invoice_id,p_process_items)                      Creates a copy record of an invoice               NONE
# update_status_name(p_invoice, p_status_name)                         Update status                                     NONE
# update_processible_invoice(p_invoice)                                Update status in "Wait processing"                NONE
# mass_update_processible_invoices()                                   Update processible invoices                       p_invoice.*, input_status
# invoice_input_header(p_invoice)                                      Input the invoice header details                  p_invoice.*, input_status
# invoice_input_detail(p_invoice,w_name)                               Input invoice detail                              input_status  (integer)
# update_line_total()                                                  Update the invoice line totals                    NONE
# update_totals(cnt)                                                   Updates all line totals (individual)              NONE
# invoice_update_totals(p_invoice_id)                                  Calculates invoice total and stores it to the DB  NONE
# invoice_edit(p_invoice_id)                                           Edit an invoice                                   NONE
# invoice_query(p_status)                                              Calls popup and views returned invoice            NONE
# invoice_popup_data_source(p_order_field,p_ord_dir,p_status)          Show invoice list & choose view/edit/delete       NULL or rv_invoice_id
# invoice_popup(p_invoice_id,p_order_field,p_accept_action,p_status)   Show invoice list & choose view/edit/delete       NULL or rv_invoice_id  (integer)
# select_invoices()                                                    called for print                                  NONE
# show_invoice_header(p_invoice)                                       displays the invoice header details to form       NONE
# show_invoice_detail(p_invoice_id)                                    Display invoice details                           NONE
# invoice_show_temp_detail()                                           Show temporary invoice detail                     NONE
# show_invoice(p_invoice_id)                                           Display invoice fields by name                    NONE
# invoice_view(p_invoice_id)                                           View Invoice Record                               NONE
# past_due_val()                                                       Return day interval                               day_interval 
# get_invoice_status(p_status_id,p_invoice_date)                       Get invoice status                                status or err_msg         
# is_past_due(p_status_id,p_invoice_date)                              Check if invoice is past due                      TRUE or FALSE
# display_credit_note(p_balance)                                       Display credit note                               NONE
# verify_inv_data(p_invoice)                                           Verify input data of invoice                      TRUE or FALSE
# verify_inv_lines(p_invoice_lines)                                    Verify input stock items of invoice               TRUE or FALSE
# invoice_status_combo_list(cb_field_name, p_invoice_id)               Populate invoice_status combo list from database  NONE
# get_inv_status_id(inv_status_name)                                   Get invoice status id from name                   l_inv_status_id
# get_inv_status_name(inv_status_id)                                   Get invoice status name from id                   l_inv_status_name
# grid_header_f_invoice_scroll_g()                                     Populate grid headers on form for gui mode        NONE
# populate_invoice_form_labels_t()                                     Populate invoice form for text mode               NONE
# populate_invoice_form_labels_g()                                     Populate invoice form for gui mode                NONE
# populate_invoice_form_view_labels_t()                                Populate invoice view form for text mode          NONE
# populate_invoice_form_view_labels_g()                                Populate invoice view form for gui mode           NONE
# populate_invoice_form_input_labels_t()                               Populate invoice input form for text mode         NONE
# populate_invoice_form_input_labels_g()                               Populate invoice input form for gui mode          NONE
# populate_invoice_form_query_labels_t()                               Populate invoice query form for text mode         NONE
# populate_invoice_form_query_labels_g()                               Populate invoice query form for gui mode          NONE
# populate_invoice_scroll_form_labels_t()                              Populate invoice scroll form for text mode        NONE
# populate_invoice_scroll_form_labels_g()                              Populate invoice scroll form for gui mode         NONE
#
##################################################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# Module Definitions
######################################################
  DEFINE md_inv_lines          DYNAMIC ARRAY  OF t_inv_line --[20]
  DEFINE inv_total            LIKE invoice.inv_total
  DEFINE tax_total            LIKE invoice.tax_total
  DEFINE net_total            LIKE invoice.net_total
  DEFINE xchg_rate            LIKE currency.xchg_rate
  DEFINE for_total            LIKE invoice.for_total
  DEFINE l_inv_stat           LIKE invoice.status
  DEFINE l_inv_id             LIKE invoice.invoice_id
  DEFINE l_status             DYNAMIC ARRAY OF VARCHAR(20)
  DEFINE past_due_msg         VARCHAR(20)
  DEFINE wait_payment_msg     VARCHAR(100)
  DEFINE wait_processing_msg  VARCHAR(100)
  DEFINE help_msg             VARCHAR(100)
  DEFINE l_verified_inv_lines SMALLINT
########## END of module variables #################


############################################################################################################################
# Menu functions
############################################################################################################################

###################################################
# FUNCTION invoice_main()
#
# invoice main menu
#
# RETURN NONE
###################################################
FUNCTION invoice_main()
  DEFINE
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings
  MENU "invoicing"
    BEFORE MENU
      CALL set_help_id(601)
      CALL publish_toolbar("Invoice",0)

	CALL ui.Interface.setImage("qx://application/icon16/money/invoice/invoice.png")
	CALL ui.Interface.setText("Invoice")



        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(1420)
        LET menu_str_arr2[1] = get_str(1421)
        LET menu_str_arr1[2] = get_str(1422)
        LET menu_str_arr2[2] = get_str(1423)
        LET menu_str_arr1[3] = get_str(1424)
        LET menu_str_arr2[3] = get_str(1425)
        LET menu_str_arr1[4] = get_str(1426)
        LET menu_str_arr2[4] = get_str(1427)
        LET menu_str_arr1[5] = get_str(1428)
        LET menu_str_arr2[5] = get_str(1429)
        LET menu_str_arr1[6] = get_str(1430)
        LET menu_str_arr2[6] = get_str(1431)
        LET menu_str_arr1[7] = get_str(1432)
        LET menu_str_arr2[7] = get_str(1433)
        LET menu_str_arr1[8] = get_str(1434)
        LET menu_str_arr2[8] = get_str(1435)
        LET menu_str_arr1[9] = get_str(1436)
        LET menu_str_arr2[9] = get_str(1437)
        LET menu_str_arr1[10] = get_str(2463)
        LET menu_str_arr2[10] = get_str(2464)
        LET menu_str_arr1[11] = get_str(2465)
        LET menu_str_arr2[11] = get_str(2466)
        LET menu_str_arr1[12] = get_str(2467)
        LET menu_str_arr2[12] = get_str(2468)
        LET menu_str_arr1[13] = get_str(2469)
        LET menu_str_arr2[13] = get_str(2470)
        LET menu_str_arr1[14] = get_str(2471)
        LET menu_str_arr2[14] = get_str(2472)
        LET menu_str_arr1[15] = get_str(2473)
        LET menu_str_arr2[15] = get_str(2474)
        LET menu_str_arr1[16] = get_str(2477)
        LET menu_str_arr2[16] = get_str(2478)
        LET l_status[1] = get_str(2463)
	LET l_status[2] = get_str(2465)
	LET l_status[3] = get_str(2467)
	LET l_status[4] = get_str(2469)
	LET l_status[5] = get_str(2471)
	LET past_due_msg = get_str(2473)
        LET help_msg = get_str(1539);
        LET wait_payment_msg = get_str(2475);
        LET wait_processing_msg = get_str(2476);


    COMMAND KEY (F601,"N") menu_str_arr1[1] menu_str_arr2[1] HELP 501  -- "New" "Create a new invoice" help 501
      CALL invoice_create()

    COMMAND KEY (F602,"F") menu_str_arr1[2] menu_str_arr2[2] HELP 503  -- "Find" "Find - Search for an invoice" help 503
      IF fgl_fglgui() THEN --gui client
        CALL invoice_popup(NULL,"invoice_id",1,NULL)
      ELSE
        MENU "find_invoice"
          COMMAND KEY ("A") menu_str_arr1[16] menu_str_arr2[16] HELP 503
            CALL invoice_popup(NULL,"invoice_id",1,NULL)
          COMMAND menu_str_arr1[10] menu_str_arr2[10] HELP 503 -- Not payed
            CALL invoice_popup(NULL,"invoice_id",1,1)
          COMMAND menu_str_arr1[11] menu_str_arr2[11] HELP 503 -- Canceled
            CALL invoice_popup(NULL,"invoice_id",1,2)
          COMMAND menu_str_arr1[15] menu_str_arr2[15] HELP 503 -- Past-Due
            CALL invoice_popup(NULL,"invoice_id",1,-1)
          COMMAND menu_str_arr1[12] menu_str_arr2[12] HELP 503 -- Wait for supplies
            CALL invoice_popup(NULL,"invoice_id",1,3)
          COMMAND menu_str_arr1[13] menu_str_arr2[13] HELP 503 -- In process
            CALL invoice_popup(NULL,"invoice_id",1,4)
          COMMAND menu_str_arr1[14] menu_str_arr2[14] HELP 503 -- Done
            CALL invoice_popup(NULL,"invoice_id",1,5)
         COMMAND KEY (F12,"Q")  menu_str_arr1[5] menu_str_arr2[5] HELP 506  -- "Main Menu" "Close window and return to previous menu" help 506
            EXIT MENU
    			ON ACTION ("actExit") 
      			EXIT MENU
      			            
        END MENU
        
      END IF
    COMMAND KEY (F603,"A") menu_str_arr1[3] menu_str_arr2[3] HELP 504  -- "Accounts" "Manage accounts" help 504
       CALL account_popup(NULL,"account_id",2)

    COMMAND KEY (F604,"S") menu_str_arr1[4] menu_str_arr2[4] HELP 505  -- "Stock" "Manage stock/warehouse" help 505
       CALL stock_item_popup(NULL,"stock_id",2)

    COMMAND KEY (F12,"Q")  menu_str_arr1[5] menu_str_arr2[5] HELP 506  -- "Main Menu" "Close window and return to previous menu" help 506
      EXIT MENU

    ON ACTION ("actExit") 
      EXIT MENU      
  END MENU

  #clean up toolbar
  CALL publish_toolbar("Invoice",1)

END FUNCTION


##################################################
# FUNCTION load_invoice_temp_detail()
#
#
#
# RETURN i INTEGER
##################################################
FUNCTION load_invoice_temp_detail()
  DEFINE 
    i        INTEGER,
    tax_rate LIKE tax_rates.tax_rate

  DECLARE c_inv_detail CURSOR FOR
    SELECT stock_item.stock_id,
           stock_item.item_cost,
           stock_item.item_desc,
           invoice_line_temp.quantity,
           tax_rates.tax_rate
      FROM invoice_line_temp,
           tax_rates,
           stock_item
      WHERE stock_item.rate_id = tax_rates.rate_id
        AND stock_item.stock_id = invoice_line_temp.stock_id

  LET i = 1

  FOREACH c_inv_detail 
    INTO md_inv_lines[i].stock_id,
         md_inv_lines[i].item_cost,
         md_inv_lines[i].item_desc,
         md_inv_lines[i].quantity,
         tax_rate
    
    LET md_inv_lines[i].line_net_total = (md_inv_lines[i].quantity * md_inv_lines[i].item_cost)
    LET md_inv_lines[i].line_tax_total = (md_inv_lines[i].line_net_total * tax_rate / 100)
    LET md_inv_lines[i].line_total = md_inv_lines[i].line_net_total + md_inv_lines[i].line_tax_total
    LET i = i + 1
  END FOREACH

  LET i = i - 1
		IF i > 0 THEN
			CALL md_inv_lines.resize(i)  --correct the last element of the dynamic array
		END IF 
  RETURN i
END FUNCTION




############################################################################################################################
# Edit,Create,Delete,Input functions
############################################################################################################################


###################################################
# FUNCTION invoice_delete(p_invoice_id)
###################################################
FUNCTION invoice_delete(p_invoice_id)
  DEFINE p_invoice_id LIKE invoice.invoice_id

  BEGIN WORK
    DELETE FROM invoice 
      WHERE invoice.invoice_id = p_invoice_id

    DELETE FROM invoice_line
      WHERE invoice_line.invoice_id = p_invoice_id
  COMMIT WORK
END FUNCTION


 

###################################################
# FUNCTION invoice_create()
#
# RETURN RETURN l_invoice.invoice_id or NULL (cancel or no invoice account return NULL)
#
# Create a new invoice - requires to select an invoice account
###################################################
FUNCTION invoice_create()
  DEFINE 
    local_debug BOOLEAN,
    l_invoice   RECORD LIKE invoice.*,
    l_account   RECORD LIKE account.*,
    i, cnt      INTEGER

  LET local_debug = FALSE --0=off 1=on
  #CALL fgl_message_box("Select an account for this invoice")
  IF local_debug THEN
    DISPLAY "invoice_create() - In invoice_create()"
  END IF
  CALL account_popup(NULL,"account_id",0)
    RETURNING l_account.account_id

  IF NOT exist(l_account.account_id) THEN
    RETURN NULL
  END IF

  CALL get_account_rec(l_account.account_id)
    RETURNING l_account.*

  LET l_invoice.invoice_id = 0
  #LET l_invoice.pay_method_name = get_pay_method'VISA'
  LET l_invoice.del_method = 1 --'Normal Delivery'
  LET l_invoice.status = 1
  LET l_invoice.inv_total = 0
  LET l_invoice.tax_total = 0
  LET l_invoice.net_total = 0
  LET l_invoice.operator_id = g_operator.operator_id
  LET l_invoice.account_id = l_account.account_id
  LET l_invoice.invoice_date = TODAY
  #LET l_invoice.status=1
  LET l_invoice.invoice_po = "PO-23-56-" --CURRENT HOUR TO SECOND USING "hhmmss"
  LET l_invoice.invoice_po[3] = 1
  LET l_invoice.invoice_po[6] = 7

  BEGIN WORK
    CALL create_invoice_temp(0)

    CALL invoice_input(l_invoice.*)
      RETURNING l_invoice.*


  IF local_debug THEN
    DISPLAY "invoice_create() -0- l_invoice.invoice_po=", l_invoice.invoice_po
    DISPLAY "invoice_create() -0- l_invoice.status=", l_invoice.status
    DISPLAY "invoice_create() -0- l_invoice.pay_method_desc=", l_invoice.pay_method_desc
    DISPLAY "invoice_create() -0- l_invoice.pay_method_name=", l_invoice.pay_method_name
    DISPLAY "invoice_create() -0- l_invoice.del_address1=", l_invoice.del_address1
    DISPLAY "invoice_create() -0- l_invoice.del_address2=", l_invoice.del_address2
    DISPLAY "invoice_create() -0- l_invoice.del_address3=", l_invoice.del_address3
    DISPLAY "invoice_create() -0- l_invoice.del_address_dif=", l_invoice.del_address_dif
    DISPLAY "invoice_create() -0- l_invoice.del_method=", l_invoice.del_method
    #DISPLAY "invoice_input_header() -2- l_invoice.xchg_rate=", l_invoice.xchg_rate

    DISPLAY "invoice_create() -0- l_invoice.pay_method_id=", l_invoice.pay_method_id
  END IF

    INSERT INTO invoice VALUES (l_invoice.*)

    LET l_invoice.invoice_id = sqlca.sqlerrd[2]

    CALL copy_invoice_temp(l_invoice.invoice_id,0)

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN NULL
    END IF
    CALL invoice_update_totals(l_invoice.invoice_id)

    CALL print_invoice(l_invoice.invoice_id)

  COMMIT WORK

  RETURN l_invoice.invoice_id

END FUNCTION



#############################################################
# FUNCTION invoice_input(p_invoice)
#
# Enter invoice details into form fields
#
# RETURN p_invoice.*
#############################################################
FUNCTION invoice_input(p_invoice)
  DEFINE 
    p_invoice    RECORD LIKE invoice.*,
    input_status SMALLINT,
    w_name       VARCHAR(30),
    l_verify     SMALLINT

  LET l_inv_stat = p_invoice.status
  LET l_inv_id = p_invoice.invoice_id
  LET w_name = "w_inv_input"

    IF NOT fgl_window_open(w_name, 1, 1, get_form_path("f_invoice_det_l2"),FALSE) THEN
      CALL fgl_winmessage("Error","invoice_input()\nCan not open window " || w_name,"error")
    END IF

    CALL populate_invoice_form_input_labels_g()
    CALL invoice_status_combo_list("status_name", p_invoice.invoice_id)

  CALL show_invoice_header(p_invoice.*)
  CALL invoice_show_temp_detail()
  
  IF get_account_credit_balance(p_invoice.account_id) < 0 THEN
    LET tmp_str = get_str(1460) CLIPPED, " ", get_account_credit_balance(p_invoice.account_id)
    DISPLAY tmp_str TO lbInfo1 ATTRIBUTE(RED,BOLD)
  ELSE
    LET tmp_str = get_str(1461) CLIPPED, " ", get_account_credit_balance(p_invoice.account_id)
    DISPLAY tmp_str TO lbInfo1 ATTRIBUTE(GREEN,BOLD)
  END IF

  LET l_verify = FALSE
  
  WHILE NOT l_verify

    LET input_status = 1

    WHILE (NOT int_flag AND input_status > 0)
      CALL invoice_input_header(p_invoice.*)
        RETURNING p_invoice.*, input_status
				#DISPLAY "in FUNCTION invoice_input(p_invoice) p_invoice.pay_method_name=", p_invoice.pay_method_name
      IF NOT int_flag AND input_status > 0 THEN
        CALL invoice_input_detail(p_invoice.*,w_name)
          RETURNING input_status
      END IF
    END WHILE

    LET l_verify = verify_inv_data(p_invoice.*)

  END WHILE

  CALL fgl_window_close(w_name)
  RETURN p_invoice.*

END FUNCTION



#############################################################
# FUNCTION create_invoice_temp(p_invoice_id)
#
# Create temporary invoice record
#
# RETURN NONE
#############################################################
FUNCTION create_invoice_temp(p_invoice_id)
  DEFINE p_invoice_id LIKE invoice.invoice_id

  SELECT invoice_line.*
    FROM invoice_line
    WHERE invoice_line.invoice_id = p_invoice_id
    INTO TEMP invoice_line_temp

  DELETE FROM invoice_line
    WHERE invoice_line.invoice_id = p_invoice_id
END FUNCTION



#############################################################
# FUNCTION copy_invoice_temp(p_invoice_id,p_process_items)
#
# Creates a copy record of an invoice
#
# RETURN NONE
#############################################################
FUNCTION copy_invoice_temp(p_invoice_id,p_process_items)
  DEFINE 
    p_invoice_id   LIKE invoice.invoice_id,
    p_process_items SMALLINT,
    l_invoice_line RECORD LIKE invoice_line.*


  DECLARE c_copy_temp CURSOR FOR
    SELECT * FROM invoice_line_temp

  FOREACH c_copy_temp INTO l_invoice_line.*
    LET l_invoice_line.invoice_id = p_invoice_id
    INSERT INTO invoice_line VALUES (l_invoice_line.*)
    IF p_process_items THEN
      UPDATE stock_item SET quantity = quantity - l_invoice_line.quantity WHERE stock_id = l_invoice_line.stock_id
    END IF
  END FOREACH

  DROP TABLE invoice_line_temp
END FUNCTION



#############################################################
# FUNCTION update_status_name(p_invoice, p_status_name)
#
# Update status
#
# RETURN NONE
#############################################################
FUNCTION update_status_name(p_invoice, p_status_name)

  DEFINE p_invoice      RECORD LIKE invoice.*,
         p_status_name  LIKE status_invoice.status_name

  IF p_invoice.invoice_id = 0 THEN
    DISPLAY help_msg TO lbInfo1
  ELSE
    CASE p_invoice.status 
      WHEN 1
        DISPLAY help_msg || " " || wait_payment_msg TO lbInfo1
      WHEN 4
        DISPLAY help_msg || " " || wait_processing_msg TO lbInfo1
      OTHERWISE
        DISPLAY help_msg TO lbInfo1
    END CASE
  END IF
 
  IF p_status_name IS NULL THEN
    DISPLAY get_invoice_status(p_invoice.status,p_invoice.invoice_date) TO status_name
  ELSE
    DISPLAY p_status_name TO status_name
  END IF

END FUNCTION



#############################################################
# FUNCTION update_processible_invoice(p_invoice)
#
# Update status in "Wait processing"
#
# RETURN NONE
#############################################################
FUNCTION update_processible_invoice(p_invoice)
  DEFINE
    p_invoice RECORD LIKE invoice.*,
    l_reserved_quantity LIKE invoice_line.quantity,
    l_quantity LIKE invoice_line.quantity,
    l_stock_quantity LIKE stock_item.quantity,
    l_stock_id LIKE invoice_line.stock_id,
    l_invoice_line RECORD LIKE invoice_line.*,
    l_past_due_date_value SMALLINT

  LET l_past_due_date_value = past_due_val()

  IF p_invoice.status = 3 THEN
    DECLARE c_invoice_list CURSOR FOR
      SELECT stock_id,SUM(quantity) FROM invoice_line WHERE invoice_line.invoice_id = p_invoice.invoice_id GROUP BY stock_id
    FOREACH c_invoice_list INTO l_stock_id,l_quantity
      SELECT quantity INTO l_stock_quantity FROM stock_item WHERE stock_id = l_stock_id
      SELECT SUM(invoice_line.quantity) INTO l_reserved_quantity FROM invoice_line,invoice 
        WHERE invoice.invoice_id = invoice_line.invoice_id 
          AND invoice_line.invoice_id < p_invoice.invoice_id 
          AND stock_id = l_stock_id 
          AND (invoice.status IN (3,4) OR (invoice.status = 1 AND TODAY <= invoice.invoice_date + l_past_due_date_value))
      IF l_reserved_quantity IS NULL THEN 
        LET l_reserved_quantity = 0
      END IF
      IF l_quantity > l_stock_quantity - l_reserved_quantity THEN
        RETURN
      END IF
    END FOREACH
    UPDATE invoice SET status = 4 WHERE invoice_id = p_invoice.invoice_id
  END IF
END FUNCTION



#############################################################
# FUNCTION mass_update_processible_invoices()
#
# Update processible invoices
#
# RETURN NONE
#############################################################
FUNCTION mass_update_processible_invoices()
  DEFINE p_invoice RECORD LIKE invoice.*
  DECLARE c_invoice CURSOR FOR
    SELECT * FROM invoice WHERE invoice.status = 3
  FOREACH c_invoice INTO p_invoice.*
    CALL update_processible_invoice(p_invoice.*)
  END FOREACH
END FUNCTION



#############################################################
# FUNCTION invoice_input_header(p_invoice)
#
# Input the invoice header details
#
# RETURN NONE
#############################################################
FUNCTION invoice_input_header(p_invoice)
  DEFINE 
    local_debug SMALLINT,
    a smallint,
    b smallint,
    p_invoice RECORD LIKE invoice.*,
    l_invoice_rec OF t_invoice_rec,
    input_status SMALLINT

  LET input_status = 0
  LET local_debug = 0  --0=off 1=on

  CALL show_invoice_header(p_invoice.*)

  LET l_invoice_rec.del_address1 = p_invoice.del_address1
  LET l_invoice_rec.del_address2 = p_invoice.del_address2
  LET l_invoice_rec.del_address3 = p_invoice.del_address3
  LET l_invoice_rec.del_address_dif = p_invoice.del_address_dif
  LET l_invoice_rec.del_method = p_invoice.del_method
  LET l_invoice_rec.pay_method_name = p_invoice.pay_method_name
  LET l_invoice_rec.invoice_po = p_invoice.invoice_po
  LET l_invoice_rec.xchg_rate = get_xchg_rate(p_invoice.currency_id)
  LET l_invoice_rec.currency_name = get_currency_name(p_invoice.currency_id)

  LET l_invoice_rec.pay_method_name = get_pay_method_name(p_invoice.pay_method_id)
  #LET l_invoice_rec.pay_method_id =   get_pay_method_id(l_invoice_rec.pay_method_name)
  LET l_invoice_rec.pay_method_desc = p_invoice.pay_method_desc
  #LET l_invoice_rec.status_name = get_inv_status_name(p_invoice.status)
  LET l_invoice_rec.status_name = get_invoice_status(p_invoice.status,p_invoice.invoice_date);
  LET int_flag = FALSE


  INPUT BY NAME l_invoice_rec.* WITHOUT DEFAULTS
    BEFORE INPUT
      CALL set_help_id(601)
      CALL publish_toolbar("InvoiceEditHeader",0)
      CALL update_status_name(p_invoice.*, NULL)
    ON KEY (F4)
      IF p_invoice.invoice_id > 0 THEN
        CASE p_invoice.status
          WHEN 1
            IF NOT is_past_due(p_invoice.status,p_invoice.invoice_date) THEN
              LET l_invoice_rec.status_name = get_inv_status_name(3)
            END IF
          WHEN 4
            LET l_invoice_rec.status_name = get_inv_status_name(5)
        END CASE
      END IF
      CALL update_status_name(p_invoice.*, l_invoice_rec.status_name)
    ON KEY (F9)
      IF p_invoice.invoice_id > 0 THEN
        CASE p_invoice.status 
          WHEN 1
            IF NOT is_past_due(p_invoice.status,p_invoice.invoice_date) THEN
              LET l_invoice_rec.status_name = get_inv_status_name(2)
            END IF
          WHEN 4
            LET l_invoice_rec.status_name = get_inv_status_name(2)
        END CASE
      END IF
      CALL update_status_name(p_invoice.*, l_invoice_rec.status_name)
    ON KEY (F10)

      IF INFIELD (currency_name) THEN
        LET p_invoice.currency_id = currency_popup(p_invoice.currency_id,NULL,0)
        LET l_invoice_rec.currency_name = get_currency_name(p_invoice.currency_id)
        LET l_invoice_rec.xchg_rate = get_xchg_rate(p_invoice.currency_id)
        CALL currency_combo_list("currency_name")
        DISPLAY BY NAME l_invoice_rec.currency_name
        DISPLAY BY NAME l_invoice_rec.xchg_rate
      END IF

      IF INFIELD (pay_method_name) THEN
        CALL pay_method_popup(get_pay_method_id(p_invoice.pay_method_name),NULL,0)
          RETURNING p_invoice.pay_method_name
        CALL payment_method_combo_list("pay_method_name")
        CALL get_pay_method_rec(p_invoice.pay_method_name)
          RETURNING p_invoice.pay_method_name,
                    l_invoice_rec.pay_method_name, 
                    l_invoice_rec.pay_method_desc
        DISPLAY BY NAME l_invoice_rec.pay_method_name, l_invoice_rec.pay_method_desc
      END IF

    AFTER FIELD pay_method_name
        LET p_invoice.pay_method_id = get_pay_method_id(l_invoice_rec.pay_method_name)
        CALL get_pay_method_rec(p_invoice.pay_method_id)
          RETURNING p_invoice.pay_method_id,
                    l_invoice_rec.pay_method_name, 
                    l_invoice_rec.pay_method_desc
        DISPLAY BY NAME l_invoice_rec.pay_method_name, l_invoice_rec.pay_method_desc

    IF local_debug = 1 THEN
      DISPLAY "invoice_input_header() p_invoice.pay_method_id=", p_invoice.pay_method_id
      DISPLAY "invoice_input_header() l_invoice_rec.pay_method_name=", l_invoice_rec.pay_method_name
      DISPLAY "invoice_input_header() l_invoice_rec.pay_method_desc=", l_invoice_rec.pay_method_desc
    END IF

    AFTER FIELD currency_name
        LET p_invoice.currency_id = get_currency_id(l_invoice_rec.currency_name)
        CALL get_currency_rec(p_invoice.currency_id)
          RETURNING p_invoice.currency_id,
                    l_invoice_rec.currency_name, 
                    l_invoice_rec.xchg_rate
        DISPLAY BY NAME l_invoice_rec.currency_name, l_invoice_rec.xchg_rate
        CALL update_totals(arr_count(), get_currency_id(l_invoice_rec.currency_name))

    ON KEY (F11)
      LET input_status = 1
      EXIT INPUT

    AFTER INPUT
      IF int_flag THEN
        #Do you really want to abort the new/modified record entry ?
        IF NOT yes_no(get_str(1467),get_str(1468)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF

  END INPUT   

	#			DISPLAY "in FUNCTION invoice_input_header(p_invoice) p_invoice.pay_method_name=", p_invoice.pay_method_name

  #Possible toolbar cleeanup
  CALL publish_toolbar("InvoiceEditHeader",1)

  LET p_invoice.invoice_po = l_invoice_rec.invoice_po
  LET p_invoice.pay_method_desc = l_invoice_rec.pay_method_desc
  LET p_invoice.pay_method_name = l_invoice_rec.pay_method_name
  LET p_invoice.del_address1 = l_invoice_rec.del_address1
  LET p_invoice.del_address2 = l_invoice_rec.del_address2
  LET p_invoice.del_address3 = l_invoice_rec.del_address3
  LET p_invoice.del_address_dif = l_invoice_rec.del_address_dif
  LET p_invoice.del_method = l_invoice_rec.del_method
  LET p_invoice.currency_id = get_currency_id(l_invoice_rec.currency_name)
  LET p_invoice.status = get_inv_status_id(l_invoice_rec.status_name)


  IF local_debug = 1 THEN
    DISPLAY "invoice_input_header() -2- p_invoice.invoice_po=", p_invoice.invoice_po
    DISPLAY "invoice_input_header() -2- p_invoice.pay_method_desc=", p_invoice.pay_method_desc
    DISPLAY "invoice_input_header() -2- p_invoice.pay_method_name=", p_invoice.pay_method_name
    DISPLAY "invoice_input_header() -2- p_invoice.del_address1=", p_invoice.del_address1
    DISPLAY "invoice_input_header() -2- p_invoice.del_address2=", p_invoice.del_address2
    DISPLAY "invoice_input_header() -2- p_invoice.del_address3=", p_invoice.del_address3
    DISPLAY "invoice_input_header() -2- p_invoice.del_address_dif=", p_invoice.del_address_dif
    DISPLAY "invoice_input_header() -2- p_invoice.del_method=", p_invoice.del_method
    DISPLAY "invoice_input_header() -2- xchg_rate=", xchg_rate
    DISPLAY "invoice_input_header() -2- p_invoice.pay_method_id=", p_invoice.pay_method_id
    DISPLAY "invoice_input_header() -2- p_invoice.currency_id=", p_invoice.currency_id
    DISPLAY "invoice_input_header() -2- pl_invoice_rec.currency_name=", l_invoice_rec.currency_name
  END IF


  RETURN p_invoice.*, input_status

END FUNCTION



###################################################
# FUNCTION verify_inv_data(p_invoice)
#
# Verify input data of invoice
#
# RETURN TRUE or FALSE
###################################################
FUNCTION verify_inv_data(p_invoice)

  DEFINE p_invoice      RECORD LIKE invoice.*

  IF NOT int_flag THEN

    IF p_invoice.account_id IS NULL THEN
      CALL fgl_winmessage(get_str(48),get_str(1462),"error")
      RETURN FALSE
    END IF

    IF  p_invoice.del_address_dif = 1 AND p_invoice.del_address1 IS NULL THEN
      LET tmp_str = get_Str(1463) CLIPPED, "\n", get_Str(1464)
      CALL fgl_winmessage(get_str(48),tmp_str,"error")
      RETURN FALSE
    END IF

    IF p_invoice.pay_method_name IS NULL THEN
      CALL fgl_winmessage(get_str(48),get_str(1465),"error")
      RETURN FALSE
    END IF

    IF p_invoice.del_method IS NULL OR p_invoice.del_method  < 1 OR p_invoice.del_method  > 3 THEN
      CALL fgl_winmessage(get_str(48),get_str(1466),"error")
      RETURN FALSE
    END IF

  END IF

  RETURN TRUE

END FUNCTION



###################################################
# FUNCTION verify_inv_lines(p_row_count)
#
# Verify input stock items of invoice
#
# RETURN TRUE or FALSE
###################################################
FUNCTION verify_inv_lines(p_row_count)

  DEFINE i, p_row_count  SMALLINT,
         l_stock_id      LIKE stock_item.stock_id,
         l_err_msg       VARCHAR(250)

  CALL fgl_dialog_update_data()

  FOR i=1 TO p_row_count
    LET l_stock_id = NULL

    SELECT stock_id
      INTO l_stock_id
      FROM stock_item
     WHERE stock_id = md_inv_lines[i].stock_id

    IF l_stock_id IS NULL AND md_inv_lines[i].stock_id IS NOT NULL THEN 
      LET l_err_msg = get_str(2560), " ", md_inv_lines[i].stock_id 
      CALL fgl_winmessage(get_str(48),l_err_msg,"error")
      LET l_verified_inv_lines = FALSE
      RETURN FALSE
    END IF
  END FOR

  LET l_verified_inv_lines = TRUE
  RETURN TRUE

END FUNCTION



###################################################
# FUNCTION invoice_input_detail(p_invoice,w_name)
#
# Input invoice detail
#
# RETURN input_status
###################################################
FUNCTION invoice_input_detail(p_invoice,w_name)
  DEFINE 
    a_idx, s_idx   INTEGER,
    tmp            CHAR(20),
    i              INTEGER,
    p_invoice      RECORD LIKE invoice.*,
    l_stock_item   RECORD LIKE stock_item.*,
    tax_rate       LIKE tax_rates.tax_rate,
    input_status   SMALLINT,
    cnt            INTEGER,
    w_name         VARCHAR(30)

  #Note: w_name is required to handle window operations 'current'  

  LET input_status = 0

  CALL show_invoice_header(p_invoice.*)

  LET i = load_invoice_temp_detail()

  DELETE FROM invoice_line_temp

  CALL set_count(i)

  LET int_flag = FALSE

  INPUT ARRAY md_inv_lines 
    WITHOUT DEFAULTS 
    FROM sc_inv_lines.*

    BEFORE INPUT
      CALL update_totals(arr_count(), p_invoice.currency_id)
      CALL set_help_id(601)
      CALL publish_toolbar("InvoiceEditLine",0)

    BEFORE ROW
      LET a_idx = arr_curr()
      LET s_idx = scr_line()
   
    AFTER FIELD quantity
      CALL update_line_total()

    AFTER FIELD stock_id
      IF verify_inv_lines(arr_count()) THEN
        DISPLAY "" AT 1, 1
        CALL get_stock_item(md_inv_lines[a_idx].stock_id)
          RETURNING l_stock_item.*
        LET md_inv_lines[a_idx].item_desc = l_stock_item.item_desc
        LET md_inv_lines[a_idx].item_cost = l_stock_item.item_cost
        DISPLAY md_inv_lines[a_idx].stock_id TO sc_inv_lines[s_idx].stock_id
        DISPLAY md_inv_lines[a_idx].item_desc TO sc_inv_lines[s_idx].item_desc
        DISPLAY md_inv_lines[a_idx].item_cost TO sc_inv_lines[s_idx].item_cost
        CALL update_line_total()
      END IF

    AFTER DELETE
      CALL update_totals(arr_count(), p_invoice.currency_id)

    AFTER ROW
      CALL update_line_total()
      CALL update_totals(arr_count(), p_invoice.currency_id)

    ON KEY (F10)
      IF INFIELD(stock_id) THEN
        CALL stock_item_popup(md_inv_lines[a_idx].stock_id,"stock_id",0)
          RETURNING md_inv_lines[a_idx].stock_id
        CALL fgl_window_current(w_name)  --make sure, window is active
        CALL get_stock_item(md_inv_lines[a_idx].stock_id)
          RETURNING l_stock_item.*

        LET md_inv_lines[a_idx].item_desc = l_stock_item.item_desc
        LET md_inv_lines[a_idx].item_cost = l_stock_item.item_cost

        DISPLAY md_inv_lines[a_idx].stock_id TO sc_inv_lines[s_idx].stock_id
        DISPLAY md_inv_lines[a_idx].item_desc TO sc_inv_lines[s_idx].item_desc
        DISPLAY md_inv_lines[a_idx].item_cost TO sc_inv_lines[s_idx].item_cost

        NEXT FIELD quantity
      END IF

    ON KEY (F11)
      IF verify_inv_lines(arr_count()) THEN
        LET input_status = 1
        EXIT INPUT
      END IF

    ON KEY (ACCEPT)
      IF verify_inv_lines(arr_count()) THEN
        EXIT INPUT
      ELSE
        CONTINUE INPUT
      END IF

    AFTER INPUT
      IF NOT int_flag THEN
        IF NOT l_verified_inv_lines THEN
          CONTINUE INPUT
        END IF
      END IF

  END INPUT

  #Possible toolbar clean up
  CALL publish_toolbar("InvoiceEditLine",1)

  IF int_flag THEN
    RETURN input_status
  END IF

  LET cnt = arr_count()

  FOR i = 1 TO cnt
    IF md_inv_lines[i].stock_id IS NULL OR 
       md_inv_lines[i].stock_id < 1 
    THEN
      CONTINUE FOR
    END IF

    CALL get_stock_item_tax(md_inv_lines[i].stock_id)
      RETURNING tax_rate

    INSERT INTO invoice_line_temp 
      VALUES (p_invoice.invoice_id, 
              md_inv_lines[i].quantity,
              md_inv_lines[i].stock_id,
              tax_rate)
  END FOR
  
  RETURN input_status
END FUNCTION


###################################################
# FUNCTION update_line_total()
#
# Update the invoice line totals
#
# RETURN NONE
###################################################
FUNCTION update_line_total()
  DEFINE 
    l_stock_item RECORD LIKE stock_item.*,
    l_tax_rates  RECORD LIKE tax_rates.*,
    a_idx, s_idx INTEGER,
    l_account_id LIKE account.account_id

  LET a_idx = arr_curr()
  LET s_idx = scr_line()

  CALL get_stock_item(md_inv_lines[a_idx].stock_id)
    RETURNING l_stock_item.*

  CALL get_tax_rate_rec(l_stock_item.rate_id)
    RETURNING l_tax_rates.*

  LET md_inv_lines[a_idx].line_net_total = l_stock_item.item_cost * md_inv_lines[a_idx].quantity
  LET md_inv_lines[a_idx].line_tax_total = md_inv_lines[a_idx].line_net_total * l_tax_rates.tax_rate / 100
  LET md_inv_lines[a_idx].line_total = md_inv_lines[a_idx].line_net_total + md_inv_lines[a_idx].line_tax_total
  
  #DISPLAY invoice detail summary
  DISPLAY md_inv_lines[a_idx].line_net_total TO sc_inv_lines[s_idx].line_net_total  
  DISPLAY md_inv_lines[a_idx].line_tax_total TO sc_inv_lines[s_idx].line_tax_total
  DISPLAY md_inv_lines[a_idx].line_total TO sc_inv_lines[s_idx].line_total

END FUNCTION



###################################################
# FUNCTION update_totals(cnt)
#
# updates all line totals (individual) NOT summary total
#
# RETURN NONE
###################################################
FUNCTION update_totals(cnt, p_currency_id)
  DEFINE i, cnt INTEGER,
         p_currency_id LIKE currency.currency_id

  LET inv_total = 0
  LET tax_total = 0
  LET net_total = 0
  LET for_total = 0
  LET xchg_rate = 1
  LET xchg_rate = get_xchg_rate(p_currency_id)

  IF xchg_rate IS NULL THEN
    LET xchg_rate = get_xchg_rate_iv(l_inv_id)
  END IF
  
  IF cnt THEN
    FOR i = 1 TO cnt
      LET inv_total = inv_total + md_inv_lines[i].line_total
      LET tax_total = tax_total + md_inv_lines[i].line_tax_total
      LET net_total = net_total + md_inv_lines[i].line_net_total
    END FOR

    LET for_total = inv_total * xchg_rate
  END IF

  DISPLAY BY NAME inv_total, tax_total, net_total, for_total


END FUNCTION



###################################################
# FUNCTION invoice_update_totals(p_invoice_id)
#
# Calculates invoice total and stores it to the database
#
# RETURN NONE
###################################################
FUNCTION invoice_update_totals(p_invoice_id)
  DEFINE 
    p_invoice_id LIKE invoice.invoice_id,
    l_tax_total  LIKE invoice.tax_total,
    l_net_total  LIKE invoice.net_total,
    l_inv_total  LIKE invoice.inv_total,
    l_for_total  LIKE invoice.for_total,
    l_xchg_rate  LIKE currency.xchg_rate

  SELECT SUM(stock_item.item_cost * invoice_line.quantity),
         SUM(stock_item.item_cost * tax_rates.tax_rate / 100 * invoice_line.quantity)
    INTO l_net_total, l_tax_total
    FROM invoice_line, tax_rates, stock_item, invoice
    WHERE invoice_line.invoice_id = invoice.invoice_id
      AND invoice.invoice_id = p_invoice_id
      AND invoice_line.stock_id = stock_item.stock_id
      AND tax_rates.rate_id = stock_item.rate_id    

  IF sqlca.sqlcode <> NOTFOUND THEN
    LET l_xchg_rate = get_xchg_rate_iv(p_invoice_id)

    LET l_inv_total = l_net_total + l_tax_total
    LET l_for_total = l_inv_total * l_xchg_rate

    IF l_inv_total IS NULL THEN
      LET l_inv_total = 0
    END IF

    IF l_for_total IS NULL THEN
      LET l_for_total = 0
    END IF

    IF l_tax_total IS NULL THEN
      LET l_tax_total = 0
    END IF

    IF l_net_total IS NULL THEN
      LET l_net_total = 0
    END IF

    UPDATE invoice SET
      invoice.inv_total = l_inv_total,
      invoice.net_total = l_net_total,
      invoice.tax_total = l_tax_total,
      invoice.for_total = l_for_total
    WHERE invoice.invoice_id = p_invoice_id
  END IF
END FUNCTION



####################################################
# FUNCTION invoice_edit(p_invoice_id)
#
# Edit Invoice Record - inc. store and print
#
# RETURN NONE
####################################################
FUNCTION invoice_edit(p_invoice_id)
  DEFINE 
    p_invoice_id LIKE invoice.invoice_id,
    l_invoice    RECORD LIKE invoice.*,
    l_process_invoice SMALLINT,
    l_pre_invoice_status LIKE invoice.status

  CALL get_invoice_rec(p_invoice_id)
    RETURNING l_invoice.*

  LET l_pre_invoice_status = l_invoice.status
  LET l_process_invoice = 0
  BEGIN WORK
    CALL create_invoice_temp(l_invoice.invoice_id)

    CALL invoice_input(l_invoice.*)
      RETURNING l_invoice.*

    IF l_pre_invoice_status = 4 AND l_invoice.status = 5 THEN
      LET l_process_invoice = 1
    END IF
    CALL copy_invoice_temp(l_invoice.invoice_id, l_process_invoice)

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN
    ELSE
      UPDATE invoice SET
        invoice.account_id = l_invoice.account_id,
        invoice.invoice_date = l_invoice.invoice_date,
        invoice.pay_date = l_invoice.pay_date,
        invoice.tax_total = l_invoice.tax_total,
        invoice.net_total = l_invoice.net_total,
        invoice.inv_total = l_invoice.inv_total,
        invoice.currency_id = l_invoice.currency_id,
        invoice.for_total = l_invoice.for_total,
        invoice.operator_id = l_invoice.operator_id,
        invoice.pay_method_id = l_invoice.pay_method_id,
        invoice.pay_method_name = l_invoice.pay_method_name,
        invoice.pay_method_desc = l_invoice.pay_method_desc,
        invoice.del_address_dif = l_invoice.del_address_dif,
        invoice.del_address1 = l_invoice.del_address1,
        invoice.del_address2 = l_invoice.del_address2,
        invoice.del_address3 = l_invoice.del_address3,
        invoice.del_method = l_invoice.del_method,
        invoice.invoice_po = l_invoice.invoice_po,
        invoice.status = l_invoice.status
        WHERE invoice.invoice_id = p_invoice_id
    END IF

    CALL invoice_update_totals(l_invoice.invoice_id)
    CALL update_processible_invoice(l_invoice.*)
    CALL print_invoice(l_invoice.invoice_id)

  COMMIT WORK
END FUNCTION



############################################################################################################################
# Search & List functions
############################################################################################################################


###################################################
# FUNCTION invoice_query()
#
# calls popup and views returned invoice
#
# RETURN NONE
###################################################
FUNCTION invoice_query(p_status)
  DEFINE 
        l_invoice_id LIKE invoice.invoice_id,
	p_status SMALLINT

  LET l_invoice_id = invoice_popup(l_invoice_id,"invoice_id",0,p_status)  --get invoice from selection list
  LET l_inv_id = l_invoice_id
  
  IF exist(l_invoice_id) THEN

      IF NOT fgl_window_open("w_invoice_query", 3, 3, get_form_path("f_invoice_det_l2"),FALSE) THEN
        CALL fgl_winmessage("Error","invoice_input()\nCan not open window w_invoice_show()","error")
        RETURN
      END IF
      CALL populate_invoice_form_query_labels_g()
     CALL invoice_status_combo_list("status_name", l_invoice_id)

    CALL show_invoice(l_invoice_id)
    CALL fgl_getkey()

    CALL fgl_window_close("w_invoice_show")
  END IF

END FUNCTION



###################################################
# FUNCTION invoice_popup_data_source(p_order_field,p_ord_dir,p_status)
#
#  Displays invoice list and let's operator choose to view/edit/delete
#
# RETURN rv_invoice_id
###################################################
FUNCTION invoice_popup_data_source(p_order_field,p_ord_dir,p_status)
  DEFINE 
    i INTEGER,
    p_order_field         VARCHAR(128),
    local_debug           SMALLINT,
    sql_stmt              CHAR(2048),
    p_ord_dir             SMALLINT,
    p_status		  SMALLINT,
    p_ord_dir_str         VARCHAR(4)

  LET local_Debug = 0

  IF p_order_field IS NULL  THEN
    LET p_order_field = "invoice_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  LET sql_stmt = "SELECT invoice.invoice_id, ",
                   "company.comp_name, ",
                   "invoice.net_total, ",
                   "invoice.tax_total, ",
                   "invoice.inv_total, ",
                   "invoice.invoice_date, ",
		   "invoice.status ",
                 "FROM invoice, account, company ",
                 "WHERE invoice.account_id = account.account_id ",
                   "AND company.comp_id = account.comp_id"

  IF p_status IS NOT NULL THEN
    IF p_status < 0 THEN
      LET p_status = 1
    END IF
    LET sql_stmt = sql_stmt CLIPPED, " AND invoice.status = ", p_status
  END IF
#  DISPLAY "sql_stmt",sql_stmt," ds ",p_status
  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF
  IF local_debug THEN
    DISPLAY "invoice_popup_data_source() - SQL"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
  END IF

  PREPARE p_invoice_scroll FROM sql_stmt
  DECLARE c_invoice_scroll CURSOR FOR p_invoice_scroll

END FUNCTION



###################################################
# FUNCTION invoice_popup(p_invoice_id,p_order_field,accept_action,p_status)
#
#  Displays invoice list and let's operator choose to view/edit/delete
#
# RETURN rv_invoice_id
###################################################
FUNCTION invoice_popup(p_invoice_id,p_order_field,p_accept_action,p_status)
  DEFINE 
    l_invoice_arr                  DYNAMIC ARRAY OF t_invoice_short_rec,
    l_arr_size                     SMALLINT,
    p_invoice_id                   LIKE invoice.invoice_id,
    i                              INTEGER,
    rv_invoice_id                  LIKE invoice.invoice_id,
    p_order_field,p_order_field2   VARCHAR(128), 
    local_debug                    SMALLINT,
    col_sort                       SMALLINT,   --is set to true if the column should be sorted
    p_accept_action                SMALLINT,
    p_status                       SMALLINT,
    l_scroll_row                   SMALLINT,
    l_scroll_invoice_id            LIKE invoice.invoice_id,
    l_invoice_id                   LIKE invoice.invoice_id,
    l_status                       SMALLINT

  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC
  #LET l_arr_size = sizeof(l_invoice_arr) removed due to dynamic array change
  LET l_arr_size = 1000 --limit the rows to 1000 max --sizeof(l_invoice_arr)
  
  LET l_scroll_row = 1

  IF NOT p_order_field THEN
    LET p_order_field = "invoice_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF


    IF NOT fgl_window_open("w_invoice_popup", 3, 3, get_form_path("f_invoice_scroll_l2"), FALSE) THEN
      CALL fgl_winmessage("Error","invoice_popup()\nCould not open window w_invoice_popup","error")
      RETURN p_invoice_id

    END IF
    CALL populate_invoice_scroll_form_labels_g()

  LET int_flag = FALSE

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL invoice_popup_data_source(p_order_field,get_toggle_switch(),p_status)

		IF l_invoice_arr.getsize() > 0 THEN
			CALL l_invoice_arr.delete(1 , l_invoice_arr.getsize())
		END IF

    LET i = 1
    FOREACH c_invoice_scroll INTO l_invoice_arr[i].invoice_id,l_invoice_arr[i].comp_name,l_invoice_arr[i].net_total,l_invoice_arr[i].tax_total
		,l_invoice_arr[i].inv_total,l_invoice_arr[i].invoice_date,l_status
      IF i > l_arr_size THEN
        CALL fgl_winmessage("Too many rows for array","Can only display the first " || l_arr_size || " of records","info")
        EXIT FOREACH
      END IF
      IF p_status = -1 AND NOT is_past_due(l_status,l_invoice_arr[i].invoice_date) OR p_status = 1 AND is_past_due(l_status,l_invoice_arr[i].invoice_date) THEN
        CONTINUE FOREACH
      END IF
      IF l_invoice_arr[i].invoice_id = l_scroll_invoice_id THEN
        LET l_scroll_row = i
      END IF
      LET l_invoice_arr[i].status_name = get_invoice_status(l_status,l_invoice_arr[i].invoice_date)
      LET i = i + 1
    END FOREACH
    LET i = i - 1


		IF i > 0 THEN
			CALL l_invoice_arr.reSize(i)  --correct the last element of the dynamic array
		ELSE
			CALL l_invoice_arr.clear()
		END IF 
		
		
    #CALL set_count(i)

    DISPLAY ARRAY l_invoice_arr TO sc_invoice_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1

      BEFORE DISPLAY
        #IF fgl_fglgui() THEN
          IF i = 0 THEN
            DISPLAY "*" TO bt_print
            DISPLAY "*" TO bt_edit
            DISPLAY "*" TO bt_ok
          ELSE
            DISPLAY "!" TO bt_print
            DISPLAY "!" TO bt_edit
            DISPLAY "!" TO bt_ok
          END IF
        #END IF

        CALL publish_toolbar("InvoiceList",0)
        CALL fgl_dialog_setcurrline(1,l_scroll_row)
        LET i = arr_curr()
        LET l_invoice_id = l_invoice_arr[i].invoice_id  
        LET l_scroll_invoice_id = l_invoice_arr[i].invoice_id 
    
      BEFORE ROW
        LET i = arr_curr()
        LET l_invoice_id = l_invoice_arr[i].invoice_id  
        LET l_scroll_invoice_id = l_invoice_arr[i].invoice_id  

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)


        CASE p_accept_action
          WHEN 0  --just return id
            EXIT WHILE
          WHEN 1  --view

            CALL fgl_window_current("SCREEN")

            CALL invoice_view(l_invoice_id)
            CALL fgl_window_current("w_invoice_popup")

          WHEN 2  --edit
            CALL fgl_window_current("SCREEN")
            CALL invoice_edit(l_invoice_id)
            CALL fgl_window_current("w_invoice_popup")
          WHEN 3
            CALL invoice_delete(l_invoice_id)
          WHEN 4
            CALL print_invoice(l_invoice_id)
            CALL fgl_window_current("w_invoice_popup")
          OTHERWISE
            CALL fgl_winmessage("invoice_popup() - 4GL Source Error","invoice_popup(p_invoice_id,p_order_field,p_accept_action = " || p_accept_action, "error") 
        END CASE

        CALL fgl_window_current("w_invoice_popup")

      ON KEY (F4)
        CALL invoice_create()
        EXIT DISPLAY

      ON KEY (F5)  --Edit Invoice
        CALL fgl_window_current("SCREEN")
        CALL invoice_edit(l_invoice_id)
        CALL fgl_window_current("w_invoice_popup")
        EXIT DISPLAY

      ON KEY (F6)  --delete invoice
        CALL invoice_delete(l_invoice_id)
        EXIT DISPLAY

      ON KEY (F8)  --print invoice
        CALL print_invoice(l_invoice_id)
        DISPLAY "                                             " AT 1,1
        DISPLAY "                                             " AT 2,1
        DISPLAY get_str(1540) TO lbTitle
        EXIT DISPLAY



      #Column Sort shortcut- Grid Column
      ON KEY(F13)
        CALL column_sort(p_order_field,"invoice_id") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      ON KEY(F14)
        CALL column_sort(p_order_field,"comp_name") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      ON KEY(F15)
        CALL column_sort(p_order_field,"net_total") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      ON KEY(F16)
        CALL column_sort(p_order_field,"tax_total") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      ON KEY(F17)
        CALL column_sort(p_order_field,"inv_total") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      ON KEY(F18)
        CALL column_sort(p_order_field,"invoice_date") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      # Status Filter
      ON KEY (F121)
        LET p_status = NULL
        EXIT DISPLAY
      ON KEY (F122)
        LET p_status = 1
        EXIT DISPLAY
      ON KEY (F123)
        LET p_status = 2
        EXIT DISPLAY
      ON KEY (F124)
        LET p_status = -1
        EXIT DISPLAY
      ON KEY (F125)
        LET p_status = 3
        EXIT DISPLAY
      ON KEY (F126)
        LET p_status = 4
        EXIT DISPLAY
      ON KEY (F127)
        LET p_status = 5
        EXIT DISPLAY
        
    END DISPLAY

    CALL publish_toolbar("InvoiceList",1)

    IF int_flag = TRUE THEN  --check if operator choose cancel
      EXIT WHILE
    END IF
    
    IF col_sort THEN  
      LET col_sort = FALSE
      CALL invoice_popup_data_source(p_order_field,NULL,NULL)
    END IF

  END WHILE

  CALL fgl_window_close("w_invoice_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_invoice_id
  ELSE
    LET i = arr_curr()
    LET rv_invoice_id = l_invoice_arr[i].invoice_id
  END IF

  RETURN rv_invoice_id
END FUNCTION



####################################################
# FUNCTION select_invoices()
#
# Select invoice from list for printing
#
# RETURN
####################################################
FUNCTION select_invoices()
  DEFINE 
    l_invoice_id   LIKE invoice.invoice_id,
    l_invoice_line RECORD LIKE invoice_line.*

  LET l_invoice_id = invoice_popup(l_invoice_id,"invoice_id",0,NULL)

  IF exist(l_invoice_id) THEN
    #CALL print_invoice(l_invoice_id)
    RETURN l_invoice_id
  END IF

  RETURN NULL

END FUNCTION




############################################################################################################################
# Display functions
############################################################################################################################

###################################################
# FUNCTION show_header(p_invoice)
#
# Displays the invoice details to the current window/form
#
# RETURN NONE
###################################################
FUNCTION show_invoice_header(p_invoice)
  DEFINE 
    p_invoice   RECORD LIKE invoice.*,
    l_account   RECORD LIKE account.*,
    l_company   RECORD LIKE company.*,
    l_contact   RECORD LIKE contact.*,
    l_operator_name LIKE operator.name,
    l_currency   RECORD LIKE currency.*

  CALL get_account_rec(p_invoice.account_id)
    RETURNING l_account.*

  SELECT operator.name
    INTO l_operator_name
    FROM operator  
    WHERE operator.operator_id = p_invoice.operator_id

  CALL get_company_rec(l_account.comp_id)
    RETURNING l_company.*

  CALL web_get_contact_rec(l_company.comp_main_cont)
    RETURNING l_contact.*

  CALL get_currency_rec(p_invoice.currency_id)
    RETURNING l_currency.*

  DISPLAY l_operator_name TO operator.name

  #DISPLAY BY NAME p_invoice.pay_method
  DISPLAY BY NAME p_invoice.pay_method_name
  DISPLAY BY NAME l_company.comp_id
  DISPLAY BY NAME l_company.comp_name
  DISPLAY BY NAME l_contact.cont_name
  DISPLAY BY NAME l_contact.cont_phone
  DISPLAY BY NAME l_account.discount
  DISPLAY BY NAME l_account.credit_limit
  DISPLAY BY NAME p_invoice.invoice_date
  DISPLAY BY NAME p_invoice.invoice_id
  DISPLAY BY NAME p_invoice.del_method
  DISPLAY BY NAME p_invoice.del_address1
  DISPLAY BY NAME p_invoice.del_address2
  DISPLAY BY NAME p_invoice.del_address3
  DISPLAY BY NAME p_invoice.del_address_dif
  DISPLAY BY NAME p_invoice.invoice_po
  DISPLAY get_invoice_status(p_invoice.status,p_invoice.invoice_date) TO status_name
  DISPLAY get_pay_method_name(p_invoice.pay_method_id) TO pay_method_name
  DISPLAY get_pay_method_desc(p_invoice.pay_method_id) TO pay_method_desc
  DISPLAY get_currency_name(p_invoice.currency_id) TO currency_name
  DISPLAY BY NAME l_currency.currency_name
  DISPLAY BY NAME l_currency.xchg_rate

END FUNCTION



##################################################
# FUNCTION show_invoice_detail(p_invoice_id)
##################################################
FUNCTION show_invoice_detail(p_invoice_id)
  DEFINE 
    p_invoice_id   LIKE invoice.invoice_id,
    # is modular now l_inv_line     DYNAMIC ARRAY OF t_inv_line,  --[20]
    i              INTEGER,
    l_account_id   LIKE account.account_id,
    l_currency_id LIKE currency.currency_id

  DECLARE c_get_invoice_detail CURSOR FOR
    SELECT stock_item.stock_id,
           invoice_line.quantity,
           stock_item.item_cost,
           stock_item.item_desc,
           (stock_item.item_cost * invoice_line.quantity),
           ((stock_item.item_cost * invoice_line.quantity) * tax_rates.tax_rate / 100),
           ((stock_item.item_cost * invoice_line.quantity) + ((stock_item.item_cost * invoice_line.quantity) * tax_rates.tax_rate / 100))
      FROM stock_item, invoice_line, invoice, tax_rates
      WHERE invoice.invoice_id = p_invoice_id
        AND invoice_line.invoice_id = invoice.invoice_id
        AND stock_item.stock_id = invoice_line.stock_id
        AND stock_item.rate_id = tax_rates.rate_id

  LET i = 1

  FOREACH c_get_invoice_detail INTO md_inv_lines[i].*
    LET i = i + 1
    IF i > 20 THEN
      EXIT FOREACH
    END IF
  END FOREACH

  LET i = i - 1

	IF i > 0 THEN
		CALL md_inv_lines.resize(i)  --correct the last element of the dynamic array
	END IF   
	
  #CALL set_count(i)

  DISPLAY ARRAY md_inv_lines TO sc_inv_lines.* WITHOUT SCROLL


  #CALL display_credit_note(get_account_credit_balance(p_invoice.account_id))
  #retrieve the account_id and check for balance/credit statement
  LET l_account_id = get_account_id_from_invoice_id(p_invoice_id)
  IF get_account_credit_balance(l_account_id) < 0 THEN
    #No Credit Left - Oustanding Balance:
    LET tmp_str = get_Str(1469), " ", get_account_credit_balance(l_account_id)
    DISPLAY tmp_str  TO lbInfo1 ATTRIBUTE(RED,BOLD)
  ELSE
    #Max. Credit Left:
    LET tmp_str = get_Str(1470), " ", get_account_credit_balance(l_account_id)
    DISPLAY tmp_str  TO lbInfo1 ATTRIBUTE(GREEN)
  END IF

  SELECT invoice.currency_id
    INTO l_currency_id
    FROM invoice
   WHERE invoice.invoice_id = p_invoice_id 

  CALL update_totals(i, l_currency_id)

END FUNCTION




##################################################
# FUNCTION invoice_show_temp_detail()
#
#
#
# RETURN NONE
##################################################
FUNCTION invoice_show_temp_detail()
  DEFINE i INTEGER

  LET i = load_invoice_temp_detail()

  CALL set_count(i)
  CALL update_totals(i, NULL)

  DISPLAY ARRAY md_inv_lines TO sc_inv_lines.* WITHOUT SCROLL
END FUNCTION




####################################################
# FUNCTION show_invoice(p_invoice_id)
#
# View Invoice (no edit)
#
# RETURN NONE
####################################################
FUNCTION show_invoice(p_invoice_id)
  DEFINE 
    p_invoice_id LIKE invoice.invoice_id,
    l_invoice    RECORD LIKE invoice.*

  CALL get_invoice_rec(p_invoice_id)
    RETURNING l_invoice.*

  CALL show_invoice_header(l_invoice.*)
  CALL show_invoice_detail(p_invoice_id)

END FUNCTION



####################################################
# FUNCTION invoice_view(p_invoice_id)
#
# View Invoice Record
#
# RETURN NONE
####################################################
FUNCTION invoice_view(p_invoice_id)
  DEFINE 
    p_invoice_id LIKE invoice.invoice_id,
    inp_char     CHAR

  LET l_inv_id = p_invoice_id

  CALL fgl_window_current("SCREEN")

    IF NOT fgl_window_open("w_inv_view", 1, 1, get_form_path("f_invoice_det_l2"),FALSE) THEN
      CALL fgl_winmessage("Error","invoice_view()\nCan not open window w_inv_view()","error")
      RETURN

    END IF
    CALL populate_invoice_form_view_labels_g()
    CALL invoice_status_combo_list("status_name", p_invoice_id)


  CALL show_invoice(p_invoice_id)

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(INTERRUPT,ACCEPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_inv_view")

END FUNCTION

FUNCTION past_due_val()

  RETURN INTERVAL (3) DAY TO DAY

END FUNCTION

FUNCTION get_invoice_status(p_status_id,p_invoice_date)
  DEFINE
    p_status_id    LIKE invoice.status,
    p_invoice_date LIKE invoice.invoice_date
  
  IF p_status_id <= 0 OR p_status_id > 5 THEN
    RETURN "<ERR>"
  END IF

  IF is_past_due(p_status_id,p_invoice_date) THEN
    IF fgl_fglgui() THEN --gui client
      CALL fgl_list_clear("status_name", 1, 100) 
      CALL fgl_list_set("status_name", 1, past_due_msg)
    END IF
    RETURN past_due_msg
  ELSE
    RETURN l_status[p_status_id]
  END IF
END FUNCTION

FUNCTION is_past_due(p_status_id,p_invoice_date)
  DEFINE
    p_status_id LIKE invoice.status,
    p_invoice_date LIKE invoice.invoice_date
  RETURN p_status_id = 1 AND TODAY > p_invoice_date + past_due_val()
END FUNCTION



####################################################
# FUNCTION display_credit_note(p_balance)
#
#
#
# RETURN NONE
####################################################
FUNCTION display_credit_note(p_balance)
  DEFINE p_balance MONEY(8,2)

  IF p_balance < 0 THEN  --No Credit Left - Oustanding Balance:
    LET tmp_str = get_str(1469), " ", p_balance
    DISPLAY tmp_str TO lbInfo1 ATTRIBUTE(RED,BOLD)
  ELSE  --Max. Credit Left: 
    LET tmp_str = get_str(1470), " ", p_balance
    DISPLAY tmp_str TO lbInfo1 ATTRIBUTE(GREEN)
  END IF

END FUNCTION



###################################################################################
# FUNCTION invoice_status_combo_list(cb_field_name, p_invoice_id)
#
# Populate invoice_status combo list from database
#
# RETURN NONE
###################################################################################
FUNCTION invoice_status_combo_list(cb_field_name, p_invoice_id)
  DEFINE  
    p_invoice_id     INTEGER,
    l_status_id      LIKE status_invoice.status_id,
    l_inv_status_id  INTEGER,
    row_count        INTEGER,
    current_row      INTEGER,
    cb_field_name    VARCHAR(20)   --form field name for the invoice status combo list field

  IF p_invoice_id IS NOT NULL THEN
    SELECT invoice.status
      INTO l_status_id
      FROM invoice
     WHERE invoice.invoice_id = p_invoice_id
  ELSE
    LET l_status_id = 0
  END IF

  DECLARE c_inv_status_scroll CURSOR FOR 
   SELECT status_invoice.status_id
     FROM status_invoice
    WHERE status_id = l_status_id
   UNION
   SELECT status_invoice.status_id
     FROM status_invoice
    WHERE status_id = l_status_id + 1
      AND (l_status_id > 2 OR l_status_id = 0)
      AND l_status_id <> 3
   UNION
   SELECT status_invoice.status_id
     FROM status_invoice
    WHERE status_id = 3
      AND l_status_id = 1
   UNION
   SELECT status_invoice.status_id
     FROM status_invoice
    WHERE status_id = 2 
      AND l_status_id NOT IN (0,5)

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_inv_status_scroll INTO l_inv_status_id
    CALL fgl_list_set(cb_field_name, row_count, l_status[l_inv_status_id])
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

END FUNCTION



#########################################################
# FUNCTION get_inv_status_id(inv_status_name)
#
# Get invoice status id from name
#
# RETURN l_inv_status_id
#########################################################
FUNCTION get_inv_status_id(inv_status_name)

  DEFINE inv_status_name LIKE status_invoice.status_name,
         i               INTEGER
       
  FOR i=1 TO 5
    IF l_status[i] = inv_status_name THEN
      RETURN i
    END IF
  END FOR

  RETURN l_inv_stat

END FUNCTION




#########################################################
# FUNCTION get_inv_status_name(inv_status_id)
#
# Get invoice status name from id
#
# RETURN l_inv_status_name
#########################################################
FUNCTION get_inv_status_name(inv_status_id)

  DEFINE inv_status_id     LIKE status_invoice.status_id

  RETURN l_status[inv_status_id]

END FUNCTION


