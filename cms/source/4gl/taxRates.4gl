##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the tax_rates table
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                  RETURN
# get_tax_rate_rec(p_rate_id)                      get tax rate record from tax_id               l_tax_rate.*
# get_tax_rate(p_rate_id)                          get tax from rate_id                          l_tax_rate.tax_rate
# tax_rate_create()                                create tax rate record                        l_tax_rate.rate_id
# tax_rate_edit(p_rate_id)                         edit tax rate record                          NONE
# tax_rate_input(p_tax_rate)                       input tax rate record data                    p_tax_rate.*
# tax_rate_delete(p_rate_id)                       delete tax rate record                        NONE
# tax_rate_view_by_rec(p_tax_rate_rec)             display tax rate details on form              NONE
# tax_rate_view(p_tax_rate_id)                     display tax rate details in window-form by ID NONE
# tax_rate_popup(p_rate_id)                        display tax rate selection window             l_tax_arr[i].rate_id or p_rate_id
# grid_header_tax_scroll                           populate grid array with labels               NONE
#
############################################################################################################
####################################################
#
####################################################
GLOBALS "globals.4gl"

####################################################
# FUNCTION tax_rate_create()
#
# Create tax rate record
#
# RETURN l_tax_rate.rate_id
####################################################
FUNCTION tax_rate_create()
  DEFINE l_tax_rate RECORD LIKE tax_rates.*

  LET l_tax_rate.rate_id = 0

  CALL tax_rate_input(l_tax_rate.*)
    RETURNING l_tax_rate.*
 
  IF NOT int_flag THEN
    INSERT INTO tax_rates VALUES (l_tax_rate.*)
    LET l_tax_rate.rate_id = sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
  END IF

  RETURN l_tax_rate.rate_id
END FUNCTION


####################################################
# FUNCTION tax_rate_edit(p_rate_id)
#
# Edit tax rate record
#
# RETURN NONE
####################################################
FUNCTION tax_rate_edit(p_rate_id)
  DEFINE 
    p_rate_id  LIKE tax_rates.rate_id,
    l_tax_rate RECORD LIKE tax_rates.*

  CALL get_tax_rate_rec(p_rate_id)
    RETURNING l_tax_rate.*

  CALL tax_rate_input(l_tax_rate.*)
    RETURNING l_tax_rate.*

  IF NOT int_flag THEN
    UPDATE tax_rates
      SET tax_rate = l_tax_rate.tax_rate,
          tax_desc = l_tax_rate.tax_desc
      WHERE tax_rates.rate_id = l_tax_rate.rate_id
  ELSE
    LET int_flag = FALSE
  END IF

END FUNCTION


####################################################
# FUNCTION tax_rate_input(p_tax_rate)
#
# Input tax record details (edit/create)
#
# RETURN p_tax_rate.* 
####################################################
FUNCTION tax_rate_input(p_tax_rate)
  DEFINE p_tax_rate RECORD LIKE tax_rates.*


    CALL fgl_window_open("w_tax_rate", 3, 3, get_form_path("f_tax_rates_l2"), FALSE)
    CALL populate_tax_detail_form_labels_g()
    #DISPLAY "!" TO bt_ok
    #DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_Str(1850))

  INPUT BY NAME p_tax_rate.tax_desc, 
                p_tax_rate.tax_rate 
    WITHOUT DEFAULTS
    AFTER INPUT
      IF p_tax_rate.tax_rate IS NULL THEN
        CALL fgl_winmessage(get_str(48),get_str(1881),"error")
        NEXT FIELD tax_rate
        CONTINUE INPUT
      END IF
      IF p_tax_rate.tax_desc IS NULL THEN
        CALL fgl_winmessage(get_str(48),get_str(1882),"error")
        NEXT FIELD tax_desc
        CONTINUE INPUT
      END IF
  END INPUT

  CALL fgl_window_close("w_tax_rate")

  RETURN p_tax_rate.*
END FUNCTION



####################################################
# FUNCTION tax_rate_view(p_tax_rate_id)
#
# display tax rate details in window-form by ID
#
# RETURN NONE
####################################################
FUNCTION tax_rate_view(p_tax_rate_id)
  DEFINE 
    p_tax_rate_id       LIKE tax_rates.rate_id,
    l_tax_rate_rec      RECORD LIKE tax_rates.*

  CALL get_tax_rate_rec(p_tax_rate_id) RETURNING l_tax_rate_rec.*
  CALL tax_rate_view_by_rec(l_tax_rate_rec.*)

END FUNCTION


####################################################
# FUNCTION tax_rate_view_by_rec(p_tax_rate_rec)
#
# display tax rate details on form
#
# RETURN NONE
####################################################
FUNCTION tax_rate_view_by_rec(p_tax_rate_rec)
  DEFINE 
    p_tax_rate_rec      RECORD LIKE tax_rates.*,
    inp_char            CHAR

    CALL fgl_window_open("w_tax_rate", 3, 3, get_form_path("f_tax_rates_l2"), FALSE)
    CALL populate_tax_detail_form_labels_g() 
    CALL fgl_settitle(get_Str(1850))
    #DISPLAY "!" TO fb_ok
    #DISPLAY "!" TO fb_cancel

  DISPLAY BY NAME p_tax_rate_rec.* 

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE 
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_tax_rate")

END FUNCTION



####################################################
# FUNCTION tax_rate_delete(p_rate_id)
#
# Delete Tax Rate record
#
# RETURN NONE
####################################################
FUNCTION tax_rate_delete(p_rate_id)
  DEFINE p_rate_id LIKE tax_rates.rate_id

  #"Delete!","Are you sure you want to delete this value?"
  IF yes_no(get_str(817),get_str(1880)) THEN
    DELETE FROM tax_rates
      WHERE tax_rates.rate_id = p_rate_id
  END IF
END FUNCTION


####################################################
# FUNCTION tax_rate_popup(p_rate_id)
#
# Data Source (cursor) for tax_rate_popup()
#
# RETURN NONE
####################################################
FUNCTION tax_rate_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "rate_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT * FROM tax_rates "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_tax_rates FROM sql_stmt
  DECLARE c_tax_rates CURSOR FOR p_tax_rates

END FUNCTION


####################################################
# FUNCTION tax_rate_popup(p_rate_id,p_order_field,p_accept_action)
#
# Popup tax rate selection window
#
# RETURN l_tax_arr[i].rate_id or p_rate_id
####################################################
FUNCTION tax_rate_popup(p_rate_id,p_order_field,p_accept_action)
  DEFINE 
    l_tax_arr                    DYNAMIC ARRAY OF RECORD LIKE tax_rates.*,
    i                            INTEGER,
    p_rate_id                    LIKE tax_rates.rate_id,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

    CALL fgl_window_open("w_tax_scroll", 2, 8, get_form_path("f_tax_scroll_l2"), FALSE) 
    CALL populate_tax_list_form_labels_g()
    #DISPLAY "!" TO bt_ok
    #DISPLAY "!" TO bt_create
    #DISPLAY "!" TO bt_edit
    #DISPLAY "!" TO bt_delete
    #DISPLAY "!" TO bt_cancel
    CALL fgl_settitle(get_Str(1851))
    CALL grid_header_tax_scroll()

	CALL ui.Interface.setImage("qx://application/icon16/money/percentage.png")
	CALL ui.Interface.setText("Tax Rate")
	
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL tax_rate_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_tax_rates INTO l_tax_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH

    LET i = i - 1

		If i > 0 THEN
			CALL l_tax_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF  

    LET int_flag = FALSE

    DISPLAY ARRAY l_tax_arr TO sa_tax_rates.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE DISPLAY
        CALL publish_toolbar("TaxRateList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_tax_arr[i].rate_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL tax_rate_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL tax_rate_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL tax_rate_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL tax_rate_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","tax_rate_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "tax_rate_popup(p_rate_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("tax_rate_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) # add
        CALL tax_rate_create()
        EXIT DISPLAY

      ON KEY (F5) # edit
        LET i = arr_curr()
        LET i = l_tax_arr[i].rate_id
        CALL tax_rate_edit(i)
        EXIT DISPLAY

      ON KEY (F6) # delete
        LET i = arr_curr()
        LET i = l_tax_arr[i].rate_id
        CALL tax_rate_delete(i)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "rate_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tax_rate"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tax_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_tax_scroll")

  IF NOT int_flag THEN
    LET i = arr_curr()
    RETURN l_tax_arr[i].rate_id
  ELSE 
    #RETURN NULL
    RETURN p_rate_id
  END IF
END FUNCTION

