##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



####################################################
# FUNCTION grid_header_country_scroll()
#
# Populate grid header
#
# RETURN NONE
####################################################
FUNCTION grid_header_country_scroll()
  CALL fgl_grid_header("sc_country_scroll","country",get_str(931),"left","F13")    --"Country:"

END FUNCTION

####################################################
# FUNCTION grid_header_country_scroll_advanced()
####################################################
FUNCTION grid_header_country_scroll_advanced()
  CALL fgl_grid_header("advanced_lookup_arr","country",get_str(942),"left","F13")   --"Country:"
END FUNCTION



#######################################################
# FUNCTION populate_country_form_labels_t()
#
# Populate country form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_country_form_labels_t()
  DISPLAY get_str(920) TO lbTitle
  DISPLAY get_str(921) TO dl_f1
  #DISPLAY get_str(922) TO dl_f2
  #DISPLAY get_str(923) TO dl_f3
  #DISPLAY get_str(924) TO dl_f4
  #DISPLAY get_str(925) TO dl_f5
  #DISPLAY get_str(926) TO dl_f6
  #DISPLAY get_str(927) TO dl_f7
  #DISPLAY get_str(928) TO dl_f8
  DISPLAY get_str(929) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_country_form_labels_g()
#
# Populate country form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_country_form_labels_g()
  DISPLAY get_str(920) TO lbTitle
  DISPLAY get_str(921) TO dl_f1
  #DISPLAY get_str(922) TO dl_f2
  #DISPLAY get_str(923) TO dl_f3
  #DISPLAY get_str(924) TO dl_f4
  #DISPLAY get_str(925) TO dl_f5
  #DISPLAY get_str(926) TO dl_f6
  #DISPLAY get_str(927) TO dl_f7
  #DISPLAY get_str(928) TO dl_f8
  DISPLAY get_str(929) TO lbInfo1

  CALL fgl_settitle(get_str(920))

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION


#######################################################
# FUNCTION populate_country_list_form_labels_g()
#
# Populate country list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_country_list_form_labels_g()
  DISPLAY get_str(930) TO lbTitle
  #DISPLAY get_str(931) TO dl_f1
  #DISPLAY get_str(932) TO dl_f2
  #DISPLAY get_str(933) TO dl_f3
  #DISPLAY get_str(934) TO dl_f4
  #DISPLAY get_str(935) TO dl_f5
  #DISPLAY get_str(936) TO dl_f6
  #DISPLAY get_str(937) TO dl_f7
  #DISPLAY get_str(938) TO lbInfo12
  DISPLAY get_str(939) TO lbInfo1

  CALL fgl_settitle(get_str(930))

  DISPLAY get_str(810) TO bt_accept
  DISPLAY "!" TO bt_accept

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL grid_header_country_scroll()

END FUNCTION


#######################################################
# FUNCTION populate_country_list_form_labels_t()
#
# Populate country list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_country_list_form_labels_t()
  DISPLAY get_str(930) TO lbTitle
  #DISPLAY get_str(931) TO dl_f1
  #DISPLAY get_str(932) TO dl_f2
  #DISPLAY get_str(933) TO dl_f3
  #DISPLAY get_str(934) TO dl_f4
  #DISPLAY get_str(935) TO dl_f5
  #DISPLAY get_str(936) TO dl_f6
  #DISPLAY get_str(937) TO dl_f7
  DISPLAY get_str(938) TO lbInfo12
  DISPLAY get_str(939) TO lbInfo1

END FUNCTION

{
#######################################################
# FUNCTION populate_country_list_form_labels_g()
#
# Populate country list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_country_list_form_labels_g()
  DISPLAY get_str(930) TO lbTitle
  #DISPLAY get_str(931) TO dl_f1
  #DISPLAY get_str(932) TO dl_f2
  #DISPLAY get_str(933) TO dl_f3
  #DISPLAY get_str(934) TO dl_f4
  #DISPLAY get_str(935) TO dl_f5
  #DISPLAY get_str(936) TO dl_f6
  #DISPLAY get_str(937) TO dl_f7
  DISPLAY get_str(938) TO lbInfo12
  DISPLAY get_str(939) TO lbInfo1

END FUNCTION

}


#######################################################
# FUNCTION populate_country_quick_search_form_labels_g()
#
# Populate country quick search form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_country_quick_search_form_labels_g()
  DISPLAY get_str(940) TO lbTitle
  DISPLAY get_str(941) TO dl_f1
  #DISPLAY get_str(942) TO dl_f2
  #DISPLAY get_str(943) TO dl_f3
  #DISPLAY get_str(944) TO dl_f4
  #DISPLAY get_str(945) TO dl_f5
  #DISPLAY get_str(946) TO dl_f6
  #DISPLAY get_str(947) TO dl_f7
  DISPLAY get_str(948) TO lbInfo2
  DISPLAY get_str(949) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_country_quick_search_form_labels_t()
#
# Populate country quick search form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_country_quick_search_form_labels_t()
  DISPLAY get_str(940) TO lbTitle
  DISPLAY get_str(941) TO dl_f1
  #DISPLAY get_str(942) TO dl_f2
  #DISPLAY get_str(943) TO dl_f3
  #DISPLAY get_str(944) TO dl_f4
  #DISPLAY get_str(945) TO dl_f5
  #DISPLAY get_str(946) TO dl_f6
  #DISPLAY get_str(947) TO dl_f7
  DISPLAY get_str(948) TO lbInfo2
  DISPLAY get_str(949) TO lbInfo1

END FUNCTION
