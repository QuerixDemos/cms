##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to print html files
# with variable contents
#
# Created:
# 06.12.06 
#
# Modification History:
# None
# 
#
#
#
# FUNCTION                                           DESCRIPTION                                                RETURN
# print_html_simple(template_file,out_file,arg_str,p_mode) Take template input file and create html output 
#                                                    file with application data                                 NONE
# print_html(filename)                              Print a html file                                          NONE
# get_print_html_template(id)                        Returns the corresponding html print template file name    qxt_settings.print_html_template[id]
# get_print_html_image(id)                           Returns the corresponding html print image file name       qxt_settings.print_html_image[id]
# get_print_html_output()                            Returns the temporary print html file name (no path)       qxt_settings.print_html_output
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"



####################################################
# FUNCTION process_print_html_cfg_import()
#
# Import/Process cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_print_html_cfg_import(p_cfg_filename)

DEFINE 
  p_cfg_filename  VARCHAR(150),
  ret              SMALLINT,
  local_debug      SMALLINT,
  err_msg          VARCHAR(200)


  LET local_debug = FALSE  --0=off 1=on

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_print_html_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    LET err_msg = "Error in process_print_html_cfg_import()\nConfiguration file name was not specified ! "
    CALL fgl_winmessage("Error in process_print_html_cfg_import()", err_msg, "error")
    RETURN NULL
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[Print-Html]
  LET qxt_settings.print_html_template_unl_filename = configGet(ret, "[PrintHtml]", "print_html_template_unl_filename")
  LET qxt_settings.print_html_image_unl_filename    = configGet(ret, "[PrintHtml]", "print_html_image_unl_filename")
  LET qxt_settings.print_html_output                = configGet(ret, "[PrintHtml]", "print_html_output")


  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_print_html_cfg_filename = ", p_cfg_filename CLIPPED, "###############################"

    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [PrintHtml] Section          x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    
    DISPLAY "configInit() - qxt_settings.print_html_template_unl_filename=",   qxt_settings.print_html_template_unl_filename
    DISPLAY "configInit() - qxt_settings.print_html_image_unl_filename=",      qxt_settings.print_html_image_unl_filename
    DISPLAY "configInit() - qxt_settings.print_html_output=",                  qxt_settings.print_html_output

  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_print_html_cfg_export()
#
# Export print-html cfg data
#
# RETURN ret
###########################################################
FUNCTION process_print_html_cfg_export(p_cfg_filename)
  DEFINE 
    ret             SMALLINT,
    p_cfg_filename VARCHAR(100),
    err_msg         VARCHAR(200)

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_print_html_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    LET err_msg = "Error in process_print_html_cfg_import()\nConfiguration file name was not specified! "
    CALL fgl_winmessage("Error in process_print_html_cfg_import()", err_msg, "error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_cfg_filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[Print-Html]
  #Write to Print-Html section
  CALL configWrite(ret, "[PrintHtml]", "print_html_template_unl_filename",   qxt_settings.print_html_template_unl_filename )
  CALL configWrite(ret, "[PrintHtml]", "print_html_image_unl_filename",      qxt_settings.print_html_image_unl_filename )
  CALL configWrite(ret, "[PrintHtml]", "print_html_output",                  qxt_settings.print_html_output )

END FUNCTION


#########################################################################################
# Upper Level common/shared print-html functions
#########################################################################################

##########################################################################################
# FUNCTION print_html_simple(server_template_file,out_file,arg_str,p_mode)
#
# Take template input file and create html output file with application data
#
# RETURN NONE
##########################################################################################
FUNCTION print_html_simple(server_template_file,out_file,arg_str,p_mode)
  DEFINE 
    line_buffer       VARCHAR(5000),
    file_buffer       VARCHAR(30000),
    read_status       SMALLINT, 
    write_status      SMALLINT,
    arg_str           VARCHAR(32000),
    server_template_file VARCHAR(200),
    out_file          VARCHAR(200),
    err_msg           VARCHAR(200),
    dummy, p_mode     CHAR,   --p_mode a=append n=new
    ret               SMALLINT,
    local_debug       SMALLINT

  LET local_debug = FALSE

  IF out_file IS NULL THEN
    LET out_file = get_server_app_temp_path(get_print_html_output())
  END IF

  IF local_debug THEN
    DISPLAY "print_html_simple() - server_template_file=", server_template_file
    DISPLAY "print_html_simple() - out_file=", out_file
    DISPLAY "print_html_simple() - arg_str=", arg_str
    DISPLAY "print_html_simple() - p_mode=", p_mode
  END IF

  #if the input file does not exist on the server, don't attempt to do anything.. simply return with an error message
  IF NOT fgl_test("e",server_template_file) THEN
    LET err_msg = "File Error in print_html_simple()\n", get_str_tool(762), "\nHtml Print Template File: ", server_template_file
    CALL fgl_winmessage(get_str_tool(30) ,err_msg, "error")
    RETURN NULL
  END IF


  #Open File for Template INPUT
  IF NOT fgl_channel_open_file("stream_in",server_template_file, "r") THEN  --open the input html file and read it
    LET err_msg = get_str_tool(703), "\nError in print_html_simple() Can not open Template Input file\n", trim(server_template_file)
    CALL fgl_winmessage("Error in print_html_simple() " || get_str_tool(703), err_msg, "error")
    RETURN NULL
  END IF


  IF p_mode = "n" THEN  --new document
    LET dummy = "" --delete existing contents
    #CALL fgl_channel_open_file("stream_out", out_file, "w")

    #Open File for Template Output - Write Mode - RAW MODE = TRUE
    IF NOT fgl_channel_open_file("stream_out",out_file, "w",TRUE) THEN  --open the output html file
      LET err_msg = get_str_tool(704), "\nError in print_html_simple() Can not open the 'write' Output Html file\n", trim(out_file)
      CALL fgl_winmessage("Error in print_html_simple() " || get_str_tool(704), err_msg, "error")
      RETURN NULL
    END IF

  ELSE
    #Open File for Template Output - Append Mode - RAW MODE = TRUE
    IF NOT fgl_channel_open_file("stream_out",out_file, "a", TRUE) THEN  --open the output html file
      LET err_msg = get_str_tool(704), "\nError in print_html_simple() Can not open the 'append' Output Html file\n", trim(out_file)
      CALL fgl_winmessage("Error in print_html_simple() " || get_str_tool(704), err_msg, "error")
      RETURN NULL
    END IF

  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream_in","") THEN
    LET err_msg = get_str_tool(706), "\nError in print_html_simple()\nCan not set delimiter for file ", trim(server_template_file)
    CALL fgl_winmessage("Error in print_html_simple()" || get_str_tool(706), err_msg, "error")
    RETURN NULL
  END IF

  IF NOT fgl_channel_set_delimiter("stream_out","") THEN
    LET err_msg = get_str_tool(706), "\nError in print_html_simple()\nCan not set delimiter for file ", trim(out_file)
    CALL fgl_winmessage("Error in print_html_simple()" || get_str_tool(706), err_msg, "error")
    RETURN NULL
  END IF


  #Assign macro strings for template replacement...
  CALL assign_macro_data(arg_str)

  LET line_buffer = ""

  LET read_status = 1
  LET write_status = 1

  WHILE read_status
    CALL read_html_file(server_template_file) 
      RETURNING line_buffer, read_status

    #IF local_debug THEN
    #  DISPLAY "do it line buffer - Import: ", line_buffer
    #END IF


    LET line_buffer = process_html_string(line_buffer)

    #IF local_debug THEN
    #  DISPLAY "do it line buffer - Process: ", line_buffer
    #END IF


    CALL write_html_file(out_file,line_buffer) 
      RETURNING file_buffer, write_status

    #IF local_debug THEN
    #  DISPLAY "do it write_status: ", write_status
    #END IF
  END WHILE

  #Close file handle - for output file
  IF NOT fgl_channel_close("stream_out") THEN
    LET err_msg = get_str_tool(702), "\nError in print_html_simple() Can not close Output file\n", trim(out_file)
    CALL fgl_winmessage("Error in print_html_simple()", err_msg, "error")
    RETURN NULL
  END IF


  #Close file handle - for input file
  IF NOT fgl_channel_close("stream_in") THEN
    LET err_msg = get_str_tool(702), "\nError in print_html_simple() Can not close Template Input file \n", trim(server_template_file)
    CALL fgl_winmessage("Error in print_html_simple()", err_msg, "error")
    RETURN NULL
  END IF

  IF local_debug THEN
    DISPLAY "print_html_simple() - RETURN out_file = ", out_file
  END IF

  RETURN out_file

END function


####################################################################################
# FUNCTION print_html(filename)
#
# Print a html file
#
# RETURN NONE
####################################################################################
FUNCTION print_html(filename)
  DEFINE
    filename   VARCHAR(200),
    inp_char    CHAR,
    loop        SMALLINT,
    local_debug SMALLINT

  LET local_debug = FALSE

  # set help file '10' (10 is used for this qxt library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(10)

  #LET filename = create_print_html_file(filename)
  CALL fgl_window_open("w_print_preview",3,4,get_form_path("f_qxt_print_html_l2"),FALSE)
  CALL fgl_settitle(get_str_tool(601))   

  IF local_debug THEN
    DISPLAY "print_html(filename) - filename=", filename
    DISPLAY "print_html(filename) - get_form_path(\"f_qxt_print_html_l2\")=", get_form_path("f_qxt_print_html_l2")
    LET qxt_tmp_str = get_str_tool(601) CLIPPED, " - ", filename
    CALL fgl_settitle(qxt_tmp_str)  
  END IF

  DISPLAY filename TO bw_print_preview

  WHILE TRUE





    PROMPT "" FOR CHAR inp_char HELP 400
      BEFORE PROMPT
        CALL publish_toolbar("Print",0)
      ON KEY(ACCEPT)  --Print

				#Only LyciaDesktop Client provides direct access to the local system
				IF ui.Interface.getFrontEndName() = "Lyciadesktop.html5" THEN
        	CALL fgl_set_property("gui", "window.winshellexec.verb", "print")
        	CALL winshellexec(filename)
        	# this next call is to simply reset the verb
        	CALL fgl_set_property("gui", "window.winshellexec.verb", "open")
				ELSE
					CALL fgl_winmessage("Browser Client Limitation","To utilize client operating system facilities,\n you need to use LyciaDesktop\nCan not run local programs!","info")
				END IF
        EXIT WHILE
      ON KEY(INTERRUPT)  --Cancel/Close
        LET int_flag = FALSE
        EXIT WHILE
      ON ACTION "CANCEL"  --Cancel/Close
        LET int_flag = FALSE
        EXIT WHILE

    END PROMPT
  END WHILE

  CALL publish_toolbar("Print",1)

  CALL fgl_window_close("w_print_preview")


END FUNCTION


{
##############################################################
# FUNCTION get_print_html_template(id)
#
# Returns the corresponding html print template file name
#
# RETURN qxt_settings.print_html_template[id]
##############################################################

FUNCTION get_print_html_template(id)
  DEFINE
    ID           SMALLINT,
    local_debug  SMALLINT

  LET local_debug = FALSE

  IF id > 8 OR id < 1 THEN
    CALL fgl_winmessage("Error in get_print_html_template()","Calling argument is out of range","error")
    RETURN NULL
  END IF

  IF local_debug THEN
    DISPLAY "get_print_html_template() - id=", id
    DISPLAY "get_print_html_template() - qxt_settings.print_html_template[id]=", qxt_settings.print_html_template[id]
  END IF

  RETURN qxt_settings.print_html_template[id]

END FUNCTION
}


##############################################################
# FUNCTION get_print_html_cfg_filename()
#
# Returns the temporary print html file name (no path)
#
# RETURN qxt_settings.print_html_cfg_filename
##############################################################
FUNCTION get_print_html_cfg_filename()

  RETURN qxt_settings.print_html_cfg_filename
END FUNCTION

##############################################################
# FUNCTION set_print_html_cfg_filename(p_filename
#
# Specify the temporary print html file name (no path)
#
# RETURN NULL
##############################################################
FUNCTION set_print_html_cfg_filename(p_filename)
  DEFINE p_filename  VARCHAR(200)

  LET qxt_settings.print_html_cfg_filename = trim(p_filename)
END FUNCTION


##############################################################
# FUNCTION get_print_html_output()
#
# Returns the temporary print html file name (no path)
#
# RETURN qxt_settings.print_html_output
##############################################################
FUNCTION get_print_html_output()

  RETURN qxt_settings.print_html_output
END FUNCTION

##############################################################
# FUNCTION set_print_html_output()
#
# Specify the temporary print html file name (no path)
#
# RETURN NULL
##############################################################
FUNCTION set_print_html_output(p_filename)
  DEFINE p_filename  VARCHAR(200)

  LET qxt_settings.print_html_output = trim(p_filename)
END FUNCTION



##############################################################
# FUNCTION get_print_html_template_unl_filename()
#
# Returns the temporary print template list unl file name (no path)
#
# RETURN qxt_settings.print_html_template_cfg_filename
##############################################################
FUNCTION get_print_html_template_unl_filename()

  RETURN qxt_settings.print_html_template_unl_filename
END FUNCTION

##############################################################
# FUNCTION set_print_html_template_unl_filename(p_filename)
#
# Specify the temporary print template list unl file name (no path)
#
# RETURN NULL
##############################################################
FUNCTION set_print_html_template_unl_filename(p_filename)
  DEFINE p_filename  VARCHAR(200)

  LET qxt_settings.print_html_template_unl_filename = trim(p_filename)
END FUNCTION



##############################################################
# FUNCTION get_print_html_image_unl_filename()
#
# Returns the temporary print image list unl file name (no path)
#
# RETURN qxt_settings.print_html_image_cfg_filename
##############################################################
FUNCTION get_print_html_image_unl_filename()

  RETURN qxt_settings.print_html_image_unl_filename

END FUNCTION

##############################################################
# FUNCTION set_print_html_image_unl_filename(p_filename)
#
# Specify the temporary print image list unl file name (no path)
#
# RETURN NULL
##############################################################
FUNCTION set_print_html_image_unl_filename(p_filename)
  DEFINE p_filename  VARCHAR(200)

  LET qxt_settings.print_html_image_unl_filename = trim(p_filename)


END FUNCTION


#############################################################################################
# EOF
#############################################################################################
