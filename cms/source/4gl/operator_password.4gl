##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

##################################################
# FUNCTION change_password()
#
# Change password
#
# RETURN NONE
##################################################
FUNCTION change_password()
  DEFINE change_passwd_rec OF t_operator_passwd_rec

  LET int_flag = FALSE
  WHILE TRUE

    IF NOT fgl_window_open("w_password", 5,10, get_form_path("f_password_change_l2"),FALSE)   THEN
      CALL fgl_winmessage("Error","change_password()\nCannot open window w_password","error")
      RETURN
    END IF  

		CALL ui.Interface.setImage("qx://application/icon16/security/password/password-edit.png")
		CALL ui.Interface.setText("Password")
	    
    CALL populate_change_password_form_labels_g()


    INPUT BY NAME change_passwd_rec.* WITHOUT DEFAULTS
      BEFORE INPUT
        CALL publish_toolbar("chg_passw",0)
      AFTER INPUT
        CALL publish_toolbar("chg_passw",1)
    END INPUT
    CALL fgl_window_close("w_password")

    IF change_passwd_rec.passwd1 <> change_passwd_rec.passwd_confirm THEN   ---if the passwords dont match..
      CALL fgl_message_box(get_str(1076))
    ELSE
      EXIT WHILE
    END IF

  END WHILE

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF
  IF change_passwd_rec.passwd1 IS NULL OR change_passwd_rec.passwd1 IS NULL THEN
    #"Change Password" - Error - Could not update Password
    CALL fgl_winmessage(get_Str(1077),get_str(1078),"error")
    RETURN FALSE
  ELSE
    UPDATE operator   ---change the database entry
      SET operator.password = change_passwd_rec.passwd1
      WHERE operator.operator_id = g_operator.operator_id
      # Change Password - Password sucessfully changed
      CALL fgl_winmessage(get_Str(1077),get_str(1079),"info")
      RETURN TRUE
  END IF
END FUNCTION



#######################################################
# FUNCTION populate_change_password_form_labels_t()
#
# Populate change password form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_change_password_form_labels_t()
  DISPLAY get_str(1050) TO lbTitle
  DISPLAY get_str(1051) TO dl_f1
  DISPLAY get_str(1052) TO dl_f2
  #DISPLAY get_str(1053) TO dl_f3
  #DISPLAY get_str(1054) TO dl_f4
  #DISPLAY get_str(1055) TO dl_f5
  #DISPLAY get_str(1056) TO dl_f6
  #DISPLAY get_str(1057) TO dl_f7
  #DISPLAY get_str(1058) TO dl_f8
  DISPLAY get_str(1059) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_change_password_form_labels_g()
#
# Populate change password form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_change_password_form_labels_g()
  DISPLAY get_str(1050) TO lbTitle
  DISPLAY get_str(1051) TO dl_f1
  DISPLAY get_str(1052) TO dl_f2
  #DISPLAY get_str(1053) TO dl_f3
  #DISPLAY get_str(1054) TO dl_f4
  #DISPLAY get_str(1055) TO dl_f5
  #DISPLAY get_str(1056) TO dl_f6
  #DISPLAY get_str(1057) TO dl_f7
  #DISPLAY get_str(1058) TO dl_f8
  DISPLAY get_str(1059) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(1050))  --"Change Password")

END FUNCTION


