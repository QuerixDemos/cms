##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# String intialisation and handling for international support
#
# Created:
# 10.10.06 HH - V3 - Extracted from Guidemo V3 module gd_guidemo.4gl
#
# Modification History:
# None
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# process_string_import(filename)                Import string file                                         NONE
# get_str(id)                                     Get program related multi language string                  trim(qxt_multi_lingual_prog_str[ID].txt_en)
# get_string_tool(id)                             Get lib-tool related multi language string                 trim(qxt_multi_lingual_tool_str[ID].txt_en)
# get_strm(id)                                    Return mono language string from id                        trim(qxt_mono_lang_prog_str[id]) 
# get_strm_tool(id)                               Return mono language string (for tools functions) from id  trim(qxt_mono_lang_prog_str[id]) 
# process_mono_lang_string_import(filename)      Process mono language string                               NONE
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


#################################################################################################
# Init/Config functions
#################################################################################################


###########################################################
# FUNCTION process_string_cfg_import(filename)
#
# Import/Process string cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_string_cfg_import(p_string_cfg_filename)

DEFINE 
  p_string_cfg_filename  VARCHAR(200),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "process_string_cfg_import() - p_string_cfg_filename=", p_string_cfg_filename
  END IF

  IF p_string_cfg_filename IS NULL THEN
    LET p_string_cfg_filename = get_string_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_string_cfg_import() - p_string_cfg_filename=", p_string_cfg_filename
  END IF

  #Add path
  LET p_string_cfg_filename = get_cfg_path(p_string_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_string_cfg_import() - p_string_cfg_filename=", p_string_cfg_filename
  END IF


  IF p_string_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_string_cfg_import()","Error in process_string_cfg_import()\nNo import string_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    EXIT PROGRAM
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_string_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  # [String]
  LET qxt_settings.string_tool_source       = configGetInt(ret, "[String]", "string_tool_source")
  LET qxt_settings.string_tool_filename     = configGet(ret,    "[String]", "string_tool_filename")
  LET qxt_settings.string_tool_table_name   = configGet(ret,    "[String]", "string_tool_table_name")

  LET qxt_settings.string_app_source        = configGetInt(ret, "[String]", "string_app_source")
  LET qxt_settings.string_app_filename      = configGet(ret,    "[String]", "string_app_filename")
  LET qxt_settings.string_app_table_name    = configGet(ret,    "[String]", "string_app_table_name")


  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_icon_cfg_filename = ", p_string_cfg_filename CLIPPED, "###############################"

    #String section
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [String] Section             x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    DISPLAY "configInit() - qxt_settings.string_tool_source=",      qxt_settings.string_tool_source
    DISPLAY "configInit() - qxt_settings.string_tool_filename=",   qxt_settings.string_tool_filename
    DISPLAY "configInit() - qxt_settings.string_tool_table_name=",  qxt_settings.string_tool_table_name

    DISPLAY "configInit() - qxt_settings.string_app_source=",       qxt_settings.string_app_source
    DISPLAY "configInit() - qxt_settings.string_app_filename=",    qxt_settings.string_app_filename
    DISPLAY "configInit() - qxt_settings.string_app_table_name=",   qxt_settings.string_app_table_name

   
  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_string_cfg_export(filename)
#
# Export string cfg data
#
# RETURN ret
###########################################################
FUNCTION process_string_cfg_export(p_string_cfg_filename)
  DEFINE 
    ret                   SMALLINT,
    p_string_cfg_filename VARCHAR(100),
    local_debug           SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_string_cfg_export() - p_string_cfg_filename=", p_string_cfg_filename
  END IF

  IF p_string_cfg_filename IS NULL THEN
    LET p_string_cfg_filename = get_string_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_string_cfg_export() - p_string_cfg_filename=", p_string_cfg_filename
  END IF

  #Add path
  LET p_string_cfg_filename = get_cfg_path(p_string_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_string_cfg_export() - p_string_cfg_filename=", p_string_cfg_filename
  END IF


  IF p_string_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_string_cfg_export()","Error in process_string_cfg_export()\nNo import string_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    RETURN NULL
  END IF




  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_string_cfg_filename)

  #CALL fgl_channel_set_delimiter(filename,"")

  # [String]
  CALL configWrite(ret, "[String]", "string_tool_source",        qxt_settings.string_tool_source )
  CALL configWrite(ret, "[String]", "string_tool_filename",      qxt_settings.string_tool_filename )
  CALL configWrite(ret, "[String]", "string_tool_table_name",    qxt_settings.string_tool_table_name )

  CALL configWrite(ret, "[String]", "string_app_source",         qxt_settings.string_app_source )
  CALL configWrite(ret, "[String]", "string_app_filename",       qxt_settings.string_app_filename )
  CALL configWrite(ret, "[String]", "string_app_table_name",     qxt_settings.string_app_table_name )

  IF local_debug THEN
    DISPLAY "process_string_cfg_export() - qxt_settings.string_tool_source=", qxt_settings.string_tool_source
    DISPLAY "process_string_cfg_export() - qxt_settings.string_tool_filename=", qxt_settings.string_tool_filename
    DISPLAY "process_string_cfg_export() - qxt_settings.string_tool_table_name=", qxt_settings.string_tool_table_name
    DISPLAY "process_string_cfg_export() - qxt_settings.string_app_source=", qxt_settings.string_app_source
    DISPLAY "process_string_cfg_export() - qxt_settings.string_app_filename=", qxt_settings.string_app_filename
    DISPLAY "process_string_cfg_export() - qxt_settings.string_app_table_name=", qxt_settings.string_app_table_name
  END IF


END FUNCTION


######################################################################################
# Data Access Functions
######################################################################################


###########################################################
# FUNCTION get_string_cfg_filename()
#
# Get the string configuration file name
#
# RETURN qxt_settings.string_cfg_filename
###########################################################
FUNCTION get_string_cfg_filename()
  RETURN qxt_settings.string_cfg_filename
END FUNCTION

###########################################################
# FUNCTION set_string_cfg_filename(p_fname)
#
# Set the string library configuration file name
#
# RETURN NONE
###########################################################
FUNCTION set_string_cfg_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.string_cfg_filename = p_fname
END FUNCTION



###########################################################
# FUNCTION get_string_tool_filename()
#
# Return the file name for the tool string data import
#
# RETURN NONE
###########################################################
FUNCTION get_string_tool_filename() 

  RETURN qxt_settings.string_tool_filename

END FUNCTION


###########################################################
# FUNCTION get_string_app_filename()
#
# Return the file name for the tool string data import
#
# RETURN NONE
###########################################################
FUNCTION get_string_app_filename()
  RETURN qxt_settings.string_app_filename
END FUNCTION

###########################################################
# FUNCTION get_string_tool_table_name()
#
# Return the table name for the tool string data
#
# RETURN NONE
###########################################################
FUNCTION get_string_tool_table_name()

  RETURN qxt_settings.string_tool_table_name

END FUNCTION


###########################################################
# FUNCTION set_string_tool_table_name(p_table_name)
#
# Set the table name for the tool string data
#
# RETURN NONE
###########################################################
FUNCTION set_string_tool_table_name(p_table_name)
  DEFINE
    p_table_name  VARCHAR(50)

  LET qxt_settings.string_tool_table_name = p_table_name

END FUNCTION

###########################################################
# FUNCTION get_string_app_table_name()
#
# Return the table name for the application string data
#
# RETURN NONE
###########################################################
FUNCTION get_string_app_table_name()

  RETURN qxt_settings.string_app_table_name
END FUNCTION 


###########################################################
# FUNCTION set_string_app_table_name(p_table_name)
#
# Set the table name for the application  (app) string data
#
# RETURN NONE
###########################################################
FUNCTION set_string_app_table_name(p_table_name)
  DEFINE
    p_table_name  VARCHAR(50)

  LET qxt_settings.string_app_table_name = p_table_name

END FUNCTION


###########################################################
# FUNCTION set_string_tool_source(p_type)
#
# Set the tool string source  1=file to mem 1 = db to mem 2 = db direct
#
# RETURN NONE
###########################################################
FUNCTION set_string_tool_source(p_type)

  DEFINE p_type SMALLINT
  LET qxt_settings.string_tool_source = p_type

END FUNCTION



###########################################################
# FUNCTION set_string_app_source(p_type)
#
# Set the application string source  1=file to mem 1 = db to mem 2 = db direct
#
# RETURN NONE
###########################################################
FUNCTION set_string_app_source(p_type)

  DEFINE p_type SMALLINT
  LET qxt_settings.string_app_source = p_type

END FUNCTION


###########################################################
# FUNCTION set_string_app_source(p_type)
#
# Get the application string source  1=file to mem 1 = db to mem 2 = db direct
#
# RETURN NONE
###########################################################
FUNCTION get_string_app_source()

  RETURN qxt_settings.string_app_source

END FUNCTION
