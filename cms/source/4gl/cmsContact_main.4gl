##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# MAIN
#
# NONE
######################################################
MAIN

	#read arguments, config files and initialise
	CALL prepareModuleStart()
	CALL prepareModuleStartExtended()
	
	#default toolbar buttons (event/key to toolbar button matching)
	CALL publish_toolbar("Global",0)

  # set help file '1' define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(1)
  	if md_lacy > 2 then
		CALL ui.interface.frontcall("html5","styleImport",["qx://application/cmsContact.css"],[])
	end if
	--call apply_theme("theme/zeplin")

	############################################################
	#Launch actual child app
	CALL contact_main(NULL, NULL)
	############################################################

  #Clean applications temporary file on the server
  CALL clean_app_server_temp_files()



END MAIN


