##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage activities
##################################################################################################################
#
# FUNCTION:                                     DESCRIPTION:                                         RETURN:
# activity_main(value)                          Main activity menu                                   NONE
# activity_operator_scroll()                    Displays all activities to the                       ret_activity_id
# (p_activity_id,p_order_field,p_accept_action) corresponding contact                             
# activity_operator_scroll_data_source          Data Source (Cursor) for activity_operator_scroll()  NONE
#  (p_filter,p_order_field)                     
# activity_show(p_activity_id)                  show activity details                                NONE
# activity_close(p_activity_id)                 close activity                                       NONE
# activity_delete(p_activity_id)                delete activity                                      NONE
# activity_view(p_activity_id)                  view an activity                                     NONE
# activity_edit((p_activity_id)                 Edit activity (with window+form)                     NONE
# activity_create(p_contact_id)                 create a new activity                                NONE
# populate_activity_form_combo_boxes()          populates all combo boxes dynamically                NONE
# set_act_default_titlebar()                    set the titlebar text to default                     NONE
# get_activity(p_activity_id)                   get activity record from activity_id                 l_activity.* (RECORD LIKE activity.*)
# get_activity_rec(p_activity_id)               return an activity record (by id)                    l_activity_rec.*
# grid_header_activity_scroll()                 Populate the grid header of the activity scroll grid NONE
# grid_header_activity_scroll                   Populate the grid header of the activity popup grid  NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# FUNCTION activity_main(value)
######################################################
FUNCTION activity_main(value)
  DEFINE value LIKE operator.cont_id

  CALL set_help_id(400)
  CALL activity_operator_scroll(NULL,NULL,2)  --2=edit on accept

END FUNCTION




############################################################
# FUNCTION activity_operator_scroll_data_source(p_filter,p_order_field)
#
# Data Source (Cursor) for activity_operator_scroll()
#
# RETURN NONE
############################################################
FUNCTION activity_operator_scroll_data_source(p_filter,p_order_field,p_ord_dir)

  DEFINE 
    i                  INTEGER,
    p_order_field      VARCHAR(30),
    p_filter           INTEGER,
    #l_operator_cont_id     LIKE operator.operator_id,
    sql_stmt           CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

 # LET l_operator_cont_id = get_operator_contact_id_from_operator_id(g_operator.operator_id)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "activity_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


#CALL fgl_winmessage("get_current_operator_contact_id()",get_current_operator_contact_id(),"info")



  LET sql_stmt = "SELECT activity.activity_id, ",
       	           "company.comp_name, ",
	           "contact.cont_name, ",
                   "operator.name, ",
	           "activity.short_desc, ",
	           "activity_type.atype_name, ",
	           "activity.priority, ",
	           "activity.open_date, ",
                   "activity.close_date ",
                 "FROM   activity, activity_type, contact, OUTER company, OUTER operator ",
                 "WHERE  (activity.operator_id = ? OR  activity.contact_id = ? ) ",
                   "AND  activity.operator_id = operator.operator_id ",
                   "AND  activity.contact_id = contact.cont_id ",
                   "AND  contact.cont_org = company.comp_id ",
                   "AND  activity_type.type_id = activity.act_type "

  CASE p_filter
    WHEN 1
    WHEN 2
      LET sql_stmt = sql_stmt CLIPPED, " AND activity.close_date IS NULL"
    WHEN 3
      LET sql_stmt = sql_stmt CLIPPED, " AND activity.close_date IS NOT NULL"
    OTHERWISE 
      CALL fgl_winmessage("Error in case","activity_operator_scroll_data_source() case","error")
  END CASE

  #LET sql_stmt = sql_stmt CLIPPED, " ORDER BY  activity.priority DESC, activity.open_date DESC "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

#	DISPLAY sql_stmt
	
  PREPARE p_act_arr FROM sql_stmt
  DECLARE c_act_arr CURSOR FOR p_act_arr

END FUNCTION


######################################################
# FUNCTION activity_operator_scroll(p_activity_id,p_order_field,accept_action)
######################################################
FUNCTION activity_operator_scroll(p_activity_id,p_order_field,p_accept_action)
  DEFINE 
    rb_status                     SMALLINT,
    p_filter                      SMALLINT,
    p_accept_action               SMALLINT,
    p_order_field,p_order_field2  VARCHAR(128), 
    i                             SMALLINT,
    p_activity_id                 LIKE activity.activity_id,
    l_operator_cont_id            LIKE operator.operator_id,
    ret_activity_id               LIKE activity.activity_id,
    error_msg                     VARCHAR(200)

  DEFINE l_activity_arr DYNAMIC ARRAY OF t_act_rec2


		CALL ui.Interface.setImage("qx://application/icon16/basic/pin/pin_blue.png")
		CALL ui.Interface.setText("Activity")

  #Open window depending on client
	IF NOT fgl_window_open("w_activity_operator_scroll", 1,1,get_form_path("f_activity_scroll_l2"),FALSE) THEN
      CALL fgl_winmessage("Error","email_write_by_id()\nCould not open window w_activity_operator_scroll","error")
		      
	END IF

    CALL populate_activity_list_form_labels_g()
    CALL populate_activity_form_combo_boxes()
    CALL fgl_settitle(get_str(601))  --"CMS Demo - Display Activity List")
    CALL grid_header_activity_scroll()
    #DISPLAY "!" TO bt_view
    #DISPLAY "!" TO bt_close
    #DISPLAY "!" TO bt_delete
    #DISPLAY "!" TO rb_filter
    #DISPLAY "!" TO bt_done


  #Initialise p_order_field and p_filter in case they are not specified

  IF NOT p_order_field THEN
    LET p_order_field = "activity_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 1  --View is default 
  END IF


  IF p_filter = 0 OR p_filter IS NULL THEN
    LET p_filter = 1
  END IF


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL activity_operator_scroll_data_source(p_filter,p_order_field,get_toggle_switch())
#DISPLAY "p_filter=",p_filter
#DISPLAY "p_order_field=", p_order_field
#DISPLAY "get_toggle_switch()=" , get_toggle_switch()
#DISPLAY "g_operator.operator_id=", g_operator.operator_id
#DISPLAY "l_operator_cont_id=", l_operator_cont_id
    LET i = 1  --initialise row counter i

    FOREACH c_act_arr USING g_operator.operator_id, l_operator_cont_id INTO l_activity_arr[i].*
#DISPLAY "array=", l_activity_arr[i].*
      LET i = i + 1
      #IF i > 100 THEN
			#EXIT FOREACH
  		#END IF
    END FOREACH

    LET i = i - 1  --correct i by -1

		If i > 0 THEN
			CALL l_activity_arr.resize(i)   --remove the last item which has no data
	ELSE
		CALL l_activity_arr.clear() 
	END IF   
		
    #CALL set_count(i)

    IF NOT i THEN  --if no activities exist, return
      CALL fgl_message_box(get_str(2210))
      ##RETURN
    END IF

    DISPLAY ARRAY l_activity_arr TO sa_act_arr.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 402

      BEFORE DISPLAY
        CALL set_help_id(420)
        CALL publish_toolbar("ActivityList",0)


      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET ret_activity_id = l_activity_arr[i].activity_id

        CASE p_accept_action

          WHEN 0  --just return id
            EXIT WHILE

          WHEN 1  --view
            CURRENT WINDOW IS SCREEN
            CALL activity_view(l_activity_arr[i].activity_id)
            CALL fgl_window_current("w_activity_operator_scroll")

          WHEN 2  --edit
            CURRENT WINDOW IS SCREEN
            CALL activity_edit(l_activity_arr[i].activity_id)
            CALL fgl_window_current("w_activity_operator_scroll")

          WHEN 3  --delete
            CALL activity_delete(l_activity_arr[i].activity_id)
            EXIT DISPLAY

          WHEN 4  --print
            CALL fgl_winmessage("Not implemented","activity_operator_scroll()\nActivity Print is not implemented","info")
            EXIT DISPLAY

          WHEN 5
            CALL activity_close(l_activity_arr[i].activity_id)
            EXIT DISPLAY

          OTHERWISE
            LET error_msg = "activity_operator_scroll(p_filter,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("activity_operator_scroll() - 4GL Source Error",error_msg, "error") 
        END CASE

            CALL fgl_window_current("w_activity_operator_scroll")
        
      ON KEY (F5)   ---edit
        LET i = arr_curr()
        CALL activity_edit(l_activity_arr[i].activity_id)
        EXIT DISPLAY

      ON KEY (F6)   ---delete
        LET i = arr_curr()
        CALL activity_delete(l_activity_arr[i].activity_id)
        EXIT DISPLAY

      ON KEY (F7)   ---close
        LET i = arr_curr()
        CALL activity_close(l_activity_arr[i].activity_id)
        EXIT DISPLAY

      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "activity_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY


      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F19)
        LET p_order_field2 = p_order_field

        LET p_order_field = "priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F20)
        LET p_order_field2 = p_order_field

        LET p_order_field = "open_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY

      ON KEY(F21)
        LET p_order_field2 = p_order_field

        LET p_order_field = "close_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF
        EXIT DISPLAY


      ON KEY (F22)  -- Show all
        LET p_filter = 1
        EXIT DISPLAY
      ON KEY (F23)  --open activ
        LET p_filter = 2
        EXIT DISPLAY
      ON KEY (F24)  --close all
        LET p_filter = 3
        EXIT DISPLAY

      AFTER DISPLAY
        CALL publish_toolbar("ActivityList",1)


    END DISPLAY

    IF int_flag THEN
      LET int_flag = FALSE
      LET ret_activity_id = p_activity_id
      EXIT WHILE
    ELSE
      LET i = arr_curr()
      LET ret_activity_id = l_activity_arr[i].activity_id
    END IF

  END WHILE

  CALL fgl_window_close("w_activity_operator_scroll")
  RETURN ret_activity_id


END FUNCTION


######################################################
# FUNCTION activity_edit((p_activity_id)
#
# Edit activity (with window+form)
#
# RETURN NONE
######################################################
# display activity detail
FUNCTION activity_edit(p_activity_id)
  DEFINE 
    p_activity_id  LIKE activity.activity_id,
    l_activity_rec OF t_act_arr3,
    local_debug    SMALLINT

  LET local_debug = FALSE  --0=off 1=on
  LOCATE l_activity_rec.long_desc IN MEMORY


  SELECT activity.activity_id,
         activity.open_date,
         activity.close_date,
	 contact.cont_name, 
	 activity.priority,
	 company.comp_name,
	 activity_type.atype_name,
	 activity.short_desc,
	 activity.long_desc,
         activity.operator_id
    INTO l_activity_rec.*
    FROM activity, activity_type, contact, OUTER company
   WHERE activity.activity_id = p_activity_id
     AND activity.contact_id = contact.cont_id
     AND contact.cont_org = company.comp_id
     AND activity_type.type_id = activity.act_type

  IF sqlca.sqlcode THEN
    RETURN
  END IF

    CALL fgl_window_open("w_act_edit", 2,2, get_form_path("f_activity_edit_l2"),FALSE) 
    CALL populate_activity_details_form_labels_g()
    CALL fgl_settitle(get_str(630))  --"Edit Activity")
    CALL activity_type_combo_list("atype_name")

  DISPLAY BY NAME l_activity_rec.activity_id 
  DISPLAY BY NAME l_activity_rec.open_date 
  DISPLAY BY NAME l_activity_rec.close_date
  DISPLAY BY NAME l_activity_rec.cont_name 
  DISPLAY BY NAME l_activity_rec.priority
  DISPLAY BY NAME l_activity_rec.comp_name
  DISPLAY BY NAME l_activity_rec.atype_name
  DISPLAY BY NAME l_activity_rec.short_desc
  DISPLAY BY NAME l_activity_rec.long_desc
  DISPLAY get_operator_name(l_activity_rec.operator_id) TO a_owner

  IF local_debug THEN  
    DISPLAY "activity_edit() owner:",get_operator_name(l_activity_rec.operator_id)
    DISPLAY "activity_edit() l_activity_rec.activity_id :",l_activity_rec.activity_id 
    DISPLAY "activity_edit() l_activity_rec.open_date :",l_activity_rec.open_date 
    DISPLAY "activity_edit() l_activity_rec.cont_name :",l_activity_rec.cont_name 
    DISPLAY "activity_edit() l_activity_rec.priority :",l_activity_rec.priority 
    DISPLAY "activity_edit() l_activity_rec.comp_name :",l_activity_rec.comp_name 
    DISPLAY "activity_edit() l_activity_rec.atype_name :",l_activity_rec.atype_name 
    DISPLAY "activity_edit() l_activity_rec.short_desc :",l_activity_rec.short_desc 
    DISPLAY "activity_edit() l_activity_rec.long_desc :",l_activity_rec.long_desc 
    DISPLAY "activity_edit() p_activity_id :",p_activity_id
  END IF


  INPUT BY NAME  
    l_activity_rec.close_date, 
    l_activity_rec.priority,  
    l_activity_rec.short_desc,
    l_activity_rec.long_desc

    WITHOUT DEFAULTS HELP 403
--alpr Commented because of absence of any buttons for accept or cancel input
--    BEFORE INPUT
--      CALL hide_dialog_navigation_toolbar(0)

    AFTER INPUT

      IF int_flag THEN  --cancel
        LET int_flag = FALSE
        #CLOSE WINDOW w_act_edit

        FREE l_activity_rec.long_desc
  
      ELSE
        IF l_activity_rec.short_desc IS NULL THEN
          CALL fgl_winmessage(get_str(48),get_str(708),"error")
          CONTINUE INPUT
        END IF

        UPDATE activity SET
	  close_date = l_activity_rec.close_date,
	  short_desc = l_activity_rec.short_desc,
	  long_desc = l_activity_rec.long_desc,
	  priority = l_activity_rec.priority

        WHERE activity.activity_id = p_activity_id

      END IF
  END INPUT

  CALL fgl_window_close("w_act_edit")


  FREE l_activity_rec.long_desc

END FUNCTION


######################################################
# FUNCTION activity_show(p_activity_id)
######################################################
# display activity detail
FUNCTION activity_show(p_activity_id)
  DEFINE 
    p_activity_id  LIKE activity.activity_id,
    l_activity_rec OF t_act_arr4

  LOCATE l_activity_rec.long_desc IN MEMORY


  SELECT activity.activity_id,
         activity.open_date,
         activity.close_date,
	 contact.cont_name, 
	 activity.priority,
	 company.comp_name,
	 activity_type.atype_name,
	 activity.short_desc,
	 activity.long_desc
    INTO l_activity_rec.*
    FROM activity, activity_type, contact, OUTER company
   WHERE activity.activity_id = p_activity_id
     AND activity.contact_id = contact.cont_id
     AND contact.cont_org = company.comp_id
     AND activity_type.type_id = activity.act_type

  IF sqlca.sqlcode THEN
    RETURN
  END IF


  CALL fgl_window_open("w_act_show", 2,2, get_form_path("f_activity_detail_l2"),FALSE) 

  CALL fgl_settitle(get_str(670))  --"CMS Demo - Show Activity Details")

  DISPLAY BY NAME l_activity_rec.* 

  CALL fgl_getkey()

  CALL fgl_window_close("w_act_show")

  FREE l_activity_rec.long_desc

END FUNCTION



######################################################
# FUNCTION activity_close(p_activity_id)
#
# Close an activity record (set a close date)
#
# RETURN NONE
######################################################
FUNCTION activity_close(p_activity_id)
  DEFINE p_activity_id LIKE activity.activity_id

  UPDATE activity
     SET activity.close_date = TODAY 
   WHERE activity.activity_id = p_activity_id

END FUNCTION


######################################################
# FUNCTION activity_open(p_activity_id)
#
# Open an activity record (remove close date)
#
# RETURN NONE
######################################################
FUNCTION activity_open(p_activity_id)
  DEFINE p_activity_id LIKE activity.activity_id

  UPDATE activity
     SET activity.close_date = "" 
   WHERE activity.activity_id = p_activity_id

END FUNCTION


######################################################
# FUNCTION activity_delete(p_activity_id)
#
# Delete an activity record
#
# RETURN NONE
######################################################
FUNCTION activity_delete(p_activity_id)
   DEFINE p_activity_id LIKE activity.activity_id
   #Do you really want to delete this activity ?
   IF yes_no(get_str(817),get_str(709)) THEN
     DELETE FROM activity WHERE activity.activity_id = p_activity_id
     CALL fgl_winmessage("Deleted","Activity Deleted","info")
     RETURN TRUE
   ELSE
     RETURN FALSE
   END IF

END FUNCTION





######################################################
# FUNCTION activity_view(p_activity_id)
######################################################
FUNCTION activity_view(p_activity_id)
  DEFINE 
    p_activity_id  LIKE activity.activity_id,
    local_debug    SMALLINT, 
    inp_char       CHAR,
    l_activity_rec OF t_act_rec5 
{
      RECORD
        open_date  LIKE activity.open_date,
        name       LIKE operator.name,
        priority   LIKE activity.priority,
        cont_name  LIKE contact.cont_name,
        comp_name  LIKE company.comp_name,
        atype_name LIKE activity_type.atype_name,
        short_desc LIKE activity.short_desc,
        long_desc  LIKE activity.long_desc
      END RECORD
}
  LET local_debug = FALSE  --0=off 1=on

  CALL get_activity_rec(p_activity_id) RETURNING l_activity_rec.*


  IF local_debug THEN
    DISPLAY "activity_view() - p_activity_id =", p_activity_id
    DISPLAY "activity_view() - l_activity_rec.open_date =", l_activity_rec.open_date
    DISPLAY "activity_view() - l_activity_rec.name =", l_activity_rec.name
    DISPLAY "activity_view() - l_activity_rec.cont_name =", l_activity_rec.cont_name
    DISPLAY "activity_view() - l_activity_rec.short_desc =", l_activity_rec.short_desc
  END IF


    CALL fgl_window_open("w_activity_view", 3,3, get_form_path("f_activity_view_l2"),FALSE)
      CALL fgl_settitle(get_str(670))  --"CMS-Demo: Activity View")
      DISPLAY "!" TO bt_done


  DISPLAY get_str(670) TO lbTitle   --"View Activity Details"

  DISPLAY BY NAME l_activity_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_activity_view")

END FUNCTION


######################################################
# FUNCTION activity_create(p_contact_id)
######################################################
FUNCTION activity_create(p_contact_id)
  DEFINE 
    p_contact_id LIKE contact.cont_id,
    l_activity RECORD LIKE activity.*,
    l_activity_temp_inp_rec OF t_act_rec5

  IF  NOT contact_id_count(p_contact_id) THEN
    LET tmp_str = get_str(701), "\n", get_str(702)
    CALL fgl_winmessage(get_str(700),tmp_str,"error")
    RETURN
  END IF


    CALL fgl_window_open("w_new_activity", 3, 3,get_form_path("f_activity_new_l2"),FALSE)
    CALL populate_activity_form_combo_boxes()
    CALL fgl_settitle(get_str(602))  --"CMS - Create new activity")


  LET int_flag = FALSE
  LET l_activity_temp_inp_rec.priority = 5
  LET l_activity_temp_inp_rec.name = g_operator.name
  LET l_activity_temp_inp_rec.open_date = TODAY
  LET l_activity_temp_inp_rec.cont_name = get_contact_name(p_contact_id)
  LET l_activity_temp_inp_rec.comp_name = get_contact_comp_name(p_contact_id)

  LOCATE l_activity.long_desc IN MEMORY
  LOCATE l_activity_temp_inp_rec.long_desc IN MEMORY

  INPUT BY NAME l_activity_temp_inp_rec.* WITHOUT DEFAULTS  HELP 401

    BEFORE INPUT
      CALL publish_toolbar("ActivityNew",0)
      CALL populate_activity_form_combo_boxes()

    ON KEY(F10)  

      IF INFIELD (atype_name) THEN  --Lookup activity type
        CALL fgl_dialog_update_data()
        #display get_activity_type_id(activity_type_popup(get_activity_type_id(l_activity_temp_inp_rec.atype_name),NULL,0))
        #LET l_activity_temp_inp_rec.atype_name = get_activity_type_id(activity_type_popup(get_activity_type_id(l_activity_temp_inp_rec.atype_name),NULL,0))
 	LET l_activity_temp_inp_rec.atype_name = get_activity_type_name(activity_type_popup(get_activity_type_id(l_activity_temp_inp_rec.atype_name),NULL,0))
        #LET l_activity_temp_inp_rec.atype_name = get_activity_type_name(l_activity_temp_inp_rec.act_type)
        DISPLAY BY NAME l_activity_temp_inp_rec.atype_name 

     END IF


    BEFORE FIELD atype_name
      IF NOT fgl_fglgui() THEN  --text mode client only
        IF l_activity_temp_inp_rec.atype_name IS NULL THEN
          LET l_activity_temp_inp_rec.atype_name = get_activity_type_id(activity_type_popup(get_activity_type_id(l_activity_temp_inp_rec.atype_name),NULL,0))
          #LET l_activity_temp_inp_rec.atype_name = get_activity_type_name(l_activity_temp_inp_rec.act_type)
          DISPLAY BY NAME l_activity_temp_inp_rec.atype_name 

  	  #LET l_activity_temp_inp_rec.act_type = activity_type_popup(l_activity_temp_inp_rec.act_type,NULL,0)
          #LET l_activity_temp_inp_rec.atype_name = get_activity_type_name(l_activity_temp_inp_rec.act_type)
          DISPLAY BY NAME l_activity_temp_inp_rec.atype_name 
          NEXT FIELD NEXT
        END IF
      END IF

    #AFTER FIELD atype_name
      #LET l_activity_temp_inp_rec.act_type = get_activity_type_id(l_activity_temp_inp_rec.atype_name) 


    AFTER INPUT

    IF NOT int_flag THEN
      IF l_activity_temp_inp_rec.open_date = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.open_date","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.name = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.name","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.priority < 1 OR l_activity_temp_inp_rec.priority > 10 THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.priority","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.cont_name = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.cont_name","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.comp_name = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.comp_name","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.atype_name = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.atype_name","empty","info")
        CONTINUE INPUT
      END IF 
      IF l_activity_temp_inp_rec.short_desc = "" THEN
        CALL fgl_winmessage("l_activity_temp_inp_rec.short_desc","empty","info")
        CONTINUE INPUT
      END IF 
      #IF l_activity_temp_inp_rec.long_desc = "" THEN
      #  CALL fgl_winmessage("l_activity_temp_inp_rec.long_desc","empty","info")
      #END IF 
      #IF l_activity_temp_inp_rec.act_type < 1 OR  l_activity_temp_inp_rec.act_type > 3  THEN
      #  CALL fgl_winmessage("l_activity_temp_inp_rec.act_type","empty","info")
      #  CONTINUE INPUT
      #END IF 
    ELSE
      --Confirm, if operator wants to abort record creation
      IF NOT yes_no(get_str(703),get_str(704)) THEN
        CONTINUE INPUT
      END IF

    END IF

  END INPUT


  IF NOT int_flag THEN
    LET l_activity.contact_id = p_contact_id
    LET l_activity.open_date = l_activity_temp_inp_rec.open_date
    LET l_activity.priority = l_activity_temp_inp_rec.priority
    LET l_activity.short_desc = l_activity_temp_inp_rec.short_desc
    LET l_activity.long_desc = l_activity_temp_inp_rec.long_desc
    LET l_activity.activity_id = 0    --- SERIAL - use 0 to allow DB to assign
    LET l_activity.operator_id = g_operator.operator_id
    LET l_activity.a_owner = g_operator.operator_id -- false condition, but hey
    LET l_activity.act_type = get_activity_type_id(l_activity_temp_inp_rec.atype_name)
    LET l_activity.comp_id = get_comp_id(l_activity_temp_inp_rec.comp_name)

    INSERT INTO activity VALUES(l_activity.*)

    IF sqlca.sqlcode THEN
      #Activity creation failed
      CALL fgl_winmessage(get_str(602),get_str(705),"stop")
    ELSE
      #New Activity record successfully created!
      CALL fgl_winmessage(get_str(602),get_str(706),"info")
    END IF
  ELSE
    #Activity creation cancelled at operator request
    LET int_flag = FALSE
    CALL fgl_winmessage(get_str(602),get_str(707),"info")
    CLEAR FORM
  END IF

  FREE l_activity_temp_inp_rec.long_desc
  FREE l_activity.long_desc

  CALL fgl_window_close("w_new_activity")

END FUNCTION



#############EOF#############


