##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the dde_font table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_qxt_dde_font(p_language_id)  get dde_font id from p_language_id        l_dde_font
# get_language_id(category_id)    Get language_id from p_dde_font        l_language_id
# get_dde_font_rec(p_dde_font)   Get the dde_font record from p_dde_font  l_dde_font.*
# dde_font_popup_data_source()               Data Source (cursor) for dde_font_popup              NONE
# dde_font_popup                             dde_font selection window                            p_dde_font
# (p_dde_font,p_order_field,p_accept_action)
# dde_font_combo_list(cb_field_name)         Populates dde_font combo list from db                NONE
# dde_font_create()                          Create a new dde_font record                         NULL
# dde_font_edit(p_dde_font)      Edit dde_font record                                 NONE
# dde_font_input(p_dde_font_rec)    Input dde_font details (edit/create)                 l_dde_font.*
# dde_font_delete(p_dde_font)    Delete a dde_font record                             NONE
# dde_font_view(p_dde_font)      View dde_font record by ID in window-form            NONE
# dde_font_view_by_rec(p_dde_font_rec) View dde_font record in window-form               NONE
# get_qxt_dde_font_from_language_id(p_language_id)          Get the dde_font from a file                      l_dde_font
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# dde_font_id_count(p_dde_font)  tests if a record with this dde_font already exists r_count
# copy_dde_font_record_to_form_record        Copy normal dde_font record data to type dde_font_form_rec   l_dde_font_form_rec.*
# (p_dde_font_rec)
# copy_dde_font_form_record_to_record        Copy type dde_font_form_rec to normal dde_font record data   l_dde_font_rec.*
# (p_dde_font_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_dde_font_scroll()              Populate dde_font grid headers                       NONE
# populate_dde_font_form_labels_g()          Populate dde_font form labels for gui                NONE
# populate_dde_font_form_labels_t()          Populate dde_font form labels for text               NONE
# populate_dde_font_form_edit_labels_g()     Populate dde_font form edit labels for gui           NONE
# populate_dde_font_form_edit_labels_t()     Populate dde_font form edit labels for text          NONE
# populate_dde_font_form_view_labels_g()     Populate dde_font form view labels for gui           NONE
# populate_dde_font_form_view_labels_t()     Populate dde_font form view labels for text          NONE
# populate_dde_font_form_create_labels_g()   Populate dde_font form create labels for gui         NONE
# populate_dde_font_form_create_labels_t()   Populate dde_font form create labels for text        NONE
# populate_dde_font_list_form_labels_g()     Populate dde_font list form labels for gui           NONE
# populate_dde_font_list_form_labels_t()     Populate dde_font list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_dde_font_manager_globals.4gl"


############################################################################################
# Data Access Functions
############################################################################################


#########################################################
# FUNCTION get_dde_font_rec(p_dde_font_id)
#
# get dde_font record from p_dde_font
#
# RETURN l_dde_font_rec.*
#########################################################
FUNCTION get_dde_font_rec(p_dde_font_id)
  DEFINE 
    p_dde_font_id               LIKE qxt_dde_font.dde_font_id,
    l_dde_font_rec              RECORD LIKE qxt_dde_font.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_dde_font_rec() - p_dde_font_id = ", p_dde_font_id
  END IF

    SELECT *
      INTO l_dde_font_rec.*
      FROM qxt_dde_font
      WHERE qxt_dde.dde_font_id = p_dde_font_id

  RETURN l_dde_font_rec.*
END FUNCTION



########################################################################################################
# Combo list and selection list-management functions
########################################################################################################

{
###################################################################################
# FUNCTION dde_font_combo_list(p_cb_field_name)
#
# Populates dde_font_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION dde_font_id_combo_list(p_cb_field_name)
  DEFINE 
    p_cb_field_name           VARCHAR(50),   --form field name for the country combo list field
    l_dde_font_id             DYNAMIC ARRAY OF LIKE qxt_dde_font.dde_font_id,
    row_count                 INTEGER,
    current_row               INTEGER,
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "dde_font_combo_list() - p_cb_field_name=", p_cb_field_name
  END IF

  DECLARE c_dde_font_scroll2 CURSOR FOR 
    SELECT qxt_dde_font.dde_font_id
      FROM qxt_dde_font
      ORDER BY dde_font_id ASC


  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_dde_font_scroll2 INTO l_dde_font_id[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_dde_font_id[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_dde_font_id[row_count] CLIPPED, ")"
      DISPLAY "dde_font_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION

}

######################################################
# FUNCTION dde_font_popup_data_source(p_dde_font_id,p_ord_dir)
#
# Data Source (cursor) for the dde font manager popup (display array)
#
# RETURN NONE
######################################################
FUNCTION dde_font_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    #p_dde_font_id       LIKE qxt_dde_font.dde_font_id,
    p_order_field       VARCHAR(128),
    p_ord_dir           SMALLINT,
    sql_stmt            CHAR(2048),
    l_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "dde_font_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "dde_font_popup_data_source() - p_ord_dir=", p_ord_dir
  ENd IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "dde_font_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET l_ord_dir_str = "DESC"
  ELSE
    LET l_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "* ",
                 "FROM qxt_dde_font "
                    

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", l_ord_dir_str CLIPPED,  " "
  END IF

  IF local_debug THEN
    DISPLAY "dde_font_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_dde_font FROM sql_stmt
  DECLARE c_dde_font CURSOR FOR p_dde_font

END FUNCTION


######################################################
# FUNCTION dde_font_popup(p_dde_font,p_order_field,p_accept_action)
#
# dde_font selection window
#
# RETURN p_dde_font
######################################################
FUNCTION dde_font_popup(p_dde_font_id,p_order_field,p_accept_action)
  DEFINE 
    p_dde_font_id           LIKE qxt_dde_font.dde_font_id,  --default return value if user cancels
    l_dde_font_arr            DYNAMIC ARRAY OF t_qxt_dde_font_form_rec,    --RECORD LIKE qxt_dde_font.*,  
    i                                               INTEGER,
    p_accept_action                                 SMALLINT,
    err_msg                                         VARCHAR(240),
    p_order_field,p_order_field2                    VARCHAR(128), 
    l_dde_font_id           LIKE qxt_dde_font.dde_font_id,
    l_language_id                                   LIKE qxt_language.language_id,
    current_row                                     SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                                     SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1


  IF local_debug THEN
    DISPLAY "dde_font_popup() - p_dde_font_id=",p_dde_font_id
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_dde_font_scroll", 2, 8, get_form_path("f_qxt_dde_font_scroll_g"),FALSE) 
    CALL populate_dde_font_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_dde_font_scroll", 2, 8, get_form_path("f_qxt_dde_font_scroll_t"),FALSE) 
    CALL populate_dde_font_list_form_labels_t()
  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_dde_font

  #IF i < 1 THEN
    #CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      #CALL dde_font_create()
    #END IF
  #END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL dde_font_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_dde_font INTO l_dde_font_arr[i].*
      IF local_debug THEN
        DISPLAY "dde_font_popup() - i=",i
        DISPLAY "dde_font_popup() - l_dde_font_arr[i].dde_font_id=",l_dde_font_arr[i].dde_font_id
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 100 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		IF i > 0 THEN
			CALL l_dde_font_arr.resize(i) --correct the last element of the dynamic array
		END IF
		
    IF local_debug THEN
      DISPLAY "dde_font_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_dde_font_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "dde_font_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_dde_font_arr TO sc_dde_font.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
      BEFORE DISPLAY
        CALL publish_toolbar("DDEFontManagerList",0)


        CALL fgl_dialog_setcurrline(1,current_row)

      BEFORE ROW
        LET i = arr_curr()
        LET l_dde_font_id = l_dde_font_arr[i].dde_font_id

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET l_dde_font_id = l_dde_font_arr[i].dde_font_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL dde_font_view(l_dde_font_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL dde_font_edit(l_dde_font_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL dde_font_delete(l_dde_font_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL dde_font_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","dde_font_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "dde_font_popup(p_dde_font,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("dde_font_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL dde_font_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL dde_font_edit(l_dde_font_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL dde_font_delete(l_dde_font_id)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "dde_font_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "font_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "font_bold"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "font_size"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "strike_through"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "super_script"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "lower_script"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F20)
        LET p_order_field2 = p_order_field
        LET p_order_field = "option_1"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F21)
        LET p_order_field2 = p_order_field
        LET p_order_field = "option_2"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F22)
        LET p_order_field2 = p_order_field
        LET p_order_field = "option_3"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F23)
        LET p_order_field2 = p_order_field
        LET p_order_field = "font_color"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

  
    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_dde_font_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_dde_font_id
  ELSE 
    RETURN l_dde_font_id
  END IF

END FUNCTION





##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION dde_font_create()
#
# Create a new dde_font record
#
# RETURN NULL
######################################################
FUNCTION dde_font_create()
  DEFINE 
    l_dde_font_rec RECORD LIKE qxt_dde_font.*,
    local_debug     SMALLINT

  LET local_debug = FALSE


#select max column from table
#informix guide to sql

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_g"), TRUE) 
    CALL populate_dde_font_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_t"), TRUE) 
    CALL populate_dde_font_form_create_labels_t()
  END IF



  LET int_flag = FALSE
  
  #Initialise some variables

  IF local_debug THEN
    DISPLAY "dde_font_create() - l_dde_font.dde_font_id=", l_dde_font_rec.dde_font_id
  END IF

  # CALL the INPUT
  CALL dde_font_input(l_dde_font_rec.*)
    RETURNING l_dde_font_rec.*

  CALL fgl_window_close("w_dde_font")

  IF NOT int_flag THEN
    INSERT INTO qxt_dde_font VALUES (
      l_dde_font_rec.dde_font_id,
      l_dde_font_rec.font_name, 
      l_dde_font_rec.font_bold,      
      l_dde_font_rec.font_size,           
      l_dde_font_rec.strike_through,       
      l_dde_font_rec.super_script,          
      l_dde_font_rec.lower_script,         
      l_dde_font_rec.option_1,              
      l_dde_font_rec.option_2,            
      l_dde_font_rec.option_3,             
      l_dde_font_rec.font_color            
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_dde_font_rec.dde_font_id
    END IF

    #DISPLAY sqlca.sqlerrd[2]
  
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION dde_font_edit(p_dde_font)
#
# Edit dde_font record
#
# RETURN NONE
#################################################
FUNCTION dde_font_edit(p_dde_font_id)
  DEFINE 
    p_dde_font_id       LIKE qxt_dde_font.dde_font_id,
    l_key1_dde_font_id  LIKE qxt_dde_font.dde_font_id,

    l_dde_font_rec        RECORD LIKE qxt_dde_font.*,
    local_debug                                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "dde_font_edit() - Function Entry Point"
    DISPLAY "dde_font_edit() - p_dde_font_id=", p_dde_font_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_dde_font_id = p_dde_font_id

  #Get the record (by id)
  CALL get_dde_font_rec(p_dde_font_id) RETURNING l_dde_font_rec.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_g"), TRUE) 
    CALL populate_dde_font_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_t"), TRUE) 
    CALL populate_dde_font_form_edit_labels_t()
  END IF


  #Call the INPUT
  CALL dde_font_input(l_dde_font_rec.*) 
    RETURNING l_dde_font_rec.*

    IF local_debug THEN
      DISPLAY "dde_font_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF


  CALL fgl_window_close("w_dde_font")

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "dde_font_edit() - l_dde_font_rec.dde_font=",l_dde_font_rec.dde_font_id
      DISPLAY "dde_font_edit() - l_key1_dde_font=",l_key1_dde_font_id

    END IF

    UPDATE  qxt_dde_font
      SET   dde_font_id    = l_dde_font_rec.dde_font_id,
            font_name      = l_dde_font_rec.font_name, 
            font_bold      = l_dde_font_rec.font_bold,      
            font_size      = l_dde_font_rec.font_size,           
            strike_through = l_dde_font_rec.strike_through,       
            super_script   = l_dde_font_rec.super_script,          
            lower_script   = l_dde_font_rec.lower_script,         
            option_1       = l_dde_font_rec.option_1,              
            option_2       = l_dde_font_rec.option_2,            
            option_3       = l_dde_font_rec.option_3,             
            font_color     = l_dde_font_rec.font_color  

      WHERE l_dde_font_rec.dde_font_id = l_key1_dde_font_id


    IF local_debug THEN
      DISPLAY "dde_font_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(825),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(825),NULL)
      RETURN l_dde_font_rec.dde_font_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(825),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION dde_font_input(p_dde_font_rec)
#
# Input dde_font details (edit/create)
#
# RETURN l_dde_font.*
#################################################
FUNCTION dde_font_input(p_dde_font_rec)
  DEFINE 
    p_dde_font_rec            RECORD LIKE qxt_dde_font.*,
    l_dde_font_form_rec       OF t_qxt_dde_font_form_rec,
    l_orignal_dde_font_id     LIKE qxt_dde_font.dde_font_id,
    local_debug               SMALLINT,
    tmp_str                   VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "dde_font_input() - p_dde_font_rec.dde_font_id=",p_dde_font_rec.dde_font_id
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_dde_font_id = p_dde_font_rec.dde_font_id

  #copy record data to form_record format record
  CALL copy_dde_font_record_to_form_record(p_dde_font_rec.*) 
    RETURNING l_dde_font_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_dde_font_form_rec.* WITHOUT DEFAULTS HELP 1

    AFTER FIELD dde_font_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(826), l_dde_font_form_rec.dde_font_id,NULL,TRUE)  THEN
        NEXT FIELD dde_font
      END IF



    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #dde_font must not be empty
        IF NOT validate_field_value_numeric_exists(get_str_tool(826), l_dde_font_form_rec.dde_font_id,NULL,TRUE)  THEN
          NEXT FIELD dde_font
          CONTINUE INPUT
        END IF


      #The dde_font - Primary KEY
      IF dde_font_id_count(l_dde_font_form_rec.dde_font_id) THEN
        #Constraint only exists for newly created records (not modify)
       IF l_orignal_dde_font_id IS NULL OR  --it is not an edit operation
          l_orignal_dde_font_id <> l_dde_font_form_rec.dde_font_id THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(826),NULL)
          NEXT FIELD dde_font_id
          CONTINUE INPUT
        END IF
      END IF

  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "dde_font_input() - l_dde_font_form_rec.dde_font_id=",l_dde_font_form_rec.dde_font_id
  END IF


  #Copy the form record data to a normal dde_font record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_dde_font_form_record_to_record(l_dde_font_form_rec.*) RETURNING p_dde_font_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "dde_font_input() - p_dde_font_rec.dde_font_id=",p_dde_font_rec.dde_font_id
  END IF

  RETURN p_dde_font_rec.*

END FUNCTION


#################################################
# FUNCTION dde_font_delete(p_dde_font)
#
# Delete a dde_font record
#
# RETURN NONE
#################################################
FUNCTION dde_font_delete(p_dde_font_id)
  DEFINE 
    p_dde_font_id       LIKE qxt_dde_font.dde_font_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(825), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_dde_font 
        WHERE dde_font_id = p_dde_font_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION dde_font_view(p_dde_font)
#
# View dde_font record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION dde_font_view(p_dde_font_id)
  DEFINE 
    p_dde_font_id        LIKE qxt_dde_font.dde_font_id,
    l_dde_font_rec       RECORD LIKE qxt_dde_font.*


  CALL get_dde_font_rec(p_dde_font_id) RETURNING l_dde_font_rec.*
  CALL dde_font_view_by_rec(l_dde_font_rec.*)

END FUNCTION


#################################################
# FUNCTION dde_font_view_by_rec(p_dde_font_rec)
#
# View dde_font record in window-form
#
# RETURN NONE
#################################################
FUNCTION dde_font_view_by_rec(p_dde_font_rec)
  DEFINE 
    p_dde_font_rec     RECORD LIKE qxt_dde_font.*,
    inp_char           CHAR,
    tmp_str            VARCHAR(250)

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_g"), TRUE) 
    CALL populate_dde_font_form_view_labels_g()
  ELSE
    CALL fgl_window_open("w_dde_font", 3, 3, get_form_path("f_qxt_dde_font_det_t"), TRUE) 
    CALL populate_dde_font_form_view_labels_t()
  END IF

  DISPLAY BY NAME p_dde_font_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_dde_font")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################



####################################################
# FUNCTION dde_font_id_count(p_dde_font_id)
#
# tests if a record with this dde_font_id already exists
#
# RETURN r_count
####################################################
FUNCTION dde_font_id_count(p_dde_font_id)
  DEFINE
    p_dde_font_id  LIKE qxt_dde_font.dde_font_id,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_dde_font
      WHERE qxt_dde_font.dde_font_id = p_dde_font_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION

###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_dde_font_record_to_form_record(p_dde_font_rec)  
#
# Copy normal dde_font record data to type dde_font_form_rec
#
# RETURN l_dde_font_form_rec.*
######################################################
FUNCTION copy_dde_font_record_to_form_record(p_dde_font_rec)  
  DEFINE
    p_dde_font_rec       RECORD LIKE qxt_dde_font.*,
    l_dde_font_form_rec  OF t_qxt_dde_font_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_dde_font_form_rec.dde_font_id     = p_dde_font_rec.dde_font_id
  LET l_dde_font_form_rec.font_name       = p_dde_font_rec.font_name
  LET l_dde_font_form_rec.font_bold       = p_dde_font_rec.font_bold
  LET l_dde_font_form_rec.font_size       = p_dde_font_rec.font_size
  LET l_dde_font_form_rec.strike_through  = p_dde_font_rec.strike_through
  LET l_dde_font_form_rec.super_script    = p_dde_font_rec.super_script
  LET l_dde_font_form_rec.lower_script    = p_dde_font_rec.lower_script
  LET l_dde_font_form_rec.option_1        = p_dde_font_rec.option_1
  LET l_dde_font_form_rec.option_2        = p_dde_font_rec.option_2
  LET l_dde_font_form_rec.option_3        = p_dde_font_rec.option_3
  LET l_dde_font_form_rec.font_color      = p_dde_font_rec.font_color


  IF local_debug THEN
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.dde_font_id=",    p_dde_font_rec.dde_font_id
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.font_name=",      p_dde_font_rec.font_name
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.font_bold=",      p_dde_font_rec.font_bold
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.font_size=",      p_dde_font_rec.font_size
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.strike_through=", p_dde_font_rec.strike_through
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.super_script=",   p_dde_font_rec.super_script
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.lower_script=",   p_dde_font_rec.lower_script
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.option_1=",       p_dde_font_rec.option_1
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.option_2=",       p_dde_font_rec.option_2
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.option_3=",       p_dde_font_rec.option_3
    DISPLAY "copy_dde_font_record_to_form_record() - p_dde_font_rec.font_color=",     p_dde_font_rec.font_color


   
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.dde_font_id=",    l_dde_font_form_rec.dde_font_id
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.font_name=",      l_dde_font_form_rec.font_name
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.font_bold=",      l_dde_font_form_rec.font_bold
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.font_size=",      l_dde_font_form_rec.font_size
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.strike_through=", l_dde_font_form_rec.strike_through
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.super_script=",   l_dde_font_form_rec.super_script
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.lower_script=",   l_dde_font_form_rec.lower_script
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.option_1=",       l_dde_font_form_rec.option_1
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.option_2=",       l_dde_font_form_rec.option_2
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.option_3=",       l_dde_font_form_rec.option_3
    DISPLAY "copy_dde_font_record_to_form_record() - l_dde_font_form_rec.font_color=",     l_dde_font_form_rec.font_color

  END IF

  RETURN l_dde_font_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_dde_font_form_record_to_record(p_dde_font_form_rec)  
#
# Copy type dde_font_form_rec to normal dde_font record data
#
# RETURN l_dde_font_rec.*
######################################################
FUNCTION copy_dde_font_form_record_to_record(p_dde_font_form_rec)  
  DEFINE
    l_dde_font_rec       RECORD LIKE qxt_dde_font.*,
    p_dde_font_form_rec  OF t_qxt_dde_font_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_dde_font_rec.dde_font_id     = p_dde_font_form_rec.dde_font_id
  LET l_dde_font_rec.font_name       = p_dde_font_form_rec.font_name
  LET l_dde_font_rec.font_bold       = p_dde_font_form_rec.font_bold
  LET l_dde_font_rec.font_size       = p_dde_font_form_rec.font_size
  LET l_dde_font_rec.strike_through  = p_dde_font_form_rec.strike_through
  LET l_dde_font_rec.super_script    = p_dde_font_form_rec.super_script
  LET l_dde_font_rec.lower_script    = p_dde_font_form_rec.lower_script
  LET l_dde_font_rec.option_1        = p_dde_font_form_rec.option_1
  LET l_dde_font_rec.option_2        = p_dde_font_form_rec.option_2
  LET l_dde_font_rec.option_3        = p_dde_font_form_rec.option_3
  LET l_dde_font_rec.font_color      = p_dde_font_form_rec.font_color



  IF local_debug THEN
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.dde_font_id=",    p_dde_font_form_rec.dde_font_id
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.font_name=",      p_dde_font_form_rec.font_name
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.font_bold=",      p_dde_font_form_rec.font_bold
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.font_size=",      p_dde_font_form_rec.font_size
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.strike_through=", p_dde_font_form_rec.strike_through
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.super_script=",   p_dde_font_form_rec.super_script
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.lower_script=",   p_dde_font_form_rec.lower_script
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.option_1=",       p_dde_font_form_rec.option_1
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.option_2=",       p_dde_font_form_rec.option_2
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.option_3=",       p_dde_font_form_rec.option_3
    DISPLAY "copy_dde_font_form_record_to_record() - p_dde_font_form_rec.font_color=",     p_dde_font_form_rec.font_color

    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.dde_font_id=",    l_dde_font_rec.dde_font_id
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.font_name=",      l_dde_font_rec.font_name
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.font_bold=",      l_dde_font_rec.font_bold
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.font_size=",      l_dde_font_rec.font_size
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.strike_through=", l_dde_font_rec.strike_through
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.super_script=",   l_dde_font_rec.super_script
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.lower_script=",   l_dde_font_rec.lower_script
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.option_1=",       l_dde_font_rec.option_1
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.option_2=",       l_dde_font_rec.option_2
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.option_3=",       l_dde_font_rec.option_3
    DISPLAY "copy_dde_font_form_record_to_record() - l_dde_font_rec.font_color=",     l_dde_font_rec.font_color
  END IF


  RETURN l_dde_font_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_dde_font_scroll()
#
# Populate dde_font grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_dde_font_scroll()
  CALL fgl_grid_header("sc_dde_font","dde_font_id",    get_str_tool(631),"center","F13")  --dde_font
  CALL fgl_grid_header("sc_dde_font","font_name",      get_str_tool(632),"center","F14")  --dde_font
  CALL fgl_grid_header("sc_dde_font","font_bold",      get_str_tool(633),"center","F15")  --dde_font
  CALL fgl_grid_header("sc_dde_font","font_size",      get_str_tool(634),"center","F16")  --dde_font
  CALL fgl_grid_header("sc_dde_font","strike_through", get_str_tool(635),"center","F17")  --dde_font
  CALL fgl_grid_header("sc_dde_font","super_script",   get_str_tool(636),"center","F18")  --dde_font
  CALL fgl_grid_header("sc_dde_font","lower_script",   get_str_tool(637),"center","F19")  --dde_font
  CALL fgl_grid_header("sc_dde_font","option_1",       get_str_tool(638),"center","F20")  --dde_font
  CALL fgl_grid_header("sc_dde_font","option_2",       get_str_tool(639),"center","F21")  --dde_font
  CALL fgl_grid_header("sc_dde_font","option_3",       get_str_tool(640),"center","F22")  --dde_font
  CALL fgl_grid_header("sc_dde_font","font_color",     get_str_tool(641),"center","F23")  --dde_font

END FUNCTION



#######################################################
# FUNCTION populate_dde_font_form_labels_g()
#
# Populate dde_font form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_labels_g()

  CALL fgl_settitle(get_str_tool(630))

  DISPLAY get_str_tool(630) TO lbTitle

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION



#######################################################
# FUNCTION populate_dde_font_form_labels_t()
#
# Populate dde_font form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_labels_t()

  CALL fgl_settitle(get_str_tool(630))

  DISPLAY get_str_tool(630) TO lbTitle

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_dde_font_form_edit_labels_g()
#
# Populate dde_font form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(630)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(7) TO lbTitle  --Edit


  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_name")



END FUNCTION



#######################################################
# FUNCTION populate_dde_font_form_edit_labels_t()
#
# Populate dde_font form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_edit_labels_t()

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_dde_font_form_view_labels_g()
#
# Populate dde_font form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(630)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_dde_font_form_view_labels_t()
#
# Populate dde_font form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_view_labels_t()

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f114

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_language_id")

END FUNCTION


#######################################################
# FUNCTION populate_dde_font_form_create_labels_g()
#
# Populate dde_font form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(630)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

#  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_dde_font_form_create_labels_t()
#
# Populate dde_font form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_form_create_labels_t()

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_dde_font_list_form_labels_g()
#
# Populate dde_font list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(630)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_dde_font_scroll()


  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete


END FUNCTION


#######################################################
# FUNCTION populate_dde_font_list_form_labels_t()
#
# Populate dde_font list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_dde_font_list_form_labels_t()

  DISPLAY trim(get_str_tool(630)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(630) TO dl_f1

  DISPLAY get_str_tool(631) TO dl_f1
  DISPLAY get_str_tool(632) TO dl_f2
  DISPLAY get_str_tool(633) TO dl_f3
  DISPLAY get_str_tool(634) TO dl_f4
  DISPLAY get_str_tool(635) TO dl_f5
  DISPLAY get_str_tool(636) TO dl_f6
  DISPLAY get_str_tool(637) TO dl_f7
  DISPLAY get_str_tool(638) TO dl_f8
  DISPLAY get_str_tool(639) TO dl_f9
  DISPLAY get_str_tool(640) TO dl_f10
  DISPLAY get_str_tool(641) TO dl_f11

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################


