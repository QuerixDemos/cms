##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

####################################################
# FUNCTION grid_header_currency_scroll()
#
# Populate grid array header with labels
#
# RETURN NONE
####################################################
FUNCTION grid_header_currency_scroll()
  CALL fgl_grid_header("sa_currency_scroll","currency_id",get_str(1921),"right","F13")   --Cur. ID:
  CALL fgl_grid_header("sa_currency_scroll","currency_name",get_str(1922),"left","F14")  --Currency:
  CALL fgl_grid_header("sa_currency_scroll","xchg_rate",get_str(1923),"left","F15")    --Exchange Rate
END FUNCTION


#######################################################
# FUNCTION populate_currency_list_form_labels_t()
#
# Populate currency list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_currency_list_form_labels_t()
  DISPLAY get_str(1920) TO lbTitle
  DISPLAY get_str(1921) TO dl_f1
  DISPLAY get_str(1922) TO dl_f2
  DISPLAY get_str(1923) TO dl_f3
  #DISPLAY get_str(1924) TO dl_f4
  #DISPLAY get_str(1925) TO dl_f5
  #DISPLAY get_str(1926) TO dl_f6
  #DISPLAY get_str(1927) TO dl_f7
  #DISPLAY get_str(1928) TO dl_f8
  DISPLAY get_str(1929) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_currency_list_form_labels_g()
#
# Populate currency list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_currency_list_form_labels_g()
  DISPLAY get_str(1920) TO lbTitle
  #DISPLAY get_str(1921) TO dl_f1
  #DISPLAY get_str(1922) TO dl_f2
  #DISPLAY get_str(1923) TO dl_f3
  #DISPLAY get_str(1924) TO dl_f4
  #DISPLAY get_str(1925) TO dl_f5
  #DISPLAY get_str(1926) TO dl_f6
  #DISPLAY get_str(1927) TO dl_f7
  #DISPLAY get_str(1928) TO dl_f8
  DISPLAY get_str(1929) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_currency_form_labels_t()
#
# Populate currency form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_currency_form_labels_t()
  DISPLAY get_str(1910) TO lbTitle
  DISPLAY get_str(1911) TO dl_f1
  DISPLAY get_str(1912) TO dl_f2
  #DISPLAY get_str(1913) TO dl_f3
  #DISPLAY get_str(1914) TO dl_f4
  #DISPLAY get_str(1915) TO dl_f5
  #DISPLAY get_str(1916) TO dl_f6
  #DISPLAY get_str(1917) TO dl_f7
  #DISPLAY get_str(1918) TO dl_f8
  DISPLAY get_str(1919) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_currency_form_labels_g()
#
# Populate currency form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_currency_form_labels_g()
  DISPLAY get_str(1910) TO lbTitle
  DISPLAY get_str(1911) TO dl_f1
  DISPLAY get_str(1912) TO dl_f2
  #DISPLAY get_str(1913) TO dl_f3
  #DISPLAY get_str(1914) TO dl_f4
  #DISPLAY get_str(1915) TO dl_f5
  #DISPLAY get_str(1916) TO dl_f6
  #DISPLAY get_str(1917) TO dl_f7
  #DISPLAY get_str(1918) TO dl_f8
  DISPLAY get_str(1919) TO lbInfo1

END FUNCTION

