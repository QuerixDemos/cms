##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Tool String 
#
# Globals file for the string libary 
# Strings are located in a file and copied to (accessed from) memory
#
################################################################################


################################################################################
# DATABASE
################################################################################
DATABASE cms

################################################################################
# GLOBALS
################################################################################
GLOBALS

  DEFINE 
    qxt_string_tool             DYNAMIC ARRAY OF VARCHAR(100),
    qxt_string_tool_array_size  SMALLINT

  DEFINE
    t_qxt_string_tool_rec TYPE AS  
      RECORD
        string_id      SMALLINT,
        language_id    SMALLINT,
        string_data    VARCHAR(100)
      END RECORD

END GLOBALS
