##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

############################################################
# Modular
############################################################
  DEFINE t_contact_ws_rec TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      #cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
    END RECORD
    
    
########################################################################
# FUNCTION request_web_service_data()
########################################################################
FUNCTION request_web_service_data()
  DEFINE cms_cont_rec OF t_contact_ws_rec
  DEFINE ws_service VARCHAR(255)
  DEFINE ws_port VARCHAR(255)
  DEFINE ws_operation VARCHAR(255)
  DEFINE err_string CHAR(1024)
  DEFINE local_debug SMALLINT

  LET local_debug = FALSE

  CALL get_contact_ws_rec(1)
      RETURNING cms_cont_rec.*


  IF local_debug THEN
    DISPLAY "cms_cont_rec.cont_name = " ,  cms_cont_rec.cont_name         -- CHAR(20) NOT NULL,
    DISPLAY "cms_cont_rec.cont_title = ",  cms_cont_rec.cont_title       -- CHAR(5),
    DISPLAY "cms_cont_rec.cont_fname = ",  cms_cont_rec.cont_fname       -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_lname = ",  cms_cont_rec.cont_lname       -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_addr1 = ",  cms_cont_rec.cont_addr1       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_addr2 = ",  cms_cont_rec.cont_addr2       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_addr3 = ",  cms_cont_rec.cont_addr3       -- CHAR(40),
    DISPLAY "cms_cont_rec.cont_city = ",   cms_cont_rec.cont_city        -- CHAR(20),
    DISPLAY "cms_cont_rec.cont_zone = ",   cms_cont_rec.cont_zone        -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_zip = ",    cms_cont_rec.cont_zip         -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_country = ",cms_cont_rec.cont_country     -- CHAR(30),
    DISPLAY "cms_cont_rec.cont_phone = ",  cms_cont_rec.cont_phone       -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_mobile = ", cms_cont_rec.cont_mobile      -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_fax = ",    cms_cont_rec.cont_fax         -- CHAR(15),
    DISPLAY "cms_cont_rec.cont_email = ",  cms_cont_rec.cont_email       -- CHAR(50),
    DISPLAY "cms_cont_rec.comp_name = ",   cms_cont_rec.comp_name        -- CHAR(50),
    DISPLAY "cms_cont_rec.cont_notes = ",  cms_cont_rec.cont_notes       -- CHAR(50),
  END IF

END FUNCTION

