##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_tax_scroll()
#
# Populates tax rate grid header
#
# RETURN NONE
####################################################
FUNCTION grid_header_tax_scroll()
  CALL fgl_grid_header("sa_tax_rates","rate_id",get_str(1871),"right","F13")           --Rate ID:
  CALL fgl_grid_header("sa_tax_rates","tax_rate",get_str(1872),"left","F14")           --Tax Rate:
  CALL fgl_grid_header("sa_tax_rates","tax_desc",get_str(1873),"left","F15")           --Tax Description:
END FUNCTION


#######################################################
# FUNCTION populate_tax_list_form_labels_t()
#
# Populate tax rate list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tax_list_form_labels_t()
  DISPLAY get_str(1870) TO lbTitle
  DISPLAY get_str(1871) TO dl_f1
  DISPLAY get_str(1872) TO dl_f2
  DISPLAY get_str(1873) TO dl_f3
  #DISPLAY get_str(1874) TO dl_f4
  #DISPLAY get_str(1875) TO dl_f5
  #DISPLAY get_str(1876) TO dl_f6
  #DISPLAY get_str(1877) TO dl_f7
  #DISPLAY get_str(1878) TO dl_f8
  DISPLAY get_str(1879) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_tax_list_form_labels_g()
#
# Populate tax rate list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tax_list_form_labels_g()
  DISPLAY get_str(1870) TO lbTitle
  #DISPLAY get_str(1871) TO dl_f1
  #DISPLAY get_str(1872) TO dl_f2
  #DISPLAY get_str(1873) TO dl_f3
  #DISPLAY get_str(1874) TO dl_f4
  #DISPLAY get_str(1875) TO dl_f5
  #DISPLAY get_str(1876) TO dl_f6
  #DISPLAY get_str(1877) TO dl_f7
  #DISPLAY get_str(1878) TO dl_f8
  DISPLAY get_str(1879) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_tax_detail_form_labels_t()
#
# Populate tax detail form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_tax_detail_form_labels_t()
  DISPLAY get_str(1860) TO lbTitle
  DISPLAY get_str(1861) TO dl_f1
  DISPLAY get_str(1862) TO dl_f2
  #DISPLAY get_str(1863) TO dl_f3
  #DISPLAY get_str(1864) TO dl_f4
  #DISPLAY get_str(1865) TO dl_f5
  #DISPLAY get_str(1866) TO dl_f6
  #DISPLAY get_str(1867) TO dl_f7
  #DISPLAY get_str(1868) TO dl_f8
  DISPLAY get_str(1869) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_tax_detail_form_labels_g()
#
# Populate tax detail form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_tax_detail_form_labels_g()
  DISPLAY get_str(1860) TO lbTitle
  DISPLAY get_str(1861) TO dl_f1
  DISPLAY get_str(1862) TO dl_f2
  #DISPLAY get_str(1863) TO dl_f3
  #DISPLAY get_str(1864) TO dl_f4
  #DISPLAY get_str(1865) TO dl_f5
  #DISPLAY get_str(1866) TO dl_f6
  #DISPLAY get_str(1867) TO dl_f7
  #DISPLAY get_str(1868) TO dl_f8
  DISPLAY get_str(1869) TO lbInfo1

END FUNCTION

