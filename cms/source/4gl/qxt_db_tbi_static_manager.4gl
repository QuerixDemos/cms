##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tbi_tooltip table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
##################################################################################################################################
# Display functions
#################################################################################################################################
#
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


######################################################################################
# Data Access Functions
######################################################################################

FUNCTION get_tbi_static_name(p_tbi_static_id)
  DEFINE
    p_tbi_static_id   LIKE qxt_tbi_static.tbi_static_id,
    l_tbi_static_name LIKE qxt_tbi_static.tbi_static_name,
    local_debug       SMALLINT

  LET local_debug = FALSE

  SELECT tbi_static_name
  INTO l_tbi_static_name
  FROM qxt_tbi_static
  WHERE qxt_tbi_static.tbi_static_id = p_tbi_static_id

  IF local_debug THEN
    DISPLAY "get_tbi_static_name() - p_tbi_static_id =", p_tbi_static_id
    DISPLAY "get_tbi_static_name() - l_tbi_static_name =", l_tbi_static_name
  END IF

  RETURN l_tbi_static_name

END FUNCTION

FUNCTION get_tbi_static_id(p_tbi_static_name)
  DEFINE
    p_tbi_static_name   LIKE qxt_tbi_static.tbi_static_name,
    l_tbi_static_id     LIKE qxt_tbi_static.tbi_static_id,
    local_debug         SMALLINT

  LET local_debug = FALSE

  SELECT tbi_static_id
  INTO l_tbi_static_id
  FROM qxt_tbi_static
  WHERE qxt_tbi_static.tbi_static_name = p_tbi_static_name

  IF local_debug THEN
    DISPLAY "get_tbi_static_name() - p_tbi_static_name =", p_tbi_static_name
    DISPLAY "get_tbi_static_name() - l_tbi_static_id =", l_tbi_static_id
  END IF

  RETURN l_tbi_static_id

END FUNCTION

###################################################################################
# FUNCTION toolbar_menu_action_combo_list(cb_field_name)
#
# Populates toolbar_menu_action combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION tbi_static_name_combo_list(cb_field_name)
  DEFINE 
    l_qxt_tbi_static_name_arr DYNAMIC ARRAY OF LIKE qxt_tbi_static.tbi_static_name,
    row_count                 INTEGER,
    current_row               INTEGER,
    cb_field_name             VARCHAR(40),   --form field name for the country combo list field
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT
 
  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "tbi_static_name_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_qxt_tbi_static_name_scroll2 CURSOR FOR 
    SELECT qxt_tbi_static.tbi_static_name
      FROM qxt_tbi_static

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_qxt_tbi_static_name_scroll2 INTO l_qxt_tbi_static_name_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_qxt_tbi_static_name_arr[row_count])

    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_qxt_tbi_static_name_arr[row_count] CLIPPED, ")"
      DISPLAY "tbi_static_name_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

END FUNCTION

