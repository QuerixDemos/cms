##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to print html template files
# with variable contents
#
# Created:
# 06.12.06 
#
# Modification History:
# None
# 
#
#
#
# FUNCTION                                           DESCRIPTION                                                RETURN
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

##################################################################################
# FUNCTION get_print_html_template_directory()
#
# Return print html template directory
#
# RETURN qxt_settings.print_html_template_path
##################################################################################
FUNCTION get_print_html_template_directory()

  RETURN qxt_settings.print_html_template_path

END FUNCTION


##################################################################################
# FUNCTION set_print_html_template_directory(p_path)
#
# Set the print html template directory
#
# RETURN qxt_settings.print_html_template_path
##################################################################################
FUNCTION set_print_html_template_directory(p_path)
  DEFINE p_path VARCHAR(100)

  LET qxt_settings.print_html_template_path = p_path

END FUNCTION



##############################################################
# FUNCTION get_print_html_template_path()
#
# Returns the print html template path with file name 
#
# RETURN qxt_settings.print_html_output
##############################################################
FUNCTION get_print_html_template_path(p_filename)
  DEFINE
    file_path VARCHAR(200),
    p_filename VARCHAR(100)

  LET file_path = qxt_settings.print_html_template_path CLIPPED, "/", p_filename

  RETURN file_path

END FUNCTION



###############################################################################################
# EOF
###############################################################################################


