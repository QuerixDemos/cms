##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage acounts
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                          RETURN:
# get_account_credit_balance(p_account_id)    Retrieve the credit balance for the account           credit_limit - t_inv_balance 
# get_account_balance(p_account_id)           Get the account balance of an account                 l_total_balance
# get_account_rec(p_account_id)               Get the account record                                l_account.* (LIKE account.*)
# get_form_account(p_account_id)              ????                                                  l_account_rec.*  (cust record)
# account_input(p_account)                    input form for new & edit account                     p_account.*
# account_edit(p_account_id)                  Edit account details                                  NONE
# account_create()                            Create a new account                                  l_account.account_id                                
# account_delete(p_account_id)                Delete account record                                 NONE
# account_view(p_account_id)                  View account details in window                        NONE
# account_view_by_rec(p_account_rec)          Display account details on form using record argument NONE
# account_popup_data_source(p_order_field)    Data Source (Cursor) for the account_popup function   NONE
# account_popup(p_account_id,p_order_field)   Display accounts list to select and return id         l_account_arr[i].account_id  (or p_account_id for Cancel)
# account_to_form_account(p_account)          ????                                                  l_account_rec.*
# account_display(p_account_id)               DISPLAY account record                                NONE
# form_account_to_account(p_account_rec)      ????                                                  l_account.*
# disp_account_comp(p_comp_id)                Display extracted company details (shorter version)   l_comp_rec.*
# grid_header_f_account_scroll_g()            Populate grid header details                          NONE
##################################################################################################################



############################################################################################################
# Display/Format etc. functions
############################################################################################################


####################################################
# FUNCTION grid_header_f_account_scroll_g()
#
# Populate grid header details
#
# RETURN NONE
####################################################
FUNCTION grid_header_f_account_scroll_g()
  CALL fgl_grid_header("sa_account","account_id",get_str(1121),"right","F13")        --Account ID
  CALL fgl_grid_header("sa_account","comp_name",get_str(1122),"left","F14")          --Company
  CALL fgl_grid_header("sa_account","cont_name",get_str(1123),"left","F15")          --Contact
  CALL fgl_grid_header("sa_account","credit_limit",get_str(1124),"right","F16")      --Credit Limit
  CALL fgl_grid_header("sa_account","discount",get_str(1125),"right","F17")          --Discount
  CALL fgl_grid_header("sa_account","name",get_str(1126),"left","F18")               --Acount.Mngr
END FUNCTION


#######################################################
# FUNCTION populate_account_list_form_labels_t()
#
# Populate account list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_account_list_form_labels_t()
  DISPLAY get_str(1120) TO lbTitle
  DISPLAY get_str(1121) TO dl_f1
  DISPLAY get_str(1122) TO dl_f2
  DISPLAY get_str(1123) TO dl_f3
  DISPLAY get_str(1124) TO dl_f4
  DISPLAY get_str(1125) TO dl_f5
  DISPLAY get_str(1126) TO dl_f6
  #DISPLAY get_str(1127) TO dl_f7
  #DISPLAY get_str(1128) TO dl_f8
  DISPLAY get_str(1129) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_account_list_form_labels_g()
#
# Populate account list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_account_list_form_labels_g()
  DISPLAY get_str(1120) TO lbTitle
  #DISPLAY get_str(1121) TO dl_f1
  #DISPLAY get_str(1122) TO dl_f2
  #DISPLAY get_str(1123) TO dl_f3
  #DISPLAY get_str(1124) TO dl_f4
  #DISPLAY get_str(1125) TO dl_f5
  #DISPLAY get_str(1126) TO dl_f6
  #DISPLAY get_str(1127) TO dl_f7
  #DISPLAY get_str(1128) TO dl_f8
  DISPLAY get_str(1129) TO lbInfo1

  CALL grid_header_f_account_scroll_g()
  CALL fgl_settitle(get_str(1101))  --"Account List")


  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(3) TO bt_cancel   --done
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(819) TO bt_new
  DISPLAY "!" TO bt_new

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

END FUNCTION


#######################################################
# FUNCTION populate_account_form_labels_g()
#
# Populate account list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_labels_g()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_account_form_labels_t()
#
# Populate account list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_labels_t()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_account_form_input_labels_g()
#
# Populate account input form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_input_labels_g()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

    CALL fgl_settitle(get_str(1102))  --"Account Details")

END FUNCTION


#######################################################
# FUNCTION populate_account_form_input_labels_t()
#
# Populate account input form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_input_labels_t()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_account_form_view_labels_g()
#
# Populate account view form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_view_labels_g()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL fgl_settitle(get_str(1103))  --"Account Details")




END FUNCTION


#######################################################
# FUNCTION populate_account_form_view_labels_t()
#
# Populate account view form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_account_form_view_labels_t()
  DISPLAY get_str(1200) TO lbTitle
  DISPLAY get_str(1201) TO dl_f1
  DISPLAY get_str(1202) TO dl_f2
  DISPLAY get_str(1203) TO dl_f3
  DISPLAY get_str(1204) TO dl_f4
  DISPLAY get_str(1205) TO dl_f5
  DISPLAY get_str(1206) TO dl_f6
  DISPLAY get_str(1207) TO dl_f7
  DISPLAY get_str(1208) TO dl_f8
  DISPLAY get_str(1209) TO dl_f9
  DISPLAY get_str(1210) TO dl_f10
  DISPLAY get_str(1211) TO dl_f11
  #DISPLAY get_str(1212) TO dl_f12
  #DISPLAY get_str(1213) TO dl_f13
  #DISPLAY get_str(1214) TO dl_f14
  #DISPLAY get_str(1215) TO dl_f15
  #DISPLAY get_str(1216) TO dl_f16
  #DISPLAY get_str(1217) TO dl_f17
  #DISPLAY get_str(1218) TO dl_f18
  #DISPLAY get_str(1219) TO dl_f19
  DISPLAY get_str(1219) TO lbInfo1

END FUNCTION

############################################################################################################
# EOF
############################################################################################################
