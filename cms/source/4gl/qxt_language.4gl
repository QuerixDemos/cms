##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to change the help file dynamically at runtime
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# get_language()                                  Get the current language choice                            language
# set_language(lang)                              Set the current language                                   NONE
# lang_menu()                                     The lang_menu() function allows to specify the language.   ret 
#                                                 Note: language is a simple global SMALLINT which represents 
#                                                 the language 1-english 2-spanisch 3-german 4-french 
#                                                 5-arabic 6-italian, 7-other
#
# 
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


######################################################################################
# Init/Configuration Functions
######################################################################################


###########################################################
# FUNCTION process_language_cfg_import(filename)
#
# Import/Process language cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_language_cfg_import(p_cfg_filename)

DEFINE 
  p_cfg_filename  VARCHAR(100),
  ret SMALLINT,
  local_debug SMALLINT


  LET local_debug = FALSE  --0=off 1=on

  IF p_cfg_filename IS NULL OR p_cfg_filename = "" THEN
    LET p_cfg_filename = get_cfg_path(get_language_cfg_filename())
  ELSE
  	DISPLAY "Why is this varChar NOT NULL or <empty> ????"
  END IF

  IF p_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_language_cfg_import()","Error in process_language_cfg_import()\nMissing function argument and/or missing entry in language.cfg","error")
    RETURN NULL
  END IF


  IF local_debug THEN
    DISPLAY "process_language_cfg_import() - get_language_cfg_filename() = ", get_language_cfg_filename()
    DISPLAY "process_language_cfg_import() - p_cfg_filename = ", p_cfg_filename
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  # [Language]
  #read from language section
  LET qxt_settings.language_id              = configGetInt(ret, "[Language]", "language_id")
  LET qxt_settings.language_default_id      = configGetInt(ret, "[Language]", "language_default_id")
  LET qxt_settings.language_list_source     = configGetInt(ret, "[Language]", "language_list_source")
  LET qxt_settings.language_list_filename   = configGet(ret,    "[Language]", "language_list_filename")
  LET qxt_settings.language_list_table_name = configGet(ret,    "[Language]", "language_list_table_name")



  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_icon_cfg_filename = ", p_cfg_filename CLIPPED, "###############################"

    #language section
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [Language] Section           x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    DISPLAY "configInit() - qxt_settings.language_id=",                qxt_settings.language_id
    DISPLAY "configInit() - qxt_settings.language_default_id=",        qxt_settings.language_default_id
    DISPLAY "configInit() - qxt_settings.language_list_source=",       qxt_settings.language_list_source
    DISPLAY "configInit() - qxt_settings.language_list_filename=",     qxt_settings.language_list_filename
    DISPLAY "configInit() - qxt_settings.language_list_table_name=",   qxt_settings.language_list_table_name

   
  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_language_cfg_export(filename)
#
# Export language cfg data
#
# RETURN ret
###########################################################
FUNCTION process_language_cfg_export(p_cfg_filename)
  DEFINE 
    ret SMALLINT,
    p_cfg_filename VARCHAR(100)

  IF p_cfg_filename IS NULL THEN
    LET p_cfg_filename = get_cfg_path(get_language_cfg_filename())
  END IF

  IF p_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_language_cfg_export()","Error in process_language_cfg_export()\nMissing function argument and/or missing entry in language.cfg","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_cfg_filename)  --configInit(filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  # [Language]
  #write to language section
  CALL configWrite(ret, "[Language]",  "language_id",                qxt_settings.language_id )
  CALL configWrite(ret, "[Language]",  "language_default_id",        qxt_settings.language_default_id )
  CALL configWrite(ret, "[Language]",  "language_list_source",    qxt_settings.language_list_source )
  CALL configWrite(ret, "[Language]",  "language_list_filename", qxt_settings.language_list_filename )
  CALL configWrite(ret, "[Language]",  "language_list_table_name",qxt_settings.language_list_table_name )

END FUNCTION


######################################################################################
# Data Access Functions
######################################################################################


###########################################################
# FUNCTION get_language_cfg_filename()
#
# Get the language configuration file name
#
# RETURN qxt_settings.icon_cfg_filename
###########################################################
FUNCTION get_language_cfg_filename()
  RETURN qxt_settings.language_cfg_filename
END FUNCTION

###########################################################
# FUNCTION set_language_cfg_filename(p_fname
#
# Set the language configuration file name
#
# RETURN NONE
###########################################################
FUNCTION set_language_cfg_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.language_cfg_filename = p_fname
END FUNCTION


############################################################
# FUNCTION get_language()
#
# Get the current language choice
#
# RETURN language
############################################################
FUNCTION get_language()

  RETURN qxt_settings.language_id

END FUNCTION


#############################################################
# FUNCTION set_language(lang)
#
# Set the current language 
#
# RETURN NONE
############################################################
FUNCTION set_language(lang)
  DEFINE lang SMALLINT

  IF lang < 1 OR lang > 99 THEN
    LET qxt_err_msg = get_str_tool(30), " - set_language(lang) Language ID\n " , lang 
    CALL fgl_winmessage(get_str_tool(804),qxt_err_msg,"error") 
  ELSE
    LET qxt_settings.language_id = lang
    #CALL publish_toolbar("global",0)  -- toolbar refresh
    #CALL publish_toolbar("global",1)  -- toolbar refresh
    #CALL load_main_menu()  --refresh main menu with new language

  END IF

END FUNCTION


############################################################
# FUNCTION get_language_list_source()
#
# Get the current language list source (db or file)
#
# RETURN qxt_settings.language_list_source
############################################################
FUNCTION get_language_list_source()
  RETURN qxt_settings.language_list_source
END FUNCTION


############################################################
# FUNCTION set_language_list_source(p_lang)
#
# Set the current language list source (db or file)
#
# RETURN qxt_settings.language_list_source
############################################################
FUNCTION set_language_list_source(p_lang)
  DEFINE
    p_lang SMALLINT

  LET qxt_settings.language_list_source = p_lang
END FUNCTION


############################################################
# FUNCTION get_language_list_filename()
#
# Get the current language list filename
#
# RETURN qxt_settings.language_list_filename
############################################################
FUNCTION get_language_list_filename()
  RETURN qxt_settings.language_list_filename
END FUNCTION


############################################################
# FUNCTION set_language_list_filename(p_lang)
#
# Set the current language list filename
#
# RETURN NONE
############################################################
FUNCTION set_language_list_filename(p_lang)
  DEFINE
    p_lang VARCHAR(50)

  LET qxt_settings.language_list_filename = p_lang
END FUNCTION


############################################################
# FUNCTION get_language_list_table_name()
#
# Get the language list table name
#
# RETURN qxt_settings.language_list_table_name
############################################################
FUNCTION get_language_list_table_name()
  RETURN qxt_settings.language_list_table_name
END FUNCTION


############################################################
# FUNCTION set_language_list_table_name(p_lang)
#
# Set the language list table name
#
# RETURN NONE
############################################################
FUNCTION set_language_list_table_name(p_lang)
  DEFINE
    p_lang VARCHAR(18)

  LET qxt_settings.language_list_table_name = p_lang
END FUNCTION


############################################################
# FUNCTION get_language_default()
#
# Get the default language (all strings are available for default language)
#
# RETURN qxt_settings.language_default_id
############################################################
FUNCTION get_language_default()
  RETURN qxt_settings.language_default_id
END FUNCTION


############################################################
# FUNCTION get_language_default()
#
# Set the default language (all strings are available for default language)
#
# RETURN NONE
############################################################
FUNCTION set_language_default(p_lang_id)
  DEFINE
    p_lang_id SMALLINT

  LET qxt_settings.language_default_id = p_lang_id
END FUNCTION

##########################################################################
# FUNCTION lang_menu()
##########################################################################
# The lang_menu() function allows to specify the language. 
# Note: language is a simple global SMALLINT which represents the language
#  1-english 2-spanisch 3-german 4-french 5-arabic (6-italian, 7-other)
#
# RETURN ret
##########################################################################
FUNCTION lang_menu()
  DEFINE
    inp_char CHAR,
    ret SMALLINT,
    previous_help_file_id SMALLINT


  #LET previous_help_file_id = get_current_classic_help()
  # set help file '10' (10 is used for this tools library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  #CALL set_classic_help_file(10)

  CLEAR SCREEN

  #Open full size FLAT language window embedded into the screen window
  CALL fgl_window_open("w_languages",1,1,24,80,0)


{
 # IF fgl_fglgui() THEN
    WHILE TRUE 

      PROMPT "" FOR CHAR inp_char HELP 200
 
        BEFORE PROMPT
          CALL publish_toolbar("gd_lang",0)  --publish context sensitive toolbar icons for this menu

        ON KEY ("F71")  -- "English" "View form in English" HELP 71
          CALL set_language(1)
          LET ret =  1
          EXIT WHILE


        ON KEY ("F72") --"Español" "View form in Spanish" HELP 72
          CALL set_language(2)
          LET ret =  2
          EXIT WHILE

        ON KEY ("F73") --"Deutsch" "Form auf Deutsch" HELP 73
          CALL set_language(3)
          LET ret =  3
          EXIT WHILE

        ON KEY ("F74") --"Francais" "voi form en Francais" HELP 74
          CALL set_language(4)
          LET ret =  4
          EXIT WHILE

        ON KEY ("F75") --"Arabic" "View form in Arabic" HELP 75
          CALL set_language(5)
          LET qxt_tmp_str = get_str_tool(901) CLIPPED, "\n", get_str_tool(902) CLIPPED, "\n",get_str_tool(903)
          CALL fgl_winmessage(get_str_tool(900), qxt_tmp_str, "info")
          LET ret =  5
          EXIT WHILE

        ON KEY ("F76") --"Italian" "Italian" HELP 76
          CALL set_language(6)
          LET ret =  6
          EXIT WHILE

        ON KEY ("F77") --"Other" "Other Language" HELP 77
          CALL set_language(7)
          LET ret =  7
          EXIT WHILE


        ON KEY("F12") --"Return" "Return to previous menu" HELP 99
          LET ret =  0
          EXIT WHILE

        ON KEY (HELP) --"Help..."
          CALL showhelp(1)

        #AFTER PROMPT
         # CALL publish_toolbar("gd_lang",1)

      END PROMPT

    END WHILE

  ELSE  --text client
}   
    MENU "Language"
      COMMAND "English"
        LET ret = 1
        EXIT MENU
      COMMAND "Español"
        LET ret = 2
        EXIT MENU
      COMMAND "Deutsch"
        LET ret = 3
        EXIT MENU
      COMMAND "Francais"
        LET ret = 4
        EXIT MENU
      COMMAND "Arabic"
        LET ret = 5
        EXIT MENU
      COMMAND "Italian"
        LET ret = 6
        EXIT MENU
      COMMAND "Other"
        LET ret = 7
        EXIT MENU
      #COMMAND "Return"
      #  LET ret = 0
      #  EXIT MENU
    END MENU
  
 # END IF

  CALL fgl_window_close("w_languages")

  #CALL set_classic_help_file(previous_help_file_id)

  RETURN ret
END FUNCTION

