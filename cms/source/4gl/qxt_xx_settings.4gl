##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings
#
# This 4gl module includes functions to change the environment i.e. language choice
#
# Modification History:
# 19.10.06 HH - Created - contents extracted from gd_guidemo.4gl
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# settings_general()                              Opens windows with tabs - to change settings and stores    NONE
#                                                 them back to disk 
# copy_settings_to_config_array(p_cfg_file_id)    Writes settings data back to config array variables        NONE
#                                                 This way, we can update the entire configuration file 
#                                                 (write back to) in one single file operation
# process_cfg_import(file_name)                   Import/Process cfg file                                    RET
#                                                 Using qxt from www.bbf7.de - rk-config
# process_cfg_export(file_name)                   Export cfg data                                            ret
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

{
############################################################
# FUNCTION settings_general()
#
# Opens windows with tabs - to change settings and store them back to disk
#
# RETURN NONE
############################################################
FUNCTION settings_general()
  DEFINE
    previous_help_file_id SMALLINT,
    tmp_str               VARCHAR(200),
    l_settings_form       OF t_qxt_settings_form

  CALL copy_settings_to_settings_form_rec() RETURNING l_settings_form.*

  LET previous_help_file_id = get_current_classic_help()
  # set help file '10' (10 is used for this qxt library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(10)

  CALL fgl_window_open("w_settings_general",5,5,get_form_path("f_qxt_settings_g"),1)
  CALL fgl_settitle(get_str_tool(100))  --General Settings
  CALL fgl_list_set("language",99, "Hello test")

  CALL language_combo_list("language")


  DISPLAY get_str_tool(111) TO dl_field11 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(112) TO dl_field12 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(113) TO dl_field13 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(114) TO dl_field14 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(115) TO dl_field15 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(116) TO dl_field16 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(117) TO dl_field17 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(118) TO dl_field18 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(119) TO dl_field19 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(221) TO dl_field121 ATTRIBUTE(BLUE, BOLD)  --had to reserve another 10'block section for path section


  DISPLAY get_str_tool(121) TO dl_field21 ATTRIBUTE(BLUE, BOLD)  --File path configuration and string
  DISPLAY get_str_tool(122) TO dl_field22 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(123) TO dl_field23 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(124) TO dl_field24 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(125) TO dl_field25 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(126) TO dl_field26 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(127) TO dl_field27 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(128) TO dl_field28 ATTRIBUTE(BLUE, BOLD) 
  DISPLAY get_str_tool(129) TO dl_field29 ATTRIBUTE(BLUE, BOLD)  --Online Demo Path:

  DISPLAY get_str_tool(131) TO dl_field31 ATTRIBUTE(BLUE, BOLD)  -- CLAssic 4gl Help
  DISPLAY get_str_tool(132) TO dl_field32 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(133) TO dl_field33 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(134) TO dl_field34 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(135) TO dl_field35 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(136) TO dl_field36 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(137) TO dl_field37 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(138) TO dl_field38 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(139) TO dl_field39 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(140) TO dl_field40 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(141) TO dl_field41 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(142) TO dl_field42 ATTRIBUTE(BLUE, BOLD)

  DISPLAY get_str_tool(151) TO dl_field51 ATTRIBUTE(BLUE, BOLD)  -- Help file base URL:
  DISPLAY get_str_tool(152) TO dl_field52 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(153) TO dl_field53 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(154) TO dl_field54 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(155) TO dl_field55 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(156) TO dl_field56 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(157) TO dl_field57 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(158) TO dl_field58 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(159) TO dl_field59 ATTRIBUTE(BLUE, BOLD)


  DISPLAY get_str_tool(171) TO dl_field71 ATTRIBUTE(BLUE, BOLD)  --Webservice Name:
  DISPLAY get_str_tool(172) TO dl_field72 ATTRIBUTE(BLUE, BOLD)

  #Language Section
  DISPLAY get_str_tool(181) TO dl_field81 ATTRIBUTE(BLUE, BOLD)  -- Language
  DISPLAY get_str_tool(182) TO dl_field82 ATTRIBUTE(BLUE, BOLD)  -- string_system_type
  DISPLAY get_str_tool(183) TO dl_field83 ATTRIBUTE(BLUE, BOLD)  -- tool_string_file_name
  DISPLAY get_str_tool(184) TO dl_field84 ATTRIBUTE(BLUE, BOLD)  -- tool_string_table_name
  DISPLAY get_str_tool(185) TO dl_field85 ATTRIBUTE(BLUE, BOLD)  -- app_string_file_name
  DISPLAY get_str_tool(186) TO dl_field86 ATTRIBUTE(BLUE, BOLD)  -- app_string_table_name

  #Print Html
  DISPLAY get_str_tool(191) TO dl_field91 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(192) TO dl_field92 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(193) TO dl_field93 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(194) TO dl_field94 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(195) TO dl_field95 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(196) TO dl_field96 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(197) TO dl_field97 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(198) TO dl_field98 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(199) TO dl_field99 ATTRIBUTE(BLUE, BOLD)  

  #Print Html Images
  DISPLAY get_str_tool(201) TO dl_field101 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(202) TO dl_field102 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(203) TO dl_field103 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(204) TO dl_field104 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(205) TO dl_field105 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(206) TO dl_field106 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(207) TO dl_field107 ATTRIBUTE(BLUE, BOLD)  
  DISPLAY get_str_tool(208) TO dl_field108 ATTRIBUTE(BLUE, BOLD)  

  #DDE Settings
  DISPLAY get_str_tool(211) TO dl_field111 ATTRIBUTE(BLUE, BOLD)  


  INPUT 
      #general section
      l_settings_form.cfg_path,
      l_settings_form.unl_path,
      l_settings_form.form_path,
      l_settings_form.document_path, 
      l_settings_form.html_path,
      l_settings_form.image_path,
      l_settings_form.icon10_path,
      l_settings_form.icon16_path,
      l_settings_form.icon32_path,
      l_settings_form.server_blob_temp_path,

      #Other specific cfg file path
      l_settings_form.main_cfg_file_name,
      l_settings_form.toolbar_cfg_file_name,
      l_settings_form.help_url_mapping_file_name,
      l_settings_form.dde_cfg_file_name,

      #Online Demo
      l_settings_form.online_demo_path,

      #classic 4gl help section
      l_settings_form.msg_path,
      l_settings_form.classic_help_multi_lang,
      l_settings_form.classic_help_file_1,
      l_settings_form.classic_help_file_2,
      l_settings_form.classic_help_file_3,
      l_settings_form.classic_help_file_4,
      l_settings_form.classic_help_file_5,
      l_settings_form.classic_help_file_6,
      l_settings_form.classic_help_file_7,
      l_settings_form.classic_help_file_8,
      l_settings_form.classic_help_file_9,
      l_settings_form.classic_help_file_10,

      #Html Help
      l_settings_form.help_system_type,
      l_settings_form.help_html_path,
      l_settings_form.help_file_base_url,

      #l_settings_form.help_language_dir_1,
      #l_settings_form.help_language_dir_2,
      #l_settings_form.help_language_dir_3,
      #l_settings_form.help_language_dir_4,
      #l_settings_form.help_language_dir_5,
      #l_settings_form.help_language_dir_6,
      #l_settings_form.help_language_dir_7,

      #Webservices
      l_settings_form.ws_name,
      l_settings_form.ws_port,

      #language
      l_settings_form.language,
      l_settings_form.tool_string_file_name,
      l_settings_form.tool_string_table_name,
      l_settings_form.app_string_file_name,
      l_settings_form.app_string_table_name,
      #l_settings_form.string_system_type,


      #Print-Html
      l_settings_form.html_print_output,
      l_settings_form.html_print_template_1,
      l_settings_form.html_print_template_2,
      l_settings_form.html_print_template_3,
      l_settings_form.html_print_template_4,
      l_settings_form.html_print_template_5,
      l_settings_form.html_print_template_6,
      l_settings_form.html_print_template_7,
      l_settings_form.html_print_template_8,

      #Print-Html Image name
      l_settings_form.html_print_image_1,
      l_settings_form.html_print_image_2,
      l_settings_form.html_print_image_3,
      l_settings_form.html_print_image_4,
      l_settings_form.html_print_image_5,
      l_settings_form.html_print_image_6,
      l_settings_form.html_print_image_7,
      l_settings_form.html_print_image_8,

      #dde 
      l_settings_form.dde_timeout

    WITHOUT DEFAULTS FROM 

      cfg_path,
      unl_path,
      form_path,
      document_path, 
      html_path,
      image_path,
      icon10_path,
      icon16_path,
      icon32_path,
      server_blob_temp_path,

      #specific file path
      main_cfg_file_name,
      toolbar_cfg_file_name,
      help_url_mapping_file_name,
      dde_cfg_file_name,
      #online demo
      online_demo_path,

      #classic 4gl help section
      msg_path,
      classic_help_multi_lang,
      classic_help_file_1,
      classic_help_file_2,
      classic_help_file_3,
      classic_help_file_4,
      classic_help_file_5,
      classic_help_file_6,
      classic_help_file_7,
      classic_help_file_8,
      classic_help_file_9,
      classic_help_file_10,

      #html help section
      help_system_type,
      help_html_path,
      help_file_base_url,
      #help_language_dir_uk,
      #help_language_dir_sp,
      #help_language_dir_de,
      #help_language_dir_fr,
      #help_language_dir_ar,
      #help_language_dir_it,
      #help_language_dir_ot,

      #Webservices
      ws_name,
      ws_port,

      #Language
      language,
      #string_system_type,
      tool_string_file_name,
      tool_string_table_name,
      app_string_file_name,
      app_string_table_name,

      #print-html
      html_print_output,
      html_print_template_1,
      html_print_template_2,
      html_print_template_3,
      html_print_template_4,
      html_print_template_5,
      html_print_template_6,
      html_print_template_7,
      html_print_template_8,

      #print-html-image
      html_print_image_1,
      html_print_image_2,
      html_print_image_3,
      html_print_image_4,
      html_print_image_5,
      html_print_image_6,
      html_print_image_7,
      html_print_image_8,

      #dde
      dde_timeout
    HELP 100

    BEFORE INPUT
      CALL publish_toolbar("settings",0)

      IF l_settings_form.string_system_type THEN
        DISPLAY "Strings are imported from a Database" TO string_system_type
      ELSE
        DISPLAY "Strings are imported from a Text File" TO string_system_type
      END IF
      
      CALL language_combo_list(qxt_settings.language)


    ON KEY(F4)  --OK & Save
      CALL fgl_dialog_update_data()
      CALL copy_settings_to_config_array(get_cfg_file_id(qxt_settings.main_cfg_file_name))
      CALL configWriteArrayToDisk(get_cfg_file_id(qxt_settings.main_cfg_file_name)) --main_cfg_file_id)     main_cfg_file_name
      EXIT INPUT


    ON KEY(F13)
      CALL get_4gl_window_information()

    ON KEY(F1011)
      NEXT FIELD cfg_path

    ON KEY(F1012)
      NEXT FIELD online_demo_path


    ON KEY(F1013)
      NEXT FIELD msg_path


    ON KEY(F1014)
      NEXT FIELD help_file_base_url

    ON KEY(F1015)
      NEXT FIELD ws_name

    ON KEY(F1016)
      NEXT FIELD language


    ON KEY(F1017)
      NEXT FIELD html_print_output

    ON KEY(F1018)
      NEXT FIELD html_print_image_1

    ON KEY(F1019)
      NEXT FIELD dde_timeout

    AFTER INPUT --check if files /directories exist
      #Check if server directories exist and if not create them
      CASE verify_server_directory_structure(TRUE)
        WHEN TRUE
          EXIT CASE
        WHEN -1
          NEXT FIELD server_blob_temp_path
          CONTINUE INPUT
      END CASE

   CALL copy_settings_form_rec_to_settings(l_settings_form.*)

  END INPUT
  CALL refresh_classic_help()
  CALL process_string_population()


  CALL publish_toolbar("settings",1)
  #CALL load_main_menu()
  CALL fgl_window_close("w_settings_general")

  CALL set_classic_help_file(previous_help_file_id)

END FUNCTION


############################################################
# FUNCTION settings_general_text()
#
# Opens windows settings for text mode client - to change settings and store them back to disk
#
# RETURN NONE
############################################################
FUNCTION settings_general_text()
  DEFINE
    previous_help_file_id SMALLINT,
    l_settings_form       OF t_qxt_settings_form

  CALL copy_settings_to_settings_form_rec() RETURNING l_settings_form.*


  LET previous_help_file_id = get_current_classic_help()
  # set help file '10' (10 is used for this qxt library) define in congiruation & language   HELP FILE "msg/cm_context_help.erm",
  CALL set_classic_help_file(10)

  IF NOT fgl_window_open("w_settings_general",5,5,get_form_path("f_qxt_settings_t"),FALSE) THEN
    CALL fgl_winmessage("Error","settings_general_text()\nCannot open window w_settings_general","error")
    RETURN
  END IF

  CALL fgl_settitle(get_str_tool(100))  --General Settings

  DISPLAY get_str_tool(111) TO dl_field11 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(112) TO dl_field12 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(113) TO dl_field13 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(114) TO dl_field14 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(115) TO dl_field15 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(116) TO dl_field16 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(221) TO dl_field121 ATTRIBUTE(BLUE, BOLD)

  DISPLAY get_str_tool(121) TO dl_field21 ATTRIBUTE(BLUE, BOLD)  --File path configuration and string
  DISPLAY get_str_tool(122) TO dl_field22 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(123) TO dl_field23 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(124) TO dl_field24 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(125) TO dl_field25 ATTRIBUTE(BLUE, BOLD)
  #DISPLAY get_str_tool(126) TO dl_field26 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(127) TO dl_field27 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(128) TO dl_field28 ATTRIBUTE(BLUE, BOLD) 
  DISPLAY get_str_tool(129) TO dl_field29 ATTRIBUTE(BLUE, BOLD)  --Online Demo Path:

  DISPLAY get_str_tool(131) TO dl_field31 ATTRIBUTE(BLUE, BOLD)  -- CLAssic 4gl Help
  DISPLAY get_str_tool(132) TO dl_field32 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(133) TO dl_field33 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(134) TO dl_field34 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(135) TO dl_field35 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(136) TO dl_field36 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(137) TO dl_field37 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(138) TO dl_field38 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(139) TO dl_field39 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(140) TO dl_field40 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(141) TO dl_field41 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(142) TO dl_field42 ATTRIBUTE(BLUE, BOLD)

  DISPLAY get_str_tool(171) TO dl_field71 ATTRIBUTE(BLUE, BOLD)  --Webservice Name:
  DISPLAY get_str_tool(172) TO dl_field72 ATTRIBUTE(BLUE, BOLD)

  #Language Section
  DISPLAY get_str_tool(181) TO dl_field81 ATTRIBUTE(BLUE, BOLD)  -- Language
  DISPLAY get_str_tool(182) TO dl_field82 ATTRIBUTE(BLUE, BOLD)  -- string_system_type
  DISPLAY get_str_tool(183) TO dl_field83 ATTRIBUTE(BLUE, BOLD)  -- tool_string_file_name
  DISPLAY get_str_tool(184) TO dl_field84 ATTRIBUTE(BLUE, BOLD)  -- tool_string_table_name
  DISPLAY get_str_tool(185) TO dl_field85 ATTRIBUTE(BLUE, BOLD)  -- app_string_file_name
  DISPLAY get_str_tool(186) TO dl_field86 ATTRIBUTE(BLUE, BOLD)  -- app_string_table_name



  INPUT 
      l_settings_form.cfg_path,
      l_settings_form.unl_path,
      l_settings_form.form_path,
      l_settings_form.document_path, 
      l_settings_form.html_path,
      l_settings_form.image_path,
      l_settings_form.server_blob_temp_path,

      #Other specific file path
      l_settings_form.main_cfg_file_name,
      l_settings_form.toolbar_cfg_file_name,
      l_settings_form.help_url_mapping_file_name,
      l_settings_form.dde_cfg_file_name,

      #Online Demo
      l_settings_form.online_demo_path,

      #classic 4gl help section
      l_settings_form.msg_path,
      l_settings_form.classic_help_multi_lang,
      l_settings_form.classic_help_file_1,
      l_settings_form.classic_help_file_2,
      l_settings_form.classic_help_file_3,
      l_settings_form.classic_help_file_4,
      l_settings_form.classic_help_file_5,
      l_settings_form.classic_help_file_6,
      l_settings_form.classic_help_file_7,
      l_settings_form.classic_help_file_8,
      l_settings_form.classic_help_file_9,
      l_settings_form.classic_help_file_10,

      #Webservices
      l_settings_form.ws_name,
      l_settings_form.ws_port,

      #language
      l_settings_form.language,
      #l_settings_form.string_system_type,
      l_settings_form.tool_string_file_name,
      l_settings_form.tool_string_table_name,
      l_settings_form.app_string_file_name,
      l_settings_form.app_string_table_name


    WITHOUT DEFAULTS FROM 

      cfg_path,
      unl_path,
      form_path,
      document_path, 
      html_path,
      image_path,
      server_blob_temp_path,
      #specific file path
      main_cfg_file_name,
      toolbar_cfg_file_name,
      help_url_mapping_file_name,
      dde_cfg_file_name,
      #online demo
      online_demo_path,

      #classic 4gl help section
      msg_path,
      classic_help_multi_lang,
      classic_help_file_1,
      classic_help_file_2,
      classic_help_file_3,
      classic_help_file_4,
      classic_help_file_5,
      classic_help_file_6,
      classic_help_file_7,
      classic_help_file_8,
      classic_help_file_9,
      classic_help_file_10,

      #Webservices
      ws_name,
      ws_port,

      #Language
      language,
      #string_system_type,
      tool_string_file_name,
      tool_string_table_name,
      app_string_file_name,
      app_string_table_name




    HELP 100

     ON KEY(F4)  --OK & Save
      CALL fgl_dialog_update_data()
      CALL copy_settings_to_config_array(get_cfg_file_id(qxt_settings.main_cfg_file_name))
      CALL configWriteArrayToDisk(get_cfg_file_id(qxt_settings.main_cfg_file_name)) --main_cfg_file_id)     main_cfg_file_name
      EXIT INPUT


    ON KEY(F13)
      CALL get_4gl_window_information()


    AFTER INPUT --check if files /directories exist
      #Check if server directories exist and if not create them
      CASE verify_server_directory_structure(TRUE)
        WHEN TRUE
          EXIT CASE
        WHEN -1
          NEXT FIELD server_blob_temp_path
          CONTINUE INPUT
      END CASE

   CALL copy_settings_form_rec_to_settings(l_settings_form.*)


  END INPUT

  CALL refresh_classic_help()

  #CALL load_main_menu()
  CALL fgl_window_close("w_settings_general")

  CALL set_classic_help_file(previous_help_file_id)

END FUNCTION
}


##############################################################################################################
# ALL Settings (all configuration files)
##############################################################################################################

###########################################################
# FUNCTION copy_settings_form_rec_to_settings(p_settings_form)
#
# Copy main form settings record to normal record
#
# RETURN NONE
###########################################################

FUNCTION copy_settings_form_rec_to_settings(p_settings_form)
  DEFINE 
    p_settings_form OF t_qxt_settings_form


  ##################################
  # Main configuration file
  ##################################

  #[Application] Section
  LET qxt_settings.application_id                   = get_application_id(p_settings_form.application_name) --directory name for config files
  LET qxt_settings.displayKeyInstructions           = p_settings_form.displayKeyInstructions				--switch to display key instructions
  LET qxt_settings.displayUsageInstructions         = p_settings_form.displayUsageInstructions			--switch to display usage instructions


  #[GeneralPath] Section
  LET qxt_settings.cfg_path                         = p_settings_form.cfg_path                     --directory name for config files
  LET qxt_settings.unl_path                         = p_settings_form.unl_path                     --directory name for unl files
  LET qxt_settings.form_path                        = p_settings_form.form_path                    --directory name for forms
  LET qxt_settings.msg_path                         = p_settings_form.msg_path                     --directory name for forms
  LET qxt_settings.document_path                    = p_settings_form.document_path                --directory name for documents
  LET qxt_settings.html_path                        = p_settings_form.html_path                    --directory name for html documents
  LET qxt_settings.image_path                       = p_settings_form.image_path                   --directory name for normal images
  LET qxt_settings.print_html_template_path         = p_settings_form.print_html_template_path     --directory name for normal images

  #[TempPath] Section
  LET qxt_settings.server_app_temp_path             = p_settings_form.server_app_temp_path         --directory name for 16x16 icons
  LET qxt_settings.server_blob_temp_path            = p_settings_form.server_blob_temp_path        --directory name for 16x16 icons

  #[FilePath]  --specific cfg files 
  LET qxt_settings.main_cfg_filename                = p_settings_form.main_cfg_filename            --main configuration file name & Path    default: cfg/config.cfg
  LET qxt_settings.toolbar_cfg_filename             = p_settings_form.toolbar_cfg_filename         --toolbar configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.icon_cfg_filename                = p_settings_form.icon_cfg_filename            --Icon configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_classic_cfg_filename        = p_settings_form.help_classic_cfg_filename    --Help Classic configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_html_cfg_filename           = p_settings_form.help_html_cfg_filename       --Help Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.language_cfg_filename            = p_settings_form.language_cfg_filename        --Language configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.string_cfg_filename              = p_settings_form.string_cfg_filename          --String configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.print_html_cfg_filename          = p_settings_form.print_html_cfg_filename      --Print Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.dde_cfg_filename                 = p_settings_form.dde_cfg_filename             --DDE configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.online_resource_cfg_filename     = p_settings_form.online_resource_cfg_filename --Online Resource configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.webservice_cfg_filename          = p_settings_form.webservice_cfg_filename      --WebService configuration/ini unl file name  default: toolbar.cfg


  #[Database] Section
  LET qxt_settings.db_name_tool                     = p_settings_form.db_name_tool                 --Datbase Name for the QXT Library
  LET qxt_settings.db_name_app                      = p_settings_form.db_name_app                  --Datbase Name for this application


  ##################################
  # Language.cfg Configuration file
  ##################################
  #[Language] Section
  LET qxt_settings.language_id                      = get_language_id(p_settings_form.language_name)            --language id
  LET qxt_settings.language_default_id              = get_language_id(p_settings_form.language_default_name)    --default language to be used if a string does not exist in a particular language
  LET qxt_settings.language_list_source             = get_source_id(p_settings_form.language_list_source_name)  --source is 0=file 1=db
  LET qxt_settings.language_list_filename           = p_settings_form.language_list_filename                    --Language import file - lists all languages (not strings)
  LET qxt_settings.language_list_table_name         = p_settings_form.language_list_table_name                  --Language table name - lists all languages (not strings)


  ##################################
  # String.cfg Configuration file
  ##################################

  #[String] Section
  LET qxt_settings.string_tool_source               = get_source_id(p_settings_form.string_tool_source_name) --0=file to mem 1= db direct 2=db to mem
  LET qxt_settings.string_tool_filename             = p_settings_form.string_tool_filename                   --multi lingual string file for tools lib  default: string_tool.unl
  LET qxt_settings.string_tool_table_name           = p_settings_form.string_tool_table_name                 --multi lingual string db table for tools lib  default: string_tool.unl
  LET qxt_settings.string_app_source                = get_source_id(p_settings_form.string_app_source_name)  --0=file to mem 1= db direct 2=db to mem
  LET qxt_settings.string_app_filename              = p_settings_form.string_app_filename                    --multi lingual string file for application    default: string.unl
  LET qxt_settings.string_app_table_name            = p_settings_form.string_app_table_name                  --multi lingual string db table for application    default: string.unl


  ##################################
  # HelpClassic.cfg Configuration file
  ##################################

  #[HelpClassic] Classic 4GL Help Section
  LET qxt_settings.help_classic_multi_lang          = get_help_classic_multi_lang()  --get_multi_lang_id(p_settings_form.help_classic_multi_lang_name)  --if classic help file supports multi languages
  LET qxt_settings.help_classic_unl_filename        = p_settings_form.help_classic_unl_filename     --file names for classic 4gl help file list (unl file)


  ##################################
  # Help_Html.cfg Configuration file
  ##################################

  #[HelpHtml] Section
  LET qxt_settings.help_html_system_type            = get_help_system_type_id(p_settings_form.help_html_system_type_name)       
  LET qxt_settings.help_html_url_map_fname          = p_settings_form.help_html_url_map_fname       --File name of the html help url map config file
  LET qxt_settings.help_html_url_map_tname          = p_settings_form.help_html_url_map_tname       --Table name of the html help url map config information
  LET qxt_settings.help_html_base_dir               = p_settings_form.help_html_base_dir            --Base DIR for the help file url ie. help/help-html
  LET qxt_settings.help_html_base_url               = p_settings_form.help_html_base_url            --Base URL for the help file url ie. www.querix.com/help
  LET qxt_settings.help_html_win_x                  = p_settings_form.help_html_win_x               --x position of the help html window
  LET qxt_settings.help_html_win_y                  = p_settings_form.help_html_win_y               --y position of the help html window
  LET qxt_settings.help_html_win_width              = p_settings_form.help_html_win_width           --width of the help html window
  LET qxt_settings.help_html_win_height             = p_settings_form.help_html_win_height          --height of the help html window


  ##################################
  # Toolbar.cfg Configuration file
  ##################################
  #[Toolbar] Section
  LET qxt_settings.toolbar_unl_filename             = p_settings_form.toolbar_unl_filename          -- Toolbar unl file (required for file lib)
  LET qxt_settings.tooltip_unl_filename             = p_settings_form.tooltip_unl_filename          -- Toolbar Tooltip unl file (required for file lib)
  LET qxt_settings.toolbar_icon_size                = p_settings_form.toolbar_icon_size             -- Size for the toolbar icon


  ##################################
  # Icon.cfg Configuration file
  ##################################
  #[Icon] Section
  LET qxt_settings.icon10_path                      = p_settings_form.icon10_path                   --directory name for 10x10 icons
  LET qxt_settings.icon16_path                      = p_settings_form.icon16_path                   --directory name for 16x16 icons
  LET qxt_settings.icon24_path                      = p_settings_form.icon24_path                   --directory name for 24x24 icons
  LET qxt_settings.icon32_path                      = p_settings_form.icon32_path                   --directory name for 32x32 icons

  ##################################
  # PrintHtml.cfg Configuration file
  ##################################
  #[PrintHtml] section
  LET qxt_settings.print_html_output                = p_settings_form.print_html_output                 --Dynamically created print html (output) name inc. rel. path
  LET qxt_settings.print_html_template_unl_filename = p_settings_form.print_html_template_unl_filename  --Import file for html print template file name list
  LET qxt_settings.print_html_image_unl_filename    = p_settings_form.print_html_image_unl_filename     --Import file for html print image file name list


  ##################################
  # dde.cfg Configuration file
  ##################################
  #[DDE] Section
  LET qxt_settings.dde_font_excel_filename          = p_settings_form.dde_font_excel_filename            --dde excel font list (unl)
  LET qxt_settings.dde_timeout                      = p_settings_form.dde_timeout                        --timeout for dde tools


  ##################################
  # Online_resource.cfg Configuration file
  ##################################
  #[OnlineResource] Section
  LET qxt_settings.online_demo_path                 = p_settings_form.online_demo_path                   --URL for online self running demo


  ##################################
  # Webservice.cfg Configuration file
  ##################################
  #[WebService]  section
  LET qxt_settings.ws_name                          = p_settings_form.ws_name                    --Webservice Name
  LET qxt_settings.ws_port                          = p_settings_form.ws_port                    --Webservice Port

  RETURN qxt_settings.*

END FUNCTION


###########################################################
# FUNCTION copy_settings_to_settings_form_rec()
#
# Copy settings form record to normal record
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_to_settings_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_form
  

  ##################################
  # Main configuration file
  ##################################

  #[Application] Section
  LET l_settings_form.application_name               = get_application_name(qxt_settings.application_id) --directory name for config files
  LET l_settings_form.main_cfg_filename              = qxt_settings.main_cfg_filename            --directory name for config files
  LET l_settings_form.displayKeyInstructions         = qxt_settings.displayKeyInstructions       -- display Key Instructions
  LET l_settings_form.displayUsageInstructions       = qxt_settings.displayUsageInstructions     --display Usage Instructions


  #[GeneralPath] Section
  LET l_settings_form.cfg_path                       = qxt_settings.cfg_path                     --directory name for config files
  LET l_settings_form.unl_path                       = qxt_settings.unl_path                     --directory name for unl files
  LET l_settings_form.form_path                      = qxt_settings.form_path                    --directory name for forms
  LET l_settings_form.msg_path                       = qxt_settings.msg_path                     --directory name for forms
  LET l_settings_form.document_path                  = qxt_settings.document_path                --directory name for documents
  LET l_settings_form.html_path                      = qxt_settings.html_path                    --directory name for html documents
  LET l_settings_form.image_path                     = qxt_settings.image_path                   --directory name for normal images
  LET l_settings_form.print_html_template_path       = qxt_settings.print_html_template_path     --directory name for normal images

  #[TempPath] Section
  LET l_settings_form.server_app_temp_path           = qxt_settings.server_app_temp_path         --directory name for 16x16 icons
  LET l_settings_form.server_blob_temp_path          = qxt_settings.server_blob_temp_path        --directory name for 16x16 icons

  #[File-Path]  --specific cfg files 
  LET l_settings_form.main_cfg_filename              = qxt_settings.main_cfg_filename            --main configuration file name & Path    default: cfg/config.cfg
  LET l_settings_form.toolbar_cfg_filename           = qxt_settings.toolbar_cfg_filename         --toolbar configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.icon_cfg_filename              = qxt_settings.icon_cfg_filename            --Icon configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.help_classic_cfg_filename      = qxt_settings.help_classic_cfg_filename    --Help Classic configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.help_html_cfg_filename         = qxt_settings.help_html_cfg_filename       --Help Html configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.language_cfg_filename          = qxt_settings.language_cfg_filename        --Language configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.string_cfg_filename            = qxt_settings.string_cfg_filename          --String configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.print_html_cfg_filename        = qxt_settings.print_html_cfg_filename      --Print Html configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.dde_cfg_filename               = qxt_settings.dde_cfg_filename             --DDE configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.online_resource_cfg_filename   = qxt_settings.online_resource_cfg_filename --Online Resource configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.webservice_cfg_filename        = qxt_settings.webservice_cfg_filename      --WebService configuration/ini unl file name  default: toolbar.cfg


  #[Database] Section
  LET l_settings_form.db_name_tool                   = qxt_settings.db_name_tool                 --Datbase Name for the QXT Library
  LET l_settings_form.db_name_app                    = qxt_settings.db_name_app                  --Datbase Name for this application


  ##################################
  # Language.cfg Configuration file
  ##################################
  #[Language] Section
  LET l_settings_form.language_name                  = get_language_name(qxt_settings.language_id)                  --language id
  LET l_settings_form.language_default_name          = get_language_name(qxt_settings.language_default_id)          --default language to be used if a string does not exist in a particular language
  LET l_settings_form.language_list_source_name      = qxt_settings.language_list_source      --source is 0=file 1=db
  LET l_settings_form.language_list_filename         = qxt_settings.language_list_filename    --Language import file - lists all languages (not strings)
  LET l_settings_form.language_list_table_name       = qxt_settings.language_list_table_name  --Language table name - lists all languages (not strings)


  ##################################
  # String.cfg Configuration file
  ##################################

  #[String] Section
  LET l_settings_form.string_tool_source_name      = get_source_name(qxt_settings.string_tool_source) --0=file to mem 1= db direct 2=db to mem
  LET l_settings_form.string_tool_filename         = qxt_settings.string_tool_filename    --multi lingual string file for tools lib  default: string_tool.unl
  LET l_settings_form.string_tool_table_name       = qxt_settings.string_tool_table_name  --multi lingual string db table for tools lib  default: string_tool.unl
  LET l_settings_form.string_app_source_name       = qxt_settings.string_app_source       --0=file to mem 1= db direct 2=db to mem
  LET l_settings_form.string_app_filename          = qxt_settings.string_app_filename     --multi lingual string file for application    default: string.unl
  LET l_settings_form.string_app_table_name        = qxt_settings.string_app_table_name   --multi lingual string db table for application    default: string.unl


  ##################################
  # HelpClassic.cfg Configuration file
  ##################################

  #[HelpClassic] Classic 4GL Help Section
  LET l_settings_form.help_classic_multi_lang_name  = get_language_name(qxt_settings.help_classic_multi_lang)  --get_multi_lang_name(qxt_settings.help_classic_multi_lang)  --if classic help file supports multi languages
  LET l_settings_form.help_classic_unl_filename     = qxt_settings.help_classic_unl_filename     --file names for classic 4gl help file list (unl file)


  ##################################
  # Help_Html.cfg Configuration file
  ##################################

  #[HelpHtml] Section
  LET l_settings_form.help_html_system_type_name    = get_help_system_type_name(qxt_settings.help_html_system_type)
  LET l_settings_form.help_html_url_map_fname       = qxt_settings.help_html_url_map_fname       --File name of the html help url map config file
  LET l_settings_form.help_html_url_map_tname       = qxt_settings.help_html_url_map_tname       --Table name of the html help url map config information
  LET l_settings_form.help_html_base_dir            = qxt_settings.help_html_base_dir            --Base DIR for the help file url ie. help/help-html
  LET l_settings_form.help_html_base_url            = qxt_settings.help_html_base_url            --Base URL for the help file url ie. www.querix.com/help
  LET l_settings_form.help_html_win_x               = qxt_settings.help_html_win_x               --x position of the help html window
  LET l_settings_form.help_html_win_y               = qxt_settings.help_html_win_y               --y position of the help html window
  LET l_settings_form.help_html_win_width           = qxt_settings.help_html_win_width           --width of the help html window
  LET l_settings_form.help_html_win_height          = qxt_settings.help_html_win_height          --height of the help html window


  ##################################
  # Toolbar.cfg Configuration file
  ##################################
  #[Toolbar] Section
  LET l_settings_form.toolbar_unl_filename          = qxt_settings.toolbar_unl_filename          -- Toolbar unl file (required for file lib)
  LET l_settings_form.tooltip_unl_filename          = qxt_settings.tooltip_unl_filename          -- Toolbar Tooltip unl file (required for file lib)
  LET l_settings_form.toolbar_icon_size             = qxt_settings.toolbar_icon_size             -- Size for the toolbar icon


  ##################################
  # Icon.cfg Configuration file
  ##################################
  #[Icon] Section
  LET l_settings_form.icon10_path                   = qxt_settings.icon10_path                   --directory name for 10x10 icons
  LET l_settings_form.icon16_path                   = qxt_settings.icon16_path                   --directory name for 16x16 icons
  LET l_settings_form.icon24_path                   = qxt_settings.icon24_path                   --directory name for 24x24 icons
  LET l_settings_form.icon32_path                   = qxt_settings.icon32_path                   --directory name for 32x32 icons

  ##################################
  # Icon.cfg Configuration file
  ##################################
  #[PrintHtml] section
  LET l_settings_form.print_html_output                = qxt_settings.print_html_output                 --Dynamically created print html (output) name inc. rel. path
  LET l_settings_form.print_html_template_unl_filename = qxt_settings.print_html_template_unl_filename  --Import file for html print template file name list
  LET l_settings_form.print_html_image_unl_filename    = qxt_settings.print_html_image_unl_filename     --Import file for html print image file name list


  ##################################
  # dde.cfg Configuration file
  ##################################
  #[DDE] Section
  LET l_settings_form.dde_font_excel_filename         = qxt_settings.dde_font_excel_filename            --dde excel font list (unl)
  LET l_settings_form.dde_timeout                     = qxt_settings.dde_timeout                        --timeout for dde tools


  ##################################
  # Online_resource.cfg Configuration file
  ##################################

  #[OnlineResource] Section
  LET l_settings_form.online_demo_path                = qxt_settings.online_demo_path                   --URL for online self running demo


  ##################################
  # Webservice.cfg Configuration file
  ##################################
  #[WebService]  section  
  LET l_settings_form.ws_name                         = qxt_settings.ws_name                            --Webservice Name
  LET l_settings_form.ws_port                         = qxt_settings.ws_port                            --Webservice Port


  RETURN l_settings_form.*

  END FUNCTION



##############################################################################################################
# Main Settings (main configuration file)
##############################################################################################################

###########################################################
# FUNCTION copy_main_settings_form_rec_to_settings(p_settings_form)
#
# Copy main settings form record to normal record
#
# RETURN NONE
###########################################################
FUNCTION copy_main_settings_form_rec_to_settings(p_settings_form)
  DEFINE 
    p_settings_form OF t_qxt_settings_form


  ##################################
  # Main configuration file
  ##################################

  #[Application] Section
  LET qxt_settings.application_id                   = get_application_id(p_settings_form.application_name) --directory name for config files
  LET qxt_settings.main_cfg_filename                = p_settings_form.main_cfg_filename                         --directory name for config files

  #[General] Section
  LET qxt_settings.cfg_path                         = p_settings_form.cfg_path                     --directory name for config files
  LET qxt_settings.unl_path                         = p_settings_form.unl_path                     --directory name for unl files
  LET qxt_settings.form_path                        = p_settings_form.form_path                    --directory name for forms
  LET qxt_settings.msg_path                         = p_settings_form.msg_path                     --directory name for forms
  LET qxt_settings.document_path                    = p_settings_form.document_path                --directory name for documents
  LET qxt_settings.html_path                        = p_settings_form.html_path                    --directory name for html documents
  LET qxt_settings.image_path                       = p_settings_form.image_path                   --directory name for normal images
  LET qxt_settings.print_html_template_path         = p_settings_form.print_html_template_path     --directory name for normal images

  #[TempPath] Section
  LET qxt_settings.server_app_temp_path             = p_settings_form.server_app_temp_path         --directory name for 16x16 icons
  LET qxt_settings.server_blob_temp_path            = p_settings_form.server_blob_temp_path        --directory name for 16x16 icons

  #[File-Path]  --specific cfg files 
  LET qxt_settings.main_cfg_filename                = p_settings_form.main_cfg_filename            --main configuration file name & Path    default: cfg/config.cfg
  LET qxt_settings.toolbar_cfg_filename             = p_settings_form.toolbar_cfg_filename         --toolbar configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.icon_cfg_filename                = p_settings_form.icon_cfg_filename            --Icon configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_classic_cfg_filename        = p_settings_form.help_classic_cfg_filename    --Help Classic configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_html_cfg_filename           = p_settings_form.help_html_cfg_filename       --Help Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.language_cfg_filename            = p_settings_form.language_cfg_filename        --Language configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.string_cfg_filename              = p_settings_form.string_cfg_filename          --String configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.print_html_cfg_filename          = p_settings_form.print_html_cfg_filename      --Print Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.dde_cfg_filename                 = p_settings_form.dde_cfg_filename             --DDE configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.online_resource_cfg_filename     = p_settings_form.online_resource_cfg_filename --Online Resource configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.webservice_cfg_filename          = p_settings_form.webservice_cfg_filename      --WebService configuration/ini unl file name  default: toolbar.cfg


  #[Database] Section
  LET qxt_settings.db_name_tool                     = p_settings_form.db_name_tool                 --Datbase Name for the QXT Library
  LET qxt_settings.db_name_app                      = p_settings_form.db_name_app                  --Datbase Name for this application


  RETURN qxt_settings.*

END FUNCTION



{

############################################################
# FUNCTION copy_settings_to_config_array(p_cfg_file_id)
#
# Writes settings data back to config array variables
# This way, we can update the entire configuration file (write back to) in one single file operation
#
# RETURN NONE
############################################################
FUNCTION copy_settings_to_config_array(p_cfg_file_id)
  DEFINE 
    p_cfg_file_id SMALLINT,
    file_name VARCHAR(100)

  #Write to general section
  #CALL configWriteToArray(p_cfg_file_id, "[General]", "unl_path", tl_settings.string_system_type )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "unl_path", qxt_settings.unl_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "cfg_path", qxt_settings.cfg_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "form_path", qxt_settings.form_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "document_path", qxt_settings.document_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "html_path", qxt_settings.html_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "image_path", qxt_settings.image_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "icon10_path", qxt_settings.icon10_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "icon16_path", qxt_settings.icon16_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "icon24_path", qxt_settings.icon24_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "icon32_path", qxt_settings.icon32_path )
  CALL configWriteToArray(p_cfg_file_id, "[General]", "server_blob_temp_path", qxt_settings.server_blob_temp_path )


  #write to classic-help section
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "msg_path", qxt_settings.msg_path )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_multi_lang", qxt_settings.classic_help_multi_lang )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_1", qxt_settings.classic_help_file[1]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_2", qxt_settings.classic_help_file[2]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_3", qxt_settings.classic_help_file[3]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_4", qxt_settings.classic_help_file[4]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_5", qxt_settings.classic_help_file[5]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_6", qxt_settings.classic_help_file[6]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_7", qxt_settings.classic_help_file[7]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_8", qxt_settings.classic_help_file[8]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_9", qxt_settings.classic_help_file[9]  )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Classic]", "classic_help_file_tool", qxt_settings.classic_help_file[10])


  #write to html-help section
  CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_system_type",   qxt_settings.help_system_type )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path",     qxt_settings.help_html_path )
  CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_file_base_url", qxt_settings.help_file_base_url)
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_uk", qxt_settings.help_language_dir[1] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_sp", qxt_settings.help_language_dir[2] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_de", qxt_settings.help_language_dir[3] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_fr", qxt_settings.help_language_dir[4] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_ar", qxt_settings.help_language_dir[5] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_it", qxt_settings.help_language_dir[6] )
  #CALL configWriteToArray(p_cfg_file_id, "[Help-Html]", "help_html_path_ot", qxt_settings.help_language_dir[7] )

  #write to File-Path section
  CALL configWriteToArray(p_cfg_file_id, "[File-Path]", "toolbar_cfg_file_name", qxt_settings.toolbar_cfg_file_name )
  CALL configWriteToArray(p_cfg_file_id, "[File-Path]", "help_url_mapping_file_name", qxt_settings.help_url_mapping_file_name )
  CALL configWriteToArray(p_cfg_file_id, "[File-Path]", "dde_cfg_file_name", qxt_settings.dde_cfg_file_name )

  #write to OnlineResource section
  CALL configWriteToArray(p_cfg_file_id, "[OnlineResource]", "online_demo_path", qxt_settings.online_demo_path )

  #write to webservice section
  CALL configWriteToArray(p_cfg_file_id, "[WebService]", "ws_name", qxt_settings.ws_name )
  CALL configWriteToArray(p_cfg_file_id, "[WebService]", "ws_port", qxt_settings.ws_port )


  #write to language section
  #CALL configWriteToArray(p_cfg_file_id, "[Language]", "string_system_type" , qxt_settings.string_system_type )
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "language",            qxt_settings.language )

  CALL configWriteToArray(p_cfg_file_id, "[Language]", "tool_string_file_name", qxt_settings.tool_string_file_name )
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "tool_string_table_name", qxt_settings.tool_string_table_name )
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "app_string_file_name", qxt_settings.app_string_file_name )
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "app_string_table_name", qxt_settings.app_string_table_name )

  CALL configWriteToArray(p_cfg_file_id, "[Language]", "english_file_ext",    qxt_settings.language_file_ext[1])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "spanish_file_ext",    qxt_settings.language_file_ext[2])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "german_file_ext",     qxt_settings.language_file_ext[3])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "french_file_ext",     qxt_settings.language_file_ext[4])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "arabic_file_ext",     qxt_settings.language_file_ext[5])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "italian_file_ext",    qxt_settings.language_file_ext[6])
  CALL configWriteToArray(p_cfg_file_id, "[Language]", "other_file_ext",      qxt_settings.language_file_ext[7])



  #write to print-html section
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_output", qxt_settings.html_print_output )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_1", qxt_settings.html_print_template[1] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_2", qxt_settings.html_print_template[2] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_3", qxt_settings.html_print_template[3] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_4", qxt_settings.html_print_template[4] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_5", qxt_settings.html_print_template[5] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_6", qxt_settings.html_print_template[6] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_7", qxt_settings.html_print_template[7] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html]", "html_print_template_8", qxt_settings.html_print_template[8] )

  #write to print-html-image section
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_1", qxt_settings.html_print_image[1] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_2", qxt_settings.html_print_image[2] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_3", qxt_settings.html_print_image[3] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_4", qxt_settings.html_print_image[4] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_5", qxt_settings.html_print_image[5] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_6", qxt_settings.html_print_image[6] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_7", qxt_settings.html_print_image[7] )
  CALL configWriteToArray(p_cfg_file_id, "[Print-Html-Image]", "html_print_image_8", qxt_settings.html_print_image[8] )

  #write to dde section
  CALL configWriteToArray(p_cfg_file_id, "[DDE]", "dde_timeout", qxt_settings.dde_timeout)


END FUNCTION


}




