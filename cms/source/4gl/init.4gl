##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

#########################################################################################################
# FUNCTION init_data()
######################################################################################################### 
FUNCTION init_data()
  dEFINE 
    local_debug SMALLINT,
    lib_type    CHAR(2)

  LET local_debug = FALSE


  ######################
  # Specify main configuration file name
  ###################### 
  CALL set_main_cfg_filename("cfg/cms.cfg")


  ######################
  # Initialise tools library
  ###################### 

  CALL init_tools_data()


  ############################
  #Connect to the database
  ############################
  IF get_db_name_tool() IS NOT NULL THEN
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL qxt_connect_db_tool()"
    END IF
    CALL qxt_connect_db_tool()
  END IF

  ############################
  # Application String
  ############################

  #File string library needs to import the string file
  IF get_string_app_lib_info() <> "DB" THEN
    CALL qxt_string_app_init(NULL)
  END IF


  ######################
  # Classic 4GL Help - Multilingual
  ###################### 
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_help_classic_cfg_import(NULL)"
  END IF

  #cfg file import
  CALL process_help_classic_cfg_import(NULL)

  #file based (non-db) library needs to import the unl file
  LET lib_type = get_help_classic_lib_info()
  IF lib_type <> "DB" THEN --File type library
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_classic_init(NULL)"
    END IF
    CALL process_help_classic_init(NULL)
  END IF


  ######################
  # Web Service
  ###################### 
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_webservice_cfg_import(NULL)"
  END IF
  CALL process_webservice_cfg_import(NULL)


  #########GUI client only section ##########################
  IF fgl_fglgui() THEN

    ######################
    # Help Html 
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_help_html_cfg_import(NULL)"
    END IF

    CALL process_help_html_cfg_import(NULL)

    #file based (non-db) library needs to import the unl file
    LET lib_type = get_help_html_url_map_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL init_help_html_url_map(NULL)"
      END IF
      CALL init_help_html_url_map(NULL)
    END IF



    ######################
    # DDE
    ######################  
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_dde_cfg_import(NULL)"
    END IF

    CALL process_dde_cfg_import(NULL)

    #file based (non-db) library needs to import the unl file
    LET lib_type = get_dde_font_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_dde_font_init(NULL)"
      END IF
      CALL process_dde_font_init(NULL)
    END IF



    ######################
    # Print Html
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_print_html_cfg_import(NULL)"
    END IF
    CALL process_print_html_cfg_import(NULL)

    #html print template list
    LET lib_type = get_print_html_template_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_print_html_template_init(NULL)"
      END IF
      CALL process_print_html_template_init(NULL)
    END IF

    #html print image list
    LET lib_type = get_print_html_image_lib_info()
    IF lib_type <> "DB" THEN --File type library
      IF local_debug THEN
        DISPLAY "init_tools_data() - CALL process_print_html_image_init(NULL)"
      END IF

      CALL process_print_html_image_init(NULL)
    END IF



    ######################
    # Online Resource (i.e. self running demo from webserver
    ###################### 
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_online_resource_cfg_import(NULL)"
    END IF
    CALL process_online_resource_cfg_import(NULL)


  END IF





END FUNCTION






