##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the application
#
#
#
################################################################################


################################################################################
# DATABASE
################################################################################
DATABASE cms

################################################################################
# GLOBALS
################################################################################
GLOBALS


  DEFINE current_operator_id LIKE operator.operator_id   --user/operator id (will be retrieved during login via a user name/password query)
  DEFINE g_contact 	RECORD LIKE contact.*
  DEFINE g_company 	RECORD LIKE company.*
  DEFINE g_activity 	RECORD LIKE activity.*
  DEFINE g_operator	RECORD LIKE operator.*
  # This is soooo not how we want to do it, but hey. Sue me.
  DEFINE g_picture	BYTE

  DEFINE err_msg, tmp_str,tmp_str1,tmp_str2 VARCHAR(300)

  DEFINE report_dev  VARCHAR(20)

  #Type definitions
  DEFINE t_cms_info TYPE AS 
    RECORD
      db_version VARCHAR(10),
      db_build   VARCHAR(10),
      db_other   vARCHAR(10)
    END RECORD

  DEFINE t_comp_arr TYPE AS
    RECORD 
      comp_id 	        LIKE company.comp_id,
      comp_name         LIKE company.comp_name,
      comp_city         LIKE company.comp_city,
      comp_country      LIKE company.comp_country,
      comp_main_cont    LIKE contact.cont_name,
      comp_mgr          LIKE operator.name
    END RECORD

  DEFINE t_wh_temp  TYPE AS
    RECORD
      id                LIKE stock_item.stock_id,
      amount            INTEGER
    END RECORD

  DEFINE t_comp_short_arr TYPE AS
    RECORD
      comp_id           LIKE company.comp_id,
      comp_name         LIKE company.comp_name,
      comp_city         LIKE company.comp_city
    END RECORD

  DEFINE t_company_rec  TYPE AS
    RECORD
      comp_name	        LIKE company.comp_name,
      comp_addr1        LIKE company.comp_addr1,
      comp_addr2        LIKE company.comp_addr2,
      comp_addr3        LIKE company.comp_addr3,
      comp_city         LIKE company.comp_city,
      comp_zone         LIKE company.comp_zone,
      comp_zip          LIKE company.comp_zip,
      comp_country      LIKE company.comp_country,
      name              LIKE operator.name,
      itype_name        LIKE industry_type.itype_name,
      comp_priority     LIKE company.comp_priority,
      ctype_name        LIKE company_type.ctype_name,
      comp_url          LIKE company.comp_url,
      cont_name         LIKE contact.cont_name,
      comp_notes        LIKE company.comp_notes
    END RECORD


  DEFINE t_company_short_rec  TYPE AS
    RECORD
      comp_id      LIKE company.comp_id,
      comp_name    LIKE company.comp_name,
      comp_addr1   LIKE company.comp_addr1,
      comp_addr2   LIKE company.comp_addr2,
      comp_city    LIKE company.comp_city,
      comp_zone    LIKE company.comp_zone,
      comp_zip     LIKE company.comp_zip,
      comp_country LIKE company.comp_country

    END RECORD

#-------------------------------
###### contact type records###
#-------------------------------

  DEFINE t_cont_rec TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      cont_dept      LIKE contact.cont_dept,
      cont_position  LIKE contact.cont_position,
      comp_name      LIKE company.comp_name,
      cont_usemail   LIKE contact.cont_usemail,
      cont_usephone  LIKE contact.cont_usephone,
      cont_notes     LIKE contact.cont_notes,
      cont_picture   LIKE contact.cont_picture
    END RECORD

  DEFINE t_contact_short_rec TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
    END RECORD

  DEFINE t_contact_address_rec TYPE AS
    RECORD 
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name
    END RECORD




  DEFINE t_cont_rec1 TYPE AS
    RECORD
      cont_id           LIKE contact.cont_id,
      cont_name	        LIKE contact.cont_name,
      cont_title        LIKE contact.cont_title,
      cont_fname        LIKE contact.cont_fname,
      cont_lname        LIKE contact.cont_lname,
      cont_phone        LIKE contact.cont_phone
    END RECORD

  DEFINE t_cont_rec2 TYPE AS
    RECORD
      cont_id           LIKE contact.cont_id,
      cont_name	        LIKE contact.cont_name,
      cont_title        LIKE contact.cont_title,
      cont_fname        LIKE contact.cont_fname,
      cont_lname        LIKE contact.cont_lname,
      cont_phone        LIKE contact.cont_phone,
      comp_name         LIKE company.comp_name
    END RECORD

  DEFINE t_cont_rec3 TYPE AS 
    RECORD
      cont_id           LIKE contact.cont_id,
      cont_name         LIKE contact.cont_name,
      cont_fname        LIKE contact.cont_fname,
      cont_lname        LIKE contact.cont_lname,
      comp_name         LIKE company.comp_name
    END RECORD

  DEFINE t_cont_rec4 TYPE AS
    RECORD
      cont_id           LIKE contact.cont_id,
      cont_name	        LIKE contact.cont_name,
      cont_fname        LIKE contact.cont_fname,
      cont_lname        LIKE contact.cont_lname,
      cont_phone        LIKE contact.cont_phone,
      comp_name         LIKE company.comp_name
    END RECORD


  #DEFINE
   # p_order_field_company, p_order_field_contact VARCHAR(30)




#---------------------------------
###### #Activity record types ###
#---------------------------------

  DEFINE t_act_rec2 TYPE AS
    RECORD 
      activity_id LIKE activity.activity_id,
      comp_name   LIKE company.comp_name,
      cont_name   LIKE contact.cont_name,
      operator_name   LIKE operator.name,
      short_desc  LIKE activity.short_desc,
      atype_name  LIKE activity_type.atype_name,
      priority    LIKE activity.priority,
      open_date   LIKE activity.open_date,
      close_date  LIKE activity.close_date
    END RECORD


  DEFINE t_act_arr3 TYPE AS
    RECORD 
      activity_id LIKE activity.activity_id,
      open_date   LIKE activity.open_date,
      close_date  LIKE activity.close_date,
      cont_name   LIKE operator.name,
      priority    LIKE activity.priority,
      comp_name   LIKE company.comp_name,
      atype_name  LIKE activity_type.atype_name,
      short_desc  LIKE activity.short_desc,
      long_desc   LIKE activity.long_desc,
      operator_id     LIKE activity.operator_id
  END RECORD

  DEFINE t_act_arr4 TYPE AS
    RECORD 
      activity_id   LIKE activity.activity_id,
      open_date     LIKE activity.open_date,
      close_date    LIKE activity.close_date,
      cont_name     LIKE operator.name,
      priority      LIKE activity.priority,
      comp_name     LIKE company.comp_name,
      atype_name    LIKE activity_type.atype_name,
      short_desc    LIKE activity.short_desc,
      long_desc     LIKE activity.long_desc
    END RECORD


  DEFINE t_act_rec5 TYPE AS
    RECORD
      open_date     LIKE activity.open_date,
      name          LIKE operator.name,
      priority      LIKE activity.priority,
      cont_name     LIKE contact.cont_name,
      comp_name     LIKE company.comp_name,
      atype_name    LIKE activity_type.atype_name,
      short_desc    LIKE activity.short_desc,
      long_desc     LIKE activity.long_desc
    END RECORD

  DEFINE t_act_rec6 TYPE AS  
    RECORD 
      activity_id     LIKE activity.activity_id,
      atype_name      LIKE activity_type.atype_name,
      open_date       LIKE activity.open_date,
      name            LIKE operator.name,
      short_desc      LIKE activity.short_desc,
      priority        LIKE activity.priority
    END RECORD

  DEFINE t_act_rec7 TYPE AS
    RECORD
      long_desc   LIKE activity.long_desc
    END RECORD

  #Activity_Type Types
  DEFINE t_act_type_rec TYPE AS
    RECORD
      type_id       LIKE activity_type.type_id,
      atype_name    LIKE activity_type.atype_name,
      user_def      LIKE activity_type.user_def
    END RECORD

  DEFINE t_act_type_rec2 TYPE AS
    RECORD
      atype_name    LIKE activity_type.atype_name,
      user_def      LIKE activity_type.user_def
    END RECORD
 

#---------------------------------
####### Account record Types ###
#---------------------------------
  

  DEFINE t_acc_rec TYPE AS 
    RECORD
      account_id      LIKE account.account_id,
      comp_name       LIKE company.comp_name,
      comp_addr1      LIKE company.comp_addr1,
      comp_addr2      LIKE company.comp_addr2,
      comp_city       LIKE company.comp_city,
      comp_zone       LIKE company.comp_zone,
      comp_zip        LIKE company.comp_zip,
      comp_country    LIKE company.comp_country,
      credit_limit    LIKE account.credit_limit,
      discount        LIKE account.discount
    END RECORD 

  DEFINE t_acc_rec2 TYPE AS
    RECORD
      account_id      LIKE account.account_id,
      comp_name       LIKE company.comp_name,
      cont_name       LIKE contact.cont_name,
      credit_limit    LIKE account.credit_limit,
      discount        LIKE account.discount,
      acct_mgr        LIKE operator.name
    END RECORD


  DEFINE t_acc_filter_rec TYPE AS
    RECORD 
      f_name     INTEGER,
      id_type    INTEGER,
      obj_name   LIKE company.comp_name,
      f_type     INTEGER,
      type_name  LIKE activity_type.atype_name,
      f_status   INTEGER,
      stat       INTEGER,
      f_odate    INTEGER,
      odate_sel  VARCHAR(10),
      open_date  DATE,
      f_cdate    INTEGER,
      cdate_sel  VARCHAR(10),
      close_date DATE
    END RECORD

#---------------------------------
######## Country Record Types ###
#---------------------------------  
 
  DEFINE t_country_rec TYPE AS
    RECORD
      country LIKE country.country
    END RECORD


#---------------------------------
######## Currency Record Type ###
#---------------------------------  
 
  DEFINE t_currency_rec TYPE AS
    RECORD
      currency_name LIKE currency.currency_name  
    END RECORD



#-------------------------------------
######## Delivery Type Record Type ###
#-------------------------------------  
  DEFINE t_delivery_type_rec TYPE AS 
    RECORD
     delivery_type_name LIKE delivery_type.delivery_type_name  
    END RECORD



#-------------------------------------
######## Industry Type Record Type ###
#------------------------------------- 
  DEFINE t_industry_type_rec TYPE AS 
    RECORD
      itype_name LIKE industry_type.itype_name  
    END RECORD


#-------------------------------------
######## Invoice Record Type ###
#------------------------------------- 
  DEFINE t_invoice_rec TYPE AS
    RECORD
      invoice_po             LIKE invoice.invoice_po,
      #pay_method_id         LIKE pay_methods.pay_method_id,
      pay_method_name        LIKE pay_methods.pay_method_name,
      pay_method_desc        LIKE invoice.pay_method_desc,
      del_address1           LIKE invoice.del_address1,
      del_address2           LIKE invoice.del_address2,
      del_address3           LIKE invoice.del_address3,
      del_address_dif        LIKE invoice.del_address_dif,
      del_method             LIKE invoice.del_method,
      currency_name          LIKE currency.currency_name,
      xchg_rate              LIKE currency.xchg_rate,
      #status LIKE invoice.status,
      status_name LIKE status_invoice.status_name
    END RECORD

    DEFINE t_invoice_short_rec TYPE AS
      RECORD
        invoice_id    LIKE invoice.invoice_id,
        comp_name     LIKE company.comp_name,
        net_total     LIKE invoice.net_total,
        tax_total     LIKE invoice.tax_total,
        inv_total     LIKE invoice.inv_total,
        invoice_date  LIKE invoice.invoice_date,
        status_name VARCHAR(20)
      END RECORD

    DEFINE t_warehouse_rec TYPE AS
      RECORD
        stock_id    LIKE stock_item.stock_id,
        item_desc   LIKE stock_item.item_desc,
        quantity    INTEGER,
        reserved    INTEGER,
        shot_term   INTEGER,
        long_term   INTEGER,
        required    INTEGER
      END RECORD


#-------------------------------------
######## Invoice Line Record Type ###
#-------------------------------------

  DEFINE t_inv_line TYPE AS
    RECORD
      stock_id LIKE stock_item.stock_id,
      quantity LIKE invoice_line.quantity,
      item_cost LIKE stock_item.item_cost,
      item_desc LIKE stock_item.item_desc,
      line_net_total MONEY(8,2),
      line_tax_total MONEY(8,2),
      line_total MONEY(8,2)
    END RECORD


#-------------------------------------
######## Supplies Record Type ###
#------------------------------------- 
  DEFINE t_supplies_rec TYPE AS
    RECORD
      suppl_id        LIKE supplies.suppl_id,
      suppl_date      LIKE supplies.suppl_date,
      exp_date        LIKE supplies.exp_date,
      status_name     VARCHAR(20)
    END RECORD

    DEFINE t_supplies_short_rec TYPE AS
      RECORD
        suppl_id      LIKE supplies.suppl_id,
        name          LIKE operator.name,
        suppl_date    LIKE supplies.suppl_date,
        exp_date      LIKE supplies.exp_date,
        status_name   VARCHAR(20),
        comp_name     LIKE company.comp_name
      END RECORD


#-------------------------------------
######## Supplies Line Record Type ###
#-------------------------------------

  DEFINE t_supp_line TYPE AS
    RECORD
      stock_id LIKE stock_item.stock_id,
      quantity LIKE supplies_line.quantity,
      item_cost LIKE stock_item.item_cost,
      item_desc LIKE stock_item.item_desc
#      ,line_net_total MONEY(8,2),
#      line_tax_total MONEY(8,2),
#      line_total MONEY(8,2)
    END RECORD


#-------------------------------------
######## Mailbox Record Type ###
#-------------------------------------

  DEFINE t_mailbox_rec TYPE AS
    RECORD
      recv_date          LIKE mailbox.recv_date,
      cont_name          LIKE contact.cont_name,
      from_email_address LIKE mailbox.from_email_address,
      to_email_address   LIKE mailbox.to_email_address,
      short_desc         LIKE activity.short_desc,
      read_flag          LIKE mailbox.read_flag,
      mail_status        LIKE mailbox.mail_status,
      urgent_flag        LIKE mailbox.urgent_flag,
      mail_id            LIKE mailbox.mail_id,
      long_desc          LIKE activity.long_desc
    END RECORD

  DEFINE t_mailbox_rec2 TYPE AS
     RECORD
        mail_id            LIKE mailbox.mail_id,
        recv_date          LIKE mailbox.recv_date,
        cont_name          LIKE contact.cont_name,
        from_email_address LIKE mailbox.from_email_address,
        to_email_address   LIKE mailbox.to_email_address,
        short_desc         LIKE activity.short_desc,
        read_flag          VARCHAR(3),  --LIKE mailbox.read_flag,
        mail_status        VARCHAR(3),
        #checked           CHAR,
        urgent_flag        LIKE mailbox.urgent_flag
     END RECORD


  DEFINE t_mailbox_in_scroll_rec TYPE AS
     RECORD
        mail_id            LIKE mailbox.mail_id,
        recv_date          LIKE mailbox.recv_date,
        cont_name          LIKE contact.cont_name,
        from_email_address LIKE mailbox.from_email_address,
        to_email_address   LIKE mailbox.to_email_address,
        short_desc         LIKE activity.short_desc,
        read_flag          VARCHAR(3),  --LIKE mailbox.read_flag,
        urgent_flag        VARCHAR(3), --LIKE mailbox.urgent_flag,
        mail_status        VARCHAR(3),
        long_desc          LIKE activity.long_desc
     END RECORD

  DEFINE t_mailbox_out_scroll_rec TYPE AS
     RECORD
        mail_id            LIKE mailbox.mail_id,
        recv_date          LIKE mailbox.recv_date,
        cont_name          LIKE contact.cont_name,
        from_email_address LIKE mailbox.from_email_address,
        to_email_address   LIKE mailbox.to_email_address,
        short_desc         LIKE activity.short_desc,
        #read_flag          VARCHAR(3),  --LIKE mailbox.read_flag,
        #checked           CHAR,
        urgent_flag        VARCHAR(3),
        mail_status        VARCHAR(3),
        long_desc          LIKE activity.long_desc
     END RECORD

  DEFINE t_mailbox_rec3 TYPE AS
    RECORD
      mail_id      LIKE mailbox.mail_id,
      recv_date    LIKE mailbox.recv_date,
      read_flag    LIKE mailbox.read_flag,
      mail_status  LIKE mailbox.mail_status,
      urgent_flag  LIKE mailbox.urgent_flag,
      short_desc   LIKE activity.short_desc,
      cont_name    LIKE contact.cont_name,
      long_desc    LIKE activity.long_desc
    END RECORD

  DEFINE t_mailbox_rec4 TYPE AS
    RECORD
      cont_name   LIKE contact.cont_name,
      short_desc  LIKE activity.short_desc,
      recv_date   LIKE mailbox.recv_date,
      read_flag    LIKE mailbox.read_flag,
      mail_status  LIKE mailbox.mail_status,
      checked     CHAR
    END RECORD

  DEFINE t_mailbox_rec5 TYPE AS
    RECORD
      long_desc   LIKE activity.long_desc,
      mail_id     LIKE mailbox.mail_id
    END RECORD


#-------------------------------------
######## Email Record TYPE ###
#-------------------------------------
  DEFINE t_email_rec TYPE AS
    RECORD
      mail_id              LIKE mailbox.mail_id,
      activity_id          LIKE activity.activity_id,
      from_email_address   LIKE mailbox.from_email_address,
      from_cont_id         LIKE contact.cont_id,
      from_cont_fname      LIKE contact.cont_fname,
      from_cont_lname      LIKE contact.cont_lname,
      to_email_address     LIKE mailbox.to_email_address,
      to_cont_id           LIKE contact.cont_id,
      to_cont_fname        LIKE contact.cont_fname,
      to_cont_lname        LIKE contact.cont_lname,
      recv_date            LIKE mailbox.recv_date,
      read_flag            LIKE mailbox.read_flag,
      urgent_flag          LIKE mailbox.urgent_flag,
      short_desc           LIKE activity.short_desc,
      long_desc            LIKE activity.long_desc,
      mail_status          LIKE mailbox.mail_status,
      operator_id          LIKE mailbox.operator_id
    END RECORD

  DEFINE t_email_form_rec TYPE AS
    RECORD
      #mail_id              LIKE mailbox.mail_id,
      #activity_id          LIKE activity.activity_id,
      from_email_address   LIKE mailbox.from_email_address,
      from_cont_id         LIKE contact.cont_id,
      from_cont_fname      LIKE contact.cont_fname,
      from_cont_lname      LIKE contact.cont_lname,
      to_email_address     LIKE mailbox.to_email_address,
      to_cont_id           LIKE contact.cont_id,
      to_cont_fname        LIKE contact.cont_fname,
      to_cont_lname        LIKE contact.cont_lname,
      recv_date            LIKE mailbox.recv_date,
      read_flag            LIKE mailbox.read_flag,
      urgent_flag          LIKE mailbox.urgent_flag,
      short_desc           LIKE activity.short_desc,
      long_desc            LIKE activity.long_desc,
      mail_status          LIKE mailbox.mail_status
      #operator_id          LIKE mailbox.operator_id
    END RECORD


#-------------------------------------
######## Payment Method TYPE ###
#-------------------------------------    
  DEFINE t_pay_method_name_rec TYPE AS
    RECORD
     pay_method_name LIKE pay_methods.pay_method_name  
    END RECORD


#-------------------------------------
######## Invoice Status TYPE ###
#-------------------------------------    
  DEFINE t_inv_status_name_rec TYPE AS
    RECORD
     inv_status_name LIKE status_invoice.status_name  
    END RECORD

#-------------------------------------
######## Supply State TYPE ###
#-------------------------------------    
  DEFINE t_sup_state_name_rec TYPE AS
    RECORD
     sup_state_name LIKE state_supply.state_name  
    END RECORD



  #Position (contact) TYPES
  DEFINE t_position_name_rec TYPE AS
    RECORD
      ptype_name LIKE position_type.ptype_name  
    END RECORD

  #Stock item Types
  DEFINE t_stock_rec TYPE AS
    RECORD
      stock_id  LIKE stock_item.stock_id,
      item_desc LIKE stock_item.item_desc,
      item_cost LIKE stock_item.item_cost,
      tax_rate  LIKE tax_rates.tax_rate,
      quantity  INTEGER,
      reserved  INTEGER,
      available INTEGER,
      ordered   INTEGER
    END RECORD 

  DEFINE t_stock_rec2 TYPE AS
    RECORD
      stock_id  LIKE stock_item.stock_id,
      item_desc LIKE stock_item.item_desc,
      item_cost LIKE stock_item.item_cost,
      tax_rate  LIKE tax_rates.tax_rate,
      tax_desc LIKE tax_rates.tax_desc
    END RECORD

  #Title (contact) types  
  DEFINE t_title_rec TYPE AS
    RECORD
      title LIKE title.title
    END RECORD


  #operator (as in cms operator) types  
  DEFINE t_operator_rec TYPE AS
    RECORD
      name 	LIKE operator.name,
      password 	LIKE operator.password
    END RECORD

  DEFINE t_operator_rec2 TYPE AS
    RECORD 
      operator_id LIKE operator.operator_id,
      name LIKE operator.name,
      password LIKE operator.password,
      type LIKE operator.type,
      cont_name LIKE contact.cont_name,
      email_address LIKE operator.email_address
    END RECORD 


	DEFINE t_operator_session_rec2 TYPE AS
		RECORD
			operator_id			LIKE operator_session.operator_id,
			name						LIKE operator.name,	
			session_id			LIKE operator_session.session_id,
			session_counter	LIKE operator_session.session_counter,
			expired					LIKE operator_session.expired,
			session_created		LIKE operator_session.session_created,
			session_modified	LIKE operator_session.session_modified,
			client_host			LIKE operator_session.client_host,
			client_ip				LIKE operator_session.client_ip,
			reserve1				LIKE operator_session.reserve1,																					
			reserve2				LIKE operator_session.reserve2,				
			cont_id					LIKE contact.cont_id,	
			cont_fname			LIKE contact.cont_fname,
			cont_lname			LIKE contact.cont_lname					
		END RECORD
	

	

  DEFINE t_operator_passwd_rec TYPE AS
    RECORD 
      passwd1 LIKE operator.password,
      passwd_confirm LIKE operator.password
    END RECORD


#menu options
  DEFINE t_menu_options_rec TYPE AS
    RECORD
      option_name     LIKE qxt_string_app.string_data,
      option_comment  LIKE qxt_string_app.string_data,
      option_keypress LIKE menu_options.option_keypress,
      option_id       LIKE menu_options.option_id
    END RECORD

  DEFINE main_menu DYNAMIC ARRAY OF t_menu_options_rec  --RECORD LIKE menu_options.*
  DEFINE gl_std_menu  DYNAMIC ARRAY OF t_menu_options_rec  --RECORD LIKE menu_options.*



  DEFINE t_contact_ws_rec TYPE AS
    RECORD 
      cont_name      LIKE contact.cont_name,
      cont_title     LIKE contact.cont_title,
      cont_fname     LIKE contact.cont_fname,
      cont_lname     LIKE contact.cont_lname,
      cont_addr1     LIKE contact.cont_addr1,
      cont_addr2     LIKE contact.cont_addr2,
      cont_addr3     LIKE contact.cont_addr3,
      cont_city      LIKE contact.cont_city,
      cont_zone      LIKE contact.cont_zone,
      cont_zip       LIKE contact.cont_zip,
      cont_country   LIKE contact.cont_country,
      cont_phone     LIKE contact.cont_phone,
      cont_mobile    LIKE contact.cont_mobile,
      cont_fax       LIKE contact.cont_fax,
      cont_email     LIKE contact.cont_email,
      comp_name      LIKE company.comp_name,
      #cont_picture   LIKE contact.cont_picture,
      cont_notes     LIKE contact.cont_notes
    END RECORD
    
#### Supplies
  DEFINE status_msg        DYNAMIC ARRAY OF VARCHAR(20)
 DEFINE g_suppl_lines     DYNAMIC ARRAY OF t_supp_line   --huho - not sure why this array was made global scope

	DEFINE md_lacy INT

END GLOBALS




