##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


#######################################################
# FUNCTION populate_contact_form_labels_g()
#
# Populate contact form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_email_form_labels_g()
  DISPLAY get_str(400) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10

  #DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO bt_ok

  #DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(400))  --E-Mail

END FUNCTION






#######################################################
# FUNCTION populate_email_form_view_labels_g()
#
# Populate email view form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_email_form_view_labels_g()
  CALL updateUILabel("cntDetail2GroupBox","E-Mail Body")
  CALL updateUILabel("read_flag","Read")
  CALL updateUILabel("urgent_flag","Urgent")
  CALL updateUILabel("mail_status","Status") 
  DISPLAY get_str(437) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10

#  DISPLAY get_str(810) TO bt_ok
#  DISPLAY "!" TO bt_ok

#  DISPLAY get_str(820) TO bt_cancel
#  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(400))  --E-Mail

END FUNCTION





#######################################################
# FUNCTION populate_email_form_reply_labels_g()
#
# Populate email reply form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_email_form_reply_labels_g()
  DISPLAY get_str(438) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10

  CALL fgl_settitle(get_str(438))  --"Reply to Email (Send)")
  #DISPLAY get_str(825) TO bt_ok
  #DISPLAY get_str(820) TO bt_cancel #DISPLAY "!" TO bt_cancel
END FUNCTION




#######################################################
# FUNCTION populate_email_form_forward_labels_g()
#
# Populate email forward form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_email_form_forward_labels_g()
  CALL updateUILabel("cntDetail2GroupBox","E-Mail Body")
  CALL updateUILabel("read_flag","Read")
  CALL updateUILabel("urgent_flag","Urgent")
  CALL updateUILabel("mail_status","Status") 
  DISPLAY get_str(440) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10

  #DISPLAY get_str(825) TO bt_ok #DISPLAY "!" TO bt_ok
  #DISPLAY get_str(820) TO bt_cancel #DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(440))  --"Forward to Email (Send)")



END FUNCTION






#######################################################
# FUNCTION populate_email_form_redirect_labels_g()
#
# Populate email redirect form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_email_form_redirect_labels_g()
  CALL updateUILabel("cntDetail2GroupBox","E-Mail Body")
  CALL updateUILabel("read_flag","Read")
  CALL updateUILabel("urgent_flag","Urgent")
  CALL updateUILabel("mail_status","Status") 
  DISPLAY get_str(441) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10

  #DISPLAY get_str(825) TO bt_ok #DISPLAY "!" TO bt_ok
  #DISPLAY get_str(820) TO bt_cancel #DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(441))  --"Re-direct  Email (Send)")


END FUNCTION




#######################################################
# FUNCTION populate_mailbox2_detail_form_labels_g()
#
# Populate mailbox2 form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_mailbox2_detail_form_labels_g()
  DISPLAY get_str(400) TO lbTitle
  #DISPLAY get_str(401) TO dl_f1
  DISPLAY get_str(402) TO dl_f2
  DISPLAY get_str(403) TO dl_f3
  DISPLAY get_str(404) TO dl_f4
  #DISPLAY get_str(405) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  #DISPLAY get_str(381) TO dl_f11
  #DISPLAY get_str(382) TO dl_f12
  #DISPLAY get_str(383) TO dl_f13  --Plz
  #DISPLAY get_str(384) TO dl_f14
  #DISPLAY get_str(385) TO dl_f15
  #DISPLAY get_str(386) TO dl_f16
  #DISPLAY get_str(387) TO dl_f17
  #DISPLAY get_str(388) TO dl_f18
  #DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
END FUNCTION



