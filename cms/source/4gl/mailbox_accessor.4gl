##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

########################################################################################################################
# Mailbox functions
########################################################################################################################
#
# FUNCTION:                                             DESCRIPTION:                               RETURN:
# mailbox(value)                                        Mailbox main menu                          NONE
# open_mailbox(p_operator_id)                           Old mailbox function - not used            NONE
# get_contact_id_from_email(p_email)                    get contact_id of the email address        l_contact_id
# get_mailbox_rec_from_mail_id(p_mail_id)               get the mailbox record by using the id     l_mailbox_rec.*
# get_long_desc_from_email(p_email)                     Returns the long description of an email   l_contact_id
# open_operator_mail_inbox_data_source(p_operator_id)   Data Source for operator-Mail-Inbox        NONE
# open_operator_mail_inbox(p_operator_id)               Mail Inbox from current operator           NONE
# open_operator_mail_outbox_data_source(p_operator_id)  Data Source for operator-Mail-Outbox       NONE
# open_operator_mail_outbox(p_operator_id)              Display Mail-Outbox from current operator  NONE
#
#
########################################################################################################################


#################################################
# GLOBALS
#################################################
GLOBALS "globals.4gl"


{
#######################################################
# FUNCTION get_contact_id_from_email(p_email)
#
# Returns the contact_id of the corresponding email address
#
# RETURN l_contact_id
#######################################################
FUNCTION get_contact_id_from_email_address(p_email)
   DEFINE 
     p_email       LIKE contact.cont_email,
     l_contact_id  LIKE mailbox.from_cont_id


   SELECT contact.cont_id
     INTO l_contact_id
     FROM contact
     WHERE contact.cont_email = p_email

   RETURN l_contact_id
END FUNCTION
}



#######################################################
# FUNCTION get_mailbox_rec_from_mail_id(p_mail_id)
#
# get the mailbox record by using the id
#
# RETURN l_mailbox_rec.*
#######################################################
FUNCTION get_mailbox_rec_from_mail_id(p_mail_id)
   DEFINE 
     p_mail_id       LIKE mailbox.mail_id,
     l_mailbox_rec    RECORD LIKE mailbox.*


   SELECT *
     INTO l_mailbox_rec
     FROM mailbox
     WHERE mailbox.mail_id = p_mail_id

   RETURN l_mailbox_rec.*
END FUNCTION



#######################################################
# FUNCTION get_long_desc_from_email(p_email)
#
# Returns the long description of an email
#
# RETURN l_contact_id
#######################################################
FUNCTION get_long_desc_from_email(p_email)
   DEFINE 
     p_email       LIKE contact.cont_email,
     l_long_desc   LIKE activity.long_desc

  LOCATE l_long_desc  IN MEMORY
  
  #DISPLAY "p_email=", p_email

   SELECT activity.long_desc
     INTO l_long_desc
     FROM activity, mailbox
     WHERE mailbox.mail_id = p_email
       AND mailbox.activity_id = activity.activity_id

  RETURN l_long_desc
  FREE l_long_desc
END FUNCTION

