##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage activities
##################################################################################################################
#
# FUNCTION:                                      DESCRIPTION:                                            RETURN:
# country_combo_list()                           Populate the combo list box from DB                     NONE
# country_popup_data_source()                    Data Source (cursor) for the country_popup() function   NONE
# country_popup()                                popup a country lookup window with table management t.  p_country   (p_country LIKE country.country  )
# country_edit(p_country)                        Edit country record                                     NONE
# country_view(p_country_rec)                    DISPLAY country details in a form                       NONE
# country_input(p_country)                       Input country details from form                         l_country.*
# country_delete(p_country)                      Delete Country record                                   NONE
# country_create()                               Create new country record                               NONE
# country_advanced_lookup()                      Allows to search by entering the first letters          rv_comp_id
# country_advanced_lookup_update                 sub-routine of country_advanced_lookup to               rv_comp_id  (LIKE contact.cont_id)
# (query_filter, field_filter, do_choose)        update display
# get_country_rec(p_country)                     get country record from country                         r_country_rec.*
# 
#
# 
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



###################################################################################
# FUNCTION country_combo_list()
#
# Populate the combo list box from DB
#
# RETURN NONE
###################################################################################
FUNCTION country_combo_list(cb_field_name)
  DEFINE l_country_arr DYNAMIC ARRAY OF t_country_rec,
    rv                 LIKE country.country,  --return value = found invoice_id
    row_count          INTEGER,
    current_row        INTEGER,
    cb_field_name      VARCHAR(20),   --form field name for the country combo list field
    abort_flag        SMALLINT
 
  LET abort_flag = FALSE

  DECLARE c_country_scroll2 CURSOR FOR 
    SELECT country.country
      FROM country
      ORDER BY country.country ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_country_scroll2 INTO l_country_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_country_arr[row_count].country)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1
		If row_count > 0 THEN
			CALL l_country_arr.resize(row_count)  --remove the last item which has no data
		END IF  
END FUNCTION



####################################################
# FUNCTION get_country_rec(p_country)
#
# get country record from country
#
# RETURN r_country_rec.*
####################################################
FUNCTION get_country_rec(p_country)
  DEFINE
    p_country     LIKE country.country,
    r_country_rec RECORD LIKE country.*

  SELECT * 
    INTO r_country_rec 
    FROM country
    WHERE country.country = p_country

  RETURN r_country_rec.*
END FUNCTION
