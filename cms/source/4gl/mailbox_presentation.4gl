##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_mail_inbox_operator()
####################################################
FUNCTION grid_header_mail_inbox_operator()
  CALL fgl_grid_header("sa_mailbox","mail_id",get_str(580),"right","F13")                 --"ID"
  CALL fgl_grid_header("sa_mailbox","recv_date",get_str(581),"left","F14")                --Date
  CALL fgl_grid_header("sa_mailbox","cont_name",get_str(582),"left","F15")                --Contact
  CALL fgl_grid_header("sa_mailbox","from_email_address",get_str(583),"left","F16")       --From
  CALL fgl_grid_header("sa_mailbox","to_email_address",get_str(584),"left","F17")         --To
  CALL fgl_grid_header("sa_mailbox","short_desc",get_str(585),"left","F18")               --Short Description
  CALL fgl_grid_header("sa_mailbox","read_flag",get_str(586),"center","F19")              --Read
  CALL fgl_grid_header("sa_mailbox","urgent_flag",get_str(587),"center","F20")            --Priority
  CALL fgl_grid_header("sa_mailbox","mail_status",get_str(588),"center","F21")            --Status
END FUNCTION

####################################################
# FUNCTION grid_header_mail_outbox_operator()
####################################################
FUNCTION grid_header_mail_outbox_operator()
  CALL fgl_grid_header("sa_mailbox","mail_id",get_str(580),"right","F13")                 --"ID"
  CALL fgl_grid_header("sa_mailbox","recv_date",get_str(581),"left","F14")                --Date
  CALL fgl_grid_header("sa_mailbox","cont_name",get_str(582),"left","F15")                --Contact
  CALL fgl_grid_header("sa_mailbox","from_email_address",get_str(583),"left","F16")       --From
  CALL fgl_grid_header("sa_mailbox","to_email_address",get_str(584),"left","F17")         --To
  CALL fgl_grid_header("sa_mailbox","short_desc",get_str(585),"left","F18")               --Short Description
  CALL fgl_grid_header("sa_mailbox","urgent_flag",get_str(587),"center","F19")            --Priority
  CALL fgl_grid_header("sa_mailbox","mail_status",get_str(588),"center","F20")            --Status
END FUNCTION



#######################################################
# FUNCTION populate_mail_outbox_form_labels_t()
#
# Populate mailoutbox form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_mail_outbox_form_labels_t()
  DISPLAY get_str(550) TO lbTitle
  DISPLAY get_str(551) TO dl_f1
  DISPLAY get_str(552) TO dl_f2
  DISPLAY get_str(553) TO dl_f3
  #DISPLAY get_str(554) TO dl_f4
  DISPLAY get_str(555) TO dl_f5
  DISPLAY get_str(556) TO dl_f6
  #DISPLAY get_str(557) TO dl_f7
  #DISPLAY get_str(558) TO dl_f8 
  DISPLAY get_str(559) TO lbInfo1


END FUNCTION

#######################################################
# FUNCTION populate_mail_outbox_form_labels_g()
#
# Populate mailoutbox form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_mail_outbox_form_labels_g()
  DISPLAY get_str(550) TO lbTitle
  CALL UIRadButListItemText("rb_filter_mstatus",1,"Queued Emails")
  CALL UIRadButListItemText("rb_filter_mstatus",2,"Sent Emails")
  CALL UIRadButListItemText("rb_filter_mstatus",3,"All Emails")
  #DISPLAY get_str(551) TO dl_f1
  #DISPLAY get_str(552) TO dl_f2
  #DISPLAY get_str(553) TO dl_f3
  #DISPLAY get_str(554) TO dl_f4
  #DISPLAY get_str(555) TO dl_f5
  #DISPLAY get_str(556) TO dl_f6
  #DISPLAY get_str(557) TO dl_f7
  #DISPLAY get_str(558) TO dl_f8 
  DISPLAY get_str(559) TO lbInfo1

  #DISPLAY "!" TO rb_filter_mstatus

END FUNCTION


#######################################################
# FUNCTION populate_mail_inbox_form_labels_t()
#
# Populate mailoutbox form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_mail_inbox_form_labels_t()
  DISPLAY get_str(560) TO lbTitle
  DISPLAY get_str(561) TO dl_f1
  DISPLAY get_str(562) TO dl_f2
  DISPLAY get_str(563) TO dl_f3
  #DISPLAY get_str(564) TO dl_f4
  DISPLAY get_str(565) TO dl_f5
  DISPLAY get_str(566) TO dl_f6
  DISPLAY get_str(567) TO dl_7
  DISPLAY get_str(568) TO dl_8 
  DISPLAY get_str(569) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_mail_inbox_form_labels_g()
#
# Populate mailoutbox form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_mail_inbox_form_labels_g()
  DISPLAY get_str(560) TO lbTitle
  CALL UIRadButListItemText("rb_open_filter",1,"Opened")
  CALL UIRadButListItemText("rb_open_filter",2,"Closed")
  CALL UIRadButListItemText("rb_open_filter",3,"All")
  #DISPLAY get_str(561) TO dl_f1
  #DISPLAY get_str(562) TO dl_f2
  #DISPLAY get_str(563) TO dl_f3
  #DISPLAY get_str(564) TO dl_f4
  #DISPLAY get_str(565) TO dl_f5
  #DISPLAY get_str(566) TO dl_f6
  #DISPLAY get_str(567) TO dl_7
  #DISPLAY get_str(568) TO dl_8 
  DISPLAY get_str(569) TO lbInfo1

  CALL fgl_settitle(get_str(522))  --"CMS Demo - E-Mail Inbox")
  CALL grid_header_mail_inbox_operator()
--  DISPLAY "!" TO rb_open_filter

END FUNCTION



