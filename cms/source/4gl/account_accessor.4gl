##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage acounts
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                          RETURN:
# get_account_credit_balance(p_account_id)    Retrieve the credit balance for the account           credit_limit - t_inv_balance 
# get_account_balance(p_account_id)           Get the account balance of an account                 l_total_balance
# get_account_rec(p_account_id)               Get the account record                                l_account.* (LIKE account.*)
# get_form_account(p_account_id)              ????                                                  l_account_rec.*  (cust record)
# account_input(p_account)                    input form for new & edit account                     p_account.*
# account_edit(p_account_id)                  Edit account details                                  NONE
# account_create()                            Create a new account                                  l_account.account_id                                
# account_delete(p_account_id)                Delete account record                                 NONE
# account_view(p_account_id)                  View account details in window                        NONE
# account_view_by_rec(p_account_rec)          Display account details on form using record argument NONE
# account_popup_data_source(p_order_field)    Data Source (Cursor) for the account_popup function   NONE
# account_popup(p_account_id,p_order_field)   Display accounts list to select and return id         l_account_arr[i].account_id  (or p_account_id for Cancel)
# account_to_form_account(p_account)          ????                                                  l_account_rec.*
# account_display(p_account_id)               DISPLAY account record                                NONE
# account_count(p_account_id)
# form_account_to_account(p_account_rec)      ????                                                  l_account.*
# disp_account_comp(p_comp_id)                Display extracted company details (shorter version)   l_comp_rec.*
# grid_header_f_account_scroll_g()            Populate grid header details                          NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"





############################################################################################################
# Data Access functions
############################################################################################################




###################################################
# FUNCTION get_account_credit_balance(p_account_id)
#
# Retrieve the credit balance for the account
#
# RETURN credit_limit - t_inv_balance 
###################################################
FUNCTION get_account_credit_balance(p_account_id)
  DEFINE 
    p_account_id  LIKE account.account_id,
    status        SMALLINT,
    credit_limit  LIKE account.credit_limit,
    t_inv_balance LIKE account.credit_limit

  SELECT account.credit_limit
    INTO credit_limit
    FROM account
    WHERE account.account_id = p_account_ID

  LET t_inv_balance = get_account_balance(p_account_id)

  RETURN credit_limit - t_inv_balance 


END FUNCTION


###################################################
# FUNCTION get_account_balance(p_account_id)
#
# Get the account balance of an account
#
# RETURN l_total_balance
###################################################
FUNCTION get_account_balance(p_account_id)
  DEFINE 
    p_account_id LIKE invoice.account_id,
    l_credit_arr DYNAMIC ARRAY OF 
      RECORD
        inv_total       LIKE invoice.inv_total
      END RECORD ,
    l_total_balance LIKE invoice.inv_total ,
    i INTEGER,
    local_debug SMALLINT

  LET local_debug = 0  --0=off 1=on
  LET l_total_balance = 0  --initialise total balance
  LET i = 1

  IF local_debug = 1 THEN
    DISPLAY "get_account_balance() p_account_id= ", p_account_id
  END IF

  DECLARE c_balance CURSOR FOR
    SELECT invoice.inv_total
      FROM invoice
      WHERE account_id = p_account_id
      AND pay_date IS NULL

    FOREACH c_balance INTO l_credit_arr[i].*
    LET l_total_balance = l_total_balance + l_credit_arr[i].inv_total  

    IF local_debug = 1 THEN
      DISPLAY "get_account_balance() Foreach i= ", i
      DISPLAY "get_account_balance() l_credit_arr[i].inv_total=", l_credit_arr[i].inv_total
      DISPLAY "get_account_balance() l_total_balance=", l_total_balance
    END IF

    LET i = i + 1
    #  IF i > 20 THEN
    #    EXIT FOREACH
    #  END IF
    END FOREACH

	If i > 0 THEN
		CALL l_credit_arr.resize(i)   --remove the last item which has no data
	ELSE
		CALL l_credit_arr.clear() 
	END IF

  RETURN l_total_balance

END FUNCTION



###################################################
# FUNCTION get_account_rec(p_account_id)
#
# Get the account record
#
#  RETURN l_account.* (LIKE account.*)
###################################################
FUNCTION get_account_rec(p_account_id)
  DEFINE p_account_id LIKE account.account_id
  DEFINE l_account RECORD LIKE account.*

  SELECT account.*
    INTO l_account.*
    FROM account
    WHERE account.account_id = p_account_id

  RETURN l_account.*
END FUNCTION

###################################################
# FUNCTION get_form_account(p_account_id)
#
# ????
#
#  RETURN l_account_rec.*  (cust record)
###################################################
FUNCTION get_form_account(p_account_id)
  DEFINE 
    p_account_id  LIKE account.account_id,
    l_account     RECORD LIKE account.*,
    l_account_rec OF t_acc_rec


  CALL get_account_rec(p_account_id)
    RETURNING l_account.*

  CALL account_to_form_account(l_account.*)
    RETURNING l_account_rec.*

  RETURN l_account_rec.*
END FUNCTION



####################################################
# FUNCTION account_count(p_account_id)
#
# tests if a record with this account name already exists
#
# RETURN r_count
####################################################
FUNCTION account_count(p_account_id)
  DEFINE
    p_account_id    LIKE account.account_id,
    r_count         SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM account
      WHERE account.account_id = p_account_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION account_company_count(p_company_id)
#
# tests if a record with this company_id already exists in acccounts
# ONE company can only have ZERO or ONE account
#
# RETURN r_count
####################################################
FUNCTION account_company_count(p_company_id)
  DEFINE
    p_company_id    LIKE account.comp_id,
    r_count         SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM account
      WHERE account.comp_id = p_company_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION




############################################################################################################
# Record Copy functions
############################################################################################################


###################################################
# FUNCTION form_account_to_account(p_account_rec)
#
#  ????
#
# RETURN l_account.*
###################################################
FUNCTION form_account_to_account(p_account_rec)
  DEFINE 
    p_account_rec   OF t_acc_rec,
    l_account       RECORD LIKE account.*

  LET l_account.credit_limit = p_account_rec.credit_limit
  LET l_account.discount = p_account_rec.discount
  LET l_account.account_id = p_account_rec.account_id
  LET l_account.comp_id = 1 -- TO DO!!!!

  RETURN l_account.*
END FUNCTION


###################################################
# FUNCTION account_to_form_account(p_account)
#
# ????
#
# RETURN l_account_rec.*
###################################################
FUNCTION account_to_form_account(p_account)
  DEFINE 
    p_account       RECORD LIKE account.*,
    l_account_rec   OF t_acc_rec,
    l_company       RECORD LIKE company.*

  CALL get_company_rec(p_account.comp_id)
    RETURNING l_company.*

  LET l_account_rec.comp_name = l_company.comp_name
  LET l_account_rec.comp_addr1 = l_company.comp_addr1
  LET l_account_rec.comp_addr2 = l_company.comp_addr2
  LET l_account_rec.comp_city = l_company.comp_city
  LET l_account_rec.comp_zone = l_company.comp_zone
  LET l_account_rec.comp_country = l_company.comp_country
  LET l_account_rec.comp_zip = l_company.comp_zip
  LET l_account_rec.discount = p_account.discount
  LET l_account_rec.credit_limit = p_account.credit_limit
  LET l_account_rec.account_id = p_account.account_id

  RETURN l_account_rec.*
END FUNCTION
