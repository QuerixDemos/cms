##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


#######################################################
# FUNCTION populate_company_short_form_labels()
#
# Populate company form labels for gui (short company details)
#
# RETURN NONE
#######################################################
FUNCTION populate_company_short_form_labels()
  DISPLAY get_str(100) TO lbTitle
  DISPLAY get_str(101) TO dl_f1
  DISPLAY get_str(102) TO dl_f2
  DISPLAY get_str(103) TO dl_f3
  DISPLAY get_str(104) TO dl_f4
  DISPLAY get_str(105) TO dl_f5
  DISPLAY get_str(106) TO dl_f6
  DISPLAY get_str(107) TO dl_f7
  #DISPLAY get_str(108) TO dl_f8 
  #DISPLAY get_str(109) TO dl_f9
  #DISPLAY get_str(110) TO dl_f10
  #DISPLAY get_str(111) TO dl_f11
  #DISPLAY get_str(112) TO dl_f12
  #DISPLAY get_str(113) TO dl_f13
  #DISPLAY get_str(114) TO dl_f14

END FUNCTION


#######################################################
# FUNCTION populate_company_form()
#
# Populate company form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_company_form_labels_g()
  DISPLAY get_str(100) TO lbTitle
  DISPLAY get_str(101) TO dl_f1
  DISPLAY get_str(102) TO dl_f2
  DISPLAY get_str(103) TO dl_f3
  DISPLAY get_str(104) TO dl_f4
  DISPLAY get_str(105) TO dl_f5
  DISPLAY get_str(106) TO dl_f6
  DISPLAY get_str(107) TO dl_f7
#  DISPLAY get_str(108) TO dl_f8  --label removed.. but can be used for groupBox title 
  DISPLAY get_str(109) TO dl_f9
  DISPLAY get_str(110) TO dl_f10
  DISPLAY get_str(111) TO dl_f11
  DISPLAY get_str(112) TO dl_f12
  DISPLAY get_str(113) TO dl_f13
  DISPLAY get_str(114) TO dl_f14
  CALL updateUILabel ("cntDetail1GroupBoxLeft","Details")
  CALL updateUILabel ("cntDetail1GroupBoxCenter","Map")
  CALL updateUILabel ("cntDetail1GroupBoxRight",get_str(108))  --"Notes"
  CALL updateUILabel ("cntDetail2GroupBoxLeft","Miscellaneous")
  CALL updateUILabel ("cntDetail2GroupBoxRight","Channel Details")
  
  CALL set_comp_default_titlebar()
  CALL populate_company_form_combo_boxes()  --populate all combo boxes in this form dynamically
  CALL grid_header_company_contact_member()

END FUNCTION




#######################################################
# FUNCTION populate_company_edit_form_labels_g()
#
# Populate company edit form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_company_edit_form_labels_g()
  CALL updateUILabel ("cntDetail1GroupBoxLeft","Details")
  CALL updateUILabel ("cntDetail1GroupBoxCenter","Map")
  CALL updateUILabel ("cntDetail1GroupBoxRight","Notes")
  CALL updateUILabel ("cntDetail2GroupBoxLeft","Miscellaneous")
  CALL updateUILabel ("cntDetail2GroupBoxRight","Channel Details")
  DISPLAY get_str(100) TO lbTitle
  DISPLAY get_str(101) TO dl_f1
  DISPLAY get_str(102) TO dl_f2
  DISPLAY get_str(103) TO dl_f3
  DISPLAY get_str(104) TO dl_f4
  DISPLAY get_str(105) TO dl_f5
  DISPLAY get_str(106) TO dl_f6
  DISPLAY get_str(107) TO dl_f7
  #DISPLAY get_str(108) TO dl_f8 
  DISPLAY get_str(109) TO dl_f9
  DISPLAY get_str(110) TO dl_f10
  DISPLAY get_str(111) TO dl_f11
  DISPLAY get_str(112) TO dl_f12
  DISPLAY get_str(113) TO dl_f13
  DISPLAY get_str(114) TO dl_f14

  CALL set_comp_default_titlebar()
  CALL populate_company_form_combo_boxes()  --populate all combo boxes in this form dynamically
  CALL grid_header_company_contact_member()

END FUNCTION



#######################################################
# FUNCTION populate_company_list_form_labels_g()
#
# Populate company list (popup) form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_company_list_form_labels_g()
  DISPLAY get_str(190) TO lbTitle
  #DISPLAY get_str(191) TO dl_f1
  #DISPLAY get_str(192) TO dl_f2
  #DISPLAY get_str(193) TO dl_f3
  #DISPLAY get_str(194) TO dl_f4
  #DISPLAY get_str(195) TO dl_f5
  #DISPLAY get_str(196) TO dl_f6
  #DISPLAY get_str(197) TO dl_f7
  #DISPLAY get_str(198) TO dl_f8 
  DISPLAY get_str(199) TO lbInfo1

  DISPLAY get_str(824) TO bt_view
  DISPLAY "!" TO bt_view

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(811) TO bt_close
  DISPLAY "!" TO bt_close

  CALL grid_header_company_scroll()
  CALl fgl_settitle(get_str(190))

END FUNCTION






#######################################################
# FUNCTION populate_company_quick_search_form_labels_g()
#
# Populate company quick search form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_company_quick_search_form_labels_g()
  DISPLAY get_str(140) TO lbTitle
  DISPLAY get_str(141) TO dl_f1

  DISPLAY get_str(145) TO lbInfo2

  DISPLAY get_str(11) TO lbInfo1

END FUNCTION

















