##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to warehouse browser
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTON:                                       RETURN:
# warehouse_main()                            Warehouse main menu                               NONE  
# warehouse_view(p_header, p_mode)            Create warehouse report                           NONE
# populate_warehouse_browser_t(p_header)      Display grid headers in text mode                 NONE
# populate_warehouse_browser_g(p_header)      Display grid headers in gui mode                  NONE
# select_warehouse_data(p_mode)               Fill variables of warehouse data                  idx (INTEGER)
# wh_report_output()                          Operator choice between screen or printer print   NULL or dev VARCHAR(20) (list, screen, printer)
# 
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# Module Definitions
######################################################

DEFINE  l_wh_arr    DYNAMIC ARRAY OF t_warehouse_rec

########## END of module variables #################



############################################################################################################################
# Menu functions
############################################################################################################################

###################################################
# FUNCTION warehouse_main()
#
# Warehouse main menu
#
# RETURN NONE
###################################################
FUNCTION warehouse_main()

  DEFINE
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

  IF fgl_fglgui() THEN -- gui client 
    
    CALL warehouse_view(get_str(2546), NULL)

  ELSE

    MENU "Warehouse"

      BEFORE MENU 
				CALL ui.Interface.setImage("qx://application/icon16/business/employee/employee-type.png")
				CALL ui.Interface.setText("Title")
	      
        CALL set_help_id(901)
        LET menu_str_arr1[1] = get_str(2223)
        LET menu_str_arr2[1] = get_str(2223)
        LET menu_str_arr1[2] = get_str(180)
        LET menu_str_arr2[2] = get_str(180)
        LET menu_str_arr1[3] = get_str(2487)
        LET menu_str_arr2[3] = get_str(2487)
        LET menu_str_arr1[4] = get_str(2488)
        LET menu_str_arr2[4] = get_str(2488)
        LET menu_str_arr1[5] = get_str(90)
        LET menu_str_arr2[5] = get_str(222)

      COMMAND KEY(F111, "A")  menu_str_arr1[1]  menu_str_arr2[1] HELP 902 -- All
        CALL warehouse_view(get_str(2546), 1)
  
      COMMAND KEY(F112, "R")  menu_str_arr1[2]  menu_str_arr2[2] HELP 903 -- Required
        CALL warehouse_view(get_str(2490), 2)
 
      COMMAND KEY(F113, "P")  menu_str_arr1[3]  menu_str_arr2[3] HELP 904 -- Planed
        CALL warehouse_view(get_str(2491), 3)

      COMMAND KEY(F114, "S")  menu_str_arr1[4]  menu_str_arr2[4] HELP 905 -- Reserved
        CALL warehouse_view(get_str(2492), 4)

      COMMAND KEY("E",F12,INTERRUPT,ACCEPT) menu_str_arr1[5] menu_str_arr2[5]  Help 51   --exit / return
        EXIT MENU
        
    	ON ACTION ("actExit") 
				EXIT MENU		
    END MENU

  END IF

END FUNCTION  -- warehouse_main --


############################################################################################################################
# Data Access functions
############################################################################################################################

###################################################
# FUNCTION warehouse_view(p_header, p_mode)
#
# Create warehouse report
#
# RETURN NONE
###################################################
FUNCTION warehouse_view(p_header, p_mode)

  DEFINE p_header         CHAR(100),
         p_mode           INTEGER,
         idx              INTEGER,
         i                INTEGER,
         dev              VARCHAR(20)

  IF p_mode IS NULL THEN 
    LET dev = "list"
    LET p_mode = 1
  ELSE
    CALL wh_report_output() RETURNING dev
  END IF

  LET idx = select_warehouse_data(p_mode)

  CASE dev

    WHEN "screen"   
      START REPORT warehouse_report
      FOR i=1 TO idx
      OUTPUT TO REPORT warehouse_report(l_wh_arr[i].*)
      END FOR
      FINISH REPORT warehouse_report

    WHEN "printer"
      START REPORT warehouse_report TO PRINTER
      FOR i=1 TO idx
      OUTPUT TO REPORT warehouse_report(l_wh_arr[i].*)
      END FOR
      FINISH REPORT warehouse_report

    WHEN "list"

        IF NOT fgl_window_open("w_warehouse_browser", 1, 1, get_form_path("f_warehouse_browser_l2"), FALSE) THEN
          CALL fgl_winmessage("Error","warehouse_main()\nCould not open window w_warehouse_browser","error")
        END IF
        CALL populate_warehouse_browser_g(p_header)

      WHILE TRUE
        IF (idx = 0) THEN
          DISPLAY get_str(2493) TO lbInfo1 -- Display "No items exist in the warehouse"
          LET idx = 1
        ELSE
          CALL SET_COUNT(idx)
          LET int_flag = FALSE
          DISPLAY ARRAY l_wh_arr TO sc_warehouse_scroll.*
            ON KEY (INTERRUPT)
              EXIT WHILE
            ON KEY (ACCEPT)
              EXIT WHILE
            ON KEY (F8)
              IF fgl_fglgui() THEN
                CALL wh_report_output() RETURNING dev
                CASE dev
                  WHEN "screen"   
                    START REPORT warehouse_report
                    FOR i=1 TO idx
                    OUTPUT TO REPORT warehouse_report(l_wh_arr[i].*)
                    END FOR
                    FINISH REPORT warehouse_report
                  WHEN "printer"
                    START REPORT warehouse_report TO PRINTER
                    FOR i=1 TO idx
                    OUTPUT TO REPORT warehouse_report(l_wh_arr[i].*)
                    END FOR
                    FINISH REPORT warehouse_report
                END CASE  
              ELSE
                START REPORT warehouse_report
                FOR i=1 TO idx
                  OUTPUT TO REPORT warehouse_report(l_wh_arr[i].*)
                END FOR
                FINISH REPORT warehouse_report
              END IF
            # filter
            ON KEY (F121)
              DISPLAY get_str(2546) TO lbTitle
              LET idx = select_warehouse_data(1)
              EXIT DISPLAY
            ON KEY (F122)
              DISPLAY get_str(2490) TO lbTitle
              LET idx = select_warehouse_data(2)
              EXIT DISPLAY
            ON KEY (F123)
              DISPLAY get_str(2491) TO lbTitle
              LET idx = select_warehouse_data(3)
              EXIT DISPLAY
            ON KEY (F124)
              DISPLAY get_str(2492) TO lbTitle
              LET idx = select_warehouse_data(4)
              EXIT DISPLAY
          END DISPLAY
          IF int_flag = TRUE THEN  --check if operator choose cancel
            EXIT WHILE
          END IF
        END IF
      END WHILE
      CALL fgl_window_close("w_warehouse_browser")

  END CASE

END FUNCTION  -- warehouse_view --



###################################################
# FUNCTION populate_warehouse_browser_t(p_header)
#
# Display grid headers in text mode
#
# RETURN NONE
###################################################
FUNCTION populate_warehouse_browser_t(p_header)

  DEFINE p_header  CHAR(100)

  DISPLAY p_header      TO lbTitle
  DISPLAY get_str(2503) TO dl_f1    -- "ID"
  DISPLAY get_str(2504) TO dl_f2    -- "Description"
  DISPLAY get_str(2505) TO dl_f3    -- "Amount"
  DISPLAY get_str(2506) TO dl_f4    -- "Reserved" 
  DISPLAY get_str(2507) TO dl_f5    -- "ShotTerm"
  DISPLAY get_str(2508) TO dl_f6    -- "LongTerm" 
  DISPLAY get_str(2509) TO dl_f7    -- "Required"  
  DISPLAY get_str(2510) TO lbInfo1 -- "Escape - Return    F8 - View Report"

END FUNCTION  -- populate_warehouse_browser_t --



###################################################
# FUNCTION populate_warehouse_browser_g(p_header)
#
# Display grid headers in gui mode
#
# RETURN NONE
###################################################
FUNCTION populate_warehouse_browser_g(p_header)

  DEFINE p_header  CHAR(100)

  DISPLAY p_header      TO lbTitle
  DISPLAY get_str(2251) TO rb_wh0  --lbTitle_rb
  DISPLAY get_str(2510) TO lbInfo1 -- "Escape - Return    F8 - View Report"

CALL UIRadButListItemText("rb_wh_report",1,get_str(2477))
CALL UIRadButListItemText("rb_wh_report",2,get_str(2553))
CALL UIRadButListItemText("rb_wh_report",3,get_str(2563))
CALL UIRadButListItemText("rb_wh_report",4,get_str(2564))
  #DISPLAY get_str(2477) TO rb_wh1
  #DISPLAY get_str(2553) TO rb_wh2
  #DISPLAY get_str(2563) TO rb_wh3
  #DISPLAY get_str(2564) TO rb_wh4

  CALL fgl_settitle(get_str(2494))

  CALL fgl_grid_header("sc_warehouse_scroll" , "stock_id" , get_str(2503), "Left", "") -- "ID"
  CALL fgl_grid_header("sc_warehouse_scroll" , "item_desc", get_str(2504), "Left", "") -- "Description"
  CALL fgl_grid_header("sc_warehouse_scroll" , "quantity" , get_str(2505), "Left", "") -- "Amount"
  CALL fgl_grid_header("sc_warehouse_scroll" , "reserved" , get_str(2506), "Left", "") -- "Reserved" 
  CALL fgl_grid_header("sc_warehouse_scroll" , "shot_term", get_str(2507), "Left", "") -- "ShotTerm"
  CALL fgl_grid_header("sc_warehouse_scroll" , "long_term", get_str(2508), "Left", "") -- "LongTerm"
  CALL fgl_grid_header("sc_warehouse_scroll" , "required" , get_str(2509), "Left", "") -- "Required"  

  DISPLAY get_str(10) TO bt_print
  #DISPLAY "!" TO bt_print
  DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO bt_cancel
  DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO bt_ok
  #DISPLAY "!" TO rb_wh_report
  DISPLAY "F121" TO rb_wh_report

END FUNCTION  -- populate_warehouse_browser_g --



###################################################
# FUNCTION select_warehouse_data(p_mode)
#
# Fill variables of warehouse data
#
# REURN idx (INTEGER)
###################################################
FUNCTION select_warehouse_data(p_mode)

  DEFINE l_wh_rec         OF t_warehouse_rec,
         l_wh_temp_1      OF t_wh_temp,
         l_wh_temp_2      OF t_wh_temp,
         l_wh_temp_3      OF t_wh_temp,
         p_mode           INTEGER,
         idx              INTEGER,
         l_amount         INTEGER

  LET l_amount = 0
  LET idx = 0

  DECLARE c_0_wh CURSOR FOR 
   SELECT si.stock_id,
          si.item_desc,
          si.quantity
     FROM Stock_Item si
    ORDER BY si.stock_id

  DECLARE c_1_wh CURSOR FOR
   SELECT si.stock_id as id,
          SUM(il.quantity) as amount
     FROM Stock_Item si, Invoice i, Invoice_Line il
    WHERE il.stock_id   = si.stock_id
      AND il.invoice_id = i.invoice_id 
      AND i.status in (1, 3, 4)
      AND TODAY < i.invoice_date + 3
     GROUP BY si.stock_id
     ORDER BY si.stock_id 

  DECLARE c_2_wh CURSOR FOR 
   SELECT si.stock_id as id,
          SUM(sl.quantity) as amount
     FROM Stock_Item si, Supplies s, Supplies_Line sl
    WHERE sl.stock_id   = si.stock_id
      AND s.suppl_id   = sl.suppl_id 
      AND s.exp_date < TODAY + 4  
      AND s.state      = 1
    GROUP BY si.stock_id
    ORDER BY si.stock_id  

  DECLARE c_3_wh CURSOR FOR 
   SELECT si.stock_id as id,
          SUM(sl.quantity) as amount
     FROM Stock_Item si, Supplies s, Supplies_Line sl
    WHERE sl.stock_id   = si.stock_id
      AND s.suppl_id   = sl.suppl_id 
      AND s.exp_date > TODAY + 3  
      AND s.state      = 1
    GROUP BY si.stock_id
    ORDER BY si.stock_id 

  OPEN c_1_wh
  OPEN c_2_wh
  OPEN c_3_wh

  FETCH c_1_wh INTO l_wh_temp_1.* -- reserved
  FETCH c_2_wh INTO l_wh_temp_2.* -- shot_term
  FETCH c_3_wh INTO l_wh_temp_3.* -- long_term

  FOREACH c_0_wh INTO l_wh_rec.stock_id, l_wh_rec.item_desc, l_wh_rec.quantity

    IF l_wh_temp_1.id = l_wh_rec.stock_id THEN 
      LET l_wh_rec.reserved = l_wh_temp_1.amount
      FETCH c_1_wh INTO l_wh_temp_1.*
    ELSE 
      LET l_wh_rec.reserved = 0 
    END IF 

    IF l_wh_temp_2.id = l_wh_rec.stock_id THEN 
      LET l_wh_rec.shot_term = l_wh_temp_2.amount
      FETCH c_2_wh INTO l_wh_temp_2.*
    ELSE 
      LET l_wh_rec.shot_term = 0 
    END IF 

    IF l_wh_temp_3.id = l_wh_rec.stock_id THEN 
      LET l_wh_rec.long_term = l_wh_temp_3.amount
      FETCH c_3_wh INTO l_wh_temp_3.*
    ELSE 
      LET l_wh_rec.long_term = 0 
    END IF

    CASE p_mode 
      WHEN 1 LET l_amount = 1
      WHEN 2 LET l_amount = l_wh_rec.reserved  - l_wh_rec.quantity
      WHEN 3 LET l_amount = l_wh_rec.shot_term + l_wh_rec.long_term
      WHEN 4 LET l_amount = l_wh_rec.reserved
    END CASE
    IF l_amount > 0 THEN
      LET idx = idx + 1
      LET l_wh_arr[idx].stock_id  = l_wh_rec.stock_id
      LET l_wh_arr[idx].item_desc = l_wh_rec.item_desc
      LET l_wh_arr[idx].quantity  = l_wh_rec.quantity
      IF l_wh_rec.reserved  IS NULL THEN LET l_wh_arr[idx].reserved  = 0 ELSE LET l_wh_arr[idx].reserved  = l_wh_rec.reserved  END IF
      IF l_wh_rec.shot_term IS NULL THEN LET l_wh_arr[idx].shot_term = 0 ELSE LET l_wh_arr[idx].shot_term = l_wh_rec.shot_term END IF
      IF l_wh_rec.long_term IS NULL THEN LET l_wh_arr[idx].long_term = 0 ELSE LET l_wh_arr[idx].long_term = l_wh_rec.long_term END IF
      IF l_wh_arr[idx].reserved - l_wh_arr[idx].quantity < 0 THEN 
        LET l_wh_arr[idx].required = 0 
      ELSE 
        LET l_wh_arr[idx].required  = l_wh_arr[idx].reserved - l_wh_arr[idx].quantity 
      END IF
    END IF

  END FOREACH

  CLOSE c_1_wh
  CLOSE c_2_wh
  CLOSE c_3_wh

  RETURN idx

END FUNCTION  -- select_warehouse_data --



###################################################
# FUNCTION wh_report_output()
#
# Operator choice between screen or printer print
#
# RETURN dev (VARCHAR(20))
###################################################
FUNCTION wh_report_output()
  
  DEFINE dev           VARCHAR(20),
         menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
         menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

  MENU "Report output"

    BEFORE MENU 
      CALL set_help_id(901)
      LET menu_str_arr1[1] = get_str(2495)
      LET menu_str_arr2[1] = get_str(2496)
      LET menu_str_arr1[2] = get_str(2497)
      LET menu_str_arr2[2] = get_str(2498)
      LET menu_str_arr1[3] = get_str(2499)
      LET menu_str_arr2[3] = get_str(2500)
      LET menu_str_arr1[4] = get_str(2501)
      LET menu_str_arr2[4] = get_str(2502)

    COMMAND KEY (F171, "L") menu_str_arr1[1]  menu_str_arr2[1] HELP 906 --  "List" "Print report to screen as list of items"
      LET dev = "list"  
      EXIT MENU

    COMMAND KEY (F172, "S") menu_str_arr1[2]  menu_str_arr2[2] HELP 907 -- "Screen" "Print report to screen"
      LET dev = "screen"  
      EXIT MENU

    COMMAND KEY (F173, "P") menu_str_arr1[3]  menu_str_arr2[3] HELP 908 -- "Printer" "Print report to printer"
      LET dev = "printer"
      EXIT MENU

    COMMAND KEY (F12,"E") menu_str_arr1[4]  menu_str_arr2[4] HELP 909 --  "Cancel" "Cancel printing"
      LET dev = NULL
      EXIT MENU
      
    ON ACTION ("actExit") 
      LET dev = NULL
      EXIT MENU	
			
  END MENU

  RETURN dev

END FUNCTION  -- wh_report_output --



