##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_classic_globals.4gl"

############################################################
# FUNCTION set_help_file(h_file)
#
# Set the help file (options help file) depening on the language
#
# RETURN NONE
############################################################
FUNCTION set_classic_help_file(h_file_id)
  DEFINE
    h_file      VARCHAR(200),
    h_file_id   SMALLINT,
    lang        SMALLINT,
    local_debug SMALLINT,
    err_msg     VARCHAR(200)

  LET local_debug = FALSE

  LET qxt_previous_help_file_id = qxt_current_help_file_id
  LET lang = get_language()
  LET qxt_current_help_file_id = h_file_id

  IF h_file_id >= 1 AND h_file_id <= 10 THEN

    IF get_classic_help_multi_lang() = 0 THEN
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, ".erm"
    ELSE
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, "_", trim(get_language_dir(lang)), ".erm"
    END IF
    
    IF NOT fgl_test("e",h_file) THEN
      LET err_msg = get_str_tool(727), " ",  h_file
      CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
    ELSE
      OPTIONS HELP FILE h_file
      IF local_debug THEN
        DISPLAY "OPTIONS HELP FILE ", h_file
      END IF
    END IF

  ELSE
    LET err_msg = get_str_tool(802), "\nset_classic_help_file(h_file_id)\nh_file_id = ", h_file_id
    CALL fgl_winmessage(get_str_tool(30), err_msg, "error")
  END IF



END FUNCTION





