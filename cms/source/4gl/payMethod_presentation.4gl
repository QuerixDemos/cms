##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



####################################################
# FUNCTION grid_header_pay_method_scroll()
#
# Populate payment method grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_pay_method_scroll()
  CALL fgl_grid_header("sc_pay_method","pay_method_id",get_str(1971),"right","F13")  --PayMethod ID
  CALL fgl_grid_header("sc_pay_method","pay_method_name",get_str(1972),"left","F14")  --Pay Method Name
  CALL fgl_grid_header("sc_pay_method","pay_method_desc",get_str(1973),"left","F15")  --Pay Method Description
END FUNCTION


#######################################################
# FUNCTION populate_payment_method_form_labels_g()
#
# Populate payment method form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_payment_method_form_labels_g()
  DISPLAY get_str(1960) TO lbTitle
  DISPLAY get_str(1961) TO dl_f1
  DISPLAY get_str(1962) TO dl_f2
  #DISPLAY get_str(1963) TO dl_f3
  #DISPLAY get_str(1964) TO dl_f4
  #DISPLAY get_str(1965) TO dl_f5
  #DISPLAY get_str(1966) TO dl_f6
  #DISPLAY get_str(1967) TO dl_f7
  #DISPLAY get_str(1968) TO dl_f8
  DISPLAY get_str(1969) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_payment_method_form_labels_t()
#
# Populate payment method form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_payment_method_form_labels_t()
  DISPLAY get_str(1960) TO lbTitle
  DISPLAY get_str(1961) TO dl_f1
  DISPLAY get_str(1962) TO dl_f2
  #DISPLAY get_str(1963) TO dl_f3
  #DISPLAY get_str(1964) TO dl_f4
  #DISPLAY get_str(1965) TO dl_f5
  #DISPLAY get_str(1966) TO dl_f6
  #DISPLAY get_str(1967) TO dl_f7
  #DISPLAY get_str(1968) TO dl_f8
  DISPLAY get_str(1969) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_payment_method_list_form_labels_t()
#
# Populate payment method list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_payment_method_list_form_labels_t()
  DISPLAY get_str(1970) TO lbTitle
  DISPLAY get_str(1971) TO dl_f1
  DISPLAY get_str(1972) TO dl_f2
  DISPLAY get_str(1973) TO dl_f3
  #DISPLAY get_str(1974) TO dl_f4
  #DISPLAY get_str(1975) TO dl_f5
  #DISPLAY get_str(1976) TO dl_f6
  #DISPLAY get_str(1977) TO dl_f7
  #DISPLAY get_str(1978) TO dl_f8
  DISPLAY get_str(1979) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_payment_method_list_form_labels_g()
#
# Populate payment method list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_payment_method_list_form_labels_g()
  DISPLAY get_str(1970) TO lbTitle
  #DISPLAY get_str(1971) TO dl_f1
  #DISPLAY get_str(1972) TO dl_f2
  #DISPLAY get_str(1973) TO dl_f3
  #DISPLAY get_str(1974) TO dl_f4
  #DISPLAY get_str(1975) TO dl_f5
  #DISPLAY get_str(1976) TO dl_f6
  #DISPLAY get_str(1977) TO dl_f7
  #DISPLAY get_str(1978) TO dl_f8
  DISPLAY get_str(1979) TO lbInfo1

END FUNCTION

