##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Tool String - Shared functions when strings are located in memory
#
# Strings are located in a file but copied to memory
#
################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_string_app_in_mem_globals.4gl"

######################################################################################################################
# Init functions
######################################################################################################################

###########################################################
# FUNCTION qxt_empty_string_app_array()
#
# Initialise too string array with blanks ""
#
# RETURN NONE
###########################################################
FUNCTION qxt_empty_string_app_array()
  DEFINE
    id INTEGER
  FOR id = 1 TO qxt_string_app_array_size
    LET qxt_string_app[id] = ""
  END FOR
END FUNCTION



######################################################################################################################
# Data Access functions
######################################################################################################################

###########################################################
# FUNCTION get_str(id)
#
# Get program related multi language string
#
# RETURN trim(qxt_multi_lingual_prog_str[ID].txt_en)
###########################################################
FUNCTION get_str(id)
  DEFINE id SMALLINT


  RETURN qxt_string_app[id]

END FUNCTION

###########################################################
# FUNCTION init_string_app
#
# Initialise (Clear) app string
#
# RETURN NONE
###########################################################
FUNCTION init_string_app()
  DEFINE
    id INTEGER
  FOR id = 1 TO qxt_string_app_array_size
    LET qxt_string_app[id] = ""
  END FOR
END FUNCTION


###########################################################
# FUNCTION set_string_app(p_id,p_str)
#
# Set a single string item
#
# RETURN NONE
###########################################################
FUNCTION set_string_app(p_id,p_str)
  DEFINE
    p_id  INTEGER,
    p_str VARCHAR(100)

  LET qxt_string_app[p_id] = p_str
END FUNCTION


###########################################################
# FUNCTION get_string_app_array_size()
#
# Get the application string array zize
#
# RETURN NONE
###########################################################
FUNCTION get_string_app_array_size()
  RETURN qxt_string_app_array_size
END FUNCTION




