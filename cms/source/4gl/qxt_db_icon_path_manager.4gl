##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the icon_path table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_icon_path_name(p_icon_path_dir)  get icon_path id from p_icon_path_dir        l_icon_path_name
# get_icon_path_dir(icon_path_name)    Get icon_path_dir from p_icon_path_name        l_icon_path_dir
# get_icon_path_rec(p_icon_path_name)   Get the icon_path record from p_icon_path_name  l_icon_path.*
# icon_path_popup_data_source()               Data Source (cursor) for icon_path_popup              NONE
# icon_path_popup                             icon_path selection window                            p_icon_path_name
# (p_icon_path_name,p_order_field,p_accept_action)
# icon_path_combo_list(cb_field_name)         Populates icon_path combo list from db                NONE
# icon_path_create()                          Create a new icon_path record                         NULL
# icon_path_edit(p_icon_path_name)      Edit icon_path record                                 NONE
# icon_path_input(p_icon_path_rec)    Input icon_path details (edit/create)                 l_icon_path.*
# icon_path_delete(p_icon_path_name)    Delete a icon_path record                             NONE
# icon_path_view(p_icon_path_name)      View icon_path record by ID in window-form            NONE
# icon_path_view_by_rec(p_icon_path_rec) View icon_path record in window-form               NONE
# get_icon_path_name_from_dir(p_dir)          Get the icon_path_name from a file                      l_icon_path_name
# icon_path_dir_count(p_dir)                Tests if a record with this dir already exists               r_count
# icon_path_name_count(p_icon_path_name)  tests if a record with this icon_path_name already exists r_count
# copy_icon_path_record_to_form_record        Copy normal icon_path record data to type icon_path_form_rec   l_icon_path_form_rec.*
# (p_icon_path_rec)
# copy_icon_path_form_record_to_record        Copy type icon_path_form_rec to normal icon_path record data   l_icon_path_rec.*
# (p_icon_path_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_icon_path_scroll()              Populate icon_path grid headers                       NONE
# populate_icon_path_form_labels_g()          Populate icon_path form labels for gui                NONE
# populate_icon_path_form_labels_t()          Populate icon_path form labels for text               NONE
# populate_icon_path_form_edit_labels_g()     Populate icon_path form edit labels for gui           NONE
# populate_icon_path_form_edit_labels_t()     Populate icon_path form edit labels for text          NONE
# populate_icon_path_form_view_labels_g()     Populate icon_path form view labels for gui           NONE
# populate_icon_path_form_view_labels_t()     Populate icon_path form view labels for text          NONE
# populate_icon_path_form_create_labels_g()   Populate icon_path form create labels for gui         NONE
# populate_icon_path_form_create_labels_t()   Populate icon_path form create labels for text        NONE
# populate_icon_path_list_form_labels_g()     Populate icon_path list form labels for gui           NONE
# populate_icon_path_list_form_labels_t()     Populate icon_path list form labels for text          NONE
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


######################################################################################################
# Data/Record Access functions
######################################################################################################

#########################################################
# FUNCTION get_icon_path_name(p_icon_path_dir)
#
# get icon_path_name from p_icon_path_dir
#
# RETURN l_icon_path_name
#########################################################
FUNCTION get_icon_path_name(p_icon_path_dir)
  DEFINE 
    p_icon_path_dir   LIKE qxt_icon_path.icon_path_dir,
    l_icon_path_name     LIKE qxt_icon_path.icon_path_name,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_path_name() - p_icon_path_dir = ", p_icon_path_dir
  END IF

  SELECT qxt_icon_path.icon_path_name
    INTO l_icon_path_name
    FROM qxt_icon_path
    WHERE qxt_icon_path.icon_path_dir = p_icon_path_dir

  RETURN l_icon_path_name
END FUNCTION



#########################################################
# FUNCTION get_icon_path_dir(icon_path_name)
#
# Get icon_path_dir from p_icon_path_name
#
# RETURN l_icon_path_dir
#########################################################
FUNCTION get_icon_path_dir(p_icon_path_name)
  DEFINE 
    p_icon_path_name       LIKE qxt_icon_path.icon_path_name,
    l_icon_path_dir      LIKE qxt_icon_path.icon_path_dir

  SELECT qxt_icon_path.icon_path_dir
    INTO l_icon_path_dir
    FROM qxt_icon_path
    WHERE qxt_icon_path.icon_path_name = p_icon_path_name

  RETURN l_icon_path_dir
END FUNCTION


######################################################
# FUNCTION get_icon_path_rec(p_icon_path_name)
#
# Get the icon_path record from p_icon_path_name
#
# RETURN l_icon_path.*
######################################################
FUNCTION get_icon_path_rec(p_icon_path_name)
  DEFINE 
    p_icon_path_name  LIKE qxt_icon_path.icon_path_name,
    l_icon_path     RECORD LIKE qxt_icon_path.*

  SELECT qxt_icon_path.*
    INTO l_icon_path.*
    FROM qxt_icon_path
    WHERE qxt_icon_path.icon_path_name = p_icon_path_name

  RETURN l_icon_path.*
END FUNCTION



######################################################
# FUNCTION get_icon_path_name_from_dir(p_dir)
#
# Get the icon_path_name from a file
#
# RETURN l_icon_path_name
######################################################
FUNCTION get_icon_path_name_from_dir(p_dir)
  DEFINE 
    p_dir                 LIKE qxt_icon_path.icon_path_dir,
    l_icon_path_name LIKE qxt_icon_path.icon_path_name,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_path_name() - icon_path_dir = ", p_dir
  END IF

    SELECT qxt_icon_path.icon_path_name
      INTO l_icon_path_name
      FROM qxt_icon_path
      WHERE qxt_icon_path.icon_path_dir = p_dir

  IF local_debug THEN
    DISPLAY "get_icon_path_name() - l_icon_path_name = ", l_icon_path_name
  END IF

  RETURN l_icon_path_name

END FUNCTION


####################################################
# FUNCTION icon_path_dir_count(p_dir)
#
# tests if a record with this dir already exists
#
# RETURN r_count
####################################################
FUNCTION icon_path_dir_count(p_dir)
  DEFINE
    p_dir         LIKE qxt_icon_path.icon_path_dir,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon_path
      WHERE qxt_icon_path.icon_path_dir = p_dir

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION icon_path_name_count(p_icon_path_name)
#
# tests if a record with this icon_path_name already exists
#
# RETURN r_count
####################################################
FUNCTION icon_path_name_count(p_icon_path_name)
  DEFINE
    p_icon_path_name    LIKE qxt_icon_path.icon_path_name,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon_path
      WHERE qxt_icon_path.icon_path_name = p_icon_path_name

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION icon_path_combo_list(cb_field_name)
#
# Populates icon_path combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION icon_path_combo_list(cb_field_name)
  DEFINE 
    l_icon_path_dir_arr       DYNAMIC ARRAY OF LIKE qxt_icon_path.icon_path_dir,
    row_count                 INTEGER,
    current_row               INTEGER,
    cb_field_name             VARCHAR(30),   --form field name for the country combo list field
    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_path_combo_list() - cb_field_name=", cb_field_name
  END IF

  DECLARE c_icon_path_dir_scroll2 CURSOR FOR 
    SELECT qxt_icon_path.icon_path_dir
      FROM qxt_icon_path

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_icon_path_dir_scroll2 INTO l_icon_path_dir_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_icon_path_dir_arr[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_icon_path_dir_arr[row_count] CLIPPED, ")"
      DISPLAY "icon_path_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION




######################################################
# FUNCTION icon_path_popup_data_source()
#
# Data Source (cursor) for icon_path_popup
#
# RETURN NONE
######################################################
FUNCTION icon_path_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "icon_path_name"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_icon_path.icon_path_name, ",
                 "qxt_icon_path.icon_path_dir ",

                 "FROM qxt_icon_path "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "icon_path_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_icon_path FROM sql_stmt
  DECLARE c_icon_path CURSOR FOR p_icon_path

END FUNCTION


######################################################
# FUNCTION icon_path_popup(p_icon_path_name,p_order_field,p_accept_action)
#
# icon_path selection window
#
# RETURN p_icon_path_name
######################################################
FUNCTION icon_path_popup(p_icon_path_name,p_order_field,p_accept_action)
  DEFINE 
    p_icon_path_name       LIKE qxt_icon_path.icon_path_name,  --default return value if user cancels
    l_icon_path_arr        DYNAMIC ARRAY OF t_qxt_icon_path_form_rec,    --RECORD LIKE qxt_icon_path.*,  
    i                      INTEGER,
    p_accept_action        SMALLINT,
    err_msg                VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_icon_path_name       LIKE qxt_icon_path.icon_path_name,
    local_debug            SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "icon_path_popup() - p_icon_path_name=",p_icon_path_name
    DISPLAY "icon_path_popup() - p_order_field=",p_order_field
    DISPLAY "icon_path_popup() - p_accept_action=",p_accept_action
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_icon_path_scroll", 2, 8, get_form_path("f_qxt_icon_path_scroll_g"),FALSE) 
    CALL populate_icon_path_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_icon_path_scroll", 2, 8, get_form_path("f_qxt_icon_path_scroll_t"),FALSE) 
    CALL populate_icon_path_list_form_labels_t()
  END IF


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_icon_path

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL icon_path_create()
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL icon_path_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_icon_path INTO l_icon_path_arr[i].*
      IF local_debug THEN
        DISPLAY "icon_path_popup() - i=",i
        DISPLAY "icon_path_popup() - l_icon_path_arr[i].icon_path_name=",l_icon_path_arr[i].icon_path_name
        DISPLAY "icon_path_popup() - l_icon_path_arr[i].icon_path_dir=",l_icon_path_arr[i].icon_path_dir
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 200 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_icon_path_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		
    IF local_debug THEN
      DISPLAY "icon_path_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      CALL fgl_window_close("w_icon_path_scroll")
      RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "icon_path_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_icon_path_arr TO sc_icon_path.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET l_icon_path_name = l_icon_path_arr[i].icon_path_name

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL icon_path_view(l_icon_path_name)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL icon_path_edit(l_icon_path_name)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL icon_path_delete(l_icon_path_name)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL icon_path_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","icon_path_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "icon_path_popup(p_icon_path_name,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("icon_path_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL icon_path_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_icon_path_name = l_icon_path_arr[i].icon_path_name
        CALL icon_path_edit(l_icon_path_name)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_icon_path_name = l_icon_path_arr[i].icon_path_name
        CALL icon_path_delete(l_icon_path_name)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_path_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_path_dir"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_icon_path_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_icon_path_name 
  ELSE 
    RETURN l_icon_path_name 
  END IF

END FUNCTION



######################################################################################################
# Edit/Create/Input functions section
######################################################################################################

######################################################
# FUNCTION icon_path_create()
#
# Create a new icon_path record
#
# RETURN NULL
######################################################
FUNCTION icon_path_create()
  DEFINE 
    l_icon_path RECORD LIKE qxt_icon_path.*

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_g"), TRUE) 
    CALL populate_icon_path_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_t"), TRUE) 
    CALL populate_icon_path_form_create_labels_t()
  END IF

  LET l_icon_path.icon_path_name = 0

  LET int_flag = FALSE
  
  #Initialise some variables

  # CALL the INPUT
  CALL icon_path_input(l_icon_path.*)
    RETURNING l_icon_path.*

  CALL fgl_window_close("w_icon_path")

  IF NOT int_flag THEN
    INSERT INTO qxt_icon_path VALUES (
      l_icon_path.icon_path_name,
      l_icon_path.icon_path_dir)

    IF sqlca.sqlcode THEN --failure
      LET int_flag = FALSE
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_icon_path.icon_path_name
    END IF

    RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)

    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION icon_path_edit(p_icon_path_name)
#
# Edit icon_path record
#
# RETURN NONE
#################################################
FUNCTION icon_path_edit(p_icon_path_name)
  DEFINE 
    p_icon_path_name       LIKE qxt_icon_path.icon_path_name,
    l_key1_icon_path_name  LIKE qxt_icon_path.icon_path_name,
    l_icon_path_rec        RECORD LIKE qxt_icon_path.*,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_path_edit() - Function Entry Point"
    DISPLAY "icon_path_edit() - p_icon_path_name=", p_icon_path_name
  END IF

  #Store the primary key for the SQL update
  LET l_key1_icon_path_name = p_icon_path_name

  #Get the record (by id)
  CALL get_icon_path_rec(p_icon_path_name) RETURNING l_icon_path_rec.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_g"), TRUE) 
    CALL populate_icon_path_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_t"), TRUE) 
    CALL populate_icon_path_form_edit_labels_t()
  END IF


  #Call the INPUT
  CALL icon_path_input(l_icon_path_rec.*) 
    RETURNING l_icon_path_rec.*

    IF local_debug THEN
      DISPLAY "icon_path_edit() RETURNED FROM INPUT - int_flag=", int_flag
    END IF

  CALL fgl_window_close("w_icon_path")

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "icon_path_edit() - l_icon_path_rec.icon_path_name=",l_icon_path_rec.icon_path_name
      DISPLAY "icon_path_edit() - l_icon_path_rec.icon_path_dir=",l_icon_path_rec.icon_path_dir
    END IF

    UPDATE qxt_icon_path
      SET 
          icon_path_name =      l_icon_path_rec.icon_path_name,
          icon_path_dir =       l_icon_path_rec.icon_path_dir

      WHERE icon_path_name = l_key1_icon_path_name

    IF local_debug THEN
      DISPLAY "icon_path_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(830),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(830),NULL)
      RETURN l_icon_path_rec.icon_path_name
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(830),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF



END FUNCTION


#################################################
# FUNCTION icon_path_input(p_icon_path_rec)
#
# Input icon_path details (edit/create)
#
# RETURN l_icon_path.*
#################################################
FUNCTION icon_path_input(p_icon_path_rec)
  DEFINE 
    p_icon_path_rec           RECORD LIKE qxt_icon_path.*,
    l_icon_path_form_rec      OF t_qxt_icon_path_form_rec,
    l_orignal_icon_path_name  LIKE qxt_icon_path.icon_path_name,
    l_orignal_icon_path_dir   LIKE qxt_icon_path.icon_path_dir,
    local_debug               SMALLINT,
    tmp_str                   VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_icon_path_name = p_icon_path_rec.icon_path_name
  LET l_orignal_icon_path_dir = p_icon_path_rec.icon_path_dir

  #copy record data to form_record format record
  CALL copy_icon_path_record_to_form_record(p_icon_path_rec.*) RETURNING l_icon_path_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_icon_path_form_rec.* WITHOUT DEFAULTS HELP 1



    AFTER FIELD icon_path_name
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_string_exists(get_str_tool(831), l_icon_path_form_rec.icon_path_name,NULL,TRUE)  THEN
        NEXT FIELD icon_path_name
      END IF

      #The icon_path_name must be unique
      IF icon_path_name_count(l_icon_path_form_rec.icon_path_name) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_icon_path_name <> l_icon_path_form_rec.icon_path_name THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(831),NULL)
          NEXT FIELD icon_path_name
        END IF
      END IF


    AFTER FIELD icon_path_dir
      #dir must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(832), l_icon_path_form_rec.icon_path_dir,NULL,TRUE)  THEN
        NEXT FIELD icon_path_dir
      END IF

      #The icon_path_dir must be unique
      IF icon_path_dir_count(l_icon_path_form_rec.icon_path_dir) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_icon_path_dir = "" OR l_orignal_icon_path_dir IS NULL THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(832),NULL)
          NEXT FIELD icon_path_dir
        END IF
      END IF


    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #icon_path_name must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(831), l_icon_path_form_rec.icon_path_name,NULL,TRUE)  THEN
          NEXT FIELD icon_path_name
          CONTINUE INPUT
        END IF

      #The icon_path_name must be unique
      IF icon_path_name_count(l_icon_path_form_rec.icon_path_name) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_icon_path_name <> l_icon_path_form_rec.icon_path_name THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(831),NULL)
          NEXT FIELD icon_path_name
          CONTINUE INPUT
        END IF
      END IF

      #dir must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(832), l_icon_path_form_rec.icon_path_dir,NULL,TRUE)  THEN

          NEXT FIELD icon_path_dir
          CONTINUE INPUT
        END IF


      #The icon_path_dir must be unique
      IF icon_path_dir_count(l_icon_path_form_rec.icon_path_dir) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_icon_path_dir = "" OR l_orignal_icon_path_dir IS NULL THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_key(get_str_tool(832),NULL)
          NEXT FIELD icon_path_dir
          CONTINUE INPUT
        END IF
      END IF


  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "icon_path_input() - l_icon_path_form_rec.icon_path_name=",l_icon_path_form_rec.icon_path_name
    DISPLAY "icon_path_input() - l_icon_path_form_rec.icon_path_dir=",l_icon_path_form_rec.icon_path_dir
  END IF


  #Copy the form record data to a normal icon_path record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_icon_path_form_record_to_record(l_icon_path_form_rec.*) RETURNING p_icon_path_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "icon_path_input() - p_icon_path_rec.icon_path_name=",p_icon_path_rec.icon_path_name
    DISPLAY "icon_path_input() - p_icon_path_rec.icon_path_dir=",p_icon_path_rec.icon_path_dir
  END IF

  RETURN p_icon_path_rec.*

END FUNCTION


#################################################
# FUNCTION icon_path_delete(p_icon_path_name)
#
# Delete a icon_path record
#
# RETURN NONE
#################################################
FUNCTION icon_path_delete(p_icon_path_name)
  DEFINE 
    p_icon_path_name       LIKE qxt_icon_path.icon_path_name

  #do you really want to delete...
  IF question_delete_record(get_str_tool(830), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_icon_path 
        WHERE icon_path_name = p_icon_path_name
    COMMIT WORK

  END IF

END FUNCTION


######################################################################################################
# Record copy functions  (between table record and user data type record
######################################################################################################

######################################################
# FUNCTION copy_icon_path_record_to_form_record(p_icon_path_rec)  
#
# Copy normal icon_path record data to type icon_path_form_rec
#
# RETURN l_icon_path_form_rec.*
######################################################
FUNCTION copy_icon_path_record_to_form_record(p_icon_path_rec)  
  DEFINE
    p_icon_path_rec       RECORD LIKE qxt_icon_path.*,
    l_icon_path_form_rec  OF t_qxt_icon_path_form_rec

  LET l_icon_path_form_rec.icon_path_name = p_icon_path_rec.icon_path_name
  LET l_icon_path_form_rec.icon_path_dir = p_icon_path_rec.icon_path_dir

  RETURN l_icon_path_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_icon_path_form_record_to_record(p_icon_path_form_rec)  
#
# Copy type icon_path_form_rec to normal icon_path record data
#
# RETURN l_icon_path_rec.*
######################################################
FUNCTION copy_icon_path_form_record_to_record(p_icon_path_form_rec)  
  DEFINE
    l_icon_path_rec       RECORD LIKE qxt_icon_path.*,
    p_icon_path_form_rec  OF t_qxt_icon_path_form_rec

  LET l_icon_path_rec.icon_path_name = p_icon_path_form_rec.icon_path_name
  LET l_icon_path_rec.icon_path_dir = p_icon_path_form_rec.icon_path_dir

  RETURN l_icon_path_rec.*

END FUNCTION





######################################################################################################
# Record View functions section
######################################################################################################


#################################################
# FUNCTION icon_path_view(p_icon_path_name)
#
# View icon_path record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_path_view(p_icon_path_name)
  DEFINE 
    p_icon_path_name       LIKE qxt_icon_path.icon_path_name,
    l_icon_path_rec      RECORD LIKE qxt_icon_path.*


  CALL get_icon_path_rec(p_icon_path_name) RETURNING l_icon_path_rec.*
  CALL icon_path_view_by_rec(l_icon_path_rec.*)

END FUNCTION


#################################################
# FUNCTION icon_path_view_by_rec(p_icon_path_rec)
#
# View icon_path record in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_path_view_by_rec(p_icon_path_rec)
  DEFINE 
    p_icon_path_rec  RECORD LIKE qxt_icon_path.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_g"), TRUE) 
    CALL populate_icon_path_form_view_labels_g()
  ELSE
    CALL fgl_window_open("w_icon_path", 3, 3, get_form_path("f_qxt_icon_path_det_t"), TRUE) 
    CALL populate_icon_path_form_view_labels_t()
  END IF

  DISPLAY BY NAME p_icon_path_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_icon_path")
END FUNCTION




######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_icon_path_scroll()
#
# Populate icon_path grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_icon_path_scroll()
  CALL fgl_grid_header("sc_icon_path","icon_path_name",get_str_tool(831),"right","F13")  --icon_path
  CALL fgl_grid_header("sc_icon_path","icon_path_dir",get_str_tool(832),"left","F14")  --file dir


END FUNCTION

#######################################################
# FUNCTION populate_icon_path_form_labels_g()
#
# Populate icon_path form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(830) TO lbTitle

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("toolbar_size_dir")

END FUNCTION



#######################################################
# FUNCTION populate_icon_path_form_labels_t()
#
# Populate icon_path form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_labels_t()

  DISPLAY get_str_tool(830) TO lbTitle

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_icon_path_form_edit_labels_g()
#
# Populate icon_path form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(830)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_dir")


END FUNCTION



#######################################################
# FUNCTION populate_icon_path_form_edit_labels_t()
#
# Populate icon_path form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_edit_labels_t()

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_icon_path_form_view_labels_g()
#
# Populate icon_path form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(830)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_icon_path_form_view_labels_t()
#
# Populate icon_path form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_view_labels_t()

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_dir")

END FUNCTION


#######################################################
# FUNCTION populate_icon_path_form_create_labels_g()
#
# Populate icon_path form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(830)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_dir")

END FUNCTION



#######################################################
# FUNCTION populate_icon_path_form_create_labels_t()
#
# Populate icon_path form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_form_create_labels_t()

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(831) TO dl_f1
  DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_icon_path_list_form_labels_g()
#
# Populate icon_path list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(830)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_icon_path_scroll()

  #DISPLAY get_str_tool(831) TO dl_f1
  #DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete

END FUNCTION


#######################################################
# FUNCTION populate_icon_path_list_form_labels_t()
#
# Populate icon_path list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_path_list_form_labels_t()

  DISPLAY trim(get_str_tool(830)) || " - " || get_str_tool(16) TO lbTitle  --List

  #DISPLAY get_str_tool(831) TO dl_f1
  #DISPLAY get_str_tool(832) TO dl_f2
  #DISPLAY get_str_tool(833) TO dl_f3
  #DISPLAY get_str_tool(834) TO dl_f4
  #DISPLAY get_str_tool(835) TO dl_f5
  #DISPLAY get_str_tool(836) TO dl_f6
  #DISPLAY get_str_tool(837) TO dl_f7
  #DISPLAY get_str_tool(838) TO dl_f8
  #DISPLAY get_str_tool(839) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION

###########################################################################################################################
# EOF
###########################################################################################################################









