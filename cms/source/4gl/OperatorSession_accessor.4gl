##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


##################################################
# FUNCTION add_operator_session()
##################################################
FUNCTION add_operator_session()
  DEFINE l_operator_session RECORD LIKE operator_session.*
	DEFINE l_session_id LIKE operator_session.session_id
 		
 #CALL fgl_winmessage("New Session REcord ATTEMTP","New Session REcord ATTEMTP","info")
 
	LET l_session_id =  fgl_getenv("QX_SESSION_ID")
	
 IF count_session_record(l_session_id) > 0 THEN
 	#DISPLAY "session record alreday exists - can not create duplicate - but increment by one OR activate it"
 	CALL plus_counter_operator_session()
 	return 1
 END IF
 
	#IF count_session_record(l_session_id) = 0 THEN
 	#	DISPLAY "session record does not exist - Create new record"
	#ELSE
	#	DISPLAY "serious problem"
	#	return - 1
	#END IF
 
 
	LET l_operator_session.session_id = fgl_getenv("QX_SESSION_ID")
	LET l_operator_session.operator_id = get_current_operator_id()
	LET l_operator_session.session_counter = 1	
	LET l_operator_session.expired = 0	
	LET l_operator_session.session_created = CURRENT YEAR TO Second
	LET l_operator_session.session_modified = NULL
	LET l_operator_session.client_host = fgl_getenv("QX_CLIENT_HOST")
	LET l_operator_session.client_ip = fgl_getenv("QX_CLIENT_IP") 	
	LET l_operator_session.session_counter = 1	
	LET l_operator_session.reserve1 = ""	
	LET l_operator_session.reserve2 = ""	

#           	session_id					VARCHAR(50),
#            operator_id         INTEGER,
#            session_counter     INTEGER,
#            session_created				DATETIME YEAR TO SECOND,
#            session_modified 			DATETIME YEAR TO SECOND,
#            client_host					VARCHAR(50),
#            client_ip						VARCHAR(50),
#            reserve1						VARCHAR(50),
#            reserve2						VARCHAR(50),



  INSERT INTO operator_session VALUES (l_operator_session.*)

  RETURN sqlca.sqlerrd[2]
END FUNCTION

##################################################
#
##################################################
FUNCTION plus_counter_operator_session()
  DEFINE l_operator_session RECORD LIKE operator_session.*
  DEFINE l_session_id LIKE operator_session.session_id   
  DEFINE   l_count LIKE operator_session.session_counter

	#CALL fgl_winmessage("plus_counter_operator_session()","plus_counter_operator_session()","info")
	LET l_session_id = fgl_getenv("QX_SESSION_ID")
	LET  l_count = count_session_record(l_session_id)
	#SELECT count(*)
	#	INTO l_count
  #  FROM operator_session
  #  WHERE operator_session.session_id = l_session_id

	CASE l_count
		WHEN 0    
			DISPLAY "session_id record does not exist - can NOT increment"
			return l_count
		WHEN l_count > 0 -- exists
			#DISPLAY "session id record exists - increment counter by one +1"
		WHEN l_count < 0
			DISPLAY "serious problem..."
	END CASE
	

	
  SELECT operator_session.* 
    INTO l_operator_session.*
    FROM operator_session
    WHERE operator_session.session_id = l_session_id

	LET l_operator_session.session_counter = l_operator_session.session_counter + 1

  UPDATE operator_session    ---update record with incremented session counter +1
    SET 
    	session_counter = l_operator_session.session_counter,
			session_modified = CURRENT YEAR TO Second 
    WHERE operator_session.session_id = l_operator_session.session_id

{
 IF sqlca.sqlcode THEN
    #CALL fgl_winmessage("Contact Create","Contact creation failed!","stop")
    CALL fgl_winmessage("sqlca.sqlcode >0",sqlca.sqlcode,"stop")
  ELSE  
    #CALL fgl_winmessage("Contact Create","New contact record successfully created!","info")
    CALL fgl_winmessage("sqlca.sqlcode ELSE",sqlca.sqlcode,"info")
  END IF
}


  #DISPLAY "AFTER PLUS l_operator_session.session_counter=", l_operator_session.session_counter  
   
	RETURN  l_operator_session.session_counter  
END FUNCTION


##################################################
#
##################################################
FUNCTION minus_counter_operator_session()
  DEFINE l_operator_session RECORD LIKE operator_session.*
  DEFINE l_session_id LIKE operator_session.session_id   
  DEFINE   l_count LIKE operator_session.session_counter

#CALL fgl_winmessage("minus_counter_operator_session()","minus_counter_operator_session()","info")

	LET l_session_id = fgl_getenv("QX_SESSION_ID")
	SELECT count(*)
		INTO l_count
    FROM operator_session
    WHERE operator_session.session_id = l_session_id
    
	IF l_count < 1 THEN  --session id does NOT exist
		DISPLAY "session_id record does not exist"		
		RETURN -1
	END IF
	
  SELECT operator_session.* 
    INTO l_operator_session.*
    FROM operator_session
    WHERE operator_session.session_id = l_session_id

	IF l_operator_session.session_counter < 1 THEN
		DISPLAY "Session Counter is already 0 and can not be de-crement any further"
		DISPLAY "l_operator_session.session_counter", l_operator_session.session_counter
		RETURN l_operator_session.session_counter
	ELSE

		LET l_operator_session.session_counter = l_operator_session.session_counter - 1
	END IF
	
  UPDATE operator_session    ---update record with decremented session counter -1
    SET 
    	session_counter = l_operator_session.session_counter,
			session_modified = CURRENT YEAR TO Second  
    WHERE operator_session.session_id = l_operator_session.session_id
{
 IF sqlca.sqlcode THEN
    #CALL fgl_winmessage("Contact Create","Contact creation failed!","stop")
    CALL fgl_winmessage("sqlca.sqlcode >0",sqlca.sqlcode,"stop")
  ELSE  
    #CALL fgl_winmessage("Contact Create","New contact record successfully created!","info")
    CALL fgl_winmessage("sqlca.sqlcode ELSE",sqlca.sqlcode,"info")
  END IF
}
  #DISPLAY "AFTER MINUS l_operator_session.session_counter=", l_operator_session.session_counter  
	RETURN  l_operator_session.session_counter  
END FUNCTION

FUNCTION disable_session_record(p_session_id)
	DEFINE p_session_id LIKE operator_session.session_id
	DEFINE tempString STRING
	
	IF count_session_record(p_session_id) > 0 THEN --session record found/exists
		#Disable session record
		UPDATE operator_session    ---update record with incremented session counter +1
    	SET 
    	expired = 1,
			session_modified = CURRENT YEAR TO Second 
    WHERE operator_session.session_id = p_session_id


		IF sqlca.sqlcode <> 0 THEN  --0=success
  	  #CALL fgl_winmessage("Contact Create","Contact creation failed!","stop")
			LET tempString = "Could not disable (update) the session record\n Warning Code (sqlca.sqlcode):", trim(sqlca.sqlcode)
    	CALL fgl_winmessage("Problem with session record", tempString,"warning")
		END IF
		
	END IF
END FUNCTION

FUNCTION disable_current_session_record()
  CALL set_expired_current_session_record(TRUE)
  #CALL fgl_winmessage("disable current session","disable current session","info")
END FUNCTION

FUNCTION enable_current_session_record()
  CALL set_expired_current_session_record(FALSE)
  #CALL fgl_winmessage("enable current session","enable current session","info")
END FUNCTION


FUNCTION set_expired_current_session_record(p_expired)
	DEFINE p_expired LIKE operator_session.expired
	DEFINE l_session_id LIKE operator_session.session_id
	DEFINE tempString STRING
	
	LET l_session_id = fgl_getenv("QX_SESSION_ID")
	
	IF count_session_record(l_session_id) > 0 THEN --session record found/exists
		#Disable session record
		UPDATE operator_session    ---update record with incremented session counter +1
    	SET 
    	expired = p_expired,
			session_modified = CURRENT YEAR TO Second 
    WHERE operator_session.session_id = l_session_id


		IF sqlca.sqlcode <> 0 THEN  --0=success
  	  #CALL fgl_winmessage("Contact Create","Contact creation failed!","stop")
			LET tempString = "Could not disable (update) the session record\n Warning Code (sqlca.sqlcode):", trim(sqlca.sqlcode)
    	CALL fgl_winmessage("Problem with session record", tempString,"warning")
		END IF
		
	END IF
END FUNCTION



FUNCTION session_login()
	DEFINE l_operator_id LIKE operator_session.operator_id
	CALL plus_counter_operator_session()
	LET l_operator_id = get_operator_session_operator_id(fgl_getenv("QX_SESSION_ID"))
	CALL init_global_operator_by_id(l_operator_id)
END FUNCTION

##################################################
# FUNCTION get_operator_session_operator_id(p_session_id)
##################################################
FUNCTION get_operator_session_operator_id(p_session_id)
	DEFINE p_session_id LIKE operator_session.session_id
	DEFINE l_operator_id LIKE operator_session.operator_id
	DEFINE l_operator_session RECORD LIKE operator_session.*
	
  SELECT operator_session.operator_id 
    INTO l_operator_id
    FROM operator_session
    WHERE operator_session.session_id = p_session_id
    
    
	RETURN l_operator_id
	

END FUNCTION
	
##################################################
# FUNCTION count_session_record(p_session_id)
##################################################
FUNCTION count_session_record(p_session_id)
  DEFINE p_session_id LIKE operator_session.session_id   
  DEFINE   l_count LIKE operator_session.session_counter


	SELECT count(*)
		INTO l_count
    FROM operator_session
    WHERE operator_session.session_id = p_session_id
    

   
	RETURN  l_count  
END FUNCTION

	
	
	
	
	
##################################################
# FUNCTION current_session_expired()
##################################################
FUNCTION current_session_expired()
  DEFINE l_session_id LIKE operator_session.session_id
  DEFINE l_expired LIKE operator_session.expired   
  #DEFINE   l_count LIKE operator_session.session_counter

	LET l_session_id = fgl_getenv("QX_SESSION_ID")
	
	SELECT expired
		INTO l_expired
    FROM operator_session
    WHERE operator_session.session_id = l_session_id
    

   
	RETURN  l_expired  
END FUNCTION




##################################################
# FUNCTION operator_session_delete(p_operator_session_id)
#
# Delete operator session record
#
# RETURN NONE
##################################################
FUNCTION operator_session_delete(p_session_id)
  DEFINE p_session_id LIKE operator_session.session_id
  # Delete - 
  IF yes_no(get_str(817),get_str(1053)) THEN
    DELETE FROM operator_session WHERE operator_session.session_id = p_session_id
    #"Operator with the operator ID was removed"
    LET tmp_str = "Remove Operator Session ID " , p_session_id  --get_str(1054) , p_session_id
    CALL fgl_winmessage("Operator Session ID Removed", tmp_str ,"info")  --get_str(817)
  END IF

END FUNCTION