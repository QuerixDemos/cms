##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the string-tool management library
#
# Note: String-Tool strings are used by the qxt library
#       String-App  strings are used by the application i.e. CMS or Guidemo
#       qxt_tbi_tooltip strings are used by the qxt_db_toolbar library
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS

#tbi_tooltip TYPE
  DEFINE
    t_qxt_string_tool_rec TYPE AS  
      RECORD
        string_id           LIKE qxt_string_tool.string_id,
        language_id         LIKE qxt_string_tool.language_id,
        string_data         LIKE qxt_string_tool.string_data,
        category1_id        LIKE qxt_string_tool.category1_id,
        category2_id        LIKE qxt_string_tool.category2_id,
        category3_id        LIKE qxt_string_tool.category3_id
      END RECORD

  DEFINE
    t_qxt_string_tool_form_rec TYPE AS  
      RECORD
        string_id            LIKE qxt_string_tool.string_id,
        language_name        LIKE qxt_language.language_name,
        string_data          LIKE qxt_string_tool.string_data,
        category1_data       LIKE qxt_str_category.category_data,
        category2_data       LIKE qxt_str_category.category_data,
        category3_data       LIKE qxt_str_category.category_data

      END RECORD

  DEFINE
    t_qxt_string_tool_translate_form_rec TYPE AS  
      RECORD
        string_id            LIKE qxt_string_tool.string_id,
        language_name_1      LIKE qxt_language.language_name,
        string_data_1        LIKE qxt_string_tool.string_data,
        language_name_2      LIKE qxt_language.language_name,
        string_data_2        LIKE qxt_string_tool.string_data,
        language_name_3      LIKE qxt_language.language_name,
        string_data_3        LIKE qxt_string_tool.string_data
      END RECORD

END GLOBALS


