##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the tb table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
#
####################################################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


###########################################################
# FUNCTION qxt_tb_export_file(p_application_id,p_toolbar_unl_filename,p_tooltip_unl_filename)
#
# Export toolbar (toolbar & tooltip) file for qxt_file 
#
# RETURN NONE
###########################################################
FUNCTION qxt_tb_export_file(p_application_id,p_toolbar_unl_filename,p_tooltip_unl_filename,p_language_id)
  DEFINE
    p_application_id             LIKE qxt_application.application_id,
    p_toolbar_unl_filename       VARCHAR(150),
    p_tooltip_unl_filename       VARCHAR(150),
    p_language_id                LIKE qxt_language.language_id,
    l_toolbar_unl_filename       VARCHAR(150),
    l_tooltip_unl_filename       VARCHAR(150),
    msg                          VARCHAR(200),
    local_debug                  SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "qxt_tb_export_file() - p_application_id=", p_application_id
    DISPLAY "qxt_tb_export_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
    DISPLAY "qxt_tb_export_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  IF p_language_id IS NULL THEN
    LET p_language_id = get_language()
  END IF

  LET l_toolbar_unl_filename = p_toolbar_unl_filename
  LET l_tooltip_unl_filename = p_tooltip_unl_filename

  LET p_toolbar_unl_filename = qxt_toolbar_export_file(p_toolbar_unl_filename,p_application_id) 

  IF local_debug THEN
    DISPLAY "qxt_tb_export_file() - *p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF

  IF p_toolbar_unl_filename IS NOT NULL THEN
    LET msg = "Toolbar file ", p_toolbar_unl_filename CLIPPED, " successfully exported !"
    CALL fgl_winmessage("Toolbar File exported",msg, "info")
  ELSE
    LET msg = "Toolbar file ", l_toolbar_unl_filename CLIPPED, " failed to be exported !"
    CALL fgl_winmessage("Toolbar File export FAILED",msg, "info")
  END IF

  LET p_tooltip_unl_filename = qxt_tooltip_export_file(p_tooltip_unl_filename,p_application_id,p_language_id)



  IF local_debug THEN
    DISPLAY "qxt_tb_export_file() - *p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  IF p_tooltip_unl_filename IS NOT NULL THEN
    LET msg = "Tooltip file ", p_tooltip_unl_filename CLIPPED, " successfully exported !"
    CALL fgl_winmessage("Tooltip File exported",msg, "info")
  ELSE
    LET msg = "Tooltip file ", l_tooltip_unl_filename CLIPPED, " failed to be exported !"
    CALL fgl_winmessage("Tooltip File export FAILED",msg, "info")
  END IF
END FUNCTION


###########################################################
# FUNCTION qxt_toolbar_export_file(p_toolbar_unl_filename,p_application_id)
#
# Export toolbar file for qxt_file library
#
# RETURN NONE
###########################################################
FUNCTION qxt_toolbar_export_file(p_toolbar_unl_filename,p_application_id)
  DEFINE
    p_toolbar_unl_filename   VARCHAR(150),
    p_application_id         LIKE qxt_application.application_id,
    local_debug              SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "qxt_toolbar_export_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF

  IF p_toolbar_unl_filename IS NULL THEN
    LET p_toolbar_unl_filename = get_toolbar_unl_filename()
  END IF

  IF local_debug THEN
    DISPLAY "qxt_toolbar_export_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF

  # and add path  
  LET p_toolbar_unl_filename = get_unl_path(p_toolbar_unl_filename)

  IF local_debug THEN
    DISPLAY "qxt_toolbar_export_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF


  #Final check
  IF p_toolbar_unl_filename IS NULL THEN
    CALL fgl_winmessage("Error in qxt_toolbar_export_file()","Error in qxt_toolbar_export_file()\nNo export toolbar_unl_filename was specified in the function call\nand also not found in the toolbar.cfg","error")
    RETURN NULL
  END IF


  #Finally, export the file
  RETURN qxt_tb_export_toolbar_file(p_toolbar_unl_filename,p_application_id)

END FUNCTION





###########################################################
# FUNCTION qxt_tooltip_export_file(p_tooltip_unl_filename)
#
# Do Nothing - just stay compatible with db library
#
# RETURN NONE
###########################################################
FUNCTION qxt_tooltip_export_file(p_tooltip_unl_filename,p_application_id,p_language_id)
  DEFINE
    p_tooltip_unl_filename  VARCHAR(150),
    p_application_id        LIKE qxt_application.application_id,
    p_language_id           LIKE qxt_language.language_id,
    local_debug             SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "qxt_tooltip_export_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  IF p_tooltip_unl_filename IS NULL THEN
    LET p_tooltip_unl_filename = get_tooltip_unl_filename()
  END IF


  IF local_debug THEN
    DISPLAY "qxt_tooltip_export_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  # and add path  
  LET p_tooltip_unl_filename = get_unl_path(p_tooltip_unl_filename)

  IF local_debug THEN
    DISPLAY "qxt_tooltip_export_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF


  #Final check
  IF p_tooltip_unl_filename IS NULL THEN
    CALL fgl_winmessage("Error in qxt_tooltip_export_file()","Error in qxt_tooltip_export_file()\nNo export tooltip_unl_filename was specified in the function call\nand also not found in the toolbar.cfg","error")
    RETURN NULL
  END IF

  #Finally, export call
  RETURN qxt_tb_export_tooltip_file(p_tooltip_unl_filename,p_application_id,p_language_id)
    

END FUNCTION




###########################################################
# FUNCTION qxt_tb_export_tooltip_file(p_filename)
#
# Export the tooltip file
#
# RETURN NONE
###########################################################
FUNCTION qxt_tb_export_tooltip_file(p_filename,p_application_id,p_language_id)
  DEFINE 
    p_filename          VARCHAR(150),
    p_application_id    LIKE qxt_tb.application_id,
    p_language_id       LIKE qxt_language.language_id,
    l_tbi_tooltip_rec   OF t_qxt_tbi_tooltip_rec,
    sql_stmt            CHAR(1000),
    local_debug         SMALLINT,
    err_msg             VARCHAR(200)

  LET local_debug = FALSE

  #Currently, we ignore the language and application id
  LET sql_stmt = "SELECT ",
    "string_id, ",
    "language_id, ",
    "string_data ",
    "FROM qxt_tbi_tooltip ",
    "ORDER BY string_id, language_id  ASC "



  PREPARE p_tbi_tooltip FROM sql_stmt
  DECLARE c_tbi_tooltip CURSOR FOR p_tbi_tooltip


  #Open the file
    IF NOT fgl_channel_open_file("stream",p_filename, "w") THEN
      LET err_msg = "Error in qxt_tb_export_tooltip_file()\nCan not open file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_tooltip_file()", err_msg, "error")
      RETURN NULL
    END IF

    IF NOT fgl_channel_set_delimiter("stream","|") THEN
      LET err_msg = "Error in qxt_tb_export_tooltip_file()\nCan not set delimiter for file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_tooltip_file()", err_msg, "error")
      RETURN NULL
    END IF



  FOREACH c_tbi_tooltip INTO l_tbi_tooltip_rec.*
    CALL fgl_channel_write("stream",l_tbi_tooltip_rec.*) 

    IF local_debug THEN
      DISPLAY "draw_toolbar() - l_tbi_tooltip_rec.string_id=",   l_tbi_tooltip_rec.string_id
      DISPLAY "draw_toolbar() - l_tbi_tooltip_rec.language_id=", l_tbi_tooltip_rec.language_id
      DISPLAY "draw_toolbar() - l_tbi_tooltip_rec.string_data=", l_tbi_tooltip_rec.string_data
    END IF 

  END FOREACH


    IF NOT fgl_channel_close("stream") THEN
      LET err_msg = "Error in qxt_tb_export_tooltip_file()\nCan not close file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_tooltip_file()", err_msg, "error")
      RETURN NULL
    END IF

  RETURN p_filename  --success

END FUNCTION


###########################################################
# FUNCTION qxt_tb_export_toolbar_file(p_filename)
#
# Export the toolbar file
#
# RETURN NONE
###########################################################
FUNCTION qxt_tb_export_toolbar_file(p_filename,p_application_id)
  DEFINE 
    p_filename              VARCHAR(150),
    p_application_id        LIKE qxt_tb.application_id,
    #p_language_id           LIKE qxt_language.language_id,
    l_toolbar_file_rec      OF t_qxt_toolbar_file_rec,
    sql_stmt                CHAR(1000),
    local_debug             SMALLINT,
    err_msg                 VARCHAR(200)

  LET local_debug = FALSE

  IF p_application_id IS NULL THEN
    LET p_application_id = 1  --use 1 as the default
  END IF


  LET sql_stmt = "SELECT ",
      "qxt_tb.tb_name, ",
      "qxt_tb.tb_instance, ",
      "qxt_tbi.event_type_id, ",
      "qxt_tbi.tbi_event_name, ",
      "qxt_tbi_obj.tbi_obj_action_id, ",
      "qxt_tbi.tbi_scope_id, ",
      "qxt_tbi.tbi_position, ",
      "qxt_tbi.tbi_static_id, ",
      "qxt_tbi_obj.icon_filename, ",
      "qxt_tbi_obj.string_id ",


    "FROM ",
      "qxt_tb, ",
      "qxt_tbi, ",
      "qxt_tbi_obj ",

    "WHERE ",
    "qxt_tb.tbi_name = qxt_tbi.tbi_name ",
    "AND ",
    "qxt_tbi.tbi_obj_name = qxt_tbi_obj.tbi_obj_name ",
    "AND ",
    "qxt_tb.application_id = ", trim(p_application_id), " ",

    "ORDER BY tb_name, tb_instance, tbi_event_name  ASC "



  PREPARE p_tbi_toolbar FROM sql_stmt
  DECLARE c_tbi_toolbar CURSOR FOR p_tbi_toolbar


  #Open the file
    IF NOT fgl_channel_open_file("stream",p_filename, "w") THEN
      LET err_msg = "Error in qxt_tb_export_toolbar_file()\nCan not open file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_toolbar_file()", err_msg, "error")
      RETURN NULL
    END IF

    IF NOT fgl_channel_set_delimiter("stream","|") THEN
      LET err_msg = "Error in qxt_tb_export_toolbar_file()\nCan not set delimiter for file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_toolbar_file()", err_msg, "error")
      RETURN NULL
    END IF



  FOREACH c_tbi_toolbar INTO l_toolbar_file_rec.*
    CALL fgl_channel_write("stream",l_toolbar_file_rec.*) 

    IF local_debug THEN
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tb_name=",           l_toolbar_file_rec.tb_name
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tb_instance=",       l_toolbar_file_rec.tb_instance
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.event_type_id=",     l_toolbar_file_rec.event_type_id
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tbi_event_name=",    l_toolbar_file_rec.tbi_event_name
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tbi_obj_action_id=", l_toolbar_file_rec.tbi_obj_action_id
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tbi_scope_id=",      l_toolbar_file_rec.tbi_scope_id
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tbi_position=",      l_toolbar_file_rec.tbi_position
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.tbi_static_id=",     l_toolbar_file_rec.tbi_static_id
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.icon_filename=",     l_toolbar_file_rec.icon_filename
      DISPLAY "qxt_tb_export_toolbar_file() - l_toolbar_file_rec.string_id=",         l_toolbar_file_rec.string_id

    END IF 


  END FOREACH


    IF NOT fgl_channel_close("stream") THEN
      LET err_msg = "Error in qxt_tb_export_toolbar_file()\nCan not close file ", trim(p_filename)
      CALL fgl_winmessage("Error in qxt_tb_export_toolbar_file()", err_msg, "error")
      RETURN NULL
    END IF

  RETURN p_filename

END FUNCTION


#########################################################################################################
# EOF
#########################################################################################################
