##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the document table 
############################################################################################################
#
# FUNCTION:                                    DESCRIPTION:                                      RETURN
# get_document_id(document_filename)           get Document id from name                   l_document_id
# get_document_filename(document_id)           Get document_id from name                       l_document_filename
# get_document_desc(p_document_id)             Get Pay method description from id                l_document_rec.document_desc
# get_document_rec(p_document_id)              Get the document record from pa_method_id       l_document.*
# document_combo_list(cb_field_name)           Populate document combo list from database  NONE
# document_popup()                             Document selection window                   p_document_id
# document_create()                            Create a new Document record                NULL
# document_delete(p_document_id)               Delete a Document record                    NONE
# document_edit(p_document_id)                 Edit Document record                        NONE
# document_input(p_document)                   Input Document details (edit/create)        l_document.*
# document_view(p_document_rec)                View Document record by ID in window-form   NONE
# document_view_by_rec(p_document_rec)         View Document record in window-form         NONE
# grid_header_document_scroll()                Populate Document grid headers              NONE
############################################################################################################
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#


############################################################
# Database
############################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_document_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_document_manager_lib_info()
  RETURN "DB - qxt_db_document_manager"
END FUNCTION 



######################################################################################################################
# Data Access functins
######################################################################################################################

#########################################################
# FUNCTION get_document_id(document_filename)
#
# get Document id from name
#
# RETURN l_document_id
#########################################################
FUNCTION get_document_id(document_filename)
  DEFINE 
    document_filename LIKE qxt_document.document_filename,
    l_document_id     LIKE qxt_document.document_id,
    local_debug       SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_document_id() - document_filename = ", document_filename
  END IF

  SELECT qxt_document.document_id
    INTO l_document_id
    FROM qxt_document
    WHERE qxt_document.document_filename = document_filename

  RETURN l_document_id
END FUNCTION

#########################################################
# FUNCTION get_document_desc(p_document_id)
#
# Get the document TEXT description
#
# RETURN l_document_desc
#########################################################
FUNCTION get_document_desc(p_document_id)
  DEFINE l_document_desc LIKE qxt_document.document_desc
  DEFINE p_document_id LIKE qxt_document.document_id

  SELECT qxt_document.document_desc
    INTO l_document_desc
    FROM qxt_document
    WHERE qxt_document.document_id = p_document_id

  RETURN l_document_desc
END FUNCTION


#########################################################
# FUNCTION get_document_blob(p_document_id)
#
# Get the document blob data
#
# RETURN l_document_blob
#########################################################
FUNCTION get_document_blob(p_document_id)
  DEFINE l_document_blob LIKE qxt_document_blob.document_blob
  DEFINE p_document_id LIKE qxt_document.document_id

  SELECT qxt_document_blob.document_blob
    INTO l_document_blob
    FROM qxt_document_blob
    WHERE qxt_documentt_blob.document_id = p_document_id

  RETURN l_document_blob
END FUNCTION


######################################################
# FUNCTION get_document_rec(p_document_id)
#
# Get the document record from pa_method_id
#
# RETURN l_document.*
######################################################
FUNCTION get_document_rec(p_document_id)
  DEFINE p_document_id  LIKE qxt_document.document_id
  DEFINE l_document     RECORD LIKE qxt_document.*

  SELECT qxt_document.*
    INTO l_document.*
    FROM qxt_document
    WHERE qxt_document.document_id = p_document_id

  RETURN l_document.*
END FUNCTION


######################################################
# FUNCTION get_document_blob_rec(p_document_id)
#
# Get the document_blob record from p_document_id
#
# RETURN l_document_blob_rec.*
######################################################
FUNCTION get_document_blob_rec(p_document_id)
  DEFINE p_document_id           LIKE qxt_document.document_id
  DEFINE l_document_blob_rec     RECORD LIKE qxt_document_blob.*

  SELECT qxt_document_blob.*
    INTO l_document_blob_rec.*
    FROM qxt_document_blob
    WHERE qxt_document_blob.document_id = p_document_id

  RETURN l_document_blob_rec.*
END FUNCTION



######################################################
# FUNCTION get_document_complete_rec(p_document_id)
#
# Get the complete document record (doc and blob data)
#
# RETURN l_document_complete.*
######################################################
FUNCTION get_document_complete_rec(p_document_id)
  DEFINE p_document_id       LIKE qxt_document.document_id
  DEFINE l_document_complete OF t_qxt_document_complete_rec

  SELECT qxt_document.* , qxt_document_blob.document_blob
    INTO l_document_complete.*
    FROM qxt_document , qxt_document_blob
    WHERE qxt_document.document_id = p_document_id
      AND qxt_document.document_id = qxt_document_blob.document_id

  RETURN l_document_complete.*

END FUNCTION


######################################################
# FUNCTION get_blob_document(p_document_id)
#
# Get the document record from pa_method_id
#
# RETURN l_document.*
######################################################
FUNCTION get_blob_document(p_document_id)
  DEFINE 
    p_document_id      LIKE qxt_document.document_id,
    p_server_file_path VARCHAR(250),
    l_document_file    OF t_qxt_document_file

  LET p_server_file_path = get_server_blob_temp_path(get_document_filename(p_document_id))

  LOCATE l_document_file.document_blob IN FILE p_server_file_path

  SELECT qxt_document.document_filename,
         qxt_document.document_blob
    INTO l_document_file.*
    FROM qxt_document
    WHERE qxt_document.document_id = p_document_id


  RETURN l_document_file.*
END FUNCTION


####################################################
# FUNCTION contact_name_count(p_cont_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION document_filename_count(p_filename)
  DEFINE
    p_filename    LIKE qxt_document.document_filename,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_document
      WHERE qxt_document.document_filename = p_filename

RETURN r_count   --0 = unique  0> is not

END FUNCTION



######################################################################################################################
# List and Combo List functions
######################################################################################################################


###################################################################################
# FUNCTION document_combo_list(cb_field_name)
#
# Populate document combo list from database
#
# RETURN NONE
###################################################################################
FUNCTION document_combo_list(cb_field_name)
  DEFINE 
    l_document_arr   DYNAMIC ARRAY OF t_qxt_document_filename_rec,
    row_count        INTEGER,
    current_row      INTEGER,
    cb_field_name    VARCHAR(20)   --form field name for the country combo list field

  DECLARE c_document_scroll2 CURSOR FOR 
    SELECT qxt_document.document_filename 
      FROM qxt_document

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_document_scroll2 INTO l_document_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_document_arr[row_count].document_filename)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


######################################################
# FUNCTION document_popup_data_source()
#
# Data Source (cursor) for document_popup
#
# RETURN NONE
######################################################
FUNCTION document_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "document_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "document_popup_data_source() - p_ord_dir=", p_ord_dir
  END IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "document_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_document.document_id, ",
                 "qxt_document.document_filename, ",
                 "qxt_document.document_app, ",
                 "qxt_document.document_category1, ",
                 "qxt_document.document_category2, ",
                 "qxt_document.document_category3, ",
                 "qxt_document.document_mod_date ",
                 "FROM qxt_document "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "document_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_document FROM sql_stmt
  DECLARE c_document CURSOR FOR p_document




END FUNCTION


######################################################
# FUNCTION document_popup(p_document_id,p_order_field,p_accept_action)
#
# Document selection window
#
# RETURN p_document_id
######################################################
FUNCTION document_popup(p_document_id,p_order_field,p_accept_action)
  DEFINE 
    p_document_id                LIKE qxt_document.document_id,
    l_document_arr               DYNAMIC ARRAY OF t_qxt_document_no_blob_rec,    --RECORD LIKE qxt_document.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "document_popup() - p_document_id=",p_document_id
    DISPLAY "document_popup() - p_order_field=",p_order_field
    DISPLAY "document_popup() - p_accept_action=",p_accept_action
  END IF


  #IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_document_scroll", 2, 8, get_form_path("f_qxt_document_scroll_l2"),FALSE) 
    CALL populate_document_list_form_labels_g()

    CALL fgl_settitle(get_str_tool(950))  --"Document List")

		CALL ui.Interface.setImage("qx://application/icon16/wordprocessing/document_text_picture.png")
		CALL ui.Interface.setText("Document")
		
    CALL grid_header_document_scroll()
  #ELSE  --text
  #  CALL fgl_window_open("w_document_scroll", 2, 8, get_form_path("f_qxt_document_scroll_t"),FALSE) 
  #  CALL populate_document_list_form_labels_t()
  #END IF

  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_document

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    IF question_not_exist_create(NULL) THEN
      CALL document_create(NULL,NULL)
    END IF
  END IF


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL document_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_document INTO l_document_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_document_arr.resize(i)  -- resize dynamic array to remove last dirty element
		END IF



    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      CALL fgl_window_close("w_document_scroll")
      RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "document_popup() - set_count(i)=",i

      DISPLAY "document_popup() - l_document_arr[1].document_id=",l_document_arr[1].document_id
      DISPLAY "document_popup() - l_document_arr[1].document_filename=",l_document_arr[1].document_filename
      DISPLAY "document_popup() - l_document_arr[1].document_app=",l_document_arr[1].document_app
      DISPLAY "document_popup() - l_document_arr[1].document_category1=",l_document_arr[1].document_category1
      DISPLAY "document_popup() - l_document_arr[1].document_category2=",l_document_arr[1].document_category2
      DISPLAY "document_popup() - l_document_arr[1].document_category3=",l_document_arr[1].document_category3
      DISPLAY "document_popup() - l_document_arr[1].document_mod_date=",l_document_arr[1].document_mod_date
    END IF


		
		#CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_document_arr TO sc_document.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE DISPLAY
        CALL publish_toolbar("DocumentList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_document_arr[i].document_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL document_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL document_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL document_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL document_create(NULL,NULL)
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","document_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "document_popup(p_document_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("document_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL document_create(NULL,NULL)
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        #LET i = l_document_arr[i].document_id
        CALL document_edit(l_document_arr[i].document_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_document_arr[i].document_id
        CALL document_delete(i)
        EXIT DISPLAY


      #Grid Column Sort    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_app"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_category1"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_category2"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_category3"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F19)
        LET p_order_field2 = p_order_field
        LET p_order_field = "document_mod_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_document_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_document_id
  ELSE 
    LET i = arr_curr()
    RETURN l_document_arr[i].document_id
  END IF

END FUNCTION



######################################################################################################################
# Edit, Create, Input, Delete,... functions
######################################################################################################################


#################################################
# FUNCTION document_input(p_document)
#
# Input Document details (edit/create)
#
# RETURN l_document.*
#################################################
FUNCTION document_input(p_document)
  DEFINE 
    p_document          OF t_qxt_document_complete_rec,
    l_server_blob_file  VARCHAR(300),  --,
    l_orignal_filename  LIKE qxt_document.document_filename

  LET int_flag = FALSE
  LET l_orignal_filename = p_document.document_filename

  INPUT BY NAME p_document.* WITHOUT DEFAULTS HELP 1

    ON KEY(F9)  --upload
      #upload_blob_document(p_client_file_path,p_dialog)
      CALL upload_blob_document(p_document.document_filename,TRUE) RETURNING l_server_blob_file --tRUE = file dialog

      IF l_server_blob_file IS NOT NULL AND fgl_test("e", l_server_blob_file)THEN
          # free the existing locator
          IF p_document.document_blob IS NOT NULL THEN
            FREE p_document.document_blob
          END IF
          #locate new blob data from file
          LOCATE p_document.document_blob IN FILE l_server_blob_file
          LET p_document.document_filename = fgl_basename(l_server_blob_file)
          DISPLAY p_document.document_filename TO document_filename
          DISPLAY p_document.document_blob TO document_blob


          # DISPLAY BY NAME l_contact_rec.cont_picture
       END IF

    ON KEY(F10)  --download
      CALL download_blob_document_to_client(p_document.document_id,NULL,TRUE)
  #download_blob_document_to_client(p_document_id,p_client_file_path,p_dialog)

    AFTER FIELD document_filename

      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(952), p_document.document_filename,NULL,TRUE)  THEN
        NEXT FIELD document_filename
      END IF

      #Filename must be unique
      IF document_filename_count(p_document.document_filename) THEN
        IF (p_document.document_filename = l_orignal_filename) THEN
          #do nothing for now, this is an edit operation
        ELSE
          DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(RED)
          CALL tl_msg_duplicate_key(get_str_tool(952) ,NULL)
          NEXT FIELD document_filename
        END IF
      ELSE
        DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(NORMAL)
      END IF


    AFTER INPUT
      IF NOT int_flag THEN --user pressed OK
        #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(952), p_document.document_filename,NULL,TRUE)  THEN
          NEXT FIELD document_filename
          CONTINUE INPUT
        END IF

        #Filename must be unique (not for edit operation)
        IF document_filename_count(p_document.document_filename) THEN
          IF NOT (p_document.document_filename = l_orignal_filename) THEN
            DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(RED)
            #You must specify an unique contact name. "
            ERROR get_str_tool(51) CLIPPED, " ",get_str_tool(952)
            NEXT FIELD document_filename
            CONTINUE INPUT
          END IF
        ELSE
          DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(NORMAL)
        END IF

        #BLOB must not be empty
        IF NOT validate_field_value_byte_exists(get_str_tool(959), p_document.document_blob,NULL,TRUE)  THEN
          NEXT FIELD document_filename
          CONTINUE INPUT
        END IF

      ELSE  --user pressed cancel
        #"Do you really want to abort the new/modified record entry ?"
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF 
      END IF  
  END INPUT

  RETURN p_document.*
END FUNCTION



######################################################
# FUNCTION document_create(p_document_id,p_document_filename)
#
# Create a new Document record
#
# RETURN NULL
######################################################
FUNCTION document_create(p_document_id,p_document_filename)
  DEFINE 
    p_document_id       LIKE qxt_document.document_id,
    p_document_filename LIKE qxt_document.document_filename,
    l_document          OF t_qxt_document_complete_rec

  LOCATE l_document.document_desc IN MEMORY
  LOCATE l_document.document_blob IN MEMORY

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_l2"), FALSE) 
    CALL populate_document_form_create_labels_g()

#  ELSE
#    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_t"), TRUE) 
#    CALL populate_document_form_create_labels_t()
#  END IF

  #Default values - document_id
  IF p_document_id IS NOT NULL THEN
    LET l_document.document_id = p_document_id
  ELSE
    LET l_document.document_id = 0
  END IF

  #Default values - doucment_filename
  IF p_document_filename IS NOT NULL THEN
    LET l_document.document_filename = p_document_filename
  END IF

  #Update the modification date
  LET l_document.document_mod_date = TODAY

  LET int_flag = FALSE
  
  CALL document_input(l_document.*)
    RETURNING l_document.*


  #User pressed OK in the previoius INPUT
  IF NOT int_flag THEN
    
    #Update document table
    LET l_document.document_id = 0  --Serial field needs 0 to create a new id
    INSERT INTO qxt_document VALUES (
          l_document.document_id,
          l_document.document_filename, 
          l_document.document_app, 
          l_document.document_category1, 
          l_document.document_category2, 
          l_document.document_category3, 
          l_document.document_mod_date, 
          l_document.document_desc)

    #get the record id from the new activity record
    LET l_document.document_id = sqlca.sqlerrd[2]

    #Update document_blob table
    INSERT INTO qxt_document_blob VALUES (
          l_document.document_id, 
          l_document.document_blob)

  END IF

  #Always
  #Free both blobs and close window
  CALL fgl_window_close("w_document")
  FREE l_document.document_desc
  FREE l_document.document_blob

  #User pressed OK in the previoius INPUT
  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(960),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(960),NULL)
      RETURN l_document.document_id
    END IF


    #RETURN sqlca.sqlerrd[2]

  ELSE   --Cancel
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(960),NULL)
    RETURN NULL
  END IF

END FUNCTION

#################################################
# FUNCTION document_edit(p_document_id)
#
# Edit Document record
#
# RETURN NONE
#################################################
FUNCTION document_edit(p_document_id)
  DEFINE 
    p_document_id LIKE qxt_document.document_id,
    l_document    OF t_qxt_document_complete_rec,
    local_debug   SMALLINT

  LOCATE l_document.document_desc IN MEMORY
  LOCATE l_document.document_blob IN MEMORY

  LET local_debug = FALSE

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_l2"), FALSE) 
    CALL populate_document_form_edit_labels_g()

#  ELSE
#    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_t"), TRUE) 
#    CALL populate_document_form_edit_labels_t()
#  END IF

  SELECT qxt_document.* , qxt_document_blob.document_blob 
    INTO l_document
    FROM qxt_document, qxt_document_blob
    WHERE qxt_document.document_id = p_document_id
      AND qxt_document.document_id = qxt_document_blob.document_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  #Update the modification date
  LET l_document.document_mod_date = TODAY

  #Input the data
  CALL document_input(l_document.*) 
    RETURNING l_document.*

  #User did not press Cancel during input (abort input)
  IF NOT int_flag THEN

    UPDATE qxt_document
      SET document_filename = l_document.document_filename,
          document_app = l_document.document_app,
          document_category1 = l_document.document_category1,
          document_category2 = l_document.document_category2,
          document_category3 = l_document.document_category3,
          document_mod_date = l_document.document_mod_date,
          document_desc = l_document.document_desc
      WHERE qxt_document.document_id = l_document.document_id

    UPDATE qxt_document_blob
      SET document_blob = l_document.document_blob
      WHERE qxt_document_blob.document_id = l_document.document_id

  END IF

  #Always
  #Free both blobs and close window
  CALL fgl_window_close("w_document")
  FREE l_document.document_desc
  FREE l_document.document_blob


  #User did not press Cancel during input (abort input)
  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_modified(0,get_str_tool(960),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_modified(1,get_str_tool(960),NULL)
      RETURN l_document.document_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(960),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF


END FUNCTION


{
        #Filename must not be empty
        IF p_document.document_filename IS NULL THEN  --file name field must not be empty
          ERROR trim(get_str_tool(50)), " ",get_str_tool(951)   --enter data for both fields
          NEXT FIELD document_filename
          CONTINUE INPUT
        END IF

        #Filename must be unique (not for edit operation)
        IF document_filename_count(p_document.document_filename) THEN
          IF (p_document.document_filename = l_orignal_filename) THEN
            #do nothing for now, this is an edit operation
          ELSE
            DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(RED)
            #You must specify an unique contact name. "
            ERROR get_str_tool(51) CLIPPED, " ",get_str_tool(951)
            NEXT FIELD document_filename
            CONTINUE INPUT
          END IF
        ELSE
          DISPLAY p_document.document_filename TO document_filename ATTRIBUTE(NORMAL)
        END IF

        #BLOB must not be empty
        IF p_document.document_blob IS NULL THEN  --blob field must not be empty
          ERROR get_str_tool(50) CLIPPED, " ",   get_str_tool(956) 
          NEXT FIELD document_blob
          CONTINUE INPUT
        END IF

}




#################################################
# FUNCTION document_delete(p_document_id)
#
# Delete a Document record
#
# RETURN NONE
#################################################
FUNCTION document_delete(p_document_id)
  DEFINE p_document_id LIKE qxt_document.document_id
  #do you really want to delete...
  IF question_delete_record(get_str_tool(960), NULL) THEN
    BEGIN WORK
      DELETE FROM qxt_document 
        WHERE document_id = p_document_id

      DELETE FROM qxt_document_blob 
        WHERE document_id = p_document_id
    COMMIT WORK

  END IF

END FUNCTION



######################################################################################################################
# Record View functions
######################################################################################################################

#################################################
# FUNCTION document_view(p_document_rec)
#
# View Document record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION document_view(p_document_id)
  DEFINE 
    p_document_id LIKE qxt_document.document_id,
    l_document_rec OF t_qxt_document_complete_rec

  LOCATE l_document_rec.document_desc IN MEMORY
  LOCATE l_document_rec.document_blob IN MEMORY
  

  CALL get_document_complete_rec(p_document_id) RETURNING l_document_rec.*
  CALL document_view_by_rec(l_document_rec.*)

  FREE l_document_rec.document_desc
  FREE l_document_rec.document_blob

END FUNCTION


#################################################
# FUNCTION document_view_by_rec(p_document_rec)
#
# View Document record in window-form
#
# RETURN NONE
#################################################
FUNCTION document_view_by_rec(p_document_rec)
  DEFINE 
    p_document_rec   OF t_qxt_document_complete_rec,
    inp_char         CHAR

#  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_l2"), FALSE) 
    CALL populate_document_view_form_labels_g()

#  ELSE
#    CALL fgl_window_open("w_document", 3, 3, get_form_path("f_qxt_document_det_t"), TRUE) 
#    CALL populate_document_view_form_labels_t()
#
#  END IF

  DISPLAY BY NAME p_document_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_document")
END FUNCTION




###############################################################################################################################
# Screen display functions
###############################################################################################################################


####################################################
# FUNCTION grid_header_document_scroll()
#
# Populate Document grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_document_scroll()
  CALL fgl_grid_header("sc_document","document_id",       get_str_tool(951),"right","F13")  -- id
  CALL fgl_grid_header("sc_document","document_filename", get_str_tool(952),"left","F14")  --Filename
  CALL fgl_grid_header("sc_document","document_app",      get_str_tool(953),"left","F15")  
  CALL fgl_grid_header("sc_document","document_category1",get_str_tool(954),"left","F16")  
  CALL fgl_grid_header("sc_document","document_category2",get_str_tool(955),"left","F17")  
  CALL fgl_grid_header("sc_document","document_category3",get_str_tool(956),"left","F18")  
  CALL fgl_grid_header("sc_document","document_mod_date",get_str_tool(957),"left","F19")  

END FUNCTION

#######################################################
# FUNCTION populate_document_form_labels_g()
#
# Populate Document form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_labels_g()

  CALL fgl_settitle(get_str_tool(950))

  DISPLAY get_str_tool(950) TO lbTitle

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_document_form_labels_t()
#
# Populate Document form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_labels_t()

  DISPLAY get_str_tool(950) TO lbTitle

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_document_view_form_labels_g()
#
# Populate Document form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_document_view_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxLeft","Details")  
  CALL updateUILabel("cntDetail1GroupBoxRight","Document Preview")  
  CALL fgl_settitle(trim(get_str_tool(950)) || " - " || get_str_tool(15)) --Document view

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(15) TO lbTitle --Document view

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_document_view_form_labels_t()
#
# Populate Document form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_document_view_form_labels_t()

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(15) TO lbTitle --Document view

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_document_form_edit_labels_g()
#
# Populate Document form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_edit_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxLeft","Details")  
  CALL updateUILabel("cntDetail1GroupBoxRight","Document Preview") 

  CALL fgl_settitle(trim(get_str_tool(950)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download



END FUNCTION



#######################################################
# FUNCTION populate_document_form_edit_labels_t()
#
# Populate Document form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_edit_labels_t()

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_document_form_create_labels_g()
#
# Populate Document form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_create_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxLeft","Details")  
  CALL updateUILabel("cntDetail1GroupBoxRight","Document Preview") 

  CALL fgl_settitle(trim(get_str_tool(950)) || " - " || get_str_tool(14))  --Create

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(14) TO lbTitle  --Create

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download



END FUNCTION



#######################################################
# FUNCTION populate_document_form_create_labels_t()
#
# Populate Document form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_document_form_create_labels_t()

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(14) TO lbTitle  --Create

  DISPLAY get_str_tool(951) TO dl_f1
  DISPLAY get_str_tool(952) TO dl_f2
  DISPLAY get_str_tool(953) TO dl_f3
  DISPLAY get_str_tool(954) TO dl_f4
  DISPLAY get_str_tool(955) TO dl_f5
  DISPLAY get_str_tool(956) TO dl_f6
  DISPLAY get_str_tool(957) TO dl_f7
  DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION




#######################################################
# FUNCTION populate_document_list_form_labels_g()
#
# Populate Document list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_document_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(950)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_document_scroll()

  #DISPLAY get_str_tool(951) TO dl_f1
  #DISPLAY get_str_tool(952) TO dl_f2
  #DISPLAY get_str_tool(953) TO dl_f3
  #DISPLAY get_str_tool(954) TO dl_f4
  #DISPLAY get_str_tool(955) TO dl_f5
  #DISPLAY get_str_tool(956) TO dl_f6
  #DISPLAY get_str_tool(957) TO dl_f7
  #DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete



END FUNCTION


#######################################################
# FUNCTION populate_document_list_form_labels_t()
#
# Populate Document list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_document_list_form_labels_t()

  DISPLAY trim(get_str_tool(950)) || " - " || get_str_tool(16) TO lbTitle  --List

  #DISPLAY get_str_tool(951) TO dl_f1
  #DISPLAY get_str_tool(952) TO dl_f2
  #DISPLAY get_str_tool(953) TO dl_f3
  #DISPLAY get_str_tool(954) TO dl_f4
  #DISPLAY get_str_tool(955) TO dl_f5
  #DISPLAY get_str_tool(956) TO dl_f6
  #DISPLAY get_str_tool(957) TO dl_f7
  #DISPLAY get_str_tool(958) TO dl_f8
  #DISPLAY get_str_tool(959) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION








