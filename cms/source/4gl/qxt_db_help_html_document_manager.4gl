##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_language_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_html_document_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_html_document_manager_lib_info()
  RETURN "qxt_db_help_html_document_manager"
END FUNCTION 



######################################################################################################################
# Data Access functins
######################################################################################################################


###########################################################
# FUNCTION get_help_url_file(p_help_id,p_filename)
#
# Get the help_file from the server to the client and return the client file name inc. path
#
# RETURN p_client_file_path
###########################################################
FUNCTION get_help_url_file(p_help_id,p_filename)
  DEFINE
    p_help_id          INTEGER,
    #p_language_id      INTEGER,
    p_filename         VARCHAR(200),
    l_client_filename_with_path  VARCHAR(200),
    l_server_filename_with_path  VARCHAR(200),
    local_debug        SMALLINT,
    tmp_msg            VARCHAR(200)


  IF local_debug THEN
    DISPLAY "get_help_url_file() - p_help_id=",                            p_help_id
    DISPLAY "get_help_url_file() - language_id=",                          get_language()
    DISPLAY "get_help_url_file() - p_filename=",                           p_filename
    DISPLAY "get_help_url_file() - l_client_filename_with_path=",          l_client_filename_with_path
    DISPLAY "get_help_url_file() - l_server_filename_with_path=",          l_server_filename_with_path
  END IF  

  LET l_client_filename_with_path = get_client_temp_path(get_help_html_base_dir_path(get_language_dir_path(get_language(),p_filename)))
  LET l_server_filename_with_path = get_help_html_base_dir_path(get_language_dir_path(get_language(),p_filename))

  IF local_debug THEN
    DISPLAY "get_help_url_file() - p_help_id=",                            p_help_id
    DISPLAY "get_help_url_file() - language_id=",                          get_language()
    DISPLAY "get_help_url_file() - p_filename=",                           p_filename
    DISPLAY "get_help_url_file() - l_client_filename_with_path=",          l_client_filename_with_path
    DISPLAY "get_help_url_file() - l_server_filename_with_path=",          l_server_filename_with_path
  END IF  

  #Only download the file, if it doesn't already exist already on client - no version control
  IF fgl_getproperty("gui","system.file.exists",l_client_filename_with_path) IS NULL THEN 

    IF local_debug THEN
      DISPLAY "1a - get_help_url_file() File does not exist on client - Let's attempt to download the file..."
    END IF

   # Download file from database - blob to app server
   LET l_client_filename_with_path = download_blob_help_html_to_client(p_help_id,get_language(),l_client_filename_with_path,0)

  ELSE
    IF local_debug THEN
      DISPLAY "1b - DID NOT download the file - It's already on the local Client PC..."
    END IF
  END IF      

  RETURN l_client_filename_with_path

END FUNCTION


#########################################################
# FUNCTION get_help_html_doc_id(p_help_filename,p_language_id)
#
# get help_html id from name
#
# RETURN l_help_html_doc_id
#########################################################
FUNCTION get_help_html_doc_id(p_help_html_filename,p_language_id)
  DEFINE 
    p_help_html_filename   LIKE qxt_help_html_doc.help_html_filename,
    p_language_id          LIKE qxt_help_html_doc.language_id,
    l_help_html_doc_id         LIKE qxt_help_html_doc.help_html_doc_id,
    local_debug            SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_html_doc_id() - p_help_filename = ", p_help_html_filename
  END IF

  SELECT help_html_doc_id
    INTO l_help_html_doc_id
    FROM qxt_help_html_doc
    WHERE help_html_filename = p_help_html_filename
      AND language_id   = p_language_id

  RETURN l_help_html_doc_id
END FUNCTION



#########################################################
# FUNCTION get_help_html_doc_data(p_help_html_doc_id,p_language_id)
#
# Get the help_html TEXT description
#
# RETURN l_help_html_data
#########################################################
FUNCTION get_help_html_doc_data(p_help_html_doc_id,p_language_id)
  DEFINE 
    l_help_html_data LIKE qxt_help_html_doc.help_html_data,
    p_language_id   LIKE qxt_help_html_doc.language_id,
    p_help_html_doc_id LIKE qxt_help_html_doc.help_html_doc_id

  LOCATE l_help_html_data IN MEMORY

  SELECT help_html_data
    INTO l_help_html_data
    FROM qxt_help_html_doc
    WHERE help_html_doc_id = p_help_html_doc_id
      AND language_id   = p_language_id

  RETURN l_help_html_data
END FUNCTION


######################################################
# FUNCTION get_help_html_doc_rec(p_help_html_doc_id,p_language_id)
#
# Get the help_html record from pa_method_id
#
# RETURN l_help_html_doc_rec.*
######################################################
FUNCTION get_help_html_doc_rec(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id  LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id   LIKE qxt_help_html_doc.language_id,
    l_help_html_doc_rec     RECORD LIKE qxt_help_html_doc.*

  LOCATE l_help_html_doc_rec.help_html_data IN MEMORY

  SELECT *
    INTO l_help_html_doc_rec.*
    FROM qxt_help_html_doc
    WHERE help_html_doc_id = p_help_html_doc_id
      AND language_id   = p_language_id

  RETURN l_help_html_doc_rec.*
END FUNCTION



######################################################################################################################
# List Management and Combo List functins
######################################################################################################################

######################################################
# FUNCTION help_html_doc_popup_data_source()
#
# Data Source (cursor) for help_html_doc_popup
#
# RETURN NONE
######################################################
FUNCTION help_html_doc_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "help_html_doc_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "help_html_doc_id, ",
                 "qxt_language.language_name, ",
                 "help_html_filename, ",
                 "help_html_mod_date ",
                 "FROM qxt_help_html_doc, qxt_language ",
                 "WHERE qxt_help_html_doc.language_id = qxt_language.language_id "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "help_html_doc_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_help_html FROM sql_stmt
  DECLARE c_help_html CURSOR FOR p_help_html

END FUNCTION


######################################################
# FUNCTION help_html_doc_popup(p_help_html_doc_id,p_language_id,p_order_field,p_accept_action)
#
# help_html selection window
#
# RETURN p_help_html_doc_id
######################################################
FUNCTION help_html_doc_popup(p_help_html_doc_id,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_help_html_doc_id           LIKE qxt_help_html_doc.help_html_doc_id,  --default return value if user cancels
    p_language_id                LIKE qxt_help_html_doc.language_id,   --default return value if user cancels
    l_help_html_arr              DYNAMIC ARRAY OF t_qxt_help_html_doc_no_blob_form_rec,    --RECORD LIKE qxt_help_html_doc.*,  
    l_arr_size                   SMALLINT,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_help_html_doc_id               LIKE qxt_help_html_doc.help_html_doc_id,
    l_language_id                LIKE qxt_help_html_doc.language_id,
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET l_arr_size = 100 -- due to dynamic array ...sizeof(l_help_html_arr)


  IF local_debug THEN
    DISPLAY "help_html_doc_popup() - p_help_html_doc_id=",p_help_html_doc_id
    DISPLAY "help_html_doc_popup() - p_order_field=",p_order_field
    DISPLAY "help_html_doc_popup() - p_accept_action=",p_accept_action
  END IF


	CALL fgl_window_open("w_help_html_scroll", 2, 8, get_form_path("f_qxt_help_html_document_scroll_l2"),FALSE) 
	CALL populate_help_html_doc_list_form_labels_g()


		CALL ui.Interface.setImage("qx://application/icon16/wordprocessing/document_text_picture.png")
		CALL ui.Interface.setText("Document")


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL help_html_doc_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_help_html INTO l_help_html_arr[i].*
      IF local_debug THEN
        DISPLAY "help_html_doc_popup() - i=",i
        DISPLAY "help_html_doc_popup() - l_help_html_arr[i].help_html_doc_id=",l_help_html_arr[i].help_html_doc_id
        DISPLAY "help_html_doc_popup() - l_help_html_arr[i].language_name=",l_help_html_arr[i].language_name
        DISPLAY "help_html_doc_popup() - l_help_html_arr[i].help_html_filename=",l_help_html_arr[i].help_html_filename
        DISPLAY "help_html_doc_popup() - l_help_html_arr[i].help_html_mod_date=",l_help_html_arr[i].help_html_mod_date
    END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_help_html_arr.resize(i)  -- resize dynamic array to remove last dirty element
		END IF
		
		
    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_help_html_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "help_html_doc_popup() - set_count(i)=",i
    END IF


		
    #CALL set_count(i)

    LET int_flag = FALSE
    DISPLAY ARRAY l_help_html_arr TO sc_help_html.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE ROW
        LET i = arr_curr()
        LET l_help_html_doc_id = l_help_html_arr[i].help_html_doc_id
        LET l_language_id = get_language_id(l_help_html_arr[i].language_name)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        #LET i = l_help_html_arr[i].help_html_doc_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL help_html_doc_view(l_help_html_doc_id, l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL help_html_doc_edit(l_help_html_doc_id, l_language_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL help_html_doc_delete(l_help_html_doc_id, l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL help_html_doc_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","help_html_doc_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "help_html_doc_popup(p_help_html_doc_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("help_html_doc_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL help_html_doc_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL help_html_doc_edit(l_help_html_doc_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL help_html_doc_delete(l_help_html_doc_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F7) -- view
        CALL help_html_doc_view(l_help_html_doc_id, l_language_id)
        EXIT DISPLAY


      #Grid Column Sort
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "help_html_doc_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "help_html_filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_help_html_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_help_html_doc_id,  p_language_id 
  ELSE 
    RETURN l_help_html_doc_id,  l_language_id 
  END IF

END FUNCTION


######################################################################################################################
# Edit/Create/Delete/Input Functions
######################################################################################################################


######################################################
# FUNCTION help_html_doc_create()
#
# Create a new help_html record
#
# RETURN NULL
######################################################
FUNCTION help_html_doc_create()
  DEFINE 
    l_help_html_doc_rec RECORD LIKE qxt_help_html_doc.*

  LOCATE l_help_html_doc_rec.help_html_data IN MEMORY

	CALL fgl_window_open("w_help_html", 3, 3, get_form_path("f_qxt_help_html_document_det_l2"), FALSE) 
	CALL populate_help_html_doc_form_create_labels_g()



  LET int_flag = FALSE
  
  #Initialise some variables
  LET l_help_html_doc_rec.help_html_doc_id = 0
  LET l_help_html_doc_rec.help_html_mod_date = TODAY
  LET l_help_html_doc_rec.language_id = 1

  # CALL the INPUT
  CALL help_html_doc_input(l_help_html_doc_rec.*)
    RETURNING l_help_html_doc_rec.*


  IF NOT int_flag THEN
    INSERT INTO qxt_help_html_doc VALUES (
      l_help_html_doc_rec.help_html_doc_id,
      l_help_html_doc_rec.language_id, 
      l_help_html_doc_rec.help_html_filename,
      l_help_html_doc_rec.help_html_mod_date,
      l_help_html_doc_rec.help_html_data)

  END IF

  CALL fgl_window_close("w_help_html")
  FREE l_help_html_doc_rec.help_html_data


  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_help_html_doc_rec.help_html_doc_id, l_help_html_doc_rec.language_id
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL
  END IF

END FUNCTION


#################################################
# FUNCTION help_html_doc_edit(p_help_html_doc_id)
#
# Edit help_html record
#
# RETURN NONE
#################################################
FUNCTION help_html_doc_edit(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id       LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id            LIKE qxt_help_html_doc.language_id,
    l_key1_help_html_doc_id  LIKE qxt_help_html_doc.help_html_doc_id,
    l_key2_language_id       LIKE qxt_help_html_doc.language_id,
    #l_help_html_form_rec    OF t_help_html_form_rec,
    l_help_html_rec          RECORD LIKE qxt_help_html_doc.*,
    local_debug              SMALLINT

  LOCATE l_help_html_rec.help_html_data IN MEMORY

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "help_html_doc_edit() - Function Entry Point"
    DISPLAY "help_html_doc_edit() - p_help_html_doc_id=", p_help_html_doc_id
    DISPLAY "help_html_doc_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key combination for the SQL update
  LET l_key1_help_html_doc_id = p_help_html_doc_id
  LET l_key2_language_id = p_language_id



  #Get the record (by id)
  CALL get_help_html_doc_rec(p_help_html_doc_id,p_language_id) RETURNING l_help_html_rec.*


	CALL fgl_window_open("w_help_html", 3, 3, get_form_path("f_qxt_help_html_document_det_l2"), FALSE) 
	CALL populate_help_html_doc_form_edit_labels_g()

  #Update the modification date
  LET l_help_html_rec.help_html_mod_date = TODAY

  #Call the INPUT
  CALL help_html_doc_input(l_help_html_rec.*) 
    RETURNING l_help_html_rec.*

  IF local_debug THEN
    DISPLAY "help_html_doc_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "help_html_doc_edit() - l_help_html_rec.help_html_doc_id=",l_help_html_rec.help_html_doc_id
      DISPLAY "help_html_doc_edit() - l_help_html_rec.language_id=",l_help_html_rec.language_id
      DISPLAY "help_html_doc_edit() - l_help_html_rec.help_html_filename=",l_help_html_rec.help_html_filename
      DISPLAY "help_html_doc_edit() - l_help_html_rec.help_html_mod_date=",l_help_html_rec.help_html_mod_date
      #DISPLAY "help_html_doc_edit() - l_help_html_rec.help_html_data=",l_help_html_rec.help_html_data
    END IF

    UPDATE qxt_help_html_doc
      SET 
          help_html_doc_id =         l_help_html_rec.help_html_doc_id,
          language_id =              l_help_html_rec.language_id,
          help_html_filename =       l_help_html_rec.help_html_filename,
          help_html_mod_date =       l_help_html_rec.help_html_mod_date,
          help_html_data =           l_help_html_rec.help_html_data
      WHERE qxt_help_html_doc.help_html_doc_id = l_key1_help_html_doc_id
        AND qxt_help_html_doc.language_id  = l_key2_language_id


    IF local_debug THEN
      DISPLAY "help_html_doc_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

  END IF

  #Always DO Section
  CALL fgl_window_close("w_help_html")
  FREE l_help_html_rec.help_html_data 


  #Check if user canceled
  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL,NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)  
      RETURN l_help_html_rec.help_html_doc_id, l_help_html_rec.language_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    LET int_flag = FALSE
    RETURN NULL,NULL
  END IF



END FUNCTION


#################################################
# FUNCTION help_html_doc_input(p_help_html)
#
# Input help_html details (edit/create)
#
# RETURN l_help_html.*
#################################################
FUNCTION help_html_doc_input(p_help_html_rec)
  DEFINE 
    p_help_html_rec            RECORD LIKE qxt_help_html_doc.*,
    l_help_html_form_rec       OF t_qxt_help_html_doc_form_rec,
    l_server_blob_file         VARCHAR(300),  --,
    l_orignal_filename         LIKE qxt_help_html_doc.help_html_filename,
    l_orignal_help_html_doc_id     LIKE qxt_help_html_doc.help_html_doc_id,
    local_debug                SMALLINT

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_filename = p_help_html_rec.help_html_filename
  LET l_orignal_help_html_doc_id = p_help_html_rec.help_html_doc_id

  #copy record data to form_record format record
  CALL copy_help_html_doc_record_to_form_record(p_help_html_rec.*) RETURNING l_help_html_form_rec.*

  ####################
  #Start actual INPUT
  INPUT BY NAME l_help_html_form_rec.* WITHOUT DEFAULTS HELP 1

    ON KEY(F9)  --upload document to blob
      #upload_blob_help_html_doc(p_client_file_path,p_dialog)
      CALL upload_blob_help_html_doc(l_help_html_form_rec.help_html_filename,TRUE) RETURNING l_server_blob_file --tRUE = file dialog

      IF l_server_blob_file IS NOT NULL AND fgl_test("e", l_server_blob_file)THEN
          # free the existing locator
          IF l_help_html_form_rec.help_html_data IS NOT NULL THEN
            FREE l_help_html_form_rec.help_html_data
          END IF
          #locate new blob data from file
          LOCATE l_help_html_form_rec.help_html_data IN FILE l_server_blob_file
          LET l_help_html_form_rec.help_html_filename = fgl_basename(l_server_blob_file)
          DISPLAY l_help_html_form_rec.help_html_filename TO help_html_filename
          DISPLAY l_help_html_form_rec.help_html_data TO help_html_data

          # DISPLAY BY NAME l_contact_rec.cont_picture
       END IF

    ON KEY(F10)  --download blob document to file
      CALL download_blob_help_html_to_client(l_help_html_form_rec.help_html_doc_id,get_language_id(l_help_html_form_rec.language_name),NULL,TRUE)
      #Syntax CALL download_blob_help_html_to_client(p_help_html_doc_id,p_language_id,p_client_file_path,p_dialog)
  #download_blob_help_html_to_client(p_help_html_doc_id,p_client_file_path,p_dialog)

{
    #filename can be duplicated - it's all about help id and language_id
    AFTER FIELD help_html_filename

      IF help_html_doc_filename_count(p_help_html.help_html_filename) THEN
        IF (p_help_html.help_html_filename = l_orignal_filename) THEN
          #do nothing for now, this is an edit operation
        ELSE
          DISPLAY p_help_html.help_html_filename TO help_html_filename ATTRIBUTE(RED)
          #You must specify an unique contact name. "
          ERROR get_str(2410) CLIPPED, " ", p_help_html.help_html_filename 
          NEXT FIELD help_html_filename
        END IF
      ELSE
        DISPLAY p_help_html.help_html_filename TO help_html_filename ATTRIBUTE(NORMAL)
      END IF
}
    AFTER FIELD help_html_filename
      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_html_form_rec.help_html_filename,NULL,TRUE)  THEN
        NEXT FIELD help_html_filename
      END IF

    AFTER FIELD language_name
      #Language must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(972), l_help_html_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD language_name
      END IF

    AFTER FIELD help_html_mod_date
      IF NOT validate_field_value_date_exists(get_str_tool(974), l_help_html_form_rec.help_html_mod_date,NULL,TRUE) THEN
        NEXT FIELD help_html_mod_date
      END IF


    # AFTER INPUT BLOCK
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #Language must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(972), l_help_html_form_rec.language_name,NULL,TRUE)  THEN
          NEXT FIELD language_name
          CONTINUE INPUT
        END IF
      #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(973), l_help_html_form_rec.help_html_filename,NULL,TRUE)  THEN

          NEXT FIELD help_html_filename
          CONTINUE INPUT
        END IF

      #The combination language_id and help_html_doc_id must be unique
      IF help_html_doc_id_and_lang_id_count(l_help_html_form_rec.help_html_doc_id,get_language_id(l_help_html_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_help_html_doc_id <> l_help_html_form_rec.help_html_doc_id THEN  --it is not an edit operation
          CALL fgl_winmessage(get_str_tool(63),get_str_tool(64),"error")
          NEXT FIELD help_html_doc_id
          CONTINUE INPUT
        END IF
      END IF

      IF NOT validate_field_value_date_exists(get_str_tool(974), l_help_html_form_rec.help_html_mod_date,NULL,TRUE) THEN
        NEXT FIELD help_html_mod_date
        CONTINUE INPUT
      END IF

      #BLOB Data must be suplied, otherwise - the help system will not work correctly
      IF NOT validate_field_value_text_exists(get_str_tool(975), l_help_html_form_rec.help_html_data ,"Binary BLOB TEXT Data",TRUE) THEN
        #NEXT FIELD help_html_data --is blob viewer field - can't move the cursor into it
        NEXT FIELD help_html_filename
        CONTINUE INPUT
      END IF
  END INPUT

  IF local_debug THEN
    DISPLAY "help_html_doc_input() - l_help_html_form_rec.help_html_doc_id=",l_help_html_form_rec.help_html_doc_id
    DISPLAY "help_html_doc_input() - l_help_html_form_rec.language_name=",l_help_html_form_rec.language_name
    DISPLAY "help_html_doc_input() - l_help_html_form_rec.help_html_filename=",l_help_html_form_rec.help_html_filename
    DISPLAY "help_html_doc_input() - l_help_html_form_rec.help_html_mod_date=",l_help_html_form_rec.help_html_mod_date
    #DISPLAY "help_html_doc_edit() - l_help_html_form_rec.help_html_data=",l_help_html_form_rec.help_html_data
  END IF


  #Copy the form record data to a normal help_html record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_help_html_doc_form_record_to_record(l_help_html_form_rec.*) RETURNING p_help_html_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "help_html_doc_input() - p_help_html_rec.help_html_doc_id=",p_help_html_rec.help_html_doc_id
    DISPLAY "help_html_doc_input() - p_help_html_rec.language_id=",p_help_html_rec.language_id
    DISPLAY "help_html_doc_input() - p_help_html_rec.help_html_filename=",p_help_html_rec.help_html_filename
    DISPLAY "help_html_doc_input() - p_help_html_rec.help_html_mod_date=",p_help_html_rec.help_html_mod_date
    #DISPLAY "help_html_doc_edit() - p_help_html_rec.help_html_data=",p_help_html_rec.help_html_data
  END IF

  RETURN p_help_html_rec.*

END FUNCTION


#################################################
# FUNCTION help_html_doc_delete(p_help_html_doc_id,p_language_id)
#
# Delete a help_html record
#
# RETURN NONE
#################################################
FUNCTION help_html_doc_delete(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id       LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id        LIKE qxt_help_html_doc.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(980), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_help_html_doc 
        WHERE help_html_doc_id = p_help_html_doc_id
          AND qxt_help_html_doc.language_id   = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION help_html_doc_view(p_help_html_doc_id,p_language_id)
#
# View help_html record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION help_html_doc_view(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id       LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id        LIKE qxt_help_html_doc.language_id,
    l_help_html_rec      RECORD LIKE qxt_help_html_doc.*

  LOCATE l_help_html_rec.help_html_data IN MEMORY
 

  CALL get_help_html_doc_rec(p_help_html_doc_id,p_language_id)
    RETURNING l_help_html_rec.*

  CALL help_html_doc_view_by_rec(l_help_html_rec.*)

  FREE l_help_html_rec.help_html_data

END FUNCTION


#################################################
# FUNCTION help_html_doc_view_by_rec(p_help_html_rec)
#
# View help_html record in window-form
#
# RETURN NONE
#################################################
FUNCTION help_html_doc_view_by_rec(p_help_html_rec)
  DEFINE 
    p_help_html_rec             RECORD LIKE qxt_help_html_doc.*,
    l_help_html_form_rec        OF t_qxt_help_html_doc_no_blob_form_rec,
    l_client_file_path    VARCHAR(200),
    tmp_str               VARCHAR(200),
    inp_char                    CHAR


  LET l_help_html_form_rec.help_html_doc_id       = p_help_html_rec.help_html_doc_id
  LET l_help_html_form_rec.language_name      = get_language_name(p_help_html_rec.language_id)
  LET l_help_html_form_rec.help_html_filename = p_help_html_rec.help_html_filename
  LET l_help_html_form_rec.help_html_mod_date = p_help_html_rec.help_html_mod_date

  #Initialise client file path with temp folder & language directory
  LET tmp_str = get_help_html_base_dir_path( get_language_url_path(p_help_html_rec.language_id,p_help_html_rec.help_html_filename) )
  LET l_client_file_path = get_client_temp_path(tmp_str)

  LET l_client_file_path = download_blob_help_html_to_client(p_help_html_rec.help_html_doc_id,p_help_html_rec.language_id,l_client_file_path,FALSE)
  #                FUNCTION download_blob_help_html_to_client(p_help_html_doc_id,p_language_id,p_client_file_path,p_dialog)
  #CALL download_blob_help_html_to_client(l_help_html_form_rec.help_html_doc_id,get_language_id(l_help_html_form_rec.language_name),NULL,TRUE)

{

######################################################
# FUNCTION download_blob_help_html_to_client(p_help_html_doc_id,p_language_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN p_client_file_path
######################################################


  DEFINE t_qxt_help_html_no_blob_form_rec TYPE AS
    RECORD
     help_html_doc_id          LIKE qxt_help_html_doc.help_html_doc_id,
     language_name         LIKE qxt_language.language_name,
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_mod_date    LIKE qxt_help_html_doc.help_html_mod_date
 
    END RECORD
}

	CALL fgl_window_open("w_help_html", 3, 3, get_form_path("f_qxt_help_html_document_view_l2"), FALSE) 
	CALL populate_help_html_doc_form_view_labels_g()

  DISPLAY BY NAME l_help_html_form_rec.*
  DISPLAY l_client_file_path TO url_preview
  DISPLAY l_client_file_path TO url

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_help_html")

END FUNCTION


######################################################
# FUNCTION get_help_html_doc_id_from_filename(p_filename)
#
# Get the help_html_doc_id from a file
#
# RETURN l_help_html_doc_id
######################################################
FUNCTION get_help_html_doc_id_from_filename(p_filename,p_language)
  DEFINE 
    p_filename     LIKE qxt_help_html_doc.help_html_filename,
    p_language     LIKE qxt_help_html_doc.language_id,
    l_help_html_doc_id LIKE qxt_help_html_doc.help_html_doc_id,
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_help_html_doc_id() - help_html_filename = ", p_filename
  END IF

    SELECT qxt_help_html_doc.help_html_doc_id
      INTO l_help_html_doc_id
      FROM qxt_help_html_doc
      WHERE qxt_help_html_doc.help_html_filename = p_filename
        AND qxt_help_html_doc.language_id   = p_language

  IF local_debug THEN
    DISPLAY "get_help_html_doc_id() - l_help_html_doc_id = ", l_help_html_doc_id
  END IF

  RETURN l_help_html_doc_id

END FUNCTION


####################################################
# FUNCTION help_html_doc_filename_count(p_filename,p_language_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION help_html_doc_filename_count(p_filename,p_language_id)
  DEFINE
    p_filename    LIKE qxt_help_html_doc.help_html_filename,
    p_language_id LIKE qxt_help_html_doc.language_id,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_help_html_doc
      WHERE qxt_help_html_doc.help_html_filename = p_filename
        AND qxt_help_html_doc.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION contact_name_count(p_cont_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION help_html_doc_id_and_lang_id_count(p_help_html_doc_id,p_language_id)
  DEFINE
    p_help_html_doc_id    LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id     LIKE qxt_help_html_doc.language_id,
    r_count           SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_help_html_doc
      WHERE qxt_help_html_doc.help_html_doc_id = p_help_html_doc_id
        AND qxt_help_html_doc.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION



######################################################
# FUNCTION upload_blob_help_html_doc(p_contact_id, p_contact_name,remote_image_path)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION upload_blob_help_html_doc(p_client_file,p_dialog)
  DEFINE 
    p_help_html_doc_id      LIKE qxt_help_html_doc.help_html_doc_id,
    l_help_html_file    OF t_qxt_help_html_doc_file,
    local_debug         SMALLINT,
    p_client_file       VARCHAR(250),
    l_server_file       VARCHAR(250),
    p_dialog            SMALLINT,
    p_server_file_blob  BYTE

  LET local_debug = FALSE

  IF p_dialog THEN
    #"Please select the file"
    CALL fgl_file_dialog("open", 0, get_str_tool(65), p_client_file, p_client_file, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file
  END IF

  IF p_client_file IS NOT NULL THEN
    # This will copy the file to the CWD. Hubert can tidy this up later
    LET l_server_file = get_server_blob_temp_path(fgl_basename(p_client_file))

    IF NOT fgl_upload(p_client_file, l_server_file) OR NOT fgl_test("e", l_server_file) THEN
      LET l_server_file = ""
    END IF
  END IF


  IF local_debug THEN
    DISPLAY "upload_blob_help_html_doc() - p_client_file_path=", p_client_file
    DISPLAY "upload_blob_help_html_doc() - l_server_file_path=", l_server_file
  END IF


  RETURN l_server_file

END FUNCTION


######################################################
# FUNCTION get_blob_help_html_doc_file(p_help_html_doc_id,p_language_id)
#
# Get the help_html file
#
# RETURN p_server_file_path
######################################################
FUNCTION get_blob_help_html_doc_file(p_help_html_doc_id,p_language_id)
  DEFINE 
    p_help_html_doc_id       LIKE qxt_help_html_doc.help_html_doc_id,
    p_language_id            LIKE qxt_help_html_doc.language_id,
    p_server_file_path       VARCHAR(250),
    l_help_html_file         OF t_qxt_help_html_doc_file

  LET p_server_file_path = get_server_blob_temp_path(get_help_html_filename(p_help_html_doc_id,p_language_id))

  LOCATE l_help_html_file.help_html_data IN FILE p_server_file_path

  SELECT qxt_help_html_doc.help_html_filename,
         qxt_help_html_doc.help_html_blob
    INTO l_help_html_file.*
    FROM qxt_help_html_doc
    WHERE qxt_help_html_doc.help_html_doc_id = p_help_html_doc_id
      AND qxt_help_html_doc.language_id   = p_language_id

  RETURN p_server_file_path
END FUNCTION


######################################################
# FUNCTION copy_help_html_doc_record_to_form_record(p_help_html_rec)  
#
# Copy normal help_html record data to type help_html_form_rec
#
# RETURN l_help_html_form_rec.*
######################################################
FUNCTION copy_help_html_doc_record_to_form_record(p_help_html_rec)  
  DEFINE
    p_help_html_rec       RECORD LIKE qxt_help_html_doc.*,
    l_help_html_form_rec  OF t_qxt_help_html_doc_form_rec

  LET l_help_html_form_rec.help_html_doc_id = p_help_html_rec.help_html_doc_id
  LET l_help_html_form_rec.language_name = get_language_name(p_help_html_rec.language_id)
  LET l_help_html_form_rec.help_html_filename = p_help_html_rec.help_html_filename
  LET l_help_html_form_rec.help_html_mod_date = p_help_html_rec.help_html_mod_date
  LET l_help_html_form_rec.help_html_data = p_help_html_rec.help_html_data

  RETURN l_help_html_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_help_html_doc_form_record_to_record(p_help_html_form_rec)  
#
# Copy type help_html_form_rec to normal help_html record data
#
# RETURN l_help_html_rec.*
######################################################
FUNCTION copy_help_html_doc_form_record_to_record(p_help_html_form_rec)  
  DEFINE
    l_help_html_rec       RECORD LIKE qxt_help_html_doc.*,
    p_help_html_form_rec  OF t_qxt_help_html_doc_form_rec

  LET l_help_html_rec.help_html_doc_id = p_help_html_form_rec.help_html_doc_id
  LET l_help_html_rec.language_id = get_language_id(p_help_html_form_rec.language_name)
  LET l_help_html_rec.help_html_filename = p_help_html_form_rec.help_html_filename
  LET l_help_html_rec.help_html_mod_date = p_help_html_form_rec.help_html_mod_date
  LET l_help_html_rec.help_html_data = p_help_html_form_rec.help_html_data

  RETURN l_help_html_rec.*

END FUNCTION


###########################################################################
# EOF
###########################################################################



