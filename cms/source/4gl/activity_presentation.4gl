##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



####################################################
# FUNCTION set_act_default_titlebar()
####################################################
FUNCTION set_act_default_titlebar()

  CALL fgl_settitle(get_str(600))  --"CMS - Activity")

END FUNCTION


####################################################
# FUNCTION grid_header_activity_scroll()
#
# Populate the grid header of the activity scroll grid
#
# RETURN NONE
####################################################
FUNCTION grid_header_activity_scroll()
  CALL fgl_grid_header("sa_act_arr","activity_id",get_str(650),"right","F13")         --"ID:
  CALL fgl_grid_header("sa_act_arr","comp_name",get_str(651),"left","F14")            --Comany
  CALL fgl_grid_header("sa_act_arr","cont_name",get_str(652),"left","F15")            --Contact
  CALL fgl_grid_header("sa_act_arr","name",get_str(653),"left","F16")                 --Name
  CALL fgl_grid_header("sa_act_arr","short_desc",get_str(654),"left","F17")           --Short Description
  CALL fgl_grid_header("sa_act_arr","atype_name",get_str(655),"left","F18")           --Act. Type
  CALL fgl_grid_header("sa_act_arr","priority",get_str(656),"right","F19")            --Priority
  CALL fgl_grid_header("sa_act_arr","open_date",get_str(657),"right","F20")           --O-Date
  CALL fgl_grid_header("sa_act_arr","close_date",get_str(658),"right","F21")          --C-Date
END FUNCTION

####################################################
# FUNCTION grid_header_activity_popup()
#
# Populate the grid header of the activity popup grid
#
# RETURN NONE
####################################################
FUNCTION grid_header_activity_popup()
  CALL fgl_grid_header("sa_act_arr","type_id",get_str(650))         --ID
  CALL fgl_grid_header("sa_act_arr","a_type_name",get_str(653))   --NAME
  CALL fgl_grid_header("sa_act_arr","operator_def",get_str(659))     --U:

END FUNCTION




#######################################################
# FUNCTION populate_activity_list_form_labels_t()
#
# Populate mailoutbox form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_activity_list_form_labels_t()
  DISPLAY get_str(601) TO lbTitle
  DISPLAY get_str(611) TO lbInfo122
  DISPLAY get_str(612) TO lbInfo123
  DISPLAY get_str(613) TO lbInfo124
  DISPLAY get_str(614) TO lbInfo12
  DISPLAY get_str(615) TO lbInfo13
  DISPLAY get_str(616) TO lbInfo15
  DISPLAY get_str(617) TO lbInfo14
  DISPLAY get_str(618) TO lbInfo2 
  DISPLAY get_str(619) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_activity_list_form_labels_g()
#
# Populate mailoutbox form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_activity_list_form_labels_g()
  DISPLAY get_str(601) TO lbTitle
  CALL updateUILabel("cntDetail1GroupBox","Activity - Filter")
  CALL UIRadButListItemText("rb_filter",1,"All Activities")
  CALL UIRadButListItemText("rb_filter",2,"Open Activities")
  CALL UIRadButListItemText("rb_filter",3,"Closed Activities")
  #DISPLAY get_str(611) TO lbInfo122
  #DISPLAY get_str(612) TO lbInfo123
  #DISPLAY get_str(613) TO lbInfo124
  #DISPLAY get_str(614) TO lbInfo12
  #DISPLAY get_str(615) TO lbInfo13
  #DISPLAY get_str(616) TO lbInfo15
  #DISPLAY get_str(617) TO lbInfo14
  #DISPLAY get_str(618) TO lbInfo2 
  #DISPLAY get_str(619) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_activity_details_form_labels_t()
#
# Populate activity details form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_activity_details_form_labels_t()
  DISPLAY get_str(670) TO lbTitle
  DISPLAY get_str(671) TO dl_f1
  DISPLAY get_str(672) TO dl_f2
  DISPLAY get_str(673) TO dl_f3
  DISPLAY get_str(674) TO dl_f4
  DISPLAY get_str(675) TO dl_f5
  DISPLAY get_str(676) TO dl_f6
  DISPLAY get_str(677) TO dl_f7
  DISPLAY get_str(678) TO dl_f8
  DISPLAY get_str(679) TO dl_f9
  DISPLAY get_str(680) TO dl_f10
  DISPLAY get_str(681) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_activity_details_form_labels_g()
#
# Populate activity details form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_activity_details_form_labels_g()
  DISPLAY get_str(670) TO lbTitle
  DISPLAY get_str(671) TO dl_f1
  DISPLAY get_str(672) TO dl_f2
  DISPLAY get_str(673) TO dl_f3
  DISPLAY get_str(674) TO dl_f4
  DISPLAY get_str(675) TO dl_f5
  DISPLAY get_str(676) TO dl_f6
  DISPLAY get_str(677) TO dl_f7
  DISPLAY get_str(678) TO dl_f8
  DISPLAY get_str(679) TO dl_f9
  DISPLAY get_str(680) TO dl_f10
  DISPLAY get_str(681) TO lbInfo1

END FUNCTION
