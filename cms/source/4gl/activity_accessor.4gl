##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage activities
##################################################################################################################
#
# FUNCTION:                                     DESCRIPTION:                                         RETURN:
# activity_main(value)                          Main activity menu                                   NONE
# activity_operator_scroll()                    Displays all activities to the                       ret_activity_id
# (p_activity_id,p_order_field,p_accept_action) corresponding contact                             
# activity_operator_scroll_data_source          Data Source (Cursor) for activity_operator_scroll()  NONE
#  (p_filter,p_order_field)                     
# activity_show(p_activity_id)                  show activity details                                NONE
# activity_close(p_activity_id)                 close activity                                       NONE
# activity_delete(p_activity_id)                delete activity                                      NONE
# activity_view(p_activity_id)                  view an activity                                     NONE
# activity_edit((p_activity_id)                 Edit activity (with window+form)                     NONE
# activity_create(p_contact_id)                 create a new activity                                NONE
# populate_activity_form_combo_boxes()          populates all combo boxes dynamically                NONE
# set_act_default_titlebar()                    set the titlebar text to default                     NONE
# get_activity(p_activity_id)                   get activity record from activity_id                 l_activity.* (RECORD LIKE activity.*)
# get_activity_rec(p_activity_id)               return an activity record (by id)                    l_activity_rec.*
# grid_header_activity_scroll()                 Populate the grid header of the activity scroll grid NONE
# grid_header_activity_scroll                   Populate the grid header of the activity popup grid  NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


{
######################################################
# FUNCTION get_activity_rec(p_activity_id)
######################################################
FUNCTION get_activity_rec(p_activity_id)
  DEFINE 
    p_activity_id  LIKE activity.activity_id,
    l_activity_rec OF t_act_rec5

  LOCATE l_activity_rec.long_desc IN MEMORY

  SELECT activity.open_date, 
 	 operator.name,
	 activity.priority, 
	 contact.cont_name,
	 company.comp_name,
	 activity_type.atype_name,
	 activity.short_desc,
	 activity.long_desc
    INTO l_activity_rec.*
    FROM activity, operator, contact, activity_type, OUTER company
    WHERE activity.contact_id = contact.cont_id
      AND company.comp_id = contact.cont_org
      AND activity.operator_id = operator.operator_id
      AND activity_type.type_id = activity.act_type
      AND activity.activity_id = p_activity_id


  RETURN l_activity_rec.*

END FUNCTION

}


######################################################
# FUNCTION get_activity_rec(p_activity_id)
######################################################
FUNCTION get_activity_rec(p_activity_id)
  DEFINE 
    p_activity_id  LIKE activity.activity_id,
    l_activity_rec RECORD LIKE activity.*

  LOCATE l_activity_rec.long_desc IN MEMORY

  SELECT *
    INTO l_activity_rec.*
    FROM activity
    WHERE activity.activity_id = p_activity_id


  RETURN l_activity_rec.*

END FUNCTION



####################################################
# FUNCTION populate_activity_form_combo_boxes()
####################################################
FUNCTION populate_activity_form_combo_boxes()
  IF fgl_fglgui() THEN  --populate the combo boxes dynamically from the database
    CALL activity_type_combo_list("atype_name")
  END IF
END FUNCTION


    

########################################################################
# FUNCTION get_activity(p_activity_id)
#
# get activity record from activity_id
#
# RETURN l_activity.* (RECORD LIKE activity.*)
########################################################################
FUNCTION get_activity(p_activity_id)
  DEFINE p_activity_id LIKE activity.activity_id
  DEFINE l_activity    RECORD LIKE activity.*
  LOCATE l_activity.long_desc IN MEMORY

  SELECT activity.* 
    INTO l_activity.*
    FROM activity
    WHERE activity.activity_id = p_activity_id

  RETURN l_activity.*
END FUNCTION

