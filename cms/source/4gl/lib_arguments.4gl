##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###################################################
# GLOBALS
###################################################
GLOBALS "globals.4gl"

DEFINE md_startWindowFullScreen BOOLEAN  --rendering of the first prog window 1 = windowed 0=fullScreen
DEFINE md_startWindowStyleSet BOOLEAN

DEFINE md_cssState BOOLEAN
DEFINE md_cssId SMALLINT
DEFINE md_themeState BOOLEAN
DEFINE md_demoMode BOOLEAN
DEFINE md_themeId SMALLINT
DEFINE md_styleId SMALLINT
DEFINE md_argmod STRING
DEFINE md_subs CHAR
DEFINE md_password STRING
DEFINE md_user_name STRING
DEFINE md_debug BOOLEAN
DEFINE md_customerdb VARCHAR(18)
DEFINE md_modulegroup VARCHAR(3)
DEFINE md_op_sys CHAR(1)
DEFINE md_module STRING
DEFINE md_clientHostName VARCHAR(20)
DEFINE md_ignoreDivider BOOLEAN  --program argument to ignore divider menu items
DEFINE md_menuId INTEGER
DEFINE md_parent_SESSION_ID STRING

DEFINE md_config_filename  STRING
DEFINE md_currentOperatorId INT
--DEFINE md_lacy INT


FUNCTION processArguments()
  #DEFINE args ARRAY[30] OF STRING
  DEFINE i SMALLINT
  DEFINE l_arg, l_argsub, l_errMsg STRING
  DEFINE argSize INT
  define isChild char

	#Init some variables
	let isChild = fgl_getenv("QX_CHILD")
	LET md_argmod = NULL    
	LET argSize = NUM_ARGS()
	LET md_cssState = FALSE  --theme NOT specified is the default
	LET md_demoMode = FALSE		
		  
	IF NUM_ARGS() > 30 THEN

		LET l_errMsg = "Too many arguments - closing application\n Argument count: ", argSize
		CALL fgl_winmessage("Too many arguments",l_errMsg,"error")
		EXIT PROGRAM
	END IF
    
    FOR i = 1 TO NUM_ARGS()
    	LET l_arg = ARG_VAL(i)
    	LET l_arg = l_arg.ToLowerCase()
    	#CALL fgl_Winmessage("arg=",arg,"info")
    	CASE l_arg
    	
      	WHEN "config"  --configuration file name - default is cms.cfg
      		LET md_config_filename = ARG_VAL(i+1)
      		LET i = i+1
					#call fgl_winmessage("config", md_config_filename,"info")

      	WHEN "operatorid"  --configuration file name - default is cms.cfg
      		LET md_currentOperatorId = ARG_VAL(i+1)
      		LET i = i+1
					#call fgl_winmessage("operatorId", md_currentOperatorId,"info")


      	WHEN "menuid"  --configuration file name - default is cms.cfg
      		LET md_menuId = ARG_VAL(i+1)
      		LET i = i+1

      	WHEN "parentsessionid"  --configuration file name - default is cms.cfg
      		LET md_parent_SESSION_ID = ARG_VAL(i+1)
      		LET i = i+1
    	
      	#WHEN "theme" 
      	#	CALL loadStyle(ARG_VAL(i+1))
      	#	LET md_themeSet = TRUE
       	#	LET i = i+1
       		
      	WHEN "startwindowfullscreen"  --startWindowFullScreen startWindowStyle
      		LET l_argSub = ARG_VAL(i+1)
      		LET i = i+1
      		      		
      		CASE l_argSub  --.ToLowerCase()
      			WHEN "1"
      				LET md_startWindowFullScreen = TRUE --main opening window floating
      			WHEN "0"
      				LET md_startWindowFullScreen = FALSE --main opening window full screen
					END CASE
					
					LET md_startWindowStyleSet = TRUE
					
				WHEN "debugmode"
      		LET l_argSub = ARG_VAL(i+1)
      		LET i = i+1
      		      		
      		CASE l_argSub.ToLowerCase()
      			WHEN "true"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "false"
      				LET md_debug = FALSE --main opening window full screen

      			WHEN "1"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "0"
      				LET md_debug = FALSE --main opening window full screen


      			WHEN "t"
      				LET md_debug = TRUE --main opening window floating
      			WHEN "f"
      				LET md_debug = FALSE --main opening window full screen

					END CASE				

					
				WHEN "module"
      		LET md_module = ARG_VAL(i+1)		
      		LET i = i+1
      		
				WHEN "argmod"
      		LET md_argmod = ARG_VAL(i+1)		
      		LET i = i+1
      							
				WHEN "subs"
					LET md_subs =  ARG_VAL(i+1)	
      		LET i = i+1
      		
				WHEN "modulegroup"
					LET md_modulegroup =  ARG_VAL(i+1)	
      		LET i = i+1
      							
				WHEN "password"
					LET md_password = ARG_VAL(i+1)	
      		LET i = i+1
      		
						
				WHEN "customerdb"
					LET md_customerdb = ARG_VAL(i+1)	
      		LET i = i+1
      		
				WHEN "op_sys"
					LET md_op_sys = ARG_VAL(i+1)	
      		LET i = i+1					

						
				WHEN "user"
					#CALL fgl_winmessage("WHEN gl_catusr ARG_VAL(i)",ARG_VAL(i),"info")	
					#CALL fgl_winmessage("WHEN gl_catusr ARG_VAL(i)",ARG_VAL(i+1),"info")	

					LET md_user_name = ARG_VAL(i+1)	
      		LET i = i+1
					#LET gl_catusr = ARG_VAL(i+1)

				WHEN "clienthostname"
					LET md_clientHostName = ARG_VAL(i+1)	
      		LET i = i+1					

				WHEN "cssid"
					LET md_cssState = TRUE
					LET md_cssId = ARG_VAL(i+1)	
      		LET i = i+1					

				WHEN "themeid"
					LET md_themeState = TRUE
					LET md_themeId = ARG_VAL(i+1)	
      		LET i = i+1					

				WHEN "demomode"
					LET md_demoMode = ARG_VAL(i+1)
				#lacy is simply for lazy people.. one flag triggers a number of options for css, startmenu etc..
				WHEN "lacy"  --let's use lacy (not lazy) otherwise my boss may kill me
					LET md_lacy = ARG_VAL(i+1)	
      		LET i = i+1					


			END CASE       			




# "gl_user_name ", trim(userrec.username), "gl_password ", trim(userrec.password)     
      
    END FOR    
    
	#hack.. can currently not see any sense as it is always 'U'   was an argument for each module call i.e. TAA n5 U
	#IF md_subs = "" THEN
	#	LET md_subs = "A"
	#END IF

	IF getDebugState() = TRUE THEN
		DISPLAY "##########################################################"
		DISPLAY "# In themeByArg()                                        #"
		DISPLAY "##########################################################"	
		DISPLAY "getStartWindowFullScreen()=", trim(getStartWindowFullScreen())
		DISPLAY "getModuleArgument()=", trim(getModuleArgument())
		DISPLAY "getsubs()=", trim(getsubs())
		DISPLAY "getPassword()=", trim(getPassword())
		DISPLAY "getCustomerDb()=", trim(getCustomerDb())
		DISPLAY "getUserName()=", trim(getUserName())
		DISPLAY "getmodulegroup()=", trim(getmodulegroup())		
		DISPLAY "getDebugState()=", trim(getDebugState())		
		DISPLAY "getop_sys()=", trim(getop_sys())	
		DISPLAY "getDemoMode =", trim(getDemoMode())
	
	END IF


	#######################################
	# Default & lacy settings for missing arguments
	LET md_styleId = 1   --0=no css 1=classic  2=facebook style

	CASE md_lacy

		WHEN 100  --classic and NO startmenu at all (SDI too) mainly used for cms (cms-all) as a standalone option
			LET md_config_filename = "cfg/cms.cfg"
			LET md_menuId = 0
			LET md_cssState = FALSE	
			LET md_cssId = 0
			LET md_themeState = FALSE	
			LET md_themeId = 0			
			LET md_styleId = 0   --0=no css 1=classic  2=facebook style

		WHEN 0  --classic and NO startmenu at all (SDI too) mainly used for cms (cms-all) as a standalone option
			LET md_config_filename = "cfg/cms.cfg"
			LET md_menuId = 0
			LET md_cssState = TRUE	
			LET md_cssId = 1
			LET md_themeState = TRUE	
			LET md_themeId = 1			
			LET md_styleId = 0   --0=no css 1=classic  2=facebook style


		WHEN 1  --classic skin - SDI - with StartMenu
			LET md_config_filename = "cfg/cms.cfg"
			LET md_menuId = 1
			LET md_cssState = TRUE	
			LET md_cssId = 1
			LET md_themeState = TRUE	
			LET md_themeId = 2			
			LET md_styleId = 1   --0=no css 1=classic  2=facebook style
			
		WHEN 2  --classic skin - MDI - with StartMenu
			LET md_config_filename = "cfg/cms.cfg"
			LET md_menuId = 2
			LET md_cssState = TRUE	
			LET md_cssId = 1
			LET md_themeState = TRUE	
			LET md_themeId = 2
			LET md_styleId = 1   --0=no css 1=classic  2=facebook style

		WHEN 3  --facebook skin - SDI
			LET md_config_filename = "cfg/cms.cfg"
			LET md_menuId = 3
			LET md_cssState = TRUE	
			LET md_cssId = 3
			LET md_themeState = TRUE	
			LET md_themeId = 3
			LET md_styleId = 2   --0=no css 1=classic  2=facebook style
			
		WHEN 4  --facebook skin - MDI
			if isChild is null then
				display 'MDI main'
				LET md_config_filename = "cfg/cms.cfg"
				LET md_menuId = 4
				LET md_cssState = TRUE	
				LET md_cssId = 4
				LET md_themeState = TRUE	
				LET md_themeId = 4
				LET md_styleId = 2   --0=no css 1=classic  2=facebook style
			else
				display 'MDI child'
				LET md_config_filename = "cfg/cms.cfg"
				LET md_menuId = 0
				LET md_cssState = false	
				LET md_cssId = 0
				LET md_themeState = TRUE	
				LET md_themeId = 4
				LET md_styleId = 0   --0=no css 1=classic  2=facebook style
			end if
	END CASE



	#IF md_themeSet = FALSE THEN
	#	CALL loadStyle(7)  -- 7 is blue css style
	#END IF

	#IF md_startWindowStyleSet = FALSE THEN  --this is not used in cms_2016
	#	LET md_startWindowFullScreen = FALSE  --Default 0=windowed 1=full screen / stretched
	#END IF

			    
END FUNCTION


#flag if css was specified in the argument/url
FUNCTION get_cssState()
	RETURN md_cssState
END FUNCTION

#flag for the css identifier/id (numeric.. will be matched with a case statement)
FUNCTION get_cssId()
	RETURN md_cssId
END FUNCTION 

#flag if theme was specified in the argument/url
FUNCTION get_themeState()
	RETURN md_themeState
END FUNCTION

#flag for the theme identifier/id (numeric.. will be matched with a case statement)
FUNCTION get_themeId()
	RETURN md_themeId
END FUNCTION 

FUNCTION get_styleId()
	RETURN md_styleId
END FUNCTION

FUNCTION setDebugState(state)
	define state BOOLEAN
	let md_debug = state
end function

FUNCTION getDebugState()
	RETURN md_debug
END FUNCTION

FUNCTION getConfigFilename()
	#CALL fgl_winmessage("getConfigFilename()",md_config_filename,"info")
	RETURN md_config_filename
END FUNCTION	

FUNCTION getArgCurrentOperatorId()
	#CALL fgl_winmessage("getArgCurrentOperatorId()",md_currentOperatorId,"info")
	RETURN md_currentOperatorId
END FUNCTION

FUNCTION getPassword()
	RETURN md_password
END FUNCTION

FUNCTION getUserName()
	RETURN md_user_name
END FUNCTION


FUNCTION getMenuId()
	RETURN md_menuId
END FUNCTION

FUNCTION getParentSessionId()
	RETURN md_parent_SESSION_ID
END FUNCTION
	
FUNCTION getsubs()
	DEFINE tempString1 CHAR(1)
	DEFINE tempString2 STRING
	LET tempString2 = md_subs[1]
	LET tempString1 = 	tempString2.toUpperCase()
	#CALL fgl_winmessage("tempString1",tempString1,"info")
	RETURN tempString1 --toUpperCase() --ToLowerCase()
END FUNCTION


FUNCTION getStartWindowFullScreen()
	#DISPLAY sWinStyle
	RETURN md_startWindowFullScreen
END FUNCTION


FUNCTION getModuleArgument()
	#DISPLAY sWinStyle
	IF md_argmod = "" OR md_argmod IS NULL THEN
		RETURN NULL
	ELSE
		RETURN md_argmod
	END IF
END FUNCTION

FUNCTION getModule()
	RETURN md_module
END FUNCTION


FUNCTION getCustomerDb()
	RETURN md_customerdb
END FUNCTION

FUNCTION getModuleGroup()
RETURN md_modulegroup
END FUNCTION


FUNCTION getop_sys()
RETURN md_op_sys
END FUNCTION

FUNCTION getClientHostName()
	RETURN md_clientHostName
END FUNCTION


FUNCTION getDemoMode()
	RETURN md_demoMode
END FUNCTION

### for mdi container
FUNCTION setIgnoreDividerState(state)
	define state BOOLEAN
	let md_ignoreDivider = state
end function

FUNCTION getIgnoreDividerState()
	RETURN md_ignoreDivider
END FUNCTION