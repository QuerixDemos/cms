##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions for contact management
##################################################################################################################
#
# FUNCTION:                                          DESCRIPTION:                                           RETURN
# contact_main(p_contact_id, p_company_id)           Main Contact Menu                                      NONE
# contact_query()                                    Search for contact                                     rv (rv LIKE contact.cont_id)
# query_contact()                                    Search for contact                                     cont_arr[i].cont_id or NULL
# contact_create(p_company_id,p_window)              Create a new contact record                            sqlca.sqlerrd[2]
#                                                    p_window defines if a window should be opened
# contact_input(l_contact.*)                         Crate new contact function                             p_contact.*   (p_contact RECORD LIKE contact.*)
# contact_choose(p_company_id)                       (Not used) ..Select contact in window                  l_contact_id or NULL
# contact_edit(p_contact_id)                         Edit Contact                                           NONE
# contact_delete(p_contact_id)                       Delete Contact                                         rv  (rv SMALLINT Boolean)
# contact_show(p_contact_id)                         Display contact data to form                           NONE
# contact_view(p_contact_id)                         Displays a window with contact details                 NONE
# contact_popup                                      Popup contact selection window                         l_contact[i].cont_id or NULL
# (p_cont_id,p_order_field,p_accept_action)
# contact_to_form_contact(p_contact)                 ??                                                     l_contact_rec.*
# contact_activities_populate_grid_panel_data_source Data Source (cursor) for grid_panel                    NONE
#   ((p_contact_id,p_order_field)
# contact_activities_populate_grid_panel
#   (p_contact_id,p_order_field,p_scroll)            Display activities in screen array                     NONE
# populate_contact_form_combo_boxes()                populates all combo boxes dynamically from dB          NONE
# download_contact_image                             Download a DB BLOB(server side) located image          NONE
#  (p_contact_id, p_contact_name,remote_image_path)  to the client file system
# set_cont_default_titlebar()                        Labels the titlebar with the default label             NONE
# web_get_contact_rec(p_contact_id)                      get contact record from contact_id                     l_contact.*
# get_contact_address_rec(p_contact_id)              Get the contact address record from an id              l_contact.*
#                                                    Note: This function is also published as a webservice
# get_contact_name(p_contact_id)                     returns the contact name based on the contact_id       r_contact_name
# get_contact_lname(p_contact_id)                    Get the contact lname from an id                       r_contact_name
# get_contact_fname(p_contact_id)                    Get the contact fname from an id                       r_contact_name
# get_contact_email_address(p_contact_id)            Get the corresponding email address from a contact_id  r_contact_name
# get_form_contact(p_cont_id)                        ??                                                     l_contact_rec.*
# advanced_contact_lookup()                          Allows to search by entering the first letters         rv_cont_id
# update_advanced_contact_lookup                     sub-routine of advanced_contact_lookup to              rv_cont_id (LIKE contact.cont_id)
# (query_filter,field_filter,do_choose)              update display                     
# print_contact_html(p_company_id)                   Prints company details using a dynamically             NONE
#                                                    created html page.
# grid_header_contact_scroll()                       Populate grid header for scroll                        NONE
# grid_header_open_act_scroll()                      Populate grid header for open activity scroll          NONE
# grid_header_contact_scroll_advanced()              Populate grid header for advanced search               NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


######################################################
# FUNCTION contact_main(p_contact_id, p_company_id)
#
# RETURN NONE
######################################################
FUNCTION contact_main(p_contact_id, p_company_id)
  DEFINE 
    p_contact_id                  LIKE contact.cont_id,
    p_company_id                  LIKE company.comp_id,
    l_contact_id                  LIKE contact.cont_id,
    l_company_id                  LIKE company.comp_id,
    pend_contact_id               LIKE contact.cont_id,
    p_company                     RECORD LIKE company.*,
    tmp_cont_name                 LIKE contact.cont_name,
    p_order_field2, p_order_field VARCHAR(128),
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

	DEFINE fSearch STRING

  LET p_order_field2 = NULL
  LET l_company_id = p_company_id
  LET pend_contact_id = NULL
  CLEAR SCREEN

    OPEN WINDOW w_contact_main
      #AT 1,1
      WITH FORM "form/f_contact_det_l2"
      #ATTRIBUTE(FORM LINE 3)
    CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
    CALL populate_contact_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/business/customer/customer.png")
	CALL ui.Interface.setText("Contact")

############################ WHILE  ############################################
  WHILE (TRUE)
    DECLARE c_cont_scroll SCROLL CURSOR FOR
      SELECT contact.cont_id, contact.cont_name 
        FROM contact
        ORDER BY contact.cont_name ASC
  
    OPEN c_cont_scroll
    FETCH FIRST c_cont_scroll INTO l_contact_id, tmp_cont_name

    IF exist(pend_contact_id) THEN
      WHILE l_contact_id <> pend_contact_id
        FETCH NEXT c_cont_scroll INTO l_contact_id, tmp_cont_name
        IF sqlca.sqlcode THEN
          FETCH FIRST c_cont_scroll INTO l_contact_id, tmp_cont_name
          EXIT WHILE
        END IF
      END WHILE
    END IF

    LET pend_contact_id = NULL

    CALL contact_show(l_contact_id)
    CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)



############################ DIALOG  ############################################
	DIALOG ATTRIBUTES(UNBUFFERED, FIELD ORDER FORM)

        	
	# DIALOG(FIELD ORDER FORM)
 
		INPUT BY NAME fSearch 

			AFTER FIELD fSearch
				
				LET pend_contact_id =  contactSearch(fSearch)
							        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()
        
			ON ACTION actSearchContactGlobal
				
				LET pend_contact_id =  contactSearch(fSearch)
				
        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()
        				
			ON KEY (ACCEPT)
							
				LET pend_contact_id =  contactSearch(fSearch)

        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()
        				
			ON KEY (F9)
							
				LET pend_contact_id =  contactSearch(fSearch)
#@@@
        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()



 			
		ON ACTION actContactEdit     #  COMMAND KEY (F301)  menu_str_arr1[1]  menu_str_arr2[1] HELP 201  --"Edit" "Edit a contact record" Help 201
        CALL set_help_id(301)
        CALL fgl_settitle("CMS - Edit Contact Record")

        IF NOT exist(l_contact_id) THEN
          LET l_contact_id = contact_query() 
        END IF
        CALL contact_edit(l_contact_id,FALSE)
        CALL contact_show(l_contact_id)   

        CALL set_cont_default_titlebar()
        CALL displayKeyGuide("")
 
			ON ACTION actContactNew  #COMMAND KEY (F302)  menu_str_arr1[2]  menu_str_arr2[2] HELP 202  -- "New" "Create a new contact record" Help 202
				CALL set_help_id(302)
        LET pend_contact_id = contact_create(0,FALSE)  --false is don't open window
        IF pend_contact_id IS NOT NULL THEN
          CALL contact_show(pend_contact_id)
        ELSE
          CALL contact_show(l_contact_id)
        END IF

        IF exist(pend_contact_id) THEN
          EXIT DIALOG
        END IF
        CALl contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

			ON ACTION actContactDelete #COMMAND KEY (F303)   menu_str_arr1[3]  menu_str_arr2[3] HELP 203  -- "Delete" "Delete a contact record" Help 203
        CALL set_help_id(303)
        CALL fgl_settitle(get_str(251))

        IF NOT exist(l_contact_id) THEN
          CALL contact_query() 
            RETURNING l_contact_id
        END IF
        IF exist(l_contact_id) THEN
          IF contact_delete(l_contact_id)  THEN
            EXIT DIALOG
          END IF
        END IF
        #clean up
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

			ON ACTION actContactSendEmail      #email_write_by_id  #     COMMAND KEY (F304)   menu_str_arr1[4]  menu_str_arr2[4] HELP 204  --  "Send eMail" "Send the contact an email" Help 204
        CALL set_help_id(304)

         IF NOT exist(l_contact_id) THEN
           LET l_contact_id = contact_query()
           IF exist(l_contact_id) THEN
             CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
           END IF
         END IF

         CALL email_write_by_id(get_current_operator_contact_id(),l_contact_id) 
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()        


			ON ACTION actContactLetterDDE      #Letter by id   COMMAND  KEY(F309) menu_str_arr1[12]  menu_str_arr2[12] HELP 204  #Letter by id
				#NOTE: This option should only be available for the LyciaDesktop client - browsers and mobile devices do NOT support MS-Word and DDE
				IF ui.Interface.getFrontEndName() = "lyciadesktop.html5" THEN
        	CALL set_help_id(304)

        	 IF NOT exist(l_contact_id) THEN
        	   LET l_contact_id = contact_query()
        	   IF exist(l_contact_id) THEN
        	     CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        	   END IF
        	 END IF
        	 CALL letter_write_by_id(get_current_operator_contact_id(),l_contact_id) 
        	 #CALL email_write_by_id(get_current_operator_contact_id(),l_contact_id) 
        	 CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        	#clean up

        	CALL displayKeyGuide("")
        	CALL set_cont_default_titlebar()
        ELSE
        	CALL fgl_winmessage("DDE - Demo Notice","DDE is only supported for LyciaDesktop clients on MS-Windows OS", "info")
				END IF
				
			ON ACTION actNewActivity      #new activity COMMAND KEY (F305)   menu_str_arr1[5]  menu_str_arr2[5] HELP 205  --   "New activity" "Create a new Activity" Help 205
        CALL set_help_id(305)
         CALL activity_create(l_contact_id)
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()
			


			ON ACTION actContactSearchQuick #Quick find     COMMAND KEY(F306) menu_str_arr1[6]  menu_str_arr2[6] HELP 206  --   "Quick Find" HELP 206
        CALL set_help_id(306)
        LET pend_contact_id = advanced_contact_lookup(pend_contact_id)
  
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

        #clean up
        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

			ON ACTION actContactSearchDetailed      #Detailed Search    COMMAND KEY (F307)  menu_str_arr1[7]  menu_str_arr2[7] HELP 207  --   "Find" "Find a contact record" Help 207
        CALL set_help_id(207)
        LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT DIALOG
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

			ON ACTION actContactShowCompany  #      COMMAND KEY(F308)   menu_str_arr1[8]  menu_str_arr2[8] HELP 208  --   "Show Company" "Show the contact's company" HELP 208
        CALL set_help_id(208)
        
        #CALL company_view(get_contact_company_id(l_contact_id))
        CALL company_view_short_by_id(get_contact_company_id(l_contact_id))

        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()
			



			ON KEY(KEY_HOME)
			#ON ACTION actNavigateFirstRecord  #      COMMAND KEY ("CONTROL-F","KEY_HOME",F91)  --First Record
        FETCH FIRST c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

			ON KEY(PREVIOUS)
			#ON ACTION actNavigatePreviousRecord #      COMMAND KEY ("CONTROL-P","PREVPAGE",UP,F93)  --Previous Record
        FETCH PRIOR c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        IF sqlca.sqlcode THEN
          FETCH FIRST c_cont_scroll
            INTO l_contact_id, tmp_cont_name
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

			ON KEY(NEXT)
			#ON ACTION actNavigateNextRecord ATTRIBUTE(ACCELERATOR="CONTROL-N", ACCELERATOR2="NEXTPAGE", ACCELERATOR2="KEY_HOME") #      COMMAND KEY ("CONTROL-N","NEXTPAGE",DOWN,F94)   --Next Record
        FETCH NEXT c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        IF sqlca.sqlcode THEN
          FETCH LAST c_cont_scroll
            INTO l_contact_id, tmp_cont_name
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

			ON KEY(KEY_END)
			#ON ACTION actNavigateLastRecord #      COMMAND KEY ("CONTROL-L","KEY_END",F96)     --Last Record
        FETCH LAST c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

			ON ACTION actActivityGrid #      COMMAND KEY(F11)  menu_str_arr1[9]  menu_str_arr2[9] HELP 209  --  -- Scroll/activate grid
        CALL contact_activities_populate_grid_panel(l_contact_id, "activity.activity_id",TRUE,1)      


     #COMMAND KEY(F11)  menu_str_arr1[9]  menu_str_arr2[9] HELP 209  --  -- Scroll/activate grid
     #   CALL contact_activities_populate_grid_panel(l_contact_id, "activity.activity_id",TRUE,1)      


			ON KEY(F30)
			#ON ACTION actContactPrintHtml  #    COMMAND KEY("CONTROL-P",F30)   menu_str_arr1[10]  menu_str_arr2[10] HELP 209  --   Print HTML
        CALL print_contact_html(l_contact_id)

			ON KEY(F31)
			#ON ACTION actContactGridExport     # COMMAND KEY(F31)
        CALL grid_export("sa_open_act")

			ON KEY(F12)  --duplicated for startmenu AND toolbar
        EXIT WHILE
			
			ON ACTION actExit #      COMMAND KEY (F12,"R")   menu_str_arr1[11]  menu_str_arr2[11] HELP 209  -- "Main Menu" "Close Window and return to main menu/window" Help 51
        EXIT WHILE

#			ON ACTION actLogOut #      COMMAND KEY (F12,"R")   menu_str_arr1[11]  menu_str_arr2[11] HELP 209  -- "Main Menu" "Close Window and return to main menu/window" Help 51
#				CALL disable_current_session_record()
#        EXIT WHILE


      #Column sort short cuts for grid
			ON KEY(F13) --Order screen array by activity_id
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.activity_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())

			ON KEY(F14) --Order screen array by open_date
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity_type.atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())
      
			ON KEY(F15) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.open_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


			ON KEY(F16) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator.name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


			ON KEY(F17) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


			ON KEY(F18) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())



         
		END INPUT
		
		BEFORE DIALOG
			CALL set_help_id(300)
			CALL publish_toolbar("Contact",0)	
		
		ON ACTION actExit
  		EXIT DIALOG

		ON ACTION actLogOut #      COMMAND KEY (F12,"R")   menu_str_arr1[11]  menu_str_arr2[11] HELP 209  -- "Main Menu" "Close Window and return to main menu/window" Help 51
			CALL disable_current_session_record()  
			LET int_flag = TRUE
			EXIT DIALOG

	END DIALOG
 ############################ END DIALOG  ############################################
 
 {
    MENU "Contact"
      BEFORE MENU
        CALL set_help_id(300)
        CALL publish_toolbar("Contact",0)

        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(201)
        LET menu_str_arr2[1] = get_str(202)
        LET menu_str_arr1[2] = get_str(203)
        LET menu_str_arr2[2] = get_str(204)
        LET menu_str_arr1[3] = get_str(205)
        LET menu_str_arr2[3] = get_str(206)
        LET menu_str_arr1[4] = get_str(207)
        LET menu_str_arr2[4] = get_str(208)
        LET menu_str_arr1[5] = get_str(209)
        LET menu_str_arr2[5] = get_str(210)
        LET menu_str_arr1[6] = get_str(211)
        LET menu_str_arr2[6] = get_str(212)
        LET menu_str_arr1[7] = get_str(213)
        LET menu_str_arr2[7] = get_str(214)
        LET menu_str_arr1[8] = get_str(215)
        LET menu_str_arr2[8] = get_str(216)
        LET menu_str_arr1[9] = get_str(217)
        LET menu_str_arr2[9] = get_str(218)
        LET menu_str_arr1[10] = get_str(219)
        LET menu_str_arr2[10] = get_str(220)
        LET menu_str_arr1[11] = get_str(221)
        LET menu_str_arr2[11] = get_str(222)
        LET menu_str_arr1[12] = get_str(223)  --word letter print
        LET menu_str_arr2[12] = get_str(224)  --word letter print
       IF NOT fgl_fglgui() THEN
         HIDE OPTION menu_str_arr1[4]    
         HIDE OPTION menu_str_arr1[10]
         HIDE OPTION menu_str_arr1[12]  --DDE Word Letter

       END IF

        IF fgl_getuitype() = "JAVA" THEN  --Java client does not support DDE
          HIDE OPTION "menu_str_arr1[12]"
        END IF



      COMMAND KEY (F301)  menu_str_arr1[1]  menu_str_arr2[1] HELP 201  --"Edit" "Edit a contact record" Help 201
        HIDE OPTION ALL
        CALL set_help_id(301)
        CALL fgl_settitle("CMS - Edit Contact Record")

        IF NOT exist(l_contact_id) THEN
          LET l_contact_id = contact_query() 
        END IF
        CALL contact_edit(l_contact_id,FALSE)
        CALL contact_show(l_contact_id)   

        #clean up
        SHOW OPTION ALL

        CALL set_cont_default_titlebar()
        CALL displayKeyGuide("")


      COMMAND KEY (F302)  menu_str_arr1[2]  menu_str_arr2[2] HELP 202  -- "New" "Create a new contact record" Help 202
        HIDE OPTION ALL
        CALL set_help_id(302)
        LET pend_contact_id = contact_create(0,FALSE)  --false is don't open window
        IF pend_contact_id IS NOT NULL THEN
          CALL contact_show(pend_contact_id)
        ELSE
          CALL contact_show(l_contact_id)
        END IF

        IF exist(pend_contact_id) THEN
          EXIT MENU
        END IF
        CALl contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        #CALL contact_show(l_contact_id)
        #CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      COMMAND KEY (F303)   menu_str_arr1[3]  menu_str_arr2[3] HELP 203  -- "Delete" "Delete a contact record" Help 203
        HIDE OPTION ALL
        CALL set_help_id(303)
        CALL fgl_settitle(get_str(251))

        IF NOT exist(l_contact_id) THEN
          CALL contact_query() 
            RETURNING l_contact_id
        END IF
        IF exist(l_contact_id) THEN
          IF contact_delete(l_contact_id)  THEN
            EXIT MENU
          END IF
        END IF
        #clean up
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      #email_write_by_id
      COMMAND KEY (F304)   menu_str_arr1[4]  menu_str_arr2[4] HELP 204  --  "Send eMail" "Send the contact an email" Help 204
        HIDE OPTION ALL
        CALL set_help_id(304)

         IF NOT exist(l_contact_id) THEN
           LET l_contact_id = contact_query()
           IF exist(l_contact_id) THEN
             CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
           END IF
         END IF

         CALL email_write_by_id(get_current_operator_contact_id(),l_contact_id) 
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        SHOW OPTION ALL
        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      #Letter by id
      COMMAND  KEY(F309) menu_str_arr1[12]  menu_str_arr2[12] HELP 204  #Letter by id
        HIDE OPTION ALL
        CALL set_help_id(304)

         IF NOT exist(l_contact_id) THEN
           LET l_contact_id = contact_query()
           IF exist(l_contact_id) THEN
             CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
           END IF
         END IF
         CALL letter_write_by_id(get_current_operator_contact_id(),l_contact_id) 
         #CALL email_write_by_id(get_current_operator_contact_id(),l_contact_id) 
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      #new activity
      COMMAND KEY (F305)   menu_str_arr1[5]  menu_str_arr2[5] HELP 205  --   "New activity" "Create a new Activity" Help 205
         HIDE OPTION ALL 
        CALL set_help_id(305)
         CALL activity_create(l_contact_id)
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()


      #Quick find
      COMMAND KEY(F306) menu_str_arr1[6]  menu_str_arr2[6] HELP 206  --   "Quick Find" HELP 206
        HIDE OPTION ALL
        CALL set_help_id(306)
        LET pend_contact_id = advanced_contact_lookup(pend_contact_id)
  
        IF exist(pend_contact_id) THEN 
          EXIT MENU
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

        #clean up
        SHOW OPTION ALL
        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      #Detailed Search
      COMMAND KEY (F307)  menu_str_arr1[7]  menu_str_arr2[7] HELP 207  --   "Find" "Find a contact record" Help 207
        HIDE OPTION ALL
        CALL set_help_id(207)
        LET pend_contact_id = query_contact()
        IF exist(pend_contact_id) THEN 
          EXIT MENU
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      COMMAND KEY(F308)   menu_str_arr1[8]  menu_str_arr2[8] HELP 208  --   "Show Company" "Show the contact's company" HELP 208
        HIDE OPTION ALL
        CALL set_help_id(208)
        
        #CALL company_view(get_contact_company_id(l_contact_id))
        CALL company_view_short_by_id(get_contact_company_id(l_contact_id))

        #clean up
        SHOW OPTION ALL

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

      COMMAND KEY ("CONTROL-F","KEY_HOME",F91)  --First Record
        FETCH FIRST c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

      COMMAND KEY ("CONTROL-P","PREVPAGE",UP,F93)  --Previous Record
        FETCH PRIOR c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        IF sqlca.sqlcode THEN
          FETCH FIRST c_cont_scroll
            INTO l_contact_id, tmp_cont_name
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

      COMMAND KEY ("CONTROL-N","NEXTPAGE",DOWN,F94)   --Next Record
        FETCH NEXT c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        IF sqlca.sqlcode THEN
          FETCH LAST c_cont_scroll
            INTO l_contact_id, tmp_cont_name
        END IF
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

      COMMAND KEY ("CONTROL-L","KEY_END",F96)     --Last Record
        FETCH LAST c_cont_scroll
          INTO l_contact_id, tmp_cont_name
        CALL contact_show(l_contact_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)

      COMMAND KEY(F11)  menu_str_arr1[9]  menu_str_arr2[9] HELP 209  --  -- Scroll/activate grid
        CALL contact_activities_populate_grid_panel(l_contact_id, "activity.activity_id",TRUE,1)      


      COMMAND KEY("CONTROL-P",F30)   menu_str_arr1[10]  menu_str_arr2[10] HELP 209  --   Print HTML
        CALL print_contact_html(l_contact_id)

      COMMAND KEY(F31)
        CALL grid_export("sa_open_act")

      COMMAND KEY (F12,"R")   menu_str_arr1[11]  menu_str_arr2[11] HELP 209  -- "Main Menu" "Close Window and return to main menu/window" Help 51
        EXIT WHILE


      #Column sort short cuts for grid
      COMMAND KEY(F13) --Order screen array by activity_id
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.activity_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())

      COMMAND KEY(F14) --Order screen array by open_date
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity_type.atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())
      
      COMMAND KEY(F15) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.open_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      COMMAND KEY(F16) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator.name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      COMMAND KEY(F17) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      COMMAND KEY(F18) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


    END MENU

}

    #Clean up toolbar
    CALL publish_toolbar("Contact",1)

    CLOSE c_cont_scroll

    IF int_flag THEN
      LET int_flag = FALSE
      EXIT WHILE
    END IF
  END WHILE
############################ END WHILE  ############################################


  CLOSE WINDOW w_contact_main

  CLEAR SCREEN
END FUNCTION








#############################################################################################################
# Contact BLOB upload-download
#############################################################################################################


######################################################
# FUNCTION upload_contact_image()
#
# Upload a client side located image to the db
#
# RETURN local_file_name
######################################################
FUNCTION upload_contact_image()
  DEFINE client_image_path VARCHAR(2000)
  DEFINE server_file_name VARCHAR(200)

  #"Please select the file"
  CALL fgl_file_dialog("open", 0, get_str(273), "", "", "Image (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
    RETURNING client_image_path

  IF client_image_path IS NOT NULL THEN
    # This will copy the file to the CWD. Hubert can tidy this up later
    LET server_file_name = get_server_blob_temp_path(fgl_basename(client_image_path))
    IF NOT fgl_upload(client_image_path, server_file_name) THEN
      LET server_file_name = ""
    END IF
    
    IF NOT fgl_test("e", server_file_name) THEN
      LET server_file_name = ""
    END IF
  END IF

  RETURN server_file_name
END FUNCTION


######################################################
# FUNCTION download_contact_image(p_contact_id, p_image_name,p_file_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION download_contact_image(p_contact_id, p_image_name,p_file_dialog)
  DEFINE p_contact_id      LIKE contact.cont_id
  DEFINE p_image_name      STRING
  DEFINE server_file_name  STRING
  DEFINE client_image_path STRING
  DEFINE default_file_name STRING
  DEFINE 
    temp_blob               BYTE,
    p_file_dialog           SMALLINT,  --TRUE = with file dialog
    local_debug             SMALLINT

    #target_location VARCHAR(200)

  LET local_debug = FALSE
  LET p_image_name = trim(p_image_name)
  IF local_debug THEN
    DISPLAY "download_contact_image() - p_contact_id=", p_contact_id
    DISPLAY "download_contact_image() - p_image_name=", p_image_name
    DISPLAY "download_contact_image() - p_file_dialog=", p_file_dialog
  ENd IF

  LET default_file_name = p_image_name  -- CLIPPED, ".jpg"
  LET server_file_name = get_server_blob_temp_path(fgl_basename(default_file_name))

  IF local_debug THEN
    display "download_contact_image() - default_file_name=", default_file_name
    display "download_contact_image() - get_server_blob_temp_path(default_file_name)=", get_server_blob_temp_path(default_file_name)
    display "download_contact_image() - server_file_name=", server_file_name
  END IF

  #If argument has NULL, use file dialog to choose target file name
  IF p_file_dialog THEN
    CALL fgl_file_dialog("save", 0, get_str(273), "", p_image_name, "Image (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING client_image_path   
  ELSE
    LET client_image_path = get_image_path(get_print_html_image_filename(2))
  END IF


  IF local_debug THEN
    display "download_contact_image() - client_image_path=", client_image_path
    display "download_contact_image() - server_file_name=", server_file_name
  END IF

  IF client_image_path IS NOT NULL THEN
    LOCATE temp_blob IN FILE server_file_name

    SELECT contact.cont_picture
      INTO temp_blob
      FROM contact
      WHERE contact.cont_id = p_contact_id

    LET client_image_path = fgl_download(server_file_name, client_image_path)

    FREE temp_blob
    RETURN client_image_path
  ELSE
    RETURN NULL
  END IF
END FUNCTION







#############################################################################################################
# Edit, Create, Delete, Input,...
#############################################################################################################


######################################################
# FUNCTION contact_input(p_contact,edit_type)
#
# The input handler for new & edit contact record
#
# RETURN p_contact.*
######################################################
FUNCTION contact_input(p_contact)
  DEFINE
    p_contact 	                  RECORD LIKE contact.*,
    l_dept_id                     LIKE contact_dept.dept_id,
    l_pos_id                      LIKE position_type.type_id,
    local_file_name               VARCHAR(200),
    l_contact_rec                 OF t_cont_rec,
    l_contact_id                  LIKE contact.cont_id,
    p_order_field2, p_order_field VARCHAR(128)

DEFINE regexMail,regexPhone util.REGEX
DEFINE match util.MATCH_RESULTS
  #copy data from the full record to the form/input record
  CALL contact_to_form_contact(p_contact.*)
    RETURNING l_contact_rec.*

  let l_contact_id = p_contact.cont_id
  CLEAR cont_picture

  INPUT BY NAME l_contact_rec.* WITHOUT DEFAULTS HELP 251
    BEFORE INPUT
			IF p_contact.cont_id = 0 THEN -- new recor	
	      CALL publish_toolbar("ContactNew",0)
			ELSE  -- edit existing record
    	  CALL publish_toolbar("ContactEdit",0)
			END IF

      CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB

      AFTER FIELD cont_name

      IF contact_name_count(l_contact_rec.cont_name) THEN
        IF (p_contact.cont_name = l_contact_rec.cont_name) THEN
          #do nothing for now
        ELSE
          DISPLAY l_contact_rec.cont_name TO cont_name ATTRIBUTE(RED)
          #You must specify an unique contact name. "
          ERROR get_str(274), l_contact_rec.cont_name CLIPPED, get_str(275)
          NEXT FIELD comp_name
        END IF
      ELSE
        DISPLAY l_contact_rec.cont_name TO cont_name 
      END IF


{
      IF contact_name_count(l_contact_rec.cont_name) THEN
        IF (p_contact.cont_name = l_contact_rec.cont_name) THEN
          #do nothing for now
        ELSE
          DISPLAY l_contact_rec.cont_name TO cont_name ATTRIBUTE(RED)
          ERROR "You must specify an unique contact name. ", l_contact_rec.cont_name CLIPPED, " already exists!"
          NEXT FIELD comp_name
        END IF
      ELSE
        DISPLAY l_contact_rec.cont_name TO cont_name 
      END IF
}

    ON KEY(F9)
      IF INFIELD (cont_country) THEN
        LET l_contact_rec.cont_country = country_advanced_lookup(l_contact_rec.cont_country)
        CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
        DISPLAY BY NAME l_contact_rec.cont_country
      END IF



    ON KEY(F10)  --OK


      IF INFIELD (cont_title) THEN
        LET l_contact_rec.cont_title = title_popup(l_contact_rec.cont_title,NULL,0)
#        CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
        DISPLAY BY NAME l_contact_rec.cont_title
      END IF

      IF INFIELD (cont_country) THEN
        LET l_contact_rec.cont_country = country_popup(l_contact_rec.cont_country,NULL,0)
#        CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
        DISPLAY BY NAME l_contact_rec.cont_country
      END IF

      IF INFIELD (cont_position) THEN
        LET l_pos_id = position_type_popup(l_pos_id,NULL,0)
        IF exist(l_pos_id) THEN
          LET l_contact_rec.cont_position = get_position_type_name(l_pos_id)
        END IF
#        CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
        DISPLAY BY NAME l_contact_rec.cont_position
      END IF

      IF INFIELD (cont_dept) THEN
        LET l_dept_id = contact_dept_popup(l_dept_id,NULL,0)
        IF exist(l_dept_id) THEN
          LET l_contact_rec.cont_dept = get_contact_dept_name(l_dept_id)
        END IF
#        CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
        DISPLAY BY NAME l_contact_rec.cont_dept
      END IF


      #Company
      IF INFIELD (comp_name) THEN

        LET p_contact.cont_org = company_popup(p_contact.cont_org,NULL, 0)
        LET l_contact_rec.comp_name = get_company_name(p_contact.cont_org)
        CURRENT WINDOW IS w_contact_main
        DISPLAY BY NAME l_contact_rec.comp_name
      END IF

      #Always re-populate the combo boxes in case, the user added a new entry
      CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB



      ON KEY(F321)  --upload image
        CALL upload_contact_image()
          RETURNING local_file_name


        IF local_file_name IS NOT NULL THEN
          # free the existing locator
          IF l_contact_rec.cont_picture IS NOT NULL THEN
            FREE l_contact_rec.cont_picture
          END IF

          LOCATE l_contact_rec.cont_picture IN FILE local_file_name
          DISPLAY BY NAME l_contact_rec.cont_picture
        END IF



      ON KEY(F322)  --download image
        CALL download_contact_image(p_contact.cont_id, p_contact.cont_name,TRUE)  --TRUE will display the file dialog box for target name & path



      ON KEY (F304) --"Send eMail" "Send the contact an email" Help 204
         CALL email_write_by_id(get_current_operator_contact_id(),p_contact.cont_id) 

         #CALL email_write_by_id(l_contact_id, "", "", 0, get_current_operator_id())   ---sets body and subject to empty strings
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)


      ON KEY (F305) --"New activity" "Create a new Activity" Help 205

         CALL activity_create(l_contact_id)
         CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)


      ON KEY(F11) -- Scroll/activate grid
        CALL contact_activities_populate_grid_panel(l_contact_id, "activity.activity_id",TRUE,2)      



      #Column sort short cuts for grid
      ON KEY(F13) --Order screen array by activity_id
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.activity_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())

      ON KEY(F14) --Order screen array by open_date
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity_type.atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())
      
      ON KEY(F15) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.open_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      ON KEY(F16) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator.name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      ON KEY(F17) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      ON KEY(F18) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL contact_activities_populate_grid_panel(l_contact_id, p_order_field,NULL,get_toggle_switch())


      ON KEY(F30)
        IF p_contact.cont_id THEN
          CALL print_contact_html(l_contact_id)
        ELSE
          #CALL fgl_winmessage("Cannot print an unsaved record","Save your record before you print it","error")

          CALL fgl_winmessage(get_str(276),get_str(277),"error")
        END IF


    AFTER INPUT
      #operator wants to store data - check if required fields are filled with data
      IF NOT int_flag THEN
        IF l_contact_rec.cont_name IS NULL THEN
          #You must specify an unique value for Contact: (Name)
          CALL fgl_winmessage(get_str(278),get_str(279),"error")
          DISPLAY "<Required>" TO cont_name ATTRIBUTE (RED,BOLD)
          SLEEP 2
          DISPLAY "" TO cont_name
          NEXT FIELD cont_name
          CONTINUE INPUT
        END IF

        IF l_contact_rec.cont_email IS NULL THEN
          #You must specify an unique value for E-Mail: (Name)
          CALL fgl_winmessage(get_str(278),"You must specify an unique value for Email:","error")
          DISPLAY "<Required>" TO cont_email ATTRIBUTE (RED,BOLD)
          SLEEP 2
          DISPLAY "" TO cont_email
          NEXT FIELD cont_email
          CONTINUE INPUT
        END IF

#		LET regexMail = ~/^[-a-z0-_]+(\.[-a-z0-9_]+)@([a-z0-9]([-a-z0-9]{0,61}[a-z0-9])\.)(aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/
		LET regexMail = /.+@.+.+/
#		DISPLAY "|",l_contact_rec.cont_email CLIPPED,"|"
		IF NOT util.REGEX.match(l_contact_rec.cont_email CLIPPED, regexMail) THEN --check if email is valid some_text@some_text 
		  CALL fgl_winmessage(get_str(278),"Email has wrong format","error")
          NEXT FIELD cont_email
          CONTINUE INPUT
		END IF


        LET regexPhone = /\(?\+[0-9]{1,3}\)? ?-?[0-9]{1,3} ?-?[0-9]{3,5} ?-?[0-9]{4}( ?-?[0-9]{3})? ?(\w{1,10}\s?\d{1,6})?/
        IF NOT util.REGEX.match(trim(l_contact_rec.cont_phone), regexPhone) THEN
          CALL fgl_winmessage("Contact phone","Phone number should have format +xx xxx xxxxxxx","error")
          NEXT FIELD cont_phone
          CONTINUE INPUT
        END IF
        IF NOT util.REGEX.match(trim(l_contact_rec.cont_mobile), regexPhone) THEN
          CALL fgl_winmessage("Contact phone","Cell number should have format +xx xxx xxxxxxx","error")
          NEXT FIELD cont_mobile
          CONTINUE INPUT
        END IF
        IF NOT util.REGEX.match(trim(l_contact_rec.cont_fax), regexPhone) THEN
          CALL fgl_winmessage("Contact phone","Fax number should have format +xx xxx xxxxxxx","error")
          NEXT FIELD cont_fax
          CONTINUE INPUT
        END IF
      ELSE
        #Check for Interrupt key
        #IF NOT yes_no("Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?") THEN
        IF NOT yes_no(get_str(280),get_str(281)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
        END IF

      END IF
  END INPUT

 --possible toolbar clean up
  CALL publish_toolbar("ContactEdit",1)

  LET p_contact.cont_name = l_contact_rec.cont_name
  LET p_contact.cont_fname = l_contact_rec.cont_fname
  LET p_contact.cont_lname = l_contact_rec.cont_lname
  LET p_contact.cont_title = l_contact_rec.cont_title
  LET p_contact.cont_addr1 = l_contact_rec.cont_addr1
  LET p_contact.cont_addr2 = l_contact_rec.cont_addr2
  LET p_contact.cont_addr3 = l_contact_rec.cont_addr3
  LET p_contact.cont_city = l_contact_rec.cont_city
  LET p_contact.cont_zone = l_contact_rec.cont_zone
  LET p_contact.cont_zip = l_contact_rec.cont_zip
  LET p_contact.cont_country = l_contact_rec.cont_country
  LET p_contact.cont_phone = l_contact_rec.cont_phone
  LET p_contact.cont_mobile = l_contact_rec.cont_mobile
  LET p_contact.cont_fax = l_contact_rec.cont_fax
  LET p_contact.cont_email = l_contact_rec.cont_email
  LET p_contact.cont_picture = l_contact_rec.cont_picture
  LET p_contact.cont_position = l_contact_rec.cont_position
  LET p_contact.cont_usemail = l_contact_rec.cont_usemail
  LET p_contact.cont_usephone = l_contact_rec.cont_usephone
  LET p_contact.cont_dept = l_contact_rec.cont_dept
  LET p_contact.cont_notes = l_contact_rec.cont_notes  

  RETURN p_contact.*
END FUNCTION



######################################################
# FUNCTION contact_create(p_company_id,p_window)
#
# Create a new contact record - p_window defines if a window should be opened
#
# RETURN sqlca.sqlerrd[2]
######################################################
FUNCTION contact_create(p_company_id,p_window)
  DEFINE 
    p_company_id          LIKE company.comp_id,
    l_contact, l_contact2 RECORD LIKE contact.*,
    l_company             RECORD LIKE company.*,
    inherite              VARCHAR(10),
    local_debug           SMALLINT,
    p_window      SMALLINT                  --specifies, if a window with form should be opened


  LOCATE l_contact.cont_picture IN MEMORY

  LET local_debug = 0
  LET l_contact.cont_id = 0
  LET l_contact.cont_org = p_company_id



  IF p_window THEN
      CALL fgl_window_open("w_contact_main2",1,1, get_form_path("f_contact_det_l2"), FALSE)
      CALL populate_contact_edit_form_labels_g()
      #CALL grid_header_open_act_scroll()
      CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB
      #DISPLAY "!" TO cont_usemail
      #DISPLAY "!" TO cont_usephone
    

  ELSE

      CALL populate_contact_edit_form_labels_g()
      #DISPLAY "!" TO cont_usemail
      #DISPLAY "!" TO cont_usephone
   

  END IF



  CALL contact_activities_clear_grid_panel()

  #When creating a new record, part of the data can be retrieved from 
  #the corresponding company record or another contact record
  LET inherite = fgl_winbutton(get_str(264),  get_str(265),"No","Company|Contact|No","question","")   --"New Contact - Inherite Details ?","Do you want to inherite Details from a Company or Contact","No","Company|Contact|No","question","")

  IF local_debug = 1 THEN
    DISPLAY "contact_create() - p_company_id=", p_company_id
    DISPLAY "contact_create() - inherite=", inherite
  END IF

  CASE inherite 
    WHEN "Company"
      LET p_company_id = advanced_company_lookup(p_company_id) --get company_id from selection list
      CALL get_company_rec(p_company_id) RETURNING l_company.*     --get company record from company_id

      LET l_contact.cont_addr1   = l_company.comp_addr1          --copy 'some' company details to contact record
      LET l_contact.cont_addr2   = l_company.comp_addr2
      LET l_contact.cont_addr3   = l_company.comp_addr3
      LET l_contact.cont_city    = l_company.comp_city
      LET l_contact.cont_zone    = l_company.comp_zone
      LET l_contact.cont_zip     = l_company.comp_zip
      LET l_contact.cont_country = l_company.comp_country
      LET l_contact.cont_org     = l_company.comp_id


    WHEN "Contact"
      LET l_contact2.cont_id = advanced_contact_lookup(l_contact2.cont_id)             --get company_id from selection list
      CALL web_get_contact_rec(l_contact2.cont_id) RETURNING l_contact2.*     --get company record from company_id

      LET l_contact.cont_addr1   = l_contact2.cont_addr1          --copy 'some' company details to contact record
      LET l_contact.cont_addr2   = l_contact2.cont_addr2
      LET l_contact.cont_addr3   = l_contact2.cont_addr3
      LET l_contact.cont_city    = l_contact2.cont_city
      LET l_contact.cont_zone    = l_contact2.cont_zone
      LET l_contact.cont_zip     = l_contact2.cont_zip
      LET l_contact.cont_country = l_contact2.cont_country
      LET l_contact.cont_org     = l_contact2.cont_org


      LET l_contact.cont_phone = l_contact2.cont_phone
      LET l_contact.cont_fax = l_contact2.cont_fax
      LET l_contact.cont_mobile = l_contact2.cont_mobile


    WHEN "No"
      #do nothing - nothing to inherite

    OTHERWISE
      # CALL fgl_winmessage(get_str(50)"Internal 4GL Demo source code Error","Invalid case value!\nFunction contact_create(p_company_id="|| p_company_id || ") inherite= " || inherite,"error")
      LET err_msg = get_str(52) CLIPPED, "\nFunction contact_create(p_company_id=", p_company_id , ") inherite= " , inherite
      CALL fgl_winmessage(get_str(50),err_msg,"error")
  END CASE 

  CALL contact_input(l_contact.*)  --create new
    RETURNING l_contact.*

  IF p_window THEN
    CALL fgl_window_close("w_contact_main2")
  END IF

  IF int_flag THEN  --user aborts input
    LET int_flag = FALSE
    CALL fgl_winmessage(get_str(268),get_str(269),"info")
    RETURN NULL
  END IF

  INSERT INTO contact VALUES (l_contact.*)

  IF sqlca.sqlcode THEN
    #CALL fgl_winmessage("Contact Create","Contact creation failed!","stop")
    CALL fgl_winmessage(get_str(268),get_str(270),"stop")
  ELSE  
    #CALL fgl_winmessage("Contact Create","New contact record successfully created!","info")
    CALL fgl_winmessage(get_str(268),get_str(271),"info")
  END IF



  FREE l_contact.cont_picture

  RETURN sqlca.sqlerrd[2]
END FUNCTION



######################################################
# FUNCTION contact_edit(p_contact_id,p_window)
# 
# Edit an existing contact record (p_window specifies, if a window with form should be opened)
#
# RETURN NONE
######################################################
FUNCTION contact_edit(p_contact_id,p_window)
  DEFINE 
    p_contact_id  LIKE contact.cont_id,
    l_contact     RECORD LIKE contact.*,
    p_window      SMALLINT                  --specifies, if a window with form should be opened

  IF p_window IS NULL THEN  --apply default
    LET p_window = TRUE
  END IF
  
  CALL web_get_contact_rec(p_contact_id)
    RETURNING l_contact.*

  IF p_window THEN


      CALL fgl_window_open("w_contact_main2",1,1, get_form_path("f_contact_det_l2"), FALSE)
      CALL populate_contact_edit_form_labels_g()
      CALL grid_header_open_act_scroll()
      CALL populate_contact_form_combo_boxes()  --populate the combo boxes dynamically from DB


  ELSE

      CALL populate_contact_edit_form_labels_g()
   

  END IF

  CALL contact_input(l_contact.*)  --edit
    RETURNING l_contact.*

  IF int_flag THEN
    LET int_flag = FALSE
  ELSE
    UPDATE contact SET
      cont_name = l_contact.cont_name,
      cont_title = l_contact.cont_title,
      cont_fname = l_contact.cont_fname,
      cont_lname = l_contact.cont_lname,
      cont_addr1 = l_contact.cont_addr1,
      cont_addr2 = l_contact.cont_addr2,
      cont_addr3 = l_contact.cont_addr3,
      cont_city = l_contact.cont_city,
      cont_zone = l_contact.cont_zone,
      cont_zip = l_contact.cont_zip,
      cont_country = l_contact.cont_country,
      cont_phone = l_contact.cont_phone,
      cont_mobile = l_contact.cont_mobile,
      cont_fax = l_contact.cont_fax,
      cont_email = l_contact.cont_email,
      cont_dept = l_contact.cont_dept,
      cont_position = l_contact.cont_position,
      cont_org = l_contact.cont_org,
      cont_usemail = l_contact.cont_usemail,
      cont_usephone = l_contact.cont_usephone,
      cont_notes = l_contact.cont_notes,
      cont_picture = l_contact.cont_picture
    WHERE contact.cont_id = p_contact_id
  END IF

  IF p_window THEN
    CALL fgl_window_close("w_contact_main2")

  ELSE
    IF fgl_fglgui() THEN --gui
      DISPLAY "*" TO bt_upload
      DISPLAY "*" TO bt_download
    END IF
  END IF


  # FREE l_contact.cont_picture
END FUNCTION



######################################################
# FUNCTION contact_delete(p_contact_id)
#
# Delete contact record
#
# RETURN rv   (rv SMALLINT)
######################################################
FUNCTION contact_delete(p_contact_id)
  DEFINE 
    p_contact_id LIKE contact.cont_id,
    rv           SMALLINT

  ---only delete operator once confirmed, to reduce risk of accidental delete
  #"Are you sure you want to delete this contact?
  IF yes_no(get_str(282),get_str(283)) THEN
    DELETE FROM contact 
      WHERE cont_id = p_contact_id

    IF sqlca.sqlcode THEN
      CALL fgl_winmessage(get_str(282), get_str(284),"stop")
    ELSE  --"Contact delete completed successfully"
      CALL fgl_winmessage(get_str(282), get_str(285),"info")
    END IF

    LET rv = TRUE
  ELSE --Contact delete cancelled at operator request
    CALL fgl_winmessage(get_str(282), get_str(286),"info")
  END IF

  RETURN rv
END FUNCTION





#########################################################################################################
#
# Contact view functions
#
#########################################################################################################

######################################################
# FUNCTION contact_show(p_contact_id)
#
# RETURN NONE
######################################################
FUNCTION contact_show(p_contact_id)
  DEFINE p_contact_id LIKE contact.cont_id
  DEFINE l_contact_rec OF t_cont_rec

  LOCATE l_contact_rec.cont_picture IN MEMORY


  CALL get_form_contact(p_contact_id)
    RETURNING l_contact_rec.*

	CALL updateContactGoogleMap(l_contact_rec.*)

  DISPLAY BY NAME l_contact_rec.*

  FREE l_contact_rec.cont_picture
END FUNCTION


FUNCTION updateContactGoogleMap(l_contact_rec)
	DEFINE l_contact_rec OF t_cont_rec
	DEFINE mapQuery STRING
	 
	LET mapQuery = trim(l_contact_rec.cont_addr1), " , ", trim(l_contact_rec.cont_addr2), " , ",trim(l_contact_rec.cont_addr3), " , ",trim(l_contact_rec.cont_city), " , ", trim(l_contact_rec.cont_zone), " , ", trim(l_contact_rec.cont_zip), " , ",trim(l_contact_rec.cont_country)
	#CALL fgl_winmessage("map",mapQuery,"info")
	DISPLAY mapQuery TO wc_google_map
END FUNCTION

######################################################
# FUNCTION contact_view(p_contact_id)
#
# Displays a window with contact details
#
# RETURN NONE
######################################################
# A quick pop-up window for the given contact details.
FUNCTION contact_view(p_contact_id)
  DEFINE 
    p_contact_id LIKE contact.cont_id,
    inp_char CHAR


    CALL fgl_window_open("w_contact_display" , 2,2 , get_form_path("f_contact_det_l2"), FALSE)

    CALL grid_header_contact_scroll()
    CALL populate_contact_view_form_labels_g()


  CALL contact_show(p_contact_id)

  WHILE TRUE

    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        EXIT WHILE
    END PROMPT

  END WHILE

  #MESSAGE get_str(51)  --"Press any key to continue"

  #CALL fgl_getkey()
  CALL fgl_window_close("w_contact_display")
END FUNCTION



###################################################
# FUNCTION contact_view_short_by_id(p_contact_id)
#
# Display small/short set of company details to form (fields by name)
#
# RETURN NOTHING
###################################################
FUNCTION contact_view_short_by_id(p_contact_id)
  DEFINE 
    p_contact_id        LIKE contact.cont_id,
    l_contact_short_rec OF t_contact_short_rec,
    inp_char            CHAR

  #only proceess if id is not null
  IF p_contact_id IS NULL THEN
    RETURN
  END IF

    CALL fgl_window_open("w_comp_short",2,8,get_form_path("f_contact_view_short_l2"),FALSE)
    CALL populate_contact_short_form_labels_g()
    CALL fgl_settitle(get_str(101))

  #CALL populate_contact_short_form_labels_g()

  CALL get_contact_short_rec(p_contact_id)
    RETURNING l_contact_short_rec.*

  DISPLAY BY NAME l_contact_short_rec.*


  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_comp_short")

END FUNCTION







###########################################################################################################
#
# Contact List & Combo functions
#
###########################################################################################################



######################################################
# FUNCTION contact_popup_data_source(p_order_field)
#
# Data Source (Cursor) for contact_popup(p_cont_id,p_order_field,p_accept_action)
#
# RETURN NONE
#
######################################################
FUNCTION contact_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field      VARCHAR(30),
    sql_stmt           CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = 0


  IF p_order_field IS NULL  THEN
    LET p_order_field = "cont_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT contact.cont_id, ",
                   "contact.cont_name, ",
                   "contact.cont_fname, ",
                   "contact.cont_lname, ",
                   "company.comp_name, ",
                   "contact.cont_phone ",
                 "FROM contact, OUTER company ",
                 "WHERE company.comp_id = contact.cont_org "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "contact_popup_data_source() SQL:"
    DISPLAY sql_stmt[1,50] 
    DISPLAY sql_stmt[51,100] 
    DISPLAY sql_stmt[101,150] 
    DISPLAY sql_stmt[151,200] 
  END IF

  PREPARE p_cont FROM sql_stmt
  DECLARE c_cont CURSOR FOR p_cont

END FUNCTION


######################################################
# FUNCTION contact_popup(p_cont_id,p_order_field,p_accept_action)
#
# Display array with all contacts to select
#
# RETURN l_contact[i].cont_id or p_cont_id  (in case of cancel)
######################################################
FUNCTION contact_popup(p_cont_id,p_order_field,p_accept_action)
  DEFINE 
    p_cont_id                      LIKE contact.cont_id,
    p_order_field,p_order_field2   VARCHAR(128), 
    i                              INTEGER,
    p_accept_action                SMALLINT,
    ret_cont_id                    LIKE contact.cont_id,
    error_msg                      VARCHAR(200),
    l_contact                      DYNAMIC ARRAY OF t_cont_rec1,
    l_cont_id                      LIKE contact.cont_id,
    local_debug                    SMALLINT,
    l_scroll_cont_id               LIKE contact.cont_id,
    l_scroll_row_id                SMALLINT

  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC

  #Grid auto scroll - to find the last row number 
  LET l_scroll_row_id = 1
  IF p_cont_id IS NOT NULL THEN
    LET l_scroll_cont_id = p_cont_id
  ELSE
    LET l_scroll_cont_id = NULL
  END IF



  IF NOT p_order_field THEN
    LET p_order_field = "cont_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF

  #Open window with form
    CALL fgl_window_open("w_contact_popup", 3,3, get_form_path("f_contact_scroll_l2"), FALSE) 
    CALL populate_contact_popup_form_labels_g()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    #Prepare sql and populate cursor with field order
    CALL contact_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_cont INTO l_contact[i].*

      IF l_scroll_cont_id IS NOT NULL THEN
        IF l_contact[i].cont_id = l_scroll_cont_id THEN
          LET l_scroll_row_id = i
        END IF
      END IF

      LET i = i + 1
      IF i > 50 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1

		IF i > 0 THEN
			CALL l_contact.resize(i)   --correct the last element of the dynamic array
		END IF  
		
    #CALL set_count(i)

    DISPLAY ARRAY l_contact TO sa_cont_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        CALL publish_toolbar("ContactList",0)


        IF l_scroll_row_id IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,l_scroll_row_id)
        END IF

      BEFORE ROW
	LET i = arr_curr()
        LET l_cont_id = l_contact[i].cont_id
        LET l_scroll_cont_id = l_contact[i].cont_id

      ON KEY(INTERRUPT)  --on cancel, we return the calling argument
        EXIT WHILE

      ON KEY(ACCEPT)

        LET i = arr_curr()
      
        CASE p_accept_action
          WHEN 0  --just return id
            LET ret_cont_id = l_cont_id
            EXIT WHILE

          WHEN 1  --view
            CALL contact_view(l_cont_id)
            CALL fgl_window_current("w_contact_popup")
            EXIT DISPLAY

          WHEN 2  --edit
            CALL contact_edit(l_cont_id,TRUE)
            CALL fgl_window_current("w_contact_popup")
            EXIT DISPLAY

          WHEN 3  --delete
            CALL contact_delete(l_cont_id)
            CALL fgl_window_current("w_contact_popup")
            EXIT DISPLAY

          WHEN 4  --print
            CALL fgl_window_current("w_contact_popup")
            CALL fgl_winmessage("Not implemented","activity_operator_scroll()\nActivity Print is not implemented","info")
            EXIT DISPLAY


          OTHERWISE
            LET error_msg = "contact_popup(p_cont_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("contact_popup() - 4GL Source Error",error_msg, "error") 
        END CASE

      ON KEY(F4) --Add/Create
        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL contact_create(0,TRUE)  --true=open window
        CALL fgl_window_current("w_contact_popup")
        EXIT DISPLAY

      ON KEY(F5)  --Edit
        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL contact_edit(l_cont_id,TRUE)
        CALL fgl_window_current("w_contact_popup")
        EXIT DISPLAY

      ON KEY(F6)  --Delete
        CALL contact_delete(l_cont_id)
        CALL fgl_window_current("w_contact_popup")
        EXIT DISPLAY

      ON KEY(F7)  --View short

        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF
        CALL contact_view_short_by_id(l_cont_id)
        CALL fgl_window_current("w_contact_popup")
        LET int_flag = FALSE
        

      ON KEY(F8)  --View detailed

        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL contact_view(l_cont_id)
        LET int_flag = FALSE
        CALL fgl_window_current("w_contact_popup")

      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_lname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_phone"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY


    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_contact_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_cont_id   --return the function argument in case of cancel...
  ELSE 
    LET i = arr_curr()
    RETURN l_contact[i].cont_id
  END IF
END FUNCTION





##############################################################################################################
#
# Record copy / convert functions
#
##############################################################################################################


######################################################
# FUNCTION contact_to_form_contact(p_contact)
#
# RETURN l_contact_rec.*
######################################################
FUNCTION contact_to_form_contact(p_contact)
  DEFINE 
    p_contact RECORD LIKE contact.*,
    l_contact_rec OF t_cont_rec

  LET l_contact_rec.cont_name = p_contact.cont_name
  LET l_contact_rec.cont_title = p_contact.cont_title
  LET l_contact_rec.cont_fname = p_contact.cont_fname
  LET l_contact_rec.cont_lname = p_contact.cont_lname
  LET l_contact_rec.cont_addr1 = p_contact.cont_addr1
  LET l_contact_rec.cont_addr2 = p_contact.cont_addr2
  LET l_contact_rec.cont_addr3 = p_contact.cont_addr3
  LET l_contact_rec.cont_city = p_contact.cont_city
  LET l_contact_rec.cont_zone = p_contact.cont_zone
  LET l_contact_rec.cont_zip = p_contact.cont_zip
  LET l_contact_rec.cont_country = p_contact.cont_country
  LET l_contact_rec.cont_phone = p_contact.cont_phone
  LET l_contact_rec.cont_fax = p_contact.cont_fax
  LET l_contact_rec.cont_mobile = p_contact.cont_mobile
  LET l_contact_rec.cont_email = p_contact.cont_email
  LET l_contact_rec.cont_dept = p_contact.cont_dept
  LET l_contact_rec.comp_name = get_contact_comp_name(p_contact.cont_id)
  LET l_contact_rec.cont_position = p_contact.cont_position
  LET l_contact_rec.cont_notes = p_contact.cont_notes
  LET l_contact_rec.cont_picture = p_contact.cont_picture
  LET l_contact_rec.cont_usemail = p_contact.cont_usemail
  LET l_contact_rec.cont_usephone = p_contact.cont_usephone
 
  RETURN l_contact_rec.*
END FUNCTION

######################################################
# FUNCTION get_form_contact(p_cont_id)
#
# RETURN l_contact_rec.*
######################################################
FUNCTION get_form_contact(p_cont_id)
  DEFINE p_cont_id LIKE contact.cont_id
  DEFINE l_contact_rec OF t_cont_rec 

  LOCATE l_contact_rec.cont_picture IN MEMORY

  SELECT contact.cont_name, 
         contact.cont_title, 
         contact.cont_fname, 
         contact.cont_lname, 
         contact.cont_addr1,
         contact.cont_addr2, 
         contact.cont_addr3, 
         contact.cont_city, 
         contact.cont_zone, 
         contact.cont_zip, 
         contact.cont_country, 
         contact.cont_phone, 
         contact.cont_mobile, 
         contact.cont_fax, 
         contact.cont_email, 
         contact.cont_dept, 
         contact.cont_position, 
         company.comp_name, 
         contact.cont_usemail,
         contact.cont_usephone,
         contact.cont_notes,
         contact.cont_picture
    INTO l_contact_rec.*
    FROM contact, OUTER company
    WHERE contact.cont_id = p_cont_id
      AND company.comp_id = contact.cont_org

  RETURN l_contact_rec.*
END FUNCTION






##############################################################################################################
#
# Grid / Screen Array functions
#
##############################################################################################################

######################################################
# FUNCTION contact_activities_populate_grid_panel_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for contact_activities_populate_grid_panel
#
# RETURN NONE
#####################################################
FUNCTION contact_activities_populate_grid_panel_data_source(p_order_field,p_ord_dir)
  DEFINE
    p_order_field       VARCHAR(128),
    local_debug         SMALLINT,
    i                   INTEGER,
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  LET local_debug = 0

  IF p_order_field IS NULL  THEN
    LET p_order_field = "cont_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                    "activity.activity_id, ",
                    "activity_type.atype_name, ",
	            "activity.open_date, ",
                    "operator.name, ",
                    "activity.short_desc, ",
                    "activity.priority ",
                  "FROM activity, OUTER activity_type, OUTER operator ",
                  "WHERE activity.act_type = activity_type.type_id ",
                    "AND activity.contact_id = ? ",
                    "AND activity.operator_id = operator.operator_id "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF


  IF local_debug THEN
    DISPLAY sql_stmt[1,50]
    DISPLAY sql_stmt[51,100]
    DISPLAY sql_stmt[101,150]
    DISPLAY sql_stmt[151,200]
  END IF

  PREPARE p_act FROM sql_stmt
  DECLARE c_act CURSOR FOR p_act



END FUNCTION




######################################################
# FUNCTION contact_activities_populate_grid_panel((p_contact_id,p_order_field,p_scroll)
#####################################################
FUNCTION contact_activities_clear_grid_panel()
  DEFINE
    l_act_arr                     ARRAY[1] OF t_act_rec6

  CALL set_count(0)
  DISPLAY ARRAY l_act_arr TO sa_open_act.* WITHOUT SCROLL

END FUNCTION




######################################################
# FUNCTION contact_activities_populate_grid_panel((p_contact_id,p_order_field,p_scroll)
#####################################################
FUNCTION contact_activities_populate_grid_panel(p_contact_id, p_order_field,p_scroll,p_ord_dir)
  DEFINE 
    p_order_field,p_order_field2  VARCHAR(128),
    p_contact_id                  LIKE contact.cont_id,
    local_debug                   SMALLINT,
    i                             INTEGER,
    p_company_id                  LIKE company.comp_id,
    p_scroll                      SMALLINT,
    l_act_arr                     DYNAMIC ARRAY OF t_act_rec6,
    action_edit                   SMALLINT,
    p_ord_dir                     SMALLINT

  LET local_debug = 0  ---0=off   1=on
  LET action_edit = FALSE

  IF local_debug THEN
    DISPLAY "1-contact_activities_populate_grid_panel() - p_contact_id=",p_contact_id 
    DISPLAY "1-contact_activities_populate_grid_panel() - p_order_field=",p_order_field 
    DISPLAY "1-contact_activities_populate_grid_panel() - p_scroll=",p_scroll 
    DISPLAY "1-contact_activities_populate_grid_panel() - p_ord_dir=",p_ord_dir 
    DISPLAY "1-contact_activities_populate_grid_panel() - get_toggle_switch()=",get_toggle_switch() 

  END IF

  IF p_ord_dir IS NULL THEN
    CALL toggle_switch_on()  --default sort order ASC
    IF local_debug THEN
      DISPLAY "2a-contact_activities_populate_grid_panel() - p_ord_dir IS NULL = ", p_ord_dir
      DISPLAY "2a-contact_activities_populate_grid_panel() - toggle_switch =", get_toggle_switch()
    END IF
  ELSE
    CALL set_toggle_switch(p_ord_dir)
    IF local_debug THEN
      DISPLAY "2b-contact_activities_populate_grid_panel() - p_ord_dir IS NOT NULL = ", p_ord_dir
      DISPLAY "2b-contact_activities_populate_grid_panel() - toggle_switch =", get_toggle_switch()
    END IF
  END IF

  IF p_order_field IS NULL THEN
    LET p_order_field = "open_date"
  END IF

  IF local_debug THEN
    DISPLAY "3-contact_activities_populate_grid_panel() - p_contact_id=",p_contact_id 
    DISPLAY "3-contact_activities_populate_grid_panel() - p_order_field=",p_order_field 
    DISPLAY "3-contact_activities_populate_grid_panel() - p_scroll=",p_scroll 
    DISPLAY "3-contact_activities_populate_grid_panel() - p_ord_dir=",p_ord_dir 
    DISPLAY "3-contact_activities_populate_grid_panel() - get_toggle_switch()=",get_toggle_switch() 

  END IF

  WHILE TRUE

    CALL contact_activities_populate_grid_panel_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    IF local_debug = 1 THEN
      DISPLAY "p_contact_id=",p_contact_id
    END IF


  FOREACH c_act USING p_contact_id INTO l_act_arr[i].* 
    IF local_debug = 1 THEN
      DISPLAY "contact_activities_populate_grid_panel() - atype_name",l_act_arr[i].atype_name
      DISPLAY "contact_activities_populate_grid_panel() - open_date",l_act_arr[i].open_date
      DISPLAY "contact_activities_populate_grid_panel() - short_desc",l_act_arr[i].short_desc
      DISPLAY "contact_activities_populate_grid_panel() - priority",l_act_arr[i].priority
      DISPLAY "contact_activities_populate_grid_panel() - name",l_act_arr[i].name
    END IF

    LET i = i + 1
  END FOREACH
  LET i = i-1
  
	IF i > 0 THEN
		CALL l_act_arr.resize(i)   --correct the last element of the dynamic array
	END IF  
		  
	
  #CALL set_count(i-1)

  IF p_scroll THEN  -- function parameter

    DISPLAY ARRAY l_act_arr TO sa_open_act.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL set_help_id(300)
        CALL publish_toolbar("ContactEditGrid",0)

      ON KEY (ACCEPT)
        LET action_edit = TRUE
        LET i = arr_curr()   --get current row
        EXIT DISPLAY

      ON KEY (INTERRUPT,F11)
        EXIT WHILE


      # Column Sort short cuts

      ON KEY(F13) --Order screen array by activity_id
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.activity_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F14) --Order screen array by open_date
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity_type.atype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

     
      ON KEY(F15) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.open_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F16) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "operator.name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F17) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18) 
        LET p_order_field2 = p_order_field
        LET p_order_field = "activity.priority"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      LET int_flag = FALSE
      EXIT WHILE
    ELSE 
      IF action_edit THEN  --User choose accept to edit (double click or ACCEPT key)
        CALL activity_edit(l_act_arr[i].activity_id)
        LET action_edit = FALSE
      END IF
    END IF

    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

  ELSE 
    DISPLAY ARRAY l_act_arr TO sa_open_act.* WITHOUT SCROLL
    EXIT WHILE
  END IF

  END WHILE

  #possible toolbar clean up 
  CALL publish_toolbar("ContactEditGrid",1)

END FUNCTION





