##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings help_classic
#
# This 4gl module includes functions to change the environment 
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_help_classic_edit()
#
# Edit help_classic settings (help_classic.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_help_classic_edit()
  DEFINE 
    l_settings_help_classic_rec  OF t_qxt_settings_help_classic_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_help_classic",5,5,get_form_path("f_qxt_settings_help_classic"),FALSE)
  CALL qxt_settings_help_classic_view_form_details()

  #Get the corresponding record
  CALL get_settings_help_classic_form_rec()
    RETURNING  l_settings_help_classic_rec.*

  #Input
  INPUT BY NAME l_settings_help_classic_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    ON ACTION("settingsSave")  --Save 
      CALL fgl_dialog_update_data()
      CALL copy_settings_help_classic_form_rec(l_settings_help_classic_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_help_classic_form_rec(l_settings_help_classic_rec.*) --Write to memory
      CALL process_help_classic_cfg_export(NULL) --Write to disk
      EXIT INPUT

    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD help_classic_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_help_classic_form_rec(l_settings_help_classic_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  CALL publish_toolbar("settingsCfg",1)

  IF NOT int_flag THEN
    CALL copy_settings_help_classic_form_rec(l_settings_help_classic_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_help_classic")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_help_classic_view_form_details()

  CALL fgl_settitle(get_str_tool(180))

  DISPLAY get_str_tool(181) TO dl_f81 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(182) TO dl_f82 ATTRIBUTE(BLUE, BOLD)

 
  CALL boolean_enabled_combo_list("help_classic_multi_lang_name")   

END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_help_classic_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_help_classic_form_rec
  

  ##################################
  # help_classic.cfg Configuration file
  ##################################
  #[help_classic] Section

  LET l_settings_form.help_classic_multi_lang_name  = get_boolean_enabled_name(qxt_settings.help_classic_multi_lang) --help_classic id
  LET l_settings_form.help_classic_unl_filename     = qxt_settings.help_classic_unl_filename                --default help_classic to be used if a help_classic does not exist in a particular help_classic

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_help_classic_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_help_classic_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_help_classic_form_rec
  
  ##################################
  # help_classic.cfg Configuration file
  ##################################
  #[help_classic] Section
  LET qxt_settings.help_classic_multi_lang         = get_boolean_enabled_id(p_settings_form_rec.help_classic_multi_lang_name) --help_classic id
  LET qxt_settings.help_classic_unl_filename       = p_settings_form_rec.help_classic_unl_filename                   --default help_classic to be used if a help_classic does not exist in a particular help_classic


END FUNCTION



####################################################################################################
# EOF
####################################################################################################



