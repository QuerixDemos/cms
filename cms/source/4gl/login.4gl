##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

##################################################
# FUNCTION login()
##################################################
FUNCTION login()
  DEFINE l_retValidLogin  BOOLEAN
  DEFINE l_operator       OF t_operator_rec
	DEFINE windowObject WINDOW
	DEFINE l_session_count LIKE operator_session.session_counter
	DEFINE l_session_id LIKE operator_session.session_id
	
	#DISPLAY "!!! In Login module !!!"
	
	LET l_session_id = fgl_getenv("QX_SESSION_ID")
	
	IF count_session_record(l_session_id) > 0 AND NOT current_session_expired() THEN  --session already exists - do not double login
		#IF NOT current_session_expired() THEN
			#CALL plus_counter_operator_session()
			CALL session_login()
			#RETURN TRUE --valid_login
			LET l_retValidLogin = TRUE
		#END IF
	ELSE
    
		CALL windowObject.openWithForm("Login",get_form_path("f_login_l2"),1,1,"STYLE=\"full-screen,statusbar_lines5\"")	
 

		CALL populate_login_form_labels_g()    


	  LET int_flag = FALSE

	  #to make it easy and la'c'y :-) for demo purpose
	  LET l_operator.password = "ALEX"
	  LET l_operator.name = "ALEX"

	  INPUT BY NAME l_operator.* WITHOUT DEFAULTS HELP 80
	    BEFORE INPUT
	      CALL hide_dialog_navigation_toolbar(0)

			ON ACTION ("actExit", "actExitMenu", "actLogOut")
				EXIT Program
				      
	    AFTER INPUT
	      IF int_flag THEN   --operator pressed cancel
	        LET int_flag = FALSE
	        LET l_retValidLogin = FALSE
	        EXIT INPUT
	      ELSE  -- operator pressed OK / Accept
	        SELECT *
	          INTO g_operator.*
	          FROM operator
	         WHERE operator.name = l_operator.name
	           AND operator.password = l_operator.password

	        IF sqlca.sqlcode = 0 THEN   ---SUCCESS ! if a valid operator for login
	          LET l_retValidLogin = TRUE
						CALL enable_current_session_record()  --new login always sets sessions record expired to FALSE (new session record or existing session record)

	          EXIT INPUT
	        ELSE
	          LET l_retValidLogin = FALSE
	          CALL fgl_message_box(get_Str(1030)) --Login screen
	          CONTINUE INPUT
	        END IF
        
	      END IF
	  END INPUT

		#CALL fgl_window_close("w_login")
		CALL windowObject.close()

		IF l_retValidLogin = FALSE THEN
			EXIT PROGRAM
		ELSE
	
			CALL add_operator_session()
			LET l_retValidLogin = TRUE
		END IF

	END IF	

 	RETURN l_retValidLogin
 
END FUNCTION

