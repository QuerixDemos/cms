##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#######################################################################################################################################################
# Functions to manage Invoices
#
# FUNCTION:                                                                DESCRIPTON:                                       RETURN:
# supplies_main()                                                          Main Supplies Menu                                NONE
# supplies_create()                                                        Cretae new supply                                 suppl_id
# supplies_input(p_supplies)                                               Supplies Input                                    NONE
# create_supplies_temp(p_supplies_id)                                      Create temporary supply                           NONE
# copy_supplies_temp(p_supplies_id,p_process_items)                        Copy temporary suply                              NONE
# update_supplies_status(p_supplies)                                       Update supply status                              NONE
# get_supplies_status(p_supplies_status)                                   Get status name by status id                      status_name                                      
# supplies_input_header(p_supplies)                                        Input supply header details                       status_name                              
# supplies_input_detail(p_supplies,w_name)                                 Input supply items details                        status_name
# get_supplies_rec(p_suppl_id)                                             Get supply details                                r_supplies.* 
# supplies_view(p_supplies_id)                                             View supply                                       NONE
# supplies_edit(p_supplies_id)                                             Edit Supply                                       NONE  
# supplies_popup_data_source(p_order_field,p_ord_dir,p_status)             Prepare supplies cursor                           NONE  
# supplies_popup(p_supplies_id,p_order_field,p_accept_action,p_status)     Show supplies list                                supply_id     
# populate_supplies_form_g()                                               Populate supplies form in gui mode                NONE
# populate_supplies_form_t()                                               Populate supplies form in text mode               NONE   
# select_supplies()                                                        Select supplies                                   supplies_id or NULL
# show_supplies_header(p_supplies)                                         Show supplies header details                      NONE
# show_supplies_detail(p_supplies_id)                                      Show supplies items detail                        NONE
# supplies_show_temp_detail()                                              Show details temporary supply                     NONE 
# load_supplies_temp_detail()                                              Load detatils temporary supply                    NONE
# supplies_delete(p_suppl_id)                                              Delete supply                                     NONE    
# supply_state_combo_list(cb_field_name, p_supply_id)                      Populate supply_state combo list from database    NONE
# get_sup_state_id(sup_state_name)                                         Get supply state id from name                     l_sup_state_id
# get_sup_state_name(sup_state_id)                                         Get supply state name from id                     l_sup_state_name
# verify_sup_data(p_supplies)                                              Verify input data of supply                       TRUE or FALSE
# verify_sup_lines(p_invoice_lines)                                        Verify input stock items of supply                TRUE or FALSE
#
#########################################################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# Module Definitions
######################################################
 # DEFINE status_msg        DYNAMIC ARRAY OF VARCHAR(20)
 # DEFINE l_suppl_lines     DYNAMIC ARRAY OF t_supp_line  --huho - someone made this global.. haven't got the time to investigate this
 # DEFINE l_sup_stat        LIKE supplies.state

  DEFINE wait_supplies_msg VARCHAR(100)
  DEFINE help_msg VARCHAR(100)
  #DEFINE l_verified_sup_lines SMALLINT
########## END of module variables #################


############################################################################################################################
# Menu functions
############################################################################################################################

###################################################
# FUNCTION supplies_main()
#
# suplies main menu
#
# RETURN NONE
###################################################
FUNCTION supplies_main()
  DEFINE
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings
  MENU "supplies"
    BEFORE MENU
      CALL set_help_id(601)
      CALL publish_toolbar("Invoice",0)

	CALL ui.Interface.setImage("qx://application/icon16/industry/building_storage-import.png")
	CALL ui.Interface.setText("Supplies")

        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(1420)
        LET menu_str_arr2[1] = get_str(1421)
        LET menu_str_arr1[2] = get_str(1422)
        LET menu_str_arr2[2] = get_str(1423)
        LET menu_str_arr1[3] = get_str(2477)
        LET menu_str_arr2[3] = get_str(2478)
        LET menu_str_arr1[4] = get_str(2479)
        LET menu_str_arr2[4] = get_str(2480)
        LET menu_str_arr1[5] = get_str(1428)
        LET menu_str_arr2[5] = get_str(1429)
        LET menu_str_arr1[6] = get_str(2481)
        LET menu_str_arr2[6] = get_str(2482)
        LET menu_str_arr1[7] = get_str(2483)
        LET menu_str_arr2[7] = get_str(2484)

        LET status_msg[1] = get_str(2479)
        LET status_msg[2] = get_str(2483)
        LET status_msg[3] = get_str(2481)
        LET help_msg = get_str(1539)
        LET wait_supplies_msg = get_str(2475)

    COMMAND KEY (F601,"N") menu_str_arr1[1] menu_str_arr2[1] HELP 501  -- "New" "Create a new supplies" help 501
      CALL supplies_create()

    COMMAND KEY (F602,"F") menu_str_arr1[2] menu_str_arr2[2] HELP 503  -- "Find" "Find - Search for an supplies" help 503
      IF fgl_fglgui() THEN --gui client
        CALL supplies_popup(NULL,"suppl_id",1,NULL)
      ELSE
        MENU "find_supplies"
          COMMAND KEY ("A") menu_str_arr1[3] menu_str_arr2[3] HELP 503
            CALL supplies_popup(NULL,"suppl_id",1,NULL)
          COMMAND menu_str_arr1[4] menu_str_arr2[4] HELP 503 -- Wait Supplies
            CALL supplies_popup(NULL,"suppl_id",1,1)
          COMMAND menu_str_arr1[6] menu_str_arr2[6] HELP 503 -- Supplied
            CALL supplies_popup(NULL,"suppl_id",1,2)
          COMMAND menu_str_arr1[7] menu_str_arr2[7] HELP 503 -- Cancelled
            CALL supplies_popup(NULL,"suppl_id",1,3)
          COMMAND KEY (F12,"Q")  menu_str_arr1[5] menu_str_arr2[5] HELP 506  -- "Main Menu" "Close window and return to previous menu" help 506
            EXIT MENU
    			ON ACTION ("actExit") 
						EXIT MENU		            
        END MENU
      END IF
    COMMAND KEY (F12,"Q")  menu_str_arr1[5] menu_str_arr2[5] HELP 506  -- "Main Menu" "Close window and return to previous menu" help 506
      EXIT MENU
      
    ON ACTION ("actExit") 
			EXIT MENU		      
  END MENU

  #clean up toolbar
  CALL publish_toolbar("Invoice",1)

END FUNCTION



############################################################################################################################
# Data Access functions
############################################################################################################################

###################################################
# FUNCTION supplies_create()
#
# Returns the new supply id
#
# RETURN suppl_id
###################################################
FUNCTION supplies_create()
  DEFINE 
    local_debug SMALLINT,
    l_supplies  RECORD LIKE supplies.*,
    i, cnt      INTEGER,
    l_account   RECORD LIKE account.*

  CALL account_popup(NULL,"account_id",0)
    RETURNING l_account.account_id

  IF NOT exist(l_account.account_id) THEN
    RETURN NULL
  END IF

  CALL get_account_rec(l_account.account_id)
    RETURNING l_account.*

  LET l_supplies.suppl_id = 0
  LET l_supplies.state = 1
  LET l_supplies.operator_id = g_operator.operator_id
  LET l_supplies.suppl_date = TODAY
  LET l_supplies.exp_date = TODAY + INTERVAL (3) DAY TO DAY
  LET l_supplies.account_id = l_account.account_id

  BEGIN WORK
    CALL create_supplies_temp(0)

    CALL supplies_input(l_supplies.*)
      RETURNING l_supplies.*

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN NULL
    END IF

    INSERT INTO supplies VALUES (l_supplies.*)

    LET l_supplies.suppl_id = sqlca.sqlerrd[2]

    CALL copy_supplies_temp(l_supplies.suppl_id,0)

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN NULL
    END IF

#    CALL print_report_supplies(l_supplies.suppl_id)

  COMMIT WORK

  RETURN l_supplies.suppl_id

END FUNCTION



###################################################
# FUNCTION supplies_input(p_supplies)
#
# Returns supply details information
#
# RETURN p_supplies.*
###################################################
FUNCTION supplies_input(p_supplies)
  DEFINE 
    p_supplies    RECORD LIKE supplies.*,
    input_status SMALLINT,
    w_name       VARCHAR(30),
    l_verify     SMALLINT,
    l_init_state LIKE supplies.state
	DEFINE l_sup_stat        LIKE supplies.state

  LET l_sup_stat = p_supplies.state
  LET w_name = "w_inv_input"

	IF NOT fgl_window_open(w_name, 2, 8, get_form_path("f_supplies_det_l2"),FALSE) THEN
		CALL fgl_winmessage("Error","supplies_input()\nCannot open window " || w_name,"error")
	END IF

  CALL show_supplies_header(p_supplies.*)
  CALL supplies_show_temp_detail()

  LET l_init_state = p_supplies.state
  LET l_verify = FALSE
  
  WHILE NOT l_verify
 
    LET input_status = 1

    WHILE (NOT int_flag AND input_status > 0)
      CALL supplies_input_header(p_supplies.*)
        RETURNING p_supplies.*, input_status

      IF NOT int_flag AND input_status > 0 THEN
        CALL supplies_input_detail(p_supplies.*,w_name)
          RETURNING input_status
      END IF
    END WHILE

    LET l_verify = verify_sup_data(p_supplies.*, l_init_state)

  END WHILE

  CALL fgl_window_close(w_name)
  RETURN p_supplies.*

END FUNCTION



###################################################
# FUNCTION create_supplies_temp(p_supplies_id)
#
# Temp table
#
# RETURN NONE
###################################################
FUNCTION create_supplies_temp(p_supplies_id)
  DEFINE p_supplies_id LIKE supplies.suppl_id

  SELECT supplies_line.*
    FROM supplies_line
    WHERE supplies_line.suppl_id = p_supplies_id
    INTO TEMP supplies_line_temp
  DELETE FROM supplies_line
    WHERE supplies_line.suppl_id = p_supplies_id
    

END FUNCTION



###################################################
# FUNCTION copy_supplies_temp(p_supplies_id,p_process_items)
#
#
#
# RETURN NONE
###################################################
FUNCTION copy_supplies_temp(p_supplies_id,p_process_items)
  DEFINE 
    p_supplies_id   LIKE supplies.suppl_id,
    p_process_items SMALLINT,
    l_supplies_line RECORD LIKE supplies_line.*


  DECLARE c_copy_temp CURSOR FOR
    SELECT * FROM supplies_line_temp

  FOREACH c_copy_temp INTO l_supplies_line.*
    LET l_supplies_line.suppl_id = p_supplies_id
    INSERT INTO supplies_line VALUES (l_supplies_line.*)
    IF p_process_items THEN 
      UPDATE stock_item SET quantity = quantity + l_supplies_line.quantity WHERE stock_id = l_supplies_line.stock_id
    END IF
  END FOREACH

  DROP TABLE supplies_line_temp
END FUNCTION



###################################################
# FUNCTION update_supplies_status(p_supplies)
#
#
#
# RETURN NONE
###################################################
FUNCTION update_supplies_status(p_supplies, p_status_name)

  DEFINE p_supplies     RECORD LIKE supplies.*,
         p_status_name  LIKE state_supply.state_name


  IF p_supplies.suppl_id = 0 THEN
    DISPLAY help_msg TO lbInfo1
  ELSE
    CASE p_supplies.state
      WHEN 1
        DISPLAY help_msg || " " || wait_supplies_msg TO lbInfo1
      OTHERWISE
        DISPLAY help_msg TO lbInfo1
    END CASE
  END IF

  IF p_status_name IS NULL THEN
    DISPLAY get_supplies_status(p_supplies.state) TO status_name
  ELSE
    DISPLAY p_status_name TO status_name
  END IF

    
END FUNCTION



###################################################
# FUNCTION get_supplies_status(p_supplies_status)
#
# Return the status name or error message
#
# RETURN status_name
###################################################
FUNCTION get_supplies_status(p_supplies_status)

  DEFINE p_supplies_status SMALLINT

  LET status_msg[1]  = get_str(2556)
  LET status_msg[2]  = get_str(2557)
  LET status_msg[3]  = get_str(2558)

  IF p_supplies_status < 1 OR p_supplies_status > 3 THEN
    IF fgl_fglgui() THEN --gui client
      CALL fgl_list_clear("status_name", 1, 100) 
      CALL fgl_list_set("status_name", 1, "<err>")
    END IF
    RETURN "<err>"
  ELSE
    RETURN status_msg[p_supplies_status]
  END IF

END FUNCTION



###################################################
# FUNCTION supplies_input_header(p_supplies)
#
# Returns supply header information
#
# RETURN status_name
###################################################
FUNCTION supplies_input_header(p_supplies)
  DEFINE 
    a smallint,
    b smallint,
    p_supplies RECORD LIKE supplies.*,
    l_supplies_rec OF t_supplies_rec,
    input_status SMALLINT

  LET input_status = 0

  CALL show_supplies_header(p_supplies.*)

  {!! set l_supplies_rec !!}
  LET l_supplies_rec.suppl_id = p_supplies.suppl_id
  LET l_supplies_rec.suppl_date = p_supplies.suppl_date
  LET l_supplies_rec.exp_date = p_supplies.exp_date
  #LET l_supplies_rec.status_name = get_supplies_status(p_supplies.state)
  LET l_supplies_rec.status_name = get_sup_state_name(p_supplies.state) 

  LET int_flag = FALSE


  INPUT BY NAME l_supplies_rec.* WITHOUT DEFAULTS
    BEFORE INPUT
      CALL set_help_id(601)
      CALL publish_toolbar("InvoiceEditHeader",0)
      CALL update_supplies_status(p_supplies.*, NULL)
      LET l_supplies_rec.status_name = get_supplies_status(p_supplies.state)
    ON KEY (F4)
      IF p_supplies.suppl_id > 0 AND p_supplies.state = 1 THEN
        LET l_supplies_rec.status_name=get_sup_state_name(2)
      END IF
      CALL update_supplies_status(p_supplies.*, l_supplies_rec.status_name)
    ON KEY (F9)
      IF p_supplies.suppl_id > 0 AND p_supplies.state = 1 THEN
        LET l_supplies_rec.status_name=get_sup_state_name(3)
      END IF
      CALL update_supplies_status(p_supplies.*, l_supplies_rec.status_name)
    ON KEY (F11)
      CALL fgl_dialog_update_data()
      LET input_status = 1
      DISPLAY help_msg TO lbInfo1
      EXIT INPUT

    AFTER INPUT
      IF int_flag THEN
        IF NOT yes_no(get_str(1467),get_str(1468)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF

  END INPUT   

  #Possible toolbar cleeanup
  CALL publish_toolbar("InvoiceEditHeader",1)

  {!! set p_supplies data !!}
  LET p_supplies.exp_date = l_supplies_rec.exp_date
  LET p_supplies.state = get_sup_state_id(l_supplies_rec.status_name)
  RETURN p_supplies.*, input_status

END FUNCTION



###################################################
# FUNCTION supplies_input_detail(p_supplies,w_name)
#
# Returns the status
#
# RETURN input_status
###################################################
FUNCTION supplies_input_detail(p_supplies,w_name)
  DEFINE 
    a_idx, s_idx   INTEGER,
    tmp            CHAR(20),
    i              INTEGER,
    p_supplies     RECORD LIKE supplies.*,
    l_stock_item   RECORD LIKE stock_item.*,
    input_status   SMALLINT,
    cnt            INTEGER,
    w_name         VARCHAR(30)

	DEFINE l_verified_sup_lines SMALLINT

  #Note: w_name is required to handle window operations 'current'
  LET input_status = 0

  CALL show_supplies_header(p_supplies.*)

  LET i = load_supplies_temp_detail()

  DELETE FROM supplies_line_temp

  CALL set_count(i)

  LET int_flag = FALSE

  INPUT ARRAY g_suppl_lines 
    WITHOUT DEFAULTS 
    FROM sc_suppl_lines.*

    BEFORE INPUT
      CALL set_help_id(601)
      CALL publish_toolbar("InvoiceEditLine",0)

    BEFORE ROW
      LET a_idx = arr_curr()
      LET s_idx = scr_line()

    AFTER FIELD stock_id
      IF verify_sup_lines(arr_count()) THEN
        DISPLAY "" AT 1, 1
        CALL get_stock_item(g_suppl_lines[a_idx].stock_id)
          RETURNING l_stock_item.*
        LET g_suppl_lines[a_idx].stock_id = l_stock_item.stock_id
        LET g_suppl_lines[a_idx].item_cost = l_stock_item.item_cost
        LET g_suppl_lines[a_idx].item_desc = l_stock_item.item_desc
        DISPLAY g_suppl_lines[a_idx].stock_id TO sc_suppl_lines[s_idx].stock_id
        DISPLAY g_suppl_lines[a_idx].item_cost TO sc_suppl_lines[s_idx].item_cost
        DISPLAY g_suppl_lines[a_idx].item_desc TO sc_suppl_lines[s_idx].item_desc
      END IF

    ON KEY (F10)
      CALL stock_item_popup(g_suppl_lines[a_idx].stock_id,"stock_id",0)
        RETURNING g_suppl_lines[a_idx].stock_id
      CALL fgl_window_current(w_name)  --make sure, window is active
      CALL get_stock_item(g_suppl_lines[a_idx].stock_id)
        RETURNING l_stock_item.*
      LET g_suppl_lines[a_idx].stock_id = l_stock_item.stock_id
      LET g_suppl_lines[a_idx].item_cost = l_stock_item.item_cost
      LET g_suppl_lines[a_idx].item_desc = l_stock_item.item_desc
      DISPLAY g_suppl_lines[a_idx].stock_id TO sc_suppl_lines[s_idx].stock_id
      DISPLAY g_suppl_lines[a_idx].item_cost TO sc_suppl_lines[s_idx].item_cost
      DISPLAY g_suppl_lines[a_idx].item_desc TO sc_suppl_lines[s_idx].item_desc
      NEXT FIELD quantity

    ON KEY (F11)
      IF verify_sup_lines(arr_count()) THEN
        LET input_status = 1
        CALL update_supplies_status(p_supplies.*, NULL)
        EXIT INPUT
      END IF

    ON KEY (ACCEPT)
      IF verify_sup_lines(arr_count()) THEN
        EXIT INPUT
      ELSE
        CONTINUE INPUT
      END IF

    AFTER INPUT
      IF NOT int_flag THEN
        IF NOT l_verified_sup_lines THEN
          CONTINUE INPUT
        END IF
      ELSE
        IF NOT yes_no(get_str(1467),get_str(1468)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
    
  END INPUT

  CALL publish_toolbar("InvoiceEditLine",1)

  LET cnt = arr_count()

  FOR i = 1 TO cnt
    IF g_suppl_lines[i].stock_id IS NULL THEN
      CONTINUE FOR
    END IF

    INSERT INTO supplies_line_temp 
      VALUES (p_supplies.suppl_id, 
              g_suppl_lines[i].stock_id,
              g_suppl_lines[i].quantity
	  )
  END FOR
  
  RETURN input_status

END FUNCTION




###################################################
# FUNCTION supplies_view(p_supplies_id)
#
# 
#
# RETURN NONE
###################################################
FUNCTION supplies_view(p_supplies_id)
  DEFINE 
    p_supplies_id LIKE supplies.suppl_id
  CALL supplies_edit(p_supplies_id)
END FUNCTION



###################################################
# FUNCTION supplies_edit(p_supplies_id)
#
# 
#
# RETURN NONE
###################################################
FUNCTION supplies_edit(p_supplies_id)
  DEFINE 
    p_supplies_id LIKE supplies.suppl_id,
    l_supplies    RECORD LIKE supplies.*,
    l_process_supplies SMALLINT,
    l_pre_supplies_state LIKE supplies.state

  CALL get_supplies_rec(p_supplies_id)
    RETURNING l_supplies.*

  LET l_pre_supplies_state = l_supplies.state
  LET l_process_supplies = 0
  BEGIN WORK
    CALL create_supplies_temp(l_supplies.suppl_id)

    CALL supplies_input(l_supplies.*)
      RETURNING l_supplies.*

    IF l_pre_supplies_state = 1 AND l_supplies.state = 2 THEN
      LET l_process_supplies = 1
    END IF
    CALL copy_supplies_temp(l_supplies.suppl_id,l_process_supplies)

    IF int_flag THEN
      LET int_flag = FALSE
      ROLLBACK WORK
      RETURN
    ELSE

      UPDATE supplies SET
        exp_date = l_supplies.exp_date {!!!}
        WHERE suppl_id = p_supplies_id
          AND state = 1

      UPDATE supplies SET
        state = l_supplies.state {!!!}
        WHERE suppl_id = p_supplies_id

    IF l_supplies.state = 2 THEN
      CALL mass_update_processible_invoices()
    END IF

    END IF
  COMMIT WORK

END FUNCTION



###################################################
# FUNCTION supplies_popup_data_source(p_order_field,p_ord_dir,p_status)
#
# 
#
# RETURN NONE
###################################################
FUNCTION supplies_popup_data_source(p_order_field,p_ord_dir,p_status)
  DEFINE 
    i INTEGER,
    p_order_field         VARCHAR(128),
    local_debug           SMALLINT,
    sql_stmt              CHAR(2048),
    p_ord_dir             SMALLINT,
    p_status		  SMALLINT,
    p_ord_dir_str         VARCHAR(4)

  LET local_Debug = 0

  IF p_order_field IS NULL  THEN
    LET p_order_field = "suppl_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  LET sql_stmt = "SELECT supplies.suppl_id, ",
                   "operator.name, ",
                   "supplies.suppl_date, ",
                   "supplies.exp_date, ",
                   "supplies.state, ",
                   "company.comp_name ",
                 #"FROM supplies,operator,company ",
                 #"WHERE supplies.operator_id = operator.operator_id"
                 "FROM supplies, operator, account, company ",
                 "WHERE supplies.operator_id = operator.operator_id ",
                    "AND supplies.account_id = account.account_id ",
                    "AND company.comp_id = account.comp_id"
                   
                    

  IF p_status IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " AND supplies.state = ", p_status
  END IF
  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_supplies_scroll FROM sql_stmt
  DECLARE c_supplies_scroll CURSOR FOR p_supplies_scroll

END FUNCTION



###################################################
# FUNCTION supplies_popup(p_supplies_id,p_order_field,p_accept_action,p_status)
#
# Return choosen supply id
#
# RETURN supply_id
###################################################
FUNCTION supplies_popup(p_supplies_id,p_order_field,p_accept_action,p_status)
  DEFINE 
    l_supplies_arr                 DYNAMIC ARRAY OF t_supplies_short_rec,
    l_arr_size                     SMALLINT,
    p_supplies_id                  LIKE supplies.suppl_id,
    i                              INTEGER,
    rv_supplies_id                 LIKE supplies.suppl_id,
    p_order_field,p_order_field2   VARCHAR(128), 
    local_debug                    SMALLINT,
    col_sort                       SMALLINT,   --is set to true if the column should be sorted
    p_accept_action                SMALLINT,
    p_status			   SMALLINT,
    l_scroll_row                   SMALLINT,
    l_scroll_supplies_id           LIKE supplies.suppl_id,
    l_supplies_id                  LIKE supplies.suppl_id,
    l_status			   SMALLINT
  
  CALL toggle_switch_on()  --default sort order ASC
  LET l_arr_size = 100 -- due to dynamic array ...sizeof(l_supplies_arr)
  LET l_scroll_row = 1

  IF NOT p_order_field THEN
    LET p_order_field = "suppl_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF


    IF NOT fgl_window_open("w_supplies_popup", 3, 3, get_form_path("f_supplies_scroll_l2"), FALSE) THEN
      CALL fgl_winmessage("Error","supplies_popup()\nCould not open window w_supplies_popup","error")
      RETURN p_supplies_id
    END IF
    CALL populate_supplies_form_g()

  LET int_flag = FALSE

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF
    CALL supplies_popup_data_source(p_order_field,get_toggle_switch(),p_status)

    LET i = 1
    FOREACH c_supplies_scroll INTO l_supplies_arr[i].suppl_id,l_supplies_arr[i].name,l_supplies_arr[i].suppl_date,l_supplies_arr[i].exp_date,l_status,l_supplies_arr[i].comp_name
      IF i > l_arr_size THEN
        CALL fgl_winmessage("Too many rows for array","Can only display the first " || l_arr_size || " of records","info")
        EXIT FOREACH
      END IF
      LET l_supplies_arr[i].status_name = get_supplies_status(l_status);
      IF l_supplies_arr[i].suppl_id = l_scroll_supplies_id THEN
        LET l_scroll_row = i
      END IF
      LET i = i + 1
    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_supplies_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF 

     DISPLAY ARRAY l_supplies_arr TO sc_suppl_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1

      BEFORE DISPLAY
        IF fgl_fglgui() THEN
          IF i = 0 THEN
            #DISPLAY "*" TO bt_print
            #DISPLAY "*" TO bt_edit
            #DISPLAY "*" TO bt_ok
            #DISPLAY "*" TO bt_delete
          ELSE
            #DISPLAY "!" TO bt_print
            #DISPLAY "!" TO bt_edit
            #DISPLAY "!" TO bt_ok
            #DISPLAY "!" TO bt_delete
          END IF
        END IF

        CALL publish_toolbar("InvoiceList",0)
        CALL fgl_dialog_setcurrline(1,l_scroll_row)
        LET i = arr_curr()
        LET l_supplies_id = l_supplies_arr[i].suppl_id 
    
      BEFORE ROW
        LET i = arr_curr()
        LET l_supplies_id = l_supplies_arr[i].suppl_id  
        LET l_scroll_supplies_id = l_supplies_arr[i].suppl_id

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)


        CASE p_accept_action
          WHEN 0  --just return id
            EXIT WHILE
          WHEN 1  --view

            CALL fgl_window_current("SCREEN")

            CALL supplies_view(l_supplies_id)
            CALL fgl_window_current("w_supplies_popup")

          WHEN 2  --edit
            CALL fgl_window_current("SCREEN")
            CALL supplies_edit(l_supplies_id)
            CALL fgl_window_current("w_supplies_popup")
          WHEN 3
            CALL supplies_delete(l_supplies_id)
          WHEN 4
#            CALL print_report_supplies(l_supplies_id)
            CALL fgl_window_current("w_supplies_popup")
          OTHERWISE
            CALL fgl_winmessage("supplies_popup() - 4GL Source Error","supplies_popup(p_supplies_id,p_order_field,p_accept_action = " || p_accept_action, "error") 
        END CASE

        CALL fgl_window_current("w_supplies_popup")
        EXIT DISPLAY

      ON KEY (F4)
        CALL supplies_create()
        EXIT DISPLAY

      ON KEY (F5)  --Edit Invoice
        CALL fgl_window_current("SCREEN")
        CALL supplies_edit(l_supplies_id)
        CALL fgl_window_current("w_supplies_popup")
        EXIT DISPLAY

      ON KEY (F6)  --delete supplies
        CALL supplies_delete(l_supplies_id)
        EXIT DISPLAY

      ON KEY (F8)  --print supplies
        CALL print_report_supplies(l_supplies_id)
        DISPLAY "                                             " AT 1,1
        DISPLAY "                                             " AT 2,1
        DISPLAY get_str(2544) TO lbTitle
        EXIT DISPLAY



      #Column Sort shortcut- Grid Column
      ON KEY(F13, F81)
        CALL column_sort(p_order_field,"suppl_id") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY
      ON KEY(F82)
        CALL column_sort(p_order_field, "name") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY
      ON KEY(F83)
        CALL column_sort(p_order_field, "suppl_date") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY
      ON KEY(F84)
        CALL column_sort(p_order_field, "exp_date") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY
      ON KEY(F85)
        CALL column_sort(p_order_field, "state") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY
      ON KEY(F86)
        CALL column_sort(p_order_field, "comp_name") RETURNING p_order_field2,p_order_field
        EXIT DISPLAY

      #Filter
      ON KEY (F121)
        LET p_status = NULL
        EXIT DISPLAY
      ON KEY (F122)
        LET p_status = 1
        EXIT DISPLAY
      ON KEY (F123)
        LET p_status = 2
        EXIT DISPLAY
      ON KEY (F124)
        LET p_status = 3
        EXIT DISPLAY

    END DISPLAY

    CALL publish_toolbar("InvoiceList",1)

    IF int_flag = TRUE THEN  --check if operator choose cancel
      EXIT WHILE
    END IF
    
    IF col_sort THEN  
      LET col_sort = FALSE
      CALL supplies_popup_data_source(p_order_field,NULL,NULL)
    END IF

  END WHILE

  CALL fgl_window_close("w_supplies_popup")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_supplies_id
  ELSE
    LET i = arr_curr()
    LET rv_supplies_id = l_supplies_arr[i].suppl_id
  END IF

  RETURN rv_supplies_id
END FUNCTION




###################################################
# FUNCTION select_supplies()
#
# 
#
# RETURN supplies_id or NULL
###################################################
FUNCTION select_supplies()
  DEFINE 
    l_supplies_id   LIKE supplies.suppl_id,
    l_supplies_line RECORD LIKE supplies_line.*
  LET l_supplies_id = supplies_popup(l_supplies_id,"suppl_id",0,NULL)
  IF exist(l_supplies_id) THEN
    RETURN l_supplies_id
  END IF
  RETURN NULL
END FUNCTION







###################################################
# FUNCTION show_supplies_detail(p_supplies_id)
#
# 
#
# RETURN NONE
###################################################
FUNCTION show_supplies_detail(p_supplies_id)
  DEFINE 
    p_supplies_id   LIKE supplies.suppl_id,
    l_supl_line     ARRAY [20] OF t_supp_line,
    i              INTEGER

  DECLARE c_get_supplies_detail CURSOR FOR
    SELECT stock_item.stock_id,
           supplies_line.quantity,
           stock_item.item_cost,
           stock_item.item_desc
      FROM stock_item,supplies_line,supplies
      WHERE supplies.suppl_id = p_supplies_id
        AND supplies_line.suppl_id = supplies.suppl_id
        AND stock_item.stock_id = supplies_line.stock_id
  LET i = 1
  FOREACH c_get_supplies_detail INTO g_suppl_lines[i].*
    LET i = i + 1
    IF i > 20 THEN
      EXIT FOREACH
    END IF
  END FOREACH
  LET i = i - 1

		If i > 0 THEN
			CALL g_suppl_lines.resize(i) -- resize dynamic array to remove last dirty element
		END IF    

  DISPLAY ARRAY g_suppl_lines TO sc_supl_lines.* WITHOUT SCROLL

END FUNCTION



###################################################
# FUNCTION supplies_show_temp_detail()
#
# 
#
# RETURN NONE
###################################################
FUNCTION supplies_show_temp_detail()
  DEFINE i INTEGER

  LET i = load_supplies_temp_detail()

  CALL set_count(i)

  DISPLAY ARRAY g_suppl_lines TO sc_suppl_lines.* WITHOUT SCROLL
END FUNCTION



###################################################
# FUNCTION load_supplies_temp_detail()
#
# 
#
# RETURN NONE
###################################################
FUNCTION load_supplies_temp_detail()
  DEFINE  i INTEGER
  DECLARE c_supl_detail CURSOR FOR
    SELECT stock_item.stock_id,
           supplies_line_temp.quantity,
           stock_item.item_cost,
           stock_item.item_desc
      FROM supplies_line_temp,
           stock_item
      WHERE stock_item.stock_id = supplies_line_temp.stock_id
  LET i = 1
  FOREACH c_supl_detail INTO g_suppl_lines[i].*
    LET i = i + 1
  END FOREACH
  LET i = i - 1
  
		If i > 0 THEN
			CALL g_suppl_lines.resize(i) -- resize dynamic array to remove last dirty element
		END IF  

  RETURN i
END FUNCTION



###################################################
# FUNCTION supplies_delete(p_suppl_id)
#
# 
#
# RETURN NONE
###################################################
FUNCTION supplies_delete(p_suppl_id)
  DEFINE p_suppl_id LIKE supplies.suppl_id

  IF NOT yes_no(get_str(2548),get_str(2549)) THEN
    RETURN FALSE
  END IF

  BEGIN WORK
    DELETE FROM supplies
      WHERE suppl_id = p_suppl_id

    DELETE FROM supplies_line
      WHERE suppl_id = p_suppl_id
  COMMIT WORK
END FUNCTION




