##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the payment method 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                      RETURN
# get_pay_method_id(pay_method_name)               get payment method id from name                   l_pay_method_id
# get_pay_method_name(pay_method_id)               Get pay_method_id from name                       l_pay_method_name
# get_pay_method_desc(p_pay_method_id)             Get Pay method description from id                l_pay_method_rec.pay_method_desc
# get_pay_method_rec(p_pay_method_id)              Get the pay_method record from pa_method_id       l_pay_method.*
# payment_method_combo_list(cb_field_name)         Populate payment_method combo list from database  NONE
# pay_method_popup()                               Payment Method selection window                   p_pay_method_id
# pay_method_create()                              Create a new payment method record                NULL
# pay_method_delete(p_pay_method_id)               Delete a payment method record                    NONE
# pay_method_edit(p_pay_method_id)                 Edit Payment method record                        NONE
# pay_method_input(p_pay_method)                   Input payment method details (edit/create)        l_pay_method.*
# pay_method_view(p_pay_method_rec)                View payment method record by ID in window-form   NONE
# pay_method_view_by_rec(p_pay_method_rec)         View payment method record in window-form         NONE
# grid_header_pay_method_scroll()                  Populate payment method grid headers              NONE
############################################################################################################


######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

#########################################################
# FUNCTION get_pay_method_id(pay_method_name)
#
# get payment method id from name
#
# RETURN l_pay_method_id
#########################################################
FUNCTION get_pay_method_id(pay_method_name)
  DEFINE 
    pay_method_name LIKE pay_methods.pay_method_name,
    l_pay_method_id LIKE pay_methods.pay_method_id

  SELECT pay_methods.pay_method_id
    INTO l_pay_method_id
    FROM pay_methods
    WHERE pay_methods.pay_method_name = pay_method_name

  RETURN l_pay_method_id
END FUNCTION

#########################################################
# FUNCTION get_pay_method_name(pay_method_id)
#
# Get pay_method_id from name
#
# RETURN l_pay_method_name
#########################################################
FUNCTION get_pay_method_name(pay_method_id)
  DEFINE pay_method_id LIKE pay_methods.pay_method_id
  DEFINE l_pay_method_name LIKE pay_methods.pay_method_name

  SELECT pay_methods.pay_method_name
    INTO l_pay_method_name
    FROM pay_methods
    WHERE pay_methods.pay_method_id = pay_method_id

  RETURN l_pay_method_name
END FUNCTION


#########################################################
# FUNCTION get_pay_method_desc(p_pay_method_id)
#
# Get Pay method description from id
#
# RETURN l_pay_method_rec.pay_method_desc
#########################################################
FUNCTION get_pay_method_desc(p_pay_method_id)
  DEFINE l_pay_method_rec RECORD LIKE pay_methods.*
  DEFINE p_pay_method_id LIKE pay_methods.pay_method_id

  CALL get_pay_method_rec(p_pay_method_id)
    RETURNING l_pay_method_rec.*

  RETURN l_pay_method_rec.pay_method_desc
END FUNCTION


######################################################
# FUNCTION get_pay_method_rec(p_pay_method_id)
#
# Get the pay_method record from pa_method_id
#
# RETURN l_pay_methods.*
######################################################
FUNCTION get_pay_method_rec(p_pay_method_id)
  DEFINE p_pay_method_id LIKE pay_methods.pay_method_id
  DEFINE l_pay_methods RECORD LIKE pay_methods.*

  SELECT pay_methods.*
    INTO l_pay_methods.*
    FROM pay_methods
    WHERE pay_methods.pay_method_id = p_pay_method_id

  RETURN l_pay_methods.*
END FUNCTION


###################################################################################
# FUNCTION payment_method_combo_list(cb_field_name)
#
# Populate payment_method combo list from database
#
# RETURN NONE
###################################################################################
FUNCTION payment_method_combo_list(cb_field_name)
  DEFINE 
    l_pay_method_arr DYNAMIC ARRAY OF t_pay_method_name_rec,
    row_count        INTEGER,
    current_row      INTEGER,
    cb_field_name    VARCHAR(20)   --form field name for the country combo list field

  DECLARE c_pay_method_scroll2 CURSOR FOR 
    SELECT pay_methods.pay_method_name 
      FROM pay_methods

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_pay_method_scroll2 INTO l_pay_method_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_pay_method_arr[row_count].pay_method_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1

		If row_count > 0 THEN
			CALL l_pay_method_arr.resize(row_count)  --remove the last item which has no data
		END IF

END FUNCTION
