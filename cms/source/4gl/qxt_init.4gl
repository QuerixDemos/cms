##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Functions to initialise variables
#
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# init_data()                                     Initialise tools library variables                         NONE
# init_gui_path()                                 Initialisee gui client path                                NONE
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"


#########################################################################################################
# FUNCTION init_tools_data()
#
# Initialise tools library variables
#
# RETURN NONE
######################################################################################################### 
FUNCTION init_tools_data()
  DEFINE
    ret         SMALLINT,
    local_debug SMALLINT

  LET local_debug = FALSE                                 -- Turn Debug on/off
	IF fgl_getenv("LYCIA_DB_DRIVER") = "informix" THEN
		LET qxt_settings.dbWildcard = "%"  --%=informix *=others
	ELSE
		LET qxt_settings.dbWildcard = "*"  --%=informix *=others	
	END IF

	
	#DISPLAY "In FUNCTION init_tools_data()"
  #Init static array sizes
  #LET qxt_app_string_array_size = 3000
  #LET qxt_tool_string_array_size = 1000

  #Note: Please keep the order of the first six items as they are !


  #############################
  # Default language english
  #############################
  LET qxt_settings.language_id = 1                            -- 1 English = default NOTE: must be the first specified value in the init_data()


  #############################
  # Process main cfg file name import
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_cfg_import(",get_main_cfg_filename() CLIPPED, ")"
  END IF
  #DISPLAY "CALL process_cfg_import(get_main_cfg_filename()) "
	#CALL fgl_winmessage("call process_cfg_import()","call process_cfg_import","info")
  CALL process_cfg_import(get_main_cfg_filename())        -- 2 Import main cfg file
	{
  #############################
  # GUI Path
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL init_gui_path()"
  END IF

  IF fgl_fglgui() THEN
    CALL init_gui_path()                                                             --initialise common local & server paths
  END IF
}

  #############################
  # Process Language cfg import
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_language_cfg_import()"
  END IF

  CALL process_language_cfg_import(NULL)


  #############################
  # Init Language List
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL init_language_list()"
  END IF

  IF get_language_list_source() = 0 THEN  --file
    CALL init_language_list(get_language_list_filename())
  ELSE  --DB
    CALL init_language_list(get_language_list_table_name())
  END IF


  #############################
  # Process String cfg import
  #############################

  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_string_cfg_import(NULL)"
  END IF
	#DISPLAY "CALL process_string_cfg_import(NULL)"
  CALL process_string_cfg_import(NULL)


  #############################
  # Init 'Tool' String
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL qxt_string_tool_init(NULL)"
  END IF
	
  CALL qxt_string_tool_init(NULL)               -- Initialises / Import data for tool lib strings


  #############################
  # Init 'Icon' 
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_icon_cfg_import(NULL)"
  END IF

  CALL process_icon_cfg_import(NULL)               -- Initialises / Import data for icon lib cfg

  #############################
  # Init 'Toolbar' 
  #############################
  IF local_debug THEN
    DISPLAY "init_tools_data() - CALL process_toolbar_cfg_import(NULL)"
  END IF

  CALL process_toolbar_cfg_import(NULL)               -- Initialises / Import data for toolbar lib cfg

  IF get_toolbar_lib_info() <> "DB" THEN
    IF local_debug THEN
      DISPLAY "init_tools_data() - CALL process_toolbar_init(NULL)"
    END IF

    CALL process_toolbar_init(NULL,NULL)
  END IF

  #############################
  # Set/Get environment user name
  #############################
  IF local_debug THEN
    DISPLAY "qxt_user name = ",qxt_user_name
  END IF

  LET qxt_user_name = fgl_username()

  
  #############################
  # Init toggle switch
  #############################
  IF local_debug THEN
    DISPLAY "set_toggle_switch(1)"
  END IF

  CALL set_toggle_switch(1)                                -- initialise the global switch toggle.. i.e. used for asc/desc sort switch


END FUNCTION


#########################################################################################################
# FUNCTION init_gui_path()
#
# Initialisee gui client path
#
# RETURN NONE
######################################################################################################### 
FUNCTION init_gui_path()
  #DEFINE temp_path VARCHAR(200) - is global
  define 
    r smallint,
    app_temp_dir,phoenix_cache, chimera_cache VARCHAR(50),
    local_debug SMALLINT

  LET local_debug = FALSE

  LET app_temp_dir = "temp"
  LET phoenix_cache = "Phoenix_cache"
  LET chimera_cache = "Chimera_cache"

  #Client Cache and Working Directory ##############################################################################
  LET qxt_client_temp_directory = fgl_get_property("gui","system.file.client_temp","")

  #check if directory exists on client system
  IF qxt_client_temp_directory IS NULL THEN
    LET qxt_err_msg = "Client Temp Directory not found" , "\n" , get_str_tool(46) CLIPPED, "\n" , get_str_tool(47)
    CALL fgl_winmessage(get_str_tool(44),qxt_err_msg,"error")
  END IF


  ############################################################################################
  # Set the server temp directory ############################################################
  LET qxt_server_temp_directory = fgl_getenv("TEMP")

  IF qxt_server_temp_directory IS NULL THEN
    LET qxt_server_temp_directory = "temp"
    #CALL fgl_winmessage("Missing temp path in your GUI appliation server environment", "This server application could not read the enviroment variable TEMP.\n Please set your TEMP=<path> in your gui server environment","error")
  END IF


  ############################################################################################
  # Set the application temp directory #######################################################
  LET qxt_server_client_user_temp_directory = qxt_server_temp_directory , "/" , qxt_user_name

  IF fgl_test("e",qxt_server_client_user_temp_directory)= 0 THEN
    LET r = fgl_mkdir(qxt_server_client_user_temp_directory)  -- --r-- 0 =failure  1=success

    IF r = 0 THEN
      LET r = fgl_mkdir(qxt_server_client_user_temp_directory)
    END IF
  END IF



  ############################################################################################
  # Just some debug output  ##################################################################
  IF local_debug THEN
    DISPLAY "qxt_server_client_user_temp_directory= ", qxt_server_client_user_temp_directory
    DISPLAY "qxt_client_temp_directory= ", qxt_client_temp_directory
  END IF

END FUNCTION







