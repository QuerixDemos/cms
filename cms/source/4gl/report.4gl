##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Admin Functions to manage lookup tables etc...
##################################################################################################################
#
# FUNCTIONS:                                    DESCRIPTION:                                     RETURN:
# report_main()                                 Main report Menu                                 NONE
# start_activity_report(dev)                             Start report to screen                           dev
# report_all_activities()                       output all activities to report                  NONE
# get_temp_op(p_str)                            ??                                               rv  (CHAR)
# report_activity_selection()                   operator can select report filter                NONE
# REPORT activity(p_activity)                   REPORT create report on activity                 NONE
# print_invoice(p_invoice_id)                   print an invoice                                 NONE
# REPORT invoice_report(p_inv_line)             REPORT Invoice Report                            NONE
# report_output()                               operator choice between screen or printer print      dev (VARCHAR(20))
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

######################################################
# FUNCTION report_main() 
#
# RETURN NONE
######################################################
FUNCTION report_main(argModule) 
  DEFINE 
    l_invoice_id LIKE invoice.invoice_id,
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100), --i.e. used for rinmenu strings
		argModule STRING
	
		CALL ui.Interface.setImage("qx://application/icon16/money/invoice/invoice-print.png")
		CALL ui.Interface.setText("Print Invoice")
			
		#CALL fgl_winmessage("argModule",argModule,"info")
		CASE argModule
			WHEN "invoice"
      	CALL set_help_id(703)
      	LET l_invoice_id = select_invoices()
      	IF l_invoice_id IS NOT NULL THEN
        	CALL print_invoice(l_invoice_id)
      	END IF			
				EXIT PROGRAM  --exit as we only use the print invoice module0
		END CASE



		
  #OPEN WINDOW w_report_main AT 1,1 WITH 24 ROWS, 80 COLUMNS

      #need to assign strings to temp_strings - MENU does not support functions for strings
      LET menu_str_arr1[1] = get_str(2221)
      LET menu_str_arr2[1] = get_str(2222)
      LET menu_str_arr1[2] = get_str(2223)
      LET menu_str_arr2[2] = get_str(2224)
      LET menu_str_arr1[3] = get_str(2225)
      LET menu_str_arr2[3] = get_str(2226)
      LET menu_str_arr1[4] = get_str(2227)
      LET menu_str_arr2[4] = get_str(2228)
      LET menu_str_arr1[5] = get_str(2229)
      LET menu_str_arr2[5] = get_str(2230)
      LET menu_str_arr1[6] = get_str(2231)
      LET menu_str_arr2[6] = get_str(2232)
      LET menu_str_arr1[7] = get_str(2489)
      LET menu_str_arr2[7] = get_str(2494)

  MENU "Report"
    BEFORE MENU
      CALL set_help_id(700)
      CALL publish_toolbar("Report",0)

    COMMAND KEY (F701,"S") menu_str_arr1[1]  menu_str_arr2[1]  HELP 701 --"Select" "Select activity based reports" HELP 701
      CALL set_help_id(701)
      CALL report_activity_selection()

    COMMAND KEY (F702,"A") menu_str_arr1[2]  menu_str_arr2[2]  HELP 702 -- "All" "All activities" HELP 702
      CALL set_help_id(702)
      CALL report_all_activities()

    COMMAND KEY (F703,"I") menu_str_arr1[3]  menu_str_arr2[3]  HELP 703 -- "Invoice" "Print Invoices" HELP 703
      CALL set_help_id(703)
      LET l_invoice_id = select_invoices()
      IF l_invoice_id IS NOT NULL THEN
        CALL print_invoice(l_invoice_id)
      END IF
      #CALL invoice_popup(NULL,"invoice_id",1)

    COMMAND KEY (F704,"W") menu_str_arr1[7]  menu_str_arr2[7]  HELP 705 -- "Warehouse" "Warehouse Report" HELP 705
      CALL warehouse_main()

    COMMAND KEY (F12,"Q")  menu_str_arr1[6]  menu_str_arr2[6]  HELP 704 -- "Quit" "Return to previous screen" HELP 704
      EXIT MENU

    ON ACTION actExit --COMMAND KEY (F12,"Q")  menu_str_arr1[6]  menu_str_arr2[6]  HELP 704 -- "Quit" "Return to previous screen" HELP 704
      EXIT MENU


#    ON ACTION ("actExit") 
#			EXIT MENU
        
  END MENU

  #clean up toolbar
  CALL publish_toolbar("Report",0)

  #CLOSE WINDOW w_report_main

END FUNCTION



######################################################
# FUNCTION start_activity_report(dev)
#
# Start activity report
#
# RETURN dev
######################################################
# branch this out, to allow for later destinatrion selection
FUNCTION start_activity_report(dev)
  DEFINE dev VARCHAR(20)

  #CALL report_output()
  #  RETURNING dev

  IF dev IS NULL THEN
    CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN NULL
  END IF
  IF dev = "dde-excel" THEN
    RETURN dev
  END IF

  IF NOT fgl_fglgui() THEN  --text mode client
    CASE dev
      WHEN "printer"
        START REPORT activity_report TO PRINTER
      WHEN "screen"
        START REPORT activity_report TO SCREEN
    END CASE
  ELSE  --gui
    START REPORT activity_report TO PIPE dev
  END IF
  RETURN dev
END FUNCTION


######################################################
# FUNCTION start_invoice_report(dev)
#
# Start activity report
#
# RETURN dev
######################################################
# branch this out, to allow for later destinatrion selection
FUNCTION start_invoice_report(dev)
  DEFINE dev VARCHAR(20)

  #CALL report_output()
  #  RETURNING dev

  IF dev IS NULL THEN
    CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN NULL
  END IF
  IF dev = "dde-excel" THEN
    RETURN dev
  END IF

  IF NOT fgl_fglgui() THEN  --text mode client
    CASE dev
      WHEN "printer"
        START REPORT invoice_report TO PRINTER
      WHEN "screen"
        START REPORT invoice_report TO SCREEN
    END CASE
  ELSE  --gui
    START REPORT invoice_report TO PIPE dev
  END IF
  RETURN dev
END FUNCTION


######################################################
# FUNCTION start_supplies_report(dev)
#
# Start supplies report
#
# RETURN dev
######################################################
# branch this out, to allow for later destinatrion selection
FUNCTION start_supplies_report(dev)
  DEFINE dev VARCHAR(20)

  #CALL report_output()
  #  RETURNING dev

  IF dev IS NULL THEN
    CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN NULL
  END IF
  IF dev = "dde-excel" THEN
    RETURN dev
  END IF

  IF NOT fgl_fglgui() THEN  --text mode client
    CASE dev
      WHEN "printer"
        START REPORT supplies_report TO PRINTER
      WHEN "screen"
        START REPORT supplies_report TO SCREEN
    END CASE
  ELSE  --gui
    START REPORT supplies_report TO PIPE dev
  END IF
  RETURN dev
END FUNCTION


######################################################
# FUNCTION report_all_activities()
######################################################
FUNCTION report_all_activities()
  DEFINE 
    l_activity   RECORD LIKE activity.*,
    dev          VARCHAR(20),
    rep_method   SMALLINT,  --0=classic 1 = DDE 2 = XML  3 - HTML
    line_no      SMALLINT,
    doc_name     VARCHAR(200),
    client_doc_name_with_path  VARCHAR(250),
    ret          CHAR(3)

  LOCATE l_activity.long_desc IN MEMORY

  DECLARE  c_all_act CURSOR FOR
    SELECT activity.* 
      FROM activity

  ORDER BY activity.act_type, activity.open_date, activity.activity_id

  CALL report_output() RETURNING dev, rep_method

  #check if device was specified, if not - exit menu
  IF dev IS NULL THEN
    #CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN
  END IF

  IF dev THEN
	CALL fgl_report_type("pipe1",dev)
	START REPORT activity_report TO PIPE "pipe1"
      FOREACH c_all_act INTO l_activity.*
        OUTPUT TO REPORT activity_report (l_activity.*)
      END FOREACH
	FINISH REPORT activity_report   
  ELSE
	CALL makebirt("c_all_act")
  END IF
{
  #Only call start_activity_report(dev) if a classic 4gl report output was choosen
  CASE rep_method 
    WHEN 0  --classic 4gl report
      CALL start_activity_report(dev) RETURNING dev

      FOREACH c_all_act INTO l_activity.*
        OUTPUT TO REPORT activity_report (l_activity.*)
      END FOREACH

      FINISH REPORT activity_report

    WHEN 1  --DDE MS-Excel Report

      CASE dev
        WHEN "dde-excel-1"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "cms_activity_report_01.xls"

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            #RETURN
          END IF

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          LET line_no = 5
          FOREACH c_all_act INTO l_activity.*
            CALL dde_transfer_excel_cell(line_no,1,l_activity.activity_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,2,l_activity.open_date CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,3,l_activity.close_date CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,4,l_activity.contact_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,5,get_contact_name(l_activity.contact_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,6,get_operator_name(l_activity.operator_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,7,l_activity.short_desc CLIPPED,0)
            #CALL dde_transfer_excel_cell(line_no,8,l_activity.long_desc CLIPPED,0)
            LET line_no = line_no+1  --increment line number by one
          END FOREACH

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()


        WHEN "dde-excel-2"
          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF
          #Assign the document file name
          LET doc_name = "cms_activity_report_02.xls"

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            #RETURN
          END IF


          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off


          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on


          LET line_no = 5
          FOREACH c_all_act INTO l_activity.*
            CALL dde_transfer_excel_cell(line_no,1,get_str(671),6)
            CALL dde_transfer_excel_cell(line_no,2,l_activity.activity_id,0)

            CALL dde_transfer_excel_cell(line_no+1,1,get_str(691),6)
            CALL dde_transfer_excel_cell(line_no+1,2,l_activity.open_date,0)


            CALL dde_transfer_excel_cell(line_no+2,1,get_str(692),6)
            CALL dde_transfer_excel_cell(line_no+2,2,l_activity.close_date,0)

            CALL dde_transfer_excel_cell(line_no,4,get_str(330),6)
            CALL dde_transfer_excel_cell(line_no,5,l_activity.contact_id,0)

            CALL dde_transfer_excel_cell(line_no,4,get_str(332),6)
            CALL dde_transfer_excel_cell(line_no,5,get_contact_name(l_activity.contact_id),0)

            CALL dde_transfer_excel_cell(line_no+1,4,get_str(1020),6)
            CALL dde_transfer_excel_cell(line_no+1,5,get_operator_name(l_activity.operator_id),0)

            CALL dde_transfer_excel_cell(line_no+3,1,get_str(1861),6)
            CALL dde_transfer_excel_cell(line_no+3,2,l_activity.short_desc,0)
            #CALL dde_transfer_excel_cell(line_no,8,l_activity.long_desc CLIPPED,0)
            LET line_no = line_no+5  --increment line number by 5
          END FOREACH

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()


      END CASE
  END CASE
}
  FREE l_activity.long_desc

END FUNCTION




######################################################
# FUNCTION get_temp_op(p_str)
######################################################    
FUNCTION get_temp_op(p_str)
  DEFINE 
    p_str VARCHAR(10),
    rv    CHAR

  CASE p_str 
    WHEN "Before"
      LET rv = "<"
    WHEN "On"
      LET rv = "="
    WHEN "After"
      LET rv = ">"
  END CASE

  RETURN rv
END FUNCTION


######################################################
# FUNCTION report_activity_selection()
######################################################   
FUNCTION report_activity_selection()
  DEFINE 
    form_rec     OF t_acc_filter_rec,
    tmp_id       INTEGER,
    temp_op      CHAR,
    l_activity   RECORD LIKE activity.*,
    sql_stmt     CHAR(1024),
    dev          VARCHAR(20),
    rep_method   SMALLINT,  --0=classic 1 = DDE 2 = XML  3 - HTML
    line_no      SMALLINT,
    doc_name     VARCHAR(200),
    client_doc_name_with_path VARCHAR(250),
    ret          CHAR(3),
    SessionID    INTEGER
  LET form_rec.cdate_sel = "On"
  LET form_rec.odate_sel = "On"
  LET form_rec.f_name = 1
  LET form_rec.stat = 0
  LET form_rec.id_type = 1

  LOCATE l_activity.long_desc IN MEMORY

	CALL fgl_window_open("w_act_report", 2,2, get_form_path("f_report_act_l2"), FALSE)
	CALL populate_report_act_form_labels_g()

  LET int_flag = FALSE

  INPUT BY NAME form_rec.* WITHOUT DEFAULTS HELP 701
    BEFORE INPUT
    CALL activity_type_combo_list("type_name")

    ON KEY (F10)
      IF infield(obj_name) THEN
        IF form_rec.id_type = 1 THEN
          LET tmp_id = contact_popup(NULL,NULL,0)
          LET form_rec.obj_name = get_contact_name(tmp_id)
        ELSE
          LET tmp_id = company_popup(NULL,NULL,NULL)
          LET form_rec.obj_name = get_company_name(tmp_id)
        END IF
        DISPLAY BY NAME form_rec.obj_name
      END IF
      IF infield(type_name) THEN
        LET form_rec.type_name = activity_type_popup(get_activity_type_id(form_rec.type_name),NULL,0) 
        DISPLAY form_rec.type_name TO type_name 
      END IF
  END INPUT

  CALL fgl_window_close("w_act_report")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF

  LET sql_stmt = "SELECT activity.* FROM activity, activity_type, contact"    

  IF form_rec.f_name AND form_rec.id_type = 2 THEN     
    LET sql_stmt = sql_stmt CLIPPED, " ,company "   
  END IF    

  LET sql_stmt = sql_stmt CLIPPED, " WHERE activity.act_type = activity_type.type_id ",
                                   " AND contact.cont_id = activity.contact_id "
  IF form_rec.f_type THEN
      LET sql_stmt = sql_stmt CLIPPED, " AND activity_type.atype_name = '", form_rec.type_name CLIPPED, "' "
  END IF

  IF form_rec.f_name THEN
    IF form_rec.id_type = 2 THEN
      LET sql_stmt = sql_stmt CLIPPED,
	" AND contact.cont_org = company.comp_id ",
	" AND company.comp_name = '",form_rec.obj_name CLIPPED,"' "
    ELSE 
      LET sql_stmt = sql_stmt CLIPPED,
	" AND contact.cont_name = '", form_rec.obj_name CLIPPED, "' "
    END IF
  END IF

  IF form_rec.f_status THEN
    IF NOT form_rec.stat THEN
      LET sql_stmt = sql_stmt CLIPPED,
        " AND activity.close_date IS NULL "
    ELSE
      LET sql_stmt = sql_stmt CLIPPED,
        " AND activity.close_date IS NOT NULL "
    END IF
  END IF

  IF form_rec.f_odate THEN
    LET temp_op = get_temp_op(form_rec.odate_sel)
    LET sql_stmt = sql_stmt CLIPPED,
      " AND activity.open_date ", temp_op, " '", form_rec.open_date, "'"
  END IF

  IF form_rec.f_cdate THEN
    LET temp_op = get_temp_op(form_rec.cdate_sel)
    LET sql_stmt = sql_stmt CLIPPED,
      " AND activity.close_date ", temp_op, " '", form_rec.close_date, "'"
  END IF

  LET sql_stmt = sql_stmt CLIPPED, " ORDER BY activity.act_type, activity.open_date"
  PREPARE p1 FROM sql_stmt

  DECLARE c_act_rep CURSOR FOR p1

  #user chooses report dev & type/method
  CALL report_output() RETURNING dev, rep_method

  #check if device was specified, if not - exit menu
  IF dev IS NULL THEN
    #CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN
  END IF

  IF dev THEN
	CALL fgl_report_type("pipe1",dev)
	START REPORT activity_report TO PIPE "pipe1"
      FOREACH c_act_rep INTO l_activity.*
        OUTPUT TO REPORT activity_report (l_activity.*)
      END FOREACH
	FINISH REPORT activity_report   
  ELSE
    SELECT DBINFO('sessionid') INTO SessionID from systables where tabid=1
    FOREACH c_act_rep INTO l_activity.*
      INSERT INTO activity_birt VALUES (SessionID,l_activity.activity_id,
      											  l_activity.open_date,
      											  l_activity.close_date,
      											  l_activity.contact_id,
      											  l_activity.comp_id,
      											  l_activity.operator_id,
      											  l_activity.act_type,
      											  l_activity.long_desc,
      											  l_activity.short_desc,
      											  l_activity.a_owner,
      											  l_activity.priority)
    END FOREACH
	CALL makebirt("c_act_rep")
	DELETE FROM activity_birt WHERE sesion_id = SessionID
  END IF   
  #Only call start_activity_report(dev) if a classic 4gl report output was choosen
{
  CASE rep_method 
    WHEN 0  --classic 4gl report
      CALL start_activity_report(dev) RETURNING dev

      FOREACH c_act_rep INTO l_activity.*
        OUTPUT TO REPORT activity_report (l_activity.*)
      END FOREACH

      FINISH REPORT activity_report

    WHEN 1  --DDE MS-Excel Report

      CASE dev
        WHEN "dde-excel-1"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "cms_activity_report_01.xls"

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            RETURN
          END IF

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          LET line_no = 5
          FOREACH c_act_rep INTO l_activity.*
            CALL dde_transfer_excel_cell(line_no,1,l_activity.activity_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,2,l_activity.open_date CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,3,l_activity.close_date CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,4,l_activity.contact_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,5,get_contact_name(l_activity.contact_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,6,get_operator_name(l_activity.operator_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,7,l_activity.short_desc CLIPPED,0)
            #CALL dde_transfer_excel_cell(line_no,8,l_activity.long_desc CLIPPED,0)
            LET line_no = line_no+1  --increment line number by one
          END FOREACH

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF
          #Finnish/Close all DDE connections
          CALL DDEFinishAll()
	call fgl_message_box("???")
        WHEN "dde-excel-2"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "cms_activity_report_02.xls"

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            RETURN
          END IF
          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on


          LET line_no = 5
          FOREACH c_act_rep INTO l_activity.*
            CALL dde_transfer_excel_cell(line_no,1,get_str(671),6)
            CALL dde_transfer_excel_cell(line_no,2,l_activity.activity_id,0)

            CALL dde_transfer_excel_cell(line_no+1,1,get_str(691),6)
            CALL dde_transfer_excel_cell(line_no+1,2,l_activity.open_date,0)


            CALL dde_transfer_excel_cell(line_no+2,1,get_str(692),6)
            CALL dde_transfer_excel_cell(line_no+2,2,l_activity.close_date,0)

            CALL dde_transfer_excel_cell(line_no,4,get_str(330),6)
            CALL dde_transfer_excel_cell(line_no,5,l_activity.contact_id,0)

            CALL dde_transfer_excel_cell(line_no,4,get_str(332),6)
            CALL dde_transfer_excel_cell(line_no,5,get_contact_name(l_activity.contact_id),0)

            CALL dde_transfer_excel_cell(line_no+1,4,get_str(1020),6)
            CALL dde_transfer_excel_cell(line_no+1,5,get_operator_name(l_activity.operator_id),0)

            CALL dde_transfer_excel_cell(line_no+3,1,get_str(1861),6)
            CALL dde_transfer_excel_cell(line_no+3,2,l_activity.short_desc,0)
            #CALL dde_transfer_excel_cell(line_no,8,l_activity.long_desc CLIPPED,0)
            LET line_no = line_no+5  --increment line number by 5
          END FOREACH

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()

      END CASE
  END CASE
}
  FREE l_activity.long_desc

END FUNCTION

{
###################################
# REPORT activity(p_activity)
#
# Report Definition based on activities
#
# RETURN NONE
###################################
REPORT activity_report(p_activity)
  DEFINE 
    l_activity_type RECORD LIKE activity_type.*,
    p_activity      RECORD LIKE activity.*,
    l_activity      RECORD LIKE activity.*,
    l_company       RECORD LIKE company.*,
    l_contact       RECORD LIKE contact.*,
    activity_cnt    INTEGER

  FORMAT

    ON EVERY ROW       
      LET activity_cnt = activity_cnt + 1

      CALL web_get_contact_rec(p_activity.contact_id) 
        RETURNING l_contact.* 
    ##dde stuff
  CALL dde_transfer_excel_cell(LINENO,1,"Contact: " CLIPPED,3)
  CALL dde_transfer_excel_cell(LINENO,2,l_contact.cont_name CLIPPED,3)
  CALL dde_transfer_excel_cell(LINENO,3,"Description: " CLIPPED,3)
  CALL dde_transfer_excel_cell(LINENO,4, p_activity.short_desc CLIPPED,3)
  CALL dde_transfer_excel_cell(LINENO,5,p_activity.long_desc CLIPPED,3)




END REPORT
}


###################################
# REPORT activity(p_activity)
#
# Report Definition based on activities
#
# RETURN NONE
###################################
REPORT activity_report(p_activity)
   DEFINE 
    l_activity_type RECORD LIKE activity_type.*,
    p_activity      RECORD LIKE activity.*,
    l_activity      RECORD LIKE activity.*,
    l_company       RECORD LIKE company.*,
    l_contact       RECORD LIKE contact.*,
    activity_cnt    INTEGER

  FORMAT
    PAGE HEADER
      PRINT get_str(2270) --"Contact Activity Report"

    PAGE TRAILER
      PRINT COLUMN 40, get_str(2271), COLUMN 50, PAGENO  --Page

    ON EVERY ROW       
      LET activity_cnt = activity_cnt + 1

      CALL web_get_contact_rec(p_activity.contact_id) 
        RETURNING l_contact.* 

      PRINT get_str(2272), COLUMN 15, p_activity.activity_id  --Activity #:
      PRINT get_str(2273), COLUMN 15, p_activity.open_date    --Open Date:      
      SKIP 2 LINES
      PRINT COLUMN 10, get_str(2274), l_contact.cont_name
      PRINT COLUMN 10, get_str(2275), COLUMN 25, p_activity.short_desc
      PRINT COLUMN 25, p_activity.long_desc WORDWRAP RIGHT MARGIN 70


    BEFORE GROUP OF p_activity.act_type
      NEED 2 LINES    
      LET l_activity_type.atype_name =  get_activity_type_name(p_activity.act_type)         
      PRINT COLUMN 10, get_str(2276), l_activity_type.atype_name  --"Activity: "
      PRINT "-----------------------------------------"

    BEFORE GROUP OF p_activity.contact_id
      LET activity_cnt = 0
      NEED 3 LINES

      CALL web_get_contact_rec(p_activity.contact_id) 
        RETURNING l_contact.* 

      CALL get_company_rec(l_contact.cont_org) 
        RETURNING l_company.* 

      PRINT get_str(2277), COLUMN 12, l_contact.cont_fname, " ", l_contact.cont_lname
      PRINT get_str(2278), COLUMN 12, l_company.comp_name
      PRINT get_str(2279), COLUMN 12, l_company.comp_country

    AFTER GROUP OF p_activity.contact_id
      NEED 2 LINES
      PRINT get_str(2280), activity_cnt  
      PRINT "--------------------------------------------"    
      SKIP 2 LINES

END REPORT



###################################################
# FUNCTION report_output()
#
# operator choice between screen or printer print
#
# RETURN dev (VARCHAR(20))
###################################################
FUNCTION report_output()
  DEFINE 
    dev VARCHAR(20),
    rep_method SMALLINT,  --0=classic 1 = DDE 2 = XML  3 - HTML
    do_exit INTEGER,
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings
      #need to assign strings to temp_strings - MENU does not support functions for strings
      LET menu_str_arr1[1] = get_str(2291)
      LET menu_str_arr2[1] = get_str(2292)
      LET menu_str_arr1[2] = get_str(2293)
      LET menu_str_arr2[2] = get_str(2294)
      LET menu_str_arr1[3] = get_str(2295)
      LET menu_str_arr2[3] = get_str(2296)
      LET menu_str_arr1[4] = get_str(2297)
      LET menu_str_arr2[4] = get_str(2298)
      LET menu_str_arr1[5] = get_str(2299)
      LET menu_str_arr2[5] = get_str(2300)
      LET menu_str_arr1[6] = get_str(2301)
      LET menu_str_arr2[6] = get_str(2302)
      LET menu_str_arr1[7] = get_str(2303)
      LET menu_str_arr2[7] = get_str(2304)
      LET menu_str_arr1[8] = get_str(2305)
      LET menu_str_arr2[8] = get_str(2306)
      LET menu_str_arr1[9] = get_str(2307)
      LET menu_str_arr2[9] = get_str(2308)

  MENU "Rep_Output"
    BEFORE MENU
      CALL set_help_id(611)
      #DDE does not work in text or Java clients because of platform independent aspects
      #fgl_getuitype() returns the client type - the windows client phoenix returns wtk
      #this is a D4GL function call
{
      HIDE OPTION menu_str_arr1[3]
      HIDE OPTION menu_str_arr1[4]
      IF fgl_getuitype() = "WTK" THEN
        SHOW OPTION menu_str_arr1[3]
        SHOW OPTION menu_str_arr1[4]
      END IF
}
      CALL publish_toolbar("PrintDevice",0)

    COMMAND KEY (F621,"S") menu_str_arr1[1] menu_str_arr2[1]  HELP 791  --"Screen" "Print invoice to screen"
      LET dev = "screen"  
      LET rep_method = 0  --classic 4gl report
      EXIT MENU

    COMMAND KEY (F622,"P") menu_str_arr1[2] menu_str_arr2[2]  HELP 792  --"Printer" "Print invoice to printer"
      LET dev = "printer"
      LET rep_method = 0  --classic 4gl report
      EXIT MENU

    COMMAND KEY (F625,"H") "New Tab" "New Tab"--"New Tab" "html"
      LET dev = "new_window"
      LET rep_method = 0  --classic 4gl report
      EXIT MENU

    COMMAND KEY (F626,"D") "Download" "Download"--"Download" "report"
      LET dev = "download"
      LET rep_method = 0  --classic 4gl report
      EXIT MENU

    COMMAND KEY (F627,"B") "Birt" "Birt report"--"Download" "report"
      LET dev = FALSE
      LET rep_method = 0  --classic 4gl report
      EXIT MENU
         
{
    COMMAND KEY (F623) menu_str_arr1[3] menu_str_arr2[3]  HELP 793  -- "DDE MS-Excel 1" "Export report data to MS-Excel via DDE in table format"
      LET dev = "dde-excel-2"
      LET rep_method = 0  --Windows DDE (Phoenix only)
      EXIT MENU

    COMMAND KEY (F624) menu_str_arr1[4] menu_str_arr2[4]  HELP 794  -- "DDE MS-Excel 2" "Export report data to MS-Excel via DDE in group format"
      LET dev = "dde-excel-2"
      LET rep_method = 1  --Windows DDE (Phoenix only)
      EXIT MENU
}
    COMMAND KEY (F12,"E")  menu_str_arr1[9] menu_str_arr2[9]  HELP 794  -- 2307 "Cancel" "Cancel printing"
      LET dev = NULL
      LET rep_method = NULL  --NULL / Nothing
      EXIT MENU

    ON ACTION ("actExit") 
      LET dev = NULL
      LET rep_method = NULL  --NULL / Nothing
      EXIT MENU
			
	     
  END MENU

  CALL publish_toolbar("PrintDevice",1)

  RETURN dev, rep_method

END FUNCTION


###################################################
# FUNCTION print_invoice(p_invoice_id)
#
# Print invoice
#
# RETURN NONE
###################################################
FUNCTION print_invoice(p_invoice_id)
  DEFINE 
    p_invoice_id  LIKE invoice.invoice_id,
    l_line        RECORD LIKE invoice_line.*,
    l_invoice     RECORD LIKE invoice.*,
    l_account     RECORD LIKE account.*,
    l_company     RECORD LIKE company.*,
    l_contact     RECORD LIKE contact.*,
    dev           VARCHAR(20),
    rep_method    SMALLINT,  --0=classic 1 = DDE 2 = XML  3 - HTML
    line_no       SMALLINT,
    doc_name      VARCHAR(200),
    client_doc_name_with_path VARCHAR(250),
    ret           CHAR(3),
    local_debug   SMALLINT,
    l_count       INTEGER

  LET local_debug = FALSE

  CALL get_invoice_rec(p_invoice_id) RETURNING l_invoice.*
  CALL get_account_rec(l_invoice.account_id) RETURNING l_account.*
  CALL get_company_rec(l_account.comp_id) RETURNING l_company.*
  CALL web_get_contact_rec(l_company.comp_main_cont) RETURNING l_contact.*

  DECLARE c_invoice_report CURSOR FOR
    SELECT * FROM invoice_line
      WHERE invoice_line.invoice_id = p_invoice_id

  SELECT COUNT(*) 
    INTO l_count 
    FROM invoice_line
   WHERE invoice_line.invoice_id = p_invoice_id

  #user chooses report dev & type/method
  CALL report_output() RETURNING dev, rep_method

  #check if device was specified, if not - exit menu
  IF dev IS NULL THEN
    #CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN
  END IF

  IF dev THEN
    CALL fgl_report_type("pipe1",dev)
    START REPORT invoice_report TO PIPE "pipe1"
      FOREACH c_invoice_report INTO l_line.*
       OUTPUT TO REPORT invoice_report(l_line.*)
      END FOREACH

      IF l_count < 1 THEN
        OUTPUT TO REPORT invoice_report(p_invoice_id, NULL, NULL, NULL)
      END IF
      FINISH REPORT invoice_report
  ELSE
     CALL makebirt("c_invoice_report","p_invoice_id",p_invoice_id)
  END IF
 

  #Only call start_activity_report(dev) if a classic 4gl report output was choosen
{
  CASE rep_method 


    WHEN 0  --classic 4gl report
      CALL start_invoice_report(dev) RETURNING dev

      FOREACH c_invoice_report INTO l_line.*
       OUTPUT TO REPORT invoice_report(l_line.*)
      END FOREACH

      IF l_count < 1 THEN
        OUTPUT TO REPORT invoice_report(p_invoice_id, NULL, NULL, NULL)
      END IF

      FINISH REPORT invoice_report

    WHEN 1  --DDE MS-Excel Report

      CASE dev
        WHEN "dde-excel-1"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "invoice_report_01.xls"

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            RETURN
          END IF

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout 10 seconds.. NULL is default from dde.cfg TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          #Title & Date
          LET line_no = 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2300) ,0)  --"Invoice"
          #CALL dde_transfer_excel_cell(line_no,26,"Date" ,0)
          #CALL dde_transfer_excel_cell(line_no,30,l_invoice.invoice_date CLIPPED,0)

          #Title
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2310) ,0)  --"Billing Address"
          CALL dde_transfer_excel_cell(line_no,12,get_str(2314) ,0)  --,  "Contact"
          CALL dde_transfer_excel_cell(line_no,24, get_str(2311),0)       --"Delivery Address"

          #1st line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_name CLIPPED,0)

          LET tmp_str = l_contact.cont_title CLIPPED , " ", l_contact.cont_fname CLIPPED, " ", l_contact.cont_lname 
          CALL dde_transfer_excel_cell(line_no,12,tmp_str,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address1 ,0)



          #2nd line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr1 CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,12,get_str(2315) ,0)  --"Tel:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_phone CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address2 ,0)


          #3rd line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr2 CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,12,get_str(2317) ,0)  --"Cel:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_mobile CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address3 ,0)


          #4th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr3 CLIPPED,0)
          CALL dde_transfer_excel_cell(line_no,12,get_str(2316) ,0)  --"Fax:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_fax CLIPPED,0)


          #5th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_city CLIPPED,0)
          CALL dde_transfer_excel_cell(line_no,12,get_str(2331) ,0)  --"E-Mail:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_email CLIPPED,0)

          #6th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_zone CLIPPED,0)


          #7th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_zip CLIPPED,0)


          #8th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_country CLIPPED,0)


          #Invoice Date
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2318) ,0)  --"Inv. Date:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_date CLIPPED,0)


          #Order Number
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,get_str(2319) ,0)  --"Ord. No:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_po CLIPPED,0)

          #Invoice Number
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,get_str(2320) ,0)  --"Inv. No:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_id CLIPPED,0)


          #Invoice for ID...
          LET line_no = line_no + 2
          LET tmp_str = get_str(2312), l_invoice.invoice_id   -- "Items for Invoice "
          CALL dde_transfer_excel_cell(line_no,1,tmp_str ,0)

          #Headers for invoice list items
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2322),0)  --"Item ID:" 
          CALL dde_transfer_excel_cell(line_no,5,get_str(2323) ,0)  --"Quantity:"
          CALL dde_transfer_excel_cell(line_no,8,get_str(2324) ,0)  --"Item Desc.:"
          CALL dde_transfer_excel_cell(line_no,18,get_str(2325) ,0)  --"Item Cost:"
          CALL dde_transfer_excel_cell(line_no,22,get_str(2332) ,0)  --"VAT:"
          CALL dde_transfer_excel_cell(line_no,26, get_str(2326) ,0)  --"Item Amount:"

          LET line_no = line_no + 1

          FOREACH c_invoice_report INTO l_line.*
            CALL dde_transfer_excel_cell(line_no,1,l_line.stock_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,5,l_line.quantity CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,8,get_stock_item_desc(l_line.stock_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,18,get_stock_item_cost(l_line.stock_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,22,l_line.item_tax CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,27,get_stock_item_cost(l_line.stock_id) * l_line.quantity CLIPPED,0)
            LET line_no = line_no+1  --increment line number by one
          END FOREACH

          #Total tax etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2333) ,7)  --Tax Total:
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.tax_total CLIPPED,8)

          #Total NET etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2334) ,7)  --"Net Total:"
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.net_total CLIPPED,8)

          #Total cost etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2335) ,7)  --Total
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.inv_total CLIPPED,8)

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()



        WHEN "dde-excel-2"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "invoice_report_02.xls"

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            #RETURN
          END IF


          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          #Title & Date
          LET line_no = 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2300) ,0)  --"Invoice"
          #CALL dde_transfer_excel_cell(line_no,26,"Date" ,0)
          #CALL dde_transfer_excel_cell(line_no,30,l_invoice.invoice_date CLIPPED,0)

          #Title
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2310) ,0)  --"Billing Address"
          CALL dde_transfer_excel_cell(line_no,12,get_str(2314) ,0)  --,  "Contact"
          CALL dde_transfer_excel_cell(line_no,24, get_str(2311),0)       --"Delivery Address"

          #1st line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_name CLIPPED,0)

          LET tmp_str = l_contact.cont_title CLIPPED , " ", l_contact.cont_fname CLIPPED, " ", l_contact.cont_lname 
          CALL dde_transfer_excel_cell(line_no,12,tmp_str,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address1 ,0)



          #2nd line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr1 CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,12,get_str(2315) ,0)  --"Tel:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_phone CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address2 ,0)


          #3rd line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr2 CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,12,get_str(2317) ,0)  --"Cel:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_mobile CLIPPED,0)

          CALL dde_transfer_excel_cell(line_no,24,l_invoice.del_address3 ,0)


          #4th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_addr3 CLIPPED,0)
          CALL dde_transfer_excel_cell(line_no,12,get_str(2316) ,0)  --"Fax:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_fax CLIPPED,0)


          #5th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_city CLIPPED,0)
          CALL dde_transfer_excel_cell(line_no,12,get_str(2331) ,0)  --"E-Mail:"
          CALL dde_transfer_excel_cell(line_no,15,l_contact.cont_email CLIPPED,0)

          #6th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_zone CLIPPED,0)


          #7th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_zip CLIPPED,0)


          #8th line contact details
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,l_company.comp_country CLIPPED,0)


          #Invoice Date
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2318) ,0)  --"Inv. Date:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_date CLIPPED,0)


          #Order Number
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,get_str(2319) ,0)  --"Ord. No:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_po CLIPPED,0)

          #Invoice Number
          LET line_no = line_no + 1
          CALL dde_transfer_excel_cell(line_no,1,get_str(2320) ,0)  --"Inv. No:"
          CALL dde_transfer_excel_cell(line_no,10,l_invoice.invoice_id CLIPPED,0)


          #Invoice for ID...
          LET line_no = line_no + 2
          LET tmp_str = get_str(2312), l_invoice.invoice_id   -- "Items for Invoice "
          CALL dde_transfer_excel_cell(line_no,1,tmp_str ,0)

          #Headers for invoice list items
          LET line_no = line_no + 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2322),0)  --"Item ID:" 
          CALL dde_transfer_excel_cell(line_no,5,get_str(2323) ,0)  --"Quantity:"
          CALL dde_transfer_excel_cell(line_no,8,get_str(2324) ,0)  --"Item Desc.:"
          CALL dde_transfer_excel_cell(line_no,18,get_str(2325) ,0)  --"Item Cost:"
          CALL dde_transfer_excel_cell(line_no,22,get_str(2332) ,0)  --"VAT:"
          CALL dde_transfer_excel_cell(line_no,26, get_str(2326) ,0)  --"Item Amount:"

          LET line_no = line_no + 1

          FOREACH c_invoice_report INTO l_line.*
            CALL dde_transfer_excel_cell(line_no,1,l_line.stock_id CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,5,l_line.quantity CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,8,get_stock_item_desc(l_line.stock_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,18,get_stock_item_cost(l_line.stock_id) CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,22,l_line.item_tax CLIPPED,0)
            CALL dde_transfer_excel_cell(line_no,27,get_stock_item_cost(l_line.stock_id) * l_line.quantity CLIPPED,0)
            LET line_no = line_no+1  --increment line number by one
          END FOREACH

          #Total tax etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2333) ,7)  --Tax Total:
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.tax_total CLIPPED,8)

          #Total NET etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2334) ,7)  --"Net Total:"
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.net_total CLIPPED,8)

          #Total cost etc.. 
          LET line_no = line_no+1  --increment line number by one
          CALL dde_transfer_excel_cell(line_no,22,get_str(2335) ,7)  --Total
          CALL dde_transfer_excel_cell(line_no,27,l_invoice.inv_total CLIPPED,8)

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()


      END CASE
  END CASE
}

#@@@@@@@@@@@@@@@@@@@@@@@@@@
{
    OTHERWISE
      FOREACH c_invoice_report INTO l_line.*
       OUTPUT TO REPORT invoice_report(l_line.*)
      END FOREACH

      FINISH REPORT invoice_report
  END CASE
}

END FUNCTION

#######################################
# REPORT invoice_report(p_inv_line)
#
# Report definition for invoice
#
# RETURN NONE
#######################################
REPORT invoice_report(p_inv_line)
  DEFINE 
    p_inv_line       RECORD LIKE invoice_line.*,
    ship_to_hdr      VARCHAR(20),
    l_del_address1   LIKE invoice.del_address1,
    l_del_address2   LIKE invoice.del_address2,
    l_del_address3   LIKE invoice.del_address3,
    l_tax_rates      RECORD LIKE tax_rates.*,
    l_stock_item     RECORD LIKE stock_item.*,
    l_contact        RECORD LIKE contact.*,
    l_company        RECORD LIKE company.*,
    l_invoice        RECORD LIKE invoice.*,
    l_account        RECORD LIKE account.*,
    line_tax, line_total, 
    line_net        MONEY(8,2),
    l_currency_name LIKE currency.currency_name

  FORMAT 
    FIRST PAGE HEADER
      CALL get_invoice_rec(p_inv_line.invoice_id)
        RETURNING l_invoice.*

      IF l_invoice.del_address_dif THEN
        LET ship_to_hdr = get_str(2311)  --"Ship To:"
        LET l_del_address1 = l_invoice.del_address1
        LET l_del_address2 = l_invoice.del_address2
        LET l_del_address3 = l_invoice.del_address3
      END IF

      CALL get_account_rec(l_invoice.account_id)
        RETURNING l_account.*

      CALL get_company_rec(l_account.comp_id)
        RETURNING l_company.*

      CALL web_get_contact_rec(l_company.comp_main_cont)
        RETURNING l_contact.*

      PRINT get_str(2312), CURRENT YEAR TO MINUTE  --"Sales Order Invoice "
      SKIP 1 LINE

      PRINT get_str(2313), --"Billing Address:", 
        COLUMN 35, get_str(2314),  --"Contact:", 
        COLUMN 60, ship_to_hdr
      SKIP 1 LINE

      PRINT l_company.comp_name CLIPPED, 
        COLUMN 35, l_contact.cont_fname CLIPPED, " ", l_contact.cont_lname,
        COLUMN 60, l_company.comp_name
      PRINT l_company.comp_addr1 CLIPPED, 
        COLUMN 35, get_str(2315),  --"Tel:", 
        COLUMN 43, l_contact.cont_phone CLIPPED,
        COLUMN 60, l_del_address1
      PRINT l_company.comp_addr2 CLIPPED, 
        COLUMN 35, get_str(2316),  --"Fax:", 
        COLUMN 43, l_contact.cont_fax CLIPPED,
        COLUMN 60, l_del_address2
      PRINT l_company.comp_city CLIPPED, 
        COLUMN 35, get_str(2316),  --"Mobile:", 
        COLUMN 43, l_contact.cont_mobile CLIPPED,
        COLUMN 60, l_del_address3       

      PRINT l_company.comp_country
      PRINT l_company.comp_zip
      SKIP 3 LINES

      PRINT get_str(2318), COLUMN 30, l_invoice.invoice_date  --"Invoice date: ",
      PRINT get_str(2319), COLUMN 30, l_invoice.invoice_po  --Invoice P.O. Number:
      PRINT get_str(2320), COLUMN 30, l_invoice.invoice_id USING "<<<<<<&&"
      SKIP 1 LINE

    BEFORE GROUP OF p_inv_line.invoice_id
      NEED 4 LINES
      PRINT get_str(2321), l_invoice.invoice_id USING "<<<<<<&&"  --"Items for invoice no. : "
      SKIP 1 LINE
      PRINT get_str(2322), COLUMN 11, get_str(2323),      --"Item ID"  --Qty
                       COLUMN 16, get_str(2324),  --Item Description
                       COLUMN 50, get_str(2325),  --"Item cost" 
                       COLUMN 66, get_str(2326),  --"Tax" 
                       COLUMN 73, get_str(2327)  --Line total
      PRINT "----------------------------------------------------------------------------------"

    ON EVERY ROW
      CALL get_stock_item(p_inv_line.stock_id)
        RETURNING l_stock_item.*

      CALL get_tax_rate_rec(l_stock_item.rate_id) 
        RETURNING l_tax_rates.*

      LET line_net = l_stock_item.item_cost * p_inv_line.quantity
      LET line_tax = line_net * l_tax_rates.tax_rate / 100
      LET line_total = line_net + line_tax

      PRINT l_stock_item.stock_id, 
            COLUMN 11, p_inv_line.quantity USING "<<<&&",
            COLUMN 16, l_stock_item.item_desc WORDWRAP RIGHT MARGIN 50

      PRINT COLUMN 50, l_stock_item.item_cost,
            COLUMN 64, l_tax_rates.tax_rate USING "#&.&&%",
            COLUMN 72, line_total

   ON LAST ROW
     PRINT "----------------------------------------------------------------------------------"
     SKIP 1 LINES
     PRINT get_str(2328), COLUMN 25, l_invoice.inv_total;  --"Total Amount Due:"
     LET l_currency_name = get_currency_name(l_invoice.currency_id)

     IF exist(l_invoice.for_total) THEN 
       PRINT COLUMN 36, "/ ", l_invoice.for_total USING "<<<<&.&&", " ", l_currency_name
       PRINT COLUMN 26, get_str(2329), get_xchg_rate(l_invoice.currency_id) USING "<&.&&&"  --Exchange rate:
     ELSE 
       PRINT ""
     END IF

  PAGE TRAILER
    PRINT COLUMN 20, get_str(2330), PAGENO USING "<<<<&"  --Page

END REPORT



###################################################
# FUNCTION print_supply(p_invoice_id)
#
# Print supply
#
# RETURN NONE
###################################################
FUNCTION print_report_supplies(p_supplies_id)
  DEFINE 
    p_supplies_id  LIKE supplies.suppl_id,
    l_line        RECORD LIKE supplies_line.*,
    l_supplies    RECORD LIKE supplies.*,
    l_account     RECORD LIKE account.*,
    l_company     RECORD LIKE company.*,
    l_contact     RECORD LIKE contact.*,
    dev           VARCHAR(20),
    rep_method    SMALLINT,  --0=classic 1 = DDE 2 = XML  3 - HTML
    line_no       SMALLINT,
    doc_name      VARCHAR(200),
    client_doc_name_with_path VARCHAR(250),
    ret           CHAR(3),
    local_debug   SMALLINT

  LET local_debug = FALSE

  CALL get_supplies_rec(p_supplies_id) RETURNING l_supplies.*

  DECLARE c_supplies_report CURSOR FOR
    SELECT supplies_line.* FROM supplies_line
      WHERE supplies_line.suppl_id = p_supplies_id

  #user chooses report dev & type/method
  CALL report_output() RETURNING dev, rep_method

  #check if device was specified, if not - exit menu
  IF dev IS NULL THEN
    CALL fgl_winmessage("No Device specified","There was no printing device specified","ERROR")
    RETURN
  END IF

  IF dev THEN
	CALL fgl_report_type("pipe1",dev)
	START REPORT c_supplies_report TO PIPE "pipe1"
      FOREACH c_supplies_report INTO l_line.*
        OUTPUT TO REPORT supplies_report(l_line.*)
      END FOREACH
	FINISH REPORT c_supplies_report
  ELSE
	CALL makebirt("c_supplies_report","p_supplies_id",p_supplies_id)
  END IF
{
  #Only call start_activity_report(dev) if a classic 4gl report output was choosen
  CASE rep_method 

    WHEN 0  --classic 4gl report
      CALL start_supplies_report(dev) RETURNING dev

      FOREACH c_supplies_report INTO l_line.*
        OUTPUT TO REPORT supplies_report(l_line.*)
      END FOREACH

      FINISH REPORT supplies_report

    WHEN 1  --DDE MS-Excel Report

      CASE dev
        WHEN "dde-excel-1"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "supplies_report_01.xls"

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            RETURN
          END IF

          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout 10 seconds.. NULL is default from dde.cfg TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          #Title & Date
          LET line_no = 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2300) ,0)  --"Invoice"
          #CALL dde_transfer_excel_cell(line_no,26,"Date" ,0)
          #CALL dde_transfer_excel_cell(line_no,30,l_invoice.invoice_date CLIPPED,0)

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()

        WHEN "dde-excel-2"

          IF fgl_getuitype() <> "WTK" THEN
            CALL fgl_winmessage("DDE requires a Windows client","DDE is only supported for native MS-Windows clients","error")
            RETURN
          END IF

          #Assign the document file name
          LET doc_name = "supplies_report_02.xls"

          #Define the client side document name and location
          LET client_doc_name_with_path = get_client_temp_path(get_document_path(doc_name))

          #Extract document from BLOB (db) and download it to the client
          LET client_doc_name_with_path = download_blob_document_to_client(get_document_id_from_filename(doc_name),client_doc_name_with_path,0)

          #Start the corresponding (file system associated app in windows) with this document
          #Start up Word using the document - file will be downloaded by the function
          IF NOT run_client_application_with_client_document(client_doc_name_with_path) THEN
            CALL fgl_winmessage("Error","Could not start associated appliation with the document","error")
            #RETURN
          END IF


          #Start up excel using the document - file will be downloaded by the function
          #CALL run_client_application_with_server_document(get_document_path(doc_name))

          #set dde environment
          CALL init_dde("excel",doc_name,client_doc_name_with_path,NULL,FALSE)    --10=timeout TRUE/FALSE turns debug on/off

          #Set Excel to work with R1C1 format
          CALL dde_execute("[A1.R1C1(0)]",FALSE)  --True turns error checking/capturing on

          #Title & Date
          LET line_no = 2
          CALL dde_transfer_excel_cell(line_no,1,get_str(2300) ,0)  --"Invoice"
          #CALL dde_transfer_excel_cell(line_no,26,"Date" ,0)
          #CALL dde_transfer_excel_cell(line_no,30,l_invoice.invoice_date CLIPPED,0)

          #Optional Printing in Excel
          LET ret = fgl_winquestion("Print", "Do you want to print the MS-Excel Document?", "Yes", "Yes|No", "question", 1)
  
          IF ret = "Yes" THEN
            CALL dde_execute("[PRINT]",FALSE)   --FALSE turns error checking/capturing off
          END IF

          #Finnish/Close all DDE connections
          CALL DDEFinishAll()

      END CASE
  END CASE
}
END FUNCTION -- print supply --



###################################################
# FUNCTION get_supplies_rec(p_invoice_id)
#
# Get supplies record from id
#
# RETURN l_supplies.* (LIKE supplies.*)
###################################################
#FUNCTION get_supplies_rec(p_supplies_id)
#  DEFINE 
#    p_supplies_id   LIKE supplies.suppl_id,
#    r_supplies      RECORD LIKE supplies.*

#  SELECT supplies.*
#    INTO r_supplies.*
#    FROM supplies
#    WHERE supplies.suppl_id = p_supplies_id

#  RETURN r_supplies.*

#END FUNCTION



#######################################
# REPORT supplies_report(p_supp_line)
#
# Report definition for supplies
#
# RETURN NONE
#######################################
REPORT supplies_report(p_supp_line)
  DEFINE 
    p_supp_line       RECORD LIKE supplies_line.*,
    ship_to_hdr      VARCHAR(20),
    #l_del_address1   LIKE invoice.del_address1,
    #l_del_address2   LIKE invoice.del_address2,
    #l_del_address3   LIKE invoice.del_address3,
    #l_tax_rates      RECORD LIKE tax_rates.*,
    l_stock_item     RECORD LIKE stock_item.*,
    #l_contact        RECORD LIKE contact.*,
    #l_company        RECORD LIKE company.*,
    l_supplies       RECORD LIKE supplies.*,
    line_total       MONEY(8,2)
    
  FORMAT 
    FIRST PAGE HEADER
      CALL get_supplies_rec(p_supp_line.suppl_id)
        RETURNING l_supplies.*

      PRINT "Supplies Order", CURRENT YEAR TO MINUTE  --"Sales Order Invoice "
      SKIP 1 LINE

      PRINT "Supplies Order Date", COLUMN 30, l_supplies.suppl_date  --"Invoice date: ",
      PRINT get_str(2320), COLUMN 30, l_supplies.suppl_id USING "<<<<<<&&"
      SKIP 1 LINE

    BEFORE GROUP OF p_supp_line.suppl_id
      NEED 4 LINES
      PRINT "Items for Supplies Order no.:", l_supplies.suppl_id USING "<<<<<<&&"  --"Items for invoice no. : "
      SKIP 1 LINE
      PRINT get_str(2322), COLUMN 11, get_str(2323),      --"Item ID"  --Qty
                       COLUMN 16, get_str(2324),  --Item Description
                       COLUMN 50, get_str(2325),  --"Item cost" 
                       COLUMN 73, get_str(2327)  --Line total
                       
      PRINT "----------------------------------------------------------------------------------"

    ON EVERY ROW
      CALL get_stock_item(p_supp_line.stock_id)
        RETURNING l_stock_item.*

      LET line_total = l_stock_item.item_cost * p_supp_line.quantity

      PRINT l_stock_item.stock_id, 
            COLUMN 11, p_supp_line.quantity USING "<<<&&",
            COLUMN 16, l_stock_item.item_desc
# WORDWRAP RIGHT MARGIN 50

      PRINT COLUMN 50, l_stock_item.item_cost,
            COLUMN 72, line_total

   ON LAST ROW
     PRINT "----------------------------------------------------------------------------------"
     SKIP 1 LINES

  PAGE TRAILER
    PRINT COLUMN 20, get_str(2330), PAGENO USING "<<<<&"  --Page

END REPORT



{

BEFORE GROUP OF p_inv_line.invoice_id
      NEED 4 LINES
      PRINT get_str(2321), l_invoice.invoice_id USING "<<<<<<&&"  --"Items for invoice no. : "
      SKIP 1 LINE
      PRINT get_str(2322), COLUMN 11, get_str(2323),      --"Item ID"  --Qty
                       COLUMN 16, get_str(2324),  --Item Description
                       COLUMN 50, get_str(2325),  --"Item cost" 
                       COLUMN 66, get_str(2326),  --"Tax" 
                       COLUMN 73, get_str(2327)  --Line total
      PRINT "----------------------------------------------------------------------------------"

    ON EVERY ROW
      CALL get_stock_item(p_inv_line.stock_id)
        RETURNING l_stock_item.*

      CALL get_tax_rate_rec(l_stock_item.rate_id) 
        RETURNING l_tax_rates.*

      LET line_net = l_stock_item.item_cost * p_inv_line.quantity
      LET line_tax = line_net * l_tax_rates.tax_rate / 100
      LET line_total = line_net + line_tax

      PRINT l_stock_item.stock_id, 
            COLUMN 11, p_inv_line.quantity USING "<<<&&",
            COLUMN 16, l_stock_item.item_desc WORDWRAP RIGHT MARGIN 50

      PRINT COLUMN 50, l_stock_item.item_cost,
            COLUMN 64, l_tax_rates.tax_rate USING "#&.&&%",
            COLUMN 72, line_total

   ON LAST ROW
     PRINT "----------------------------------------------------------------------------------"
}



#######################################################
# FUNCTION populate_report_act_form_labels_t()
#
# Populate report activity selection form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_report_act_form_labels_t()

  DISPLAY get_str(2250) TO lbTitle
  DISPLAY get_str(2251) TO dl_f1  
  #DISPLAY get_str(2252) TO dl_f2 
  #DISPLAY get_str(2253) TO dl_f3 
  DISPLAY get_str(2254) TO dl_f4 
  DISPLAY get_str(2255) TO dl_f5 
  #DISPLAY get_str(2256) TO dl_f6 
  #DISPLAY get_str(2257) TO dl_f7 
  DISPLAY get_str(2258) TO dl_f8 
  DISPLAY get_str(2259) TO dl_f9 
  DISPLAY get_str(2260) TO dl_f10 
  DISPLAY get_str(2261) TO dl_f11 
  DISPLAY get_str(2262) TO dl_f12 
  DISPLAY get_str(2263) TO dl_f13 
  DISPLAY get_str(2264) TO dl_f14 
  #DISPLAY get_str(2265) TO dl_f15 
  #DISPLAY get_str(2266) TO dl_f16 
  #DISPLAY get_str(2267) TO dl_f17 
  #DISPLAY get_str(2268) TO dl_f18 
  #DISPLAY get_str(2269) TO lbInfo1 
END FUNCTION


#######################################################
# FUNCTION populate_report_act_form_labels_g()
#
# Populate report activity selection form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_report_act_form_labels_g()

  DISPLAY get_str(2250) TO lbTitle
  DISPLAY get_str(2251) TO dl_f1  
  --DISPLAY get_str(2252) TO dl_f2 
  --DISPLAY get_str(2253) TO dl_f3 
  CALL UIRadButListItemText("id_type",1,get_str(2252))
  CALL UIRadButListItemText("id_type",2,get_str(2253))
  --DISPLAY get_str(2254) TO dl_f4 
  --DISPLAY get_str(2255) TO dl_f5 
  --DISPLAY get_str(2256) TO dl_f6 
  --DISPLAY get_str(2257) TO dl_f7  
  CALL UIRadButListItemText("stat",1,get_str(2256))
  CALL UIRadButListItemText("stat",2,get_str(2257))
  --DISPLAY get_str(2258) TO dl_f8 
  --DISPLAY get_str(2259) TO dl_f9
CALL updateUILabel("f_name",get_str(2260))
CALL updateUILabel("f_type",get_str(2254))
CALL updateUILabel("f_status",get_str(2255))
CALL updateUILabel("f_cdate",get_str(2258))
CALL updateUILabel("f_odate",get_str(2259))
  #############################
  #DISPLAY get_str(2260) TO dl_f10 
  #DISPLAY get_str(2261) TO dl_f11 
  #DISPLAY get_str(2262) TO dl_f12 
  #DISPLAY get_str(2263) TO dl_f13 
  #DISPLAY get_str(2264) TO dl_f14 
  #DISPLAY get_str(2265) TO dl_f15 
  #DISPLAY get_str(2266) TO dl_f16 
  #DISPLAY get_str(2267) TO dl_f17 
  #DISPLAY get_str(2268) TO dl_f18 
  #DISPLAY get_str(2269) TO lbInfo1 

  DISPLAY get_str(826) TO bt_ok
  #DISPLAY "!" TO bt_ok
  DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL activity_type_combo_list("type_name")

  CALL fgl_settitle(get_str(2268))  --"Customized Report (Select)")

END FUNCTION
{
#########################################################################################################
# FUNCTION dde_demo()  Main DDE demo function
######################################################################################################### 
FUNCTION dde_excel_demo()
  DEFINE on_error_continue, temp_rec SMALLINT
  LET on_error_continue = 0

  CALL dde_init()

  OPEN WINDOW w_dde_demo AT 1,1 WITH FORM "f_dde_main"

  LET dde_settings.f_exec_return =  dde_run_excel()
  IF  (dde_settings.f_exec_return = -2) OR  (dde_settings.f_exec_return = 1) THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF

  CALL wait_for_excel()  -- this function loops with a dde system connect until sucessful (max 10 seconds)


  IF dde_system_connect() != 1 THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF


  IF dde_save_document(1) != 1 THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF

  IF dde_document_connect() != 1 THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF

#  IF dde_transfer_data() != 1 THEN
#    CLOSE WINDOW w_dde_demo
#    RETURN
#  END IF

  IF dde_save_document(2) != 1 THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF

  IF dde_print_document() != 1 THEN
    CLOSE WINDOW w_dde_demo
    RETURN
  END IF

  call DDEFinishAll()

  CLOSE WINDOW w_dde_demo

END FUNCTION


}