##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_help_classic_globals.4gl"



###########################################################
# FUNCTION process_help_classic_cfg_import()
#
# Import/Process classic_help cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################





###########################################################
# FUNCTION get_print_html_image_lib_info()
#
# Simply returns libray type /  name
#
# RETURN "FILE - qxt_file_print_html_image"
###########################################################
FUNCTION get_help_classic_lib_info()
  RETURN "FILE - qxt_help_classic"
END FUNCTION 




#####################################################################################
# Init/Config/Import Functions
#####################################################################################

###########################################################
# FUNCTION process_help_classic_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_help_classic_init(p_filename)
  DEFINE 
    p_filename VARCHAR(100)

  CALL process_help_classic_cfg_import(NULL)

  IF p_filename IS NULL THEN
    LET p_filename = get_help_classic_unl_filename()
  END IF

  CALL qxt_help_classic_unl_import_file(get_unl_path(p_filename))
  #This function must be compatible with the file toolbar library
END FUNCTION

###########################################################
# FUNCTION qxt_help_classic_import_file(p_filename)
#
# Import the classic help unl file
#
# RETURN NONE
###########################################################
FUNCTION qxt_help_classic_unl_import_file(p_filename)
  DEFINE 
    p_filename                     VARCHAR(150),
    l_tmp_qxt_help_classic_rec     OF t_qxt_help_classic_rec,
    sql_stmt                       CHAR(1000),
    local_debug                    SMALLINT,
    err_msg                        VARCHAR(200),
    id                             SMALLINT

  LET local_debug = FALSE


  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_help_classic_import_file()\nCan not open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_help_classic_import_file()", err_msg, "error")
    RETURN
  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_help_classic_import_file()\nCan not set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_help_classic_import_file()", err_msg, "error")
    RETURN
  END IF


  WHILE fgl_channel_read("stream",l_tmp_qxt_help_classic_rec.*) 

    LET qxt_help_classic[l_tmp_qxt_help_classic_rec.id] = l_tmp_qxt_help_classic_rec.filename



    IF local_debug THEN
      DISPLAY "qxt_help_classic_import_file() - l_tmp_qxt_help_classic_rec.id=",                l_tmp_qxt_help_classic_rec.id 
      DISPLAY "qxt_help_classic_import_file() - l_tmp_qxt_help_classic_rec.filename=",          l_tmp_qxt_help_classic_rec.filename 
      DISPLAY "qxt_help_classic_import_file() - qxt_help_classic[l_tmp_qxt_help_classic_rec.id]=",  qxt_help_classic[l_tmp_qxt_help_classic_rec.id]

    END IF 

  END WHILE

  #Close the file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_help_classic_import_file()\nCan not close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_help_classic_import_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION


FUNCTION get_help_classic_filename(p_id)
  DEFINE p_id SMALLINT

  RETURN qxt_help_classic[p_id]

END FUNCTION





############################################################
# FUNCTION set_help_file(h_file)
#
# Set the help file (options help file) depening on the language
#
# RETURN NONE
############################################################
FUNCTION set_classic_help_file(h_file_id)
  DEFINE
    h_file      VARCHAR(200),
    h_file_id   SMALLINT,
    lang        SMALLINT,
    local_debug SMALLINT,
    err_msg     VARCHAR(200)

  LET local_debug = FALSE

  LET qxt_previous_help_file_id = qxt_current_help_file_id
  LET lang = get_language()
  LET qxt_current_help_file_id = h_file_id

  IF h_file_id >= 1 AND h_file_id <= 10 THEN

    IF get_help_classic_multi_lang() = 0 THEN
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, ".erm"
    ELSE
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, "_", trim(get_language_dir(lang)), ".erm"
    END IF
    
    IF NOT fgl_test("e",h_file) THEN
      LET err_msg = get_str_tool(727), " ",  h_file
      CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
    ELSE
      OPTIONS HELP FILE h_file
      IF local_debug THEN
        DISPLAY "OPTIONS HELP FILE ", h_file
      END IF
    END IF

  ELSE
    LET err_msg = get_str_tool(802), "\nset_classic_help_file(h_file_id)\nh_file_id = ", h_file_id
    CALL fgl_winmessage(get_str_tool(30), err_msg, "error")
  END IF



END FUNCTION




############################################################
# FUNCTION set_previous_classic_help()
#
# Switch back to the last used help file (id)
#
# RETURN NONE
############################################################
FUNCTION set_previous_classic_help()

  CALL  set_classic_help_file(get_previous_classic_help())

END FUNCTION



############################################################
# FUNCTION set_previous_classic_help()
#
# Switch back to the last used help file (id)
#
# RETURN NONE
############################################################
FUNCTION get_previous_classic_help()

  RETURN qxt_previous_help_file_id

END FUNCTION


############################################################
# FUNCTION get_current_help_file_id()
#
# Return the currently active help file id
#
# RETURN NONE
############################################################
FUNCTION get_current_classic_help()

  RETURN qxt_current_help_file_id

END FUNCTION




############################################################
# FUNCTION refresh_classic_help()
#
# Refresh classic help file with the current id (required for changes i.e. in language)
#
# RETURN NONE
############################################################
FUNCTION refresh_classic_help()

  CALL set_classic_help_file(qxt_current_help_file_id)

END FUNCTION
