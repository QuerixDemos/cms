##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

import java java.util.logging.Level
import java org.eclipse.birt.core.framework.Platform
import java org.eclipse.birt.report.engine.api.EngineConfig
import java org.eclipse.birt.report.engine.api.HTMLRenderOption
import java org.eclipse.birt.report.engine.api.IReportEngine
import java org.eclipse.birt.report.engine.api.IReportEngineFactory
import java org.eclipse.birt.report.engine.api.IReportRunnable
import java org.eclipse.birt.report.engine.api.IRunAndRenderTask

FUNCTION makebirt(CurBirtType,ParamName,ParamId)
DEFINE CurBirtType,ParamId CHAR(50)
DEFINE ParamName CHAR(50)
	define config EngineConfig
	define factory IReportEngineFactory
	define engine IReportEngine
	define design IReportRunnable  
	define task IRunAndRenderTask
	define html_opts HTMLRenderOption
	define birt_home_dir string
--WHENEVER ERROR CONTINUE    	   
    let birt_home_dir = fgl_getenv("LYCIA_DIR") || "\ReportEngine"
	let config = EngineConfig.create()
	call config.setBIRTHome(birt_home_dir)	
	call Platform.startup(config)
    LET factory = Platform.createFactoryObject("org.eclipse.birt.report.engine.ReportEngineFactory")
	let engine = factory.createReportEngine(config)       
--WHENEVER ERROR STOP	
	--open BIRT report form
CASE CurBirtType
 WHEN "c_all_act"
      	LET design = engine.openReportDesign("birt/c_all_act.rptdesign")
	    LET task = engine.createRunAndRenderTask(design)
	    LET html_opts = HTMLRenderOption.create()
	    CALL html_opts.setOutputFileName("report/c_all_act.html")       
        CALL task.setRenderOption(html_opts)	
	    DISPLAY "HTML render task is running..."
		CALL task.run()
		CALL task.close()
		CALL engine.destroy()	        
--		CALL Platform.shutdown()
    	DISPLAY "Report has been saved into file:"
	    DISPLAY "report\\c_all_act.html"
      
 WHEN "c_act_rep"
      	LET design = engine.openReportDesign("birt/c_act_rep.rptdesign")
	    LET task = engine.createRunAndRenderTask(design)
	    LET html_opts = HTMLRenderOption.create()
	    CALL html_opts.setOutputFileName("report/c_act_rep.html")       
        CALL task.setRenderOption(html_opts)	
	    DISPLAY "HTML render task is running..."
		CALL task.run()
		CALL task.close()
		CALL engine.destroy()	        
--		CALL Platform.shutdown()
    	DISPLAY "Report has been saved into file:"
	    DISPLAY "report\\c_act_rep.html"
      
 WHEN "c_invoice_report"
 		LET design = engine.openReportDesign("birt/c_invoice_report.rptdesign")
		LET task = engine.createRunAndRenderTask(design)
	-- Set query parameter to  adress_menu defined in form report(see birt form in birt perspective)
    -- Remove these two CALLs if report is simple - without parameters 
    	CALL task.setParameterValue("p_invoice_id",ParamId)	         
    	CALL task.validateParameters()
    	LET html_opts = HTMLRenderOption.create()
	    CALL html_opts.setOutputFileName("report/c_invoice_report.html")       
        CALL task.setRenderOption(html_opts)	
	    DISPLAY "HTML render task is running..."
		CALL task.run()
		CALL task.close()
		CALL engine.destroy()	        
--		CALL Platform.shutdown()
    	DISPLAY "Report has been saved into file:"
	    DISPLAY "report\\c_invoice_report.html"
 WHEN "c_supplies_report"
 		LET design = engine.openReportDesign("birt/c_supplies_report.rptdesign")
		LET task = engine.createRunAndRenderTask(design)
	-- Set query parameter to  adress_menu defined in form report(see birt form in birt perspective)
    -- Remove these two CALLs if report is simple - without parameters 
    	CALL task.setParameterValue(ParamName,ParamId)	         
    	CALL task.validateParameters()
    	LET html_opts = HTMLRenderOption.create()
	    CALL html_opts.setOutputFileName("report/c_supplies_report.html")       
        CALL task.setRenderOption(html_opts)	
	    DISPLAY "HTML render task is running..."
		CALL task.run()
		CALL task.close()
		CALL engine.destroy()	        
--		CALL Platform.shutdown()
    	DISPLAY "Report has been saved into file:"
	    DISPLAY "report\\c_supplies_report.html"
END CASE
end function
