##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


############################################################################################################################
# Form population functions
############################################################################################################################


####################################################
# FUNCTION grid_header_f_invoice_scroll_g()
####################################################
FUNCTION grid_header_f_invoice_scroll_g()
  CALL fgl_grid_header("sc_invoice_scroll", "invoice_id"  , get_str(1541), "left", "F13")  --Invoice ID
  CALL fgl_grid_header("sc_invoice_scroll", "comp_name"   , get_str(1542), "left", "F14")  --Company
  CALL fgl_grid_header("sc_invoice_scroll", "net_total"   , get_str(1543), "left", "F15")  --Net.Total
  CALL fgl_grid_header("sc_invoice_scroll", "tax_total"   , get_str(1544), "left", "F16")  --Tax Total
  CALL fgl_grid_header("sc_invoice_scroll", "inv_total"   , get_str(1545), "left", "F17")  --Inv. Total
  CALL fgl_grid_header("sc_invoice_scroll", "invoice_date", get_str(1546), "left", "F18")  --Date
  CALL fgl_grid_header("sc_invoice_scroll", "status_name" , get_str(2255), "left", "F18")  --Status
END FUNCTION 



#######################################################
# FUNCTION populate_invoice_form_labels_t()
#
# Populate invoice detail form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_labels_t()
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1515) TO dl_f15
  DISPLAY get_str(1517) TO dl_f17
  DISPLAY get_str(1518) TO dl_f18
  DISPLAY get_str(1519) TO dl_f19
  DISPLAY get_str(1520) TO dl_f20
  DISPLAY get_str(1521) TO dl_f21
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1526) TO dl_f26
  DISPLAY get_str(1527) TO dl_f27
  DISPLAY get_str(1528) TO dl_f28
  DISPLAY get_str(1529) TO dl_f29
  DISPLAY get_str(1530) TO dl_f30
  DISPLAY get_str(1535) TO dl_a  --dl_f35
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_invoice_form_labels_g()
#
# Populate invoice detail form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_labels_g()
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

  CALL fgl_grid_header("sc_inv_lines", "stock_id"      , get_str(1515), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "quantity"      , get_str(1535), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_cost"     , get_str(1517), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_desc"     , get_str(1518), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_net_total", get_str(1519), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_tax_total", get_str(1520), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_total"    , get_str(1521), "Left", "")

END FUNCTION


#######################################################
# FUNCTION populate_invoice_form_view_labels_t()
#
# Populate invoice view detail form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_view_labels_t()
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1515) TO dl_f15
  DISPLAY get_str(1517) TO dl_f17
  DISPLAY get_str(1518) TO dl_f18
  DISPLAY get_str(1519) TO dl_f19
  DISPLAY get_str(1520) TO dl_f20
  DISPLAY get_str(1521) TO dl_f21
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1526) TO dl_f26
  DISPLAY get_str(1527) TO dl_f27
  DISPLAY get_str(1528) TO dl_f28
  DISPLAY get_str(1529) TO dl_f29
  DISPLAY get_str(1530) TO dl_f30
  DISPLAY get_str(1535) TO dl_a  --dl_f35
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_invoice_form_view_labels_g()
#
# Populate invoice detail form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_view_labels_g()
  CALL updateUILabel("c20","Customer")  
  CALL updateUILabel("c21","Miscellaneous")  
  CALL updateUILabel("c22","Inv. Header")  
  CALL updateUILabel("c23","Delivery")  
  CALL updateUILabel("c24","Foreign Currency")
  CALL updateUILabel("del_address_dif","Diff. Delivery Addr.")
  CALL UIRadButListItemText("del_method",1,"Normal Delivery")
  CALL UIRadButListItemText("del_method",2,"Next-Day Del.")
  CALL UIRadButListItemText("del_method",3,"Same Day Del")
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

  CALL fgl_grid_header("sc_inv_lines", "stock_id"      , get_str(1515), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "quantity"      , get_str(1535), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_cost"     , get_str(1517), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_desc"     , get_str(1518), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_net_total", get_str(1519), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_tax_total", get_str(1520), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_total"    , get_str(1521), "Left", "")

  DISPLAY "!" TO bt_ok
  DISPLAY "!" TO bt_cancel
  DISPLAY "!" TO bt_help

  CALL payment_method_combo_list("pay_method_name")
  CALL currency_combo_list("currency_name")

END FUNCTION



#######################################################
# FUNCTION populate_invoice_form_input_labels_t()
#
# Populate invoice input detail form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_input_labels_t()
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1515) TO dl_f15
  DISPLAY get_str(1517) TO dl_f17
  DISPLAY get_str(1518) TO dl_f18
  DISPLAY get_str(1519) TO dl_f19
  DISPLAY get_str(1520) TO dl_f20
  DISPLAY get_str(1521) TO dl_f21
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1526) TO dl_f26
  DISPLAY get_str(1527) TO dl_f27
  DISPLAY get_str(1528) TO dl_f28
  DISPLAY get_str(1529) TO dl_f29
  DISPLAY get_str(1530) TO dl_f30
  DISPLAY get_str(1535) TO dl_a  --dl_f35
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_invoice_form_input_labels_g()
#
# Populate invoice input  detail form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_input_labels_g()
  CALL updateUILabel("c20","Customer")  
  CALL updateUILabel("c21","Miscellaneous")  
  CALL updateUILabel("c22","Inv. Header")  
  CALL updateUILabel("c23","Delivery")  
  CALL updateUILabel("c24","Foreign Currency")
  CALL updateUILabel("del_address_dif","Diff. Delivery Addr.")
  CALL UIRadButListItemText("del_method",1,"Normal Delivery")
  CALL UIRadButListItemText("del_method",2,"Next-Day Del.")
  CALL UIRadButListItemText("del_method",3,"Same Day Del")
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23

  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

  CALL fgl_grid_header("sc_inv_lines", "stock_id"      , get_str(1515), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "quantity"      , get_str(1535), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_cost"     , get_str(1517), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_desc"     , get_str(1518), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_net_total", get_str(1519), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_tax_total", get_str(1520), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_total"    , get_str(1521), "Left", "")

  DISPLAY "!" TO bt_ok
  DISPLAY "!" TO bt_cancel
  DISPLAY "!" TO bt_help
  CALL fgl_settitle(get_str(1400)) --"Invoice")
  CALL payment_method_combo_list("pay_method_name")
  CALL currency_combo_list("currency_name")

END FUNCTION



#######################################################
# FUNCTION populate_invoice_form_query_labels_t()
#
# Populate invoice query detail form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_query_labels_t()
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1515) TO dl_f15
  DISPLAY get_str(1517) TO dl_f17
  DISPLAY get_str(1518) TO dl_f18
  DISPLAY get_str(1519) TO dl_f19
  DISPLAY get_str(1520) TO dl_f20
  DISPLAY get_str(1521) TO dl_f21
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1526) TO dl_f26
  DISPLAY get_str(1527) TO dl_f27
  DISPLAY get_str(1528) TO dl_f28
  DISPLAY get_str(1529) TO dl_f29
  DISPLAY get_str(1530) TO dl_f30
  DISPLAY get_str(1535) TO dl_a  --dl_f35
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_invoice_form_query_labels_g()
#
# Populate invoice query  detail form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_form_query_labels_g()
  CALL updateUILabel("c20","Customer")  
  CALL updateUILabel("c21","Miscellaneous")  
  CALL updateUILabel("c22","Inv. Header")  
  CALL updateUILabel("c23","Delivery")  
  CALL updateUILabel("c24","Foreign Currency")
  CALL updateUILabel("del_address_dif","Diff. Delivery Addr.")
  CALL UIRadButListItemText("del_method",1,"Normal Delivery")
  CALL UIRadButListItemText("del_method",2,"Next-Day Del.")
  CALL UIRadButListItemText("del_method",3,"Same Day Del")
  DISPLAY get_str(1500) TO lbTitle
  DISPLAY get_str(1501) TO dl_f1
  DISPLAY get_str(1502) TO dl_f2
  DISPLAY get_str(1503) TO dl_f3
  DISPLAY get_str(1504) TO dl_f4
  DISPLAY get_str(1505) TO dl_f5
  DISPLAY get_str(1506) TO dl_f6
  DISPLAY get_str(1507) TO dl_f7
  DISPLAY get_str(1508) TO dl_f8
  DISPLAY get_str(1509) TO dl_f9
  DISPLAY get_str(1510) TO dl_f10
  DISPLAY get_str(1511) TO dl_f11
  DISPLAY get_str(1512) TO dl_f12
  DISPLAY get_str(1513) TO dl_f13
  DISPLAY get_str(1514) TO dl_f14
  DISPLAY get_str(1522) TO dl_f22
  DISPLAY get_str(1523) TO dl_23  --dl_f23
  DISPLAY get_str(1536) TO dl_b  --dl_f36
  DISPLAY get_str(1537) TO dl_c  --dl_f37
  DISPLAY get_str(1538) TO dl_d  --dl_f38

  #DISPLAY get_str(1539) TO lbInfo1

  CALL fgl_grid_header("sc_inv_lines", "stock_id"      , get_str(1515), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "quantity"      , get_str(1535), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_cost"     , get_str(1517), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "item_desc"     , get_str(1518), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_net_total", get_str(1519), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_tax_total", get_str(1520), "Left", "")
  CALL fgl_grid_header("sc_inv_lines", "line_total"    , get_str(1521), "Left", "")

  DISPLAY get_str(812) TO bt_ok
  DISPLAY "!" TO bt_ok
  #DISPLAY "!" TO bt_cancel
  #DISPLAY "!" TO bt_help
  CALL payment_method_combo_list("pay_method_name")
  CALL currency_combo_list("currency_name")

END FUNCTION



#######################################################
# FUNCTION populate_invoice_scroll_form_labels_t()
#
# Populate invoice list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_scroll_form_labels_t()
  DISPLAY get_str(1540) TO lbTitle
  DISPLAY get_str(1541) TO dl_f1
  DISPLAY get_str(1542) TO dl_f2
  DISPLAY get_str(1543) TO dl_f3
  DISPLAY get_str(1544) TO dl_f4
  DISPLAY get_str(1545) TO dl_f5
  DISPLAY get_str(1546) TO dl_f6
  #DISPLAY get_str(1547) TO dl_f7
  #DISPLAY get_str(1548) TO dl_f8
  #DISPLAY get_str(1549) TO lbInfo1

END FUNCTION



#######################################################
# FUNCTION populate_invoice_scroll_form_labels_g()
#
# Populate invoice list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_invoice_scroll_form_labels_g()
  CALL UIRadButListItemText("rb_status_filter",1, get_str(2477))
  CALL UIRadButListItemText("rb_status_filter",2, get_str(2565))
  CALL UIRadButListItemText("rb_status_filter",3, get_str(2465))
  CALL UIRadButListItemText("rb_status_filter",4, get_str(2473))
  CALL UIRadButListItemText("rb_status_filter",5, get_str(2566))
  CALL UIRadButListItemText("rb_status_filter",6, get_str(2469))
  CALL UIRadButListItemText("rb_status_filter",7, get_str(2471))
  
  DISPLAY get_str(1540) TO lbTitle
{
  DISPLAY get_str(2477) TO rb_st1
  DISPLAY get_str(2565) TO rb_st2
  DISPLAY get_str(2465) TO rb_st3
  DISPLAY get_str(2473) TO rb_st4
  DISPLAY get_str(2566) TO rb_st5
  DISPLAY get_str(2469) TO rb_st6
  DISPLAY get_str(2471) TO rb_st7
}
  DISPLAY get_str(810) TO bt_ok
--  DISPLAY "!" TO bt_ok
  DISPLAY get_str(820) TO bt_cancel
--  DISPLAY "!" TO bt_cancel
  DISPLAY get_str(818) TO bt_edit
--  DISPLAY "!" TO bt_edit
  DISPLAY get_str(10) TO bt_print
--  DISPLAY "!" TO bt_print
--  DISPLAY "!" TO  rb_status_filter
  DISPLAY "F121" TO rb_status_filter

  CALL grid_header_f_invoice_scroll_g()
  CALL fgl_settitle(get_str(1540))

END FUNCTION









