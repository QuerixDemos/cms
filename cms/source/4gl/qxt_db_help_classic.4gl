##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_help_classic_globals.4gl"


############### #######################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_help_classic_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_help_classic_lib_info()
  RETURN "DB - qxt_db_help_classic"
END FUNCTION 


##################################################################################################
# Init & Data import functions
##################################################################################################


############################################################
# FUNCTION init_help_classics()
#
# Initialises help_classic settings
#
# RETURN NONE
############################################################
FUNCTION process_help_classic_init(p_filename)
  DEFINE 
    p_filename VARCHAR(100)

  CALL process_help_classic_cfg_import(NULL)
  #Just a dummy function
  # only required to be compatible with the 'file' help_classic library

END FUNCTION

###########################################################################################
# Data Acces Functions
###########################################################################################

{

#############################################################
# FUNCTION get_help_classic_id(p_language_id,p_filename)
#
# Returns the id from a help_classic (name)
#
# RETURN qxt_help_classic_rec[id].id
#############################################################
FUNCTION get_help_classic_id(p_filename)
  DEFINE 
    p_filename      LIKE qxt_help_classic.filename,
    l_id            LIKE qxt_help_classic.id


  SELECT id
    INTO l_id
    FROM qxt_help_classic

  RETURN l_id
END FUNCTION

}

###########################################################
# FUNCTION get_help_classic_filename(p_language_id,p_id)
#
# get help_classic filenameectory from p_id i.e. de,sp,fr,...
#
# RETURN l_filename
###########################################################
FUNCTION get_help_classic_filename(p_id)
  DEFINE 
    p_id         LIKE qxt_help_classic.id,
    l_filename   LIKE qxt_help_classic.filename

  SELECT filename
    INTO l_filename
    FROM qxt_help_classic
    WHERE id = p_id

  RETURN l_filename
END FUNCTION


{
###########################################################
# FUNCTION get_help_classic_rec(p_id)
#
# get help_classic record from p_id
#
# RETURN l_help_classic_rec
###########################################################
FUNCTION get_help_classic_rec(p_id)
  DEFINE 
    p_id                   LIKE qxt_help_classic.id,
    l_help_classic_rec     RECORD LIKE qxt_help_classic.*

  SELECT *
    INTO l_help_classic_rec
    FROM qxt_help_classic
    WHERE id = p_id

  RETURN l_help_classic_rec.*
END FUNCTION

}

########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION help_classic_combo_list(cb_field_name)
#
# Populates the combo box with help_classics
#
# RETURN NONE
###################################################################################
FUNCTION help_classic_combo_list(cb_field_name)
  DEFINE 
    cb_field_name          VARCHAR(20),   --form field name for the country combo list field
    l_help_classic_rec     RECORD LIKE qxt_help_classic.*, 
    #l_help_classic_arr    DYNAMIC ARRAY OF t_qxt_help_classic_rec,
    row_count              INTEGER,
    current_row            INTEGER,
    abort_flag            SMALLINT,
    local_debug            SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "help_classic_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_help_classic_scroll2 CURSOR FOR 
    SELECT  *
      FROM  qxt_help_classic

  LET int_flag = FALSE
  LET row_count = 0

  FOREACH c_help_classic_scroll2 INTO l_help_classic_rec.*
    CALL fgl_list_set(cb_field_name,row_count, l_help_classic_rec.filename)
    IF local_debug THEN
      DISPLAY "help_classic_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_help_classic_rec.filename), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

	LET row_count = row_count - 1
  RETURN row_count

END FUNCTION


###################################################################################
# FUNCTION help_classic_id_combo_list(cb_field_name)
#
# Populates the combo box with help_classic ids
#
# RETURN NONE
###################################################################################
FUNCTION help_classic_id_combo_list(cb_field_name)
  DEFINE 
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    l_help_classic_rec RECORD LIKE qxt_help_classic.*, 
    #l_help_classic_arr DYNAMIC ARRAY OF t_qxt_help_classic_rec,
    row_count     INTEGER,
    current_row   INTEGER,
    abort_flag   SMALLINT,
    local_debug   SMALLINT

  LET local_debug = FALSE
 
  IF local_debug THEN
    DISPLAY "id_combo_list() - cb_field_name =", cb_field_name
  END IF

  #LET rv = NULL
  LET abort_flag = FALSE

  DECLARE c_help_classic_scroll4 CURSOR FOR 
    SELECT filename 
      FROM qxt_help_classic

  LET int_flag = FALSE
  LET row_count = 0

  FOREACH c_help_classic_scroll4 INTO l_help_classic_rec.*
    CALL fgl_list_set(cb_field_name,row_count, l_help_classic_rec.id)
    IF local_debug THEN
      DISPLAY "id_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(row_count), ",", trim(l_help_classic_rec.id), ")"
    END IF
    LET row_count = row_count + 1
  END FOREACH

	LET row_count = row_count - 1
  RETURN row_count

END FUNCTION




############################################################
# FUNCTION set_help_file(h_file)
#
# Set the help file (options help file) depening on the language
#
# RETURN NONE
############################################################
FUNCTION set_classic_help_file(h_file_id)
  DEFINE
    h_file      VARCHAR(200),
    h_file_id   SMALLINT,
    lang        SMALLINT,
    local_debug SMALLINT,
    err_msg     VARCHAR(200)

  LET local_debug = FALSE

  LET qxt_previous_help_file_id = qxt_current_help_file_id
  LET lang = get_language()
  LET qxt_current_help_file_id = h_file_id

  IF h_file_id >= 1 AND h_file_id <= 10 THEN

    IF get_help_classic_multi_lang() = 0 THEN
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, ".erm"
    ELSE
      LET h_file = get_msg_path(get_help_classic_filename(h_file_id)) CLIPPED, "_", trim(get_language_dir(lang)), ".erm"
    END IF
    
    IF NOT fgl_test("e",h_file) THEN
      LET err_msg = get_str_tool(727), " ",  h_file
      CALL fgl_winmessage(get_str_tool(30),err_msg, "error")
    ELSE
      OPTIONS HELP FILE h_file
      IF local_debug THEN
        DISPLAY "OPTIONS HELP FILE ", h_file
      END IF
    END IF

  ELSE
    LET err_msg = get_str_tool(802), "\nset_classic_help_file(h_file_id)\nh_file_id = ", h_file_id
    CALL fgl_winmessage(get_str_tool(30), err_msg, "error")
  END IF



END FUNCTION




############################################################
# FUNCTION set_previous_classic_help()
#
# Switch back to the last used help file (id)
#
# RETURN NONE
############################################################
FUNCTION set_previous_classic_help()

  CALL  set_classic_help_file(get_previous_classic_help())

END FUNCTION



############################################################
# FUNCTION set_previous_classic_help()
#
# Switch back to the last used help file (id)
#
# RETURN NONE
############################################################
FUNCTION get_previous_classic_help()

  RETURN qxt_previous_help_file_id

END FUNCTION


############################################################
# FUNCTION get_current_help_file_id()
#
# Return the currently active help file id
#
# RETURN NONE
############################################################
FUNCTION get_current_classic_help()

  RETURN qxt_current_help_file_id

END FUNCTION




############################################################
# FUNCTION refresh_classic_help()
#
# Refresh classic help file with the current id (required for changes i.e. in language)
#
# RETURN NONE
############################################################
FUNCTION refresh_classic_help()

  CALL set_classic_help_file(qxt_current_help_file_id)

END FUNCTION

