##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the support of the industry_type
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                 RETURN VALUE:
# industry_type_combo_list(cb_field_name)          populates a combo box with DB data           NONE
# get_industry_type_id(p_itype_name)               return ind_type_id based on type_name        l_type_id  (LIKE industry_type.type_id)
# get_industry_type_name(p_type_id)                return ind_type_name                         l_itype_name (LIKE industry_type.itype_name)
# industry_type_popup(p_ind_type_id)               popup selection for ind. type selection      l_industry_type_arr[i].type_id OR p_ind_type_id
# industry_type_popup_data_source                  Data Source (cursor) for                     NONE
#   (p_order_field,p_ord_dir)                      industry_type_popup()
# industry_type_create()                           create new ind. type record                  NONE
# industry_type_delete(p_type_id)                  delete ind. type record                      NONE
# industry_type_edit(p_type_id)                    edit ind. type record                        NONE
# industry_type_view_by_rec(p_industry_type_rec)   Display ind_type details on a form           NONE
# industry_type_view(p_industry_type_rec)          Display ind_type details on a form using ID  NONE
# industry_type_input(p_industry_type)             input ind. type details                      l_industry_type.*
# grid_header_ind_type_scroll()                    Populates grid header labels                 NONE
############################################################################################################


#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"

#################################################
# FUNCTION industry_type_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for industry_type_popup()
#
# RETURN NONE
#################################################
FUNCTION industry_type_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "type_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

   
  LET sql_stmt = "SELECT * FROM industry_type "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_ind_type FROM sql_stmt
  DECLARE c_ind_type CURSOR FOR p_ind_type

END FUNCTION


#################################################
# FUNCTION industry_type_popup(p_ind_type_id,p_order_field,p_accept_action)
#################################################
FUNCTION industry_type_popup(p_ind_type_id,p_order_field,p_accept_action)
  DEFINE  
    p_ind_type_id                LIKE industry_type.type_id,
    l_industry_type_arr          DYNAMIC ARRAY OF RECORD LIKE industry_type.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT--,
    #l_ord_dir                    SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

    CALL fgl_window_open("w_ind_type_scroll", 2, 8, get_form_path("f_ind_type_scroll_l2") , FALSE) 
    CALL populate_industry_type_scroll_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/industry/building_factory_1-type.png")
	CALL ui.Interface.setText("Industry Type")


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL industry_type_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_ind_type INTO l_industry_type_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1
    IF NOT i THEN
      CALL fgl_window_close("w_ind_type_scroll")
      RETURN NULL
    END IF

		IF i > 0 THEN
			CALL l_industry_type_arr.resize(i)  --correct the last element of the dynamic array
		END IF   
	
		
    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_industry_type_arr TO sa_ind_type.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      BEFORE DISPLAY
        CALL publish_toolbar("IndustryTypeList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_industry_type_arr[i].type_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL industry_type_view(get_industry_type_rec(i))
            EXIT DISPLAY

          WHEN 2  --edit
            CALL industry_type_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL industry_type_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL industry_type_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","industry_type_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "industry_type_popup(p_ind_type_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("industry_type_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL industry_type_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_industry_type_arr[i].type_id
        CALL industry_type_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_industry_type_arr[i].type_id
        CALL industry_type_delete(i)
        EXIT DISPLAY
    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "type_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "itype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "user_def"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_ind_type_scroll")

  IF NOT int_flag THEN
    LET i = arr_curr()
    RETURN l_industry_type_arr[i].type_id
  ELSE 
    #RETURN NULL
    RETURN p_ind_type_id --Return the original function argument value if user aborts selection
  END IF

END FUNCTION

#################################################
# FUNCTION industry_type_create()
#################################################
FUNCTION industry_type_create()
  DEFINE l_industry_type RECORD LIKE industry_type.*

  CALL industry_type_input(l_industry_type.*)
    RETURNING l_industry_type.*

  IF NOT int_flag THEN 
    LET l_industry_type.type_id = 0
    INSERT INTO industry_type VALUES (l_industry_type.*)
  END IF
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate

#################################################
# FUNCTION industry_type_delete(p_type_id)
#################################################
FUNCTION industry_type_delete(p_type_id)
  DEFINE p_type_id INTEGER
  #Are you sure you want to delete this Industry Type?
  IF yes_no(get_str(55),get_str(56)) THEN
    DELETE FROM industry_type WHERE type_id = p_type_id
    LET tmp_str = get_str(1761), " ", p_type_id
    CALL fgl_winmessage(get_str(817),tmp_str,"info")
  END IF

END FUNCTION

#################################################
# FUNCTION industry_type_edit(p_type_id)
#################################################
FUNCTION industry_type_edit(p_type_id)
  DEFINE 
    p_type_id       INTEGER,
    l_industry_type RECORD LIKE industry_type.*

  SELECT * 
    INTO l_industry_type
    FROM industry_type
    WHERE industry_type.type_id = p_type_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL industry_type_input(l_industry_type.*) 
    RETURNING l_industry_type.*

  IF NOT int_flag THEN
    UPDATE industry_type
      SET itype_name = l_industry_type.itype_name,
          user_def = l_industry_type.user_def
      WHERE industry_type.type_id = l_industry_type.type_id
  END IF
END FUNCTION

#################################################
# FUNCTION industry_type_input(p_industry_type)
#
# Input details from form (create_edit)
#
# RETURN l_industry_type.*
#################################################
FUNCTION industry_type_input(p_industry_type)
  DEFINE 
    p_industry_type RECORD LIKE industry_type.*,
    l_industry_type RECORD LIKE industry_type.*

  LET l_industry_type.* = p_industry_type.*

    CALL fgl_window_open("w_ind_type", 2, 8, get_form_path("f_ind_type_l2"), FALSE) 
    CALL populate_industry_type_form_labels_g()

  INPUT BY NAME l_industry_type.itype_name WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_industry_type.itype_name IS NULL THEN
          #Input Error","The 'Industry Type' (name) field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1762),"error")
          CONTINUE INPUT
        END IF
      ELSE
        IF NOT yes_no(get_str(60),get_Str(61)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT


  CALL fgl_window_close("w_ind_type")

  #IF int_flag THEN
  #  LET int_flag = FALSE
  #END IF

  RETURN l_industry_type.*
END FUNCTION

#################################################
# FUNCTION industry_type_view(p_industry_type_rec)
#
# Display ind_type details on a form using ID
#
# RETURN NONE
#################################################
FUNCTION industry_type_view(p_industry_type_id)
  DEFINE 
    p_industry_type_id LIKE industry_type.type_id,
    l_industry_type_rec RECORD LIKE industry_type.*


  CALL get_industry_type(p_industry_type_id) RETURNING l_industry_type_rec.*
  CALL industry_type_view_by_rec(l_industry_type_rec.*)

END FUNCTION


#################################################
# FUNCTION industry_type_view_by_rec(p_industry_type_rec)
#
# Display ind_type details on a form
#
# RETURN NONE
#################################################
FUNCTION industry_type_view_by_rec(p_industry_type_rec)
  DEFINE 
    p_industry_type_rec RECORD LIKE industry_type.*,
    inp_char CHAR


    CALL fgl_window_open("w_ind_type", 2, 8, get_form_path("f_ind_type_l2"), FALSE) 
    CALL populate_industry_type_form_labels_g()

   DISPLAY BY NAME p_industry_type_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 3

      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
   
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_ind_type")


END FUNCTION


