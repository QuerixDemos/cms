##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage stock item
##################################################################################################################
#
# FUNCTION:                                   DESCRIPTION:                                        RETURN:
# get_stock_item(p_stock_id)                  Get stock item record from id                       l_stock_item.*
# get_stock_item_tax(p_stock_id)              get tax rate of a stock item                        l_tax_rate
# get_stock_item_desc(p_stock_id)             get stock item name/description of a stock item     l_item_desc
# check_stock_id(p_stock_id)                  Check if stock_id exists                            i  (1 yes  0 not)
# stock_item_popup_data_source(p_order_field) Data Source Cursor for the stock_item_popup array   NONE
# stock_item_popup(p_stock_id,p_order_field)  Display stock item list and return selected it      l_stock_arr[i].stock_id (or p_stock_id for cancel)
# stock_item_create()                         Create new stock item record                        l_stock_item.stock_id
# stock_item_input(p_stock_item)              Form input for new/edit stock item                  p_stock_item.*
# stock_item_edit(p_stock_id)                 Edit stock item record                              NONE
# stock_item_delete(p_stock_id)               Delete stock item                                   NONE
# stock_item_to_form_stock(p_stock_item)      ????                                                l_stock_rec.*
# stock_item_view(p_stock_id)                 View stock item record                              NONE
# grid_header_f_stock_scroll_g()              Poupulates the grid columns                         NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


###################################################
# FUNCTION stock_item_popup_data_source(p_order_field)
#
# Data Source Cursor for the stock_item_popup display array
#
# RETURN NONE
###################################################
FUNCTION stock_item_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    i INTEGER,
    p_order_field       VARCHAR(128),
    local_debug         SMALLINT,
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)
    #l_stock_id,p_stock_id LIKE stock_item.stock_id

  IF p_order_field IS NULL  THEN
    LET p_order_field = "stock_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT stock_item.stock_id,                   ",
                 "       stock_item.item_desc,                  ",
                 "       stock_item.item_cost,                  ",
                 "       tax_rates.tax_rate,                    ",
                 "       stock_item.quantity,                   ",
                 "       0 reserved,                            ",
                 "       0 avaliable,                           ",
                 "       0 ordered                              ",
                 "  FROM stock_item, tax_rates                  ",
                 " WHERE stock_item.rate_id = tax_rates.rate_id "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p1 FROM sql_stmt
  DECLARE c_stock_item CURSOR FOR p1

END FUNCTION


###################################################
# FUNCTION stock_item_popup(p_stock_id,p_order_field)
#
# Display stock item list and return selected it
#
# RETURN l_stock_arr[i].stock_id (or p_stock_id for cancel)
###################################################
FUNCTION stock_item_popup(p_stock_id,p_order_field,p_accept_action)
  DEFINE l_stock_id,p_stock_id        LIKE stock_item.stock_id,
         l_stock_arr                  DYNAMIC ARRAY OF t_stock_rec, 
         i, l_count                   INTEGER,
         local_debug                  SMALLINT,
         col_sort                     SMALLINT,   --is set to true if the column should be sorted
         p_accept_action              SMALLINT,
         err_msg                      VARCHAR(240),
         p_order_field,p_order_field2 VARCHAR(128)

  LET local_debug = FALSE  --1=on 0=off

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

  LET i = 1

  IF local_debug = 1 THEN
    DISPLAY "stock_item_popup() - p_account_id=",p_stock_id
    DISPLAY "stock_item_popup() - p_order_field=",p_order_field
  END IF

    IF NOT fgl_window_open("w_stock_scroll", 2, 8, get_form_path("f_stock_scroll_l2"), FALSE) THEN
      CALL fgl_winmessage("Error","stock_item_popup()\nCannot open window w_stock_scroll","error")
      RETURN p_stock_id
    END IF
    CALL populate_stock_item_list_form_labels_g()


	CALL ui.Interface.setImage("qx://application/icon16/industry/building_storage/building_storage.png")
	CALL ui.Interface.setText("Stock")
	
	
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL stock_item_popup_data_source(p_order_field,get_toggle_switch())
    LET i = 1

    FOREACH c_stock_item INTO l_stock_arr[i].*

      SELECT SUM(il.quantity)
        INTO l_stock_arr[i].reserved
        FROM invoice_line il, invoice ie
       WHERE ie.invoice_id = il.invoice_id
         AND ie.status in (1, 3, 4)
         AND TODAY < ie.invoice_date + 3
         AND il.stock_id = l_stock_arr[i].stock_id 

      SELECT SUM(sl.quantity)
        INTO l_stock_arr[i].ordered
        FROM supplies_line sl, supplies s
       WHERE s.suppl_id = sl.suppl_id
         AND s.state in (1)
         AND sl.stock_id = l_stock_arr[i].stock_id

      IF l_stock_arr[i].ordered IS NULL THEN
        LET l_stock_arr[i].ordered = 0
      END IF

      IF l_stock_arr[i].reserved IS NULL THEN
        LET l_stock_arr[i].reserved = 0
      END IF

      LET l_stock_arr[i].available = l_stock_arr[i].quantity - l_stock_arr[i].reserved

      IF l_stock_arr[i].available < 0 THEN
        LET l_stock_arr[i].available = 0
      END IF

      IF l_stock_arr[i].available IS NULL THEN
        LET l_stock_arr[i].available = l_stock_arr[i].quantity
      END IF

      LET i = i + 1

      IF i > 500 THEN
        EXIT FOREACH
      END IF

    END FOREACH

    LET i = i - 1

		If i > 0 THEN
			CALL l_stock_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF      
		
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_stock_arr TO sa_stock_item.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
 
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET l_stock_id = l_stock_arr[i].stock_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL stock_item_view(get_industry_type_rec(l_stock_id))
            EXIT DISPLAY

          WHEN 2  --edit
            CALL stock_item_edit(l_stock_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL stock_item_delete(l_stock_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL stock_item_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","stock_item_popup()\n Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "stock_item_popup(p_stock_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("stock_item_popup() - 4GL Source Error",err_msg, "error") 
      
        END CASE


      ON KEY (F3) -- view
        LET i = arr_curr()
        LET l_stock_id = l_stock_arr[i].stock_id
        CALL stock_item_view(l_stock_id)
        EXIT DISPLAY 
      ON KEY (F4) -- add
        CALL stock_item_create()
        EXIT DISPLAY
      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_stock_id = l_stock_arr[i].stock_id
        CALL stock_item_edit(l_stock_id)
        EXIT DISPLAY
      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_stock_id = l_stock_arr[i].stock_id
        CALL stock_item_delete(l_stock_id)
        EXIT DISPLAY


      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "stock_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "item_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "item_cost"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "tax_rate"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_stock_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_stock_id
  ELSE
    LET i = arr_curr()
    RETURN l_stock_arr[i].stock_id
  END IF
END FUNCTION


###################################################
# FUNCTION stock_item_create()
#
# Create new stock item record
#
# RETURN l_stock_item.stock_id
###################################################
FUNCTION stock_item_create()
  DEFINE l_stock_item RECORD LIKE stock_item.*

  LET l_stock_item.rate_id = 1
  LET l_stock_item.stock_id = "XXXXXXXXXX"

  CALL stock_item_input(l_stock_item.*)
    RETURNING l_stock_item.*

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  END IF

  INSERT INTO stock_item VALUES (l_stock_item.*)

  LET l_stock_item.stock_id = sqlca.sqlerrd[2]

  RETURN l_stock_item.stock_id
END FUNCTION


###################################################
# FUNCTION stock_item_input(p_stock_item)
#
# Form input for new/edit stock item
#
# RETURN p_stock_item.*
###################################################
FUNCTION stock_item_input(p_stock_item)
  DEFINE 
    p_stock_item RECORD LIKE stock_item.*,
    l_tax_rate   RECORD LIKE tax_rates.*,
    l_stock_rec  OF t_stock_rec2,
    i            INTEGER,
    l_addit_info RECORD
                   quantity   INTEGER,
                   reserved   INTEGER,
                   available  INTEGER,
                   required   INTEGER,
                   shot_term  INTEGER,
                   long_term  INTEGER
                 END RECORD

  LET int_flag = FALSE

  CALL stock_item_to_form_stock(p_stock_item.*)
    RETURNING l_stock_rec.*

    IF NOT fgl_window_open("w_stock_item", 4, 4, get_form_path("f_stock_item_det_l2"), FALSE) THEN
      CALL fgl_winmessage("Error","stock_item_input()\nCannot open window w_stock_item","error")
      RETURN p_stock_item.*

    END IF
    CALL populate_stock_item_form_edit_labels_g()


  CALL get_stock_item_info(p_stock_item.stock_id) RETURNING l_addit_info.*
  DISPLAY BY NAME l_addit_info.*

  INPUT BY NAME l_stock_rec.* WITHOUT DEFAULTS
    ON KEY (F10)
      IF INFIELD(tax_rate) THEN
        CALL tax_rate_popup(p_stock_item.rate_id,NULL,0)  --tax_rate_popup(p_rate_id,p_order_field,p_accept_action)
          RETURNING p_stock_item.rate_id

        CALL get_tax_rate_rec(p_stock_item.rate_id)
          RETURNING l_tax_rate.*
         
        LET l_stock_rec.tax_rate = l_tax_rate.tax_rate
        LET l_stock_rec.tax_desc = l_tax_rate.tax_desc
        DISPLAY BY NAME l_stock_rec.tax_rate, l_stock_rec.tax_desc
      END IF

    AFTER FIELD stock_id
      IF l_stock_rec.stock_id <> p_stock_item.stock_id THEN
        IF check_stock_id(l_stock_rec.stock_id) THEN
          CALL fgl_message_box("Stock item " || l_stock_rec.stock_id CLIPPED || " already exists")
          NEXT FIELD stock_id
        END IF
      END IF

    AFTER INPUT
      IF NOT int_flag THEN
        IF l_stock_rec.item_desc IS NULL THEN
          #The 'Stock Item Description' (name) field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1320),"error")
          CONTINUE INPUT

        END IF
        IF l_stock_rec.item_cost IS NULL THEN
          #The 'Stock Item Cost'  field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1321),"error")
          CONTINUE INPUT

        END IF
        IF l_stock_rec.tax_rate IS NULL THEN
          #The 'Stock Item Tax Rate'  field must not be empty !
          CALL fgl_winmessage(get_str(48),get_str(1322),"error")
          CONTINUE INPUT
        END IF

      ELSE
        #Cancel Record Creation/Modification   
        #Do you really want to abort the new/modified record entry ?
        IF NOT yes_no(get_str(1323),get_str(1324)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF


  END INPUT

  LET p_stock_item.item_desc = l_stock_rec.item_desc
  LET p_stock_item.item_cost = l_stock_rec.item_cost
  LET p_stock_item.stock_id = l_stock_rec.stock_id

  CALL fgl_window_close("w_stock_item")

  RETURN p_stock_item.*
END FUNCTION





###################################################
# FUNCTION stock_item_edit(p_stock_id)
#
# Edit stock item record
#
# RETURN NONE
###################################################
FUNCTION stock_item_edit(p_stock_id)
  DEFINE 
    p_stock_id   LIKE stock_item.stock_id,
    l_stock_item RECORD LIKE stock_item.*

  CALL get_stock_item(p_stock_id)
    RETURNING l_stock_item.*

  CALL stock_item_input(l_stock_item.*)
    RETURNING l_stock_item.*

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF

  UPDATE stock_item
    SET stock_id = l_stock_item.stock_id,
        item_desc = l_stock_item.item_desc,
        item_cost = l_stock_item.item_cost,
        rate_id = l_stock_item.rate_id
    WHERE stock_item.stock_id = p_stock_id
END FUNCTION

###################################################
# FUNCTION stock_item_view(p_stock_id)
#
# View stock item record
#
# RETURN NONE
###################################################
FUNCTION stock_item_view(p_stock_id)
  DEFINE 
    p_stock_id   LIKE stock_item.stock_id,
    l_stock_item RECORD LIKE stock_item.*

  CALL get_stock_item(p_stock_id)
    RETURNING l_stock_item.*

  CALL stock_item_display(l_stock_item.*)

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF


END FUNCTION


###################################################
# FUNCTION stock_item_display(p_stock_item)
#
# Form view for stock item
#
# RETURN p_stock_item.*
###################################################
FUNCTION stock_item_display(p_stock_item)
  DEFINE 
    char_inp        CHAR,
    p_stock_item    RECORD LIKE stock_item.*,
    l_tax_rate      RECORD LIKE tax_rates.*,
    l_stock_rec     OF t_stock_rec2,
    i               INTEGER,
    l_addit_info    RECORD
                      quantity   INTEGER,
                      reserved   INTEGER,
                      available  INTEGER,
                      required   INTEGER,
                      shot_term  INTEGER,
                      long_term  INTEGER
                    END RECORD

  LET int_flag = FALSE

  CALL stock_item_to_form_stock(p_stock_item.*)
    RETURNING l_stock_rec.*

    IF NOT fgl_window_open("w_stock_item", 4, 4, get_form_path("f_stock_item_det_l2"),FALSE) THEN 
      CALL fgl_winmessage("Error","stock_item_display()\nCannot open window w_stock_item","error")
      RETURN

    END IF
    CALL populate_stock_item_form_labels_g()



  DISPLAY BY NAME l_stock_rec.*
  CALL get_stock_item_info(p_stock_item.stock_id) RETURNING l_addit_info.*
  DISPLAY BY NAME l_addit_info.*

  WHILE TRUE
    PROMPT "" FOR char_inp HELP 2
      ON KEY(INTERRUPT,ACCEPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_stock_item")

END FUNCTION



###################################################
# FUNCTION stock_item_delete(p_stock_id)
#
# Delete stock item
#
# RETURN NONE
###################################################
FUNCTION stock_item_delete(p_stock_id)
  DEFINE p_stock_id LIKE stock_item.stock_id

  #IF yes_no("Delete Record?","Are you sure you want to delete this stock item ?") THEN
  IF yes_no(get_str(1325),get_str(1326)) THEN
    DELETE FROM stock_item
      WHERE stock_item.stock_id = p_stock_id
  END IF

END FUNCTION


###################################################
# FUNCTION stock_item_to_form_stock(p_stock_item)
#
# ????
#
# RETURN l_stock_rec.*
###################################################
FUNCTION stock_item_to_form_stock(p_stock_item)
  DEFINE 
    p_stock_item RECORD LIKE stock_item.*,
    l_tax_rates  RECORD LIKE tax_rates.*,
    l_stock_rec  OF t_stock_rec2

  CALL get_tax_rate_rec(p_stock_item.rate_id)
    RETURNING l_tax_rates.*

  LET l_stock_rec.stock_id = p_stock_item.stock_id
  LET l_stock_rec.item_desc = p_stock_item.item_desc
  LET l_stock_rec.item_cost = p_stock_item.item_cost
  LET l_stock_rec.tax_rate = l_tax_rates.tax_rate
  LET l_stock_rec.tax_desc = l_tax_rates.tax_desc

  RETURN l_stock_rec.*
END FUNCTION

