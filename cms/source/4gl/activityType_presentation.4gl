##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_activity_type_popup()
#
# Populate the grid header of the activity type popup grid
#
# RETURN NONE
####################################################
FUNCTION grid_header_activity_type_popup()
  CALL fgl_grid_header("s_act_arr","type_id",get_str(2091),"right","F13")    --"ID:"
  CALL fgl_grid_header("s_act_arr","atype_name",get_str(2092),"left","F14")  --"Name:"
  CALL fgl_grid_header("s_act_arr","user_def",get_str(2093),"left","F15")    --"U:"

END FUNCTION


#######################################################
# FUNCTION populate_act_type_form_labels_t()
#
# Populate activity type form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_act_type_form_labels_t()
  DISPLAY get_str(2080) TO lbTitle
  DISPLAY get_str(2081) TO dl_f1
  DISPLAY get_str(2082) TO dl_f2
  DISPLAY get_str(2083) TO dl_f3
  #DISPLAY get_str(2084) TO dl_f4
  #DISPLAY get_str(2085) TO dl_f5
  #DISPLAY get_str(2086) TO dl_f6
  #DISPLAY get_str(2087) TO dl_f7
  #DISPLAY get_str(2088) TO dl_f8
  DISPLAY get_str(2089) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_act_type_form_labels_g()
#
# Populate activity type form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_act_type_form_labels_g()
  DISPLAY get_str(2080) TO lbTitle
  DISPLAY get_str(2081) TO dl_f1
  DISPLAY get_str(2082) TO dl_f2
  DISPLAY get_str(2083) TO dl_f3
  #DISPLAY get_str(2084) TO dl_f4
  #DISPLAY get_str(2085) TO dl_f5
  #DISPLAY get_str(2086) TO dl_f6
  #DISPLAY get_str(2087) TO dl_f7
  #DISPLAY get_str(2088) TO dl_f8
  DISPLAY get_str(2089) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL fgl_settitle(get_str(2080))

END FUNCTION




#######################################################
# FUNCTION populate_act_type_list_form_labels_t()
#
# Populate activity type list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_act_type_list_form_labels_t()
  DISPLAY get_str(2090) TO lbTitle
  DISPLAY get_str(2091) TO dl_f1
  DISPLAY get_str(2092) TO dl_f2
  DISPLAY get_str(2093) TO dl_f3
  #DISPLAY get_str(2094) TO dl_f4
  #DISPLAY get_str(2095) TO dl_f5
  #DISPLAY get_str(2096) TO dl_f6
  #DISPLAY get_str(2097) TO dl_f7
  #DISPLAY get_str(2098) TO dl_f8
  DISPLAY get_str(2099) TO lbInfo1

END FUNCTION






#######################################################
# FUNCTION populate_act_type_list_form_labels_g()
#
# Populate activity type list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_act_type_list_form_labels_g()
  DISPLAY get_str(2090) TO lbTitle
  #DISPLAY get_str(2091) TO dl_f1
  #DISPLAY get_str(2092) TO dl_f2
  #DISPLAY get_str(2093) TO dl_f3
  #DISPLAY get_str(2094) TO dl_f4
  #DISPLAY get_str(2095) TO dl_f5
  #DISPLAY get_str(2096) TO dl_f6
  #DISPLAY get_str(2097) TO dl_f7
  #DISPLAY get_str(2098) TO dl_f8
  DISPLAY get_str(2099) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_del
  DISPLAY "!" TO bt_del

  CALL fgl_settitle(get_str(2090))
  CALL grid_header_activity_type_popup()


END FUNCTION


