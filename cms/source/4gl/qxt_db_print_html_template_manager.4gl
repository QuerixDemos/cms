##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_language_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_print_html_template_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_print_html_template_manager_lib_info()
  RETURN "qxt_print_html_template_manager"
END FUNCTION 



######################################################################################################################
# Data Access functins
######################################################################################################################

{
###########################################################
# FUNCTION get_help_url_file(p_help_id,p_filename)
#
# Get the help_file from the server to the client and return the client file name inc. path
#
# RETURN p_client_file_path
###########################################################
FUNCTION get_help_url_file(p_help_id,p_filename)
  DEFINE
    p_help_id          INTEGER,
    #p_language_id      INTEGER,
    p_filename         VARCHAR(200),
    l_client_filename_with_path  VARCHAR(200),
    l_server_filename_with_path  VARCHAR(200),
    local_debug        SMALLINT,
    tmp_msg            VARCHAR(200)


  IF local_debug THEN
    DISPLAY "get_help_url_file() - p_help_id=",                            p_help_id
    DISPLAY "get_help_url_file() - language_id=",                          get_language()
    DISPLAY "get_help_url_file() - p_filename=",                           p_filename
    DISPLAY "get_help_url_file() - l_client_filename_with_path=",          l_client_filename_with_path
    DISPLAY "get_help_url_file() - l_server_filename_with_path=",          l_server_filename_with_path
  END IF  

  LET l_client_filename_with_path = get_client_temp_path(get_print_template_base_dir_path(get_language_dir_path(p_filename)))
  LET l_server_filename_with_path = get_print_template_base_dir_path(get_language_dir_path(p_filename))

  IF local_debug THEN
    DISPLAY "get_help_url_file() - p_help_id=",                            p_help_id
    DISPLAY "get_help_url_file() - language_id=",                          get_language()
    DISPLAY "get_help_url_file() - p_filename=",                           p_filename
    DISPLAY "get_help_url_file() - l_client_filename_with_path=",          l_client_filename_with_path
    DISPLAY "get_help_url_file() - l_server_filename_with_path=",          l_server_filename_with_path
  END IF  

  #Only download the file, if it doesn't already exist already on client - no version control
  IF fgl_getproperty("gui","system.file.exists",l_client_filename_with_path) IS NULL THEN 

    IF local_debug THEN
      DISPLAY "1a - get_help_url_file() File does not exist on client - Let's attempt to download the file..."
    END IF

   # Download file from database - blob to app server
   LET l_client_filename_with_path = download_blob_print_template_to_client(p_help_id,l_client_filename_with_path,0)

  ELSE
    IF local_debug THEN
      DISPLAY "1b - DID NOT download the file - It's already on the local Client PC..."
    END IF
  END IF      

  RETURN l_client_filename_with_path

END FUNCTION

}
#########################################################
# FUNCTION get_template_id(p_filename,p_language_id)
#
# get print_template id from name
#
# RETURN l_template_id
#########################################################
FUNCTION get_template_id(p_filename,p_language_id)
  DEFINE 
    p_filename       LIKE qxt_print_template.filename,
    p_language_id    LIKE qxt_print_template.language_id,
    l_template_id    LIKE qxt_print_template.template_id,
    local_debug      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_template_id() - p_filename = ", p_filename
  END IF

  SELECT template_id
    INTO l_template_id
    FROM qxt_print_template
    WHERE filename = p_filename
      AND language_id   = p_language_id

  RETURN l_template_id
END FUNCTION

#########################################################
# FUNCTION get_template_data(p_template_id,p_language_id)
#
# Get the print_template TEXT description
#
# RETURN l_template_data
#########################################################
FUNCTION get_template_data(p_template_id,p_language_id)
  DEFINE 
    p_language_id    LIKE qxt_print_template.language_id,
    p_template_id    LIKE qxt_print_template.template_id,
    l_template_data  LIKE qxt_print_template.template_data,
    local_debug      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_template_data() - p_template_id=", p_template_id
    DISPLAY "get_template_data() - p_language_id=", p_language_id
  END IF

  LOCATE l_template_data IN MEMORY

  SELECT template_data
    INTO l_template_data
    FROM qxt_print_template
    WHERE template_id = p_template_id
      AND language_id   = p_language_id

  RETURN l_template_data
END FUNCTION


######################################################
# FUNCTION get_print_template_rec(p_template_id,p_language_id)
#
# Get the print_template record from pa_method_id
#
# RETURN l_print_template.*
######################################################
FUNCTION get_print_template_rec(p_template_id,p_language_id)
  DEFINE 
    p_template_id  LIKE qxt_print_template.template_id,
    p_language_id   LIKE qxt_print_template.language_id,
    l_print_template     RECORD LIKE qxt_print_template.*

  LOCATE l_print_template.template_data IN MEMORY

  SELECT qxt_print_template.*
    INTO l_print_template.*
    FROM qxt_print_template
    WHERE qxt_print_template.template_id = p_template_id
      AND qxt_print_template.language_id   = p_language_id

  RETURN l_print_template.*
END FUNCTION



######################################################################################################################
# List Management and Combo List functins
######################################################################################################################

######################################################
# FUNCTION print_template_popup_data_source()
#
# Data Source (cursor) for print_template_popup
#
# RETURN NONE
######################################################
FUNCTION print_template_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "template_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_print_template.template_id, ",
                 "qxt_language.language_name, ",
                 "qxt_print_template.filename, ",
                 "qxt_print_template.mod_date ",
                 "FROM qxt_print_template, qxt_language ",
                 "WHERE qxt_print_template.language_id = qxt_language.language_id "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "print_template_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_print_template FROM sql_stmt
  DECLARE c_print_template CURSOR FOR p_print_template

END FUNCTION


######################################################
# FUNCTION print_template_popup(p_template_id,p_language_id,p_order_field,p_accept_action)
#
# print_template selection window
#
# RETURN p_template_id
######################################################
FUNCTION print_template_popup(p_template_id,p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_template_id                LIKE qxt_print_template.template_id,  --default return value if user cancels
    p_language_id                LIKE qxt_print_template.language_id,   --default return value if user cancels
    l_print_template_arr         DYNAMIC ARRAY OF t_qxt_print_template_no_blob_form_rec,    --RECORD LIKE qxt_print_template.*,  
    l_arr_size                   SMALLINT,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_template_id                LIKE qxt_print_template.template_id,
    l_language_name              LIKE qxt_language.language_name,
    l_language_id                LIKE qxt_language.language_id,
    l_filename                   LIKE qxt_print_template.filename,
    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    scroll_template_id           LIKE qxt_print_template.template_id,
    scroll_language_name         LIKE qxt_language.language_name,
    export_filename              VARCHAR(200),
    local_debug                  SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1
  LET l_arr_size = 100 -- due to dynamic array ...sizeof(l_print_template_arr)

  IF local_debug THEN
    DISPLAY "print_template_popup() - p_template_id=",p_template_id
    DISPLAY "print_template_popup() - p_order_field=",p_order_field
    DISPLAY "print_template_popup() - p_accept_action=",p_accept_action
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_print_template_scroll", 2, 8, get_form_path("f_qxt_print_template_scroll_g"),FALSE) 
    CALL populate_print_template_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_print_template_scroll", 2, 8, get_form_path("f_qxt_print_template_scroll_t"),FALSE) 
    CALL populate_print_template_list_form_labels_t()
  END IF


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL print_template_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_print_template INTO l_print_template_arr[i].*
      IF local_debug THEN
        DISPLAY "print_template_popup() - i=",i
        DISPLAY "print_template_popup() - l_print_template_arr[i].template_id=",l_print_template_arr[i].template_id
        DISPLAY "print_template_popup() - l_print_template_arr[i].language_name=",l_print_template_arr[i].language_name
        DISPLAY "print_template_popup() - l_print_template_arr[i].filename=",l_print_template_arr[i].filename
        DISPLAY "print_template_popup() - l_print_template_arr[i].mod_date=",l_print_template_arr[i].mod_date
    END IF

      IF l_print_template_arr[i].template_id = scroll_template_id THEN
        IF l_print_template_arr[i].language_name = scroll_language_name THEN
          LET current_row = i
          #DISPLAY current_row
        END IF
      END IF

      LET i = i + 1
			CALL l_print_template_arr.resize(i)
			
      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        EXIT FOREACH
      END IF

    END FOREACH

    LET i = i - 1

		If i > 0 THEN
			CALL l_print_template_arr.resize(i)-- resize dynamic array to remove last dirty element
		END IF    
		
		
    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_print_template_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "print_template_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_print_template_arr TO sc_print_template.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
       #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        CALL fgl_dialog_setcurrline(5,current_row)
 
      BEFORE ROW
        LET i = arr_curr()
        LET l_template_id = l_print_template_arr[i].template_id
        LET scroll_template_id = l_print_template_arr[i].template_id
        LET scroll_language_name = l_print_template_arr[i].language_name
        LET l_language_id = get_language_id(l_print_template_arr[i].language_name)
        LET l_filename = l_print_template_arr[i].filename

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)

        #LET i = l_print_template_arr[i].template_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL print_template_view(l_template_id, l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL print_template_edit(l_template_id, l_language_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL print_template_delete(l_template_id, l_language_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL print_template_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","print_template_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "print_template_popup(p_template_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("print_template_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL print_template_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        CALL print_template_edit(l_template_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL print_template_delete(l_template_id,l_language_id)
        EXIT DISPLAY

      ON KEY (F7) -- view
        CALL print_template_view(l_template_id, l_language_id)
        EXIT DISPLAY

      ON KEY(F8)  --export data for qxt_file library
        LET export_filename = get_server_app_temp_path("print_html_template_list.unl")

        UNLOAD TO export_filename
          SELECT 
              template_id,
              language_id,
              filename 
            FROM qxt_print_template 
            ORDER BY template_id, language_id ASC

        CALL file_download(export_filename,"print_html_template_list.unl", TRUE,"unl")

        EXIT DISPLAY

      ON KEY(F9)  --export data for qxt_file library
        CALL download_blob_print_template_to_client(l_template_id,l_language_id,l_filename,TRUE)
        EXIT DISPLAY

      #Grid Column Sort
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "template_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_print_template_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_template_id,  p_language_id 
  ELSE 
    RETURN l_template_id,  l_language_id 
  END IF

END FUNCTION


######################################################################################################################
# Edit/Create/Delete/Input Functions
######################################################################################################################


######################################################
# FUNCTION print_template_create()
#
# Create a new print_template record
#
# RETURN NULL
######################################################
FUNCTION print_template_create()
  DEFINE 
    l_print_template RECORD LIKE qxt_print_template.*

  LOCATE l_print_template.template_data IN MEMORY

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_det_g"), TRUE) 
    CALL populate_print_template_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_det_t"), TRUE) 
    CALL populate_print_template_form_create_labels_t()
  END IF

  LET l_print_template.template_id = 0

  LET int_flag = FALSE
  
  #Initialise some variables
  LET l_print_template.mod_date = TODAY
  LET l_print_template.language_id = 1

  # CALL the INPUT
  CALL print_template_input(l_print_template.*)
    RETURNING l_print_template.*


  IF NOT int_flag THEN
    INSERT INTO qxt_print_template VALUES (
      l_print_template.template_id,
      l_print_template.language_id, 
      l_print_template.filename,
      l_print_template.mod_date,
      l_print_template.template_data)
  END IF

  CALL fgl_window_close("w_print_template")
  FREE l_print_template.template_data

  IF NOT int_flag THEN
    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_print_template.template_id
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION print_template_edit(p_template_id)
#
# Edit print_template record
#
# RETURN NONE
#################################################
FUNCTION print_template_edit(p_template_id,p_language_id)
  DEFINE 
    p_template_id       LIKE qxt_print_template.template_id,
    p_language_id        LIKE qxt_print_template.language_id,
    l_key1_template_id  LIKE qxt_print_template.template_id,
    l_key2_language_id   LIKE qxt_print_template.language_id,
    #l_print_template_form_rec OF t_print_template_form_rec,
    l_print_template_rec      RECORD LIKE qxt_print_template.*,
    local_debug          SMALLINT

  #Not required - will be allocated in get_print_template_rec() 
  #but must be freed
  #LOCATE l_print_template_rec.template_data IN MEMORY

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "print_template_edit() - Function Entry Point"
    DISPLAY "print_template_edit() - p_template_id=", p_template_id
    DISPLAY "print_template_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key combination for the SQL update
  LET l_key1_template_id = p_template_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_print_template_rec(p_template_id,p_language_id) 
    RETURNING l_print_template_rec.*


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_det_g"), TRUE) 
    CALL populate_print_template_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_det_t"), TRUE) 
    CALL populate_print_template_form_edit_labels_t()
  END IF

  #Update the modification date
  LET l_print_template_rec.mod_date = TODAY

  #Call the INPUT
  CALL print_template_input(l_print_template_rec.*) 
    RETURNING l_print_template_rec.*

  IF local_debug THEN
    DISPLAY "print_template_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "print_template_edit() - l_print_template_rec.template_id=",l_print_template_rec.template_id
      DISPLAY "print_template_edit() - l_print_template_rec.language_id=",l_print_template_rec.language_id
      DISPLAY "print_template_edit() - l_print_template_rec.filename=",l_print_template_rec.filename
      DISPLAY "print_template_edit() - l_print_template_rec.mod_date=",l_print_template_rec.mod_date
      #DISPLAY "print_template_edit() - l_print_template_rec.template_data=",l_print_template_rec.template_data
    END IF

    UPDATE qxt_print_template
      SET 
          template_id =             l_print_template_rec.template_id,
          language_id =             l_print_template_rec.language_id,
          filename =                l_print_template_rec.filename,
          mod_date =                l_print_template_rec.mod_date,
          template_data =           l_print_template_rec.template_data
      WHERE qxt_print_template.template_id = l_key1_template_id
        AND qxt_print_template.language_id  = l_key2_language_id


    IF local_debug THEN
      DISPLAY "print_template_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

  END IF

  CALL fgl_window_close("w_print_template")
  FREE l_print_template_rec.template_data 


  #Check if user canceled
  IF NOT int_flag THEN

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_print_template_rec.template_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    LET int_flag = FALSE
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION print_template_input(p_print_template)
#
# Input print_template details (edit/create)
#
# RETURN l_print_template.*
#################################################
FUNCTION print_template_input(p_print_template_rec)
  DEFINE 
    p_print_template_rec       RECORD LIKE qxt_print_template.*,
    l_print_template_form_rec  OF t_qxt_print_template_form_rec,
    l_server_blob_file         VARCHAR(300),  --,
    l_orignal_filename         LIKE qxt_print_template.filename,
    l_orignal_template_id      LIKE qxt_print_template.template_id,
    local_debug                SMALLINT

  LET local_debug = FALSE
  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_filename = p_print_template_rec.filename
  LET l_orignal_template_id = p_print_template_rec.template_id

  #copy record data to form_record format record
  CALL copy_print_template_record_to_form_record(p_print_template_rec.*) 
    RETURNING l_print_template_form_rec.*

  ####################
  #Start actual INPUT
  INPUT BY NAME l_print_template_form_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
    ON KEY(F9)  --upload document to blob
      #upload_blob_print_template(p_client_file_path,p_dialog)
      CALL upload_blob_print_template(l_print_template_form_rec.filename,TRUE) 
        RETURNING l_server_blob_file --tRUE = file dialog

      IF local_debug THEN
        DISPLAY "print_template_input() - l_server_blob_file=", l_server_blob_file
      END IF

      IF l_server_blob_file IS NOT NULL AND fgl_test("e", l_server_blob_file)THEN
          # free the existing locator
          IF l_print_template_form_rec.template_data IS NOT NULL THEN
            FREE l_print_template_form_rec.template_data
          END IF
          #locate new blob data from file
          LOCATE l_print_template_form_rec.template_data IN FILE l_server_blob_file
          LET l_print_template_form_rec.filename = fgl_basename(l_server_blob_file)
          DISPLAY l_print_template_form_rec.filename TO filename
          DISPLAY l_print_template_form_rec.template_data TO template_data

          # DISPLAY BY NAME l_contact_rec.cont_picture
       END IF

    ON KEY(F10)  --download blob document to file
      CALL download_blob_print_template_to_client(l_print_template_form_rec.template_id,get_language_id(l_print_template_form_rec.language_name),l_print_template_form_rec.filename,TRUE)
      #Syntax CALL download_blob_print_template_to_client(p_template_id,p_language_id,p_client_file_path,p_dialog)


    AFTER FIELD filename
      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(973), l_print_template_form_rec.filename,NULL,TRUE)  THEN
        NEXT FIELD filename
      END IF

    AFTER FIELD language_name
      #Language must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(972), l_print_template_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD language_name
      END IF

    AFTER FIELD mod_date
      IF NOT validate_field_value_date_exists(get_str_tool(974), l_print_template_form_rec.mod_date,NULL,TRUE) THEN
        NEXT FIELD mod_date
      END IF


    # AFTER INPUT BLOCK
    AFTER INPUT
      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF
      #Language must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(972), l_print_template_form_rec.language_name,NULL,TRUE)  THEN
          NEXT FIELD language_name
          CONTINUE INPUT
        END IF
      #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(973), l_print_template_form_rec.filename,NULL,TRUE)  THEN

          NEXT FIELD filename
          CONTINUE INPUT
        END IF
      #Date must not be empty
      IF NOT validate_field_value_date_exists(get_str_tool(974), l_print_template_form_rec.mod_date,NULL,TRUE) THEN
        NEXT FIELD mod_date
        CONTINUE INPUT
      END IF
      #BLOB Data must be suplied, otherwise - the help system will not work correctly
      IF NOT validate_field_value_text_exists(get_str_tool(975), l_print_template_form_rec.template_data ,"Binary BLOB TEXT Data",TRUE) THEN
        #NEXT FIELD template_data --is blob viewer field - can't move the cursor into it
        NEXT FIELD filename
        CONTINUE INPUT
      END IF
      #The combination language_id and template_id must be unique
      IF template_id_and_lang_id_count(l_print_template_form_rec.template_id,get_language_id(l_print_template_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_template_id <> l_print_template_form_rec.template_id THEN  --it is not an edit operation
          CALL fgl_winmessage(get_str_tool(63),get_str_tool(64),"error")
          NEXT FIELD template_id
          CONTINUE INPUT
        END IF
      END IF

  END INPUT
  IF local_debug THEN
    DISPLAY "print_template_input() - l_print_template_form_rec.template_id=",   l_print_template_form_rec.template_id
    DISPLAY "print_template_input() - l_print_template_form_rec.language_name=", l_print_template_form_rec.language_name
    DISPLAY "print_template_input() - l_print_template_form_rec.filename=",      l_print_template_form_rec.filename
    DISPLAY "print_template_input() - l_print_template_form_rec.mod_date=",      l_print_template_form_rec.mod_date
    #DISPLAY "print_template_edit() - l_print_template_form_rec.template_data=", l_print_template_form_rec.template_data
  END IF


  #Copy the form record data to a normal print_template record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_print_template_form_record_to_record(l_print_template_form_rec.*) RETURNING p_print_template_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "print_template_input() - p_print_template_rec.template_id=",p_print_template_rec.template_id
    DISPLAY "print_template_input() - p_print_template_rec.language_id=",p_print_template_rec.language_id
    DISPLAY "print_template_input() - p_print_template_rec.filename=",p_print_template_rec.filename
    DISPLAY "print_template_input() - p_print_template_rec.mod_date=",p_print_template_rec.mod_date
    #DISPLAY "print_template_edit() - p_print_template_rec.template_data=",p_print_template_rec.template_data
  END IF

  RETURN p_print_template_rec.*

END FUNCTION


#################################################
# FUNCTION print_template_delete(p_template_id,p_language_id)
#
# Delete a print_template record
#
# RETURN NONE
#################################################
FUNCTION print_template_delete(p_template_id,p_language_id)
  DEFINE 
    p_template_id       LIKE qxt_print_template.template_id,
    p_language_id        LIKE qxt_print_template.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(980), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_print_template 
        WHERE template_id = p_template_id
          AND qxt_print_template.language_id   = p_language_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION print_template_view(p_template_id,p_language_id)
#
# View print_template record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION print_template_view(p_template_id,p_language_id)
  DEFINE 
    p_template_id       LIKE qxt_print_template.template_id,
    p_language_id        LIKE qxt_print_template.language_id,
    l_print_template_rec      RECORD LIKE qxt_print_template.*

  LOCATE l_print_template_rec.template_data IN MEMORY
 

  CALL get_print_template_rec(p_template_id,p_language_id) RETURNING l_print_template_rec.*
  CALL print_template_view_by_rec(l_print_template_rec.*)

  FREE l_print_template_rec.template_data

END FUNCTION


#################################################
# FUNCTION print_template_view_by_rec(p_print_template_rec)
#
# View print_template record in window-form
#
# RETURN NONE
#################################################
FUNCTION print_template_view_by_rec(p_print_template_rec)
  DEFINE 
    p_print_template_rec       RECORD LIKE qxt_print_template.*,
    l_print_template_form_rec  OF  t_qxt_print_template_no_blob_form_rec,
    inp_char         CHAR,
    tmp_str          VARCHAR(200),
    l_client_file_path VARCHAR(200)

  #Copy data to form record
  LET l_print_template_form_rec.template_id = p_print_template_rec.template_id
  LET l_print_template_form_rec.language_name = get_language_name(p_print_template_rec.language_id)
  LET l_print_template_form_rec.filename = p_print_template_rec.filename
  LET l_print_template_form_rec.mod_date = p_print_template_rec.mod_date

  #Initialise client file path with temp folder & language directory
  LET tmp_str = get_html_path(p_print_template_rec.filename)
  LET l_client_file_path = get_client_temp_path(tmp_str)
  #LET l_client_file_path = download_blob_help_html_to_client(p_help_html_rec.help_html_doc_id,p_help_html_rec.language_id,l_client_file_path,FALSE)
  LET l_client_file_path = download_blob_print_template_to_client(p_print_template_rec.template_id,p_print_template_rec.language_id,l_client_file_path,FALSE)


  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_view_g"), TRUE) 
    CALL populate_print_template_form_view_labels_g()

  ELSE
    CALL fgl_window_open("w_print_template", 3, 3, get_form_path("f_qxt_print_template_view_t"), TRUE) 
    CALL populate_print_template_form_view_labels_t()

  END IF

  DISPLAY BY NAME l_print_template_form_rec.*
  DISPLAY l_client_file_path TO url_preview
  DISPLAY l_client_file_path TO url


  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_print_template")
END FUNCTION


######################################################
# FUNCTION get_template_id_from_filename(p_filename)
#
# Get the template_id from a file
#
# RETURN l_template_id
######################################################
FUNCTION get_template_id_from_filename(p_filename,p_language)
  DEFINE 
    p_filename     LIKE qxt_print_template.filename,
    p_language     LIKE qxt_print_template.language_id,
    l_template_id LIKE qxt_print_template.template_id,
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_template_id() - filename = ", p_filename
  END IF

    SELECT qxt_print_template.template_id
      INTO l_template_id
      FROM qxt_print_template
      WHERE qxt_print_template.filename = p_filename
        AND qxt_print_template.language_id   = p_language

  IF local_debug THEN
    DISPLAY "get_template_id() - l_template_id = ", l_template_id
  END IF

  RETURN l_template_id

END FUNCTION


####################################################
# FUNCTION template_filename_count(p_filename,p_language_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION template_filename_count(p_filename,p_language_id)
  DEFINE
    p_filename    LIKE qxt_print_template.filename,
    p_language_id LIKE qxt_print_template.language_id,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_print_template
      WHERE qxt_print_template.filename = p_filename
        AND qxt_print_template.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION contact_name_count(p_cont_name)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION template_id_and_lang_id_count(p_template_id,p_language_id)
  DEFINE
    p_template_id    LIKE qxt_print_template.template_id,
    p_language_id     LIKE qxt_print_template.language_id,
    r_count           SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_print_template
      WHERE qxt_print_template.template_id = p_template_id
        AND qxt_print_template.language_id   = p_language_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION



######################################################
# FUNCTION upload_blob_print_template(p_contact_id, p_contact_name,remote_image_path)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION upload_blob_print_template(p_client_file,p_dialog)
  DEFINE 
    p_template_id          LIKE qxt_print_template.template_id,
    l_print_template_file  OF t_qxt_print_template_file,
    local_debug            SMALLINT,
    p_client_file          VARCHAR(250),
    l_server_file          VARCHAR(250),
    p_dialog               SMALLINT,
    p_server_file_blob     BYTE

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "1 - upload_blob_print_template() - p_dialog=", p_dialog
    DISPLAY "1 - upload_blob_print_template() - p_client_file=", p_client_file
    DISPLAY "1 - upload_blob_print_template() - l_server_file=", l_server_file
  END IF


  IF p_dialog THEN
    #"Please select the file"
#    CALL fgl_file_dialog("save", 0, get_str(273), p_client_file_path, p_client_file_path, "File (*.*)|*.*|Html (*.html)|*.html|Htm (*.htm)|*.htm|Unl (*.unl)|*.unl|RTF (*.rtf)|*.rtf|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")

    CALL fgl_file_dialog("open", 0, get_str_tool(65), p_client_file, p_client_file, "File (*.*)|*.*|Html (*.html)|*.html|Htm (*.htm)|*.htm|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file
  END IF

  IF local_debug THEN
    DISPLAY "2 - upload_blob_print_template() - p_client_file=", p_client_file
  END IF


  IF p_client_file IS NOT NULL THEN
    LET l_server_file = get_server_blob_temp_path(fgl_basename(p_client_file))

    IF local_debug THEN
      DISPLAY "3 - upload_blob_print_template() - l_server_file=", l_server_file
      DISPLAY "3 - upload_blob_print_template() - fgl_test(\"e\", l_server_file) = ", fgl_test("e", l_server_file)
      DISPLAY "3 - upload_blob_print_template() - fgl_upload(p_client_file, l_server_file) =", fgl_upload(p_client_file, l_server_file) 
      DISPLAY "3 - upload_blob_print_template() - fgl_upload(\"c:/321.txt\", \"blob_temp_path/321.txt\") =", fgl_upload("c:/321.txt", "blob_temp_path/321.txt") 
    END IF

    IF NOT fgl_upload(p_client_file, l_server_file) OR NOT fgl_test("e", l_server_file) THEN
      LET l_server_file = ""
    END IF
  END IF


  IF local_debug THEN
    DISPLAY "4 - upload_blob_print_template() - l_server_file_path=", l_server_file
  END IF

  RETURN l_server_file

END FUNCTION


######################################################
# FUNCTION get_blob_print_template_file(p_template_id,p_language_id)
#
# Get the print_template file
#
# RETURN p_server_file_path
######################################################
FUNCTION get_blob_print_template_file(p_template_id,p_language_id)
  DEFINE 
    p_template_id       LIKE qxt_print_template.template_id,
    p_language_id        LIKE qxt_print_template.language_id,
    p_server_file_path   VARCHAR(250),
    l_print_template_file     OF t_qxt_print_template_file

  LET p_server_file_path = get_server_blob_temp_path(get_print_html_template_filename(p_template_id,p_language_id))

  LOCATE l_print_template_file.template_data IN FILE p_server_file_path

  SELECT qxt_print_template.filename,
         qxt_print_template.print_template_blob
    INTO l_print_template_file.*
    FROM qxt_print_template
    WHERE qxt_print_template.template_id = p_template_id
      AND qxt_print_template.language_id   = p_language_id

  RETURN p_server_file_path
END FUNCTION


######################################################
# FUNCTION copy_print_template_record_to_form_record(p_print_template_rec)  
#
# Copy normal print_template record data to type print_template_form_rec
#
# RETURN l_print_template_form_rec.*
######################################################
FUNCTION copy_print_template_record_to_form_record(p_print_template_rec)  
  DEFINE
    p_print_template_rec       RECORD LIKE qxt_print_template.*,
    l_print_template_form_rec  OF t_qxt_print_template_form_rec

  LET l_print_template_form_rec.template_id = p_print_template_rec.template_id
  LET l_print_template_form_rec.language_name = get_language_name(p_print_template_rec.language_id)
  LET l_print_template_form_rec.filename = p_print_template_rec.filename
  LET l_print_template_form_rec.mod_date = p_print_template_rec.mod_date
  LET l_print_template_form_rec.template_data = p_print_template_rec.template_data

  RETURN l_print_template_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_print_template_form_record_to_record(p_print_template_form_rec)  
#
# Copy type print_template_form_rec to normal print_template record data
#
# RETURN l_print_template_rec.*
######################################################
FUNCTION copy_print_template_form_record_to_record(p_print_template_form_rec)  
  DEFINE
    l_print_template_rec       RECORD LIKE qxt_print_template.*,
    p_print_template_form_rec  OF t_qxt_print_template_form_rec

  LET l_print_template_rec.template_id = p_print_template_form_rec.template_id
  LET l_print_template_rec.language_id = get_language_id(p_print_template_form_rec.language_name)
  LET l_print_template_rec.filename = p_print_template_form_rec.filename
  LET l_print_template_rec.mod_date = p_print_template_form_rec.mod_date
  LET l_print_template_rec.template_data = p_print_template_form_rec.template_data

  RETURN l_print_template_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_print_template_scroll()
#
# Populate print_template grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_print_template_scroll()
  CALL fgl_grid_header("sc_print_template","template_id",get_str_tool(971),"right","F13")  --Help ID
  CALL fgl_grid_header("sc_print_template","language_name",get_str_tool(972),"left","F14")  --language
  CALL fgl_grid_header("sc_print_template","filename",get_str_tool(973),"left","F15")  --file name
  CALL fgl_grid_header("sc_print_template","mod_date",get_str_tool(974),"left","F16")  --modificationn date


END FUNCTION

#######################################################
# FUNCTION populate_print_template_form_labels_g()
#
# Populate print_template form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_labels_g()

  CALL fgl_settitle(get_str_tool(976))

  DISPLAY get_str_tool(976) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_print_template_form_labels_t()
#
# Populate print_template form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_labels_t()

  DISPLAY get_str_tool(976) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_print_template_form_edit_labels_g()
#
# Populate print_template form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(976)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download

  CALL language_combo_list("language_name")


END FUNCTION



#######################################################
# FUNCTION populate_print_template_form_edit_labels_t()
#
# Populate print_template form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_edit_labels_t()

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_print_template_form_view_labels_g()
#
# Populate print_template form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(976)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_done
  DISPLAY "!" TO bt_done

END FUNCTION



#######################################################
# FUNCTION populate_print_template_form_view_labels_t()
#
# Populate print_template form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_view_labels_t()

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_name")

END FUNCTION


#######################################################
# FUNCTION populate_print_template_form_create_labels_g()
#
# Populate print_template form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(976)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download

  CALL language_combo_list("language_name")

END FUNCTION



#######################################################
# FUNCTION populate_print_template_form_create_labels_t()
#
# Populate print_template form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_form_create_labels_t()

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_print_template_list_form_labels_g()
#
# Populate print_template list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(976)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_print_template_scroll()

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete

  DISPLAY get_str_tool(15) TO bt_view
  DISPLAY "!" TO bt_view

  DISPLAY get_str_tool(24) TO bt_export
  DISPLAY "!" TO bt_export

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download

END FUNCTION


#######################################################
# FUNCTION populate_print_template_list_form_labels_t()
#
# Populate print_template list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_template_list_form_labels_t()

  DISPLAY trim(get_str_tool(976)) || " - " || get_str_tool(16) TO lbTitle  --List

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION
















