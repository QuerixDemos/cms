##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

-- Modified on 23.9.14 by Convex for Log 5162 (inclu of jfk_ tables)
-- Modified on 16.9.14 by Convex for increasing g_repos for Log 4976
-- Modified on 10.9.14 by Convex for variables qvgl_function & write_ok for Log 5128
-- Modified on 19.8.14 by Convex for increasing gl_p_line,safe_line for Log 5128
-- Modified on 24.7.14 by Convex for Merge
{--------------------------------------------------}
{ Inflair     Version 4.3.17              28/02/06 }
{--------------------------------------------------}
{ Author: K. Vijayan                               }
{ Module: tc_globs.4gl                             }
{ Purpose: Provides global variables for general   }
{          use                                     }
{ Amd Robert for 4051 cl_6, fmeal, fflt* 110922    }
{ Amd Partha for 4310 120402                       }
{ Amd Partha for CMB 4333 120611			   }
{ Amd Partha KUL 4147 130228		              }
{ Amd Partha - KUL 5102					   }
{ Amd Convex - KUL 5128					   }
{--------------------------------------------------}
DATABASE cms  --huho needs sorting

GLOBALS

{
    DEFINE 
    # General variables 
        answer CHAR(1),
        confirm CHAR(1),
        chk_acc ,
        DEBUG,BELL CHAR(1),
        account CHAR(8) ,
        dept CHAR(4) ,
        gl_catpres char(18),
        gl_auto_rep,gl_auto_rep2 CHAR(1),
        gl_version CHAR(9),
        gl_password CHAR(20),
        gl_xml_name CHAR (5),
        gl_html_name CHAR (5),
        gl_ref_name CHAR(30),
        gl_today DATE,
        err_count INT,
        gl_menu_desc ARRAY [18] OF CHAR(45),
        gl_demo_version CHAR(1),
        gl_subs CHAR(1),
        gl_day ARRAY [7] OF CHAR(1),
        qxt_glob_int_switch SMALLINT,
         g_repr_rec RECORD LIKE inf_log.*,
        user_name_arr ARRAY[100] OF RECORD
              user_name LIKE reference.ref_filler     
        END RECORD,
        user_name1_arr ARRAY[100] OF RECORD
              user_name char(30),
              macname char(20),
              strt CHAR (30),
              user_sub char(20)
        END RECORD,
        gl_vat_rate DECIMAL(6,3),
        gl_last_ordtmp INTEGER,
        gl_rep_name CHAR(12),
        g_num_locs INTEGER,       -- num of lines displayed in diary 
        wide_prt_rec record like reference.*, --wide screen switch
        
        #Enhancement Variables
        gl_updlgr char(1),
        gl_trspre,gl_rptlgr CHAR(1),

    # Error handling variables 
        gl_menu_opt CHAR(10),
        gl_mod CHAR(10),
        gl_function CHAR(30),
        qvgl_function CHAR(30),

    # Print Check variables for Log 5128 
	write_ok CHAR(1),

    # User variables 
        gl_prg_acc ARRAY [47] of CHAR(1),
        gl_usr CHAR(8),  --encrypted password - only taking the first 8 letters from the password to encrypt
        gl_user_name CHAR(30),
        gl_user_help CHAR(1),
        gl_count int,
        hl_count INT,
        gl_hname CHAR(40),
        
    # Grid sort variables 
     x ARRAY[6] OF RECORD 
        a INTEGER,
        b INTEGER
      END RECORD,
    i INTEGER,
    recs INTEGER,    
        
    # Report variables - ev_report.4gl
        gl_logo CHAR (1),
        gl_top_of_page SMALLINT,
        g_repos CHAR (11),
        gl_rep_init,gl_rep_complete,gl_rep_abort,gl_device,gl_html,gl_xml,gl_xls,gl_repos CHAR(1),
        gl_new_page,gl_rep_force CHAR(1),
        gl_pg_no,gl_ln_no,gl_max_lines,gl_rep_row SMALLINT,
        gl_ln_need SMALLINT,
        gl_rep_head CHAR(44),    #changed for tco_83 ln 1751
        gl_footer CHAR(250),
        pr_header CHAR(1),
        def_prt CHAR(20),
        gl_p_line char(250),gl_pg_ln1,gl_pg_ln2,gl_pg_ln3,gl_pg_ln4,gl_pg_ln5 CHAR(250),
        gl_pg_hd1,gl_pg_hd2,gl_pg_hd3 CHAR(250),safe_line CHAR(250),
        gl_scr_table ARRAY [19] OF CHAR (250),
        gl_si CHAR (1),
        gl_item_cost_tot DECIMAL (10,3), 
        gl_all_cost_tot DECIMAL (10,3),
     File Input/Output Library variables #
        gl_rec_stat CHAR(2),

    # confre Print & Total Variables 
        gl_inv_layout ARRAY [31] OF CHAR(10),
        gl_max_det_ln SMALLINT,
        gl_inv_text ARRAY [31] OF CHAR(20),
        gl_inv_total,gl_inv_vat_total,gl_inv_disc_total DECIMAL(10,2),
        gl_group_code ARRAY [10] OF CHAR(3),
        gl_group_total ARRAY [10] OF DECIMAL(10,2),
        gl_group_vat ARRAY [10] OF DECIMAL(10,2),
        gl_vat_goods ARRAY [10] OF DECIMAL(10,2),
        gl_vat_amount ARRAY [10] OF DECIMAL(10,2),
        gl_srch_sub SMALLINT,
        gl_print_env CHAR(80),
    #      tco_81a#
        fcst_item_cost DECIMAL (12,6),
    #      tco_22 
        col_title char(130),
    #      tco_22 
        srch_string char(30),
    # Record Indicators 
    #      tco_22 
        new_pax CHAR(1),
    # Record layouts 
        p_fltacrw_hdr RECORD LIKE fltacrw_hdr.*,
        p_fltacrw_det RECORD LIKE fltacrw_det.*,
        p_tail_nos RECORD LIKE tail_nos.*,
        p_currmls RECORD LIKE currmls.*,
        p_currmpr RECORD LIKE currmpr.*,
        p_al_class RECORD LIKE al_class.*,
        p_ml_class RECORD LIKE ml_class.*,
        #r_rec RECORD LIKE reference.*,
        r_rec RECORD LIKE inf_log.*,
        ref_rec RECORD LIKE reference.*,
        report_rec RECORD LIKE fcreport.*,
        rep_rec_250 char(250),

   #     report_rec RECORD 
	#  rep_rec char(250)
	#END RECORD,

        p_acper RECORD LIKE acper.*,
        p_arch_hdr RECORD LIKE arch_hdr.*,
        p_arch_det RECORD LIKE arch_det.*,
        p_curloc RECORD LIKE curloc.*,
        p_cursupp RECORD LIKE cursupp.*,
        p_curdept RECORD LIKE curdept.*,
        p_curunit RECORD LIKE curunit.*,
        r_curunit RECORD LIKE curunit.*,
        w_curunit RECORD LIKE curunit.*,
        p_curgroup RECORD LIKE curgroup.*,
        p_curprod RECORD LIKE curprod.*,
        r_curprod RECORD LIKE curprod.*,
        p_prodex RECORD LIKE prodex.*,  # Partha 5102
        p_sinee  RECORD LIKE sinee.*,   # Partha 5102
        p_wcurram RECORD LIKE wcurram.*,
        p_wcurramd RECORD LIKE wcurramd.*,
        p_hcurram RECORD LIKE hcurram.*,
        p_hcurramd RECORD LIKE hcurramd.*,
        p_tcurram RECORD LIKE hcurram.*,
        p_tcurramd RECORD LIKE hcurramd.*,
        p_currecins RECORD LIKE currecins.*,
        p_rmtins RECORD LIKE rmtins.*,
        p_cursp RECORD LIKE cursp.*,
        p_curstok RECORD LIKE curstok.*,
        p_customer RECORD LIKE customer.*,
        p_airdisc RECORD LIKE airdisc.*,
        p_aircrd RECORD LIKE aircrd.*,
        p_airinvrm RECORD LIKE airinvrm.*,
        p_airdnorm RECORD LIKE airdnorm.*,
        p_aircraft RECORD LIKE aircraft.*,
        p_altconf RECORD LIKE altconf.*,
        p_pcc RECORD LIKE pcc.*,
        p_pcc_hdr RECORD LIKE pcc_hdr.*,
        h_pcc RECORD LIKE hpcc.*,
        h_pcc_hdr RECORD LIKE hpcc_hdr.*,
        p_fc_mentp RECORD LIKE fc_mentp.*,
        p_fc_mensbtp RECORD LIKE fc_mensbtp.*,
        p_fc_menitem RECORD LIKE fc_menitem.*,
        p_fc_wmencyc RECORD LIKE fc_wmencyc.*,
        p_flight RECORD LIKE flight.*,
        p_flight_hdr RECORD LIKE flight_hdr.*,
        p_flight_type RECORD LIKE flight_type.*,
        p_fs_mentp RECORD LIKE fs_mentp.*,
        p_fs_mensbtp RECORD LIKE fs_mensbtp.*,
        p_fs_menitem RECORD LIKE fs_menitem.*,
        p_fltmst RECORD LIKE fltmst.*,
        p_fltmentp RECORD LIKE fltmentp.*,
        p_fltcrwml RECORD LIKE fltcrwml.*,
        p_fltsupp RECORD LIKE fltsupp.*,
        p_fltbok RECORD LIKE fltbok.*,
        p_hfltbok RECORD LIKE hfltbok.*,
        p_fobsup RECORD LIKE fobsup.*, 
        p_fobpo RECORD LIKE fobpo.*,
        p_fipo	RECORD LIKE fipo.*,
        p_fltdet RECORD LIKE fltdet.*,
        p_fltextr_hdr RECORD LIKE fltextr_hdr.*,
        p_fltextr RECORD LIKE fltextr.*,
        p_fltdn_hdr RECORD LIKE fltdn_hdr.*,
        p_fltdn RECORD LIKE fltdn.*,
        p_fltdnex_hdr RECORD LIKE fltdnex_hdr.*,
        p_fltdnex RECORD LIKE fltdnex.*,
        p_fltmail_hdr RECORD LIKE fltmail_hdr.*,
        p_sfltdn_hdr RECORD LIKE sfltdn_hdr.*,
        p_sfltdn_det RECORD LIKE sfltdn_det.*,
        p_wfltdn_hdr RECORD LIKE sfltdn_hdr.*,
        p_wfltdn_det RECORD LIKE sfltdn_det.*,
        p_fltmail RECORD LIKE fltmail.*,
        p_drymst_hdr RECORD LIKE drymst_hdr.*,
        p_drymst RECORD LIKE drymst.*,
        p_drydn_hdr RECORD LIKE drydn_hdr.*,
        p_drydn RECORD LIKE drydn.*,
        p_drydn_cls RECORD LIKE drydn_cls.*,
        p_drydnex_hdr RECORD LIKE drydnex_hdr.*,
        p_drydnex RECORD LIKE drydnex.*,
        p_drymail_hdr RECORD LIKE drymail_hdr.*,
        p_drymail RECORD LIKE drymail.*,
        p_addldn_hdr RECORD LIKE addldn_hdr.*,
        p_addldn RECORD LIKE addldn.*,
        p_addldn_cls RECORD LIKE addldn_cls.*,
        p_pur_req_hdr RECORD LIKE pur_req_hdr.*,
        p_pur_req_det RECORD LIKE pur_req_det.*,
        p_pur_req_mark RECORD LIKE pur_req_mark.*,
        p_pur_con_hdr RECORD LIKE pur_con_hdr.*,
        p_pur_con_det RECORD LIKE pur_con_det.*,
        p_pur_ord_hdr RECORD LIKE pur_ord_hdr.*,
        p_pur_ord_det RECORD LIKE pur_ord_det.*,
        p_pcrmt RECORD LIKE pcrmt.*,
        p_pcrcp RECORD LIKE pcrcp.*,
        p_pccon RECORD LIKE pccon.*,
        p_reqt_hdr RECORD LIKE reqt_hdr.*,
        p_reqt_ord RECORD LIKE reqt_ord.*,
        p_reqt_rcp RECORD LIKE reqt_rcp.*,
        p_invhdr RECORD LIKE invhdr.*,
        p_invhdr1 RECORD LIKE invhdr1.*,
        p_invdet1 RECORD LIKE invdet1.*,
        p_prclist RECORD LIKE prclist.*,
        p_rmtcosts RECORD LIKE rmtcosts.*,
        p_rcpcosts RECORD LIKE rcpcosts.*,
        p_invst_hdr RECORD LIKE invst_hdr.*,
        p_invst RECORD LIKE invst.*,
        p_picvol RECORD LIKE picvol.*,
        p_picpage RECORD LIKE picpage.*,
        p_picdet RECORD LIKE picdet.*,
        p_stklvl RECORD LIKE stklvl.*,
        p_stkbin RECORD LIKE stkbin.*,
       # p_fipo RECORD LIKE fipo.*,	
        p_stksh_hdr RECORD LIKE stksh_hdr.*,
        p_stksh_det RECORD LIKE stksh_det.*,
        p_pur_wrksh_hdr RECORD LIKE pur_wrksh_hdr.*,
        p_pur_wrksh_hdr1 RECORD LIKE pur_wrksh_hdr1.*,
        p_lcstklvl RECORD LIKE lcstklvl.*,             #added tb 28/07/11
        p_pur_wrksh_det RECORD LIKE pur_wrksh_det.*,
        p_pur_wrksh_lock RECORD LIKE pur_wrksh_lock.*,
        p_pur_ws_sum RECORD LIKE pur_ws_sum.*,
        p_temp_po_hdr RECORD LIKE temp_po_hdr.*,
        p_temp_po_det RECORD LIKE temp_po_det.*,
        p_rct_note_hdr RECORD LIKE rct_note_hdr.*,
        p_rct_note_det RECORD LIKE rct_note_det.*,
        p_stkerr RECORD LIKE stkerr.*,
        p_trf_note_hdr RECORD LIKE trf_note_hdr.*,
        p_trf_note_det RECORD LIKE trf_note_det.*,
        p_pick_list_hdr RECORD LIKE pick_list_hdr.*,
        p_pick_list_det RECORD LIKE pick_list_det.*,
        p_pur_inv_hdr RECORD LIKE pur_inv_hdr.*,
        p_pur_inv_det RECORD LIKE pur_inv_det.*,
        p_purinvst_hdr RECORD LIKE purinvst_hdr.*,
        p_purinvst RECORD LIKE purinvst.*,
        p_adj_note_hdr RECORD LIKE adj_note_hdr.*,
        p_adj_note_det RECORD LIKE adj_note_det.*,
        p_adv_note_hdr RECORD LIKE adv_note_hdr.*,
        p_adv_note_det RECORD LIKE adv_note_det.*,
        p_adq_note_hdr RECORD LIKE adq_note_hdr.*,
        p_adq_note_det RECORD LIKE adq_note_det.*,
        p_cstsh_hdr RECORD LIKE cstsh_hdr.*,
        p_cstsh_det RECORD LIKE cstsh_det.*,
        p_cst_upd_lock RECORD LIKE cst_upd_lock.*,
        p_cst_upd_err RECORD LIKE cst_upd_err.*,
        p_ncstsh_hdr RECORD LIKE ncstsh_hdr.*,
        p_ncstsh_det RECORD LIKE ncstsh_det.*,
        p_inv_upd_lock RECORD LIKE inv_upd_lock.*,
        p_inv_upd_err RECORD LIKE inv_upd_err.*,
        p_currency RECORD LIKE currency.*,
        p_bill_cur RECORD LIKE bill_cur.*,
    p_plan_hdr RECORD LIKE plan_hdr.*,
    p_plan_reqt RECORD LIKE plan_reqt.*,
    p_plan_book RECORD LIKE plan_book.*,
    p_plan_ord RECORD LIKE plan_ord.*,
    p_plan_rcp RECORD LIKE plan_rcp.*,
    p_equip_pack_hdr RECORD LIKE equip_pack_hdr.*,
    p_stow_label_hdr RECORD LIKE stow_label_hdr.*,
    p_stow_label_det RECORD LIKE stow_label_det.*,
    r_stow_label_det RECORD LIKE stow_label_det.*,
    p_ccrwmst RECORD LIKE ccrwmst.*,
    p_ccrwspec RECORD LIKE ccrwspec.*,
    p_fltcrwmst RECORD LIKE fltcrwmst.*,
    p_fltcrwdet RECORD LIKE fltcrwdet.*,
    p_supprc_hdr RECORD LIKE supprc_hdr.*,
    p_supprc_det RECORD LIKE supprc_det.*,
    #p_jfk_ship_to RECORD LIKE jfk_ship_to.*,
    #p_jfk_dnexta RECORD LIKE jfk_dnexta.*,
    #p_jfk_spon RECORD LIKE jfk_spon.*,
    #p_jfk_unc RECORD LIKE jfk_unc.*,
    p_prc_arr ARRAY[1000] OF RECORD
            item_group LIKE supprc_det.item_group,
            item_code LIKE supprc_det.item_code,
            item_desc LIKE curprod.prod_description,
            std_unit LIKE supprc_det.std_unit,
            std_cost DECIMAL(18,12),
            supp_code LIKE supprc_det.supp_code,
            supp_name LIKE cursupp.supp_name,
            supp_unit LIKE supprc_det.supp_unit,
            supp_price LIKE supprc_det.supp_price
            END RECORD, 
    p_fltdep RECORD
        c_number char(3),
        t_flight char(8),
        l_code char(8),
        tail_no char(15),
        l_conf_code char(1),
        f_date date,
        d_date date,
        edt smallint,
        cl_1_cd char(2),
        cl_1_pax smallint,
        cl_2_cd char(2),
        cl_2_pax smallint,
        cl_3_cd char(2),
        cl_3_pax smallint,
        cl_4_cd char(2),
        cl_4_pax smallint,
        cl_5_cd char(2),
        cl_5_pax smallint,
        cl_6_cd char(2),
        cl_6_pax smallint,
        cl_7_cd char(2),
        cl_7_pax smallint,
        cl_8_cd char(2),
        cl_8_pax smallint
   END RECORD,
        p_reqt_tmp RECORD
         reqt_no char(8),
         c_number char(3),
         t_flight char(8),
         l_code char(8),
         f_date date,
         class char(2),
         ml_class char(2),
         mentp_code char(4),
         pax decimal(6,2),
         mensbtp_code char(1),
         menitem_code char(6),
         output_supp_code char(6),
         output_type char(1),
         output_group char(3),
         output_code char(6),
         output_unit char(6),
         output_qty decimal(12,8),
         inp_supp_code char(6),
         inp_type char(1),
         inp_code char(6),
         inp_unit char(6),
         inp_group char(3),
         inp_qty decimal(12,8),
         level_no smallint,
         std smallint,
         a_date date,
         edt smallint
        END RECORD,
        p_curldgr RECORD
          ac_year smallint,
          ac_per_no smallint,
          doc_type char(3),
          doc_ref_no char(8),
          doc_ref_seq SMALLINT,
          doc_date date,
          loc_code char(2),
          bay_no char(2),
          line_no char(2),
          level_no char(2),
          bin_no char(3),
          item_type char(1),
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          pos_neg char(1),
          item_qty decimal(8,3),
          item_rate decimal(11,3),
          item_value decimal(11,2),
          item_comments char(30),
          supp_dep_code char(6),
          ref_loc_code char(2),
          ref_bay_no char(2),
          ref_line_no char(2),
          ref_level_no char(2),
          ref_bin_no char(3),
          ref_user_code char(8),
          ref_user_name char(30),
          ref_doc_type char(3),
          ref_doc_no char(8),
          ref_doc_date date,
          due_date date
        END RECORD,
        p_curldgr1 RECORD
          ac_year SMALLINT,
          ac_per_no SMALLINT,
          doc_type char(3),
          doc_no char(8),
          doc_date DATE,
          loc_code char(2),
          bay_no char(2),
          line_no char(2),
          level_no char(2),
          bin_no char(3),
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          pos_neg char(1),
          item_qty decimal(8,3)
        END RECORD,
        p_tempcns RECORD
          mail_date date,
          rec_group char(3),
          rec_code char(6),
          rec_unit char(6),
          rec_qty  decimal(8,3),
          loc_code char(2),
          bay_no char(2),
          line_no char(2),
          level_no char(2),
          bin_no char(3),
          mail_type char(3),
          mail_no char(8)
        END RECORD,
        p_e_trf_note_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          trf_no char(8),
          trf_seq smallint,
          from_loc_code char(2),
          from_bay_no char(2),
          from_line_no char(2),
          from_level_no char(2),
          from_bin_no char(3),
          item_type char(1),
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          item_qty decimal(8,3),
          item_comm char(30),
          to_loc_code char(2),
          to_bay_no char(2),
          to_line_no char(2),
          to_level_no char(2),
          to_bin_no char(3),
          ref_doc_type char(3),
          ref_doc_no char(8),
          trf_date Date
        END RECORD,
        p_e_adj_note_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          adj_no char(8),
          adj_seq smallint,
          from_loc_code char(2),
          from_bay_no char(2),
          from_line_no char(2),
          from_level_no char(2),
          from_bin_no char(3),
          item_type char(1),
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          item_qty decimal(8,3),
          item_rate decimal(11,2),
          item_value decimal(11,2),
          item_comm char(30),
          ref_doc_type char(3),
          ref_doc_no char(8),
          ref_doc_date date,
          adj_date date
        END RECORD,
        p_e_adv_note_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          adv_no char(8),
          adv_seq smallint,
          from_loc_code char(2),
          from_bay_no char(2),
          from_line_no char(2),
          from_level_no char(2),
          from_bin_no char(3),
          item_type char(1),
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          item_qty decimal(8,3),
          item_rate decimal(11,2),
          item_value decimal(11,2),
          item_comm char(30),
          ref_doc_type char(3),
          ref_doc_no char(8),
          ref_doc_date date,
          adv_date date
        END RECORD,
        p_e_stksh_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          stksh_no char(8),
          seq_no smallint,
          loc_code char(2),
          bay_no char(2),
          line_no char(2),
          level_no char(2),
          bin_no char(3),
          item_type char(1),
          item_group char(3),
          item_code char(6),
          item_unit1 char(6),
          item_qty1 decimal(8,3),
          item_unit2 char(6),
          item_qty2 decimal(8,3),
          item_unit3 char(6),
          item_qty3 decimal(8,3),
          phy_unit char(6),
          phy_qty decimal(11,3),
          book_qty decimal(11,3),
          stksh_date date
        END RECORD,
        p_e_rct_note_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          rct_no char(8),
          seq_no smallint,
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          bay_no char(2),
          line_no char(2),
          level_no char(2),
          bin_no char(3),
          item_due_date date,
          item_qty decimal(8,3),
          item_rate decimal(8,3),
          item_value decimal(11,2),
          item_vat_perc decimal(5,2),
          item_vat_value decimal(11,2),
          item_comm char(30),
          veh_no char(15),
          rct_temp decimal(5,2),
          mfd_date date,
          shelf_life integer,
          exp_date date,
          lot_no char(14),
          cur_code char(3),
          cur_price decimal(8,3),
          conv_rate decimal(10,5),
          cur_vat_value decimal(11,2),
          cur_value decimal(11,2),
          rct_date DATE,
          loc_code CHAR(2),
          supp_code CHAR(6),
          supp_doc_ref CHAR(15),
          supp_doc_date date
        END RECORD,
        p_e_pur_inv_det RECORD
          ac_year smallint,
          ac_per_no smallint,
          pur_inv_no char(8),
          seq_no smallint,
          item_group char(3),
          item_code char(6),
          item_unit char(6),
          item_qty decimal(8,3),
          item_rate decimal(8,3),
          item_value decimal(13,2),
          item_vat_perc decimal(5,2),
          item_vat_value decimal(13,2),
          item_comm char(30),
          cur_code char(3),
          cur_rate decimal(8,3),
          conv_rate decimal(10,5),
          cur_vat_value decimal(13,2),
          cur_value decimal(13,2),
          supp_code char(6),
          loc_code char(2),
          pur_inv_date date,
          supp_doc_ref char(15),
          supp_doc_date date
        END RECORD,
        p_flt_reqt RECORD
          supp_code char(6),
          group_code char(3),
          item_code char(6),
          item_unit char(6),
          item_qty decimal(12,6),
          mentp_code char(4),
          class char(2),
          c_number char(3),
          t_flight char(8),
          l_code char(8),
          f_date date,
          act_date date,
          act_edt SMALLINT
        END RECORD, 
     p_fltlod RECORD
         c_number char(3),
         t_flight char(8),
         e_date date,
         f_date date,
         l_code char(8),
         cl_1_cd char(2),
         cl_1_pax INTEGER,
         cl_1_cap INTEGER,
         cl_2_cd char(2),
         cl_2_pax INTEGER,
         cl_2_cap INTEGER,
         cl_3_cd char(2),
         cl_3_pax INTEGER,
         cl_3_cap INTEGER,
         cl_4_cd char(2),
         cl_4_pax INTEGER,
         cl_4_cap INTEGER,
         cl_5_cd char(2),
         cl_5_pax INTEGER,
         cl_5_cap INTEGER,
    	 cl_6_cd char(2),
         cl_6_pax INTEGER,
         cl_6_cap INTEGER
       END RECORD, 
        p_curstok1_hdr RECORD
           ac_year smallint,
           ac_per_no smallint,
           ac_st_date date,
           ac_en_date date,
           ac_close char(1),
           ac_close_date date,
           ac_close_time char(5)
        END RECORD,
        p_curstok1_det RECORD
            stock_ac_year smallint,
            stock_ac_per_no smallint,
            stock_type char(1),
            stock_group char(3),
            stock_code char(6),
            stock_loc char(2),
            stock_bay_no char(2),
            stock_line_no char(2),
            stock_level_no char(2),
            stock_bin_no char(3),
            stock_unit char(6),
            stock_bal_qty decimal(11,3)
        END RECORD,
     p_pcrmtlsg RECORD 
            prod_group char(3),
            prod_code char(6),
            prod_stand_unit char(6),
            prod_purch_unit char(6),
            prod_sales_unit char(6),
            prod_cost_vat decimal(5,2),
            prod_last_supp char(6),
            prod_last_cost decimal(8,3),
            prod_last_unit char(6),
            prod_prev_supp char(6),
            prod_prev_cost decimal(8,3),
            prod_prev_unit char(6),
            prod_sale_vat decimal(5,2),
            prod_description char(30)
     END RECORD,
    # Arrays #
        b_pcc ARRAY[1000] OF SMALLFLOAT,
        mensbtp_arr ARRAY[30] OF RECORD
          ind CHAR(1),
          mensbtp_no SMALLINT,
          mensbtp_code LIKE pcc.pcc_mensbtp_code,
          mensbtp_desc LIKE fc_mensbtp.mensbtp_desc,
          mensbtp_pccmark LIKE fc_mensbtp.mensbtp_pccmark
          END RECORD,
        menitem_arr ARRAY[60] OF RECORD # was 20
          menitem_no SMALLINT,
          menitem_code LIKE pcc.pcc_menitem_code,
          menitem_desc LIKE fc_menitem.menitem_desc,
          grp_code LIKE fc_wmencyc.mencyc_rec_group,
          enter_recipe_code LIKE fc_wmencyc.mencyc_rec_code,
          prod_description LIKE curprod.prod_description
          END RECORD,
        item_arr21 ARRAY[600] OF RECORD
          mensbtp_code LIKE fc_menitem.mensbtp_code,
          menitem_no SMALLINT,
          menitem_code LIKE fc_menitem.menitem_code,
          menitem_desc LIKE fc_menitem.menitem_desc
          END RECORD,
        item_arr24 ARRAY[600] OF RECORD
          mensbtp_code LIKE fc_menitem.mensbtp_code,
          menitem_no SMALLINT,
          menitem_code LIKE fc_menitem.menitem_code,
          menitem_desc LIKE fc_menitem.menitem_desc,
          grp_code LIKE fc_wmencyc.mencyc_rec_group,
          enter_recipe_code LIKE fc_wmencyc.mencyc_rec_code,
          prod_description LIKE curprod.prod_description
          END RECORD,
        ing_arr ARRAY[100] OF RECORD
                rec_ing_line LIKE wcurramd.rec_line,
                rec_ing_type LIKE wcurramd.rec_ing_type,
                rec_ing_group LIKE wcurramd.rec_ing_group,
                enter_rec_ing_code LIKE wcurramd.rec_ing_code,
                rec_ing_desc LIKE curprod.prod_description,
                rec_ing_unit LIKE wcurramd.rec_ing_unit,
                rec_ing_qty DECIMAL(18,12),
                rec_ing_total_cost DECIMAL(18,12)
                END RECORD,
        ing_arr1 ARRAY[100] OF RECORD
                rec_ing_line LIKE wcurramd.rec_line,
                rec_ing_type LIKE wcurramd.rec_ing_type,
                rec_ing_group LIKE wcurramd.rec_ing_group,
                enter_rec_ing_code LIKE wcurramd.rec_ing_code,
                rec_ing_desc LIKE curprod.prod_description,
                rec_ing_unit LIKE wcurramd.rec_ing_unit,
                rec_ing_qty DECIMAL(18,12)
                END RECORD,
        ing_arr2 ARRAY[100] OF RECORD
                rec_ing_total_cost DECIMAL(18,12)
                END RECORD,
        enter_ing_arr ARRAY[100] OF RECORD
                rec_ing_code LIKE wcurramd.rec_ing_code
                END RECORD,
        d_purch_arr ARRAY[1000] OF RECORD
                seq_no SMALLINT,
                group_code LIKE curprod.prod_group,
                item_code LIKE curprod.prod_code,
                item_desc LIKE curprod.prod_description,
                item_unit LIKE curprod.prod_purch_unit,
                item_due_date date,
                item_qty DECIMAL(8,3),
                item_price DECIMAL(8,3),
                item_comm char(30)
                END RECORD,
                
        d_purch_arr_dynamic DYNAMIC ARRAY OF RECORD  --Added by alpr
                seq_no SMALLINT,
                group_code LIKE curprod.prod_group,
                item_code LIKE curprod.prod_code,
                item_desc LIKE curprod.prod_description,
                item_unit LIKE curprod.prod_purch_unit,
                item_due_date date,
                item_qty DECIMAL(8,3),
                item_price DECIMAL(8,3),
                item_comm char(30)
                END RECORD

				# Section Start - Added by HuHo  #####################################################                
	DEFINE	d_purreq_arr_dynamic DYNAMIC ARRAY OF RECORD  --added by HuHo
                seq_no SMALLINT,
                group_code LIKE curprod.prod_group,
                item_code LIKE curprod.prod_code,
                prod_description LIKE curprod.prod_description,
                item_unit LIKE curprod.prod_purch_unit,
                item_qty DECIMAL(8,3),
                item_due_date DATE,
                item_supp_code LIKE cursupp.supp_code,
                item_comm char(30) 
                END RECORD

	DEFINE gl_repr_rec RECORD LIKE inf_log.*
				# Section END - Added by HuHo  #####################################################                
                                
	DEFINE d_purreq_arr ARRAY[9000] OF RECORD
                seq_no SMALLINT,
                group_code LIKE curprod.prod_group,
                item_code LIKE curprod.prod_code,
                prod_description LIKE curprod.prod_description,
                item_unit LIKE curprod.prod_purch_unit,
                item_qty DECIMAL(8,3),
                item_due_date DATE,
                item_supp_code LIKE cursupp.supp_code,
                item_comm char(30) 
                END RECORD,
        d_purcon_arr ARRAY[1000] OF RECORD
                seq_no SMALLINT,
                group_code LIKE curprod.prod_group,
                item_code LIKE curprod.prod_code,
                prod_description LIKE curprod.prod_description,
                item_unit LIKE curprod.prod_purch_unit,
                item_qty DECIMAL(8,3),
                item_price DECIMAL(8,3),
                item_comm char(30) 
                END RECORD,
        ameal25_ind CHAR(1),
        aflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        aflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        aflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        bmeal25_ind CHAR(1),
        bflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        bflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        bflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        cmeal25_ind CHAR(1),
        cflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        cflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        cflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        dmeal25_ind CHAR(1),
        dflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        dflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        dflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        emeal25_ind CHAR(1),
        fmeal25_ind CHAR(1),
        fflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        fflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        fflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        eflt_mentp_arr ARRAY[10] OF RECORD
                stand_sno SMALLINT,
                stand_code LIKE fltmentp.stand_code,
                stand_desc LIKE fc_mentp.mentp_desc,
                cycle LIKE fltmentp.cycle,
                rotation LIKE fltmentp.rotation
                END RECORD,
        eflt_crwml_arr ARRAY[10] OF RECORD
                crwml_sno SMALLINT,
                crwml_code LIKE fltcrwml.crwml_code,
                crwml_desc LIKE fc_mentp.mentp_desc,
                crwml_qty LIKE fltcrwml.crwml_qty
                END RECORD,
        eflt_supp_arr ARRAY[10] OF RECORD
                supp_sno SMALLINT,
                supp_code LIKE fltsupp.supp_code,
                supp_desc LIKE fc_mentp.mentp_desc,
                supp_qty LIKE fltsupp.supp_qty
                END RECORD,
        afltbok_prod_arr ARRAY[40] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        bfltbok_prod_arr ARRAY[40] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        cfltbok_prod_arr ARRAY[40] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        dfltbok_prod_arr ARRAY[40] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        efltbok_prod_arr ARRAY[40] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        ffltbok_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        afltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        bfltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        cfltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        dfltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        efltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,
        ffltextr_prod_arr ARRAY[99] OF RECORD
                prod_no SMALLINT,
                prod_group char(3),
                prod_code char(6),
                prod_description char(30),
                prod_qty DECIMAL(6,2)
                END RECORD,        
        p_drydn_arr ARRAY[50] OF RECORD
                drydn_dn_no char(8),
                drydn_class char(2)
                END RECORD,
        p_addldn_arr ARRAY[50] OF RECORD
                addldn_dn_no char(8),
                addldn_class char(2)
                END RECORD,
        p_ram_arr ARRAY[100] OF RECORD
             sno SMALLINT,
             rec_ing_type LIKE wcurramd.rec_ing_type,
             rec_ing_group LIKE wcurramd.rec_ing_group,
             rec_ing_code LIKE wcurramd.rec_ing_code,
             prod_description LIKE curprod.prod_description,
             rec_ing_unit LIKE wcurramd.rec_ing_unit,
             rec_ing_qty DECIMAL(8,3)
        END RECORD,
        #Partha for Transfer Note
              trf_arr ARRAY[500] OF RECORD
              trf_seq   LIKE trf_note_det.trf_seq,
              trf_loc   LIKE trf_note_det.from_loc_code,
              trf_bay   LIKE trf_note_det.from_bay_no,  
              trf_line  LIKE trf_note_det.from_line_no, 
              trf_level LIKE trf_note_det.from_level_no, 
              trf_bin   LIKE trf_note_det.from_bin_no,  
              trf_type  LIKE trf_note_det.item_type, 
              trf_group LIKE trf_note_det.item_group,
              trf_code  LIKE trf_note_det.item_code
        END RECORD, 
        qual_date ARRAY[18] OF DATE,
        gl_q_due,gl_b_due SMALLINT,
        exit_flag,info_ok, ing_ok CHAR(1),
        gl_sales_vat CHAR(1),
        gl_vatrates ARRAY [10] OF DECIMAL(5,2),
        gl_d_perc DECIMAL(5,2),
		gl_inv_type CHAR(1),
        #counter SMALLINT,  -- I don't like this in the globals huho -it is used for array indexing and can lead to issues if you open i.e. a display array from another display array - both use the same size variable counter
        use_flight CHAR(4),
        dash_ln CHAR(250),
        impind smallint,
        impind1 smallint,
        imp_ind SMALLINT
        DEFINE blank_line CHAR(79);
}
END GLOBALS

#END GLOBALS
