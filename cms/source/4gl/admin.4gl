##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Admin Functions to manage lookup tables etc...
##################################################################################################################
#
# admin_main()                                   -- Main Admin Menu                                 RETURN NONE
##################################################################################################################

#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"

######################################################
# FUNCTION admin_main()
######################################################
FUNCTION admin_main()
  DEFINE
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

  MENU "Admin"
    BEFORE MENU
      CALL set_help_id(800)
      CALL publish_toolbar("Admin",0)

        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(1601)
        LET menu_str_arr2[1] = get_str(1602)
        LET menu_str_arr1[2] = get_str(1603)
        LET menu_str_arr2[2] = get_str(1604)
        LET menu_str_arr1[3] = get_str(1605)
        LET menu_str_arr2[3] = get_str(1606)
        LET menu_str_arr1[4] = get_str(1607)
        LET menu_str_arr2[4] = get_str(1608)
        LET menu_str_arr1[5] = get_str(1609)
        LET menu_str_arr2[5] = get_str(1610)
        LET menu_str_arr1[6] = get_str(1611)
        LET menu_str_arr2[6] = get_str(1612)
        LET menu_str_arr1[7] = get_str(1613)
        LET menu_str_arr2[7] = get_str(1614)
        LET menu_str_arr1[8] = get_str_tool(261) --country
        LET menu_str_arr2[8] = get_str_tool(262) --country
        LET menu_str_arr1[9] = get_str(1617)
        LET menu_str_arr2[9] = get_str(1618)
        LET menu_str_arr1[10] = get_str(1619)
        LET menu_str_arr2[10] = get_str(1620)
        LET menu_str_arr1[11] = get_str(1621)
        LET menu_str_arr2[11] = get_str(1622)
        LET menu_str_arr1[12] = get_str(1623)
        LET menu_str_arr2[12] = get_str(1624)
        LET menu_str_arr1[13] = get_str_tool(267) --Document Management
        LET menu_str_arr2[13] = get_str_tool(268) --Document Management
        LET menu_str_arr1[14] = get_str_tool(269) --Help HTML Document Management
        LET menu_str_arr2[14] = get_str_tool(270) --Help HTML Document Management
        LET menu_str_arr1[15] = get_str_tool(265) --HELP URL
        LET menu_str_arr2[15] = get_str_tool(266) --HELP URL
        LET menu_str_arr1[16] = get_str_tool(263) --language
        LET menu_str_arr2[16] = get_str_tool(264) --language
        LET menu_str_arr1[17] = get_str_tool(253) --return to previous menu
        LET menu_str_arr2[17] = get_str_tool(254) --return to previous menu

    COMMAND KEY (F801,"U") menu_str_arr1[1] menu_str_arr2[1] HELP 601  --"Operator" "Manage system operator" help 601
      CALL set_help_id(801)
      CALL operator_popup(NULL,NULL,2)  -- 2=default enter action is EDIT

    COMMAND KEY (F802,"I") menu_str_arr1[2] menu_str_arr2[2] HELP 602  -- "Industry Type" HELP 602
      CALL set_help_id(802)
      CALL industry_type_popup(NULL,NULL,2)

    COMMAND KEY (F803,"C")  menu_str_arr1[3] menu_str_arr2[3] HELP 603  -- "Company Type" HELP 603
      CALL set_help_id(803)
      CALL company_type_popup(NULL,NULL,2)

    COMMAND KEY (F804,"T")  menu_str_arr1[4] menu_str_arr2[4] HELP 604  --  "Tax Rates" HELP 604
      CALL set_help_id(804)
      CALL tax_rate_popup(NULL,NULL,2)

    COMMAND KEY (F805,"P") menu_str_arr1[5] menu_str_arr2[5] HELP 605  --  "Pay Method" HELP 605
      CALL set_help_id(805)
      CALL pay_method_popup(NULL,NULL,2)

    COMMAND KEY (F806,"M") menu_str_arr1[6] menu_str_arr2[6] HELP 606  --   "Currency" HELP 606
      CALL set_help_id(806)
      CALL currency_popup(NULL,NULL,2)

    COMMAND KEY (F807,"M") menu_str_arr1[7] menu_str_arr2[7] HELP 607  --  "Delivery Type" HELP 607
      CALL set_help_id(807)
      CALL delivery_type_popup(NULL,NULL,2)


    COMMAND KEY (F808,"L")  menu_str_arr1[8] menu_str_arr2[8] HELP 608  --  "Country" HELP 608
      CALL set_help_id(808)
      CALL country_popup(NULL,NULL,2)

    COMMAND KEY (F809,"T")  menu_str_arr1[9] menu_str_arr2[9] HELP 609  --   "Title" HELP 609
      CALL set_help_id(809)
      CALL title_popup(NULL,NULL,2)

    COMMAND KEY (F810,"D") menu_str_arr1[10] menu_str_arr2[10] HELP 610  --  "Department" HELP 610
      CALL set_help_id(810)
      CALL contact_dept_popup(NULL,NULL,2)

    COMMAND KEY (F811,"J") menu_str_arr1[11] menu_str_arr2[11] HELP 611  --  "Position" HELP 611
      CALL set_help_id(811)
      CALL position_type_popup(NULL,NULL,2)

    COMMAND KEY (F812,"A") menu_str_arr1[12] menu_str_arr2[12] HELP 612  --   "Activity Type" HELP 612
      CALL set_help_id(812)
      CALL activity_type_popup(NULL,NULL,2)

    COMMAND KEY (F813,"D") menu_str_arr1[13] menu_str_arr2[13] HELP 613  --   "Document management" HELP 613
      CALL set_help_id(813)
      CALL document_popup(NULL,NULL,2)

    #COMMAND KEY (F814,"H") menu_str_arr1[14] menu_str_arr2[14] HELP 614  --   "Help-Html management" HELP 614
    #  CALL set_help_id(814)
    #  CALL help_html_popup(NULL,NULL,NULL,2)

    #COMMAND KEY (F815,"U") menu_str_arr1[15] menu_str_arr2[15] HELP 615  --   "Help-URL-Map management" HELP 615
    #  CALL set_help_id(815)
    #  CALL help_url_map_popup(NULL,NULL,NULL,2)

    COMMAND KEY (F816,"U") menu_str_arr1[16] menu_str_arr2[16] HELP 616  --   "Language" HELP 616
      CALL set_help_id(816)
      CALL language_popup(NULL,NULL,2)

    COMMAND KEY (F12,"Q") menu_str_arr1[17] menu_str_arr2[17] HELP 50 --   "Quit" "Return to previous function" help 50
      EXIT MENU

    ON ACTION ("actExit") 
      EXIT MENU

  END MENU

  CALL publish_toolbar("Admin",1)

  #CLOSE WINDOW w_admin_main

END FUNCTION


