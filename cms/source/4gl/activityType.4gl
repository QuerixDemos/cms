##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################################################################################
# Functions to manage activities
##################################################################################################################
#
# FUNCTION:                                      DESCRIPTION:                                              RETURN:
# activity_type_popup_data_source(p_order_field) Data Source (Curosr) for activity_type_popup              NONE
# activity_type_popup
#   (p_type_id,p_order_field,p_accept_action)    Display all activity types for selection & management     l_act_arr[i].type_id OR p_type_id
# activity_type_delete(p_activity_type_id)       Delete an activity record                                 NONE
# activity_type_view_by_rec(p_activity_type_rec)        View an activity type record (with form/window)           NONE
# activity_type_edit((p_activity_type_id)        Edit activity type (with window+form)                     NONE
# activity_type_create()                         Create new activity type record (with form/window)        NONE
# activity_type_input(p_company_type)            Input company type details from form (edit/create)        company_type.* or 
# get_activity_type_name(p_activity_type_id)     Get the activity type name from the id                    l_activity_type_name
# get_activity_type_id(p_activity_type_name)     Get the activity type id from the activity type name      l_activity_type_id
# get_activity_type_rec(p_activity_type_id)      Get an activity_type record from id                       l_activity_type_rec.*
# grid_header_activity_type_popup()              Populate the grid header of the activity type popup grid NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"



######################################################
# FUNCTION activity_type_delete(p_activity_type_id)
#
# Delete an activity record
#
# RETURN NONE
######################################################
FUNCTION activity_type_delete(p_activity_type_id)
   DEFINE p_activity_type_id LIKE activity_type.type_id

   #"Delete!","Are you sure you want to delete this activity type?"
   IF yes_no(get_str(55),get_str(56)) THEN
     DELETE FROM activity_type WHERE activity_type.type_id = p_activity_type_id
     RETURN TRUE
   ELSE
     RETURN FALSE
   END IF

END FUNCTION


######################################################
# FUNCTION activity_type_edit((p_activity_type_id)
#
# Edit activity type (with window+form)
#
# RETURN NONE
######################################################
# display activity detail
FUNCTION activity_type_edit(p_activity_type_id)
  DEFINE 
    p_activity_type_id  LIKE activity_type.type_id,
    l_activity_type_rec OF t_act_type_rec,
    local_debug    SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  SELECT activity_type.type_id,
         activity_type.atype_name,
         activity_type.user_def
    INTO l_activity_type_rec.*
    FROM activity_type
   WHERE activity_type.type_id = p_activity_type_id

  IF sqlca.sqlcode THEN
    RETURN
  END IF

  CALL activity_type_input(l_activity_type_rec.*) RETURNING  l_activity_type_rec.*

  IF int_flag THEN  --cancel
    LET int_flag = FALSE
    #"Record Creation/Modification was aborted on users request!
    CALL fgl_winmessage(get_Str(53),get_str(54),"info")
  ELSE
    UPDATE activity_type SET
	atype_name = l_activity_type_rec.atype_name,
	user_def = l_activity_type_rec.user_def
      WHERE activity_type.type_id = p_activity_type_id
  END IF


END FUNCTION

{

FUNCTION activity_type_input(p_activity_type_rec)
  DEFINE 
    p_activity_type_rec RECORD LIKE activity_type.*,
    l_activity_type_rec RECORD LIKE activity_type.*,
    local_debug         SMALLINT

  LET local_debug = FALSE  --0=off 1=on
  LET l_activity_type_rec.* = p_activity_type_rec.*

  IF fgl_fglgui() THEN  --gui

    OPEN WINDOW w_act_type_edit 
      AT 2,2 
      WITH FORM "form/f_activity_type_edit_g"
      ATTRIBUTES(BORDER,FORM LINE 2) 

    CALL fgl_settitle("CMS Demo - Show Activity Details")
    #CALL activity_type_combo_list("atype_name")
    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel
    CALL fgl_settitle("Edit Activity")

  ELSE  --text client
    OPEN WINDOW w_act_type_edit 
      AT 2,2 
      WITH FORM "form/f_activity_type_edit_t"
      ATTRIBUTES(BORDER,FORM LINE 2) 

  END IF



  DISPLAY BY NAME l_activity_type_rec.type_id 
  DISPLAY BY NAME l_activity_type_rec.atype_name 
  DISPLAY BY NAME l_activity_type_rec.user_def

  IF local_debug = FALSE THEN  

    DISPLAY "activity_type_edit() l_activity_type_rec.type_id :",l_activity_type_rec.type_id 
    DISPLAY "activity_type_edit() l_activity_type_rec.atype_name :",l_activity_type_rec.atype_name 
    DISPLAY "activity_type_edit() l_activity_type_rec.user_def :",l_activity_type_rec.user_def 

  END IF


  INPUT BY NAME  
    l_activity_type_rec.atype_name,
    l_activity_type_rec.user_def WITHOUT DEFAULTS HELP 503

  CLOSE WINDOW w_act_type_edit

  RETURN p_activity_type_rec.*

END FUNCTION
}

######################################################
# FUNCTION activity_type_create()
#
# Create new activity type record (with form/window)
#
# RETURN NONE
######################################################
FUNCTION activity_type_create()
  DEFINE 
    l_activity_type_rec RECORD LIKE activity_type.*,
    local_debug         SMALLINT

  LET local_debug = FALSE

{
  IF fgl_fglgui() THEN   --gui
    OPEN WINDOW w_activity_type_new 
      AT 3, 3 
      WITH FORM "form/f_activity_type_new_g" 
      ATTRIBUTE(BORDER, FORM LINE 2)

    CALL fgl_settitle("CMS - Create new activity")
    DISPLAY "!" TO bt_ok
    DISPLAY "!" TO bt_cancel

  ELSE  --Text
    OPEN WINDOW w_activity_type_new 
      AT 3, 3 
      WITH FORM "form/f_activity_type_new_t" 
      ATTRIBUTE (BORDER, FORM LINE 3)

  END IF
}
  CALL activity_type_input(l_activity_type_rec.*) RETURNING l_activity_type_rec.*

  IF local_debug THEN
    DISPLAY "activity_type_create() - l_activity_type_rec.type_id=", l_activity_type_rec.type_id
    DISPLAY "activity_type_create() - l_activity_type_rec.atype_name=", l_activity_type_rec.atype_name
    DISPLAY "activity_type_create() - l_activity_type_rec.user_def=", l_activity_type_rec.user_def
  END IF

  IF NOT int_flag THEN

    INSERT INTO activity_type VALUES(l_activity_type_rec.*)

    IF sqlca.sqlcode THEN
      CALL fgl_winmessage(get_Str(2060),get_str(2061),"stop")
    ELSE
      CALL fgl_winmessage(get_Str(2060),get_str(2062),"info")
    END IF
  ELSE
    LET int_flag = FALSE
    CALL fgl_winmessage(get_Str(2060),get_str(2063),"info")
    CLEAR FORM
  END IF


  #CLOSE WINDOW w_activity_type_new

END FUNCTION



#############################################
# FUNCTION activity_type_input(p_company_type)
#
# Input company type details from form (edit/create)
#
# RETURN company_type.* or 
#############################################
FUNCTION activity_type_input(p_activity_type_rec)
  DEFINE 
    p_activity_type_rec RECORD LIKE activity_type.*,
    l_activity_type_rec RECORD LIKE activity_type.*,
    local_debug         SMALLINT

  LET local_debug = FALSE

  LET l_activity_type_rec.* = p_activity_type_rec.*

    CALL fgl_window_open("w_activity_type", 3, 3, get_form_path("f_activity_type_l2"), FALSE)
    CALL populate_act_type_form_labels_g()

  LET int_flag = FALSE

  INPUT BY NAME l_activity_type_rec.atype_name,
                l_activity_type_rec.user_def 
    WITHOUT DEFAULTS HELP 4

    AFTER INPUT

    IF NOT int_flag THEN
      IF l_activity_type_rec.atype_name IS NULL THEN
        #The field 'Activity Type Name' must be filled in!"
        CALL fgl_winmessage(get_str(57),get_str(2064),"info")
        CONTINUE INPUT
      END IF 
    ELSE
      --Confirm, if operator wants to abort record creation

      #Do you really want to abort the new/modified record entry ?
      IF NOT yes_no(get_str(53),get_str(54)) THEN
        #LET int_flag = FALSE
        CONTINUE INPUT
      END IF

    END IF
  END INPUT

  IF local_debug THEN
    DISPLAY "activity_type_input() - l_activity_type_rec.atype_name=", l_activity_type_rec.atype_name
  END IF

  CALl fgl_window_close("w_activity_type")
  RETURN l_activity_type_rec.*
END FUNCTION

######################################################
# FUNCTION activity_type_view_by_rec(p_activity_type_rec)
#
# View an activity type record (with form/window)
#
# RETURN NONE
######################################################
FUNCTION activity_type_view(p_activity_type_id)
  DEFINE 
    l_activity_type_rec RECORD LIKE activity_type.*,
    p_activity_type_id LIKE activity_type.type_id

    CALL get_activity_type_rec(p_activity_type_id)    RETURNING l_activity_type_rec.*
    CALL activity_type_view_by_rec(l_activity_type_rec.*)
ENd FUNCTION


######################################################
# FUNCTION activity_type_view_by_rec(p_activity_type_rec)
#
# View an activity type record (with form/window)
#
# RETURN NONE
######################################################
FUNCTION activity_type_view_by_rec(p_activity_type_rec)
  DEFINE 
    p_activity_type_rec RECORD LIKE activity_type.*,
    inp_char            CHAR
    #p_activity_type_id  LIKE activity_type.type_id

    CALL fgl_window_open("w_activity_type", 3, 3, get_form_path("f_activity_type_l2"), FALSE)
    CALL populate_act_type_form_labels_g()

    DISPLAY get_str(811) TO bt_ok
    DISPLAY "*" TO bt_cancel
    #    CALL populate_activity_form_combo_boxes()

  DISPLAY BY NAME p_activity_type_rec.* 

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char  HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    ENd PROMPT
  END WHILE

  CALL fgl_window_close("w_activity_type_view")

END FUNCTION


