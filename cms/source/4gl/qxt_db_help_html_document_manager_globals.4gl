##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the tools library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

GLOBALS

  DEFINE qxt_glob_int_switch SMALLINT

  DEFINE t_qxt_help_html_doc_rec TYPE AS
    RECORD
     help_html_doc_id      LIKE qxt_help_html_doc.help_html_doc_id,
     language_id           LIKE qxt_help_html_doc.language_id,
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_mod_date    LIKE qxt_help_html_doc.help_html_mod_date,
     help_html_data        LIKE qxt_help_html_doc.help_html_data
    END RECORD

  DEFINE t_qxt_help_html_doc_no_blob_rec TYPE AS
    RECORD
     help_html_doc_id      LIKE qxt_help_html_doc.help_html_doc_id,
     language_id           LIKE qxt_help_html_doc.language_id,
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_mod_date    LIKE qxt_help_html_doc.help_html_mod_date
 
    END RECORD

  DEFINE t_qxt_help_html_doc_no_blob_form_rec TYPE AS
    RECORD
     help_html_doc_id      LIKE qxt_help_html_doc.help_html_doc_id,
     language_name         LIKE qxt_language.language_name,
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_mod_date    LIKE qxt_help_html_doc.help_html_mod_date
 
    END RECORD

  DEFINE t_qxt_help_html_doc_form_rec TYPE AS
    RECORD
     help_html_doc_id      LIKE qxt_help_html_doc.help_html_doc_id,
     language_name         LIKE qxt_language.language_name,
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_mod_date    LIKE qxt_help_html_doc.help_html_mod_date,
     help_html_data        LIKE qxt_help_html_doc.help_html_data
 
    END RECORD

  DEFINE t_qxt_help_html_doc_file TYPE AS
    RECORD
     help_html_filename    LIKE qxt_help_html_doc.help_html_filename,
     help_html_data        LIKE qxt_help_html_doc.help_html_data
    END RECORD


END GLOBALS


