##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the contact position (in comp) table   (position_type)
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                      RETURN
# position_type_combo_list(cb_field_name)          populates the position combo box                  NONE
# get_position_type(p_type_id)                     get position type name from p_type_id             l_ptype_name
# position_type_popup(p_type_id)                   displays selection list and returns the type_id   l_position_type_arr[i].type_id or p_type_id
# position_type_create(()                          creates a new position_type record                NONE
# position_type_delete(p_type_id)                  deletes a position_type record                    NONE
# position_type_edit(p_type_id)                    edit/modify a position_type record                NONE
# position_type_input(p_position_type)             input data into the position type record          l_position_type
# position_type_view(p_position_type_id)           Display record details in window-form by ID       NONE
# position_type_view_by_rec(p_position_type_rec)   Display record details in window-form by record   NONE
# grid_header_pos_type_scroll()                    Populate grid header of position type             NONE
############################################################################################################


###########################################################
# GLOBALS
###########################################################
GLOBALS "globals.4gl"


###########################################################
# position_type_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for position_type_popup()
#
# RETURN NONE
###########################################################
FUNCTION position_type_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "type_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

 
  LET sql_stmt = "SELECT * FROM position_type "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_pos_type FROM sql_stmt
  DECLARE c_pos_type CURSOR FOR p_pos_type




END FUNCTION

###########################################################
# position_type_popup(p_type_id,p_order_field,p_accept_action)
#
# displays selection list and returns the type_id
#
# RETURN l_position_type_arr[i].type_id or p_type_id
###########################################################
FUNCTION position_type_popup(p_type_id,p_order_field,p_accept_action)
  DEFINE 
    p_type_id                    LIKE position_type.type_id,
    l_position_type_arr          DYNAMIC ARRAY OF RECORD LIKE position_type.*  ,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


    CALL fgl_window_open("w_pos_type_scroll", 2, 8, get_form_path("f_pos_type_scroll_l2"), FALSE) 
    CALL populate_pos_type_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/business/customer/customer-position.png")
	CALL ui.Interface.setText("Position Type")
	
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL position_type_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_pos_type INTO l_position_type_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1
    IF NOT i THEN
      CALL fgl_window_close("w_pos_type_scroll")
      RETURN NULL
    END IF

		IF i > 0 THEN
			CALL l_position_type_arr.resize(i)  --correct the last element of the dynamic array
		END IF
		
		
    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_position_type_arr TO sa_pos_type.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
 
      BEFORE DISPLAY
        CALL publish_toolbar("PositionTypeList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_position_type_arr[i].type_id
        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL position_type_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL position_type_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL position_type_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL position_type_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","position_type_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "position_type_popup(p_type_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("position_type_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL position_type_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_position_type_arr[i].type_id
        CALL position_type_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_position_type_arr[i].type_id
        CALL position_type_delete(i)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "type_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "ptype_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "user_def"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_pos_type_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_type_id
  ELSE 
    LET i = arr_curr()
    RETURN l_position_type_arr[i].type_id
  END IF

END FUNCTION


###########################################################
# FUNCTION position_type_create()
#
# creates a new position_type record
#
# RETURN NONE
###########################################################
FUNCTION position_type_create()
  DEFINE 
    l_position_type RECORD LIKE position_type.*

  CALL position_type_input(l_position_type.*)
    RETURNING l_position_type.*

  IF NOT int_flag THEN 
    LET l_position_type.type_id = 0
    INSERT INTO position_type VALUES (l_position_type.*)
  ELSE
    LET int_flag = FALSE
  END IF
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate


###########################################################
# FUNCTION position_type_delete(p_type_id)
#
# deletes a position_type record
#
# RETURN NONE
###########################################################
FUNCTION position_type_delete(p_type_id)
  DEFINE 
    p_type_id INTEGER

  #"Delete!","Are you sure you want to delete this value?"
  IF yes_no(get_str(55),get_str(56)) THEN
    DELETE FROM position_type WHERE type_id = p_type_id
  END IF
END FUNCTION


###########################################################
# FUNCTION position_type_edit(p_type_id)
#
# edit/modify a position_type record
#
# RETURN NONE
###########################################################
FUNCTION position_type_edit(p_type_id)
  DEFINE 
    p_type_id       INTEGER,
    l_position_type RECORD LIKE position_type.*

  SELECT * 
    INTO l_position_type
    FROM position_type
    WHERE position_type.type_id = p_type_id
  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL position_type_input(l_position_type.*) 
    RETURNING l_position_type.*

  IF NOT int_flag THEN
    UPDATE position_type
      SET ptype_name = l_position_type.ptype_name,
          user_def = l_position_type.user_def
      WHERE position_type.type_id = l_position_type.type_id
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


###########################################################
# FUNCTION position_type_input(p_position_type)
#
# input data into the position type record
#
# RETURN l_position_type
###########################################################
FUNCTION position_type_input(p_position_type)
  DEFINE 
    p_position_type RECORD LIKE position_type.*,
    l_position_type RECORD LIKE position_type.*

  LET l_position_type.* = p_position_type.*

    CALL fgl_window_open("w_pos_type", 2, 2, get_form_path("f_pos_type_l2"), FALSE)
    CALL populate_pos_type_form_labels_g()

  INPUT BY NAME l_position_type.* WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_position_type.ptype_name IS NULL THEN
          #"The 'Position Type' (name) field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(2010),"error")
          CONTINUE INPUT
        END IF
      ELSE
        #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_Str(53),get_str(54)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  CALL fgl_window_close("w_pos_type")

  #LET int_flag = FALSE
  RETURN l_position_type.*

END FUNCTION


###########################################################
# FUNCTION position_type_view(p_position_type_id)
#
# Display record details in window-form by ID
#
# RETURN NONE
###########################################################
FUNCTION position_type_view(p_position_type_id)
  DEFINE 
    p_position_type_id LIKE position_type.type_id,
    l_position_type_rec RECORD LIKE position_type.*

  CALL get_position_type_rec(p_position_type_id) RETURNING l_position_type_rec.*
  CALL position_type_view_by_rec(l_position_type_rec.*)


END FUNCTION


###########################################################
# FUNCTION position_type_view_by_rec(p_position_type_rec)
#
# Display record details in window-form by record
#
# RETURN NONE
###########################################################
FUNCTION position_type_view_by_rec(p_position_type_rec)
  DEFINE 
    p_position_type_rec RECORD LIKE position_type.*,
    inp_char            CHAR

    CALL fgl_window_open("w_pos_type", 2, 2, get_form_path("f_pos_type_l2"), FALSE)
    CALL populate_pos_type_form_labels_g()

    DISPLAY get_str(811) TO bt_cancel
    DISPLAY "*" TO bt_ok


  DISPLAY BY NAME p_position_type_rec.* 

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_pos_type")

END FUNCTION

