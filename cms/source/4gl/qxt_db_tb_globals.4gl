##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Globals file for the toolbar db management library
#
#
#
################################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms


################################################################################
# GLOBALS
################################################################################
GLOBALS

  #Keep Filter history
  DEFINE
    qxt_g_filter
      RECORD
        application_id     LIKE qxt_application.application_id,
        tb_name            LIKE qxt_tb.tb_name,
        tb_instance        LIKE qxt_tb.tb_instance,
        tbi_name           LIKE qxt_tbi.tbi_name,
        tbi_obj_name       LIKE qxt_tbi_obj.tbi_obj_name,
        icon_category1_id  LIKE qxt_icon.icon_category1_id,
        icon_category2_id  LIKE qxt_icon.icon_category2_id,
        icon_category3_id  LIKE qxt_icon.icon_category3_id
      END RECORD

  DEFINE
    qxt_data_language_id   LIKE qxt_language.language_id   --is required, if the user works in a different language but want to view 'another' language related data

#################################################################################
#  International Toolbar Icons / key labels
#################################################################################

{
  DEFINE 
    t_qxt_toolbar TYPE AS 
      RECORD
        m_name CHAR(20),
        m_item VARCHAR(20),
        action_time SMALLINT,
        action SMALLINT,
        dialog SMALLINT,
        event VARCHAR(10),
        txt_en VARCHAR(80),
        txt_sp VARCHAR(80),
        txt_de VARCHAR(80),
        txt_fr VARCHAR(80),
        txt_ar VARCHAR(80),
        txt_it VARCHAR(80),
        txt_ot VARCHAR(80),
        image VARCHAR(50),
        ord SMALLINT,
        stat SMALLINT
      END RECORD

  DEFINE 
    qxt_toolbar DYNAMIC ARRAY OF t_qxt_toolbar,
    qxt_toolbar_array_size SMALLINT

  DEFINE 
    t_qxt_toolbar_short TYPE AS
      RECORD
        m_name VARCHAR(20),
        m_item VARCHAR(20),
        event VARCHAR(10),
        stat SMALLINT,
        action SMALLINT,
        ord SMALLINT,
        txt_en VARCHAR(80),
        dialog SMALLINT,
        action_time SMALLINT,
        image VARCHAR(50)
      END RECORD
}


######################################################
# Icons
######################################################
#icon_size TYPE
  DEFINE t_qxt_icon_size_rec TYPE AS
    RECORD
      icon_size_name          LIKE qxt_icon_size.icon_size_name
    END RECORD

  DEFINE t_qxt_icon_size_form_rec TYPE AS
    RECORD
      icon_size_name          LIKE qxt_icon_size.icon_size_name
    END RECORD


#icon_path TYPE
  DEFINE t_qxt_icon_path_rec TYPE AS
    RECORD
      icon_path_name          LIKE qxt_icon_path.icon_path_name,
      icon_path_dir           LIKE qxt_icon_path.icon_path_dir
    END RECORD 

  DEFINE t_qxt_icon_path_form_rec TYPE AS
    RECORD
      icon_path_name          LIKE qxt_icon_path.icon_path_name,
      icon_path_dir           LIKE qxt_icon_path.icon_path_dir
    END RECORD


#icon_property TYPE

  DEFINE t_qxt_icon_property_rec TYPE AS
    RECORD
      icon_property_name         LIKE qxt_icon_property.icon_property_name,
      icon_size_name             LIKE qxt_icon_property.icon_size_name,
      icon_path_name             LIKE qxt_icon_property.icon_path_name
    END RECORD

  DEFINE t_qxt_icon_property_form_rec TYPE AS
    RECORD
      icon_property_name         LIKE qxt_icon_property.icon_property_name,
      icon_size_name             LIKE qxt_icon_size.icon_size_name,
      icon_path_dir              LIKE qxt_icon_path.icon_path_dir
    END RECORD

  #DEFINE t_icon_property_name_rec TYPE AS
  #  RECORD
  #    icon_property_id           LIKE icon_property.icon_property_id,
  #    icon_property_name         LIKE icon_property.icon_property_name
  #  END RECORD


#icon TYPE
  DEFINE t_qxt_icon_rec TYPE AS
    RECORD
      icon_filename         LIKE qxt_icon.icon_filename,
      #icon_property_name   LIKE qxt_icon.icon_property_name,
      icon_category1_id     LIKE qxt_icon.icon_category1_id,
      icon_category2_id     LIKE qxt_icon.icon_category2_id,
      icon_category3_id     LIKE qxt_icon.icon_category3_id

    END RECORD

  DEFINE t_qxt_icon_form_rec TYPE AS
    RECORD
      icon_filename        LIKE qxt_icon.icon_filename,
      #icon_property_name  LIKE qxt_icon_property.icon_property_name,
      icon_category1_data  LIKE qxt_icon_category.icon_category_data,
      icon_category2_data  LIKE qxt_icon_category.icon_category_data,
      icon_category3_data  LIKE qxt_icon_category.icon_category_data

    END RECORD


#icon_category TYPE
  DEFINE t_qxt_icon_category_rec TYPE AS
    RECORD
      icon_category_id      LIKE qxt_icon_category.icon_category_id,
      language_id           LIKE qxt_icon_category.language_id,
      icon_category_data    LIKE qxt_icon_category.icon_category_data
    END RECORD

  DEFINE t_qxt_icon_category_form_rec TYPE AS
    RECORD
      icon_category_id      LIKE qxt_icon_category.icon_category_id,
      language_name         LIKE qxt_language.language_name,
      icon_category_data    LIKE qxt_icon_category.icon_category_data

    END RECORD




#tbi_obj_event TYPE - EVENT
  DEFINE t_qxt_tbi_obj_event_rec TYPE AS
    RECORD
      tbi_obj_event_name LIKE qxt_tbi_obj_event.tbi_obj_event_name
    END RECORD

  DEFINE t_qxt_tbi_obj_event_form_rec TYPE AS
    RECORD
      tbi_obj_event_name LIKE qxt_tbi_obj_event.tbi_obj_event_name
    END RECORD




#tbi_obj TYPE
  DEFINE t_qxt_tbi_obj_rec TYPE AS
    RECORD
      tbi_obj_name          LIKE qxt_tbi_obj.tbi_obj_name,
      event_type_id         LIKE qxt_tbi_event_type.event_type_id,
      tbi_obj_event_name    LIKE qxt_tbi_obj.tbi_obj_event_name,
      tbi_obj_action_id     LIKE qxt_tbi_obj.tbi_obj_action_id,
      icon_filename         LIKE qxt_tbi_obj.icon_filename,
      string_id             LIKE qxt_tbi_obj.string_id,
      icon_category1_id     LIKE qxt_tbi_obj.icon_category1_id,
      icon_category2_id     LIKE qxt_tbi_obj.icon_category2_id,
      icon_category3_id     LIKE qxt_tbi_obj.icon_category3_id

    END RECORD

  DEFINE t_qxt_tbi_obj_form_rec TYPE AS
    RECORD
      tbi_obj_name           LIKE qxt_tbi_obj.tbi_obj_name,
      #tbi_obj_event         LIKE qxt_tbi_obj.tbi_obj_event,
      event_type_name        LIKE qxt_tbi_event_type.event_type_name,
      tbi_obj_event_name     LIKE qxt_tbi_obj_event.tbi_obj_event_name,
      #tbi_obj_action        LIKE qxt_tbi_obj.tbi_obj_action_id,
      tbi_action_name        LIKE qxt_tbi_obj_action.tbi_action_name,
      icon_filename          LIKE qxt_tbi_obj.icon_filename,
      string_id              LIKE qxt_tbi_obj.string_id,
      label_data             LIKE qxt_tbi_tooltip.label_data,
      string_data            LIKE qxt_tbi_tooltip.string_data,
      icon_category1_data    LIKE qxt_icon_category.icon_category_data,
      icon_category2_data    LIKE qxt_icon_category.icon_category_data,
      icon_category3_data    LIKE qxt_icon_category.icon_category_data

    END RECORD

  DEFINE t_qxt_tbi_obj_form_rec2 TYPE AS  --used for grid
    RECORD
      tbi_obj_name          LIKE qxt_tbi_obj.tbi_obj_name,
      #tbi_obj_event        LIKE qxt_tbi_obj.tbi_obj_event,
      event_type_name       LIKE qxt_tbi_event_type.event_type_name,
      tbi_obj_event_name    LIKE qxt_tbi_obj_event.tbi_obj_event_name,
      #tbi_obj_action_id    LIKE qxt_tbi_obj.tbi_obj_action_id,
      tbi_action_name       LIKE qxt_tbi_obj_action.tbi_action_name,
      icon_filename         LIKE qxt_tbi_obj.icon_filename,
      string_id             LIKE qxt_tbi_obj.string_id,
      label_data            LIKE qxt_tbi_tooltip.label_data,     
      string_data           LIKE qxt_tbi_tooltip.string_data,
      icon_category1_data   LIKE qxt_icon_category.icon_category_data,
      icon_category2_data   LIKE qxt_icon_category.icon_category_data,
      icon_category3_data   LIKE qxt_icon_category.icon_category_data

    END RECORD



#application_map TYPE
  DEFINE t_qxt_application_rec TYPE AS
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name
    END RECORD

  DEFINE t_qxt_application_form_rec TYPE AS
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name

    END RECORD

  DEFINE t_qxt_application_form_rec2 TYPE AS  --used for grid
    RECORD
      application_id                         LIKE qxt_application.application_id,
      application_name                       LIKE qxt_application.application_name
    END RECORD

#tbi_tooltip TYPE
  DEFINE
    t_qxt_tbi_tooltip_rec TYPE AS  
      RECORD
        string_id           LIKE qxt_tbi_tooltip.string_id,
        language_id         LIKE qxt_tbi_tooltip.language_id,
        label_data          LIKE qxt_tbi_tooltip.label_data,  
        string_data         LIKE qxt_tbi_tooltip.string_data,
        category1_id        LIKE qxt_string_app.category1_id,
        category2_id        LIKE qxt_string_app.category2_id,
        category3_id        LIKE qxt_string_app.category3_id
      END RECORD

  DEFINE
    t_qxt_tbi_tooltip_form_rec TYPE AS  
      RECORD
        string_id            LIKE qxt_tbi_tooltip.string_id,
        language_name        LIKE qxt_language.language_name,
        label_data          LIKE qxt_tbi_tooltip.label_data,
        string_data          LIKE qxt_tbi_tooltip.string_data,
        category1_data       LIKE qxt_str_category.category_data,
        category2_data       LIKE qxt_str_category.category_data,
        category3_data       LIKE qxt_str_category.category_data
      END RECORD


#tbi TYPE
  DEFINE t_qxt_tbi_rec TYPE AS
    RECORD
      application_id           LIKE qxt_tbi.application_id,
      tbi_name                 LIKE qxt_tbi.tbi_name,
      tbi_obj_name             LIKE qxt_tbi.tbi_obj_name,
      event_type_id            LIKE qxt_tbi_event_type.event_type_id,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_scope_id             LIKE qxt_tbi.tbi_scope_id,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_id            LIKE qxt_tbi.tbi_static_id,
      icon_category1_id        LIKE qxt_tbi_obj.icon_category1_id,
      icon_category2_id        LIKE qxt_tbi_obj.icon_category2_id,
      icon_category3_id        LIKE qxt_tbi_obj.icon_category3_id

    END RECORD

  DEFINE t_qxt_tbi_form_rec TYPE AS
    RECORD
      application_name         LIKE qxt_application.application_name,
      tbi_name                 LIKE qxt_tbi.tbi_name,
      tbi_obj_name             LIKE qxt_tbi.tbi_obj_name,
      event_type_name          LIKE qxt_tbi_event_type.event_type_name,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_scope_name           LIKE qxt_tbi_scope.tbi_scope_name,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_name          LIKE qxt_tbi_static.tbi_static_name,
      icon_category1_data      LIKE qxt_icon_category.icon_category_data,
      icon_category2_data      LIKE qxt_icon_category.icon_category_data,
      icon_category3_data      LIKE qxt_icon_category.icon_category_data

    END RECORD

  DEFINE t_qxt_tbi_form_rec2 TYPE AS  --used for grid
    RECORD
      application_name         LIKE qxt_application.application_name,
      tbi_name                 LIKE qxt_tbi.tbi_name,
      tbi_obj_name             LIKE qxt_tbi.tbi_obj_name,
      event_type_name          LIKE qxt_tbi_event_type.event_type_name,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_action_name          LIKE qxt_tbi_obj_action.tbi_action_name,
      tbi_scope_name           LIKE qxt_tbi_scope.tbi_scope_name,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_name          LIKE qxt_tbi_static.tbi_static_name,
      icon_filename            LIKE qxt_tbi_obj.icon_filename,
      string_id                LIKE qxt_tbi_obj.string_id,
      label_data             	LIKE qxt_tbi_tooltip.label_data,      
      string_data              LIKE qxt_tbi_tooltip.string_data,
      icon_category1_data      LIKE qxt_icon_category.icon_category_data,
      icon_category2_data      LIKE qxt_icon_category.icon_category_data,
      icon_category3_data      LIKE qxt_icon_category.icon_category_data

    END RECORD




#tb TYPE
  DEFINE t_qxt_tb_rec TYPE AS
    RECORD
      application_id           LIKE qxt_tbi.application_id,
      tb_name                  LIKE qxt_tb.tb_name,
      tb_instance              LIKE qxt_tb.tb_instance,
      tbi_name                 LIKE qxt_tbi.tbi_name
    END RECORD

  DEFINE t_qxt_tb_form_rec TYPE AS
    RECORD
      application_name         LIKE qxt_application.application_name,
      tb_name                  LIKE qxt_tb.tb_name,
      tb_instance              LIKE qxt_tb.tb_instance,
      tbi_name                 LIKE qxt_tbi.tbi_name
    END RECORD

  DEFINE t_qxt_tb_form_rec2 TYPE AS  --used for grid
    RECORD
      application_name         LIKE qxt_application.application_name,
      tb_name                  LIKE qxt_tb.tb_name,
      tb_instance              LIKE qxt_tb.tb_instance,
      tbi_name                 LIKE qxt_tbi.tbi_name,
      event_type_name          LIKE qxt_tbi_event_type.event_type_name,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_action_name          LIKE qxt_tbi_obj_action.tbi_action_name,
      tbi_scope_name           LIKE qxt_tbi_scope.tbi_scope_name,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_name          LIKE qxt_tbi_static.tbi_static_name,
      icon_filename            LIKE qxt_tbi_obj.icon_filename,
      label_data            	 LIKE qxt_tbi_tooltip.label_data,      
      string_data              LIKE qxt_tbi_tooltip.string_data

    END RECORD


  #Toolbar export file record
  DEFINE t_qxt_toolbar_file_rec TYPE AS
    RECORD
      #application_id           LIKE qxt_tbi.application_id,
      tb_name                  LIKE qxt_tb.tb_name,
      tb_instance              LIKE qxt_tb.tb_instance,
      event_type_id            LIKE qxt_tbi_event_type.event_type_id,
      tbi_event_name           LIKE qxt_tbi.tbi_event_name,
      tbi_obj_action_id        LIKE qxt_tbi_obj.tbi_obj_action_id,
      tbi_scope_id             LIKE qxt_tbi.tbi_scope_id,
      tbi_position             LIKE qxt_tbi.tbi_position,
      tbi_static_id            LIKE qxt_tbi.tbi_static_id,
      icon_filename            LIKE qxt_tbi_obj.icon_filename,
      string_id                LIKE qxt_tbi_obj.string_id
    END RECORD

END GLOBALS

##############################################################
# EOF
##############################################################




