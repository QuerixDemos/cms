##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

##################################################
# FUNCTION admin_operator(p_operator)
#
# Check, if operator name belongs to admin operator
# 
# RETURN Boolean (true/false)
##################################################
FUNCTION admin_operator(p_operator_name)
  ---simply checks to see if the operator is Admin or not
  DEFINE p_operator_name LIKE operator.name
  DEFINE p CHAR(1)

  SELECT operator.type
  INTO p
  FROM operator
  WHERE operator.name = p_operator_name
  IF p = "A" THEN
    RETURN TRUE
  ELSE 
    RETURN FALSE
  END IF
END FUNCTION


##################################################
# FUNCTION get_operator_name(p_operator_id)
#
# get the operator_id from a operator.name  (note: not contact - operator as in operator like admin)
#
# RETURN l_operator_name
##################################################
FUNCTION get_operator_name(p_operator_id)
  DEFINE 
    p_operator_id   LIKE operator.operator_id,
    l_operator_name LIKE operator.name

  SELECT operator.name
    INTO l_operator_name
    FROM operator
    WHERE operator.operator_id = p_operator_id

  RETURN l_operator_name
END FUNCTION





##################################################
# FUNCTION get_operator_email_address(p_operator_id)
#
# get the email address from the operator (p_operator_id)
#
# RETURN l_email_address
##################################################
FUNCTION get_operator_email_address(p_operator_id)
  DEFINE 
    p_operator_id       LIKE operator.operator_id,
    l_email_address LIKE operator.email_address

  SELECT operator.email_address
    INTO l_email_address
    FROM operator
    WHERE operator.operator_id = p_operator_id

  RETURN l_email_address
END FUNCTION


#################################################
# FUNCTION get_operator_id(p_operator_name)
#
# get the operator_id from a operator.name  (note: not contact - operator as in operator like admin)
#
# RETURN l_operator_id
#################################################
FUNCTION get_operator_id(p_operator_name)
  DEFINE 
    p_operator_name LIKE operator.name,
    l_operator_id   LIKE operator.operator_id

  SELECT operator.operator_id
    INTO l_operator_id
    FROM operator
    WHERE operator.name = p_operator_name

  RETURN l_operator_id
END FUNCTION

{
#################################################
# FUNCTION get_operatorId_bySessionId(p_session_id)
#
# get the operator_id from a operator.session_id  (note: not contact - operator as in operator like admin)
#
# RETURN l_operator_id
#################################################
FUNCTION get_operatorId_bySessionId(p_session_id)
  DEFINE 
    p_session_id		LIKE operator.session_id,
    l_operator_id   LIKE operator.operator_id

  SELECT operator.operator_id
    INTO l_operator_id
    FROM operator
    WHERE operator.session_id = p_session_id

  RETURN l_operator_id
END FUNCTION

#################################################
# FUNCTION get_sessionId_byOperatorId(p_operatorId)
#
# get the session id by operator_id 
#
# RETURN l_operator_id
#################################################
FUNCTION get_sessionId_byOperatorId(p_operatorId)
  DEFINE 
    l_session_id	LIKE operator.session_id,
    p_operatorId   LIKE operator.operator_id

  SELECT operator.session_id
    INTO l_session_id
    FROM operator
    WHERE operator.operator_id = p_operatorId

  RETURN l_session_id
END FUNCTION
}
FUNCTION set_operator_id2(p_operator_id)
	DEFINE p_operator_id LIKE operator.operator_id
	#LET operator.operator_id =
	
  SELECT *
    INTO g_operator
    FROM operator
    WHERE operator.operator_id = p_operator_id 

END FUNCTION


FUNCTION get_operatorid2()
	RETURN g_operator.operator_id
END FUNCTION




FUNCTION getCurrentOperatorId()
	RETURN g_operator.operator_id
END FUNCTION

#################################################
# FUNCTION get_operator_contact_id_from_operator_id(p_operator_id)
#
# get the contact_id from a operator.operator_id  (note: not contact - operator as in operator like admin)
#
# RETURN l_cont_id
#################################################
FUNCTION get_operator_contact_id_from_operator_id(p_operator_id)
  DEFINE 
    l_cont_id LIKE operator.cont_id,
    p_operator_id LIKE operator.operator_id

  SELECT operator.cont_id
    INTO l_cont_id
    FROM operator
    WHERE operator.operator_id = p_operator_id

  RETURN l_cont_id
END FUNCTION


FUNCTION 	init_global_operator_by_id(p_operator_id)
	DEFINE p_operator_id LIKE operator.operator_id

        SELECT *
          INTO g_operator.*
          FROM operator
         WHERE operator.operator_id = p_operator_id

        IF sqlca.sqlcode = 0 THEN   ---valid operator record
          #LET valid_login = TRUE
        ELSE
          #LET valid_login = FALSE
          CALL fgl_message_box("Problem with operator global initialisation","There is a problem with your provide operator id\nNot able to initialise the global operator record")
        END IF

END FUNCTION

#################################################
# FUNCTION get_current_operator_contact_id()
#
# get the contact_id from the current operator
#
# RETURN  g_operator.cont_id
#################################################
FUNCTION get_current_operator_contact_id()

  RETURN g_operator.cont_id
END FUNCTION

#################################################
# FUNCTION set_current_operator_contact_id()
#
# set the contact_id for the current operator
#
# RETURN  NONE
#################################################
FUNCTION set_current_operator_contact_id(p_operator_id)
  DEFINE 
    p_operator_id   LIKE operator.operator_id
    
  LET g_operator.cont_id = p_operator_id
END FUNCTION



##################################################
# FUNCTION get_operator_rec(p_operator_id)
#
# Get the operator record from an id
#
# RETURN l_operator.*
##################################################
FUNCTION get_operator_rec(p_operator_id)
  DEFINE 
    p_operator_id LIKE operator.operator_id,
    l_operator    RECORD LIKE operator.*

  SELECT operator.*
    INTO l_operator.*
    FROM operator
    WHERE operator.operator_id = p_operator_id

  RETURN l_operator.*
END FUNCTION



####################################################
# FUNCTION get_current_operator_rec() 
#
# Return operator record of currently logged in user
####################################################
FUNCTION get_current_operator_rec()
	RETURN g_operator.*
END FUNCTION


####################################################
# FUNCTION get_current_operator_id()
#
# Return currently logged in operator id
#
# RETURN g_operator.operator_id
####################################################
FUNCTION get_current_operator_id()

  RETURN g_operator.operator_id

END FUNCTION


####################################################
# FUNCTION get_current_operator_email_address()
#
# Return currently logged in operator id
#
# RETURN g_operator.email_address
####################################################
FUNCTION get_current_operator_email_address()

  RETURN g_operator.email_address

END FUNCTION

