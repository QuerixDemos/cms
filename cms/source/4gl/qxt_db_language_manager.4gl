##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# DATABASE
################################################################################
DATABASE cms

############################################################
# Globals
############################################################
GLOBALS "qxt_db_language_globals.4gl"

###########################################################
# language_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for language_popup()
#
# RETURN NONE
###########################################################
FUNCTION language_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "language_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

 
  LET sql_stmt = "SELECT * FROM qxt_language "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_language FROM sql_stmt
  DECLARE c_language CURSOR FOR p_language


END FUNCTION

###########################################################
# language_popup(p_language_id,p_order_field,p_accept_action)
#
# displays selection list and returns the language_id
#
# RETURN l_language_arr[i].language_id or p_language_id
###########################################################
FUNCTION language_popup(p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_language_id                    LIKE qxt_language.language_id,
    l_language_arr               DYNAMIC ARRAY OF RECORD LIKE qxt_language.*  ,
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


	CALL fgl_window_open("w_language_scroll", 2, 8, get_form_path("f_qxt_language_scroll_l2"), FALSE) 
	CALL populate_language_list_form_labels_g()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL language_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_language INTO l_language_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_language_arr.reSize(i) -- resize dynamic array to remove last dirty element
		END IF
	    
    
    IF NOT i THEN
      CALL fgl_window_close("w_language_scroll")
      RETURN NULL
    END IF

		
    #CALL set_count(i)
    
    LET int_flag = FALSE
    DISPLAY ARRAY l_language_arr TO sa_language.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 1
 
      BEFORE DISPLAY
        CALL publish_toolbar("LanguageList",0)

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_language_arr[i].language_id
        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL language_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL language_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL language_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL language_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","language_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "language_popup(p_language_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("language_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL language_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_language_arr[i].language_id
        CALL language_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_language_arr[i].language_id
        CALL language_delete(i)
        EXIT DISPLAY

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
            
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_dir"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      
      ON KEY(F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_url"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE

  CALL fgl_window_close("w_language_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_language_id
  ELSE 
    LET i = arr_curr()
    RETURN l_language_arr[i].language_id
  END IF

END FUNCTION



############################################################################################
# Edit/Create/Input.. Functions
############################################################################################


###########################################################
# FUNCTION language_create()
#
# creates a new language record
#
# RETURN NONE
###########################################################
FUNCTION language_create()
  DEFINE 
    l_language RECORD LIKE qxt_language.*

  CALL language_input(l_language.*)
    RETURNING l_language.*

  IF NOT int_flag THEN 
    LET l_language.language_id = 0
    INSERT INTO language VALUES (l_language.*)
  ELSE
    LET int_flag = FALSE
  END IF
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate


###########################################################
# FUNCTION language_delete(p_language_id)
#
# deletes a language record
#
# RETURN NONE
###########################################################
FUNCTION language_delete(p_language_id)
  DEFINE 
    p_language_id INTEGER

  #"Delete!","Are you sure you want to delete this value?"
  IF yes_no(get_str(55),get_str(56)) THEN
    DELETE FROM qxt_languageuage WHERE language_id = p_language_id
  END IF
END FUNCTION


###########################################################
# FUNCTION language_edit(p_language_id)
#
# edit/modify a language record
#
# RETURN NONE
###########################################################
FUNCTION language_edit(p_language_id)
  DEFINE 
    p_language_id       INTEGER,
    l_language RECORD LIKE qxt_language.*

  SELECT * 
    INTO l_language
    FROM qxt_language
    WHERE language_id = p_language_id
  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL language_input(l_language.*) 
    RETURNING l_language.*

  IF NOT int_flag THEN
    UPDATE qxt_language
      SET language_name = l_language.language_name,
          language_dir  = l_language.language_dir

      WHERE qxt_language.language_id = l_language.language_id
  ELSE 
    LET int_flag = FALSE
  END IF
END FUNCTION


###########################################################
# FUNCTION language_input(p_language)
#
# input data into the language record
#
# RETURN l_language
###########################################################
FUNCTION language_input(p_language)
  DEFINE 
    p_language RECORD LIKE qxt_language.*,
    l_language RECORD LIKE qxt_language.*

  LET l_language.* = p_language.*

	CALL fgl_window_open("w_language", 2, 2, get_form_path("f_qxt_language_det_l2"), FALSE)
	CALL populate_language_form_labels_g()

  INPUT BY NAME l_language.* WITHOUT DEFAULTS
    AFTER INPUT
      IF NOT int_flag THEN
        IF l_language.language_name IS NULL THEN
          #"The 'language' (name) field must not be empty !"
          CALL fgl_winmessage(get_str(48),get_str(2010),"error")
          CONTINUE INPUT
        END IF
      ELSE
        #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
        IF NOT yes_no(get_Str(53),get_str(54)) THEN
          #LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF
  END INPUT

  CALL fgl_window_close("w_language")

  #LET int_flag = FALSE
  RETURN l_language.*

END FUNCTION




#################################################################################################
# View Record Functions
#################################################################################################


###########################################################
# FUNCTION language_view(p_language_id)
#
# Display record details in window-form by ID
#
# RETURN NONE
###########################################################
FUNCTION language_view(p_language_id)
  DEFINE 
    p_language_id LIKE qxt_language.language_id,
    l_language_rec RECORD LIKE qxt_language.*

  CALL get_language_rec(p_language_id) RETURNING l_language_rec.*
  CALL language_view_by_rec(l_language_rec.*)


END FUNCTION


###########################################################
# FUNCTION language_view_by_rec(p_language_rec)
#
# Display record details in window-form by record
#
# RETURN NONE
###########################################################
FUNCTION language_view_by_rec(p_language_rec)
  DEFINE 
    p_language_rec RECORD LIKE qxt_language.*,
    inp_char            CHAR

	CALL fgl_window_open("w_language", 2, 2, get_form_path("f_qxt_language_det_l2"), FALSE)
	CALL populate_language_form_labels_g()

	DISPLAY get_str(811) TO bt_cancel
	DISPLAY "*" TO bt_ok


  DISPLAY BY NAME p_language_rec.* 

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  END WHILE

  CALL fgl_window_close("w_language")

END FUNCTION


#######################################################################################################
# EOF
#######################################################################################################



