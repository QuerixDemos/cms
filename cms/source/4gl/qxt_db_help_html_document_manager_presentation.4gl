##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_help_html_doc_scroll()
#
# Populate help_html grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_help_html_doc_scroll()
  CALL fgl_grid_header("sc_help_html","help_html_doc_id",get_str_tool(971),"right","F13")  --Help ID
  CALL fgl_grid_header("sc_help_html","language_name",get_str_tool(972),"left","F14")  --language
  CALL fgl_grid_header("sc_help_html","help_html_filename",get_str_tool(973),"left","F15")  --file name
  CALL fgl_grid_header("sc_help_html","help_html_mod_date",get_str_tool(974),"left","F16")  --modificationn date


END FUNCTION

#######################################################
# FUNCTION populate_help_html_doc_form_labels_g()
#
# Populate help_html form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_html_doc_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(970) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION






#######################################################
# FUNCTION populate_help_html_doc_form_edit_labels_g()
#
# Populate help_html form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_html_doc_form_edit_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","HTML Preview") 

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download

  CALL language_combo_list("language_name")


END FUNCTION






#######################################################
# FUNCTION populate_help_html_doc_form_view_labels_g()
#
# Populate help_html form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_html_doc_form_view_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","Preview") 
  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_done
  DISPLAY "!" TO bt_done

END FUNCTION






#######################################################
# FUNCTION populate_help_html_doc_form_create_labels_g()
#
# Populate help_html form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_html_doc_form_create_labels_g()
  CALL updateUILabel("cntDetail1GroupBox","Details")  
  CALL updateUILabel("cntDetail3GroupBox","HTML Preview") 
  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download

  CALL language_combo_list("language_name")

END FUNCTION




#######################################################
# FUNCTION populate_help_html_doc_list_form_labels_g()
#
# Populate help_html list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_help_html_doc_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_help_html_doc_scroll()

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete


  DISPLAY get_str_tool(15) TO bt_view
  DISPLAY "!" TO bt_view
END FUNCTION



