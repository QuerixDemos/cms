##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################





######################################################
# FUNCTION document_popup(p_document_id,p_order_field,p_accept_action)
#
# Document selection window
#
# RETURN p_document_id
######################################################
FUNCTION document_popup(p_document_id,p_order_field,p_accept_action)
  DEFINE 
    p_document_id    SMALLINT,
    p_order_field    VARCHAR(30),
    p_accept_action      SMALLINT

  CALL fgl_winmessage("This feature is not available in the QXT_FILE Library","This feature is not available in the QXT_FILE Library\nTo use the document manager, \nlink qxt_db_document.lib and qxt_db_document_manager.lib","info")

  RETURN NULL

END FUNCTION



######################################################
# FUNCTION download_blob_document_to_client(p_document_id,p_client_file_path,p_dialog)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION download_blob_document_to_client(p_document_id,p_client_file_path,p_dialog)
  DEFINE 
    p_document_id       SMALLINT,
    p_client_file_path  VARCHAR(250),
    p_dialog            SMALLINT

  CALL fgl_winmessage("This feature is not available in the QXT_FILE Library","This feature is not available in the QXT_FILE Library\nTo use the document manager, \nlink qxt_db_document.lib and qxt_db_document_manager.lib","info")

  RETURN NULL

END FUNCTION




######################################################
# FUNCTION get_document_id_from_filename(p_filename)
#
# Get the document_id from a file
#
# RETURN l_document_id
######################################################
FUNCTION get_document_id_from_filename(p_filename)
  DEFINE 
    p_filename    VARCHAR(30)

  CALL fgl_winmessage("This feature is not available in the QXT_FILE Library","This feature is not available in the QXT_FILE Library\nTo use the document manager, \nlink qxt_db_document.lib and qxt_db_document_manager.lib","info")

  RETURN NULL

END FUNCTION



