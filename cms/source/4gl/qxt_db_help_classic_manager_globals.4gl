##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

database cms

GLOBALS


#icon_category TYPE
  DEFINE t_qxt_help_classic_rec TYPE AS
    RECORD
      id           LIKE qxt_help_classic.id,
      filename     LIKE qxt_help_classic.filename

    END RECORD

  DEFINE t_qxt_help_classic_form_rec TYPE AS
    RECORD
      id           LIKE qxt_help_classic.id,
      filename     LIKE qxt_help_classic.filename
    END RECORD

  DEFINE qxt_help_classic_file DYNAMIC ARRAY OF VARCHAR(100)

END GLOBALS
