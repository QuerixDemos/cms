##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##############################################################################################################################################
# Company Record related functions
##############################################################################################################################################
#
# FUNCTION:                                      DESCRIPTION:                                                            RETURN VALUE:
# company_main(p_company_id)                     main company menu                                                       NONE
# get_company_name(p_comp_id)                    get company name                                                        l_comp_name
# get_contact_comp_name(p_contact_id)            get the company name of a contact_id                                    l_comp_name
# get_comp_name(p_company_id)                    Get the corresponding company name (from ID)                            l_comp_name
# get_company_id_from_contact_id(p_contact_id)   get company_id  from contact_id                                       l_comp_id
# get_form_company(p_company_id)                 ????                                                                    l_company_rec.*
# get_comp_id(p_comp_name)                       Get the corresponding company ID from name                              l_comp_id
# get_company_rec(p_company_id)                  get company record from company_id                                      l_company.* (RECORD LIKE company.*)
# get_company_id(p_company)                      get company_id  from company name                                       l_company.comp_id
# locate_contact()                               ??????                                                                  NONE
# add_contact(l_company_id)                      assign contact to company                                               NONE
# company_to_form_company(p_company)             ????                                                                    l_company_rec.*
# company_create(p_window)                       Create new company - p_window is if a new window should be opened       rv  --Integer 
# company_input(l_company.*)                     Input contact details                                                   p_company.*
#                                                sub-function of company_create() actual company edit/create function
# company_query()                                Search lbInfo1 for company query                                           comp_arr[i].comp_id   OR NULL
# company_edit(p_company_id)                     Edit a company record - uses company_input()                            NONE
# company_delete(p_company_id)                   Delete a company record                                                 NULL or ??? can this be right ?
# company_popup()                                Displays popup window and allows operator to select a company           l_company[i].comp_id  or NULL
# company_show(p_company_id)                     Display company details to form (fields by name)                        NONE
# company_query()                                Search Company - uses company_query()                                   l_comp_id
# company_choose()                               ??????????????????????                                                  l_company_id
# company_view(p_company_id)                     View company details in a window                                        NONE
# company_view_short_by_id(p_company_id)         Display small/short set of company details to form (fields by name)     NOTHING
# set_comp_default_titlebar()                    Labels the titlebar with the default label                              NONE
# populate_company_form_combo_boxes()            populates all combo boxes dynamically from dB                           NONE
# advanced_company_lookup(p_comp_id)             Allows to search by entering the first letters                          rv_comp_id
# update_advanced_company_lookup(
#    query_filter, field_filter, do_choose)      sub-routine of advanced_company_lookup to update display                rv_comp_id  (LIKE contact.cont_id)
# company_name_count(p_comp_name)               tests if a record with this name already exists                         r_count
# company_contacts_populate_grid_panel_data_source Data Source for company contacts grid population                      NONE
#   (p_company_id,p_order_field)
# company_contacts_populate_grid_panel           displays a list of all contacts associated with a company               NONE
# (p_company_id,p_order_field,p_scroll)
# grid_header_company_scroll()                   Populate the grid header of the company scroll window                   NONE
# grid_header_company_scroll_advanced()          Populate the grid header 'advanced comp search'                         NONE
# grid_header_company_contact_member()           Populate the grid header of the company contact members                 NONE
# print_company_html(p_company_id)               Prints company details using a dynamically created html page.           NONE
# get_company_short_rec(p_company_id)            Get the corresponding company name (from ID)                            l_company_short_rec
# 
##############################################################################################################################################
# Modification
#
# 17.05.2012 Bondar A.G. Function company_create() has been changed 
##############################################################################################################################################
###################################################
# GLOBALS
###################################################
GLOBALS "globals.4gl"



################################################################################################
# MAIN MENU
################################################################################################

###################################################
# FUNCTION company_main(p_company_id)
###################################################
FUNCTION company_main(p_company_id)
############################################
# Note: argument p_company_id is current or no longer used.. leave it for now
############################################
  DEFINE 
    p_company_id  			LIKE company.comp_id,
    l_company_id,rv_id  LIKE company.comp_id,
    l_company_rec 			RECORD LIKE company.*,
    l_contact_id  			LIKE contact.cont_id,
    l_contact_rec 			RECORD LIKE contact.*,
    main_cont     			LIKE company.comp_main_cont,
    contact_name1 			LIKE contact.cont_name,
    id                  LIKE contact.cont_id,
    test          			INTEGER,
    pend_company_id     LIKE company.comp_id,
    tmp_comp_name       LIKE company.comp_name,
    p_order_field2, p_order_field VARCHAR(128),
    menu_str_arr1 			DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 			DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

	DEFINE fSearch STRING
 
  #CLEAR SCREEN  ?? huho: why was this here ?

  LET p_order_field2 = NULL

	OPEN WINDOW w_company_main
      AT 1,1
      WITH FORM "form/f_company_det_l2"
    #CALL set_comp_default_titlebar()
    #CALL populate_company_form_combo_boxes()  --populate all combo boxes in this form dynamically
    #CALL grid_header_company_contact_member()
    CALL populate_company_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/business/company/company.png")
	CALL ui.Interface.setText("Company")

  WHILE (TRUE)
    DECLARE c_comp_scroll SCROLL CURSOR FOR
      SELECT comp_id, comp_name
        FROM company
        ORDER BY comp_name ASC

    OPEN c_comp_scroll

    FETCH FIRST c_comp_scroll INTO l_company_id, tmp_comp_name

    IF exist(pend_company_id) THEN
      WHILE l_company_id <> pend_company_id
        FETCH NEXT c_comp_scroll INTO l_company_id, tmp_comp_name
        IF sqlca.sqlcode THEN
          FETCH FIRST c_comp_scroll INTO l_company_id, tmp_comp_name
          EXIT WHILE
        END IF
      END WHILE
    END IF

    LET pend_company_id = NULL
    CALL company_show(l_company_id)

	LET fSearch = "A%"
################################################ DIALOG BEGIN #############################################
	DIALOG ATTRIBUTES(UNBUFFERED, FIELD ORDER FORM)

	
		INPUT BY NAME fSearch
		
			ON ACTION actSearchCompanyGlobal
				LET pend_company_id =  companySearch(fSearch)

        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_company_id) THEN 
          EXIT DIALOG
        END IF
 
 
         IF exist(pend_company_id) THEN
          LET l_company_id = pend_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT DIALOG
 
 			AFTER FIELD fSearch
				LET pend_company_id =  companySearch(fSearch)

        CALL set_help_id(207)
        #LET pend_contact_id = query_contact()
        IF exist(pend_company_id) THEN 
          EXIT DIALOG
        END IF
 
 
         IF exist(pend_company_id) THEN
          LET l_company_id = pend_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT DIALOG 			
        
        {
        #CALL contact_show(l_contact_id)
        CALL company_show(l_company_id)
        CALL contact_activities_populate_grid_panel(l_contact_id, NULL,NULL,NULL)
        #clean up

        CALL displayKeyGuide("")
        CALL set_cont_default_titlebar()

}
      ON KEY (F201) --(F201,"E") --menu_str_arr1[1] menu_str_arr2[1] HELP 111  --"Edit" "Edit a company record" Help 111

        #CALL fgl_settitle("Edit/Modify the current company record")
        CALL fgl_settitle(get_str(151))  --"Edit company details")
        CALL company_edit(l_company_id,FALSE)
        CALL company_show(l_company_id)
        #clean up

        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")

      ON KEY (F202) --(F202,"D") --menu_str_arr1[2] menu_str_arr2[2] HELP 111  --"Delete" "Delete current company record" Help 112

				IF getDemoMode() = TRUE THEN
					CALL demoDelete()
				ELSE
				

        CALL fgl_settitle(get_str(153))  --"Delete the currently displayed company record")
        IF NOT exist(l_company_id) THEN
          LET l_company_id = company_query()
        END IF
        IF exist(l_company_id) THEN
          CALL company_show(l_company_id)
          CALL company_delete(l_company_id) RETURNING rv_id

          #The function company_delete() will return NULL if it was aborted, 
          IF rv_id IS NULL THEN
            CALL company_show(l_company_id)
          ELSE
            LET l_company_id = NULL
            -- CLEAR FORM
          EXIT DIALOG -- cursor state is now invalid
          END IF

          CALL set_comp_default_titlebar()
        END IF
				END IF  --demo mode end if



      ON KEY (F203) --(F203,"N")  --menu_str_arr1[3] menu_str_arr2[3] HELP 113  --"New" "Create a new company record" Help 113

        CALL fgl_settitle(get_str(155))  --"Create new company record")
        CALL set_comp_default_titlebar()
        LET rv_id = company_create(FALSE)  --False = don't open a new window

        IF rv_id IS NULL THEN
          #NULL means, the new record creation was aborted or failed
          #In this case, we will display the last displayed record
            CALL company_show(l_company_id)  
        ELSE
          LET l_company_id = rv_id
          IF exist(l_company_id) THEN
            CALL company_show(l_company_id)        
            LET pend_company_id = l_company_id
            EXIT DIALOG
          END IF
        END IF


        #clean up
        CALL displayKeyGuide("")
        CALL set_comp_default_titlebar()


      ON KEY (F204) --(F204,"S")  --menu_str_arr1[4] menu_str_arr2[4] HELP 114  --"Quick Sarch" "Quick Search for an existing company" Help 114

        CALL fgl_settitle(get_str(157))  --"Search for an existing company record")
        LET l_company_id = advanced_company_lookup(l_company_id)
        IF exist(l_company_id) THEN
          LET pend_company_id = l_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT DIALOG



      ON KEY (F205) --(F205,"S")  --menu_str_arr1[5] menu_str_arr2[5] HELP 115  -- "Det. Search" "Detailed Search for an existing company" Help 115
        CALL fgl_settitle(get_str(159))  --"Search for an existing company record")
        LET l_company_id = company_query()
        IF exist(l_company_id) THEN
          LET pend_company_id = l_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT DIALOG



      ON KEY (F206) --(F206,"A")  --menu_str_arr1[6] menu_str_arr2[6] HELP 116  -- "Assign contact" "Add a contact relationship to this company" Help 116
        CALL fgl_settitle(get_str(161))  --"Assign main contact to company record")
        IF NOT exist(l_company_id) THEN
          LET l_company_id = company_query()
        END IF

        CALL company_show(l_company_id)

        IF l_company_id IS NOT NULL THEN
          CALL add_contact(l_company_id)
        END IF

        CALL company_show(l_company_id)


        CALL set_comp_default_titlebar()


      ON KEY (F91,"KEY_HOME") --(F91,"KEY_HOME")  --First Record
        FETCH FIRST c_comp_scroll 
          INTO l_company_id, tmp_comp_name
        CALL company_show(l_company_id)

      ON KEY (F93,"PREVPAGE",UP) --(F93,"PREVPAGE",UP) --Previous Record -1
        FETCH PRIOR c_comp_scroll
          INTO l_company_id, tmp_comp_name
        IF sqlca.sqlcode = NOTFOUND THEN
          FETCH FIRST c_comp_scroll
            INTO l_company_id, tmp_comp_name
        END IF
        CALL company_show(l_company_id)

      ON KEY (F94,"NEXTPAGE",DOWN) --(F94,"NEXTPAGE",DOWN) --Next Record +1
        FETCH NEXT c_comp_scroll 
          INTO l_company_id, tmp_comp_name
        IF sqlca.sqlcode = NOTFOUND THEN
          FETCH LAST c_comp_scroll
            INTO l_company_id, tmp_comp_name
        END IF
        CALL company_show(l_company_id) 

      ON KEY (F96,"KEY_END") --(F96,"KEY_END")   --Last Record
        FETCH LAST c_comp_scroll
          INTO l_company_id, tmp_comp_name
        CALL company_show(l_company_id)

      ON KEY (F11)
        CALL company_contacts_populate_grid_panel(l_company_id,NULL,TRUE,1)  --Activate/Edit grid panel (TRUE)


      #Column Sort short cuts for grid column 
      ON KEY (F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      ON KEY (F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      ON KEY (F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_title"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      ON KEY (F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      ON KEY (F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_lname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      ON KEY (F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_phone"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())


      ON KEY (F12) --(F12,"Q")   --menu_str_arr1[11] menu_str_arr2[11] HELP 50  -- "Main Menu" "Close Window and return to main window/menu" Help 50
        EXIT WHILE

      
      ON KEY(F30)
        CALL print_company_html(l_company_id)

      ON KEY(F31)
        CALL grid_export("sa_cont_scroll")
 
     ON ACTION ("actExit") 
      EXIT WHILE



		END INPUT
		
		BEFORE DIALOG
			CALL set_help_id(300)
			CALL publish_toolbar("Company",0)	
		
		ON ACTION actExit
  		EXIT DIALOG

		ON ACTION actLogOut #      COMMAND KEY (F12,"R")   menu_str_arr1[11]  menu_str_arr2[11] HELP 209  -- "Main Menu" "Close Window and return to main menu/window" Help 51
			CALL disable_current_session_record()  
			LET int_flag = TRUE
			EXIT DIALOG
			
	END DIALOG
################################################ DIALOG END #############################################

{

    MENU "Company"
      BEFORE MENU
        CALL set_help_id(101)
        CALL publish_toolbar("Company",0)

        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(150)
        LET menu_str_arr2[1] = get_str(151)
        LET menu_str_arr1[2] = get_str(152)
        LET menu_str_arr2[2] = get_str(153)
        LET menu_str_arr1[3] = get_str(154)
        LET menu_str_arr2[3] = get_str(155)
        LET menu_str_arr1[4] = get_str(156)
        LET menu_str_arr2[4] = get_str(157)
        LET menu_str_arr1[5] = get_str(158)
        LET menu_str_arr2[5] = get_str(159)
        LET menu_str_arr1[6] = get_str(160)
        LET menu_str_arr2[6] = get_str(161)
        LET menu_str_arr1[7] = get_str(162)
        LET menu_str_arr2[7] = get_str(163)
        LET menu_str_arr1[8] = get_str(164)
        LET menu_str_arr2[8] = get_str(165)
        LET menu_str_arr1[9] = get_str(166)
        LET menu_str_arr2[9] = get_str(167)
        LET menu_str_arr1[10] = get_str(168)
        LET menu_str_arr2[10] = get_str(169)
        LET menu_str_arr1[11] = get_str(90)
        LET menu_str_arr2[11] = get_str(91)
  
      COMMAND KEY (F201,"E") menu_str_arr1[1] menu_str_arr2[1] HELP 111  --"Edit" "Edit a company record" Help 111
        HIDE OPTION ALL
        #CALL fgl_settitle("Edit/Modify the current company record")
        CALL fgl_settitle(get_str(151))  --"Edit company details")
        CALL company_edit(l_company_id,FALSE)
        CALL company_show(l_company_id)
        #clean up
        SHOW OPTION ALL
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")

      COMMAND KEY (F202,"D") menu_str_arr1[2] menu_str_arr2[2] HELP 111  --"Delete" "Delete current company record" Help 112
        HIDE OPTION ALL
        CALL fgl_settitle(get_str(153))  --"Delete the currently displayed company record")
        IF NOT exist(l_company_id) THEN
          LET l_company_id = company_query()
        END IF
        IF exist(l_company_id) THEN
          CALL company_show(l_company_id)
          CALL company_delete(l_company_id) RETURNING rv_id

          #The function company_delete() will return NULL if it was aborted, 
          IF rv_id IS NULL THEN
            CALL company_show(l_company_id)
          ELSE
            LET l_company_id = NULL
            -- CLEAR FORM
          EXIT MENU -- cursor state is now invalid
          END IF

          CALL set_comp_default_titlebar()
        END IF
        SHOW OPTION ALL

      COMMAND KEY (F203,"N")  menu_str_arr1[3] menu_str_arr2[3] HELP 113  --"New" "Create a new company record" Help 113
        HIDE OPTION ALL
        CALL fgl_settitle(get_str(155))  --"Create new company record")
        CALL set_comp_default_titlebar()
        LET rv_id = company_create(FALSE)  --False = don't open a new window

        IF rv_id IS NULL THEN
          #NULL means, the new record creation was aborted or failed
          #In this case, we will display the last displayed record
            CALL company_show(l_company_id)  
        ELSE
          LET l_company_id = rv_id
          IF exist(l_company_id) THEN
            CALL company_show(l_company_id)        
            SHOW OPTION ALL
            LET pend_company_id = l_company_id
            EXIT MENU
          END IF
        END IF


        #clean up
        CALL displayKeyGuide("")
        CALL set_comp_default_titlebar()
        SHOW OPTION ALL

      COMMAND KEY (F204,"S")  menu_str_arr1[4] menu_str_arr2[4] HELP 114  --"Quick Sarch" "Quick Search for an existing company" Help 114
        HIDE OPTION ALL
        CALL fgl_settitle(get_str(157))  --"Search for an existing company record")
        LET l_company_id = advanced_company_lookup(l_company_id)
        IF exist(l_company_id) THEN
          LET pend_company_id = l_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT MENU
        SHOW OPTION ALL


      COMMAND KEY (F205,"S")  menu_str_arr1[5] menu_str_arr2[5] HELP 115  -- "Det. Search" "Detailed Search for an existing company" Help 115
        HIDE OPTION ALL
        CALL fgl_settitle(get_str(159))  --"Search for an existing company record")
        LET l_company_id = company_query()
        IF exist(l_company_id) THEN
          LET pend_company_id = l_company_id
        END IF
        #clean up
        CALL set_comp_default_titlebar()
        CALL displayKeyGuide("")
        EXIT MENU
        SHOW OPTION ALL


      COMMAND KEY (F206,"A")  menu_str_arr1[6] menu_str_arr2[6] HELP 116  -- "Assign contact" "Add a contact relationship to this company" Help 116
        HIDE OPTION ALL
        CALL fgl_settitle(get_str(161))  --"Assign main contact to company record")
        IF NOT exist(l_company_id) THEN
          LET l_company_id = company_query()
        END IF

        CALL company_show(l_company_id)

        IF l_company_id IS NOT NULL THEN
          CALL add_contact(l_company_id)
        END IF

        CALL company_show(l_company_id)

        SHOW OPTION ALL
        CALL set_comp_default_titlebar()


      COMMAND KEY (F91,"KEY_HOME")  --First Record
        FETCH FIRST c_comp_scroll 
          INTO l_company_id, tmp_comp_name
        CALL company_show(l_company_id)

      COMMAND KEY (F93,"PREVPAGE",UP) --Previous Record -1
        FETCH PRIOR c_comp_scroll
          INTO l_company_id, tmp_comp_name
        IF sqlca.sqlcode = NOTFOUND THEN
          FETCH FIRST c_comp_scroll
            INTO l_company_id, tmp_comp_name
        END IF
        CALL company_show(l_company_id)

      COMMAND KEY (F94,"NEXTPAGE",DOWN) --Next Record +1
        FETCH NEXT c_comp_scroll 
          INTO l_company_id, tmp_comp_name
        IF sqlca.sqlcode = NOTFOUND THEN
          FETCH LAST c_comp_scroll
            INTO l_company_id, tmp_comp_name
        END IF
        CALL company_show(l_company_id) 

      COMMAND KEY (F96,"KEY_END")   --Last Record
        FETCH LAST c_comp_scroll
          INTO l_company_id, tmp_comp_name
        CALL company_show(l_company_id)

      COMMAND KEY (F11)
        CALL company_contacts_populate_grid_panel(l_company_id,NULL,TRUE,1)  --Activate/Edit grid panel (TRUE)


      #Column Sort short cuts for grid column 
      COMMAND KEY (F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      COMMAND KEY (F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      COMMAND KEY (F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_title"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      COMMAND KEY (F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      COMMAND KEY (F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_lname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())

      COMMAND KEY (F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_phone"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        CALL company_contacts_populate_grid_panel(l_company_id,p_order_field,NULL,get_toggle_switch())


      COMMAND KEY (F12,"Q")   menu_str_arr1[11] menu_str_arr2[11] HELP 50  -- "Main Menu" "Close Window and return to main window/menu" Help 50
        EXIT WHILE
 
     ON ACTION ("actExit") 
      EXIT WHILE
      
      COMMAND KEY(F30)
        CALL print_company_html(l_company_id)

      COMMAND KEY(F31)
        CALL grid_export("sa_cont_scroll")

    END MENU
}



    #toolbar clean up
    CALL publish_toolbar("Company",1)

    CLOSE c_comp_scroll

    IF int_flag THEN
      LET int_flag = FALSE
      EXIT WHILE
    END IF
  END WHILE

  CLOSE WINDOW w_company_main

  RETURN l_company_id
END FUNCTION





###################################################
# FUNCTION add_contact(l_company_id)
#
# assign contact to company
# NO RETURN
###################################################
FUNCTION add_contact(l_company_id)
  DEFINE 
    l_company_id LIKE company.comp_id,
    l_contact_rec RECORD LIKE contact.*,
    main_cont LIKE company.comp_main_cont,
    contact_name1 LIKE contact.cont_name,
    contact_exist INTEGER,
    local_debug SMALLINT

  LET local_debug = 0  --for debugging 0=off 1=on
  
  ---cant seem to get it to enter data without leaving form
  #DISPLAY "Leave this window to save the data     " AT 2,1 ATTRIBUTE(YELLOW)

  SELECT comp_main_cont
    INTO main_cont
    FROM company
    WHERE company.comp_id = l_company_id

  IF local_debug = 1 THEN
    DISPLAY "main_cont=",main_cont
  END IF

  IF main_cont = 0 THEN   ---if a contact does not yet exist
        ---if there is no contact, then allow input of contact and update
    INPUT l_contact_rec.cont_name FROM cont_name HELP 116
      ON KEY(F10)
        LET l_contact_rec.cont_name = get_contact_name(contact_popup(NULL,NULL,0))
        DISPLAY BY NAME l_contact_rec.cont_name

    END INPUT

    CALL get_contact_id(l_contact_rec.cont_name) 
      RETURNING contact_name1

        ---need to check that the contact is valid
    SELECT COUNT(*)
      INTO contact_exist
      FROM contact
      WHERE contact.cont_name = l_contact_rec.cont_name

    IF contact_exist > 0 THEN  ---if =0, then doesnt exist
      UPDATE company 
        SET company.comp_main_cont = contact_name1
        WHERE company.comp_id = l_company_id
          
      DISPLAY "                                           " AT 2,1
      CALL fgl_message_box(get_str(164))  --"Contact linked with this company record")
      CALL company_show(l_company_id)
    ELSE  ---if not valid, tell operator
      CALL fgl_message_box(get_str(165))  --"Invalid contact - Contact does not exist")
    END IF

  ELSE   ---if we get here, then the company already has a contact assigned
    CALL fgl_message_box(get_str(166))  --"Company already has a contact") 
  END IF

END FUNCTION







###############################################################################################################
# Company List / Combo / Mangement functions
###############################################################################################################


###################################################
# FUNCTION company_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (Cursor) for company_popup()
#
# RETURN NONE
###################################################
FUNCTION company_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "comp_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT " , 
                   "company.comp_id, ",
                   "company.comp_name, ",
                   "company.comp_city, ",
                   "company.comp_country, ",
                   "contact.cont_name, ",
                   "operator.name ",
                 "FROM company, OUTER contact, OUTER operator ",
                 "WHERE company.acct_mgr = operator.cont_id ",
                   "AND company.comp_main_cont = contact.cont_id "


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED

  END IF

  PREPARE p_company FROM sql_stmt
  DECLARE c_company CURSOR FOR p_company

END FUNCTION


###################################################
# FUNCTION company_popup(p_comp_id,p_order_field,p_accept_action)
#
# Displays popup window and allows operator to select a company
# RETURN l_company[i].comp_id  or NULL
#
# If no value is selected, the original calling argument will be returned p_comp_id
###################################################
FUNCTION company_popup(p_comp_id,p_order_field,p_accept_action)
  DEFINE 
    p_comp_id                       LIKE company.comp_id,   --parameter which will be returned if nothing is selected
    l_comp_arr                      DYNAMIC ARRAY OF t_comp_arr,
    i                               INTEGER,
    p_order_field,p_order_field2    VARCHAR(128), 
    local_debug                     SMALLINT,
    p_accept_action                 SMALLINT,
    err_msg                         VARCHAR(240),
    l_ord_dir                       SMALLINT,
    l_scroll_comp_id                LIKE company.comp_id ,
    l_scroll_row                    SMALLINT ,
    l_arr_size                      SMALLINT,
    l_comp_id                       LIKE company.comp_id

  LET local_debug = 0  ---0=off   1=on
  CALL toggle_switch_on()  --default sort order ASC
  LET l_arr_size = 100 -- due to dynamic array ...sizeof(l_comp_arr)

 
  #Auto Grid scroll
  IF p_comp_id IS NOT NULL THEN
    LET l_scroll_comp_id = p_comp_id
  ELSE
    LET l_scroll_comp_id = 1
  END IF
  LET l_scroll_row = 1


  IF NOT p_order_field THEN
    LET p_order_field = "comp_id"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 0  --default is simple id return
  END IF


    CALL fgl_window_open("w_comp_scroll", 3,3, get_form_path("f_company_scroll_l2"), FALSE )
    CALL populate_company_list_form_labels_g()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL company_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_company INTO l_comp_arr[i].*
      #Auto Grid scroll
      IF l_comp_arr[i].comp_id = l_scroll_comp_id THEN
        LET l_scroll_row = i
      END IF
      IF i > l_arr_size THEN
        CALL fgl_winmessage("Error","Too many company records for the array\nOnly displaying the first " || l_arr_size, "error")
        EXIT FOREACH
      END IF
      LET i = i + 1
    END FOREACH

    LET i = i - 1

		IF i > 0 THEN
			CALL l_comp_arr.resize(i)   --correct the last element of the dynamic array
		END IF
		
    #CALL set_count(i)
    LET int_flag = FALSE


    DISPLAY ARRAY l_comp_arr TO sa_comp.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")

      BEFORE DISPLAY
        #The current row remembers the list position and scrolls the cursor automatically to the last line number in the display array
        IF l_scroll_row IS NOT NULL THEN
          CALL fgl_dialog_setcurrline(5,l_scroll_row)
        END IF

      BEFORE ROW
        LET i = arr_curr()
        LET l_comp_id = l_comp_arr[i].comp_id
        LET l_scroll_comp_id = l_comp_id   

      ON KEY(INTERRUPT)  --on cancel, we return the calling argument
        EXIT WHILE

      ON KEY(ACCEPT)  --ON OK events can be customized

        CASE p_accept_action

          WHEN 0
            EXIT WHILE

        
          WHEN 1  --view
            CALL company_view(get_activity_type_rec(l_comp_id))
            CALL fgl_window_current("w_comp_scroll")
            EXIT DISPLAY

          WHEN 2  --edit
            CALL company_edit(l_comp_id,TRUE)  --TRUE = with own window
            CALL fgl_window_current("w_comp_scroll")
            EXIT DISPLAY

          WHEN 3  --delete
            CALL company_delete(l_comp_id)
            CALL fgl_window_current("w_comp_scroll")
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL company_create(TRUE)  --true is, open a new window
            CALL fgl_window_current("w_comp_scroll")
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_window_current("w_comp_scroll")
            CALL fgl_winmessage("Not implemented","company_popup()\Company Print is not implemented","info")

            OTHERWISE
              LET err_msg = "company_popup(p_comp_id,p_order_field, p_accept_action = " ,p_accept_action
              CALL fgl_winmessage("company_popup() - 4GL Source Error",err_msg, "error") 
          END CASE

      ON KEY (F4) -- add 
        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL company_create(TRUE)  --true is , open a new window
        CALL fgl_window_current("w_comp_scroll")
        EXIT DISPLAY


      ON KEY (F5) -- edit
        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL company_edit(l_comp_id,TRUE)  --TRUE = with own window
        CALL fgl_window_current("w_comp_scroll")
        EXIT DISPLAY

      ON KEY (F6) -- delete
        CALL company_delete(l_comp_id)
        CALL fgl_window_current("w_comp_scroll")
        EXIT DISPLAY

      ON KEY (F7) -- View
        IF NOT fgl_fglgui() THEN
          CALL fgl_window_current("SCREEN")
        END IF

        CALL company_view_short_by_id(l_comp_id)   --company_view(l_comp_arr[i].comp_id)
        CALL fgl_window_current("w_comp_scroll")
        EXIT DISPLAY



      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_city"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "comp_country"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

  END WHILE

  CALL fgl_window_close("w_comp_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    #RETURN NULL
    RETURN p_comp_id
  ELSE 
    LET i = arr_curr()
    RETURN l_comp_arr[i].comp_id
  END IF

END FUNCTION



##########################################################################################################
# DATA Record Copy functions
##########################################################################################################

###################################################
# FUNCTION company_to_form_company(p_company)
#
# Copy data from the full company record to the shorter vers of the form record
#
# RETURN l_company_rec.*
###################################################
FUNCTION company_to_form_company(p_company)
  DEFINE p_company RECORD LIKE company.*
  DEFINE l_company_rec OF t_company_rec

  LET l_company_rec.ctype_name = get_company_type_name(p_company.comp_type)
  LET l_company_rec.comp_name = p_company.comp_name
  LET l_company_rec.comp_addr1 = p_company.comp_addr1
  LET l_company_rec.comp_addr2 = p_company.comp_addr2
  LET l_company_rec.comp_addr3 = p_company.comp_addr3
  LET l_company_rec.comp_city = p_company.comp_city
  LET l_company_rec.comp_zone = p_company.comp_zone
  LET l_company_rec.comp_zip = p_company.comp_zip
  LET l_company_rec.comp_country = p_company.comp_country
  LET l_company_rec.name = get_operator_name(p_company.acct_mgr)
  LET l_company_rec.itype_name = get_industry_type(p_company.comp_industry)
  LET l_company_rec.comp_url = p_company.comp_url
  LET l_company_rec.cont_name = get_contact_name(p_company.comp_main_cont)
  LET l_company_rec.comp_priority = p_company.comp_priority
  LET l_company_rec.comp_notes = p_company.comp_notes

  RETURN l_company_rec.*
END FUNCTION




##########################################################################################################
# INPUT / Edit,Create/View Record functions
##########################################################################################################


###################################################
# FUNCTION company_create(p_window)
#
# Create new company - p_window is if a new window should be opened
#
# RETURN rv  --Integer 
###################################################
FUNCTION company_create(p_window)
  DEFINE 
    l_company     RECORD LIKE company.*,
    rv            INTEGER,
    p_window      SMALLINT                  --specifies, if a window with form should be opened


 
  LET l_company.comp_id = 0
  CLEAR comp_id


  IF p_window THEN

		CALL fgl_window_open("w_company_main2",1,1, get_form_path("f_company_det_l2"), FALSE)
		CALL populate_company_edit_form_labels_g()

  ELSE
    CLEAR FORM
    #DISPLAY "image/cms_title_image.jpg" TO image -- agb 
    CALL populate_company_edit_form_labels_g()

  END IF

  CALL company_input(l_company.*)
    RETURNING l_company.*

  IF p_window THEN
    CALL fgl_window_close("w_company_main2")
  END IF

  IF int_flag THEN
    CALL fgl_winmessage(get_str(183),get_str(181),"info")
    LET rv = NULL

  ELSE
    INSERT INTO company VALUES (l_company.*)

    IF sqlca.sqlcode THEN
      CALL fgl_winmessage(get_str(183),get_str(185),"stop")
      LET rv = NULL
    END IF
 
    LET rv = sqlca.sqlerrd[2]
    LET l_company.comp_id = rv
    LET tmp_str = get_str(186), " ", l_company.comp_id
    CALL fgl_winmessage(get_str(184),tmp_str,"info")
  END IF

  RETURN rv  --Integer
END FUNCTION


###################################################
# FUNCTION company_view(p_company_id)
#
# View company details in a window
#
# RETURN NONE
###################################################
FUNCTION company_view(p_company_id)
  DEFINE
    p_company_id   LIKE company.comp_id,
    inp_char       CHAR


	CALL fgl_window_open("w_company_view", 1,1, get_form_path("f_company_det_l2"), FALSE)
	CALL populate_company_form_labels_g()

  CALL company_show(p_company_id)

  WHILE TRUE

    PROMPT "" FOR CHAR inp_char HELP 151
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT

  ENd WHILE

  CALL fgl_window_close("w_company_view")
END FUNCTION


###################################################
# FUNCTION company_edit(p_company_id,p_window)
#
# Edit a company record - uses company_input() p_window true is if it should open a window
# RETURN NONE
###################################################
FUNCTION company_edit(p_company_id,p_window)
  DEFINE 
    p_company_id  LIKE company.comp_id,
    l_company     RECORD LIKE company.*,
    local_debug   SMALLINT,
    p_window      SMALLINT                  --specifies, if a window with form should be opened

  LET local_debug = FALSE  --0=off 1=on

  CALL get_company_rec(p_company_id)
    RETURNING l_company.*


  IF p_window THEN

		CALL fgl_window_open("w_company_main2",1,1, get_form_path("f_company_det_l2"), FALSE)
		CALL populate_company_edit_form_labels_g()
  ELSE
		CALL populate_company_edit_form_labels_g()
  END IF


  CALL company_input(l_company.*)
    RETURNING l_company.*


  IF p_window THEN
    CALL fgl_window_close("w_company_main2")
  END IF

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN
  END IF

  UPDATE company SET
    company.comp_name = l_company.comp_name,
    company.comp_addr1 = l_company.comp_addr1,
    company.comp_addr2 = l_company.comp_addr2,
    company.comp_addr3 = l_company.comp_addr3,
    company.comp_city = l_company.comp_city,
    company.comp_zone = l_company.comp_zone,
    company.comp_zip = l_company.comp_zip,
    company.comp_country = l_company.comp_country,
    company.acct_mgr = l_company.acct_mgr,
    company.comp_link = l_company.comp_link,
    company.comp_industry = l_company.comp_industry,
    company.comp_priority = l_company.comp_priority,
    company.comp_type = l_company.comp_type,
    company.comp_main_cont = l_company.comp_main_cont,
    company.comp_url = l_company.comp_url,
    company.comp_notes = l_company.comp_notes
    WHERE  company.comp_id = p_company_id

  IF local_debug THEN
    DISPLAY "company_edit() company.comp_name=", l_company.comp_name
    DISPLAY "company_edit() company.comp_addr1=", l_company.comp_addr1
    DISPLAY "company_edit() company.comp_addr2=", l_company.comp_addr2
    DISPLAY "company_edit() company.comp_addr3=", l_company.comp_addr3
    DISPLAY "company_edit() company.comp_city=", l_company.comp_city
    DISPLAY "company_edit() company.comp_zone=", l_company.comp_zone
    DISPLAY "company_edit() company.comp_zip=", l_company.comp_zip
    DISPLAY "company_edit() company.comp_country=", l_company.comp_country
    DISPLAY "company_edit() company.acct_mgr=", l_company.acct_mgr
    DISPLAY "company_edit() company.comp_link=", l_company.comp_link
    DISPLAY "company_edit() company.comp_industry=", l_company.comp_industry
    DISPLAY "company_edit() company.comp_priority=", l_company.comp_priority
    DISPLAY "company_edit() company.comp_type=", l_company.comp_type
    DISPLAY "company_edit() company.comp_main_cont=", l_company.comp_main_cont
    DISPLAY "company_edit() company.comp_url=", l_company.comp_url
    DISPLAY "company_edit() company.comp_notes=", l_company.comp_notes
  END IF
END FUNCTION


###################################################
# FUNCTION company_delete(p_company_id)
#
# Delete a company record
# RETURN NULL or ??? can this be right ?
###################################################
FUNCTION company_delete(p_company_id)
  DEFINE p_company_id LIKE company.comp_id
  DEFINE l_contact_id LIKE contact.cont_id
  DEFINE i INTEGER
  DEFINE msg CHAR(200)
 
  IF yes_no(get_str(152),get_str(187)) THEN
    SELECT COUNT(*)  ---only continue after operator confirmation 
      INTO i 
      FROM contact
      WHERE contact.cont_org = p_company_id
    IF i THEN
      LET msg = i, get_str(189)  --contacts are associated with this company. Cascade the delete?
      IF question_box(msg CLIPPED) THEN
        DECLARE c_cont_delete CURSOR FOR
         SELECT cont_id 
           INTO l_contact_id 
           FROM contact
          WHERE contact.cont_org = p_company_id
      ---use contact function to allow for additional cleanup
        FOREACH c_cont_delete
          CALL contact_delete(l_contact_id)
        END FOREACH
      END IF
    END IF
    DELETE FROM company WHERE company.comp_id = p_company_id
  ELSE 
    CALL fgl_winmessage(get_str(152),get_str(188),"info")
  END IF

 IF int_flag THEN
     LET int_flag = FALSE
     RETURN NULL

  END IF

  RETURN NULL

END FUNCTION



###################################################
# FUNCTION company_input(p_company)
#
# Input contact details
# RETURN p_company.*
###################################################
FUNCTION company_input(p_company)
  DEFINE 
    p_company RECORD LIKE company.*,
    l_company_rec OF t_company_rec

 DEFINE 
    x INTEGER,
    l_rec 
      RECORD 
        cont_phone  LIKE contact.cont_phone,
        cont_email  LIKE contact.cont_email
      END RECORD,
    local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

	LET tmp_str = get_str(800) CLIPPED, " - ", get_str(815) CLIPPED , "    ", get_str(808) CLIPPED, " - ", get_str(820)
	CALL displayKeyGuide(tmp_str)

  CALL company_to_form_company(p_company.*)
    RETURNING l_company_rec.*



  ##################################
  # INPUT
  ##################################

  OPTIONS FIELD ORDER CONSTRAINED

	#DIALOG
	
  INPUT BY NAME l_company_rec.* WITHOUT DEFAULTS  HELP 151		

    BEFORE INPUT
			IF p_company.comp_id = 0 THEN -- new record
      	CALL publish_toolbar("CompanyNew",0)
			ELSE  -- edit existing record
			  CALL publish_toolbar("CompanyEdit",0)
			END IF

      CALL populate_company_form_combo_boxes()  --populate all combo boxes in this form dynamically

    ON KEY (F9)
      IF INFIELD(comp_country) THEN  --country
        LET l_company_rec.comp_country = country_advanced_lookup(l_company_rec.comp_country)  --country_popup(l_company_rec.comp_country)
        DISPLAY BY NAME l_company_rec.comp_country
        IF local_debug THEN
          DISPLAY "F9-company_input() l_company_rec.comp_country=", l_company_rec.comp_country
        END IF
      END IF

      IF INFIELD (cont_name) THEN  --main contact
        LET p_company.comp_main_cont = advanced_contact_lookup(p_company.comp_main_cont)
        LET l_company_rec.cont_name = get_contact_name(p_company.comp_main_cont)
        DISPLAY BY NAME l_company_rec.cont_name
        IF local_debug  THEN
          DISPLAY "F9-company_input() p_company.comp_main_cont =", p_company.comp_main_cont 
          DISPLAY "F9-company_input() l_company_rec.cont_name=", l_company_rec.cont_name
        END IF
      END IF


    ON KEY (F10)
      #country
      IF INFIELD(comp_country) THEN  --country
        LET l_company_rec.comp_country = country_popup(l_company_rec.comp_country,NULL,0)
        DISPLAY BY NAME l_company_rec.comp_country
        IF local_debug THEN
          DISPLAY "F10-company_input() l_company_rec.comp_country=", l_company_rec.comp_country
        END IF

      END IF

      #company type
      IF INFIELD(ctype_name) THEN  --company type (relation)
        LET p_company.comp_type = company_type_popup(p_company.comp_type,NULL,0)
        LET l_company_rec.ctype_name = get_company_type_name(p_company.comp_type)
        DISPLAY BY NAME l_company_rec.ctype_name
        IF local_debug  THEN
          DISPLAY "F10-company_input() p_company.comp_type=", p_company.comp_type
          DISPLAY "F10-company_input() l_company_rec.ctype_name=", l_company_rec.ctype_name
        END IF

      END IF

      #Industry Type
      IF INFIELD(itype_name) THEN  --industry type
        LET p_company.comp_industry = industry_type_popup(get_industry_type_id(p_company.comp_industry),NULL,0)   --type_id
        LET l_company_rec.itype_name = get_industry_type_name(p_company.comp_industry)
        DISPLAY BY NAME l_company_rec.itype_name
        IF local_debug THEN
          DISPLAY "F10-company_input() p_company.comp_industry =", p_company.comp_industry 
          DISPLAY "F10-company_input() l_company_rec.itype_name=", l_company_rec.itype_name
        END IF
      END IF

      #Main contact
      IF INFIELD (cont_name) THEN  --main contact
        LET p_company.comp_main_cont = contact_popup(p_company.comp_main_cont,NULL,0)
        LET l_company_rec.cont_name = get_contact_name(p_company.comp_main_cont)
        CURRENT WINDOW IS w_company_main --has to set focus to this window in case child functions change windows focus
        DISPLAY BY NAME l_company_rec.cont_name
        IF local_debug THEN
          DISPLAY "F10-company_input() p_company.comp_main_cont =", p_company.comp_main_cont 
          DISPLAY "F10-company_input() l_company_rec.cont_name=", l_company_rec.cont_name
        END IF
      END IF

      #Account Manager
      IF INFIELD (name) THEN  -- Account manager...
        LET p_company.acct_mgr = operator_popup(p_company.acct_mgr,NULL,0)
        LET l_company_rec.name = get_operator_name(p_company.acct_mgr)
        DISPLAY BY NAME l_company_rec.name
        IF local_debug  THEN
          DISPLAY "F10-company_input() p_company.acct_mgr =", p_company.acct_mgr
          DISPLAY "F10-company_input() l_company_rec.name=", l_company_rec.name
        END IF
      END IF

      #Always re-populate the combo list boxes in case, the user created a new entry
      CALL populate_company_form_combo_boxes()  --populate all combo boxes in this form dynamically


    ON KEY (F11)
     CALL company_contacts_populate_grid_panel(get_company_id(l_company_rec.comp_name),NULL,TRUE,1)  --Activate/Edit grid panel (TRUE)


    #Print Html
    ON KEY(F30)
      CALL print_company_html(p_company.comp_id)


    ##################################
    # AFTER FIELD section
    ##################################

    #company name
    AFTER FIELD comp_name
      IF company_name_count(l_company_rec.comp_name) THEN
        IF (p_company.comp_name = l_company_rec.comp_name) THEN
          #do nothing = input is ok
        ELSE
          DISPLAY l_company_rec.comp_name TO comp_name ATTRIBUTE(RED)
          ERROR get_str(176) CLIPPED, " ", l_company_rec.comp_name CLIPPED, " " , get_str(177)
          #ERROR "You must specify an unique company name. ", l_company_rec.comp_name CLIPPED, " already exists!"

          NEXT FIELD comp_name
        END IF
      ELSE
        DISPLAY l_company_rec.comp_name TO comp_name 
      END IF

    #industry type    
    AFTER FIELD itype_name
      LET p_company.comp_industry = get_industry_type_id(l_company_rec.itype_name)

    #company type
    AFTER FIELD ctype_name
      LET p_company.comp_type = get_company_type_id(l_company_rec.ctype_name)


    ##################################
    # AFTER INPUT BLOCK
    ##################################

    AFTER INPUT
      #operator wants to store data - check if required fields are filled with data
      IF int_flag = FALSE THEN
        IF l_company_rec.comp_name IS NULL THEN
          CALL fgl_winmessage(get_str(178),get_str(179),"ERROR")
          #CALL fgl_winmessage("Error Company Name IS NULL","You must specify a value for the company Name","error")
          LET tmp_str = "<", get_str(180), ">"
          DISPLAY tmp_str TO comp_name ATTRIBUTE (RED,BOLD)
          SLEEP 2
          DISPLAY "" TO comp_name
          NEXT FIELD comp_name
          CONTINUE INPUT
        END IF
      ELSE
        #IF NOT yes_no("Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?") THEN
        IF NOT yes_no(get_str(181),get_str(182)) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        END IF
      END IF

    --possible toolbar clean up
    CALL publish_toolbar("CompanyEdit",1)


  END INPUT
	#END DIALOG
	
  OPTIONS FIELD ORDER UNCONSTRAINED

  LET p_company.comp_name = l_company_rec.comp_name
  LET p_company.comp_addr1 = l_company_rec.comp_addr1
  LET p_company.comp_addr2 = l_company_rec.comp_addr2
  LET p_company.comp_addr3 = l_company_rec.comp_addr3
  LET p_company.comp_city = l_company_rec.comp_city
  LET p_company.comp_zone = l_company_rec.comp_zone
  LET p_company.comp_zip = l_company_rec.comp_zip
  LET p_company.comp_country = l_company_rec.comp_country
  LET p_company.comp_priority = l_company_rec.comp_priority
  LET p_company.comp_notes = l_company_rec.comp_notes
  #LET p_company.comp_industry = l_company_rec.comp_notes



  IF local_debug THEN
    DISPLAY "company_input() p_company.comp_name=", p_company.comp_name
    DISPLAY "company_input() p_company.comp_addr1=", p_company.comp_addr1
    DISPLAY "company_input() p_company.comp_addr2=", p_company.comp_addr2
    DISPLAY "company_input() p_company.comp_addr3=", p_company.comp_addr3
    DISPLAY "company_input() p_company.comp_city=", p_company.comp_city
    DISPLAY "company_input() p_company.comp_zone=", p_company.comp_zone
    DISPLAY "company_input() p_company.comp_zip=", p_company.comp_zip
    DISPLAY "company_input() p_company.comp_country=", p_company.comp_country
    DISPLAY "company_input() p_company.comp_priority=", p_company.comp_priority
    DISPLAY "company_input() p_company.comp_notes=", p_company.comp_notes
    DISPLAY "company_input() p_company.comp_industry=", p_company.comp_industry
    DISPLAY "company_input() p_company.comp_type=", p_company.comp_type
    DISPLAY "company_input() p_company.comp_main_cont=", p_company.comp_main_cont
    DISPLAY "company_input() p_company.acct_mgr=", p_company.acct_mgr

  END IF

  IF p_company.comp_main_cont IS NOT NULL THEN 
    LET p_company.comp_main_cont = get_contact_id(l_company_rec.cont_name)
  ELSE 
    LET p_company.comp_main_cont = 0
  END IF

  LET p_company.comp_url = l_company_rec.comp_url

  RETURN p_company.*
END FUNCTION


##########################################################################################################
# Company Related Functions (Search etc.)
##########################################################################################################


##############################################################################################################
# Lookups
##############################################################################################################


####################################################
# FUNCTION  advanced_company_lookup(p_comp_id)
#
# Allows to search by entering the first letters
#
# RETURN rv_comp_id
####################################################
FUNCTION advanced_company_lookup(p_comp_id)
  DEFINE f_filter VARCHAR(255)
  DEFINE l_filter VARCHAR(255)
  DEFINE l_key SMALLINT
  DEFINE len INTEGER
  DEFINE do_choose SMALLINT
  DEFINE rv_comp_id,p_comp_id LIKE company.comp_id

  # f_filter is what we display to the lookup field
  # l_filter is what we use in the query (ie, f_filter, "%")

  LET rv_comp_id = 0

  LET l_filter = "%"
  LET f_filter = ""
  LET do_choose = FALSE

	CALL fgl_window_open("w_advanced_lookup_company", 2, 2 , get_form_path("f_company_scroll_advanced_l2"), FALSE)

	CALL grid_header_company_scroll_advanced()
	CALL fgl_settitle(get_str(140))  --Lookup Company (Quick)
	CALL populate_company_quick_search_form_labels_g()

  #Toolbar
  CALL publish_toolbar("ContactSearchQuickGlobal",0)

  WHILE TRUE
    CALL update_advanced_company_lookup(l_filter, f_filter, do_choose)
      RETURNING rv_comp_id

    IF rv_comp_id > 0 THEN
      EXIT WHILE
    END IF

    LET do_choose = FALSE

    CALL fgl_getkey()
      RETURNING l_key

    CASE 
      WHEN l_key = fgl_keyval("HELP")
        CALL showhelp(114)

      WHEN l_key = fgl_keyval("CANCEL")
        EXIT WHILE

      WHEN l_key = fgl_keyval("RETURN")
        LET do_choose = TRUE

      WHEN l_key = fgl_keyval("ACCEPT")
        LET do_choose = TRUE

      WHEN l_key = fgl_keyval("backspace")
        LET len = LENGTH(f_filter) - 1
        IF len >= 1 THEN
          LET f_filter = f_filter[1,len]
          LET l_filter = f_filter, "%"
        ELSE 
          LET f_filter = ""
          LET l_filter = f_filter, "%"
        END IF

      WHEN (l_key >= ORD("0") AND l_key <= ORD("9"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("a") AND l_key <= ORD("z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("A") AND l_key <= ORD("Z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

    END CASE
  END WHILE

  #Toolbar
  CALL publish_toolbar("ContactSearchQuickGlobal",1)


  CALL fgl_window_close("w_advanced_lookup_company")

  #return found id - if cancel, return calling parameter
  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_comp_id
  ELSE
    RETURN rv_comp_id
  END IF

END FUNCTION


####################################################
# FUNCTION  update_advanced_company_lookup(query_filter, field_filter, do_choose)
#
# sub-routine of advanced_company_lookup to update display
#
# RETURN rv_comp_id  (LIKE contact.cont_id)
####################################################
FUNCTION update_advanced_company_lookup(query_filter, field_filter, do_choose)
  DEFINE 
    query_filter VARCHAR(255), 
    field_filter VARCHAR(255),   
    i            INTEGER,
    l_comp_short_arr DYNAMIC ARRAY OF t_comp_short_arr,
    do_choose    SMALLINT,
    rv_comp_id   LIKE company.comp_id,
    local_debug  SMALLINT

  LET local_debug = FALSE  --0=off 1=on
  LET rv_comp_id = 0

  DECLARE c_test_lookup CURSOR FOR 
    SELECT company.comp_id, company.comp_name , company.comp_city
      FROM company
      WHERE UPPER(company.comp_name) LIKE UPPER(query_filter)
      ORDER BY company.comp_name ASC


  DISPLAY field_filter TO f_search

  LET i = 0

  FOREACH c_test_lookup INTO l_comp_short_arr[i + 1].*
    LET i = i + 1
  END FOREACH

  #CALL set_count(i)
	IF i > 0 THEN
		CALL l_comp_short_arr.resize(i)   --correct the last element of the dynamic array
	ELSE
		CALL l_comp_short_arr.clear() 
	END IF

	
  
  LET int_flag = FALSE

  IF do_choose THEN

    DISPLAY ARRAY l_comp_short_arr TO advanced_lookup_arr.*  
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 1

      BEFORE DISPLAY 
        CALL publish_toolbar("CompanySearchQuickList",0)

    END DISPLAY


    IF NOT int_flag THEN
      LET i = arr_curr()
      LET rv_comp_id = l_comp_short_arr[i].comp_id
    END IF
  ELSE
    DISPLAY ARRAY l_comp_short_arr TO advanced_lookup_arr.* WITHOUT SCROLL
  END IF

  IF local_debug THEN
    DISPLAY "update_advanced_company_lookup() query_filter= ", query_filter
    DISPLAY "update_advanced_company_lookup() field_filter= ", field_filter
    DISPLAY "update_advanced_company_lookup() do_choose= ", do_choose
    DISPLAY "update_advanced_company_lookup() rv_comp_id= ",rv_comp_id
  END IF

  RETURN rv_comp_id
END FUNCTION


###################################################
# FUNCTION company_show(p_company_id)
#
# Display company details to form (fields by name)
# RETURN NOTHING
###################################################
FUNCTION company_show(p_company_id)
  DEFINE 
    p_company_id  LIKE company.comp_id,
    l_company_rec OF t_company_rec

  DEFINE l_cont_rec RECORD
    cont_name		LIKE contact.cont_name,
    cont_phone		LIKE contact.cont_phone,
    cont_email		LIKE contact.cont_email
  END RECORD 

  DEFINE l_acct_mgr_rec	RECORD 
    cont_name		LIKE contact.cont_name
  END RECORD

  IF p_company_id IS NULL THEN
    RETURN
  END IF

  CALL get_form_company(p_company_id)
    RETURNING l_company_rec.*

	CALL updateCompanyGoogleMap(l_company_rec.*)

  DISPLAY p_company_id TO comp_id
  DISPLAY BY NAME l_company_rec.*
  CALL company_contacts_populate_grid_panel(p_company_id,NULL,NULL,1)


END FUNCTION


FUNCTION updateCompanyGoogleMap(l_company_rec)
	DEFINE l_company_rec OF t_company_rec
	DEFINE mapQuery STRING
	 
	LET mapQuery = trim(l_company_rec.comp_addr1), " , ", trim(l_company_rec.comp_addr2), " , ", trim(l_company_rec.comp_addr3), " , ",trim(l_company_rec.comp_city), " , ",trim(l_company_rec.comp_zone), " , ", trim(l_company_rec.comp_zip), " , ",trim(l_company_rec.comp_country) 
	#CALL fgl_winmessage("map",mapQuery,"info")
	DISPLAY mapQuery TO wc_google_map
		
END FUNCTION

###################################################
# FUNCTION company_choose()
#
# ?????????????????????????
# RETURN l_company_id
###################################################
# should move the display array/SQL from company_query into here
FUNCTION company_choose()
  DEFINE l_company_id LIKE company.comp_id
  RETURN l_company_id
END FUNCTION


###################################################
# FUNCTION company_display(p_company_id)
#
# Display a company record by ID
#
# RETURN NONE
###################################################
FUNCTION company_display(p_company_id)
  DEFINE p_company_id LIKE company.comp_id
  DEFINE l_company RECORD LIKE company.*
  DEFINE i INTEGER,
    inp_char CHAR

  OPEN WINDOW w_comp_disp 
    AT 5,5 
    WITH FORM "form/f_company_l2" 
    ATTRIBUTE(BORDER)

  CALL company_show(p_company_id)

  MESSAGE get_str(51)

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    ENd PROMPT
  END WHILE

  CLOSE WINDOW w_comp_disp
END FUNCTION





##############################################################################################################
# Company CONTACTS GRID 
##############################################################################################################


####################################################
# FUNCTION company_contacts_populate_grid_panel_data_source(p_company_id,p_order_field)
#
# Data Source for company contacts grid population
#
# RETURN NONE
####################################################
FUNCTION company_contacts_populate_grid_panel_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "cont_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  LET sql_stmt = "SELECT ",
                   "contact.cont_id, ",
                   "contact.cont_name, ",
                   "contact.cont_title, ",
                   "contact.cont_fname, ",
                   "contact.cont_lname, ",
                   "contact.cont_phone ",
                 "FROM contact ",
                 "WHERE contact.cont_org = ? "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF


  IF local_debug THEN
    DISPLAY sql_stmt[1,50]
    DISPLAY sql_stmt[51,100]
    DISPLAY sql_stmt[101,150]
    DISPLAY sql_stmt[151,200]
  END IF

  PREPARE p_cont FROM sql_stmt
  DECLARE c_cont CURSOR FOR p_cont


END FUNCTION



####################################################
# FUNCTION company_contacts_populate_grid_panel(p_company_id,p_order_field,p_scroll,p_ord_dir)
#
# displays a list of all contacts associated with a company
#
# RETURN NONE
####################################################
FUNCTION company_contacts_populate_grid_panel(p_company_id,p_order_field,p_scroll,p_ord_dir)
  DEFINE 
    p_order_field,p_order_field2  VARCHAR(128),
    p_company_id                  LIKE company.comp_id,
    p_scroll                      SMALLINT,  --Display Array with and without scroll
    l_cont_arr                    DYNAMIC ARRAY OF t_cont_rec1,
    sql_stmt                      CHAR(2048),
    local_debug                   SMALLINT,
    i                             INTEGER,
    action_edit                   SMALLINT,
    p_ord_dir                     SMALLINT

  LET local_debug = FALSE  ---0=off   1=on
  LET action_edit = FALSE

  IF local_debug THEN
    DISPLAY "1-company_contacts_populate_grid_panel() - p_company_id=",p_company_id 
    DISPLAY "1-company_contacts_populate_grid_panel() - p_order_field=",p_order_field 
    DISPLAY "1-company_contacts_populate_grid_panel() - p_scroll=",p_scroll 
    DISPLAY "1-company_contacts_populate_grid_panel() - p_ord_dir=",p_ord_dir 
    DISPLAY "1-company_contacts_populate_grid_panel() - get_toggle_switch()=",get_toggle_switch() 
  END IF

  IF p_ord_dir IS NULL THEN
    CALL toggle_switch_on()  --default sort order ASC
    IF local_debug THEN
      DISPLAY "2a-company_contacts_populate_grid_panel() - p_ord_dir IS NULL = ", p_ord_dir
      DISPLAY "2a-company_contacts_populate_grid_panel() - toggle_switch =", get_toggle_switch()
    END IF
  ELSE
    CALL set_toggle_switch(p_ord_dir)
    IF local_debug THEN
      DISPLAY "2b-company_contacts_populate_grid_panel() - p_ord_dir IS NOT NULL = ", p_ord_dir
      DISPLAY "2b-company_contacts_populate_grid_panel() - toggle_switch =", get_toggle_switch()
    END IF
  END IF

  IF p_order_field IS NULL THEN
    LET p_order_field = "cont_id"
  END IF

  IF local_debug THEN
    DISPLAY "3-company_contacts_populate_grid_panel() - p_company_id=",p_company_id 
    DISPLAY "3-company_contacts_populate_grid_panel() - p_order_field=",p_order_field 
    DISPLAY "3-company_contacts_populate_grid_panel() - p_scroll=",p_scroll 
    DISPLAY "3-company_contacts_populate_grid_panel() - p_ord_dir=",p_ord_dir 
    DISPLAY "3-company_contacts_populate_grid_panel() - get_toggle_switch()=",get_toggle_switch() 

  END IF


  WHILE TRUE

    CALL company_contacts_populate_grid_panel_data_source(p_order_field,get_toggle_switch())

  LET i = 1

  FOREACH c_cont USING p_company_id INTO l_cont_arr[i].*
    LET i = i + 1
    IF i > 50 THEN
      EXIT FOREACH
    END IF
  END FOREACH
  LET i = i - 1

  #CALL set_count(i)
	IF i > 0 THEN
		CALL l_cont_arr.resize(i)   --correct the last element of the dynamic array
	END IF
	

  IF p_scroll THEN  -- function parameter  -- EDIT - INPUT FOCUS on GRID

    DISPLAY ARRAY l_cont_arr TO sa_cont_scroll.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL set_help_id(101)
        CALL publish_toolbar("CompanyEditGrid",0)

      ON KEY (ACCEPT)
        LET action_edit = TRUE
        LET i = arr_curr()   --get current row
        EXIT DISPLAY

      ON KEY (INTERRUPT,F11)
        EXIT WHILE


      # Column Sort short cuts
      ON KEY (F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY (F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY (F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_title"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY (F16)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_fname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY (F17)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_lname"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY (F18)
        LET p_order_field2 = p_order_field
        LET p_order_field = "cont_phone"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      AFTER DISPLAY
        #possible toolbar clean up 
        CALL publish_toolbar("CompanyEditGrid",1)

    END DISPLAY

    IF int_flag THEN
      LET int_flag = FALSE
      EXIT WHILE
    ELSE 
      IF action_edit THEN  --User choose accept to edit (double click or ACCEPT key)
        CALL contact_edit(l_cont_arr[i].cont_id,TRUE)
        LET action_edit = FALSE
      END IF
    END IF

  ELSE 
    DISPLAY ARRAY l_cont_arr TO sa_cont_scroll.* WITHOUT SCROLL
    EXIT WHILE
  END IF

    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

  END WHILE   

END FUNCTION





####################################################
# FUNCTION set_comp_default_titlebar()
#
# default titlebar for company menu
#
# RETURN NONE
####################################################
FUNCTION set_comp_default_titlebar()

  CALL fgl_settitle(get_str(163))  --"CMS-Company Management")

END FUNCTION


####################################################
# FUNCTION populate_company_form_combo_boxes()
#
# Populate all combo boxes of the company main form
#
# RETURN NONE
####################################################
FUNCTION populate_company_form_combo_boxes()
  IF fgl_fglgui() THEN  --populate the combo boxes dynamically from the database
    CALL country_combo_list("comp_country")
    CALL company_type_combo_list("ctype_name")
    CALL industry_type_combo_list("itype_name")
  END IF
END FUNCTION


####################################################
# FUNCTION grid_header_company_scroll()
#
# Populate the grid header of the company scroll window
#
# RETURN NONE
####################################################
FUNCTION grid_header_company_scroll()
  CALL fgl_grid_header("sa_comp","comp_id",get_str(170),"right","F13")
  CALL fgl_grid_header("sa_comp","comp_name",get_str(171),"left","F14")
  CALL fgl_grid_header("sa_comp","comp_city",get_str(172),"left","F15")
  CALL fgl_grid_header("sa_comp","comp_country",get_str(173),"left","F16")
  CALL fgl_grid_header("sa_comp","cont_name",get_str(174),"left","F17")
  CALL fgl_grid_header("sa_comp","name",get_str(175),"left","F18")
END FUNCTION

####################################################
# FUNCTION grid_header_company_contact_member()
#
# Populate the grid header of the company contact members
#
# RETURN NONE
####################################################
FUNCTION grid_header_company_contact_member()
  CALL fgl_grid_header("sa_cont_scroll","cont_id",get_str(115),"right","F13")
  CALL fgl_grid_header("sa_cont_scroll","cont_name2",get_str(116),"left","F14")
  CALL fgl_grid_header("sa_cont_scroll","cont_title",get_str(117),"left","F15")
  CALL fgl_grid_header("sa_cont_scroll","cont_fname",get_str(118),"left","F16")
  CALL fgl_grid_header("sa_cont_scroll","cont_lname",get_str(119),"left","F17")
  CALL fgl_grid_header("sa_cont_scroll","cont_phone",get_str(120),"left","F18")
END FUNCTION


####################################################
# FUNCTION grid_header_company_scroll_advanced()
#
# Populate the grid header 'advanced comp search'
#
# RETURN NONE
####################################################
FUNCTION grid_header_company_scroll_advanced()
  CALL fgl_grid_header("advanced_lookup_arr","comp_id",get_str(142),"left","F13")
  CALL fgl_grid_header("advanced_lookup_arr","comp_name",get_str(143),"left","F14")
  CALL fgl_grid_header("advanced_lookup_arr","comp_city",get_str(144),"left","F15")
END FUNCTION


#########################################################################################################
# FUNCTION print_company_html(p_company_id)
#
# Prints company details using a dynamically created html page.
#
# RETURN NONE
#########################################################################################################
FUNCTION print_company_html(p_company_id)
  DEFINE 
    p_company_id  LIKE company.comp_id,
    arg_str VARCHAR(2000),
    server_template_path_file VARCHAR(200),
    server_print_file         VARCHAR(200),
    client_path_file VARCHAR(300),
    l_company_rec RECORD LIKE company.*,
    local_debug SMALLINT,
    previous_help_file_id SMALLINT

  LET local_debug = FALSE

  LET previous_help_file_id = get_current_classic_help()

    
  IF NOT company_id_count(p_company_id) THEN
    CALL fgl_winmessage("Error printing","You must save the contact record to print it!","error")
    RETURN 
  END IF

  CALL get_company_rec(p_company_id) RETURNING l_company_rec.*

  LET arg_str = append_arg_str(arg_str, "hm_s01", get_str(107))  --"Company Id:"
  LET arg_str = append_arg_str(arg_str, "hm_s02", get_str(101))  --"Company:")
  LET arg_str = append_arg_str(arg_str, "hm_s03", get_str(102))  --"Address:")
  LET arg_str = append_arg_str(arg_str, "hm_s04", "")
  LET arg_str = append_arg_str(arg_str, "hm_s05", "")
  LET arg_str = append_arg_str(arg_str, "hm_s06", get_str(103))  --"City:")
  LET arg_str = append_arg_str(arg_str, "hm_s07", get_str(104))  --"Zone:")
  LET arg_str = append_arg_str(arg_str, "hm_s09", get_str(105))  --"ZIP:")  
  LET arg_str = append_arg_str(arg_str, "hm_s08", get_str(106))  --"Country:")
  LET arg_str = append_arg_str(arg_str, "hm_s10", get_str(112))  --"Account manager:")  
  LET arg_str = append_arg_str(arg_str, "hm_s11", get_str(109))  --"Industry:")  
  LET arg_str = append_arg_str(arg_str, "hm_s12", get_str(110))  --"Priority:") 
  LET arg_str = append_arg_str(arg_str, "hm_s13", get_str(113))  --"Company Type:")  
  LET arg_str = append_arg_str(arg_str, "hm_s14", get_str(114))  --"Main Contact:")  
  LET arg_str = append_arg_str(arg_str, "hm_s15", get_str(111))  --"Website:")  
  LET arg_str = append_arg_str(arg_str, "hm_s21",l_company_rec.comp_id)
  LET arg_str = append_arg_str(arg_str, "hm_s22",l_company_rec.comp_name)
  LET arg_str = append_arg_str(arg_str, "hm_s23",l_company_rec.comp_addr1)
  LET arg_str = append_arg_str(arg_str, "hm_s24",l_company_rec.comp_addr2)
  LET arg_str = append_arg_str(arg_str, "hm_s25",l_company_rec.comp_addr3)
  LET arg_str = append_arg_str(arg_str, "hm_s26",l_company_rec.comp_city)
  LET arg_str = append_arg_str(arg_str, "hm_s27",l_company_rec.comp_zone)
  LET arg_str = append_arg_str(arg_str, "hm_s29",l_company_rec.comp_zip) 
  LET arg_str = append_arg_str(arg_str, "hm_s28",l_company_rec.comp_country)
  LET arg_str = append_arg_str(arg_str, "hm_s30",get_contact_name(l_company_rec.acct_mgr)) 
  LET arg_str = append_arg_str(arg_str, "hm_s31",get_industry_type_name(l_company_rec.comp_industry)) 
  LET arg_str = append_arg_str(arg_str, "hm_s32",l_company_rec.comp_priority) 
  LET arg_str = append_arg_str(arg_str, "hm_s33",get_company_type_name(l_company_rec.comp_type)) 
  LET arg_str = append_arg_str(arg_str, "hm_s34",get_contact_name(l_company_rec.comp_main_cont)) 
  LET arg_str = append_arg_str(arg_str, "hm_s35",l_company_rec.comp_url) 

  #Image
  LET arg_str = append_arg_str(arg_str, "hm_i01",fgl_get_property("gui", "system.file.cachename", get_image_path("querix_logo_without_shade_bg_white_300_width.jpg")))

  #Memo TEXT
  LET arg_str = append_arg_str(arg_str, "hm_l01",l_company_rec.comp_notes)

 
  IF local_debug THEN
    DISPLAY "print_company_html() - get_print_html_template_filename(1,get_language()) = ", get_print_html_template_filename(1,get_language())
    DISPLAY "print_company_html() - get_html_path(get_print_html_template_filename(1,get_language())) = ", get_html_path(get_print_html_template_filename(1,get_language()))
    DISPLAY "print_company_html() - get_print_html_output() = ", get_print_html_output()
    DISPLAY "print_company_html() - get_html_path(get_print_html_output()) = ", get_html_path(get_print_html_output())
    #DISPLAY "print_company_html() - get_print_html_template_filename(get_print_html_template_filename(1,get_language()) = ", get_document_id_from_filename(get_print_html_template_filename(1,get_language()))

  END IF

  #Download template from DB blob to server (or file if you use the qxt_file_template library)
  LET server_template_path_file = download_blob_print_template_to_server(1,get_language(),NULL)

  #IF local_debug THEN
  #  DISPLAY "2 - print_company_html() - server_template_path_file= ", server_template_path_file
  #END IF

  #Merge Template data with application data
  LET server_print_file = print_html_simple(server_template_path_file,NULL,arg_str,"n")

  #IF local_debug THEN
  #  DISPLAY "3 - print_company_html() - server_print_file= ", server_print_file
  #  DISPLAY "3 - print_company_html() - client_path_file (before download) = ", client_path_file
  #END IF

  #Download print file to client
  LET client_path_file = file_download(server_print_file,fgl_basename(server_print_file), FALSE,NULL)

  #IF local_debug THEN
  #  DISPLAY "4 - print_company_html() - client_path_file= ", client_path_file
  #END IF


  #IF local_debug THEN
  #  DISPLAY "print_company_html() - client_path_file = ", client_path_file
  #END IF

  #Display client side file name to client browser window
  CALL print_html(client_path_file)                                                     -- print this html file

  CALL set_classic_help_file(previous_help_file_id)

END FUNCTION
