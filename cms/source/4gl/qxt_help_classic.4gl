##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings
#
# This 4gl module includes functions to ???????????????
#
# Modification History:
# 19.10.06 HH - Created - contents extracted from gd_guidemo.4gl
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"



############################################################
# Configuration functions
############################################################


###########################################################
# FUNCTION process_help_classic_cfg_import()
#
# Import/Process classic_help cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_help_classic_cfg_import(p_cfg_filename)

DEFINE 
  p_cfg_filename VARCHAR(100),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  LET p_cfg_filename = get_cfg_path(get_help_classic_cfg_filename())

  IF p_cfg_filename IS NULL THEN
    RETURN NULL
  END IF

  IF local_debug THEN
    DISPLAY "process_help_classic_cfg_import() - get_help_classic_cfg_filename() = ", get_help_classic_cfg_filename()
    DISPLAY "process_help_classic_cfg_import() - p_cfg_filename = ", p_cfg_filename
  END IF

############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_cfg_filename)

  #[Help-Classic]
  #read from classic 4gl help section
  LET qxt_settings.help_classic_multi_lang  =     configGetInt(ret, "[HelpClassic]", "help_classic_multi_lang")
  LET qxt_settings.help_classic_unl_filename  =   configGet(ret, "[HelpClassic]", "help_classic_unl_filename")

  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - filename = ", p_cfg_filename CLIPPED, "###############################"

    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [HelpClassic] File Section   x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

    DISPLAY "configInit() - qxt_settings.help_classic_multi_lang=", qxt_settings.help_classic_multi_lang
    DISPLAY "configInit() - qxt_settings.help_classic_unl_filename=",   qxt_settings.help_classic_unl_filename
  
  END IF

  CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_help_classic_cfg_export()
#
# Export cfg data
#
# RETURN ret
###########################################################
FUNCTION process_help_classic_cfg_export(p_cfg_filename)
  DEFINE 
    ret SMALLINT,
    p_cfg_filename VARCHAR(100)

  LET p_cfg_filename = get_cfg_path(get_help_classic_cfg_filename())

  IF p_cfg_filename IS NULL THEN
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret =  configInit(p_cfg_filename)

  #[HelpClassic]
  #write to classic-help section
  CALL configWrite(ret, "[HelpClassic]", "help_classic_multi_lang", qxt_settings.help_classic_multi_lang )
  CALL configWrite(ret, "[HelpClassic]", "help_classic_unl_filename", qxt_settings.help_classic_unl_filename )

END FUNCTION





############################################################
# Data Access functions
############################################################


###########################################################
# FUNCTION get_help_classic_cfg_filename()
#
# Get the help-classic 4gl library configuration file name
#
# RETURN qxt_settings.main_cfg_filename
###########################################################
FUNCTION get_help_classic_cfg_filename()
  RETURN qxt_settings.help_classic_cfg_filename
END FUNCTION

###########################################################
# FUNCTION set_help_classic_cfg_filename(p_fname
#
# Set the help-classic 4gl library configuration file name
#
# RETURN NONE
###########################################################
FUNCTION set_help_classic_cfg_filename(p_fname)
  DEFINE p_fname  VARCHAR(100)

  LET qxt_settings.help_classic_cfg_filename = p_fname
END FUNCTION


############################################################
# FUNCTION get_classic_help_multi_lang()
#
# Get the qxt_settings.classic_help_multi_lang
#
# RETURN qxt_settings.classic_help_multi_lang
############################################################
FUNCTION get_help_classic_multi_lang()
  RETURN qxt_settings.help_classic_multi_lang
END FUNCTION


############################################################
# FUNCTION get_help_classic_unl_filename()
#
# Get the qxt_settings.help_classic_unl_filename
#
# RETURN qxt_settings.classic_help_multi_lang
############################################################
FUNCTION get_help_classic_unl_filename()
  RETURN qxt_settings.help_classic_unl_filename
END FUNCTION


############################################################
# FUNCTION set_help_classic_unl_filename(p_filename)
#
# Set the qxt_settings.help_classic_unl_filename
#
# RETURN qxt_settings.classic_help_multi_lang
############################################################
FUNCTION set_help_classic_unl_filename(p_filename)
  DEFINE
    p_filename VARCHAR(100)

  LET qxt_settings.help_classic_unl_filename = p_filename
END FUNCTION
############################################################################################################
# EOF
############################################################################################################








