##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_file_print_image_globals.4gl"


###########################################################
# FUNCTION get_print_html_image_lib_info()
#
# Simply returns libray type /  name
#
# RETURN "FILE - qxt_file_print_html_image"
###########################################################
FUNCTION get_print_html_image_lib_info()
  RETURN "FILE - qxt_file_print_html_image"
END FUNCTION 




#####################################################################################
# Init/Config/Import Functions
#####################################################################################

###########################################################
# FUNCTION process_print_html_image_init(p_filename)
#
# Init - must stay compatible with db/file library
#
# RETURN NONE
###########################################################
FUNCTION process_print_html_image_init(p_filename)
  DEFINE 
    p_filename VARCHAR(100)

  IF p_filename IS NULL THEN
    LET p_filename = get_print_html_image_unl_filename()
  END IF

  CALL qxt_print_html_image_import_file(get_unl_path(p_filename))
  #This function must be compatible with the file toolbar library
END FUNCTION

###########################################################
# FUNCTION qxt_print_html_image_import_file(p_filename)
#
# Import the image list file
#
# RETURN NONE
###########################################################
FUNCTION qxt_print_html_image_import_file(p_filename)
  DEFINE 
    p_filename                     VARCHAR(150),
    l_tmp_qxt_print_html_image_rec OF t_qxt_print_html_image_rec,
    sql_stmt                       CHAR(1000),
    local_debug                    SMALLINT,
    err_msg                        VARCHAR(200),
    id                             SMALLINT


  LET local_debug = FALSE

  #Check if file exists
  CALL validate_file_server_side_exists_advanced(p_filename,"f",TRUE)

  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_print_html_image_import_file()\nCannot open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_image_import_file()", err_msg, "error")
    RETURN
  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_print_html_image_import_file()\nCannot set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_image_import_file()", err_msg, "error")
    RETURN
  END IF


  WHILE fgl_channel_read("stream",l_tmp_qxt_print_html_image_rec.*) 

    LET qxt_print_html_image[l_tmp_qxt_print_html_image_rec.id] = l_tmp_qxt_print_html_image_rec.filename

    IF local_debug THEN
      DISPLAY "qxt_print_html_image_import_file() - l_tmp_qxt_print_html_image_rec.id=",           l_tmp_qxt_print_html_image_rec.id 
      DISPLAY "qxt_print_html_image_import_file() - l_tmp_qxt_print_html_image_rec.filename=",     l_tmp_qxt_print_html_image_rec.filename 
    END IF 

  END WHILE

  #Close the file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_print_html_image_import_file()\nCannot close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_print_html_image_import_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION


##############################################################
# FUNCTION get_print_html_image_filename(id)
#
# Returns the corresponding html print image file name
#
# RETURN qxt_settings.print_html_image[id]
##############################################################
FUNCTION get_print_html_image_filename(id)
  DEFINE
    ID   SMALLINT

  #IF id > 8 OR id < 1 THEN
  #  CALL fgl_winmessage("Error in get_print_html_image()","Calling argument is out of range","error")
  #  RETURN NULL
  #END IF

  RETURN qxt_print_html_image[id]

END FUNCTION
