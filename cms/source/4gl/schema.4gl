##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

DATABASE cms

GLOBALS
  DEFINE window_open SMALLINT
END GLOBALS

MAIN

DEFINE i INTEGER

CALL fgl_putenv("DBDATE=dmy4/")

WHENEVER ERROR CONTINUE
DROP TABLE contact
DROP TABLE contact_dept
DROP TABLE company
DROP TABLE company_type
DROP TABLE company_link
DROP TABLE country
DROP TABLE position_type
DROP TABLE activity_type
DROP TABLE activity
DROP TABLE users
DROP TABLE menus
DROP TABLE menu_options
DROP TABLE titles
DROP TABLE industry_type
DROP TABLE mailbox
DROP TABLE invoice
DROP TABLE invoice_line
DROP TABLE account
DROP TABLE stock_item
DROP TABLE tax_rates
DROP TABLE pay_methods
DROP TABLE user_fields
DROP TABLE user_field_data
DROP TABLE curr
WHENEVER ERROR STOP

WHENEVER ERROR CALL error_func
BEGIN WORK

CALL splash(1,"menus")


CREATE TABLE menus (
	menu_id		SERIAL,
	lang_id		INTEGER,
	menu_name	VARCHAR(20)
	)

CALL splash(1,"menu_options")

CREATE TABLE menu_options (
	option_id	SERIAL,
	menu_id		INTEGER,
	option_name	VARCHAR(20),
	option_comment	VARCHAR(60),
	option_keypress	VARCHAR(10)
	)

CALL splash(1,"contact")

CREATE TABLE contact (
	cont_id 	SERIAL(1000),
	cont_title	CHAR(5),
	cont_name 	CHAR(20) NOT NULL,
	cont_fname 	CHAR(20),
	cont_lname 	CHAR(20),
	cont_addr1	CHAR(40),
	cont_addr2	CHAR(40),
	cont_addr3	CHAR(40),
	cont_city	CHAR(20),
	cont_zone	CHAR(15),
	cont_country	CHAR(30),
	cont_zip	CHAR(15),
	cont_phone	CHAR(15),
	cont_fax	CHAR(15),
	cont_mobile	CHAR(15),
	cont_email	CHAR(50),
	cont_dept	CHAR(15),	# ref cont_dept_id
	cont_org	INTEGER,	# ref company_id
	cont_position	CHAR(15),	# ref position_id
	cont_picture	BYTE,
	cont_password	CHAR(15),
	cont_ipaddr	CHAR(15),	# IP V4
	cont_usemail	SMALLINT,
	cont_usephone	SMALLINT
)

CALL splash(1,"company")

CREATE TABLE company (
	comp_id		SERIAL(1000),
	comp_name	CHAR(100) NOT NULL,
	comp_addr1	CHAR(40),
	comp_addr2	CHAR(40),
	comp_addr3	CHAR(40),
	comp_city	CHAR(20),
	comp_zone	CHAR(15),
	comp_country	CHAR(30),
	comp_zip	CHAR(15),
	acct_mgr	INTEGER,		# ref contact_id
	comp_link	INTEGER,		# future: cross ref link table
	comp_industry	INTEGER,		# ref industry_id
	comp_priority	INTEGER,
	comp_type	INTEGER,		# ref cont_dept_id
	comp_main_cont	INTEGER	NOT NULL,	# ref contact_id
	comp_url	VARCHAR(50),
	comp_notes	VARCHAR(240)
)

CREATE TABLE country (
	country         VARCHAR(30)   --country lookup
)

CALL splash(1,"user_fields")

CREATE TABLE user_fields (
	field_id	SERIAL,
	field_name	VARCHAR(50),
	field_desc	VARCHAR(50),
	field_type	INTEGER
)

CALL splash(1,"user_field_data")

CREATE TABLE user_field_data (
	field_id	INTEGER,
	contact_id	INTEGER,
	field_data	VARCHAR(50)
)

CALL splash(1,"industry_type")

CREATE TABLE industry_type (
	type_id	SERIAL,
	itype_name	VARCHAR(20),
	user_def	SMALLINT
	)

CALL splash(1,"company_type")

CREATE TABLE company_type (
	type_id		SERIAL,
	ctype_name	CHAR(15),
	user_def	SMALLINT,
	base_priority	INTEGER
)

CALL splash(1,"contact_dept")

CREATE TABLE contact_dept (
	dept_id		SERIAL,
	dept_name	CHAR(15),
	user_def	SMALLINT
)

CALL splash(1,"position_type")

CREATE TABLE position_type (
	type_id		SERIAL,
	ptype_name	CHAR(15),
	user_def	SMALLINT
)

CALL splash(1,"company_link")

CREATE TABLE company_link (
	parent_comp_id	INTEGER,	# ref comp_id
	child_comp_id	INTEGER		# ref comp_id
)

CALL splash(1,"activity_type")

CREATE TABLE activity_type (
	type_id		SERIAL,
	atype_name	CHAR(10),
	user_def	SMALLINT
)

CALL splash(1,"titles")

CREATE TABLE titles (
	lang_id		INTEGER,
	title_name	VARCHAR(10)
	)

CALL splash(1,"activity")

CREATE TABLE activity (
	activity_id	SERIAL,
	open_date	DATE NOT NULL,
	close_date	DATE,
	contact_id	INTEGER NOT NULL,	# ref contact_id
	comp_id		INTEGER,		# ref comp_id
	user_id		INTEGER NOT NULL,	# ref user_id
	act_type	INTEGER,		# ref activity_type
	long_desc	TEXT,
	short_desc	CHAR(30),
#	long_ref	INTEGER,		# ref possible blob
	a_owner		INTEGER,		# ref contact_id
	priority	INTEGER
)

CALL splash(1,"users")

CREATE TABLE users (
  user_id		SERIAL,
  name			CHAR(10) NOT NULL,
  password		CHAR(10),
  type			CHAR,
  cont_id		INTEGER # ref contact_id
)

CALL splash(1,"mailbox")

CREATE TABLE mailbox (
  mail_id		SERIAL,				# 
  mail_status		SMALLINT,			# inbound, outbound, read, deleted, etc.
  user_id		INTEGER NOT NULL, 		# ref users.user_id
  activity_id		INTEGER NOT NULL,		# ref activity
  contact_id		INTEGER,			# ref contact. 
# although the activity provides an implicit contact, we need a contact where no activity is involved. 
# Eventually, I don't want this to reference activity for obvious reasons, but
# for now it'll be OK.
  recv_date		DATE,
  read_flag		SMALLINT,
  urgent_flag		SMALLINT
)

CALL splash(1,"invoice")

CREATE TABLE invoice (
  invoice_id		SERIAL(1000),
  account_id		INTEGER NOT NULL,
  invoice_date		DATE NOT NULL,
  pay_date          	DATE,
  tax_total		MONEY(8,2) NOT NULL,
  net_total		MONEY(8,2) NOT NULL,
  inv_total		MONEY(8,2) NOT NULL,
  currency_id		INTEGER,
  for_total         	MONEY(8,2),        -- 
  user_id		INTEGER NOT NULL,
  pay_method		INTEGER NOT NULL,
  pay_desc		CHAR(20),
  del_address_dif	INTEGER NOT NULL,
  del_address1 		CHAR(30),
  del_address2 		CHAR(30),
  del_address3 		CHAR(30),
  del_method 		CHAR(20),
  invoice_po		CHAR(20)
)

CALL splash(1,"curr")

CREATE TABLE curr (
  currency_id		SERIAL,
  currency_name		CHAR(10),
  xchg_rate			DECIMAL(5,3)
)

CALL splash(1,"invoice_line")

CREATE TABLE invoice_line (
  invoice_id		INTEGER NOT NULL,    -- references invoice
  quantity		INTEGER,             
  stock_id		CHAR(10) NOT NULL,  -- references stock item
  item_tax		INTEGER NOT NULL     -- references tax rates
)

CALL splash(1,"stock_item")

CREATE TABLE stock_item (
  stock_id 		CHAR(10) NOT NULL,
  item_desc		CHAR(80),
  item_cost		MONEY(6,2),
  rate_id		INTEGER NOT NULL
)

CALL splash(1,"account")

CREATE TABLE account (
  account_id		SERIAL,
  comp_id		INTEGER NOT NULL,
  credit_limit		MONEY(6,2),
  discount		DECIMAL(4,2) NOT NULL
)

CALL splash(1,"tax_rates")

CREATE TABLE tax_rates (
  rate_id		SERIAL,
  tax_rate		DECIMAL(4,2) NOT NULL,
  tax_desc		CHAR(80)
)

CALL splash(1,"pay_methods")

CREATE TABLE pay_methods (
  pay_id		SERIAL,
  pay_name		CHAR(10),
  pay_desc		CHAR(20)
)

COMMIT WORK
BEGIN WORK

CALL splash(3,"users")
LOAD FROM "unl/users.unl" INSERT INTO users
CALL splash(3,"activity_type")
LOAD FROM "unl/activity_type.unl" INSERT INTO activity_type
CALL splash(3,"company_type")
LOAD FROM "unl/company_type.unl" INSERT INTO company_type
CALL splash(3,"position_type")
LOAD FROM "unl/position_type.unl" INSERT INTO position_type
CALL splash(3,"menus")
LOAD FROM "unl/menus.unl" INSERT INTO menus
CALL splash(3,"menu_options")
LOAD FROM "unl/menu_options.unl" INSERT INTO menu_options
CALL splash(3,"titles")
LOAD FROM "unl/titles.unl" INSERT INTO titles
CALL splash(3,"company")
LOAD FROM "unl/company.unl" INSERT INTO company
CALL splash(3,"contact")
LOAD FROM "unl/contact.unl" INSERT INTO contact
CALL splash(3,"country")
LOAD FROM "unl/country.unl" INSERT INTO country
CALL splash(3,"activity")
LOAD FROM "unl/activity.unl" INSERT INTO activity
CALL splash(3,"industry_type")
LOAD FROM "unl/industry_type.unl" INSERT INTO industry_type
CALL splash(3, "contact_dept")
LOAD FROM "unl/contact_dept.unl" INSERT INTO contact_dept
CALL splash(3, "mailbox")
LOAD FROM "unl/mailbox.unl" INSERT INTO mailbox
CALL splash(3, "invoice")
LOAD FROM "unl/invoice.unl" INSERT INTO invoice
CALL splash(3, "invoice_line")
LOAD FROM "unl/invoice_line.unl" INSERT INTO invoice_line
CALL splash(3, "stock_item")
LOAD FROM "unl/stock_item.unl" INSERT INTO stock_item
CALL splash(3, "account")
LOAD FROM "unl/account.unl" INSERT INTO account
CALL splash(3, "tax_rates")
LOAD FROM "unl/tax_rates.unl" INSERT INTO tax_rates
CALL splash(3, "pay_methods")
LOAD FROM "unl/pay_methods.unl" INSERT INTO pay_methods
COMMIT WORK

CALL fgl_message_box("Objects created successfully\n\nUse 'ADMIN/ADMIN' to log in")
IF window_open THEN
  CLOSE WINDOW w_splash
END IF
END MAIN


FUNCTION splash(p_type,p_name)
  DEFINE p_type INTEGER
  DEFINE p_name VARCHAR(40)

  IF NOT window_open THEN
    OPEN WINDOW w_splash AT 4,4 WITH 15 ROWS, 60 COLUMNS
      LET window_open = TRUE
  END IF
  DISPLAY "" AT 4,4
  CASE p_type
    WHEN 1
      DISPLAY "Creating Table ", p_name, "..." AT 4,4
    WHEN 2
      DISPLAY "Creating View ", p_name, "..." AT 4,4
    WHEN 3 
      DISPLAY "Loading Table ", p_name, "..." AT 4,4
  END CASE
  --SLEEP 1
END FUNCTION


FUNCTION error_func()
  DEFINE msg VARCHAR(200)

  IF sqlca.sqlcode = 0 THEN
    RETURN
  END IF
  LET msg = "SQL Error: ", sqlca.sqlcode, "\nObject creation failed"
  CALL fgl_message_box(msg)
  ROLLBACK WORK
  EXIT PROGRAM
END FUNCTION

