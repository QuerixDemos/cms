##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################################################################
# Functions for the contact_dept  (contact department) table 
############################################################################################################
#
# FUNCTION:                                        DESCRIPTION:                                         RETURN
# department_combo_list(cb_field_name)             populates the department combo box                   NONE
# get_contact_dept_id(p_dept_name)                 returns the corresp. id from name                    l_dept_id
# get_contact_dept_name(p_dept_id)                 Get contact dept. name from id                       l_dept_name
# get_contact_dept_rec(p_dept_id)                  get contact department record from id                l_dept_rec.*
# contact_dept_popup_data_source                   Data Source (cursor) for contact_dept_popup          NONE
#   (p_order_field,p_ord_dir)
# contact_dept_popup                               Display contact department list for                  l_contact_dept_arr[i].dept_id or p_dept_id
#   (p_dept_id,p_order_field,p_accept_action)        selection and management
# contact_dept_create()                            Create a contact dept record                         NONE
# contact_dept_delete(p_dept_id)                   Delete contact dept record                           NONE
# contact_dept_view_by_rec(p_contact_dept_rec)            Display cont. department details on form      NONE
# contact_dept_view(p_contact_dept_id)             Display cont. department details on form using id    NONE
# contact_dept_edit(p_dept_id)                     Edit cont. dep. record                               NONE
# contact_dept_input(p_contact_dept)               INPUT contact details from form (edit/create)        l_contact_dept.*
############################################################################################################


#############################################
# GLOBALS
#############################################
GLOBALS "globals.4gl"


#############################################
# FUNCTION contact_dept_popup_data_source(p_order_field,p_ord_dir)
#
# Data Source (cursor) for contact_dept_popup
#
# RETURN NONE
#############################################
FUNCTION contact_dept_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4)

  IF p_order_field IS NULL  THEN
    LET p_order_field = "dept_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF
   
  LET sql_stmt = "SELECT * FROM contact_dept "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_cont_dept FROM sql_stmt
  DECLARE c_cont_dept CURSOR FOR p_cont_dept




END FUNCTION

#############################################
# FUNCTION contact_dept_popup(p_dept_id,p_order_field,p_accept_action)
#
# Display contact department list for selection and management
#
# RETURN l_contact_dept_arr[i].dept_id or p_dept_id
#############################################
FUNCTION contact_dept_popup(p_dept_id,p_order_field,p_accept_action)
  DEFINE 
    p_dept_id                    LIKE contact_dept.dept_id,
    l_contact_dept_arr           DYNAMIC ARRAY OF RECORD LIKE contact_dept.*,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    local_debug                  SMALLINT

  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""

    CALL fgl_window_open("w_cont_dept_scroll", 2, 8 , get_form_path("f_contact_dept_scroll_l2"), FALSE) 
    CALL populate_department_list_form_labels_g()

	CALL ui.Interface.setImage("qx://application/icon16/basic/tree-type.png")
	CALL ui.Interface.setText("Contact Department")
	
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL contact_dept_popup_data_source(p_order_field,get_toggle_switch())

    LET i = 1

    FOREACH c_cont_dept INTO l_contact_dept_arr[i].*
      LET i = i + 1
      IF i > 20 THEN
        EXIT FOREACH
      END IF
    END FOREACH

    LET i = i - 1

		IF i > 0 THEN
			CALL l_contact_dept_arr.resize(i)   --correct the last element of the dynamic array
		END IF

    IF NOT i THEN
      CALL fgl_window_close("w_cont_dept_scroll")
      RETURN NULL
    END IF


		
    #CALL set_count(i)
    LET int_flag = FALSE

    DISPLAY ARRAY l_contact_dept_arr TO sa_cont_dept.*
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 4
 
      BEFORE DISPLAY
        CALL publish_toolbar("ContactDepartmentList",0)
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET i = l_contact_dept_arr[i].dept_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL contact_dept_view(i)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL contact_dept_edit(i)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL contact_dept_delete(i)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL contact_dept_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","contact_dept_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "contact_dept_popup(p_dept_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("contact_dept_popup() - 4GL Source Error",err_msg, "error") 
        END CASE


      ON KEY (F4) -- add
        CALL contact_dept_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET i = l_contact_dept_arr[i].dept_id
        CALL contact_dept_edit(i)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET i = l_contact_dept_arr[i].dept_id
        CALL contact_dept_delete(i)
        EXIT DISPLAY

      ON KEY (F9)
        LET int_flag = TRUE
        EXIT WHILE

      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "dept_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "dept_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      
      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "user_def"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF

  END WHILE

  CALL fgl_window_close("w_cont_dept_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_dept_id
  ELSE 
    LET i = arr_curr()
    RETURN l_contact_dept_arr[i].dept_id
  END IF

END FUNCTION


#############################################
# FUNCTION contact_dept_create()
#
# Create a contact dept record
#
# RETURN NONE
#############################################
FUNCTION contact_dept_create()
  DEFINE l_contact_dept RECORD LIKE contact_dept.*

  LET l_contact_dept.dept_id = 0
  CALL contact_dept_input(l_contact_dept.*)
    RETURNING l_contact_dept.*

  IF NOT int_flag THEN 
    INSERT INTO contact_dept VALUES (l_contact_dept.*)
  ELSE
    LET int_flag = FALSE
  END IF
END FUNCTION

# basic skeleton. We need to actually scan through to propogate deletions to records which reference the object.
# possibly a deletion marker would be more appropriate

#############################################
# FUNCTION contact_dept_delete(p_dept_id)
#
# Delete contact dept record
#
# RETURN NONE
#############################################
FUNCTION contact_dept_delete(p_dept_id)
  DEFINE p_dept_id INTEGER
  #"Delete!","Are you sure you want to delete this value?"
  IF yes_no(get_str(55),get_str(56)) THEN
    DELETE FROM contact_dept 
      WHERE dept_id = p_dept_id
  END IF
END FUNCTION


#############################################
# FUNCTION contact_dept_edit(p_dept_id)
#
# Edit cont. dep. record
#
# RETURN NONE
#############################################
FUNCTION contact_dept_edit(p_dept_id)
  DEFINE 
    p_dept_id INTEGER,
    l_contact_dept RECORD LIKE contact_dept.*

  SELECT contact_dept.* 
    INTO l_contact_dept
    FROM contact_dept
    WHERE contact_dept.dept_id = p_dept_id

  IF sqlca.sqlcode = 100 THEN
    RETURN
  END IF

  CALL contact_dept_input(l_contact_dept.*) 
    RETURNING l_contact_dept.*

  IF NOT int_flag THEN
    UPDATE contact_dept
      SET dept_name = l_contact_dept.dept_name,
          user_def = l_contact_dept.user_def
      WHERE contact_dept.dept_id = l_contact_dept.dept_id
  ELSE
    LET int_flag = FALSE
  END IF
END FUNCTION


#############################################
# FUNCTION contact_dept_input(p_contact_dept)
#
# INPUT contact details from form (edit/create)
#
# RETURN l_contact_dept.*
#############################################
FUNCTION contact_dept_input(p_contact_dept)
  DEFINE 
    p_contact_dept RECORD LIKE contact_dept.*,
    l_contact_dept RECORD LIKE contact_dept.*

  LET l_contact_dept.* = p_contact_dept.*

    CALL fgl_window_open("w_contact_dept", 5, 10, get_form_path("f_contact_dept_l2"), FALSE)
    CALL populate_department_form_labels_g()

  LET int_flag = FALSE

  INPUT BY NAME l_contact_dept.* WITHOUT DEFAULTS  HELP 1501
    AFTER INPUT
    IF NOT int_flag THEN
      IF l_contact_dept.dept_name IS NULL THEN
        #"The field 'Contact Department' (name) must be filled in!"
        CALL fgl_winmessage(get_str(48),get_str(1560),"info")
        CONTINUE INPUT
      END IF 
    ELSE
      --Confirm, if operator wants to abort record creation
      #"Cancel Record Creation/Modification","Do you really want to abort the new/modified record entry ?"
      IF NOT yes_no(get_str(53),get_Str(54)) THEN
        #LET int_flag = FALSE
        CONTINUE INPUT
      END IF

    END IF
  END INPUT

  CALL fgl_window_close("w_contact_dept")
  RETURN l_contact_dept.*
END FUNCTION

#############################################
# FUNCTION contact_dept_view(p_contact_dept_id)
#
# Display cont. department details on form using id
#
# RETURN NONE
#############################################
FUNCTION contact_dept_view(p_contact_dept_id)
  DEFINE
    p_contact_dept_id LIKE contact_dept.dept_id, 
    l_contact_dept_rec RECORD LIKE contact_dept.*

    CALL get_contact_dept_rec(p_contact_dept_id) RETURNING l_contact_dept_rec.*
    CALL contact_dept_view_by_rec(l_contact_dept_rec.*)
    

END FUNCTION


#############################################
# FUNCTION contact_dept_view_by_rec(p_contact_dept_rec)
#
# Display cont. department details on form
#
# RETURN NONE
#############################################
FUNCTION contact_dept_view_by_rec(p_contact_dept_rec)
  DEFINE 
    p_contact_dept_rec RECORD LIKE contact_dept.*,
    inp_char           CHAR

    CALL fgl_window_open("w_contact_dept", 5, 10, get_form_path("f_contact_dept_l2"), FALSE)
    CALL populate_department_form_labels_g()
    DISPLAY get_str(811) TO bt_ok
    DISPLAY "*" TO bt_cancel


  LET int_flag = FALSE

  DISPLAY BY NAME p_contact_dept_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_contact_dept")

END FUNCTION

