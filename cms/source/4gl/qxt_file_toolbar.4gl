##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_toolbar_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_toolbar_lib_info()
#
# Simply returns libray name
#
# RETURN "FILE - qxt_file_toolbar"
###########################################################
FUNCTION get_toolbar_lib_info()
  RETURN "FILE - qxt_file_toolbar"
END FUNCTION 



#####################################################################################
# Init/Config/Import Functions
#####################################################################################


###########################################################
# FUNCTION process_toolbar_init(file_name)
#
# Import toolbar and tooltip unl files - PS: stay compatible with db library
#
# RETURN NONE
###########################################################
FUNCTION process_toolbar_init(p_toolbar_unl_filename,p_tooltip_unl_filename)
  DEFINE 
    p_toolbar_unl_filename VARCHAR(150),
    p_tooltip_unl_filename VARCHAR(150)


   CALL qxt_toolbar_import_file(p_toolbar_unl_filename)
   CALL qxt_tooltip_import_file(p_tooltip_unl_filename)

   #This function must be compatible with the file toolbar library
END FUNCTION


###########################################################
# FUNCTION qxt_toolbar_import_file(p_toolbar_unl_filename,p_tooltip_unl_filename)
#
# Do Nothing - just stay compatible with db library
#
# RETURN NONE
###########################################################
FUNCTION qxt_toolbar_import_file(p_toolbar_unl_filename)
  DEFINE
    p_toolbar_unl_filename   VARCHAR(150),
    local_debug              SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF

  IF p_toolbar_unl_filename IS NULL THEN
    LET p_toolbar_unl_filename = get_toolbar_unl_filename()
  END IF

  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF

  # and add path  
  LET p_toolbar_unl_filename = get_unl_path(p_toolbar_unl_filename)

  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_toolbar_unl_filename=", p_toolbar_unl_filename
  END IF


  #Final check
  IF p_toolbar_unl_filename IS NULL THEN
    CALL fgl_winmessage("Error in qxt_tb_import_file()","Error in qxt_tb_import_file()\nNo import toolbar_unl_filename was specified in the function call\nand also not found in the toolbar.cfg","error")
    RETURN NULL
  END IF


  #Finally, import files
  CALL qxt_tb_import_toolbar_file(p_toolbar_unl_filename)

END FUNCTION



###########################################################
# FUNCTION qxt_tooltip_import_file(p_tooltip_unl_filename)
#
# Do Nothing - just stay compatible with db library
#
# RETURN NONE
###########################################################
FUNCTION qxt_tooltip_import_file(p_tooltip_unl_filename)
  DEFINE
    p_tooltip_unl_filename  VARCHAR(150),
    local_debug             SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  IF p_tooltip_unl_filename IS NULL THEN
    LET p_tooltip_unl_filename = get_tooltip_unl_filename()
  END IF


  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF

  # and add path  
  LET p_tooltip_unl_filename = get_unl_path(p_tooltip_unl_filename)

  IF local_debug THEN
    DISPLAY "qxt_tb_import_file() - p_tooltip_unl_filename=", p_tooltip_unl_filename
  END IF


  #Final check
  IF p_tooltip_unl_filename IS NULL THEN
    CALL fgl_winmessage("Error in qxt_tb_import_file()","Error in qxt_tb_import_file()\nNo import tooltip_unl_filename was specified in the function call\nand also not found in the toolbar.cfg","error")
    RETURN NULL
  END IF

  #Finally, import files
  CALL qxt_tb_import_tooltip_file(p_tooltip_unl_filename,get_language())

END FUNCTION


###########################################################
# FUNCTION qxt_tb_import_tooltip_file(p_filename,p_language_id)
#
# Import the toolbar tooltip file
#
# RETURN NONE
###########################################################
FUNCTION qxt_tb_import_tooltip_file(p_filename,p_language_id)
  DEFINE 
    p_filename          VARCHAR(150),
    #p_application_id    SMALLINT,
    p_language_id       SMALLINT,
    temp_tooltip_rec     OF t_qxt_toolbar_tooltip_file_rec,
#    l_tbi_tooltip_rec   OF t_qxt_tbi_tooltip_rec,
    sql_stmt            CHAR(1000),
    local_debug         SMALLINT,
    err_msg             VARCHAR(200),
    i                   SMALLINT

  LET local_debug = FALSE

  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_tb_import_tooltip_file()\nCannot open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_tooltip_file()", err_msg, "error")
    RETURN
  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_tb_import_tooltip_file()\nCannot set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_tooltip_file()", err_msg, "error")
    RETURN
  END IF


  #Import the file  (only for the current language)
  WHILE fgl_channel_read("stream",temp_tooltip_rec.*) 
    LET i = temp_tooltip_rec.string_id
  
    IF temp_tooltip_rec.language_id = p_language_id THEN
      LET qxt_toolbar_tooltip_file_rec[i].string_id   = temp_tooltip_rec.string_id
      LET qxt_toolbar_tooltip_file_rec[i].string_data = temp_tooltip_rec.string_data
    END IF

    IF local_debug THEN
      DISPLAY "qxt_tb_import_tooltip_file() - qxt_toolbar_tooltip_file_rec[", trim(i), "].string_id=",   qxt_toolbar_tooltip_file_rec[i].string_id
      DISPLAY "qxt_tb_import_tooltip_file() - qxt_toolbar_tooltip_file_rec[", trim(i), "].string_data=",   qxt_toolbar_tooltip_file_rec[i].string_data
    END IF 
    
  END WHILE

  #Close Channel file handler
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_tb_import_tooltip_file()\nCan not close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_tooltip_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION



###########################################################
# FUNCTION qxt_tb_import_toolbar_file(p_filename)
#
# Import the toolbar file
#
# RETURN NONE
###########################################################
FUNCTION qxt_tb_import_toolbar_file(p_filename)
  DEFINE 
    p_filename              VARCHAR(150),
    #l_toolbar_file_rec      OF t_qxt_toolbar_file_rec,
    sql_stmt                CHAR(1000),
    local_debug             SMALLINT,
    err_msg                 VARCHAR(200),
    i                       SMALLINT

  LET local_debug = FALSE


  #Open the file
  IF NOT fgl_channel_open_file("stream",p_filename, "r") THEN
    LET err_msg = "Error in qxt_tb_import_toolbar_file()\nCannot open file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_toolbar_file()", err_msg, "error")
    RETURN
  END IF

  #Set delimiter
  IF NOT fgl_channel_set_delimiter("stream","|") THEN
    LET err_msg = "Error in qxt_tb_import_toolbar_file()\nCannot set delimiter for file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_toolbar_file()", err_msg, "error")
    RETURN
  END IF


  LET i = 1
  WHILE fgl_channel_read("stream",qxt_toolbar_file_rec[i].*) 

    IF local_debug THEN
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tb_name=",           qxt_toolbar_file_rec[i].tb_name
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tb_instance=",       qxt_toolbar_file_rec[i].tb_instance
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].event_type_id=",     qxt_toolbar_file_rec[i].event_type_id
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tbi_event_name=",    qxt_toolbar_file_rec[i].tbi_event_name
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tbi_obj_action_id=", qxt_toolbar_file_rec[i].tbi_obj_action_id
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tbi_scope_id=",      qxt_toolbar_file_rec[i].tbi_scope_id
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tbi_position=",      qxt_toolbar_file_rec[i].tbi_position
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].tbi_static_id=",     qxt_toolbar_file_rec[i].tbi_static_id
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].icon_filename=",     qxt_toolbar_file_rec[i].icon_filename
      DISPLAY "qxt_tb_import_toolbar_file() - qxt_toolbar_file_rec[", trim(i), "].string_id=",         qxt_toolbar_file_rec[i].string_id
    END IF 

    CALL validate_file_server_side_exists_advanced(get_tb_icon_path(qxt_toolbar_file_rec[i].icon_filename),"f",TRUE)
    CALL validate_file_client_side_cache_exists(get_tb_icon_path(qxt_toolbar_file_rec[i].icon_filename), NULL,TRUE)

    LET i = i + 1
  END WHILE

  #Close the file
  IF NOT fgl_channel_close("stream") THEN
    LET err_msg = "Error in qxt_tb_import_toolbar_file()\nCannot close file ", trim(p_filename)
    CALL fgl_winmessage("Error in qxt_tb_import_toolbar_file()", err_msg, "error")
    RETURN
  END IF

END FUNCTION




#####################################################################################
# Actual toolbar / toolbar icon DRAW functions
#####################################################################################


###########################################################
# FUNCTION publish_toolbar(p_tb_name,p_instance)
#
# Publish a group of toolbar menu icons (one toolbar for one instance)
# Upper function to keep qxt_db and qxt_file compatible
#
# RETURN NONE
###########################################################
FUNCTION publish_toolbar(p_tb_name,p_instance)
  DEFINE
    p_tb_name          VARCHAR(35),
    p_instance         SMALLINT
    
  CALL draw_toolbar(p_tb_name,p_instance)

END FUNCTION



###########################################################
# FUNCTION draw_toolbar(p_tb_name,p_instance)
#
# This is the actual draw toolbar function for the file lib
#
# RETURN NONE
###########################################################
FUNCTION draw_toolbar(p_tb_name,p_instance)
  DEFINE
    p_application_id   SMALLINT,
    p_tb_name          VARCHAR(35),
    p_instance         SMALLINT,
    l_tbi_rec          OF t_qxt_toolbar_item_rec,
    local_debug        SMALLINT,
    i                  SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "draw_toolbar() - FUNCTION ENTRY"
  END IF 

  FOR i = 1 TO sizeof(qxt_toolbar_file_rec)
    IF qxt_toolbar_file_rec[i].tb_name = p_tb_name AND
       qxt_toolbar_file_rec[i].tb_instance = p_instance THEN

      LET l_tbi_rec.event_type_id     = qxt_toolbar_file_rec[i].event_type_id
      LET l_tbi_rec.tbi_event_name    = qxt_toolbar_file_rec[i].tbi_event_name
      LET l_tbi_rec.tbi_obj_action_id = qxt_toolbar_file_rec[i].tbi_obj_action_id
      LET l_tbi_rec.tbi_scope_id      = qxt_toolbar_file_rec[i].tbi_scope_id
      LET l_tbi_rec.tbi_position      = qxt_toolbar_file_rec[i].tbi_position
      LET l_tbi_rec.tbi_static_id     = qxt_toolbar_file_rec[i].tbi_static_id
      LET l_tbi_rec.icon_filename     = qxt_toolbar_file_rec[i].icon_filename
      LET l_tbi_rec.string_data       = qxt_toolbar_tooltip_file_rec[qxt_toolbar_file_rec[i].string_id].string_data

      IF local_debug THEN
        DISPLAY "draw_toolbar() - l_tbi_rec.event_type_id=",     l_tbi_rec.event_type_id
        DISPLAY "draw_toolbar() - l_tbi_rec.tbi_event_name=",    l_tbi_rec.tbi_obj_action_id
        DISPLAY "draw_toolbar() - l_tbi_rec.tbi_obj_action_id=", l_tbi_rec.tbi_obj_action_id
        DISPLAY "draw_toolbar() - l_tbi_rec.tbi_scope_id =",     l_tbi_rec.tbi_scope_id 
        DISPLAY "draw_toolbar() - l_tbi_rec.tbi_position=",      l_tbi_rec.tbi_position
        DISPLAY "draw_toolbar() - l_tbi_rec.tbi_static_id=",     l_tbi_rec.tbi_static_id
        DISPLAY "draw_toolbar() - l_tbi_rec.icon_filename=",     l_tbi_rec.icon_filename
        DISPLAY "draw_toolbar() - l_tbi_rec.string_data=",       l_tbi_rec.string_data
      END IF 

      IF local_debug THEN
        DISPLAY "draw_toolbar() - CALL draw_tb_icon()"
      END IF 

      CALL draw_tb_icon(l_tbi_rec.*)

    END IF

  END FOR

END FUNCTION

#####################################################################################
# EOF
#####################################################################################
