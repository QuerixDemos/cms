##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################
# 
# FUNCTION:                                     DESCRIPTION:                                      RETURN
# get_delivery_rate(p_delivery_type_id)         Get the Delivery Rate                             l_delivery_rate
# get_delivery_rate_iv(p_invoice_id)            The the Delivery Rate from Invoice                l_delivery_rate
# get_delivery_type(p_delivery_type_id)         The the delivery_type name                        l_delivery_type_name
# get_delivery_type_name(p_delivery_type_id)    Get the delivery_type name  (seems a duplicate)   l_delivery_type_name
# delivery_type_popup()                         View currencies and return seletec value          l_delivery_type.*
# delivery_type_create()                        Create a new delivery_type (record)               rv
# delivery_type_delete(p_delivery_type_id)      Delete a delivery_type                            NONE
# delivery_type_edit(p_delivery_type_id)        Edit a delivery_type                              NONE
# delivery_type_view_by_rec(p_delivery_type)    VIEW delivery type data onform                    NONE
# delivery_type_view(p_delivery_type_id)        VIEW delivery type data onform with ID            NONE
# delivery_type_input(p_delivery_type)          Insert new delivery_type (subfunction of create)  l_delivery_type.*
# delivery_type_combo_list(cb_field_name)       Populates combo list with values from DB          NONE
# get_delivery_type_id(p_delivery_type_name)    get the delivery_type id form the name            l_delivery_type_id
#
#
#
##################################################




##################################################
# GLOBALS
##################################################
GLOBALS "globals.4gl"

####################################################
# FUNCTION grid_header_delivery_type_scroll()
####################################################
FUNCTION grid_header_delivery_type_scroll()
  CALL fgl_grid_header("sa_delivery_type_scroll","delivery_type_id",get_str(1671),"right","F13")  --"Del. Type ID:"
  CALL fgl_grid_header("sa_delivery_type_scroll","delivery_type_name",get_str(1672),"left","F14") --delivery_type:
  CALL fgl_grid_header("sa_delivery_type_scroll","delivery_rate",get_str(1673),"left","F15")      --Delivery Rate:
END FUNCTION


#######################################################
# FUNCTION populate_delivery_type_form_labels_g()
#
# Populate delivery type form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_delivery_type_form_labels_g()
  DISPLAY get_str(1660) TO lbTitle
  DISPLAY get_str(1661) TO dl_f1
  DISPLAY get_str(1662) TO dl_f2
  #DISPLAY get_str(1663) TO dl_f3
  #DISPLAY get_str(1664) TO dl_f4
  #DISPLAY get_str(1665) TO dl_f5
  #DISPLAY get_str(1666) TO dl_f6
  #DISPLAY get_str(1667) TO dl_f7
  #DISPLAY get_str(1668) TO dl_f8
  DISPLAY get_str(1669) TO lbInfo1


  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL fgl_settitle(get_str(1650))  --"Delivery Type")

END FUNCTION


#######################################################
# FUNCTION populate_delivery_type_form_labels_t()
#
# Populate delivery type form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_delivery_type_form_labels_t()
  DISPLAY get_str(1660) TO lbTitle
  DISPLAY get_str(1661) TO dl_f1
  DISPLAY get_str(1662) TO dl_f2
  #DISPLAY get_str(1663) TO dl_f3
  #DISPLAY get_str(1664) TO dl_f4
  #DISPLAY get_str(1665) TO dl_f5
  #DISPLAY get_str(1666) TO dl_f6
  #DISPLAY get_str(1667) TO dl_f7
  #DISPLAY get_str(1668) TO dl_f8
  DISPLAY get_str(1669) TO lbInfo1
END FUNCTION


#######################################################
# FUNCTION populate_delivery_type_list_form_labels_t()
#
# Populate delivery type list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_delivery_type_list_form_labels_t()
  DISPLAY get_str(1670) TO lbTitle
  DISPLAY get_str(1671) TO dl_f1
  DISPLAY get_str(1672) TO dl_f2
  DISPLAY get_str(1673) TO dl_f3
  #DISPLAY get_str(1674) TO dl_f4
  #DISPLAY get_str(1675) TO dl_f5
  #DISPLAY get_str(1676) TO dl_f6
  #DISPLAY get_str(1677) TO dl_f7
  #DISPLAY get_str(1678) TO dl_f8
  DISPLAY get_str(1679) TO lbInfo1
END FUNCTION


#######################################################
# FUNCTION populate_delivery_type_list_form_labels_g()
#
# Populate delivery type list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_delivery_type_list_form_labels_g()
  DISPLAY get_str(1670) TO lbTitle
  #DISPLAY get_str(1671) TO dl_f1
  #DISPLAY get_str(1672) TO dl_f2
  #DISPLAY get_str(1673) TO dl_f3
  #DISPLAY get_str(1674) TO dl_f4
  #DISPLAY get_str(1675) TO dl_f5
  #DISPLAY get_str(1676) TO dl_f6
  #DISPLAY get_str(1677) TO dl_f7
  #DISPLAY get_str(1678) TO dl_f8
  DISPLAY get_str(1679) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  DISPLAY get_str(819) TO bt_new
  DISPLAY "!" TO bt_new

  CALL grid_header_delivery_type_scroll()
  CALL fgl_settitle(get_str(1650))

END FUNCTION

