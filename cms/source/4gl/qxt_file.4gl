##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


###############################################################
# FUNCTION file_download(p_server_filename,p_client_filename, p_dialog,p_file_ext)
#
# Transfers a file from the server to the client with validation
#
# RETURN doc_name
###############################################################
FUNCTION file_download(p_server_filename,p_client_filename, p_dialog,p_file_ext)
  DEFINE 
    p_server_filename VARCHAR(200),
    p_client_filename VARCHAR(200),
    p_dialog          SMALLINT,
    p_file_ext        VARCHAR(30),
    l_base_filename   VARCHAR(50),
    p_doc_name   VARCHAR(200),  
    doc_name     VARCHAR(200), --doc_name is the document name without  any path information
    local_debug  SMALLINT,
    err_msg      VARCHAR(200),
    success      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "file_download() - p_client_filename =", p_client_filename
    DISPLAY "file_download() - fgl_getproperty(\"gui\",\"system.file.exists\",p_client_filename)=", fgl_getproperty("gui","system.file.exists",p_client_filename)
    DISPLAY "file_download() - fgl_test(\"e\",p_server_filename)=", fgl_test("e",p_server_filename)
      IF fgl_test("e",p_server_filename) THEN
        DISPLAY "file does exist - File:", p_server_filename
      ELSE
        DISPLAY "file does NOT exist - File:", p_server_filename
      END IF
    DISPLAY "file_download() - p_dialog =", p_dialog
    DISPLAY "file_download() - p_file_ext =", p_file_ext

  END IF



  #Test, if server file actually exists
  IF NOT fgl_test("e",p_server_filename) THEN
    #The specified document does not exist on the server
    LET err_msg = get_str_tool(673) CLIPPED, "\n", get_str_tool(673) CLIPPED,"\nFilename: ", p_server_filename
    #File does not exist - 
    CALL fgl_winmessage(get_str_tool(672),err_msg,"error")
    RETURN 0
  END IF

  IF local_debug THEN
    DISPLAY "file_download() - fgl_getproperty(\"gui\",\"system.file.exists\",p_client_filename)=", fgl_getproperty("gui","system.file.exists",p_client_filename)
    DISPLAY "file_download() - fgl_test(\"e\",p_server_filename)=", fgl_test("e",p_server_filename)
  END IF

  #In case a file dialog box was specified (argument p_dialog)
  IF p_dialog THEN
    LET p_file_ext = "File (*.", trim(p_file_ext), ")||"

    #"Please select the file"
    CALL fgl_file_dialog("save", 0, "Specify the target (client) file path", p_client_filename, p_client_filename, p_file_ext)
      RETURNING p_client_filename
  ELSE
    LET p_client_filename = get_client_temp_path(p_client_filename)
  END IF


  #If the user cancels, do NOT download....
  IF p_client_filename IS NOT NULL THEN

    #Download the file from the server to the client
    LET p_client_filename = fgl_download(p_server_filename, p_client_filename)

    IF local_debug THEN
      DISPLAY "file_download() - p_server_filename=", p_server_filename
      DISPLAY "file_download() - p_client_filename=", p_client_filename
    END IF

    #check if file exists on client machine
    IF fgl_getproperty("gui","system.file.exists",p_client_filename) =  "false" THEN
      #The specified document does not exist on the server
      LET err_msg = get_str_tool(673) CLIPPED, "\n", get_str_tool(673) CLIPPED, p_doc_name
      CALL fgl_winmessage(get_str_tool(672),err_msg,"error")
    END IF
 
    IF local_debug THEN
      DISPLAY "file_download() - fgl_getproperty(\"gui\",\"system.file.exists\",p_client_filename)=", fgl_getproperty("gui","system.file.exists",p_client_filename)
      DISPLAY "file_download() - fgl_test(\"e\",p_server_filename)=", fgl_test("e",p_server_filename)
    END IF

  END IF

  RETURN p_client_filename

END FUNCTION


{
######################################################
# FUNCTION upload_blob_document(p_contact_id, p_contact_name,remote_image_path)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION upload_blob_document(p_client_file,p_dialog,p_file_ext)
  DEFINE 
    p_document_id       LIKE document.document_id,
    l_document_file     OF t_document_file,
    local_debug         SMALLINT,
    p_client_file       VARCHAR(250),
    l_server_file       VARCHAR(250),
    p_dialog            SMALLINT,
    p_file_ext          VARCHAR(30)  
    p_server_file_blob  BYTE

  LET local_debug = FALSE

  IF p_dialog THEN

    LET p_file_ext = "File (*.", trim(p_file_ext), "||"

    #"Please select the file"
    CALL fgl_file_dialog("open", 0, get_str(273), p_client_file, p_client_file, p_file_ext)
      RETURNING p_client_file
  END IF

  IF p_client_file IS NOT NULL THEN
    # This will copy the file to the CWD. Hubert can tidy this up later
    LET l_server_file = get_server_blob_temp_path(fgl_basename(p_client_file))

    IF NOT fgl_upload(p_client_file, l_server_file) OR NOT fgl_test("e", l_server_file) THEN
      LET l_server_file = ""
    END IF
  END IF


  IF local_debug THEN
    DISPLAY "upload_blob_document() - p_client_file_path=", p_client_file
    DISPLAY "upload_blob_document() - l_server_file_path=", l_server_file
  END IF


  RETURN l_server_file


END FUNCTION

}


