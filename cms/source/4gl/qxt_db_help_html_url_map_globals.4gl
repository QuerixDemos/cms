##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

database cms

GLOBALS


#icon_category TYPE
  DEFINE t_qxt_help_url_map_rec TYPE AS
    RECORD
      help_url_map_id       LIKE qxt_help_url_map.map_id,
      language_id           LIKE qxt_help_url_map.language_id,
      help_url_map_fname    LIKE qxt_help_url_map.map_fname,
      help_url_map_ext      LIKE qxt_help_url_map.map_ext

    END RECORD

  DEFINE t_qxt_help_url_map_form_rec TYPE AS
    RECORD
      help_url_map_id       LIKE qxt_help_url_map.map_id,
      language_name         LIKE qxt_language.language_name,
      help_url_map_fname    LIKE qxt_help_url_map.map_fname,
      help_url_map_ext      LIKE qxt_help_url_map.map_ext

    END RECORD


END GLOBALS






