##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################


####################################################
# FUNCTION grid_header_pos_type_scroll()
#
# Populate grid header of position type
#
# RETURN NONE
####################################################
FUNCTION grid_header_pos_type_scroll()
  CALL fgl_grid_header("sa_pos_type","type_id",get_str(2041),"right","F13")   --"Type ID:"
  CALL fgl_grid_header("sa_pos_type","ptype_name",get_str(2042),"left","F14") --"Position:"
  CALL fgl_grid_header("sa_pos_type","user_def",get_str(2043),"left","F15")   --"User Def.:"
END FUNCTION


#######################################################
# FUNCTION populate_pos_type_form_labels_t()
#
# Populate position type form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_pos_type_form_labels_t()
  DISPLAY get_str(2030) TO lbTitle
  DISPLAY get_str(2031) TO dl_f1
  DISPLAY get_str(2032) TO dl_f2
  DISPLAY get_str(2033) TO dl_f3
  #DISPLAY get_str(2034) TO dl_f4
  #DISPLAY get_str(2035) TO dl_f5
  #DISPLAY get_str(2036) TO dl_f6
  #DISPLAY get_str(2037) TO dl_f7
  #DISPLAY get_str(2038) TO dl_f8
  DISPLAY get_str(2039) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_pos_type_form_labels_g()
#
# Populate position type form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_pos_type_form_labels_g()
  DISPLAY get_str(2030) TO lbTitle
  DISPLAY get_str(2031) TO dl_f1
  DISPLAY get_str(2032) TO dl_f2
  DISPLAY get_str(2033) TO dl_f3
  #DISPLAY get_str(2034) TO dl_f4
  #DISPLAY get_str(2035) TO dl_f5
  #DISPLAY get_str(2036) TO dl_f6
  #DISPLAY get_str(2037) TO dl_f7
  #DISPLAY get_str(2038) TO dl_f8
  DISPLAY get_str(2039) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel


  CALL fgl_settitle(get_str(2030))

END FUNCTION



#######################################################
# FUNCTION populate_pos_type_list_form_labels_t()
#
# Populate position type list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_pos_type_list_form_labels_t()
  DISPLAY get_str(2040) TO lbTitle
  DISPLAY get_str(2041) TO dl_f1
  DISPLAY get_str(2042) TO dl_f2
  DISPLAY get_str(2043) TO dl_f3
  #DISPLAY get_str(2044) TO dl_f4
  #DISPLAY get_str(2045) TO dl_f5
  #DISPLAY get_str(2046) TO dl_f6
  #DISPLAY get_str(2047) TO dl_f7
  #DISPLAY get_str(2048) TO dl_f8
  DISPLAY get_str(2049) TO lbInfo1

END FUNCTION

#######################################################
# FUNCTION populate_pos_type_list_form_labels_g()
#
# Populate position type list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_pos_type_list_form_labels_g()
  DISPLAY get_str(2040) TO lbTitle
  #DISPLAY get_str(2041) TO dl_f1
  #DISPLAY get_str(2042) TO dl_f2
  #DISPLAY get_str(2043) TO dl_f3
  #DISPLAY get_str(2044) TO dl_f4
  #DISPLAY get_str(2045) TO dl_f5
  #DISPLAY get_str(2046) TO dl_f6
  #DISPLAY get_str(2047) TO dl_f7
  #DISPLAY get_str(2048) TO dl_f8
  DISPLAY get_str(2049) TO lbInfo1

  DISPLAY get_str(810) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(822) TO bt_add
  DISPLAY "!" TO bt_add

  DISPLAY get_str(818) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str(817) TO bt_delete
  DISPLAY "!" TO bt_delete

  CALL fgl_settitle(get_str(2040))
  CALL grid_header_pos_type_scroll()

END FUNCTION

