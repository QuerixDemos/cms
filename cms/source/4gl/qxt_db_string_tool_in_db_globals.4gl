##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

################################################################################
# Tool String
#
# Globals file for the string libary 
# Strings are located and accessed from the database
#
################################################################################


################################################################################
# DATABASE
################################################################################
DATABASE cms

################################################################################
# GLOBALS
################################################################################
GLOBALS

{
  DEFINE
    t_qxt_string_tool_rec TYPE AS  
      RECORD
        string_id      SMALLINT,
        language_id    SMALLINT,
        string_data    SMALLINT
      END RECORD
}
END GLOBALS
