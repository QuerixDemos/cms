##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings Main
#
# This 4gl module includes functions to change the environment i.e. language choice
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_main_edit()
#
# Edit main settings (main configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_main_edit()
  DEFINE 
    l_settings_main_rec  OF t_qxt_settings_main_form_rec

  #Open Window
  CALL fgl_window_open("w_settings_main",5,5,get_form_path("f_qxt_settings_main_l2"),FALSE)
  CALL qxt_settings_main_view_form_details()

 		CALL ui.Interface.setImage("qx://application/icon16/basic/gears.png")
		CALL ui.Interface.setText("Settings")  
		

  #Get the corresponding record
  CALL get_settings_main_form_rec()
    RETURNING  l_settings_main_rec.*

  #Input
  INPUT BY NAME l_settings_main_rec.* WITHOUT DEFAULTS  HELP 1

    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)
  
    ON ACTION("settingsSave")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_main_form_rec(l_settings_main_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_main_form_rec(l_settings_main_rec.*) --Write to memory
      CALL process_main_cfg_export(NULL) --Write to disk
      EXIT INPUT


    ON KEY(F1011)
      CALL fgl_settitle(get_str_tool(110))
      NEXT FIELD application_name

    ON KEY(F1012)
      CALL fgl_settitle(get_str_tool(120))
      NEXT FIELD cfg_path


    ON KEY(F1013)
      CALL fgl_settitle(get_str_tool(130))
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      CALL fgl_settitle(get_str_tool(140))
      NEXT FIELD language_cfg_filename

    ON KEY(F1015)
      CALL fgl_settitle(get_str_tool(150))
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_main_form_rec(l_settings_main_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  #Set toolbar
  #CALL publish_toolbar("settingsCfg",1)


  IF NOT int_flag THEN
    CALL copy_settings_main_form_rec(l_settings_main_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_main")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_main_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_main_view_form_details()
  CALL updateUILabel("cntTpApplication","Application")
  CALL updateUILabel("cntTpGeneralPath","General Path")
  CALL updateUILabel("cntTpTempPath","Temp Path")
  CALL updateUILabel("cntTpCfgFiles","Cfg Files")
  CALL updateUILabel("cntTpDb","DB")
  CALL UIRadButListItemText("displayKeyInstructions",1,"Off - Keyboard instructions are not displayed")
  CALL UIRadButListItemText("displayKeyInstructions",2,"On - Keyboard Instructions will be shown")
  CALL UIRadButListItemText("displayUsageInstructions",1,"Off - Usage instructions are not shown")
  CALL UIRadButListItemText("displayUsageInstructions",2,"On - Usage instructions are shown")
  CALL fgl_settitle(get_str_tool(110))

  #Application
  DISPLAY get_str_tool(111) TO dl_f11 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(112) TO dl_f12 ATTRIBUTE(BLUE, BOLD)

  #DB
  DISPLAY get_str_tool(116) TO dl_f16 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(117) TO dl_f17 ATTRIBUTE(BLUE, BOLD)

  #General Path
  DISPLAY get_str_tool(121) TO dl_f21 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(122) TO dl_f22 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(123) TO dl_f23 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(124) TO dl_f24 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(125) TO dl_f25 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(126) TO dl_f26 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(127) TO dl_f27 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(128) TO dl_f28 ATTRIBUTE(BLUE, BOLD)

  #Temp path
  DISPLAY get_str_tool(131) TO dl_f31 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(132) TO dl_f32 ATTRIBUTE(BLUE, BOLD)

  #CFG files

  DISPLAY get_str_tool(141) TO dl_f41 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(142) TO dl_f42 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(143) TO dl_f43 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(144) TO dl_f44 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(145) TO dl_f45 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(146) TO dl_f46 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(147) TO dl_f47 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(148) TO dl_f48 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(149) TO dl_f49 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(150) TO dl_f50 ATTRIBUTE(BLUE, BOLD)

  CALL application_name_combo_list("application_name")
END FUNCTION

###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_main_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_form
  

  ##################################
  # Main configuration file
  ##################################

  #[Application] Section
  LET l_settings_form.application_name               = get_application_name(qxt_settings.application_id) --directory name for config files
  LET l_settings_form.main_cfg_filename              = qxt_settings.main_cfg_filename            --directory name for config files
  LET l_settings_form.displayKeyInstructions         = qxt_settings.displayKeyInstructions				--key instructions
  LET l_settings_form.application_name               = qxt_settings.displayUsageInstructions			--usage insructions    

  #[General] Section
  LET l_settings_form.cfg_path                       = qxt_settings.cfg_path                     --directory name for config files
  LET l_settings_form.unl_path                       = qxt_settings.unl_path                     --directory name for unl files
  LET l_settings_form.form_path                      = qxt_settings.form_path                    --directory name for forms
  LET l_settings_form.msg_path                       = qxt_settings.msg_path                     --directory name for forms
  LET l_settings_form.document_path                  = qxt_settings.document_path                --directory name for documents
  LET l_settings_form.html_path                      = qxt_settings.html_path                    --directory name for html documents
  LET l_settings_form.image_path                     = qxt_settings.image_path                   --directory name for normal images
  LET l_settings_form.print_html_template_path       = qxt_settings.print_html_template_path     --directory name for normal images

  #[TempPath] Section
  LET l_settings_form.server_app_temp_path           = qxt_settings.server_app_temp_path         --directory name for 16x16 icons
  LET l_settings_form.server_blob_temp_path          = qxt_settings.server_blob_temp_path        --directory name for 16x16 icons

  #[File-Path]  --specific cfg files 
  LET l_settings_form.main_cfg_filename              = qxt_settings.main_cfg_filename            --main configuration file name & Path    default: cfg/config.cfg
  LET l_settings_form.toolbar_cfg_filename           = qxt_settings.toolbar_cfg_filename         --toolbar configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.icon_cfg_filename              = qxt_settings.icon_cfg_filename            --Icon configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.help_classic_cfg_filename      = qxt_settings.help_classic_cfg_filename    --Help Classic configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.help_html_cfg_filename         = qxt_settings.help_html_cfg_filename       --Help Html configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.language_cfg_filename          = qxt_settings.language_cfg_filename        --Language configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.string_cfg_filename            = qxt_settings.string_cfg_filename          --String configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.print_html_cfg_filename        = qxt_settings.print_html_cfg_filename      --Print Html configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.dde_cfg_filename               = qxt_settings.dde_cfg_filename             --DDE configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.online_resource_cfg_filename   = qxt_settings.online_resource_cfg_filename --Online Resource configuration/ini unl file name  default: toolbar.cfg
  LET l_settings_form.webservice_cfg_filename        = qxt_settings.webservice_cfg_filename      --WebService configuration/ini unl file name  default: toolbar.cfg


  #[Database] Section
  LET l_settings_form.db_name_tool                   = qxt_settings.db_name_tool                 --Datbase Name for the QXT Library
  LET l_settings_form.db_name_app                    = qxt_settings.db_name_app                  --Datbase Name for this application

  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_main_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_main_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_main_form_rec
  
  ##################################
  # Main configuration file
  ##################################

  #[Application] Section
  LET qxt_settings.application_id                   = get_application_id(p_settings_form_rec.application_name) --directory name for config files
  LET qxt_settings.displayKeyInstructions           = p_settings_form_rec.displayKeyInstructions			--directory name for config files
  LET qxt_settings.displayUsageInstructions         = p_settings_form_rec.displayUsageInstructions		--directory name for config files

  #[GeneralPath] Section
  LET qxt_settings.cfg_path                         = p_settings_form_rec.cfg_path                     --directory name for config files
  LET qxt_settings.unl_path                         = p_settings_form_rec.unl_path                     --directory name for unl files
  LET qxt_settings.form_path                        = p_settings_form_rec.form_path                    --directory name for forms
  LET qxt_settings.msg_path                         = p_settings_form_rec.msg_path                     --directory name for forms
  LET qxt_settings.document_path                    = p_settings_form_rec.document_path                --directory name for documents
  LET qxt_settings.html_path                        = p_settings_form_rec.html_path                    --directory name for html documents
  LET qxt_settings.image_path                       = p_settings_form_rec.image_path                   --directory name for normal images
  LET qxt_settings.print_html_template_path         = p_settings_form_rec.print_html_template_path     --directory name for normal images

  #[TempPath] Section
  LET qxt_settings.server_app_temp_path             = p_settings_form_rec.server_app_temp_path         --directory name for 16x16 icons
  LET qxt_settings.server_blob_temp_path            = p_settings_form_rec.server_blob_temp_path        --directory name for 16x16 icons

  #[FilePath]  --specific cfg files 
  LET qxt_settings.main_cfg_filename                = p_settings_form_rec.main_cfg_filename            --main configuration file name & Path    default: cfg/config.cfg
  LET qxt_settings.toolbar_cfg_filename             = p_settings_form_rec.toolbar_cfg_filename         --toolbar configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.icon_cfg_filename                = p_settings_form_rec.icon_cfg_filename            --Icon configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_classic_cfg_filename        = p_settings_form_rec.help_classic_cfg_filename    --Help Classic configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.help_html_cfg_filename           = p_settings_form_rec.help_html_cfg_filename       --Help Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.language_cfg_filename            = p_settings_form_rec.language_cfg_filename        --Language configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.string_cfg_filename              = p_settings_form_rec.string_cfg_filename          --String configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.print_html_cfg_filename          = p_settings_form_rec.print_html_cfg_filename      --Print Html configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.dde_cfg_filename                 = p_settings_form_rec.dde_cfg_filename             --DDE configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.online_resource_cfg_filename     = p_settings_form_rec.online_resource_cfg_filename --Online Resource configuration/ini unl file name  default: toolbar.cfg
  LET qxt_settings.webservice_cfg_filename          = p_settings_form_rec.webservice_cfg_filename      --WebService configuration/ini unl file name  default: toolbar.cfg


  #[Database] Section
  LET qxt_settings.db_name_tool                     = p_settings_form_rec.db_name_tool                 --Datbase Name for the QXT Library
  LET qxt_settings.db_name_app                      = p_settings_form_rec.db_name_app                  --Datbase Name for this application


END FUNCTION




####################################################################################################
# EOF
####################################################################################################



