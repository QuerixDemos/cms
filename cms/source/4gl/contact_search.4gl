##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

###############################################################################################################
# Search functions
###############################################################################################################
#
# FUNCTION:                                          DESCRIPTION:                                           RETURN
# contact_main(p_contact_id, p_company_id)           Main Contact Menu                                      NONE
# contact_query()                                    Search for contact                                     rv (rv LIKE contact.cont_id)
# query_contact()                                    Search for contact                                     cont_arr[i].cont_id or NULL
# contact_create(p_company_id,p_window)              Create a new contact record                            sqlca.sqlerrd[2]
#                                                    p_window defines if a window should be opened
# contact_input(l_contact.*)                         Crate new contact function                             p_contact.*   (p_contact RECORD LIKE contact.*)
# contact_choose(p_company_id)                       (Not used) ..Select contact in window                  l_contact_id or NULL
# contact_edit(p_contact_id)                         Edit Contact                                           NONE
# contact_delete(p_contact_id)                       Delete Contact                                         rv  (rv SMALLINT Boolean)
# contact_show(p_contact_id)                         Display contact data to form                           NONE
# contact_view(p_contact_id)                         Displays a window with contact details                 NONE
# contact_popup                                      Popup contact selection window                         l_contact[i].cont_id or NULL
# (p_cont_id,p_order_field,p_accept_action)
# contact_to_form_contact(p_contact)                 ??                                                     l_contact_rec.*
# contact_activities_populate_grid_panel_data_source Data Source (cursor) for grid_panel                    NONE
#   ((p_contact_id,p_order_field)
# contact_activities_populate_grid_panel
#   (p_contact_id,p_order_field,p_scroll)            Display activities in screen array                     NONE
# populate_contact_form_combo_boxes()                populates all combo boxes dynamically from dB          NONE
# download_contact_image                             Download a DB BLOB(server side) located image          NONE
#  (p_contact_id, p_contact_name,remote_image_path)  to the client file system
# set_cont_default_titlebar()                        Labels the titlebar with the default label             NONE
# web_get_contact_rec(p_contact_id)                      get contact record from contact_id                     l_contact.*
# get_contact_address_rec(p_contact_id)              Get the contact address record from an id              l_contact.*
#                                                    Note: This function is also published as a webservice
# get_contact_name(p_contact_id)                     returns the contact name based on the contact_id       r_contact_name
# get_contact_lname(p_contact_id)                    Get the contact lname from an id                       r_contact_name
# get_contact_fname(p_contact_id)                    Get the contact fname from an id                       r_contact_name
# get_contact_email_address(p_contact_id)            Get the corresponding email address from a contact_id  r_contact_name
# get_form_contact(p_cont_id)                        ??                                                     l_contact_rec.*
# advanced_contact_lookup()                          Allows to search by entering the first letters         rv_cont_id
# update_advanced_contact_lookup                     sub-routine of advanced_contact_lookup to              rv_cont_id (LIKE contact.cont_id)
# (query_filter,field_filter,do_choose)              update display                     
# print_contact_html(p_company_id)                   Prints company details using a dynamically             NONE
#                                                    created html page.
# grid_header_contact_scroll()                       Populate grid header for scroll                        NONE
# grid_header_open_act_scroll()                      Populate grid header for open activity scroll          NONE
# grid_header_contact_scroll_advanced()              Populate grid header for advanced search               NONE
##################################################################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"


###################################################
# FUNCTION contactSearch(pFieldSearch)
#
# This function was implemented for a general/constantly available search tool located in the top of the window
# Search string will query all major fields of the contact record
#
# Return lContactId LIKE contact.cont_id
###################################################
FUNCTION contactSearch(pFieldSearch)
DEFINE pFieldSearch STRING
DEFINE lContactId LIKE contact.cont_id
DEFINE sqlQuery STRING
DEFINE errMsg STRING
#DEFINE dbWildcard CHAR
LET pFieldSearch = trim(pFieldSearch)

IF length(pFieldSearch) > 30 THEN
LET pFieldSearch = pFieldSearch[1,30]
LET errMsg = "Search String is limited to 30 characters\nYour search string will be trimmed to 30 characters\n", trim(pFieldSearch)
CALL fgl_winmessage("Search String is too long",errMsg,"info")

END IF

IF length(pFieldSearch)> 0 THEN
IF pFieldSearch[length(pFieldSearch)] = "\n" OR pFieldSearch[length(pFieldSearch)] = "\r" THEN
LET pFieldSearch = pFieldSearch[1,length(pFieldSearch)-1]
END IF
IF pFieldSearch[length(pFieldSearch)] != get_dbWildcard() THEN
LET pFieldSearch = pFieldSearch, get_dbWildcard()
END IF
END IF

#LET dbWildcard = get_dbWildcard()


	LET sqlQuery = 
		"SELECT ",
#		"cont_id ",
    "contact.cont_id, ",
    "contact.cont_name, ",
		"contact.cont_fname, ",
    "contact.cont_lname, ",
    "company.comp_name, ",
		"contact.cont_phone ",


 
		"FROM contact, company ",
		"WHERE contact.cont_org = company.comp_id " ,
#		"WHERE	contact.cont_id 	LIKE pFieldSearch ",
		" AND ( ",
		"				contact.cont_title LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_name LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_fname LIKE \"", pFieldSearch, "\" ",		
		"		OR	contact.cont_lname LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr1 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr2 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_addr3 LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_city LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_zone LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_zip LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_country LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_phone LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_fax LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_mobile LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_email LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_dept LIKE \"", pFieldSearch, "\" ",
#		"		OR	contact.cont_org LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_position LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_ipaddr LIKE \"", pFieldSearch, "\" ",
		"		OR	contact.cont_notes LIKE \"", pFieldSearch, "\" ",
	
		
		"		OR	company.comp_name LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr1 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr2 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_addr3 LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_city LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_zone LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_zip LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_country LIKE \"", pFieldSearch, "\" ",
		"		OR	company.comp_url LIKE \"", pFieldSearch, "\" ",

		")"
	#WHENEVER ERROR STOP

#	DISPLAY "lContactId=",lContactId 

	LET lContactId = contactSearchProcess(sqlQuery)
	#"SELECT contact.cont_id, contact.cont_name, contact.cont_fname, contact.cont_lname, company.comp_name, contact.cont_phone FROM contact, company WHERE contact.cont_org = company.comp_id  AND ( 				contact.cont_title LIKE 'a%' 		OR	contact.cont_name LIKE 'a%' 		OR	contact.cont_fname LIKE 'a%' 		OR	contact.cont_lname LIKE 'a%' 		OR	contact.cont_addr1 LIKE 'a%' 		OR	contact.cont_addr2 LIKE 'a%' 		OR	contact.cont_addr3 LIKE 'a%' 		OR	contact.cont_city LIKE 'a%' 		OR	contact.cont_zone LIKE 'a%' 		OR	contact.cont_zip LIKE 'a%' 		OR	contact.cont_country LIKE 'a%' 		OR	contact.cont_phone LIKE 'a%' 		OR	contact.cont_fax LIKE 'a%' 		OR	contact.cont_mobile LIKE 'a%' 		OR	contact.cont_email LIKE 'a%' 		OR	contact.cont_dept LIKE 'a%' 		OR	contact.cont_position LIKE 'a%' 		OR	contact.cont_ipaddr LIKE 'a%' 		OR	contact.cont_notes LIKE 'a%' 		OR	company.comp_name LIKE 'a%' 		OR	company.comp_addr1 LIKE 'a%' 		OR	company.comp_addr2 LIKE 'a%' 		OR	company.comp_addr3 LIKE 'a%' 		OR	company.comp_city LIKE 'a%' 		OR	company.comp_zone LIKE 'a%' 		OR	company.comp_zip LIKE 'a%' 		OR	company.comp_country LIKE 'a%' 		OR	company.comp_url LIKE 'a%' )"
	RETURN lContactId 
		
	#RETURN lContactId     
END FUNCTION        



########################################################
# FUNCTION contact_query()
#
# RETURN rv     (rv LIKE contact.cont_id)
########################################################
FUNCTION contact_query()
  DEFINE rv LIKE contact.cont_id

    OPEN WINDOW c_win 
      AT 2,2 
      WITH FORM "form/f_contact_det_l2" 

    CALL fgl_settitle(get_str(260))  --Contact Search
    CALL displayKeyGuide(get_str(261))  --"Enter search criteria (wildcards */% are allowed)")


  #Toolbar
  CALL publish_toolbar("ContactSearchDetailed",0)

  LET rv = query_contact()

  #Toolbar
  CALL publish_toolbar("ContactSearchDetailed",1)

  CLOSE WINDOW c_win

  RETURN rv

END FUNCTION

######################################################
# FUNCTION query_contact()
#
# RETURN cont_arr[i].cont_id
######################################################
FUNCTION query_contact()
  DEFINE sql_stmt CHAR(1000)
  DEFINE where_clause CHAR(200)
  DEFINE i INTEGER
  DEFINE cont_arr DYNAMIC ARRAY OF t_cont_rec4

  #CALL displayKeyGuide("Esc-OK  Ctrl-C-Cancel  Wildcards (*/&) can be used. Leave fields empty to see the list")
  LET tmp_str = get_str(816) CLIPPED, "-",  get_str(800) CLIPPED, "   ", get_str(820) CLIPPED, "-", get_str(808) CLIPPED, "  ", get_str(261) CLIPPED
  CALL displayKeyGuide(tmp_str) -- "Esc-OK  Ctrl-C-Cancel  Wildcards (*/&) can be used. Leave fields empty to see the list")
  CALL displayUsageGuide(tmp_str) -- "Esc-OK  Ctrl-C-Cancel  Wildcards (*/&) can be used. Leave fields empty to see the list")

  CLEAR FORM

  CONSTRUCT BY NAME where_clause 
     ON contact.cont_name, 
        contact.cont_fname, 
        contact.cont_lname,
	contact.cont_addr1, 
        contact.cont_addr2, 
        contact.cont_addr3,
        contact.cont_city, 
        contact.cont_zone, 
        contact.cont_zip, 
        contact.cont_country, 
        contact.cont_phone, 
        contact.cont_fax, 
        contact.cont_mobile, 
        contact.cont_email,  
        company.comp_name
     HELP 207
    BEFORE CONSTRUCT
     CALL publish_toolbar("ContactSearchDetailed",0)
  END CONSTRUCT


  IF int_flag THEN
    LET int_flag = FALSE
    RETURN NULL
  END IF

  LET sql_stmt = "SELECT ",
                   "contact.cont_id, ",
                   "contact.cont_name, ",
		   "contact.cont_fname, ",
                   "contact.cont_lname, ",
                   "company.comp_name, ",
		   "contact.cont_phone ",
		 "FROM contact, ",
                   "OUTER company ",
		 "WHERE contact.cont_org = company.comp_id ",
                 "AND ",
		    where_clause

  PREPARE p1 FROM sql_stmt
  DECLARE curs_contact CURSOR FOR p1
  LET i = 1

  FOREACH curs_contact INTO cont_arr[i].*
    LET i = i + 1
    IF i > 50 THEN
      EXIT FOREACH
    END IF
  END FOREACH

  LET i = i - 1
  
	IF i > 0 THEN
		CALL cont_arr.resize(i)   --correct the last element of the dynamic array
	END IF  
		  
    #CALL set_count(i)

  IF NOT i THEN
    CALL fgl_message_box(get_str(262))  --No matching records found")
    RETURN NULL
  END IF

  IF i = 1 THEN
    RETURN cont_arr[i].cont_id
  ELSE
      OPEN WINDOW w_contact_scroll 
        AT 2,2 
        WITH FORM "form/f_contact_scroll_l2" 
        #ATTRIBUTE(BORDER,FORM LINE 2)

      CALL populate_contact_popup_form_labels_g()

      #CALL grid_header_contact_scroll()
      CALL fgl_settitle("List of all found contacts - Select")

    LET int_flag = FALSE

    DISPLAY ARRAY cont_arr TO sa_cont_scroll.*  
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 61

      BEFORE DISPLAY 
       CALL publish_toolbar("DetailedSearchList",0)


      ON KEY(F7)
        LET i = arr_curr()
        CALL contact_view(cont_arr[i].cont_id)
        LET int_flag = FALSE

      ON KEY (ACCEPT)
        EXIT DISPLAY

    END DISPLAY

    CLOSE WINDOW w_contact_scroll

    IF int_flag THEN
      LET int_flag = FALSE
      RETURN NULL
    END IF

    LET i = arr_curr()

    RETURN cont_arr[i].cont_id 
  END IF
END FUNCTION

######################################

FUNCTION contactSearchProcess(p_sql_stmt)
	DEFINE p_sql_stmt VARCHAR(2500)
	DEFINE cont_arr DYNAMIC ARRAY OF t_cont_rec4
	DEFINE i int



  PREPARE ph FROM p_sql_stmt
  DECLARE curs_contact_h CURSOR FOR ph
  LET i = 1

  FOREACH curs_contact_h INTO cont_arr[i].*
    LET i = i + 1
    #IF i > 50 THEN
    #  EXIT FOREACH
    #END IF
  END FOREACH

  LET i = i - 1
  
	IF i > 0 THEN
		CALL cont_arr.resize(i)   --correct the last element of the dynamic array
	END IF  
		  
    #CALL set_count(i)

  IF NOT i THEN
    CALL fgl_message_box(get_str(262))  --No matching records found")
    RETURN NULL
  END IF

  IF i = 1 THEN
    RETURN cont_arr[i].cont_id
  ELSE

      OPEN WINDOW w_contact_scroll 
        AT 2,2 
        WITH FORM "form/f_contact_scroll_l2" 
        #ATTRIBUTE(BORDER)

      CALL populate_contact_popup_form_labels_g()

      #CALL grid_header_contact_scroll()
      CALL fgl_settitle("List of all found contacts - Select")

    LET int_flag = FALSE

    DISPLAY ARRAY cont_arr TO sa_cont_scroll.*  
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 61

      BEFORE DISPLAY 
       CALL publish_toolbar("DetailedSearchList",0)


      ON KEY(F7)
        LET i = arr_curr()
        CALL contact_view(cont_arr[i].cont_id)
        LET int_flag = FALSE

      ON KEY (ACCEPT)
        EXIT DISPLAY

    END DISPLAY

    CLOSE WINDOW w_contact_scroll

    IF int_flag THEN
      LET int_flag = FALSE
      RETURN NULL
    END IF

    LET i = arr_curr()

    RETURN cont_arr[i].cont_id 
  END IF
END FUNCTION
####################################################
# FUNCTION advanced_contact_lookup(p_cont_id)
#
# Allows to search by entering the first letters
#
# RETURN rv_cont_id
####################################################
FUNCTION advanced_contact_lookup(p_cont_id)
  DEFINE 
    p_cont_id        LIKE contact.cont_id,
    f_filter         VARCHAR(255),
    l_filter         VARCHAR(255),
    l_key            SMALLINT,
    len              INTEGER,
    do_choose        SMALLINT,
    rv_cont_id       LIKE contact.cont_id,
    l_scroll_cont_id LIKE contact.cont_id,
    l_scroll_row     SMALLINT

  # f_filter is what we display to the lookup field
  # l_filter is what we use in the query (ie, f_filter, "%")

  LET rv_cont_id = 0

  LET l_filter = "%"
  LET f_filter = ""
  LET do_choose = FALSE
  LET l_scroll_row = 1

  #Auto Grid scroll to find the last row number
  IF p_cont_id IS NOT NULL THEN
    LET l_scroll_cont_id = p_cont_id
  END IF

    CALL fgl_window_open("w_advanced_lookup_contact", 2, 2, get_form_path("f_contact_scroll_advanced_l2"), FALSE)

    CALL grid_header_contact_scroll_advanced()
    CALL fgl_settitle(get_str(63))  --"Quick Search... Type first letters followed by Accept")


  #Toolbar
  CALL publish_toolbar("ContactSearchQuickGlobal",0)

  WHILE TRUE
    CALL update_advanced_contact_lookup(l_filter, f_filter, do_choose)
      RETURNING rv_cont_id

    IF rv_cont_id > 0 THEN
      EXIT WHILE
    END IF

    LET do_choose = FALSE

    CALL fgl_getkey()
      RETURNING l_key

    CASE 
      WHEN l_key = fgl_keyval("HELP")
        CALL showhelp(114)

      WHEN l_key = fgl_keyval("CANCEL")
        EXIT WHILE


      WHEN l_key = fgl_keyval("ACCEPT")
        LET do_choose = TRUE

      WHEN l_key = fgl_keyval("RETURN")
        LET do_choose = TRUE

      WHEN l_key = fgl_keyval("backspace")
        LET len = LENGTH(f_filter) - 1
        IF len >= 1 THEN
          LET f_filter = f_filter[1,len]
          LET l_filter = f_filter, "%"
        ELSE 
          LET f_filter = ""
          LET l_filter = f_filter, "%"
        END IF

      WHEN (l_key >= ORD("0") AND l_key <= ORD("9"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("a") AND l_key <= ORD("z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

      WHEN (l_key >= ORD("A") AND l_key <= ORD("Z"))
        LET f_filter = f_filter, ASCII(l_key)
        LET l_filter = f_filter, "%"

    END CASE
  END WHILE

  #Toolbar
  CALL publish_toolbar("ContactSearchGlobal",0)

  CALL fgl_window_close("w_advanced_lookup_contact")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_cont_id
  ELSE
    RETURN rv_cont_id
  END IF

END FUNCTION


####################################################
# FUNCTION  update_advanced_contact_lookup(query_filter, field_filter, do_choose)
#
# sub-routine of advanced_contact_lookup to update display
#
# RETURN rv_cont_id  (LIKE contact.cont_id)
####################################################
FUNCTION update_advanced_contact_lookup(query_filter, field_filter, do_choose)
  DEFINE 
    query_filter VARCHAR(255), 
    field_filter VARCHAR(255),   
    i            INTEGER,
    l_name_arr   DYNAMIC ARRAY OF t_cont_rec3,
    do_choose    SMALLINT,
    rv_cont_id   LIKE contact.cont_id,
    local_debug  SMALLINT

  LET local_debug = 0  --0=off 1=on
  LET rv_cont_id = 0

  DECLARE c_test_lookup CURSOR FOR 
    SELECT contact.cont_id, contact.cont_name , contact.cont_fname,contact.cont_lname,company.comp_name
      FROM contact, company
      WHERE UPPER(contact.cont_name) LIKE UPPER(query_filter)
      AND contact.cont_org = company.comp_id
      ORDER BY contact.cont_name ASC


  DISPLAY field_filter TO f_lookup

  LET i = 0

  FOREACH c_test_lookup INTO l_name_arr[i + 1].*
    LET i = i + 1
  END FOREACH

	LET i = i-1

	If i > 0 THEN
		CALL 	l_name_arr.resize(i)  -- resize dynamic array to remove last dirty element
	END IF
	
 # CALL set_count(i)
  
  LET int_flag = FALSE

  IF do_choose THEN
    DISPLAY ARRAY l_name_arr TO advanced_lookup_arr.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")  HELP 61

      BEFORE DISPLAY 
        CALL publish_toolbar("ContactSearchQuickList",0)
    END DISPLAY



    IF NOT int_flag THEN
      LET i = arr_curr()
      LET rv_cont_id = l_name_arr[i].cont_id
    END IF
  ELSE
    DISPLAY ARRAY l_name_arr TO advanced_lookup_arr.* WITHOUT SCROLL
  END IF

  IF local_debug THEN
    DISPLAY "update_advanced_contact_lookup() query_filter= ", query_filter
    DISPLAY "update_advanced_contact_lookup() field_filter= ", field_filter
    DISPLAY "update_advanced_contact_lookup() do_choose= ", do_choose
    DISPLAY "update_advanced_contact_lookup() rv_cont_id= ",rv_cont_id
  END IF

  RETURN rv_cont_id

END FUNCTION





######################################################
# FUNCTION contact_choose(p_company_id)
#
# RETURN l_contact_id
######################################################
FUNCTION contact_choose(p_company_id)
  DEFINE 
    p_company_id LIKE company.comp_id,
    l_contact_id LIKE contact.cont_id,
    cont_arr     DYNAMIC ARRAY OF RECORD LIKE contact.*,
    i            SMALLINT

  DECLARE c_contact CURSOR FOR 
    SELECT * 
    FROM contact
    WHERE contact.cont_ord = p_company_id
  FOREACH c_contact INTO cont_arr[i].*
    LET i = i + 1
    IF i > 50 THEN
      EXIT FOREACH
    END IF
  END FOREACH

  IF i = 1 THEN
    IF question_box(get_str(272)) THEN  --"No contacts exist for this company. Create new contact?"
      LET l_contact_id = contact_create(p_company_id,FALSE)  --FALSE= don[t open a window
      RETURN l_contact_id
    ELSE
      RETURN NULL
    END IF
  END IF

    CALL fgl_window_open("w_sel_cont", 2,2, get_form_path("f_scroll_l2"), FALSE)
    CALL populate_contact_popup_form_labels_g()

  #CALL set_count(i-1)
		LET i = i-1
		If i > 0 THEN
			CALL cont_arr.resize(i)  --remove the last item which has no data
		END IF

  LET int_flag = FALSE

  DISPLAY ARRAY cont_arr TO sa_contact.* HELP 61

  IF int_flag THEN
    LET int_flag = FALSE
    LET l_contact_id = NULL
  END IF

  LET i = arr_curr()
  LET l_contact_id = cont_arr[i].cont_id

  RETURN l_contact_id
END FUNCTION



