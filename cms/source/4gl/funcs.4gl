##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#################################################################################################################
# General tools / functions
#################################################################################################################
#
# FUNCTION:                        DESCRIPTION:                                        RETURN:
# displayKeyGuide(str)               Displays user instructions to field                 NONE
# popup_alert()                    Displays 'F5' for popup... message in statusbar     NONE
# load_main_menu()                 Loads the MAIN menu text                            NONE
# question_box(msg)                Question dialog box with the options yes no cancel  BOOLEAN TRUE/FALSE
# yes_no(msg)                      Question dialog box with the options yes and no     BOOLEAN TRUE/FALSE
# show_picture(p_contact_id)       ??????????????????                                  NONE
# exist(p_val)                     Check if p_val is NULL or =<0                       BOOLEAN TRUE/FALSE
#################################################################################################################

########################################################################
# GLOBALS
########################################################################
GLOBALS "globals.4gl"

########################################################################
# FUNCTION displayKeyGuide(str)
# RETURN NONE
#
# Displays user instructions to field
########################################################################
FUNCTION displayKeyGuide(str)
  DEFINE str VARCHAR(100)

	DISPLAY "" TO lbInfo1

	CASE get_displayKeyInstructions()
  	WHEN 1
  		DISPLAY str TO lbInfo1 --ATTRIBUTE(GREEN)
	END CASE
	
END FUNCTION


########################################################################
# FUNCTION displayUsageGuide(str)
# RETURN NONE
#
# Displays user instructions to field
########################################################################
FUNCTION displayUsageGuide(str)
  DEFINE str VARCHAR(100)

	DISPLAY "" TO lbInfo2

	CASE get_displayUsageInstructions()
		WHEN 1
			DISPLAY str TO lbInfo2 ATTRIBUTE(GREEN)
	END CASE
	
END FUNCTION


########################################################################
# FUNCTION popup_alert()
# RETURN NONE
#
# Displays 'F5' for popup... message in statusbar
########################################################################
FUNCTION popup_alert()
  ERROR "Press (F5) for list of known values for this field"
END FUNCTION

########################################################################
# FUNCTION question_box(msg)
#
# Question dialog box with the options yes no cancel
#
# RETURN BOOLEAN TRUE/FALSE
########################################################################
FUNCTION question_box(msg)
  DEFINE 
    msg CHAR(200),
    rv  CHAR

  CALL fgl_winquestion("CMS",msg,1,"Yes|No|Cancel","question",0) RETURNING rv

  CASE rv
    WHEN "Y"
      RETURN TRUE
    WHEN "N"
      RETURN FALSE
    WHEN "C"
      RETURN FALSE
  END CASE 

END FUNCTION





########################################################################
# FUNCTION show_picture(p_contact_id)
#
# ??????????????????
#
# RETURN NONE
########################################################################
FUNCTION show_picture(p_contact_id)
  DEFINE p_contact_id LIKE contact.cont_id
  LOCATE g_picture IN MEMORY

  SELECT obj_picture
    INTO g_picture
    FROM contact, pictures
    WHERE contact.cont_id = p_contact_id
      AND pictures.cont_id = contact.cont_id

END FUNCTION

########################################################################
# FUNCTION exist(p_val)
#
# Check if p_val is NULL or =<0 
#
# RETURN BOOLEAN TRUE/FALSE
########################################################################
FUNCTION exist(p_val)
  DEFINE p_val INTEGER
  IF p_val IS NOT NULL AND p_val > 0 THEN
    RETURN TRUE
  ELSE 
    RETURN FALSE
  END IF
END FUNCTION

