##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

######################################################
# GLOBALS
######################################################
GLOBALS "globals.4gl"

#######################################################
# FUNCTION populate_login_form_labels_g()
#
# Populate operator details form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_login_form_labels_g()
  DISPLAY get_str(1010) TO lbTitle
  DISPLAY get_str(1011) TO dl_f1
  DISPLAY get_str(1012) TO dl_f2
  #DISPLAY get_str(1013) TO dl_f3
  #DISPLAY get_str(1014) TO dl_f4
  #DISPLAY get_str(1015) TO dl_f5
  #DISPLAY get_str(1016) TO dl_f6
  #DISPLAY get_str(1017) TO dl_f7
  #DISPLAY get_str(1018) TO dl_f8
  DISPLAY get_str(1019) TO lbInfo1

 CALL fgl_settitle(get_str(1010))  --"CMS 4.31 -Demo Application Login Screen")

  DISPLAY get_str(810) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str(820) TO bt_cancel
  #DISPLAY "!" TO bt_cancel
END FUNCTION


