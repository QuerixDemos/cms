##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

####################################################################################################################
#
# View / Presentation Layer functions
#
####################################################################################################################



####################################################
# FUNCTION set_comp_default_titlebar()
#
# Sets the default titlebar text
#
# RETURN NONE
####################################################
FUNCTION set_cont_default_titlebar()

  CALL fgl_settitle(get_Str(288))  --"CMS-Contact Management")

END FUNCTION


####################################################
# FUNCTION populate_contact_form_combo_boxes()
#
# Calls all required combo list population functions
#
# RETURN NONE
####################################################
FUNCTION populate_contact_form_combo_boxes()
  IF fgl_fglgui() THEN  --populate the combo boxes dynamically from the database
    CALL department_combo_list("cont_dept")
    CALL position_type_combo_list("cont_position")
    CALL title_combo_list("cont_title")
    CALL country_combo_list("cont_country")
    #CALL company_type_combo_list("ctype_name")
    #CALL industry_type_combo_list("itype_name")
  END IF
END FUNCTION


####################################################
# FUNCTION grid_header_contact_scroll()
#
# Populate grid header for scroll
#
# RETURN NONE
####################################################
FUNCTION grid_header_contact_scroll()
  CALL fgl_grid_header("sa_cont_scroll","cont_id",get_str(350),"left","F13")  --"cont_id"
  CALL fgl_grid_header("sa_cont_scroll","cont_name",get_str(351),"left","F14")  --"Name:"
  CALL fgl_grid_header("sa_cont_scroll","cont_fname",get_str(352),"left","F15")  --F-Name:
  CALL fgl_grid_header("sa_cont_scroll","cont_lname",get_str(353),"left","F16")  --L-Name:
  CALL fgl_grid_header("sa_cont_scroll","comp_name",get_str(354),"left","F17")  --Company:
  CALL fgl_grid_header("sa_cont_scroll","cont_phone",get_str(355),"left","F18")  --Phone:

END FUNCTION




####################################################
# FUNCTION grid_header_open_act_scroll()
#
# Populate grid header for open activity scroll
#
# RETURN NONE
####################################################
FUNCTION grid_header_open_act_scroll()
  CALL fgl_grid_header("sa_open_act","activity_id",get_str(360),"right","F13")  --"Act.ID:
  CALL fgl_grid_header("sa_open_act","atype_name",get_str(361),"left","F14")  --Activity:
  CALL fgl_grid_header("sa_open_act","open_date",get_str(362),"left","F15")  --Open Date:
  CALL fgl_grid_header("sa_open_act","name",get_str(363),"left","F16")         --Operator:
  CALL fgl_grid_header("sa_open_act","short_desc",get_str(364),"left","F17")  --Short Desc.:
  CALL fgl_grid_header("sa_open_act","priority",get_str(365),"left","F18")  --Priority
  CALL fgl_grid_header("sa_open_act","close_date",get_str(366),"left","F19")  --C-Date:
END FUNCTION

####################################################
# FUNCTION grid_header_contact_scroll_advanced()
#
# Populate grid header for advanced search
#
# RETURN NONE
####################################################
FUNCTION grid_header_contact_scroll_advanced()
  CALL fgl_grid_header("advanced_lookup_arr","cont_id",get_str(350),"left","F13")  --"Contact ID:"
  CALL fgl_grid_header("advanced_lookup_arr","cont_name",get_str(351),"left","F14")   --Name
  CALL fgl_grid_header("advanced_lookup_arr","cont_fname",get_str(352),"left","F15")  --F-Name
  CALL fgl_grid_header("advanced_lookup_arr","cont_lname",get_str(353),"left","F16")     --L-Name
  CALL fgl_grid_header("advanced_lookup_arr","comp_name",get_str(354),"left","F17")  --Company

END FUNCTION







#######################################################
# FUNCTION populate_contact_form_labels_g()
#
# Populate contact form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxContact","Contact")
  CALL updateUILabel("cntDetail1GroupBoxMisc","Miscellaneous")
  CALL updateUILabel("cntDetail1GroupBoxMap","Map / Location")
  CALL updateUILabel("cntDetail2GroupBox1","Address")
  CALL updateUILabel("cntDetail2GroupBox2","Communication")
  CALL updateUILabel("cntDetail2GroupBox3","Notes")

  CALL UIRadButListItemText("cont_usemail",1,"No Mail")
  CALL UIRadButListItemText("cont_usemail",2,"Send Mail")
  CALL UIRadButListItemText("cont_usephone",1,"No Phone")
  CALL UIRadButListItemText("cont_usephone",2,"Phone Contact")
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
 # DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms



	#JUST for a TEST
	#CALL fgl_winmessage("Display web comp","Display web comp","info")
	#DISPLAY "Ahornweg 2 Finsing Deutschland" TO wc_google_map

  CALL fgl_setTitle(get_str(370))

  CALL grid_header_open_act_scroll()

END FUNCTION

#######################################################
# FUNCTION populate_contact_form_labels_t()
#
# Populate contact form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_form_labels_t()
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  DISPLAY get_str(392) TO dl_f32
  DISPLAY get_str(393) TO dl_f33
  DISPLAY get_str(394) TO dl_f34
END FUNCTION


#######################################################
# FUNCTION populate_contact_short_form_labels_g()
#
# Populate contact (short rec) form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_short_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxLeft","Name")
  CALL updateUILabel("cntDetail1GroupBoxRight","Image/Picture")
  CALL updateUILabel("c4","Address")
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  CALL fgl_setTitle(get_str(370))

  #CALL grid_header_open_act_scroll()

END FUNCTION

#######################################################
# FUNCTION populate_contact_short_form_labels_t()
#
# Populate contact (short rec) form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_short_form_labels_t()
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  #DISPLAY get_str(377) TO dl_f7
  #DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms


END FUNCTION


#######################################################
# FUNCTION populate_contact_edit_form_labels_g()
#
# Populate contact (in edit/input) form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_edit_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxContact","Contact")
  CALL updateUILabel("cntDetail1GroupBoxMisc","Miscellaneous")
  CALL updateUILabel("cntDetail1GroupBoxMap","Map / Location")
  CALL updateUILabel("cntDetail2GroupBox1","Address")
  CALL updateUILabel("cntDetail2GroupBox2","Communication")
  CALL updateUILabel("cntDetail2GroupBox3","Notes")
  
  CALL UIRadButListItemText("cont_usemail",1,"No Mail")
  CALL UIRadButListItemText("cont_usemail",2,"Send Mail")
  CALL UIRadButListItemText("cont_usephone",1,"No Phone")
  CALL UIRadButListItemText("cont_usephone",2,"Phone Contact")
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  CALL fgl_setTitle(get_str(370))

  CALL grid_header_open_act_scroll()

  DISPLAY get_str(310) TO bt_upload
  #DISPLAY "!" TO bt_upload

  DISPLAY get_str(311)  TO bt_download
  #DISPLAY "!" TO bt_download

  #DISPLAY "!" TO cont_usemail
  #DISPLAY "!" TO cont_usemail
  #DISPLAY "!" TO cont_usephone    
  #DISPLAY "!" TO cont_usephone   

END FUNCTION

#######################################################
# FUNCTION populate_contact_edit_form_labels_t()
#
# Populate contact (in edit/input) form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_edit_form_labels_t()
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
 # DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  DISPLAY get_str(392) TO dl_f32
  DISPLAY get_str(393) TO dl_f33
  DISPLAY get_str(394) TO dl_f34

  DISPLAY get_str(11) TO lbInfo1
END FUNCTION


#######################################################
# FUNCTION populate_contact_popup_form_labels_g()
#
# Populate contact popup form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_popup_form_labels_g()

  DISPLAY get_str(290) TO lbTitle
  #DISPLAY get_str(291) TO dl_f1  
  #DISPLAY get_str(292) TO dl_f2 
  #DISPLAY get_str(293) TO dl_f3 
  #DISPLAY get_str(294) TO dl_f4 
  #DISPLAY get_str(295) TO dl_f5 
  #DISPLAY get_str(296) TO dl_f6 
  #DISPLAY get_str(297) TO dl_f7 
  #DISPLAY get_str(298) TO dl_f8 
  DISPLAY get_str(299) TO lbInfo1 

  DISPLAY get_str(811) TO bt_accept
  DISPLAY "!" TO bt_accept

  DISPLAY get_str(820) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str(824) TO bt_view
  DISPLAY "!" TO bt_view

  CALL fgl_settitle(get_str(290))  --Contact List
  CALL grid_header_contact_scroll()


END FUNCTION



#######################################################
# FUNCTION populate_contact_popup_form_labels_t()
#
# Populate contact popup form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_popup_form_labels_t()

  DISPLAY get_str(290) TO lbTitle
  DISPLAY get_str(291) TO dl_f1  
  DISPLAY get_str(292) TO dl_f2 
  DISPLAY get_str(293) TO dl_f3 
  DISPLAY get_str(294) TO dl_f4 
  DISPLAY get_str(295) TO dl_f5 
  DISPLAY get_str(296) TO dl_f6 
  #DISPLAY get_str(297) TO dl_f7 
  #DISPLAY get_str(298) TO dl_f8 
  DISPLAY get_str(299) TO lbInfo1 


END FUNCTION

################EOF#################




#######################################################
# FUNCTION populate_contact_view_form_labels_g()
#
# Populate contact form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_view_form_labels_g()
  CALL updateUILabel("cntDetail1GroupBoxContact","Contact")
  CALL updateUILabel("cntDetail1GroupBoxMisc","Miscellaneous")
  CALL updateUILabel("cntDetail1GroupBoxMap","Map / Location")
  CALL updateUILabel("cntDetail2GroupBox1","Address")
  CALL updateUILabel("cntDetail2GroupBox2","Communication")
  CALL updateUILabel("cntDetail2GroupBox3","Notes")
  
  CALL UIRadButListItemText("cont_usemail",1,"No Mail")
  CALL UIRadButListItemText("cont_usemail",2,"Send Mail")
  CALL UIRadButListItemText("cont_usephone",1,"No Phone")
  CALL UIRadButListItemText("cont_usephone",2,"Phone Contact")
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  CALL fgl_setTitle(get_str(370))

  #CALL grid_header_open_act_scroll()

END FUNCTION

#######################################################
# FUNCTION populate_contact_view_form_labels_t()
#
# Populate contact form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_contact_view_form_labels_t()
  DISPLAY get_str(370) TO lbTitle
  DISPLAY get_str(371) TO dl_f1
  DISPLAY get_str(372) TO dl_f2
  DISPLAY get_str(373) TO dl_f3
  DISPLAY get_str(374) TO dl_f4
  DISPLAY get_str(375) TO dl_f5
  #DISPLAY get_str(376) TO dl_f6
  DISPLAY get_str(377) TO dl_f7
  DISPLAY get_str(378) TO dl_f8 
  #DISPLAY get_str(379) TO dl_f9
  #DISPLAY get_str(380) TO dl_f10
  DISPLAY get_str(381) TO dl_f11
  DISPLAY get_str(382) TO dl_f12
  DISPLAY get_str(383) TO dl_f13  --Plz
  DISPLAY get_str(384) TO dl_f14
  DISPLAY get_str(385) TO dl_f15
  DISPLAY get_str(386) TO dl_f16
  DISPLAY get_str(387) TO dl_f17
  DISPLAY get_str(388) TO dl_f18
  DISPLAY get_str(389) TO dl_f19
  #DISPLAY get_str(390) TO dl_f20
  #DISPLAY get_str(391) TO dl_f21  --label was removed in lycia3 cms

  DISPLAY get_str(392) TO dl_f32
  DISPLAY get_str(393) TO dl_f33
  DISPLAY get_str(394) TO dl_f34
END FUNCTION






