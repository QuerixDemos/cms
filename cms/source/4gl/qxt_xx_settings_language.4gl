##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Settings Language
#
# This 4gl module includes functions to change the environment i.e. language choice
#
# Modification History:
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
#
# 
# 
#  
#  
#########################################################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION qxt_settings_language_edit()
#
# Edit language settings (language.cfg configuration file)
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_language_edit()
  DEFINE 
    l_settings_lang_rec  OF t_qxt_settings_lang_form_rec,
    l_language_name      VARCHAR(30)

  #Open Window
  CALL fgl_window_open("w_settings_lang",5,5,get_form_path("f_qxt_settings_language_l2"),FALSE)
  CALL qxt_settings_lang_view_form_details()

	CALL ui.Interface.setImage("qx://application/icon16/flags/application-language.png")
	CALL ui.Interface.setText("Language")

  #Get the corresponding record
  CALL get_settings_lang_form_rec()
    RETURNING  l_settings_lang_rec.*

  LET l_language_name = l_settings_lang_rec.language_name


  #Input
  INPUT BY NAME l_settings_lang_rec.* WITHOUT DEFAULTS HELP 1
    BEFORE INPUT
      #Set toolbar
      CALL publish_toolbar("SettingsCfg",0)

    #Hack - required for hotlink menu until we have a pull down menu
    BEFORE FIELD language_name
      IF get_language_id(l_settings_lang_rec.language_name) = 0 THEN  --language is invalid
        LET l_settings_lang_rec.language_name = l_language_name
        DISPLAY l_settings_lang_rec.language_name TO language_name
      END IF

    AFTER FIELD language_name
      IF get_language_id(l_settings_lang_rec.language_name) = 0 THEN  --language is invalid
        LET l_settings_lang_rec.language_name = l_language_name
        DISPLAY l_settings_lang_rec.language_name TO language_name
      END IF


    ON ACTION("settingsSave")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_lang_form_rec(l_settings_lang_rec.*)
      EXIT INPUT

    ON ACTION("settingsSaveToDisk")  --Save to disk
      CALL fgl_dialog_update_data()
      CALL copy_settings_lang_form_rec(l_settings_lang_rec.*) --Write to memory
      CALL process_language_cfg_export(NULL) --Write to disk
      EXIT INPUT


    #Tab-View Next key commands
    ON KEY(F1011)
      NEXT FIELD application_name

    ON KEY(F1012)
      NEXT FIELD cfg_path


    ON KEY(F1013)
      NEXT FIELD server_app_temp_path


    ON KEY(F1014)
      NEXT FIELD language_cfg_filename

    ON KEY(F1015)
      NEXT FIELD db_name_tool

    AFTER INPUT
      CALL copy_settings_lang_form_rec(l_settings_lang_rec.*) --Write to memory
      #CALL process_main_cfg_export(NULL) --Write to disk

  END INPUT

  IF NOT int_flag THEN
    CALL copy_settings_lang_form_rec(l_settings_lang_rec.*) --Write to memory
  ELSE
    LET int_flag = FALSE
  END IF

  #Close Window
  CALL fgl_window_close("w_settings_lang")

END FUNCTION


###########################################################
# FUNCTION qxt_settings_lang_view_form_details()
#
# Display all required form objects
#
# RETURN NONE
###########################################################
FUNCTION qxt_settings_lang_view_form_details()

  CALL fgl_settitle(get_str_tool(160))

  DISPLAY get_str_tool(161) TO dl_f61 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(162) TO dl_f62 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(163) TO dl_f63 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(164) TO dl_f64 ATTRIBUTE(BLUE, BOLD)
  DISPLAY get_str_tool(165) TO dl_f65 ATTRIBUTE(BLUE, BOLD)

  CALL language_combo_list("language_name")   
  CALL language_combo_list("language_default_name")   
  CALL source_name_combo_list("language_list_source_name")   

END FUNCTION


###########################################################
# FUNCTION get_main_settings_form_rec()
#
# Copy main settings record to form record
#
# RETURN NONE
###########################################################
FUNCTION get_settings_lang_form_rec()
  DEFINE 
    l_settings_form OF t_qxt_settings_lang_form_rec,
    local_debug     SMALLINT
  
  LET local_debug = FALSE

  ##################################
  # Language.cfg Configuration file
  ##################################
  #[Language] Section

  LET l_settings_form.language_name                  = get_language_name(qxt_settings.language_id)                  --language id
  LET l_settings_form.language_default_name          = get_language_name(qxt_settings.language_default_id)          --default language to be used if a string does not exist in a particular language
  LET l_settings_form.language_list_source_name      = get_source_name(qxt_settings.language_list_source)      --source is 0=file 1=db
  LET l_settings_form.language_list_filename         = qxt_settings.language_list_filename    --Language import file - lists all languages (not strings)
  LET l_settings_form.language_list_table_name       = qxt_settings.language_list_table_name  --Language table name - lists all languages (not strings)

  IF local_debug THEN
    DISPLAY "get_settings_lang_form_rec() - qxt_settings.language_id =",               qxt_settings.language_id
    DISPLAY "get_settings_lang_form_rec() - qxt_settings.language_default_id =",       qxt_settings.language_default_id
    DISPLAY "get_settings_lang_form_rec() - qxt_settings.language_list_source =",      qxt_settings.language_list_source
    DISPLAY "get_settings_lang_form_rec() - qxt_settings.language_list_filename =",    qxt_settings.language_list_filename
    DISPLAY "get_settings_lang_form_rec() - qxt_settings.language_list_table_name =",  qxt_settings.language_list_table_name


    DISPLAY "get_settings_lang_form_rec() - l_settings_form.language_name =",             l_settings_form.language_name 
    DISPLAY "get_settings_lang_form_rec() - l_settings_form.language_default_name =",     l_settings_form.language_default_name 
    DISPLAY "get_settings_lang_form_rec() - l_settings_form.language_list_source_name =", l_settings_form.language_list_source_name 
    DISPLAY "get_settings_lang_form_rec() - l_settings_form.language_list_filename =",    l_settings_form.language_list_filename 
    DISPLAY "get_settings_lang_form_rec() - l_settings_form.language_list_table_name =",  l_settings_form.language_list_table_name 

  END IF


  RETURN l_settings_form.*
END FUNCTION



###########################################################
# FUNCTION copy_settings_lang_form_rec(p_settings_form_rec)
#
# Copy main settings form record data to globals
#
# RETURN NONE
###########################################################
FUNCTION copy_settings_lang_form_rec(p_settings_form_rec)
  DEFINE 
    p_settings_form_rec OF t_qxt_settings_lang_form_rec
  
  ##################################
  # Language.cfg Configuration file
  ##################################
  #[Language] Section
  LET qxt_settings.language_id                      = get_language_id(p_settings_form_rec.language_name)            --language id
  LET qxt_settings.language_default_id              = get_language_id(p_settings_form_rec.language_default_name)    --default language to be used if a string does not exist in a particular language
  LET qxt_settings.language_list_source             = get_source_id(p_settings_form_rec.language_list_source_name)  --source is 0=file 1=db
  LET qxt_settings.language_list_filename           = p_settings_form_rec.language_list_filename                    --Language import file - lists all languages (not strings)
  LET qxt_settings.language_list_table_name         = p_settings_form_rec.language_list_table_name                  --Language table name - lists all languages (not strings)


END FUNCTION


####################################################################################################
# EOF
####################################################################################################




