##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

GLOBALS
  DEFINE r_db_type STRING,
    env_report STRING

DEFINE 
  db_name STRING,
  rec_sql_env 
    RECORD 
      db_driver STRING,  -- i.e. INFORMIX, SSERVER, ORACLE etc..
      qx_env STRING, -- LYCIA or HYDRA
      qxss_db_is_dsn STRING,
      hydra_db_driver STRING,
      lycia_db_driver STRING,

      dsn_name STRING,
      db_info STRING,
      path STRING,
      uitype STRING,
      LD_LIBRARY_PATH STRING,
      --INFORMIX IBM
      DBPATH, INFORMIXSERVER, INFORMIXDIR STRING,
      --SQL-SERVER Microsoft
      ODBC_DSN, SQLSERVER   STRING,
      --ORACLE
      ORACLE_HOME, ORACLE_SID  STRING,
      --DB2 IBM
      DB2DIR, DB2INSTANCE  STRING
    END RECORD,
	misc
		RECORD
			server_temp_directory STRING,
			os_architecture STRING
		END RECORD


END GLOBALS


FUNCTION about()
  DEFINE inp_char CHAR
	DEFINE os_architectureName STRING
	DEFINE temp_string STRING
	
  CALL fgl_window_open("w_about",5,5,get_form_path("f_about_l2"),FALSE)  --full screen
  CALL fgl_settitle("Help About")
  
  CALL ui.Interface.setImage("qx://application/icon16/basic/application-info.png")
	CALL ui.Interface.setText("About")
	
  #DISPLAY "!" TO bt_done

  DISPLAY fgl_getversion() TO hversion 
  DISPLAY ui.Interface.getFrontEndName() TO ui_interface_getFrontEndName
  DISPLAY ui.Interface.getFrontEndVersion() TO ui_interface_getFrontEndVersion
  #DISPLAY fgl_getproperty("gui", "gui.phoenix.version", "") TO pversion
  #DISPLAY fgl_getproperty("gui", "gui.chimera.version", "") TO cversion
  #DISPLAY fgl_getproperty("gui", "gui.comms.version", "") TO commsversion
  #DISPLAY fgl_getproperty("gui", "gui.qxcommandlayer.version", "") TO qxcommandlayerversion
  DISPLAY fgl_getproperty("gui", "gui.misc.version", "") TO miscversion

  CASE fgl_arch()

    WHEN "nt"
      DISPLAY "nt  = Windows" TO farchitecture

    WHEN "lnx"
      DISPLAY "lnx = Linux"  TO farchitecture

    WHEN "sun"
      DISPLAY "sun = 32-bit Sun Solaris"  TO farchitecture

    WHEN "s64"
      DISPLAY "s64 = 64-bit Sun Solaris"  TO farchitecture

    WHEN "hp"
      DISPLAY "hp  = 32-bit HP-UX PA-RISC"  TO farchitecture

    WHEN "h64"
      DISPLAY "h64  = 64-bit HP-UX PA-RISC"  TO farchitecture

    WHEN "i32"
      DISPLAY "i32  = 32-bit HP-UX Itanium"  TO farchitecture

    WHEN "i64"
      DISPLAY "i64  = 64-bit HP-UX Itanium" TO farchitecture

    WHEN "dec"
      DISPLAY "dec  = Compaq Tru64"  TO farchitecture

    WHEN "s86"
      DISPLAY "s86  = Sun Solaris x86"  TO farchitecture

    WHEN "aix"
      DISPLAY "aix  = IBM AIX 4.31"  TO farchitecture

    WHEN "a64"
      DISPLAY "a64  = IBM AIX 5L" TO farchitecture

    WHEN "ncr"
      DISPLAY "ncr  = NCR MP-RAS"  TO farchitecture

  END CASE


	#DISPLAY get_cssState() TO cssId
	DISPLAY get_cssId() to cssId

	#General and Misc
	DISPLAY fgl_getuitype() TO miscFgl_GetUiType
	DISPLAY fgl_getversion() TO miscFgl_GetVersion
	DISPLAY fgl_arch() TO miscFgl_Arch

  CASE fgl_arch()
    WHEN "nt" 
      LET os_architectureName = "Windows"
    WHEN "lnx" 
      LET os_architectureName = "Linux"
    WHEN "sun" 
      LET os_architectureName = "32-bit Sun Solaris"
    WHEN "s64" 
      LET os_architectureName = "64-bit Sun Solaris"
    WHEN "hp" 
      LET os_architectureName = "32-bit HP-UX PA-RISC"
    WHEN "h64" 
      LET os_architectureName = "64-bit HP-UX PA-RISC"
    WHEN "i32" 
      LET os_architectureName = "32-bit HP-UX Itanium"
    WHEN "i64" 
      LET os_architectureName = "64-bit HP-UX Itanium"
    WHEN "dec" 
      LET os_architectureName = "Compaq Tru64"
    WHEN "s86" 
      LET os_architectureName = "Sun Solaris x86"
    WHEN "aix" 
      LET os_architectureName = "IBM AIX 4.3"
    WHEN "a64" 
      LET os_architectureName = "IBM AIX 5L"
    WHEN "ncr" 
      LET os_architectureName = "NCR MP-RAS"
    WHEN "osx" 
      LET os_architectureName = "Apple Mac OSX"                                                                              
    OTHERWISE
      LET os_architectureName = "Undefined Operating System"           
	END CASE

	DISPLAY os_architectureName TO miscFgl_ArchName

	DISPLAY fgl_getenv("qx_child") TO miscChildprocess




	#Server
	DISPLAY fgl_getenv("QX_SERVER_HOST") TO serverEnvQX_SERVER_HOST
	DISPLAY fgl_getenv("QX_SERVER_IP")   TO serverEnvQX_SERVER_IP
	DISPLAY fgl_getenv("QX_SESSION_ID")  TO serverEnvQX_SESSION_ID
	DISPLAY fgl_getenv("QX_COMMAND_ID")  TO serverEnvQX_COMMAND_ID
	DISPLAY fgl_getenv("QX_PROCESS_ID")  TO serverEnvQX_PROCESS_ID

	DISPLAY fgl_getproperty("server", "system.network", "hostname") TO serverSystemNetworkHostname
	DISPLAY fgl_getproperty("server", "system.network", "ipaddress") TO serverSystemNetworkIpaddress
					
	#Client
  DISPLAY ui.Interface.getFrontEndName() TO clientUi_interface_getFrontEndName
	DISPLAY fgl_getenv("QX_CLIENT_HOST") TO clientEnvQX_CLIENT_HOST
	DISPLAY fgl_getenv("QX_CLIENT_IP")   TO clientEnvQX_CLIENT_IP

	DISPLAY fgl_getproperty("gui", "system.network", "hostname")  TO clientGuiSystemNetworkHostname	
	DISPLAY fgl_getproperty("gui", "system.network", "ipaddress") TO clientGuiSystemNetworkIpaddress
	DISPLAY fgl_getproperty("gui", "system.network", "sessionid") TO clientGuiSystemNetworkSessionid
	DISPLAY fgl_getproperty("gui", "system.network", "commandid") TO clientGuiSystemNetworkCommandid
	DISPLAY fgl_getproperty("gui", "system.network", "processid") TO clientGuiSystemNetworkProcessid
	
	
	#Environment variables
	DISPLAY fgl_getenv("LYCIA_DIR") TO envLYCIA_DIR
	DISPLAY fgl_getenv("MSGPATH") TO envMSGPATH
	DISPLAY fgl_getenv("LINES") TO envLINES
	DISPLAY fgl_getenv("COLUMNS") TO envCOLUMNS
	DISPLAY fgl_getenv("LYCIA_DRIVER_PATH") TO envLYCIA_DRIVER_PATH
	DISPLAY fgl_getenv("FGLPROFILE") TO envFGLPROFILE
	DISPLAY fgl_getenv("QXDEBUG") TO envQXDEBUG
	DISPLAY fgl_getenv("QXBREAKCH_START") TO envQXBREAKCH_START
	DISPLAY fgl_getenv("QXBREAKCH_END") TO envQXBREAKCH_END
	DISPLAY fgl_getenv("PATH") TO envPATH
	DISPLAY fgl_getenv("FGLIMAGEPATH") TO envFGLIMAGEPATH
	DISPLAY fgl_getenv("QX_MENU_WINDOW") TO envQX_MENU_WINDOW
	DISPLAY fgl_getenv("BIRT_LIBDIR") TO envBIRT_LIBDIR
	DISPLAY fgl_getenv("CLASSPATH") TO envCLASSPATH
	DISPLAY fgl_getenv("TEMP") TO envTEMP	
	DISPLAY fgl_getenv("qxss_db_is_dsn") TO envQxss_db_is_dsn	

	DISPLAY fgl_getenv("QX_CLIENT_HOST") TO envQX_CLIENT_HOST
	DISPLAY fgl_getenv("QX_CLIENT_IP") TO envQX_CLIENT_IP
	DISPLAY fgl_getenv("QX_SESSION_ID") TO envQX_SESSION_ID
	DISPLAY fgl_getenv("QX_COMMAND_ID") TO envQX_COMMAND_ID	
	DISPLAY fgl_getenv("QX_PROCESS_ID") TO envQX_PROCESS_ID
	DISPLAY fgl_getenv("QX_CHILD") TO envQX_CHILD
	
	#DISPLAY fgl_getenv("") TO env
	
	#DB-General
	DISPLAY fgl_getenv("LYCIA_DB_DRIVER") TO envdbLYCIA_DB_DRIVER
	DISPLAY fgl_getenv("DBPATH") TO envdbDBPATH
	

	
	#Informix related env variables
	DISPLAY fgl_getenv("DBPATH") TO infDBPATH
	DISPLAY fgl_getenv("DB_LOCALE") TO infDB_LOCALE
	DISPLAY fgl_getenv("INFORMIXSERVER") TO infINFORMIXSERVER
	DISPLAY fgl_getenv("INFORMIXDIR") TO infINFORMIXDIR
	DISPLAY fgl_getenv("INFORMIXSQLHOSTS") TO infINFORMIXSQLHOSTS
	DISPLAY fgl_getenv("CLIENT_LOCALE") TO infCLIENT_LOCALE
	DISPLAY fgl_getenv("qxss_db_is_dsn") TO infqxss_db_is_dsn
	DISPLAY fgl_getenv("DELIMIDENT") TO infDELIMIDENT


	#SELECT DBINFO("version", "server-type") INTO temp_string FROM systables;

	SQL
		SELECT FIRST 1 DBINFO("version", "server-type") INTO $temp_string FROM systables
	END SQL
	
	SQL
		SELECT DBINFO('version', 'major')    FROM systables    INTO $temp_string WHERE tabid = 1;
	END SQL
	
	DISPLAY temp_string TO infVersionServerType

	#SELECT DBINFO("version", "full") INTO temp_string FROM systables;
	SQL
		SELECT FIRST 1 DBINFO("version", "full") INTO $temp_string FROM systables;
	END SQL
	DISPLAY temp_string TO infVersionFull

	#SELECT DBINFO("version", "major") INTO temp_string FROM systables;
	SQL
		SELECT FIRST 1 DBINFO("version", "major") INTO $temp_string FROM systables;
	END SQL
	DISPLAY temp_string TO infVersionMajor

	
	#SELECT DBINFO("version", "os") INTO temp_string FROM systables;
	SQL
		SELECT FIRST 1 DBINFO("version", "os") INTO $temp_string FROM systables;
	END SQL
	DISPLAY temp_string TO infOS
	
	#Oracle
	DISPLAY fgl_getenv("ORACLE_HOME") TO envOraORACLE_HOME
	DISPLAY fgl_getenv("ORACLE_SID") TO envOraORACLE_SID

	# SQL Server ##########################  
	DISPLAY fgl_getenv("ODBC_DSN") TO envSqlODBC_DSN
	DISPLAY fgl_getenv("SQLSERVER") TO envSqlSQLSERVER

	# DB2 ##########################    
	DISPLAY  fgl_getenv("DB2DIR") TO envDb2DB2DIR
	DISPLAY  fgl_getenv("DB2INSTANCE") TO envDb2DB2INSTANCE
  	
	#OS & Authentication
	DISPLAY fgl_username() TO osFgl_Username
	DISPLAY fgl_getproperty("system","username") TO osSystemUsername

	DISPLAY fgl_getenv("LD_LIBRARY_PATH") TO osLD_LIBRARY_PATH  --unix only

	#BIRT
	DISPLAY fgl_getenv("BIRT_LIBDIR") TO birtEnvBIRT_LIBDIR  



  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 1
      BEFORE PROMPT   
        CALL publish_toolbar("HelpAbout",0)
      #ON ACTION "createLogFile"
			#	CALL write_report_file()      
      ON ACTION ("actExit","ACCEPT")
       EXIT WHILE
    END PROMPT
  END WHILE

  CALL fgl_window_close("w_about")

END FUNCTION

######################################################
# FUNCTION add_report_str(str, str2, level)
######################################################
FUNCTION add_report_str(str, str2, level)
  DEFINE str, str2 STRING,
    error_str STRING,
    level SMALLINT


  CASE level
    WHEN 0
      LET env_report = trim(env_report), "\n", trim(str), str2
    WHEN 1
      LET env_report = trim(env_report), "\n  ", trim(str), str2
    WHEN 2
      LET env_report = trim(env_report), "\n    ", trim(str), str2
    WHEN 3
      LET env_report = trim(env_report), "\n\t\t\t", trim(str), str2
    WHEN 4
      LET env_report = trim(env_report), "\n\t\t\t\t", trim(str), str2
    OTHERWISE
      LET error_str = "Invalid level specified in add_monitor_str() level=" , level
      CALL fgl_winmessage("Internal 4gl source error - add_monitor_str(str, str2,level)",error_str,"error")
  END CASE

END FUNCTION

##############################################################
# FUNCTION write_report_file()
##############################################################
FUNCTION write_report_file()
	CALL add_report_str("*** end of report ***","",1)

	CALL fgl_channel_open_file("stream", "testconn_log.txt", "w")
	CALL fgl_channel_write("stream", env_report)
  CALL fgl_channel_close("stream")
  CALL fgl_winmessage("The log file was created.", "The log file testconn_log.txt was created.","info")
  
END FUNCTION


##############################################################
# FUNCTION get_database_type()
##############################################################
FUNCTION get_database_type()
  DEFINE 
    db_type_info VARCHAR(100),
    ret_str VARCHAR(120),
    db_type CHAR(3)

  ######################################################################
  LET db_type = db_get_database_type()
  ######################################################################

  #DISPLAY "LET db_type = db_get_database_type()" AT 3,3
  #DISPLAY "The function db_get_databasec returned : " , trim(db_type) AT 4,3

  CASE db_type
    WHEN "IFX"
      LET db_type_info = "Informix Database" 
    WHEN "ORA"
      LET db_type_info = "Oracle Database" 
    WHEN "MSV"
      LET db_type_info = "MS-SQL Server Database" 
    WHEN "DB2"
      LET db_type_info = "DB2 Database"  
    WHEN "MYS"
      LET db_type_info = "My SQL Database"  
    WHEN "PGS"
      LET db_type_info = "PostgreSQL Database"  
    WHEN "PEV"
      LET db_type_info = "Pervasive Database"  
    WHEN "MDB"
      LET db_type_info = "MAXDB (SAP-MySQL) Database" 
    WHEN "SYB"
      LET db_type_info = "Sybase Database"  
    WHEN "FIB"
      LET db_type_info = "Firebird Database" 
    WHEN "ADB"
      LET db_type_info = "Adabase Database" 

    OTHERWISE
      LET db_type_info = "Unknown" 

  END CASE
  LET ret_str = db_type CLIPPED, " = ", db_type_info
  RETURN ret_str
ENd FUNCTION


##############################################################
# FUNCTION prog_arguments()
##############################################################
FUNCTION prog_arguments()
  DEFINE prog_call VARCHAR(200), i, sl INT

  FOR i = 0 TO NUM_ARGS()
    LET prog_call = trim(prog_call) , trim(ARG_VAL(i)) , " /"
  END FOR  

  LET sl = length(prog_call)
  LET prog_call[sl] = ""  --overwrite the final slash at the ending

  RETURN prog_call


  

ENd FUNCTION







##############################################################
# Informix environment report
# FUNCTION report_informix_env()
##############################################################
FUNCTION report_informix_env()
  CALL add_report_str("Informix Environment:","",0)
  CALL add_report_str("DBPATH:",rec_sql_env.DBPATH,1)
  CALL add_report_str("INFORMIXSERVER:",rec_sql_env.INFORMIXSERVER,1)
  CALL add_report_str("INFORMIXDIR:",rec_sql_env.INFORMIXDIR,1)
  CALL add_report_str("---","",0)  --empty line
END FUNCTION



##############################################################
# Oracle enviornment report
# FUNCTION report_oracle_env()
##############################################################
FUNCTION report_oracle_env()
	CALL add_report_str("Oracle Environment:","",0)
  CALL add_report_str("ORACLE_HOME:",rec_sql_env.ORACLE_HOME,1)
  CALL add_report_str("ORACLE_SID:",rec_sql_env.ORACLE_SID,1)
  CALL add_report_str("---","",0)  --empty line
END FUNCTION

##############################################################
# MS SQL Server report
# FUNCTION report_sserver_env()
##############################################################
FUNCTION report_sserver_env()
  CALL add_report_str("Microsoft SQL Server Environment:","",0)
  CALL add_report_str("ODBC_DSN:",rec_sql_env.ODBC_DSN,1)
  CALL add_report_str("SQLSERVER:",rec_sql_env.SQLSERVER,1)
  CALL add_report_str("---","",0)  --empty line
END FUNCTION

##############################################################
# IBM DB2 report
# FUNCTION report_db2_env()
##############################################################
FUNCTION report_db2_env()
  CALL add_report_str("DB IBM Environment:","",0)
  CALL add_report_str("DB2DIR:",rec_sql_env.DB2DIR,1) 
  CALL add_report_str("DB2INSTANCE:",rec_sql_env.DB2INSTANCE,1) 
  CALL add_report_str("---","",0)  --empty line    
END FUNCTION

##############################################################
# ODBC Database report - Other database via ODBC
# FUNCTION report_odbc_env()
##############################################################
FUNCTION report_odbc_env()
  CALL add_report_str("Other RDBMS - ODBC Environment:","",0)
  CALL add_report_str("ODBC_DSN:",rec_sql_env.ODBC_DSN,1)
  CALL add_report_str("---","",0)  --empty line
END FUNCTION




##############################################################
# FUNCTION test_connection()
##############################################################
FUNCTION test_connection()
  DEFINE 
		display_str VARCHAR(100),--STRING,
		retval      SMALLINT,
		msg_str     STRING
		
    DISPLAY "Enter the database name for the test connection" TO dl_info
    INPUT db_name FROM db_name HELP 1 --PROMPT "Enter database name: " FOR db_name
    DISPLAY "" TO dl_info

   
	IF length(db_name)=0 THEN
		RETURN
	END IF
   
	LET display_str = "Connecting to database '",db_name CLIPPED,"'..."
	DISPLAY  display_str TO dl_info --AT 6, 3

	WHENEVER ERROR CONTINUE

	CALL open_db(db_name) RETURNING retval

	WHENEVER ERROR STOP

    IF retval < 0 THEN
      LET display_str = "Connection failed   Error:" , retval
      DISPLAY display_str TO dl_db_connect_result ATTRIBUTE(RED) --AT 7,3
      CALL fgl_winmessage("Error",display_str,"exclamation")
      
    ELSE
      DISPLAY "Connection successful"  TO dl_db_connect_result   ATTRIBUTE(GREEN) --AT 7,3
      CALL fgl_winmessage("Sucess","Database connection was successful","info")
    END IF

    LET msg_str =  get_database_type()
    DISPLAY msg_str TO fgl_db

    CALL add_report_str("Database Type - get_database_type():","",0)
    CALL add_report_str(msg_str,"",1)

    CALL add_report_str("---","",0)  --empty line


    #LET driver_error = fgl_driver_error()
    #CALL add_report_str("Driver Error information CALL driver_error():","",0)
    #CALL add_report_str(driver_error","",1)
    CALL add_report_str("database connection return value (error > 0) :","",0)
    CALL add_report_str(retval,"",1)

    CALL add_report_str("---","",0)  --empty line


    CALL add_report_str("Driver Error information CALL ERR_GET():","",0)
    CALL add_report_str(ERR_GET(retval),"",1)

    CALL add_report_str("---","",0)  --empty line

    CALL add_report_str("Driver Error information CALL fgl_driver_error():","",0)
    CALL add_report_str(fgl_driver_error(),"",1)

    CALL add_report_str("---","",0)  --empty line

    CALL add_report_str("Driver Error information CALL fgl_native_error():","",0)
    CALL add_report_str(fgl_native_error(),"",1)

    CALL add_report_str("---","",0)  --empty line

    CALL add_report_str("TestConn program was called in following way (including arguments):","",0)
    CALL add_report_str(prog_arguments(),"",1)
    
END FUNCTION

