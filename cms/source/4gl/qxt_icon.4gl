##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Icon Functions to access path / file access / file location
#
#
#
# FUNCTION                                        DESCRIPTION                                                RETURN
# process_cfg_import(filename)                   Import/Process cfg file                                    RET
#                                                 Using tools from www.bbf7.de - rk-config
# process_cfg_export(filename)                   Export cfg data                                            ret
# get_icon10_directory()                          Return icon10 (10pixel icon) directory                     qxt_settings.icon10_path
# get_icon10_path(document_name)                  Return file name with 10pixel 'icon10' path                trim(ret)
# get_icon16_directory()                          Return icon10 (16pixel icon) directory                     qxt_settings.icon16_path
# get_icon16_path(document_name)                  Return file name with 16pixel 'icon16' path                trim(ret)
# get_icon32_directory()                          Return icon32 (32pixel icon) directory                     qxt_settings.icon32_path
# get_icon32_path(document_name)                  Return file name with 32pixel 'icon32' path                trim(ret)
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_globals.4gl"

###########################################################
# FUNCTION  get_icon_cfg_filename()
#
# Import/Process icon cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.icon_cfg_filename
###########################################################
FUNCTION get_icon_cfg_filename()
  RETURN qxt_settings.icon_cfg_filename
END FUNCTION


###########################################################
# FUNCTION set_icon_cfg_filename(p_filename)
#
# Export/Write icon cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN qxt_settings.icon_cfg_filename
###########################################################
FUNCTION set_icon_cfg_filename(p_filename)
  dEFINE
    p_filename   VARCHAR(150)

  LET qxt_settings.icon_cfg_filename = p_filename
END FUNCTION


###########################################################
# FUNCTION process_icon_cfg_import(p_icon_cfg_filename)
#
# Import/Process Icon cfg file
# Using tools from www.bbf7.de - rk-config
#
# RETURN ret
###########################################################
FUNCTION process_icon_cfg_import(p_icon_cfg_filename)
DEFINE 
  p_icon_cfg_filename  VARCHAR(200),
  ret SMALLINT,
  local_debug SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  IF local_debug THEN
    DISPLAY "process_icon_cfg_import() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF

  IF p_icon_cfg_filename IS NULL THEN
    LET p_icon_cfg_filename = get_icon_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_icon_cfg_import() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF

  #Add path
  LET p_icon_cfg_filename = get_cfg_path(p_icon_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_icon_cfg_import() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF


  IF p_icon_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_icon_cfg_import()","Error in process_icon_cfg_import()\nNo import icon_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    EXIT PROGRAM
  END IF



############################################
# Using tools from www.bbf7.de - rk-config
############################################
  LET ret = configInit(p_icon_cfg_filename)
  #DISPLAY "ret=",ret
  #CALL fgl_channel_set_delimiter(filename,"")

  #[Icon]
  #read from Icon section
  LET qxt_settings.icon10_path         = configGet(ret, "[Icon]", "icon10_path")
  LET qxt_settings.icon16_path         = configGet(ret, "[Icon]", "icon16_path")
  LET qxt_settings.icon24_path         = configGet(ret, "[Icon]", "icon24_path")
  LET qxt_settings.icon32_path         = configGet(ret, "[Icon]", "icon32_path")


  #Debug information
  IF local_debug THEN
    DISPLAY "configInit() - get_icon_cfg_filename = ", p_icon_cfg_filename CLIPPED, "###############################"


    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    DISPLAY "x [Icon] Section               x"
    DISPLAY "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"    
    DISPLAY "configInit() - qxt_settings.icon10_path=",         qxt_settings.icon10_path
    DISPLAY "configInit() - qxt_settings.icon16_path=",         qxt_settings.icon16_path
    DISPLAY "configInit() - qxt_settings.icon24_path=",         qxt_settings.icon24_path
    DISPLAY "configInit() - qxt_settings.icon32_path=",         qxt_settings.icon32_path

   
  END IF

  #CALL verify_server_directory_structure(FALSE)


  RETURN ret

END FUNCTION


###########################################################
# FUNCTION process_icon_cfg_export(p_icon_cfg_filename)
#
# Export Icon icon cfg data
#
# RETURN ret
###########################################################
FUNCTION process_icon_cfg_export(p_icon_cfg_filename)
  DEFINE 
    p_icon_cfg_filename     VARCHAR(100),
    ret                     SMALLINT,
    local_debug             SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "process_icon_cfg_export() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF

  IF p_icon_cfg_filename IS NULL THEN
    LET p_icon_cfg_filename = get_icon_cfg_filename()
  END IF

  IF local_debug THEN
    DISPLAY "process_icon_cfg_export() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF

  #Add path
  LET p_icon_cfg_filename = get_cfg_path(p_icon_cfg_filename)

  IF local_debug THEN
    DISPLAY "process_icon_cfg_export() - p_icon_cfg_filename=", p_icon_cfg_filename
  END IF


  IF p_icon_cfg_filename IS NULL THEN
    CALL fgl_winmessage("Error in process_icon_cfg_export()","Error in process_icon_cfg_export()\nNo import icon_cfg_filename was specified in the function call\nand also not found in the application .cfg (i.e. cms.cfg) file","error")
    RETURN NULL
  END IF

  ############################################
  # Using tools from www.bbf7.de - rk-config
  ############################################
  LET ret = configInit(p_icon_cfg_filename)  --configInit(filename)

  #CALL fgl_channel_set_delimiter(filename,"")


  #[Icon]
  #Write to Icon section
  CALL configWrite(ret, "[Icon]", "icon10_path",       qxt_settings.icon10_path )
  CALL configWrite(ret, "[Icon]", "icon16_path",       qxt_settings.icon16_path )
  CALL configWrite(ret, "[Icon]", "icon24_path",       qxt_settings.icon24_path )
  CALL configWrite(ret, "[Icon]", "icon32_path",       qxt_settings.icon32_path )

END FUNCTION



##################################################################################
# FUNCTION get_icon10_directory()
#
# Return icon10 (10pixel icon) directory
#
# RETURN qxt_settings.icon10_path
##################################################################################
FUNCTION get_icon10_directory()

  RETURN qxt_settings.icon10_path

END FUNCTION


##################################################################################
# FUNCTION get_icon10_path(document_name)
#
# Return file name with 10pixel 'icon10' path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_icon10_path(icon_name)
  DEFINE
    icon_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.icon10_path), "/", trim(icon_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_icon16_directory()
#
# Return icon16 (16pixel icon) directory
#
# RETURN qxt_settings.icon16_path
##################################################################################
FUNCTION get_icon16_directory()

  RETURN qxt_settings.icon16_path

END FUNCTION


##################################################################################
# FUNCTION get_icon16_path(document_name)
#
# Return file name with 16pixel 'icon16' path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_icon16_path(icon_name)
  DEFINE
    icon_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.icon16_path), "/", trim(icon_name)
  RETURN trim(ret)
END FUNCTION

##################################################################################
# FUNCTION get_icon24_directory()
#
# Return icon16 (16pixel icon) directory
#
# RETURN qxt_settings.icon16_path
##################################################################################
FUNCTION get_icon24_directory()

  RETURN qxt_settings.icon24_path

END FUNCTION


##################################################################################
# FUNCTION get_icon24_path(icon_name)
#
# Return file name with 24pixel 'icon24' path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_icon24_path(icon_name)
  DEFINE
    icon_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.icon24_path), "/", trim(icon_name)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_icon32_directory()
#
# Return icon32 (32pixel icon) directory
#
# RETURN qxt_settings.icon32_path
##################################################################################
FUNCTION get_icon32_directory()

  RETURN qxt_settings.icon32_path

END FUNCTION


##################################################################################
# FUNCTION get_icon32_path(document_name)
#
# Return file name with 32pixel 'icon32' path
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_icon32_path(icon_name)
  DEFINE
    icon_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(qxt_settings.icon32_path), "/", trim(icon_name)
  RETURN trim(ret)
END FUNCTION












