##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

##################################################
# 
# FUNCTION:                                     DESCRIPTION:                                 RETURN
# get_xchg_rate(p_currency_id)                  Get the exchange Rate                        l_xchg_rate
# get_xchg_rate_iv(p_invoice_id)                The the exchange Rate from Invoice           l_xchg_rate
# get_currency(p_currency_id)                   The the currency name                        l_currency_name
# get_currency_name(p_currency_id)              Get the currency name  (seems a duplicate)   l_currency_name
# get_currency_id(p_currency_name)              get the currency id form the name            l_currency_id
# get_currency_rec(p_currency_id)               Get the currency record from an id           l_currency.*
# currency_popup_data_source()                  Data Source for currency_popup_data_source() NONE
# currency_popup()                              View currencies and return seletec value     l_currency.*
# currency_create()                             Create a new currency (record)               rv
# currency_delete(p_currency_id)                Delete a currency                            NONE
# currency_edit(p_currency_id)                  Edit a currency                              NONE
# currency_view_by_rec(p_currency_rec)                 View a currency record                       NONE
# currency_input(p_currency)                    Insert new currency (subfunction of create)  l_currency.*
# currency_combo_list(cb_field_name)            Populates combo list with values from DB     NONE
# grid_header_currency_scroll()                 Populate grid array header with labels       NONE
# currency_popup                                Display currency list for selection          l_currency_arr[i].currency_id or p_currency_id
# (p_currency_id,p_order_field,p_accept_action) and management                               
#
##################################################




##################################################
# GLOBALS
##################################################
GLOBALS "globals.4gl"



##################################################
# FUNCTION get_xchg_rate(p_currency_id)
#
# Get the exchange Rate                       
# RETURN l_xchg_rate
##################################################
FUNCTION get_xchg_rate(p_currency_id)
  DEFINE 
    p_currency_id LIKE currency.currency_id,
    l_xchg_rate LIKE currency.xchg_rate

  SELECT currency.xchg_rate
    INTO l_xchg_rate
    FROM currency
    WHERE currency.currency_id = p_currency_id

  RETURN l_xchg_rate
END FUNCTION


##################################################
# FUNCTION get_xchg_rate_iv(p_invoice_id)
#
# The the exchange Rate from Invoice          
# RETURN l_xchg_rate
##################################################
FUNCTION get_xchg_rate_iv(p_invoice_id)
  DEFINE 
    p_invoice_id LIKE invoice.invoice_id,
    l_xchg_rate LIKE currency.xchg_rate

  SELECT currency.xchg_rate
    INTO l_xchg_rate
    FROM currency, invoice
    WHERE currency.currency_id = invoice.currency_id
      AND invoice.invoice_id = p_invoice_id

  RETURN l_xchg_rate
END FUNCTION


##################################################
# FUNCTION get_currency_name(p_currency_id)
##################################################
FUNCTION get_currency_name(p_currency_id)
  DEFINE 
    p_currency_id LIKE currency.currency_id,
    l_currency_name LIKE currency.currency_name

  SELECT currency.currency_name
    INTO l_currency_name
    FROM currency
    WHERE currency.currency_id = p_currency_id

  RETURN l_currency_name
END FUNCTION


##################################################
# FUNCTION get_currency_id(p_currency_name)
#
# get the currency id form the name
#
# RETURN l_currency_id
##################################################
FUNCTION get_currency_id(p_currency_name)
  DEFINE 
    l_currency_id   LIKE currency.currency_id,
    p_currency_name LIKE currency.currency_name

  SELECT currency.currency_id
    INTO l_currency_id
    FROM currency
    WHERE currency.currency_name = p_currency_name

  RETURN l_currency_id
END FUNCTION


##################################################
# FUNCTION get_currency_rec(p_currency_id)
#
# Get the currency record from an id
#
# RETURN l_currency.*
##################################################
FUNCTION get_currency_rec(p_currency_id)
  DEFINE 
    p_currency_id LIKE currency.currency_id,
    l_currency    RECORD LIKE currency.*

  SELECT currency.*
    INTO l_currency.*
    FROM currency
    WHERE currency.currency_id = p_currency_id

  RETURN l_currency.*
END FUNCTION


###################################################################################
# FUNCTION currency_combo_list(cb_field_name)
#
# Populates combo list with values from DB
#
# RETURN NONE
###################################################################################
FUNCTION currency_combo_list(cb_field_name)
  DEFINE l_currency_arr DYNAMIC ARRAY OF t_currency_rec,
    row_count           INTEGER,
    current_row         INTEGER,
    cb_field_name       VARCHAR(20)   --form field name for the country combo list field
  
  DECLARE c_currency_scroll2 CURSOR FOR 
    SELECT currency.currency_name 
      FROM currency

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_currency_scroll2 INTO l_currency_arr[row_count]
    CALL fgl_list_set(cb_field_name,row_count, l_currency_arr[row_count].currency_name)
    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1
		If row_count > 0 THEN
			CALL l_currency_arr.resize(row_count)  --remove the last item which has no data
		END IF

END FUNCTION

