##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Tools for the icon_category table
#
#
# FUNCTION                                            DESCRIPTION                                                   RETURN
# get_icon_category_id(p_language_id)  get icon_category id from p_language_id        l_icon_category_id
# get_language_id(category_id)    Get language_id from p_icon_category_id        l_language_id
# get_icon_category_rec(p_icon_category_id)   Get the icon_category record from p_icon_category_id  l_icon_category.*
# icon_category_popup_data_source()               Data Source (cursor) for icon_category_popup              NONE
# icon_category_popup                             icon_category selection window                            p_icon_category_id
# (p_icon_category_id,p_order_field,p_accept_action)
# icon_category_combo_list(cb_field_name)         Populates icon_category combo list from db                NONE
# icon_category_create()                          Create a new icon_category record                         NULL
# icon_category_edit(p_icon_category_id)      Edit icon_category record                                 NONE
# icon_category_input(p_icon_category_rec)    Input icon_category details (edit/create)                 l_icon_category.*
# icon_category_delete(p_icon_category_id)    Delete a icon_category record                             NONE
# icon_category_view(p_icon_category_id)      View icon_category record by ID in window-form            NONE
# icon_category_view_by_rec(p_icon_category_rec) View icon_category record in window-form               NONE
# get_icon_category_id_from_language_id(p_language_id)          Get the icon_category_id from a file                      l_icon_category_id
# language_id_count(p_language_id)                Tests if a record with this language_id already exists               r_count
# icon_category_id_count(p_icon_category_id)  tests if a record with this icon_category_id already exists r_count
# copy_icon_category_record_to_form_record        Copy normal icon_category record data to type icon_category_form_rec   l_icon_category_form_rec.*
# (p_icon_category_rec)
# copy_icon_category_form_record_to_record        Copy type icon_category_form_rec to normal icon_category record data   l_icon_category_rec.*
# (p_icon_category_form_rec)
#
##################################################################################################################################
# Display functions
#################################################################################################################################
#
# grid_header_icon_category_scroll()              Populate icon_category grid headers                       NONE
# populate_icon_category_form_labels_g()          Populate icon_category form labels for gui                NONE
# populate_icon_category_form_labels_t()          Populate icon_category form labels for text               NONE
# populate_icon_category_form_edit_labels_g()     Populate icon_category form edit labels for gui           NONE
# populate_icon_category_form_edit_labels_t()     Populate icon_category form edit labels for text          NONE
# populate_icon_category_form_view_labels_g()     Populate icon_category form view labels for gui           NONE
# populate_icon_category_form_view_labels_t()     Populate icon_category form view labels for text          NONE
# populate_icon_category_form_create_labels_g()   Populate icon_category form create labels for gui         NONE
# populate_icon_category_form_create_labels_t()   Populate icon_category form create labels for text        NONE
# populate_icon_category_list_form_labels_g()     Populate icon_category list form labels for gui           NONE
# populate_icon_category_list_form_labels_t()     Populate icon_category list form labels for text          NONE
#
####################################################################################################################################



############################################################
# Globals
############################################################
GLOBALS "qxt_db_tb_globals.4gl"


#########################################################
# FUNCTION get_icon_category_id(p_icon_category_data, p_language_id)
#
# get icon_category_data from p_icon_category_id
#
# RETURN l_icon_category_id
#########################################################
FUNCTION get_icon_category_id(p_icon_category_data, p_language_id)
  DEFINE 
    p_icon_category_data          LIKE qxt_icon_category.icon_category_data,
    p_language_id                 LIKE qxt_language.language_id,
    l_icon_category_id            LIKE qxt_icon_category.icon_category_id,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_category_data() - p_icon_category_data = ", p_icon_category_data
    DISPLAY "get_icon_category_data() - p_language_id = ", p_language_id
  END IF

  #CHECK FOR NULL
  IF p_icon_category_data IS NULL THEN
    RETURN 1  --The first record entry will be used for the empty string / no category
  END IF

  IF p_language_id IS NULL THEN
    RETURN 1  --The first record entry will be used for the empty string / no category
  END IF

  SELECT qxt_icon_category.icon_category_id
    INTO l_icon_category_id
    FROM qxt_icon_category
    WHERE qxt_icon_category.icon_category_data = p_icon_category_data
      AND qxt_icon_category.language_id = p_language_id
    

  IF local_debug THEN
    DISPLAY "get_icon_category_data() - l_icon_category_id = ", l_icon_category_id
  END IF

  RETURN l_icon_category_id
END FUNCTION


#########################################################
# FUNCTION get_icon_category_data(p_icon_category_id,p_language_id)
#
# get icon_category_data from p_icon_category_id
#
# RETURN l_icon_category_data
#########################################################
FUNCTION get_icon_category_data(p_icon_category_id,p_language_id)
  DEFINE 
    p_icon_category_id          LIKE qxt_icon_category.icon_category_id,
    p_language_id               LIKE qxt_language.language_id,
    l_icon_category_data        LIKE qxt_icon_category.icon_category_data,
    local_debug                SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_category_data() - p_icon_category_id = ", p_icon_category_id
    DISPLAY "get_icon_category_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_icon_category.icon_category_data
    INTO l_icon_category_data
    FROM qxt_icon_category
    WHERE qxt_icon_category.icon_category_id = p_icon_category_id
      AND qxt_icon_category.language_id = p_language_id


  IF local_debug THEN
    DISPLAY "get_icon_category_data() - l_icon_category_data = ", l_icon_category_data
  END IF

  RETURN l_icon_category_data
END FUNCTION



#########################################################
# FUNCTION get_icon_category_rec(p_icon_category_id,p_language_id)
#
# get string record from p_language_id and p_icon_category_id
#
# RETURN l_icon_category_rec.*
#########################################################
FUNCTION get_icon_category_rec(p_icon_category_id,p_language_id)
  DEFINE 
    p_icon_category_id          LIKE qxt_icon_category.icon_category_id,
    p_language_id               LIKE qxt_language.language_id,
    l_icon_category_rec         RECORD LIKE qxt_icon_category.*,
    local_debug                 SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_icon_category_data() - p_icon_category_id = ", p_icon_category_id
    DISPLAY "get_icon_category_data() - p_language_id = ", p_language_id
  END IF

  SELECT qxt_icon_category.*
    INTO l_icon_category_rec.*
    FROM qxt_icon_category
    WHERE icon_category_id = p_icon_category_id
      AND language_id = p_language_id

  RETURN l_icon_category_rec.*
END FUNCTION


###################################################################################
# FUNCTION get_icon_category_new_id(p_language_id)
#
# Finds MAX category_id and Returns it +1  (SERIAL emulation) 
#
# RETURN NONE
###################################################################################
FUNCTION get_icon_category_new_id(p_language_id)
  DEFINE
    p_language_id            LIKE qxt_language.language_id,
    l_max_icon_category_id   LIKE qxt_icon_category.icon_category_id

  SELECT  MAX (icon_category_id)
    INTO l_max_icon_category_id
    FROM qxt_icon_category
    WHERE qxt_icon_category.language_id = p_language_id

  RETURN l_max_icon_category_id + 1
END FUNCTION


########################################################################################################
# Combo list and selection list-management functions
########################################################################################################


###################################################################################
# FUNCTION icon_category_data_combo_list(p_cb_field_name,p_language_id)
#
# Populates icon_category_data_combo_list list from db
#
# RETURN NONE
###################################################################################
FUNCTION icon_category_data_combo_list(p_cb_field_name,p_language_id)
  DEFINE 
    p_cb_field_name             VARCHAR(30),   --form field name for the country combo list field
    p_language_id               LIKE qxt_language.language_id,
    l_icon_category_data        DYNAMIC ARRAY OF LIKE qxt_icon_category.icon_category_data,
    row_count                   INTEGER,
    current_row                 INTEGER,
    abort_flag                  SMALLINT,
    tmp_str                     VARCHAR(250),
    local_debug                 SMALLINT

  #LET rv = NULL
  LET abort_flag = TRUE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_category_data_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "icon_category_data_combo_list() - p_language_id=", p_language_id

  END IF

  DECLARE c_icon_category_scroll2 CURSOR FOR 
  SELECT  icon_category_data
    FROM  qxt_icon_category
    WHERE language_id = p_language_id
    ORDER BY icon_category_data ASC 

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_icon_category_scroll2 INTO l_icon_category_data[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_icon_category_data[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_icon_category_data[row_count] CLIPPED, ")"
      DISPLAY "icon_category_data_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION


###################################################################################
# FUNCTION toolbar_icon_category_id_combo_list(p_cb_field_name,p_language_id)
#
# Populates toolbar_icon_category_id_combo list from db
#
# RETURN NONE
###################################################################################
FUNCTION toolbar_icon_category_id_combo_list(p_cb_field_name,p_language_id)
  DEFINE 
    p_cb_field_name           VARCHAR(30),   --form field name for the country combo list field
    p_language_id             LIKE qxt_language.language_id,
    l_icon_category_id        DYNAMIC ARRAY OF LIKE qxt_icon_category.icon_category_id,
    row_count                 INTEGER,
    current_row               INTEGER,

    abort_flag               SMALLINT,
    tmp_str                   VARCHAR(250),
    local_debug               SMALLINT

  #LET rv = NULL
  LET abort_flag = FALSE
  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "toolbar_icon_category_id_combo_list() - p_cb_field_name=", p_cb_field_name
    DISPLAY "toolbar_icon_category_id_combo_list() - p_language_id=", p_language_id
  END IF

  DECLARE c_toolbar_icon_category_id_scroll2 CURSOR FOR 
    SELECT icon_category_id
      FROM qxt_icon_category
      WHERE language_id = p_language_id 
      ORDER BY icon_category_id ASC

  LET int_flag = FALSE
  LET row_count = 1

  FOREACH c_toolbar_icon_category_id_scroll2 INTO l_icon_category_id[row_count]
    CALL fgl_list_set(p_cb_field_name,row_count, l_icon_category_id[row_count])
    IF local_debug THEN
      LET tmp_str = "fgl_list_set(", p_cb_field_name CLIPPED, ",", row_count CLIPPED, ",", l_icon_category_id[row_count] CLIPPED, ")"
      DISPLAY "toolbar_icon_category_id_combo_list()", tmp_str
    END IF

    LET row_count = row_count + 1
  END FOREACH

  LET row_count = row_count - 1


END FUNCTION



######################################################
# FUNCTION icon_category_popup_data_source()
#
# Data Source (cursor) for icon_category_popup
#
# RETURN NONE
######################################################
FUNCTION icon_category_popup_data_source(p_filter,p_language_id,p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    p_language_id       LIKE qxt_language.language_id,
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    p_filter            SMALLINT,
    local_debug         SMALLINT

  LET local_debug = FALSE
  
  IF local_debug THEN
    DISPLAY "icon_category_popup_data_source() - p_filter=", p_filter
    DISPLAY "icon_category_popup_data_source() - p_language_id=", p_language_id
    DISPLAY "icon_category_popup_data_source() - p_order_field=", p_order_field
    DISPLAY "icon_category_popup_data_source() - p_ord_dir=", p_ord_dir

  ENd IF

  IF p_order_field IS NULL  THEN
    LET p_order_field = "icon_category_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  IF p_filter IS NULL THEN
    LET p_filter = FALSE
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_icon_category.icon_category_id, ",
                 "qxt_language.language_name, ",
                 "qxt_icon_category.icon_category_data ",

                 "FROM ",
                 "qxt_icon_category, ",
                 "qxt_language ",
                 "WHERE ",
                 "qxt_icon_category.language_id = qxt_language.language_id "

  IF p_filter AND p_language_id IS NOT NULL AND p_language_id > 0 THEN
    LET sql_stmt = trim(sql_stmt), " ", 
                 "AND ",
                 "qxt_icon_category.language_id = ", trim(p_language_id) , " "
  END IF
                    


  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED,  " ,qxt_icon_category.language_id "
  END IF

  IF local_debug THEN
    DISPLAY "icon_category_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
    DISPLAY sql_stmt[501,600]
  END IF

  PREPARE p_icon_category FROM sql_stmt
  DECLARE c_icon_category CURSOR FOR p_icon_category

END FUNCTION


######################################################
# FUNCTION icon_category_popup(p_icon_category_id,p_order_field,p_accept_action)
#
# icon_category selection window
#
# RETURN p_icon_category_id
######################################################
FUNCTION icon_category_popup(p_icon_category_id, p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_icon_category_id           LIKE qxt_icon_category.icon_category_id,  --default return value if user cancels
    p_language_id                LIKE qxt_language.language_id,
    l_icon_category_arr          DYNAMIC ARRAY OF t_qxt_icon_category_form_rec,    --RECORD LIKE qxt_icon_category.*,
    l_arr_size                   SMALLINT,  
    i                            INTEGER,
    p_accept_action              SMALLINT,
    err_msg                      VARCHAR(240),
    p_order_field,p_order_field2 VARCHAR(128), 
    l_icon_category_id           LIKE qxt_icon_category.icon_category_id,
    l_language_id                LIKE qxt_language.language_id,
    l_language_name              LIKE qxt_language.language_name,
    current_row                  SMALLINT,  --to jump to last modified array line after edit/input
    local_debug                  SMALLINT,
    #l_lang_name                  VARCHAR(30),
    l_filter                     SMALLINT,
    l_icon_cat                   RECORD LIKE qxt_icon_category.*

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""
  LET current_row = 1
  LET l_arr_size = 200 -- due to dynamic array ...sizeof(l_icon_category_arr)
  IF p_language_id IS NULL OR p_language_id = 0 THEN
    LET l_filter = FALSE
    #LET p_language_id = get_language()
  ELSE
    LET l_language_name = get_language_name(p_language_id)
    LET l_filter = TRUE
  END IF

  IF p_accept_action IS NULL THEN
    LET p_accept_action = 0
  END IF

  IF local_debug THEN
    DISPLAY "icon_category_popup() - p_icon_category_id=",p_icon_category_id
    DISPLAY "icon_category_popup() - p_language_id=",p_language_id
    DISPLAY "icon_category_popup() - p_order_field=",p_order_field
    DISPLAY "icon_category_popup() - p_accept_action=",p_accept_action
  END IF


	CALL fgl_window_open("w_icon_category_scroll", 2, 8, get_form_path("f_qxt_icon_category_scroll_l2"),FALSE) 
	CALL populate_icon_category_list_form_labels_g()
	DISPLAY l_language_name TO combo_lang


  #If table is empty, ask, if the user wants to create a new record
  LET i = 0
  SELECT COUNT(*)
    INTO i
    FROM qxt_icon_category

  IF i < 1 THEN
    CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
    #IF question_not_exist_create(NULL) THEN
      CALL icon_category_create(NULL,NULL,NULL)
    #END IF
  END IF


  #Loop for the permanent grid update i.e. sort or new or delete etc..
  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL icon_category_popup_data_source(l_filter,p_language_id,p_order_field,get_toggle_switch())

    LET i = 1
    FOREACH c_icon_category INTO l_icon_category_arr[i].*
      IF local_debug THEN
        DISPLAY "icon_category_popup() - i=",i
        DISPLAY "icon_category_popup() - l_icon_category_arr[i].icon_category_id=",l_icon_category_arr[i].icon_category_id
        DISPLAY "icon_category_popup() - l_icon_category_arr[i].language_name=",l_icon_category_arr[i].language_name
        DISPLAY "icon_category_popup() - l_icon_category_arr[i].icon_category_data=",l_icon_category_arr[i].icon_category_data

      END IF
      
      #determine current row for grid array
      IF l_icon_category_arr[i].icon_category_id = l_icon_category_id THEN
        IF l_icon_category_arr[i].language_name = l_language_name THEN
          LET current_row = i
        END IF
      END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > l_arr_size THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		If i > 0 THEN
			CALL l_icon_category_arr.resize(i) -- resize dynamic array to remove last dirty element
		END IF
		
    IF local_debug THEN
      DISPLAY "icon_category_popup() (before IF NOT i THEN)- i=",i
    END IF

    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_icon_category_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "icon_category_popup() - set_count(i)=",i
    END IF

		
    #CALL set_count(i)

    LET int_flag = FALSE

    DISPLAY ARRAY l_icon_category_arr TO sc_icon_category.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
      BEFORE DISPLAY
        CALL fgl_dialog_setcurrline(5,current_row)

      BEFORE ROW
        LET i = arr_curr()
        LET current_row = i
        LET l_icon_category_id = l_icon_category_arr[i].icon_category_id
        LET l_language_id = get_language_id(l_icon_category_arr[i].language_name)
        LET l_language_name = l_icon_category_arr[i].language_name

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL icon_category_view(l_icon_category_id)  --(l_icon_category_id,l_language_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL icon_category_edit(l_icon_category_id,l_language_id)
              RETURNING l_icon_cat.*

            IF l_icon_cat.icon_category_id IS NOT NULL THEN
              EXIT DISPLAY
            END IF

          WHEN 3  --delete
            IF icon_category_delete(l_icon_category_id,l_language_id) THEN
              EXIT DISPLAY
            END IF

          WHEN 4  --create/add
            CALL icon_category_create(NULL,NULL,NULL)
              RETURNING l_icon_cat.*

            IF l_icon_cat.icon_category_id IS NOT NULL THEN
              EXIT DISPLAY
            END IF

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","icon_category_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "icon_category_popup(p_icon_category_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("icon_category_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL icon_category_create(NULL,l_language_id,NULL)
          RETURNING l_icon_cat.*

        IF l_icon_cat.icon_category_id IS NOT NULL THEN
          EXIT DISPLAY
        END IF

      ON KEY (F5) -- edit
        CALL icon_category_edit(l_icon_category_id,l_language_id)
          RETURNING l_icon_cat.*

        IF l_icon_cat.icon_category_id IS NOT NULL THEN
          EXIT DISPLAY
        END IF

      ON KEY (F6) -- delete
        IF icon_category_delete(l_icon_category_id,l_language_id) THEN
          EXIT DISPLAY
        END IF
    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_category_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "language_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY      

      ON KEY(F15)
        LET p_order_field2 = p_order_field
        LET p_order_field = "icon_category_data"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F120)
        LET l_filter = TRUE
        EXIT DISPLAY 

      ON KEY(F121)
        LET l_filter = FALSE
        EXIT DISPLAY 

      ON KEY(F122)  --Specify filter criteria
        INPUT l_language_name WITHOUT DEFAULTS FROM combo_lang HELP 1
          ON KEY(F122,ACCEPT)
            CALL fgl_dialog_update_data()
            LET p_language_id = get_language_id(l_language_name)
            LET l_filter = TRUE
            EXIT INPUT
        END INPUT
        
        EXIT DISPLAY 

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_icon_category_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_icon_category_id, p_language_id , get_icon_category_data(p_icon_category_id,p_language_id)
  ELSE 
    RETURN l_icon_category_id , l_language_id, get_icon_category_data(l_icon_category_id,p_language_id)
  END IF

END FUNCTION



#icon_category_popup(p_icon_category_id, p_language_id,p_order_field,p_accept_action)

##########################################################################################################
# Record create/modify/input functions
##########################################################################################################


######################################################
# FUNCTION icon_category_create(p_id,p_language_id,p_data)
#
# Create a new icon_category record
#
# RETURN NULL
######################################################
FUNCTION icon_category_create(p_id,p_language_id,p_data)
  DEFINE 
    p_id            LIKE qxt_icon_category.icon_category_id,
    p_language_id   LIKE qxt_icon_category.language_id,
    p_data          LIKE qxt_icon_category.icon_category_data,
    l_icon_category RECORD LIKE qxt_icon_category.*,
    local_debug     SMALLINT

  LET local_debug = FALSE

  #Initialise some variables
  IF p_id IS NOT NULL THEN
    LET l_icon_category.icon_category_id = p_id
  ELSE
    LET l_icon_category.icon_category_id = get_icon_category_new_id(get_language())
  END IF

  IF p_language_id IS NOT NULL THEN
    LET l_icon_category.language_id = p_language_id
  ELSE
    LET l_icon_category.language_id = get_language()
  END IF

  IF p_data IS NOT NULL THEN
    LET l_icon_category.icon_category_data = p_data
  END IF


#select max column from table
#informix guide to sql

	CALL fgl_window_open("w_icon_category", 3, 3, get_form_path("f_qxt_icon_category_det_l2"), FALSE) 
	CALL populate_icon_category_form_create_labels_g()

  LET int_flag = FALSE
  

  IF local_debug THEN
    DISPLAY "icon_category_create() - l_icon_category.icon_category_id=", l_icon_category.icon_category_id
    DISPLAY "icon_category_create() - get_language()=", get_language()
  END IF

  # CALL the INPUT
  CALL icon_category_input(l_icon_category.*)
    RETURNING l_icon_category.*

  CALL fgl_window_close("w_icon_category")

  IF NOT int_flag THEN
    INSERT INTO qxt_icon_category VALUES (
      l_icon_category.icon_category_id,
      l_icon_category.language_id,
      l_icon_category.icon_category_data
                                      )

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL, NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_icon_category.icon_category_id, l_icon_category.language_id, l_icon_category.icon_category_data
    END IF

    #RETURN sqlca.sqlerrd[2]
  
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL, NULL, NULL
  END IF

END FUNCTION


#################################################
# FUNCTION icon_category_edit(p_icon_category_id)
#
# Edit icon_category record
#
# RETURN NONE
#################################################
FUNCTION icon_category_edit(p_icon_category_id,p_language_id)
  DEFINE 
    p_icon_category_id            LIKE qxt_icon_category.icon_category_id,
    p_language_id                 LIKE qxt_icon_category.language_id,
    l_key1_icon_category_id       LIKE qxt_icon_category.icon_category_id,
    l_key2_language_id            LIKE qxt_icon_category.language_id,

    l_icon_category_rec           RECORD LIKE qxt_icon_category.*,
    local_debug                   SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "icon_category_edit() - Function Entry Point"
    DISPLAY "icon_category_edit() - p_icon_category_id=", p_icon_category_id
    DISPLAY "icon_category_edit() - p_language_id=", p_language_id
  END IF

  #Store the primary key for the SQL update
  LET l_key1_icon_category_id = p_icon_category_id
  LET l_key2_language_id = p_language_id

  #Get the record (by id)
  CALL get_icon_category_rec(p_icon_category_id,p_language_id) RETURNING l_icon_category_rec.*


	CALL fgl_window_open("w_icon_category", 3, 3, get_form_path("f_qxt_icon_category_det_l2"), FALSE) 
	CALL populate_icon_category_form_edit_labels_g()

  #Call the INPUT
  CALL icon_category_input(l_icon_category_rec.*) 
    RETURNING l_icon_category_rec.*

  CALL fgl_window_close("w_icon_category")

  IF local_debug THEN
    DISPLAY "icon_category_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF


  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "icon_category_edit() - l_icon_category_rec.icon_category_id=",l_icon_category_rec.icon_category_id
      DISPLAY "icon_category_edit() - l_icon_category_rec.language_id=",l_icon_category_rec.language_id
      DISPLAY "icon_category_edit() - l_icon_category_rec.icon_category_data=",l_icon_category_rec.icon_category_data
      DISPLAY "icon_category_edit() - l_key1_icon_category_id=",l_key1_icon_category_id
      DISPLAY "icon_category_edit() - l_key2_language_id=",l_key2_language_id

    END IF

    UPDATE qxt_icon_category
      SET 
          icon_category_id   = l_icon_category_rec.icon_category_id,
          language_id        = l_icon_category_rec.language_id,
          icon_category_data = l_icon_category_rec.icon_category_data
      WHERE icon_category_id = l_key1_icon_category_id
        AND language_id      = l_key2_language_id


    IF local_debug THEN
      DISPLAY "icon_category_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(835),NULL)
      RETURN NULL, NULL, NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(835),NULL)
      RETURN l_icon_category_rec.icon_category_id, l_icon_category_rec.language_id, l_icon_category_rec.icon_category_data 
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(835),NULL)
    LET int_flag = FALSE
    RETURN NULL, NULL, NULL
  END IF

END FUNCTION


#################################################
# FUNCTION icon_category_input(p_icon_category_rec)
#
# Input icon_category details (edit/create)
#
# RETURN l_icon_category.*
#################################################
FUNCTION icon_category_input(p_icon_category_rec)
  DEFINE 
    p_icon_category_rec            RECORD LIKE qxt_icon_category.*,
    l_icon_category_form_rec       OF t_qxt_icon_category_form_rec,
    l_orignal_icon_category_id     LIKE qxt_icon_category.icon_category_id,
    l_orignal_language_id          LIKE qxt_icon_category.language_id,
    local_debug                    SMALLINT,
    tmp_str                        VARCHAR(250)

  LET local_debug = FALSE

  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "icon_category_input() - p_icon_category_rec.icon_category_id=",p_icon_category_rec.icon_category_id
    DISPLAY "icon_category_input() - p_icon_category_rec.language_id=",p_icon_category_rec.language_id
    DISPLAY "icon_category_input() - p_icon_category_rec.icon_category_data=",p_icon_category_rec.icon_category_data
  END IF

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_icon_category_id = p_icon_category_rec.icon_category_id
  LET l_orignal_language_id = p_icon_category_rec.language_id

  #copy record data to form_record format record
  CALL copy_icon_category_record_to_form_record(p_icon_category_rec.*) RETURNING l_icon_category_form_rec.*


  ####################
  #Start actual INPUT
  INPUT BY NAME l_icon_category_form_rec.* WITHOUT DEFAULTS HELP 1

    BEFORE INPUT
      NEXT FIELD icon_category_data


    AFTER FIELD icon_category_id
      #id must not be empty / NULL / or 0
      IF NOT validate_field_value_numeric_exists(get_str_tool(836), l_icon_category_form_rec.icon_category_id,NULL,TRUE)  THEN
        NEXT FIELD icon_category_id
      END IF

    AFTER FIELD language_name
      #language_name must not be empty / NULL 
      IF NOT validate_field_value_string_exists(get_str_tool(837), l_icon_category_form_rec.language_name,NULL,TRUE)  THEN
        NEXT FIELD icon_category_id
      END IF

    AFTER FIELD icon_category_data
      #language_id must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(838), l_icon_category_form_rec.icon_category_data,NULL,TRUE)  THEN
        NEXT FIELD icon_category_data
      END IF




    # AFTER INPUT BLOCK ####################################
    AFTER INPUT

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #icon_category_id must not be empty
        IF NOT validate_field_value_numeric_exists(get_str_tool(836), l_icon_category_form_rec.icon_category_id,NULL,TRUE)  THEN
          NEXT FIELD icon_category_id
          CONTINUE INPUT
        END IF

      #language_name must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(837), l_icon_category_form_rec.language_name,NULL,TRUE)  THEN
          NEXT FIELD icon_category_id
          CONTINUE INPUT
        END IF

      #icon_category_data must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(838), l_icon_category_form_rec.icon_category_data,NULL,TRUE)  THEN

          NEXT FIELD icon_category_data
          CONTINUE INPUT
        END IF


      #The icon_category_id and language_id must be unique - Double Primary KEY
      IF icon_category_id_and_language_id_count(l_icon_category_form_rec.icon_category_id,get_language_id(l_icon_category_form_rec.language_name)) THEN
        #Constraint only exists for newly created records (not modify)
 
       IF NOT (l_orignal_icon_category_id = l_icon_category_form_rec.icon_category_id AND l_orignal_language_id = get_language_id(l_icon_category_form_rec.language_name) )THEN  --it is not an edit operation
          CALL  tl_msg_duplicate_doublekey(get_str_tool(836),get_str_tool(837),NULL)
          NEXT FIELD icon_category_id
          CONTINUE INPUT
        END IF
      END IF




  END INPUT
  # END INOUT BLOCK  ##########################


  IF local_debug THEN
    DISPLAY "icon_category_input() - l_icon_category_form_rec.icon_category_id=",l_icon_category_form_rec.icon_category_id
    DISPLAY "icon_category_input() - l_icon_category_form_rec.language_name=",l_icon_category_form_rec.language_name
    DISPLAY "icon_category_input() - l_icon_category_form_rec.icon_category_data=",l_icon_category_form_rec.icon_category_data
  END IF


  #Copy the form record data to a normal icon_category record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_icon_category_form_record_to_record(l_icon_category_form_rec.*) RETURNING p_icon_category_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "icon_category_input() - p_icon_category_rec.icon_category_id=",p_icon_category_rec.icon_category_id
    DISPLAY "icon_category_input() - p_icon_category_rec.language_id=",p_icon_category_rec.language_id
    DISPLAY "icon_category_input() - p_icon_category_rec.icon_category_data=",p_icon_category_rec.icon_category_data
  END IF

  RETURN p_icon_category_rec.*

END FUNCTION


#################################################
# FUNCTION icon_category_delete(p_icon_category_id)
#
# Delete a icon_category record
#
# RETURN NONE
#################################################
FUNCTION icon_category_delete(p_icon_category_id,p_language_id)
  DEFINE 
    p_icon_category_id       LIKE qxt_icon_category.icon_category_id,
    p_language_id            LIKE qxt_icon_category.language_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(835), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_icon_category 
        WHERE icon_category_id = p_icon_category_id
        AND language_id = p_language_id
    COMMIT WORK

    RETURN TRUE
  ELSE
    RETURN FALSE
  END IF

END FUNCTION


#################################################
# FUNCTION icon_category_view(p_icon_category_id)
#
# View icon_category record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_category_view(p_icon_category_id)
  DEFINE 
    p_icon_category_id             LIKE qxt_icon_category.icon_category_id,
    l_icon_category_rec    RECORD LIKE qxt_icon_category.*


  CALL get_icon_category_rec(p_icon_category_id,get_language()) RETURNING l_icon_category_rec.*
  CALL icon_category_view_by_rec(l_icon_category_rec.*)

END FUNCTION


#################################################
# FUNCTION icon_category_view_by_rec(p_icon_category_rec)
#
# View icon_category record in window-form
#
# RETURN NONE
#################################################
FUNCTION icon_category_view_by_rec(p_icon_category_rec)
  DEFINE 
    p_icon_category_rec     RECORD LIKE qxt_icon_category.*,
    inp_char                 CHAR,
    tmp_str                  VARCHAR(250)

	CALL fgl_window_open("w_icon_category", 3, 3, get_form_path("f_qxt_icon_category_det_l2"), FALSE) 
	CALL populate_icon_category_form_view_labels_g()

  DISPLAY BY NAME p_icon_category_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_icon_category")
END FUNCTION




##########################################################################################################
# Validation-Coun/Exists functions
##########################################################################################################


####################################################
# FUNCTION icon_category_id_and_language_id_count(p_icon_category_id, p_language_id)
#
# tests if a record with this icon_category_id and language_id already exists
#
# RETURN r_count
####################################################
FUNCTION icon_category_id_and_language_id_count(p_icon_category_id, p_language_id)
  DEFINE
    p_icon_category_id        LIKE qxt_icon_category.icon_category_id,
    p_language_id             LIKE qxt_icon_category.language_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon_category
      WHERE qxt_icon_category.icon_category_id = p_icon_category_id
        AND qxt_icon_category.language_id = p_language_id
  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION icon_category_data_count(p_icon_category_data)
#
# tests if a record with this language_id already exists
#
# RETURN r_count
####################################################
FUNCTION icon_category_data_count(p_icon_category_data)
  DEFINE
    p_icon_category_data  LIKE qxt_icon_category.icon_category_data,
    r_count               SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon_category
      WHERE qxt_icon_category.icon_category_data = p_icon_category_data

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FFUNCTION icon_category_id_count(p_icon_category_id)
#
# tests if a record with this icon_category_id already exists
#
# RETURN r_count
####################################################
FUNCTION icon_category_id_count(p_icon_category_id)
  DEFINE
    p_icon_category_id    LIKE qxt_icon_category.icon_category_id,
    r_count                   SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_icon_category
      WHERE qxt_icon_category.icon_category_id = p_icon_category_id

  RETURN r_count   --0 = unique  0> is not

END FUNCTION


###########################################################################################################
# Normal Record to Form Record data copy functions
###########################################################################################################

######################################################
# FUNCTION copy_icon_category_record_to_form_record(p_icon_category_rec)  
#
# Copy normal icon_category record data to type icon_category_form_rec
#
# RETURN l_icon_category_form_rec.*
######################################################
FUNCTION copy_icon_category_record_to_form_record(p_icon_category_rec)  
  DEFINE
    p_icon_category_rec       RECORD LIKE qxt_icon_category.*,
    l_icon_category_form_rec  OF t_qxt_icon_category_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_icon_category_form_rec.icon_category_id   = p_icon_category_rec.icon_category_id
  LET l_icon_category_form_rec.language_name      = get_language_name(p_icon_category_rec.language_id)
  LET l_icon_category_form_rec.icon_category_data = p_icon_category_rec.icon_category_data

  IF local_debug THEN
    DISPLAY "copy_icon_category_record_to_form_record() - p_icon_category_rec.icon_category_id=",   p_icon_category_rec.icon_category_id
    DISPLAY "copy_icon_category_record_to_form_record() - p_icon_category_rec.language_id=",        p_icon_category_rec.language_id
    DISPLAY "copy_icon_category_record_to_form_record() - p_icon_category_rec.icon_category_data=", p_icon_category_rec.icon_category_data

    DISPLAY "copy_icon_category_record_to_form_record() - l_icon_category_form_rec.icon_category_id=", l_icon_category_form_rec.icon_category_id
    DISPLAY "copy_icon_category_record_to_form_record() - l_icon_category_form_rec.icon_category_id=", l_icon_category_form_rec.language_name
    DISPLAY "copy_icon_category_record_to_form_record() - l_icon_category_form_rec.icon_category_id=", l_icon_category_form_rec.icon_category_data
  END IF

  RETURN l_icon_category_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_icon_category_form_record_to_record(p_icon_category_form_rec)  
#
# Copy type icon_category_form_rec to normal icon_category record data
#
# RETURN l_icon_category_rec.*
######################################################
FUNCTION copy_icon_category_form_record_to_record(p_icon_category_form_rec)  
  DEFINE
    l_icon_category_rec       RECORD LIKE qxt_icon_category.*,
    p_icon_category_form_rec  OF t_qxt_icon_category_form_rec,
    local_debug               SMALLINT

  LET local_debug = FALSE

  LET l_icon_category_rec.icon_category_id   = p_icon_category_form_rec.icon_category_id
  LET l_icon_category_rec.language_id        = get_language_id(p_icon_category_form_rec.language_name)
  LET l_icon_category_rec.icon_category_data = p_icon_category_form_rec.icon_category_data

  IF local_debug THEN
    DISPLAY "copy_icon_category_form_record_to_record() - p_icon_category_form_rec.icon_category_id=",   p_icon_category_form_rec.icon_category_id
    DISPLAY "copy_icon_category_form_record_to_record() - p_icon_category_form_rec.language_id=",        p_icon_category_form_rec.language_name
    DISPLAY "copy_icon_category_form_record_to_record() - p_icon_category_form_rec.icon_category_data=", p_icon_category_form_rec.icon_category_data

    DISPLAY "copy_icon_category_form_record_to_record() - l_icon_category_rec.icon_category_id=", l_icon_category_rec.icon_category_id
    DISPLAY "copy_icon_category_form_record_to_record() - l_icon_category_rec.icon_category_id=", l_icon_category_rec.language_id
    DISPLAY "copy_icon_category_form_record_to_record() - l_icon_category_rec.icon_category_id=", l_icon_category_rec.icon_category_data
  END IF


  RETURN l_icon_category_rec.*

END FUNCTION



######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_icon_category_scroll()
#
# Populate icon_category grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_icon_category_scroll()
  CALL fgl_grid_header("sc_icon_category","icon_category_id",  get_str_tool(836),"center","F13")  --icon_category
  CALL fgl_grid_header("sc_icon_category","language_name",     get_str_tool(837),"left", "F14")  --file language_id
  CALL fgl_grid_header("sc_icon_category","icon_category_data",get_str_tool(838),"left","F15")  --icon_category_data

END FUNCTION



#######################################################
# FUNCTION populate_icon_category_form_labels_g()
#
# Populate icon_category form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(835) TO lbTitle

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION

{

#######################################################
# FUNCTION populate_icon_category_form_labels_t()
#
# Populate icon_category form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_labels_t()

  DISPLAY get_str_tool(835) TO lbTitle

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION

}
#######################################################
# FUNCTION populate_icon_category_form_edit_labels_g()
#
# Populate icon_category form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(835)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel


  CALL language_combo_list("language_name")



END FUNCTION


{
#######################################################
# FUNCTION populate_icon_category_form_edit_labels_t()
#
# Populate icon_category form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_edit_labels_t()

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION

}
#######################################################
# FUNCTION populate_icon_category_form_view_labels_g()
#
# Populate icon_category form view labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(835)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION


{
#######################################################
# FUNCTION populate_icon_category_form_view_labels_t()
#
# Populate icon_category form view labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_view_labels_t()

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4

  DISPLAY get_str_tool(402) TO lbInfo1

  CALL language_combo_list("language_language_id")

END FUNCTION

}
#######################################################
# FUNCTION populate_icon_category_form_create_labels_g()
#
# Populate icon_category form create labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(835)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  CALL language_combo_list("language_name")

END FUNCTION


{
#######################################################
# FUNCTION populate_icon_category_form_create_labels_t()
#
# Populate icon_category form create labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_form_create_labels_t()

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION

}
#######################################################
# FUNCTION populate_icon_category_list_form_labels_g()
#
# Populate icon_category list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_list_form_labels_g()
  CALL updateUILabel("language_filter","Filter")
  CALL fgl_settitle(trim(get_str_tool(835)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_icon_category_scroll()

  #DISPLAY get_str_tool(836) TO dl_f1
  #DISPLAY get_str_tool(837) TO dl_f2
  #DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4


  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  #DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  #DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  #DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  #DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  #DISPLAY "!" TO bt_delete

  #DISPLAY "!" TO language_filter
  #DISPLAY "!" TO bt_set_filter_criteria

  CALL language_combo_list("combo_lang")

END FUNCTION

{
#######################################################
# FUNCTION populate_icon_category_list_form_labels_t()
#
# Populate icon_category list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_icon_category_list_form_labels_t()

  DISPLAY trim(get_str_tool(835)) || " - " || get_str_tool(16) TO lbTitle  --List

  DISPLAY get_str_tool(836) TO dl_f1
  DISPLAY get_str_tool(837) TO dl_f2
  DISPLAY get_str_tool(838) TO dl_f3
  #DISPLAY get_str_tool(839) TO dl_f4

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION
}
###########################################################################################################################
# EOF
###########################################################################################################################






