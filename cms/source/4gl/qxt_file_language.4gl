##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

############################################################
# Globals
############################################################
GLOBALS "qxt_file_language_globals.4gl"

######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_language_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_language_lib_info()
  RETURN "qxt_file_language"
END FUNCTION 

##################################################################################################
# Init & Data import functions
##################################################################################################


############################################################
# FUNCTION init_languages()
#
# Initialises Language settings
#
# RETURN NONE
############################################################
FUNCTION init_language_list(p_file_name)
  DEFINE 
    p_file_name VARCHAR(50)

  LET qxt_lang_array_count = import_languages_from_file(get_unl_path(p_file_name))

END FUNCTION


#############################################################
# FUNCTION import_languages_from_file(lang_file_name)
#
# Import languages from a unl file
#
# RETURN qxt_language_rec[p_lang_id].language
#############################################################
FUNCTION import_languages_from_file(lang_file_name)
  DEFINE
    lang_file_name        VARCHAR(100),
    l_lang_tmp_rec        OF t_qxt_language_rec,
    id                    SMALLINT,
    local_debug           SMALLINT,
    tmp_str               VARCHAR(200)

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "import_languages_from_file() - lang_file_name=", lang_file_name
  END IF

  IF validate_file_server_side_exists_advanced(lang_file_name,"f",1) THEN

    IF NOT fgl_channel_open_file("stream",lang_file_name, "r") THEN
      LET tmp_str = "Error in import_languages_from_file()\nCannot open file ", trim(lang_file_name)
      CALL fgl_winmessage("Error in import_languages_from_file()", tmp_str, "error")
      RETURN
    END IF

    IF NOT fgl_channel_set_delimiter("stream","|") THEN
      LET tmp_str = "Error in import_languages_from_file()\nCannot set delimiter for file ", trim(lang_file_name)
      CALL fgl_winmessage("Error in import_languages_from_file()", tmp_str, "error")
      RETURN
    END IF

  
    LET id = 0
    WHILE fgl_channel_read("stream",l_lang_tmp_rec.*) 
      IF l_lang_tmp_rec.language_id THEN
        LET id = id + 1
        LET qxt_language_rec[id].language_id   =    l_lang_tmp_rec.language_id
        LET qxt_language_rec[id].language_name =    l_lang_tmp_rec.language_name
        LET qxt_language_rec[id].language_dir  =    l_lang_tmp_rec.language_dir
        LET qxt_language_rec[id].language_url  =    l_lang_tmp_rec.language_url
      ELSE
        EXIT WHILE
      END IF

    END WHILE
    LET qxt_lang_array_count = id

    IF NOT fgl_channel_close("stream") THEN
      LET tmp_str = "Error in import_languages_from_file()\nCannot close file ", trim(lang_file_name)
      CALL fgl_winmessage("Error in import_languages_from_file()", tmp_str, "error")
      RETURN
    END IF

  END IF

  IF local_debug THEN
    DISPLAY "import_languages_from_file() - End OF Function"
  END IF

  RETURN qxt_lang_array_count
END FUNCTION



##################################################################################################
# Data Access functions
##################################################################################################

#############################################################
# FUNCTION get_language_id(p_lang)
#
# Returns the language_id from a language (name)
#
# RETURN qxt_language_rec[id].language_id
#############################################################
FUNCTION get_language_id(p_lang)
  DEFINE 
    p_lang VARCHAR(30),
    id        SMALLINT

  LET p_lang = downshift(p_lang)

  FOR id = 1 TO qxt_lang_array_count
    IF p_lang = downshift(qxt_language_rec[id].language_name) THEN
      RETURN qxt_language_rec[id].language_id
    END IF
  END FOR

  CALL fgl_winmessage("Error in get_language_id()","Could not find language_id for p_lang=" || p_lang,"error")

  RETURN 0

END FUNCTION


#############################################################
# FUNCTION get_language_name(p_lang_id)
#
# Returns the language (name) from from a language_id
#
# RETURN qxt_language_rec[p_lang_id].language
#############################################################
FUNCTION get_language_name(p_lang_id)
  DEFINE 
    p_lang_id SMALLINT

  IF p_lang_id > 0 AND p_lang_id <= qxt_lang_array_count THEN
    RETURN qxt_language_rec[p_lang_id].language_name
  END IF

  RETURN "Error-Language not found"
END FUNCTION


##################################################################################
# FUNCTION get_language_dir_path(p_language_id,p_filename)
#
# Return file name with the language directory i.e. sp/mytext.txt for spanish
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_language_dir_path(p_language_id,p_filename)
  DEFINE
    p_language_id SMALLINT,
    p_filename    VARCHAR(100),
    ret           VARCHAR(200)

    LET ret = trim(get_language_dir(get_language())), "/", trim(p_filename)
  RETURN trim(ret)
END FUNCTION


##################################################################################
# FUNCTION get_language_ext(file_name)
#
# Return file name with the language directory i.e. help -> help_sp for spanish
#
# RETURN trim(ret)
##################################################################################
FUNCTION get_language_ext(file_name)
  DEFINE
    file_name VARCHAR(100),
    ret VARCHAR(200)

    LET ret = trim(file_name), "_", trim(get_language_dir(get_language()))

  RETURN trim(ret)
END FUNCTION




#############################################################
# FUNCTION get_language_dir(p_lang_id)
#
# Returns the language directory from from a language_id
#
# RETURN qxt_language_rec[p_lang_id].language
#############################################################
FUNCTION get_language_dir(p_lang_id)
  DEFINE 
    p_lang_id SMALLINT

  IF p_lang_id > 0 AND p_lang_id <= qxt_lang_array_count THEN
    RETURN qxt_language_rec[p_lang_id].language_dir
  END IF

  RETURN "Error-Language not found - get_language_dir()"
END FUNCTION


#############################################################
# FUNCTION get_language_url(p_lang_id)
#
# Returns the language url from from a language_id
#
# RETURN qxt_language_rec[p_lang_id].language
#############################################################
FUNCTION get_language_url(p_lang_id)
  DEFINE 
    p_lang_id SMALLINT

  IF p_lang_id > 0 AND p_lang_id <= qxt_lang_array_count THEN
    RETURN qxt_language_rec[p_lang_id].language_url
  END IF

  RETURN "Error-Language not found - get_language_url()"
END FUNCTION

#############################################################
# FUNCTION get_language_url_path(p_lang_id)
#
# Returns the language url from from a language_id
#
# RETURN qxt_language_rec[p_lang_id].language
#############################################################
FUNCTION get_language_url_path(p_lang_id,p_url)
  DEFINE 
    p_lang_id SMALLINT,
    p_url,ret VARCHAR(200),
    err_msg   VARCHAR(200),
    local_debug SMALLINT,
    a SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_language_url_path() - p_lang_id=", p_lang_id
    DISPLAY "get_language_url_path() - p_url=", p_url
  END IF

  IF p_lang_id > 0 AND p_lang_id <= qxt_lang_array_count THEN
    IF qxt_language_rec[p_lang_id].language_url IS NOT NULL THEN
      LET ret = trim(qxt_language_rec[p_lang_id].language_url), "/", trim(p_url)
    ELSE
      LET ret = p_url
    END IF
    IF local_debug THEN
      DISPLAY "get_language_url_path() - ret=", ret
    END IF

    RETURN ret
  ELSE
    LET err_msg = "Invalid p_lang_id = ", p_lang_id CLIPPED
    CALL fgl_winmessage("Error in get_language_url_path()", err_msg, "error")
    RETURN NULL   
  END IF


END FUNCTION

##################################################################################################
# List & Combo Box functions
##################################################################################################


###########################################################
# language_popup(p_language_id,p_order_field,p_accept_action)
#
# displays selection list and returns the language_id
#
# RETURN l_language_arr[i].language_id or p_language_id
###########################################################
FUNCTION language_popup(p_language_id,p_order_field,p_accept_action)
  DEFINE 
    p_language_id                 INTEGER,
    p_accept_action               SMALLINT,
    p_order_field                 VARCHAR(128)

  CALL fgl_winmessage("DB-Tools feature only","This feature is not available in this file library","info")

END FUNCTION


###################################################################################
# FUNCTION language_combo_list(cb_field_name)
#
# Populates the combo box with languages
# Note: uses/requires tool_lib_db OR tool_lib_txt
#
# RETURN NONE
###################################################################################
FUNCTION language_combo_list(cb_field_name)
  DEFINE 
    id            INTEGER,
    cb_field_name VARCHAR(20),   --form field name for the country combo list field
    local_debug   SMALLINT

  LET local_debug = FALSE
  LET int_flag = FALSE

  IF local_debug THEN
    DISPLAY "language_combo_list() - cb_field_name =", cb_field_name
    DISPLAY "language_combo_list() - qxt_lang_array_count =", qxt_lang_array_count
  END IF

  FOR id = 1 TO qxt_lang_array_count
    CALL fgl_list_set(cb_field_name,qxt_language_rec[id].language_id, qxt_language_rec[id].language_name)
    IF local_debug THEN
      DISPLAY "language_combo_list() - fgl_list_set(", trim(cb_field_name), ",", trim(qxt_language_rec[id].language_id), ",",  trim(qxt_language_rec[id].language_name), ")"  
    END IF
  END FOR

END FUNCTION



