##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

#########################################################################################################
# Get the html file from the server file system
#
#
# FUNCTION                                      DESCRIPTION                                              RETURN
# get_help_url_file                             Get the help_file from the server to the client          p_client_file_path
# (p_help_id,p_filename,
# p_server_file_path,p_client_file_path)
#
# 
#
#########################################################################################################


############################################################
# Globals
############################################################
GLOBALS "qxt_db_print_image_manager_globals.4gl"


######################################################################################################################
# Library info - used for link testing
######################################################################################################################

###########################################################
# FUNCTION get_document_manager_lib_info()
#
# Simply returns libray name / info
#
# RETURN NONE
###########################################################
FUNCTION get_print_image_manager_lib_info()
  RETURN "qxt_print_image_manager"
END FUNCTION 



######################################################################################################################
# Data Access functions
######################################################################################################################
#########################################################
# FUNCTION get_image_id(p_filename)
#
# get print_image id from name
#
# RETURN l_image_id
#########################################################
FUNCTION get_image_id(p_filename)
  DEFINE 
    p_filename       LIKE qxt_print_image.filename,
    l_image_id       LIKE qxt_print_image.image_id,
    local_debug      SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_image_id() - p_filename = ", p_filename
  END IF

  SELECT image_id
    INTO l_image_id
    FROM qxt_print_image
    WHERE filename = p_filename

  RETURN l_image_id
END FUNCTION


#########################################################
# FUNCTION get_image_data(p_image_id)
#
# Get the print_image TEXT description
#
# RETURN l_image_data
#########################################################
FUNCTION get_image_data(p_image_id)
  DEFINE 
    l_image_data  LIKE qxt_print_image.image_data,
    p_image_id    LIKE qxt_print_image.image_id

  LOCATE l_image_data IN MEMORY

  SELECT image_data
    INTO l_image_data
    FROM qxt_print_image
    WHERE image_id = p_image_id

  RETURN l_image_data
END FUNCTION


######################################################
# FUNCTION get_image_id_from_filename(p_filename)
#
# Get the image_id from a file
#
# RETURN l_image_id
######################################################
FUNCTION get_image_id_from_filename(p_filename)
  DEFINE 
    p_filename     LIKE qxt_print_image.filename,
    l_image_id     LIKE qxt_print_image.image_id,
    local_debug    SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "get_image_id() - filename = ", p_filename
  END IF

    SELECT image_id
      INTO l_image_id
      FROM qxt_print_image
      WHERE filename = p_filename

  IF local_debug THEN
    DISPLAY "get_image_id() - l_image_id = ", l_image_id
  END IF

  RETURN l_image_id

END FUNCTION


####################################################
# FUNCTION image_filename_count(p_filename)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION image_filename_count(p_filename)
  DEFINE
    p_filename     LIKE qxt_print_image.filename,
    r_count        SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_print_image
      WHERE filename = p_filename

RETURN r_count   --0 = unique  0> is not

END FUNCTION


####################################################
# FUNCTION image_id_count(p_image_id)
#
# tests if a record with this name already exists
#
# RETURN r_count
####################################################
FUNCTION image_id_count(p_image_id)
  DEFINE
    p_image_id        LIKE qxt_print_image.image_id,
    r_count           SMALLINT

    SELECT COUNT(*)
      INTO r_count
      FROM qxt_print_image
      WHERE image_id = p_image_id

RETURN r_count   --0 = unique  0> is not

END FUNCTION



######################################################
# FUNCTION get_print_image_rec(p_image_id)
#
# Get the print_image record from pa_method_id
#
# RETURN l_print_image.*
######################################################
FUNCTION get_print_image_rec(p_image_id)
  DEFINE 
    p_image_id        LIKE qxt_print_image.image_id,
    l_print_image     RECORD LIKE qxt_print_image.*

  LOCATE l_print_image.image_data IN MEMORY

  SELECT qxt_print_image.*
    INTO l_print_image.*
    FROM qxt_print_image
    WHERE image_id = p_image_id

  RETURN l_print_image.*
END FUNCTION



######################################################################################################################
# List Management and Combo List functins
######################################################################################################################

######################################################
# FUNCTION print_image_popup_data_source()
#
# Data Source (cursor) for print_image_popup
#
# RETURN NONE
######################################################
FUNCTION print_image_popup_data_source(p_order_field,p_ord_dir)
  DEFINE 
    p_order_field       VARCHAR(128),
    sql_stmt            CHAR(2048),
    p_ord_dir           SMALLINT,
    p_ord_dir_str       VARCHAR(4),
    local_debug         SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "image_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT ",
                 "qxt_print_image.image_id, ",
                 "qxt_print_image.filename, ",
                 "qxt_print_image.mod_date ",
                 "FROM qxt_print_image "

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "print_image_popup_data_source()"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
  END IF

  PREPARE p_print_image FROM sql_stmt
  DECLARE c_print_image CURSOR FOR p_print_image

END FUNCTION


######################################################
# FUNCTION print_image_popup(p_image_id,p_order_field,p_accept_action)
#
# print_image selection window
#
# RETURN p_image_id
######################################################
FUNCTION print_image_popup(p_image_id,p_order_field,p_accept_action)
  DEFINE 
    p_image_id                    LIKE qxt_print_image.image_id,  --default return value if user cancels
    l_print_image_arr             DYNAMIC ARRAY OF t_qxt_print_image_no_blob_rec,    --RECORD LIKE qxt_print_image.*,  
    i                             INTEGER,
    p_accept_action               SMALLINT,
    err_msg                       VARCHAR(240),
    p_order_field,p_order_field2  VARCHAR(128), 
    l_image_id                    LIKE qxt_print_image.image_id,
    local_debug                   SMALLINT

  LET local_debug = FALSE
  CALL toggle_switch_on()   --default is ASC
  LET p_order_field2 = ""


  IF local_debug THEN
    DISPLAY "print_image_popup() - p_image_id=",p_image_id
    DISPLAY "print_image_popup() - p_order_field=",p_order_field
    DISPLAY "print_image_popup() - p_accept_action=",p_accept_action
  END IF


  IF fgl_fglgui() THEN  --gui
    CALL fgl_window_open("w_print_image_scroll", 2, 8, get_form_path("f_qxt_print_image_scroll_g"),FALSE) 
    CALL populate_print_image_list_form_labels_g()
  ELSE  --text
    CALL fgl_window_open("w_print_image_scroll", 2, 8, get_form_path("f_qxt_print_image_scroll_t"),FALSE) 
    CALL populate_print_image_list_form_labels_t()
  END IF


  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL print_image_popup_data_source(p_order_field,get_toggle_switch())


    LET i = 1
    FOREACH c_print_image INTO l_print_image_arr[i].*
      IF local_debug THEN
        DISPLAY "print_image_popup() - i=",i
        DISPLAY "print_image_popup() - l_print_image_arr[i].image_id=",l_print_image_arr[i].image_id
        DISPLAY "print_image_popup() - l_print_image_arr[i].filename=",l_print_image_arr[i].filename
        DISPLAY "print_image_popup() - l_print_image_arr[i].mod_date=",l_print_image_arr[i].mod_date
    END IF

      LET i = i + 1

      #Array is limited to 100 elements
      IF i > 100 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1
    
		If i > 0 THEN
			CALL l_print_image_arr.resize(i)-- resize dynamic array to remove last dirty element
		END IF    

		
    IF NOT i THEN
      CALL fgl_winmessage(get_str_tool(61),get_str_tool(62),"error")
      #CALL fgl_window_close("w_print_image_scroll")
      #RETURN NULL
    END IF

    IF local_debug THEN
      DISPLAY "print_image_popup() - set_count(i)=",i
    END IF


    #CALL set_count(i)
    LET int_flag = FALSE
    DISPLAY ARRAY l_print_image_arr TO sc_print_image.*
     ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE")
 
      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)
        LET i = arr_curr()
        LET l_image_id = l_print_image_arr[i].image_id
        #LET i = l_print_image_arr[i].image_id

        CASE p_accept_action
          WHEN 0 --just return id
            EXIT WHILE
        
          WHEN 1  --view
            CALL print_image_view(l_image_id)
            EXIT DISPLAY

          WHEN 2  --edit
            CALL print_image_edit(l_image_id)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL print_image_delete(l_image_id)
            EXIT DISPLAY

          WHEN 4  --create/add
            CALL print_image_create()
            EXIT DISPLAY

          WHEN 5  --print
            CALL fgl_winmessage("Not implemented","print_image_popup()\noperator Print is not implemented","info")
            #CALL industry_type_print(i)
        

          OTHERWISE
            LET err_msg = "print_image_popup(p_image_id,p_order_field, p_accept_action = " ,p_accept_action
            CALL fgl_winmessage("print_image_popup() - 4GL Source Error",err_msg, "error") 
        END CASE

      ON KEY (F4) -- add
        CALL print_image_create()
        EXIT DISPLAY

      ON KEY (F5) -- edit
        LET i = arr_curr()
        LET l_image_id = l_print_image_arr[i].image_id
        CALL print_image_edit(l_image_id)
        EXIT DISPLAY

      ON KEY (F6) -- delete
        LET i = arr_curr()
        LET l_image_id = l_print_image_arr[i].image_id
        CALL print_image_delete(l_image_id)
        EXIT DISPLAY

    
      ON KEY(F13)
        LET p_order_field2 = p_order_field
        LET p_order_field = "image_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

      ON KEY(F14)
        LET p_order_field2 = p_order_field
        LET p_order_field = "filename"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY
      

    END DISPLAY

    IF int_flag THEN
      EXIT WHILE
    END IF
  END WHILE


  CALL fgl_window_close("w_print_image_scroll")

  IF int_flag THEN
    LET int_flag = FALSE
    RETURN p_image_id 
  ELSE 
    RETURN l_image_id 
  END IF

END FUNCTION


######################################################################################################################
# Edit/Create/Delete/Input Functions
######################################################################################################################


######################################################
# FUNCTION print_image_create()
#
# Create a new print_image record
#
# RETURN NULL
######################################################
FUNCTION print_image_create()
  DEFINE 
    l_print_image RECORD LIKE qxt_print_image.*,
    local_debug   SMALLINT

  LOCATE l_print_image.image_data IN MEMORY

  LET local_debug = FALSE

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_g"), TRUE) 
    CALL populate_print_image_form_create_labels_g()

  ELSE
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_t"), TRUE) 
    CALL populate_print_image_form_create_labels_t()
  END IF

  LET l_print_image.image_id = 0

  LET int_flag = FALSE
  
  #Initialise some variables
  LET l_print_image.mod_date = TODAY

  IF local_debug THEN
    DISPLAY "print_image_create() - Before Input Call"
  END IF
  # CALL the INPUT
  CALL print_image_input(l_print_image.*)
    RETURNING l_print_image.*

  IF local_debug THEN
    DISPLAY "print_image_create() - After Input Call"
  END IF

  IF NOT int_flag THEN
    INSERT INTO qxt_print_image VALUES (
      l_print_image.image_id,
      l_print_image.filename,
      l_print_image.mod_date,
      l_print_image.image_data)
  END IF

  CALL fgl_window_close("w_print_image")

  #Free memory
  FREE l_print_image.image_data


  IF NOT int_flag THEN
    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)

      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_print_image.image_id
    END IF

    #RETURN sqlca.sqlerrd[2]
  ELSE
    LET int_flag = FALSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    RETURN NULL
  END IF

END FUNCTION


#################################################
# FUNCTION print_image_edit(p_image_id)
#
# Edit print_image record
#
# RETURN NONE
#################################################
FUNCTION print_image_edit(p_image_id)
  DEFINE 
    p_image_id               LIKE qxt_print_image.image_id,
    l_key1_image_id          LIKE qxt_print_image.image_id,
    #l_print_image_form_rec  OF t_print_image_form_rec,
    l_print_image_rec        RECORD LIKE qxt_print_image.*,
    local_debug              SMALLINT

  #Memory alocation is not required, done by the function get_print_image_rec()
  #But it must be freeed here
  #LOCATE l_print_image_rec.image_data IN MEMORY

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "print_image_edit() - Function Entry Point"
    DISPLAY "print_image_edit() - p_image_id=", p_image_id
  END IF

  #Store the primary key combination for the SQL update
  LET l_key1_image_id = p_image_id


  #Get the record (by id)
  CALL get_print_image_rec(p_image_id) 
    RETURNING l_print_image_rec.*

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_g"), TRUE) 
    CALL populate_print_image_form_edit_labels_g()

  ELSE
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_t"), TRUE) 
    CALL populate_print_image_form_edit_labels_t()
  END IF

  #Update the modification date
  LET l_print_image_rec.mod_date = TODAY

  #Call the INPUT
  CALL print_image_input(l_print_image_rec.*) 
    RETURNING l_print_image_rec.*

  IF local_debug THEN
    DISPLAY "print_image_edit() RETURNED FROM INPUT - int_flag=", int_flag
  END IF

  #Check if user canceled
  IF NOT int_flag THEN

    IF local_debug THEN
      DISPLAY "print_image_edit() - l_print_image_rec.image_id=",l_print_image_rec.image_id
      DISPLAY "print_image_edit() - l_print_image_rec.filename=",l_print_image_rec.filename
      DISPLAY "print_image_edit() - l_print_image_rec.mod_date=",l_print_image_rec.mod_date
      #DISPLAY "print_image_edit() - l_print_image_rec.image_data=",l_print_image_rec.image_data
    END IF

    UPDATE qxt_print_image
      SET 
          #image_id =             l_print_image_rec.image_id,
          filename =             l_print_image_rec.filename,
          mod_date =             l_print_image_rec.mod_date,
          image_data =           l_print_image_rec.image_data
      WHERE qxt_print_image.image_id = l_key1_image_id


    IF local_debug THEN
      DISPLAY "print_image_edit() - sqlca.sqlcode=", sqlca.sqlcode
    END IF

  END IF

  CALL fgl_window_close("w_print_image")
  FREE l_print_image_rec.image_data 

  #Check if user canceled
  IF NOT int_flag THEN
    IF sqlca.sqlcode THEN --failure
      #CALL tl_msg_record_created(0,get_str_tool(980),NULL)
      RETURN NULL
    ELSE  --success
      #CALL tl_msg_record_created(1,get_str_tool(980),NULL)
      RETURN l_print_image_rec.image_id
    END IF

  ELSE
    #CALL tl_msg_input_abort(get_str_tool(980),NULL)
    LET int_flag = FALSE
    RETURN NULL

  END IF

END FUNCTION


#################################################
# FUNCTION print_image_input(p_print_image)
#
# Input print_image details (edit/create)
#
# RETURN l_print_image.*
#################################################
FUNCTION print_image_input(p_print_image_rec)
  DEFINE 
    p_print_image_rec            RECORD LIKE qxt_print_image.*,
    l_print_image_form_rec       OF t_qxt_print_image_form_rec,
    l_server_blob_file           VARCHAR(300),  --,
    l_orignal_filename           LIKE qxt_print_image.filename,
    l_orignal_image_id           LIKE qxt_print_image.image_id,
    local_debug                  SMALLINT

  LET local_debug = FALSE

  LET int_flag = FALSE

  #keep the original field values to differ between edit and create new record operation
  LET l_orignal_filename = p_print_image_rec.filename
  LET l_orignal_image_id = p_print_image_rec.image_id

  #copy record data to form_record format record
  CALL copy_print_image_record_to_form_record(p_print_image_rec.*) 
    RETURNING l_print_image_form_rec.*

  ####################
  #Start actual INPUT
  INPUT BY NAME l_print_image_form_rec.* WITHOUT DEFAULTS HELP 1

    ON KEY(F9)  --upload document to blob
      #upload_blob_print_image(p_client_file_path,p_dialog)
      CALL upload_blob_print_image(l_print_image_form_rec.filename,TRUE) 
        RETURNING l_server_blob_file --tRUE = file dialog

      IF l_server_blob_file IS NOT NULL AND fgl_test("e", l_server_blob_file)THEN
          # free the existing locator
          IF l_print_image_form_rec.image_data IS NOT NULL THEN
            FREE l_print_image_form_rec.image_data
          END IF
          #locate new blob data from file
          LOCATE l_print_image_form_rec.image_data IN FILE l_server_blob_file
          LET l_print_image_form_rec.filename = fgl_basename(l_server_blob_file)
          DISPLAY l_print_image_form_rec.filename TO filename
          DISPLAY l_print_image_form_rec.image_data TO image_data

          # DISPLAY BY NAME l_contact_rec.cont_picture
       END IF

    ON KEY(F10)  --download blob document to file
      CALL download_blob_print_image_to_client(l_print_image_form_rec.image_id,NULL,TRUE)
      #Syntax CALL download_blob_print_image_to_client(p_image_id,p_client_file_path,p_dialog)
  #download_blob_print_image_to_client(p_image_id,p_client_file_path,p_dialog)

{
    #filename can be duplicated - it's all about help id 
    AFTER FIELD filename

      IF image_filename_count(p_print_image.filename) THEN
        IF (p_print_image.filename = l_orignal_filename) THEN
          #do nothing for now, this is an edit operation
        ELSE
          DISPLAY p_print_image.filename TO filename ATTRIBUTE(RED)
          #You must specify an unique contact name. "
          ERROR get_str(2410) CLIPPED, " ", p_print_image.filename 
          NEXT FIELD filename
        END IF
      ELSE
        DISPLAY p_print_image.filename TO filename ATTRIBUTE(NORMAL)
      END IF
}
    AFTER FIELD filename
      #Filename must not be empty
      IF NOT validate_field_value_string_exists(get_str_tool(973), l_print_image_form_rec.filename,NULL,TRUE)  THEN
        NEXT FIELD filename
      END IF

    AFTER FIELD mod_date
      IF NOT validate_field_value_date_exists(get_str_tool(974), l_print_image_form_rec.mod_date,NULL,TRUE) THEN
        NEXT FIELD mod_date
      END IF


    # AFTER INPUT BLOCK
    AFTER INPUT
    IF local_debug THEN
      DISPLAY "print_image_input() - AFTER INPUT"
    END IF

      #If user pressed cancel, ask - if he really wants to abort
      IF int_flag THEN
        IF NOT question_abort_input(NULL) THEN
          LET int_flag = FALSE
          CONTINUE INPUT
        ELSE
          EXIT INPUT  --no validation if user cancels
        END IF 
      END IF

      #Filename must not be empty
        IF NOT validate_field_value_string_exists(get_str_tool(973), l_print_image_form_rec.filename,NULL,TRUE)  THEN

          NEXT FIELD filename
          CONTINUE INPUT
        END IF
      #The  image_id must be unique
      IF image_id_count(l_print_image_form_rec.image_id) THEN
        #Constraint only exists for newly created records (not modify)
        IF l_orignal_image_id <> l_print_image_form_rec.image_id THEN  --it is not an edit operation
          CALL fgl_winmessage(get_str_tool(63),get_str_tool(64),"error")
          NEXT FIELD image_id
          CONTINUE INPUT
        END IF
      END IF
      IF NOT validate_field_value_date_exists(get_str_tool(974), l_print_image_form_rec.mod_date,NULL,TRUE) THEN
        NEXT FIELD mod_date
        CONTINUE INPUT
      END IF
      #BLOB Data must be suplied, otherwise - the help system will not work correctly
      IF NOT validate_field_value_byte_exists(get_str_tool(975), l_print_image_form_rec.image_data ,"Binary BLOB TEXT Data",TRUE) THEN
        #NEXT FIELD image_data --is blob viewer field - can't move the cursor into it
        NEXT FIELD filename
        CONTINUE INPUT
      END IF
  END INPUT

  IF local_debug THEN
    DISPLAY "print_image_input() - l_print_image_form_rec.image_id=",l_print_image_form_rec.image_id
    DISPLAY "print_image_input() - l_print_image_form_rec.filename=",l_print_image_form_rec.filename
    DISPLAY "print_image_input() - l_print_image_form_rec.mod_date=",l_print_image_form_rec.mod_date
    #DISPLAY "print_image_edit() - l_print_image_form_rec.image_data=",l_print_image_form_rec.image_data
  END IF


  #Copy the form record data to a normal print_image record 
  IF NOT int_flag THEN --user pressed OK
    CALL copy_print_image_form_record_to_record(l_print_image_form_rec.*) RETURNING p_print_image_rec.*
  END IF

  IF local_debug THEN
    DISPLAY "print_image_input() - p_print_image_rec.image_id=",p_print_image_rec.image_id
    DISPLAY "print_image_input() - p_print_image_rec.filename=",p_print_image_rec.filename
    DISPLAY "print_image_input() - p_print_image_rec.mod_date=",p_print_image_rec.mod_date
    #DISPLAY "print_image_edit() - p_print_image_rec.image_data=",p_print_image_rec.image_data
  END IF

  RETURN p_print_image_rec.*

END FUNCTION


#################################################
# FUNCTION print_image_delete(p_image_id)
#
# Delete a print_image record
#
# RETURN NONE
#################################################
FUNCTION print_image_delete(p_image_id)
  DEFINE 
    p_image_id       LIKE qxt_print_image.image_id

  #do you really want to delete...
  IF question_delete_record(get_str_tool(980), NULL) THEN

    BEGIN WORK
      DELETE FROM qxt_print_image 
        WHERE image_id = p_image_id
    COMMIT WORK

  END IF

END FUNCTION


#################################################
# FUNCTION print_image_view(p_image_id)
#
# View print_image record by ID in window-form
#
# RETURN NONE
#################################################
FUNCTION print_image_view(p_image_id)
  DEFINE 
    p_image_id              LIKE qxt_print_image.image_id,
    l_print_image_rec       RECORD LIKE qxt_print_image.*

  LOCATE l_print_image_rec.image_data IN MEMORY
 

  CALL get_print_image_rec(p_image_id) RETURNING l_print_image_rec.*
  CALL print_image_view_by_rec(l_print_image_rec.*)

  FREE l_print_image_rec.image_data

END FUNCTION


#################################################
# FUNCTION print_image_view_by_rec(p_print_image_rec)
#
# View print_image record in window-form
#
# RETURN NONE
#################################################
FUNCTION print_image_view_by_rec(p_print_image_rec)
  DEFINE 
    p_print_image_rec  RECORD LIKE qxt_print_image.*,
    inp_char         CHAR

  IF fgl_fglgui() THEN --gui
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_g"), TRUE) 
    CALL populate_print_image_form_view_labels_g()

  ELSE
    CALL fgl_window_open("w_print_image", 3, 3, get_form_path("f_qxt_print_image_det_t"), TRUE) 
    CALL populate_print_image_form_view_labels_t()

  END IF

  DISPLAY BY NAME p_print_image_rec.*

  WHILE TRUE
    PROMPT "" FOR CHAR inp_char HELP 2
      ON KEY(ACCEPT,INTERRUPT)
        LET int_flag = FALSE
        EXIT WHILE
    END PROMPT
  END WHILE  

  CALL fgl_window_close("w_print_image")
END FUNCTION



######################################################
# FUNCTION upload_blob_print_image(p_contact_id, p_contact_name,remote_image_path)
#
# Download a DB BLOB(server side) located image to the client file system
#
# RETURN NONE
######################################################
FUNCTION upload_blob_print_image(p_client_file,p_dialog)
  DEFINE 
    p_image_id          LIKE qxt_print_image.image_id,
    l_print_image_file  OF t_qxt_print_image_file,
    local_debug         SMALLINT,
    p_client_file       VARCHAR(250),
    l_server_file       VARCHAR(250),
    p_dialog            SMALLINT,
    p_server_file_blob  BYTE

  LET local_debug = FALSE

  IF p_dialog THEN
    #"Please select the file"
    CALL fgl_file_dialog("open", 0, get_str_tool(65), p_client_file, p_client_file, "File (*.*)|*.*|RTF (*.rtf)|*.rtf|Excel (*.xls)|*.xls|Word (*.doc)|*.doc|Text (*.txt)|*.txt|JPEG (*.jpg)|*.jpg|Bitmap (*.bmp)|*.bmp|Png (*.png)|*.png||")
      RETURNING p_client_file
  END IF

  IF p_client_file IS NOT NULL THEN
    # This will copy the file to the CWD. Hubert can tidy this up later
    LET l_server_file = get_server_blob_temp_path(fgl_basename(p_client_file))

    IF NOT fgl_upload(p_client_file, l_server_file) OR NOT fgl_test("e", l_server_file) THEN
      LET l_server_file = ""
    END IF
  END IF


  IF local_debug THEN
    DISPLAY "upload_blob_print_image() - p_client_file_path=", p_client_file
    DISPLAY "upload_blob_print_image() - l_server_file_path=", l_server_file
  END IF


  RETURN l_server_file

END FUNCTION


######################################################
# FUNCTION get_blob_print_image_file(p_image_id)
#
# Get the print_image file
#
# RETURN p_server_file_path
######################################################
FUNCTION get_blob_print_image_file(p_image_id)
  DEFINE 
    p_image_id       LIKE qxt_print_image.image_id,
    p_server_file_path   VARCHAR(250),
    l_print_image_file     OF t_qxt_print_image_file

  LET p_server_file_path = get_server_blob_temp_path(get_print_html_image_filename(p_image_id))

  LOCATE l_print_image_file.image_data IN FILE p_server_file_path

  SELECT qxt_print_image.filename,
         qxt_print_image.print_image_blob
    INTO l_print_image_file.*
    FROM qxt_print_image
    WHERE qxt_print_image.image_id = p_image_id

  RETURN p_server_file_path
END FUNCTION


######################################################
# FUNCTION copy_print_image_record_to_form_record(p_print_image_rec)  
#
# Copy normal print_image record data to type print_image_form_rec
#
# RETURN l_print_image_form_rec.*
######################################################
FUNCTION copy_print_image_record_to_form_record(p_print_image_rec)  
  DEFINE
    p_print_image_rec       RECORD LIKE qxt_print_image.*,
    l_print_image_form_rec  OF t_qxt_print_image_form_rec

  LET l_print_image_form_rec.image_id = p_print_image_rec.image_id
  LET l_print_image_form_rec.filename = p_print_image_rec.filename
  LET l_print_image_form_rec.mod_date = p_print_image_rec.mod_date
  LET l_print_image_form_rec.image_data = p_print_image_rec.image_data

  RETURN l_print_image_form_rec.*
END FUNCTION


######################################################
# FUNCTION copy_print_image_form_record_to_record(p_print_image_form_rec)  
#
# Copy type print_image_form_rec to normal print_image record data
#
# RETURN l_print_image_rec.*
######################################################
FUNCTION copy_print_image_form_record_to_record(p_print_image_form_rec)  
  DEFINE
    l_print_image_rec       RECORD LIKE qxt_print_image.*,
    p_print_image_form_rec  OF t_qxt_print_image_form_rec

  LET l_print_image_rec.image_id = p_print_image_form_rec.image_id
  LET l_print_image_rec.filename = p_print_image_form_rec.filename
  LET l_print_image_rec.mod_date = p_print_image_form_rec.mod_date
  LET l_print_image_rec.image_data = p_print_image_form_rec.image_data

  RETURN l_print_image_rec.*

END FUNCTION

######################################################################################################
# Display functions
######################################################################################################

####################################################
# FUNCTION grid_header_print_image_scroll()
#
# Populate print_image grid headers
#
# RETURN NONE
####################################################
FUNCTION grid_header_print_image_scroll()
  CALL fgl_grid_header("sc_print_image","image_id",get_str_tool(971),"right","F13")  --Help ID
  CALL fgl_grid_header("sc_print_image","filename",get_str_tool(973),"left","F14")  --file name
  CALL fgl_grid_header("sc_print_image","mod_date",get_str_tool(974),"left","F15")  --modificationn date


END FUNCTION

#######################################################
# FUNCTION populate_print_image_form_labels_g()
#
# Populate print_image form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_labels_g()

  CALL fgl_settitle(get_str_tool(970))

  DISPLAY get_str_tool(970) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel


END FUNCTION



#######################################################
# FUNCTION populate_print_image_form_labels_t()
#
# Populate print_image form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_labels_t()

  DISPLAY get_str_tool(970) TO lbTitle

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_print_image_form_edit_labels_g()
#
# Populate print_image form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_edit_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(7))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download



END FUNCTION



#######################################################
# FUNCTION populate_print_image_form_edit_labels_t()
#
# Populate print_image form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_edit_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(7) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_print_image_form_view_labels_g()
#
# Populate print_image form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_view_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(15))  --View

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


  DISPLAY get_str_tool(3) TO bt_cancel
  DISPLAY "!" TO bt_cancel

END FUNCTION



#######################################################
# FUNCTION populate_print_image_form_view_labels_t()
#
# Populate print_image form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_view_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(15) TO lbTitle  --View

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(402) TO lbInfo1


END FUNCTION


#######################################################
# FUNCTION populate_print_image_form_create_labels_g()
#
# Populate print_image form edit labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_create_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(14))  --Edit

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(12) TO bt_upload
  DISPLAY "!" TO bt_upload

  DISPLAY get_str_tool(13) TO bt_download
  DISPLAY "!" TO bt_download


END FUNCTION



#######################################################
# FUNCTION populate_print_image_form_create_labels_t()
#
# Populate print_image form edit labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_form_create_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(14) TO lbTitle  --Edit

  DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  DISPLAY get_str_tool(973) TO dl_f3
  DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(400) TO lbInfo1

END FUNCTION


#######################################################
# FUNCTION populate_print_image_list_form_labels_g()
#
# Populate print_image list form labels for gui
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_list_form_labels_g()

  CALL fgl_settitle(trim(get_str_tool(970)) || " - " || get_str_tool(16))  --List

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(16) TO lbTitle  --List

  CALL grid_header_print_image_scroll()

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

  DISPLAY get_str_tool(1) TO bt_ok
  DISPLAY "!" TO bt_ok

  DISPLAY get_str_tool(2) TO bt_cancel
  DISPLAY "!" TO bt_cancel

  DISPLAY get_str_tool(14) TO bt_create
  DISPLAY "!" TO bt_create

  DISPLAY get_str_tool(7) TO bt_edit
  DISPLAY "!" TO bt_edit

  DISPLAY get_str_tool(17) TO bt_delete
  DISPLAY "!" TO bt_delete

END FUNCTION


#######################################################
# FUNCTION populate_print_image_list_form_labels_t()
#
# Populate print_image list form labels for text
#
# RETURN NONE
#######################################################
FUNCTION populate_print_image_list_form_labels_t()

  DISPLAY trim(get_str_tool(970)) || " - " || get_str_tool(16) TO lbTitle  --List

  #DISPLAY get_str_tool(971) TO dl_f1
  #DISPLAY get_str_tool(972) TO dl_f2
  #DISPLAY get_str_tool(973) TO dl_f3
  #DISPLAY get_str_tool(974) TO dl_f4
  #DISPLAY get_str_tool(975) TO dl_f5
  #DISPLAY get_str_tool(976) TO dl_f6
  #DISPLAY get_str_tool(977) TO dl_f7
  #DISPLAY get_str_tool(978) TO dl_f8
  #DISPLAY get_str_tool(979) TO dl_f9

  DISPLAY get_str_tool(401) TO lbInfo1

END FUNCTION



