##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################



FUNCTION validate_column_exist(p_table_name,p_column_name)
  DEFINE 
    sql_type, sqllen SMALLINT,
    p_table_name  VARCHAR(18),
    p_column_name VARCHAR(18)

  CALL fgl_column_info(p_table_name,p_column_name)
    RETURNING sql_type, sqllen

  DISPLAY "sql_type=", sql_type
  DISPLAY "sqllen=", sqllen

  IF sql_type >= 0 THEN
    RETURN TRUE
  ELSE
    RETURN FALSE
  END IF

END FUNCTION




###########################################################
# FUNCTION validate_field_value_string_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# STRING - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_string_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        VARCHAR(300),
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  IF p_value IS NULL THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF

    RETURN FALSE
  ELSE
    RETURN TRUE
  END IF

END FUNCTION


###########################################################
# FUNCTION validate_field_value_numeric_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# NUMERIC - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_numeric_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        FLOAT,
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  IF p_value IS NULL OR p_value = 0 THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF
    RETURN FALSE
  ELSE
    RETURN TRUE
  END IF

END FUNCTION

{
###########################################################
# FUNCTION validate_field_value_numeric_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# NUMERIC - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_numeric_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        FLOAT,
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300),
    local_debug    SMALLINT

  LET local_debug = FALSE

    IF local_debug THEN
      DISPLAY "validate_field_value_boolean_exists() - p_fieldname=", p_fieldname
      DISPLAY "validate_field_value_boolean_exists() - p_value=", p_value
      DISPLAY "validate_field_value_boolean_exists() - p_optional_str=", p_optional_str
      DISPLAY "validate_field_value_boolean_exists() - p_dialog=", p_dialog
    END IF

  IF p_value IS NULL OR p_value = 0 THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF
    IF local_debug THEN
      DISPLAY "validate_field_value_boolean_exists() - p_value=", p_value
      DISPLAY "validate_field_value_boolean_exists() - RETURN FALSE"
    END IF
    RETURN FALSE
  ELSE
    IF local_debug THEN
      DISPLAY "validate_field_value_boolean_exists() - p_value=", p_value
      DISPLAY "validate_field_value_boolean_exists() - RETURN TRUE"
    END IF
    RETURN TRUE
  END IF



END FUNCTION
}

###########################################################
# FUNCTION validate_boolean(p_fieldname, p_value,p_optional_str,p_dialog)
#
# NUMERIC - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_boolean_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE
    p_fieldname    VARCHAR(50),
    p_value        VARCHAR(20),
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    l_v            VARCHAR(1),
    vi             INTEGER,
    ret            SMALLINT,
    tmp_str        VARCHAR(200),
    local_debug    SMALLINT


  LET local_debug = FALSE

    IF local_debug THEN
      DISPLAY "validate_field_value_boolean_exists() - p_fieldname=", p_fieldname
      DISPLAY "validate_field_value_boolean_exists() - p_value=", p_value
      DISPLAY "validate_field_value_boolean_exists() - p_optional_str=", p_optional_str
      DISPLAY "validate_field_value_boolean_exists() - p_dialog=", p_dialog
    END IF


  IF p_value IS NULL THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF
    RETURN FALSE
  ELSE
    RETURN TRUE
  END IF


{
  LET ret = 99
  LET vi = p_value

  LET l_v = p_value

  LET l_v = downshift(l_v)

  IF vi > 0 THEN
    LET l_v = TRUE
    LET ret = TRUE
  END IF

  IF vi <= 0 THEN
    LET l_v = FALSE
    LET ret = FALSE
  END IF



  CASE l_v
    WHEN l_v = 'f'
      LET ret = FALSE

    WHEN l_v = 't'
      LET ret = TRUE

    WHEN l_v = '0'
      LET ret = FALSE

    WHEN l_v = '1'
      LET ret = TRUE

    WHEN l_v IS NULL
      LET ret = FALSE

    WHEN vi > 0
      LET ret = TRUE

    WHEN vi <= 0
      LET ret = FALSE

    OTHERWISE
      LET tmp_str = "validate_boolean()\nInvalid argument\p_value =", p_value, " l_v =", l_v, "vi =", vi
      CALL fgl_winmessage("Internal Demo Code Error",tmp_str,"error")
      LET ret = 98

  END CASE

  IF NOT ret THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF
  END IF

  RETURN ret
}
END FUNCTION


###########################################################
# FUNCTION validate_field_value_date_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# DATE - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_date_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        DATE,
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  IF p_value IS NULL THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF

    RETURN 0
  ELSE
    RETURN 1
  END IF

END FUNCTION


###########################################################
# FUNCTION validate_field_value_byte_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# BYTE - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_byte_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        BYTE,
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  IF p_value IS NULL THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF

    RETURN 0
  ELSE
    RETURN 1
  END IF
END FUNCTION


###########################################################
# FUNCTION validate_field_value_text_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# TEXT - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_field_value_text_exists(p_fieldname, p_value,p_optional_str,p_dialog)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_value        TEXT,
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  IF p_value IS NULL THEN
    IF p_dialog THEN
      CALL tl_msg_is_empty(p_fieldname,p_optional_str)
    END IF

    RETURN 0
  ELSE
    RETURN 1
  END IF
END FUNCTION


############################################################
# FUNCTION validate_file_server_side_exists_advanced(p_filename,p_file_type,p_dialog)
#
# Checks, if file or directory exists
# 
# p_file_type f=file  d=directory
#
# Return -1=dir not exist but file exists 0=No 1=Yes 
############################################################
FUNCTION validate_file_server_side_exists_advanced(p_filename,p_file_type,p_dialog)
  DEFINE
    p_filename  VARCHAR(200),
    p_file_type  CHAR,
    p_dialog     SMALLINT,
    p_optional_str VARCHAR(100),
    tmp_str      VARCHAR(200),
    tmp_str2     VARCHAR(100),
    ret          SMALLINT,
    local_debug  SMALLINT

  LET local_debug = FALSE

  IF local_debug THEN
    DISPLAY "validate_file_server_side_exists_advanced() - p_filename=", p_filename
    DISPLAY "validate_file_server_side_exists_advanced() - p_file_type=", p_file_type
    DISPLAY "validate_file_server_side_exists_advanced() - p_dialog=", p_dialog
  END IF

  LET ret = -3


  #Depending on the file type / directory
  CASE p_file_type

    WHEN "f"  --file
      LET tmp_str2 = "File ", trim(p_filename), " "
      IF fgl_test("e", p_filename)  THEN
        LET ret = 1
      ELSE
        LET ret = 0
      END IF
 
    WHEN "d"  --directory
    LET tmp_str2 = "Directory ", trim(p_filename), " "
      IF fgl_test("d", p_filename) THEN
        Let ret = 1
      ELSE
        IF fgl_test("e", p_filename)  THEN
          LET ret = -1
        ELSE
          LET ret = 0
        END IF
      END IF

    OTHERWISE
      CALL fgl_winmessage("Error in function validate_file_server_side_exists_advanced()","Error in function validate_file_server_side_exists_advanced()\n Invalid argument p_file_type","error")
      RETURN -2
  END CASE


  IF NOT p_dialog THEN -- dialog if test fails as in file/directory does not exist
    RETURN ret
  ELSE
    CASE ret
      WHEN -1
        IF p_dialog > 0 THEN
        LET tmp_str = trim(tmp_str2), " does not exist! \n But a file exist with the same name!"
          CALL fgl_winmessage(tmp_str2, tmp_str, "info")
        END IF
        RETURN ret
      WHEN 0
        IF p_dialog > 0 THEN
          LET tmp_str = trim(tmp_str2), " does not exist!"
          CALL fgl_winmessage(tmp_str2, tmp_str, "info")
        END IF
        RETURN ret
      WHEN 1
        IF p_dialog > 1 THEN
          LET tmp_str = trim(tmp_str2), " does exist!"
          CALL fgl_winmessage(tmp_str2, tmp_str, "info")
        END IF
        RETURN ret
      OTHERWISE
        LET tmp_str = "Error - validate_file_server_side_exists_advanced()\nInvalid case condition ret = ", ret CLIPPED, "\n", p_optional_str 
        CALL fgl_winmessage("Error - validate_file_server_side_exists_advanced()",tmp_str,"error")
        RETURN -1
    END CASE

  END IF
END FUNCTION




###########################################################
# FUNCTION validate_file_client_side_exists(p_filename, p_optional_str,p_dialog)
#
# FILE - Validate the existence of a file on the client (with optional message)
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_file_client_side_exists(p_filename, p_optional_str,p_dialog)
  DEFINE 
    p_filename     VARCHAR(250),
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  #check if file exists on client machine
  IF NOT fgl_getproperty("gui","system.file.exists",p_filename) THEN
    IF p_dialog THEN  --dialog information flag is active
      #The specified document does not exist on the server
      LET tmp_str = get_str_tool(675) CLIPPED, "\n", p_filename CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(672),tmp_str,"error")
    END IF
    RETURN 0
  ELSE
    RETURN 1
  END IF

END FUNCTION



###########################################################
# FUNCTION validate_file_client_side_cache_exists(p_filename, p_optional_str,p_dialog)
#
# FILE - Validate the existence of a file on the client (with optional message)
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_file_client_side_cache_exists(p_filename, p_optional_str,p_dialog)
  DEFINE 
    p_filename      VARCHAR(250),
    p_optional_str  VARCHAR(100),
    p_dialog        SMALLINT,
    tmp_str         VARCHAR(300),
    cache_file_test VARCHAR(300),
    local_debug     SMALLINT

  LET local_debug = FALSE

  #check if file exists on client machine cache system
  #system.file.cachename returns the full path with the real file name or an empty string if not found
  LET cache_file_test = fgl_getproperty("gui","system.file.cachename",p_filename)

  IF local_debug THEN
    DISPLAY "validate_file_client_side_cache_exists() - p_filename=", p_filename
    DISPLAY "validate_file_client_side_cache_exists() - p_optional_str=", p_optional_str
    DISPLAY "validate_file_client_side_cache_exists() - p_dialog=", p_dialog
    DISPLAY "validate_file_client_side_cache_exists() - fgl_getproperty(\"gui\",\"system.file.cachename\",p_filename)=", fgl_getproperty("gui","system.file.cachename",p_filename)
    DISPLAY "validate_file_client_side_cache_exists() - cache_file_test=", cache_file_test
  END IF

  IF cache_file_test IS NULL THEN
    IF p_dialog THEN  --dialog information flag is active
      #The specified document does not exist on the server
      LET tmp_str = get_str_tool(677) CLIPPED, "\n", p_filename CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(672),tmp_str,"error")
    END IF
    IF local_debug THEN
      DISPLAY "validate_file_client_side_cache_exists() - RETURN 0"
    END IF
    RETURN FALSE
  ELSE
    IF local_debug THEN
      DISPLAY "validate_file_client_side_cache_exists() - RETURN 1"
    END IF
    RETURN TRUE
  END IF

END FUNCTION


###########################################################
# FUNCTION validate_file_server_side_exists(p_filename, p_optional_str,p_dialog)
#
# FILE - Validate the existence of a file on the SERVER (with optional message)
#
# RETURN BOOLEAN
###########################################################
FUNCTION validate_file_server_side_exists(p_filename, p_optional_str,p_dialog)
  DEFINE 
    p_filename     VARCHAR(250),
    p_optional_str VARCHAR(100),
    p_dialog       SMALLINT,
    tmp_str        VARCHAR(300)

  #Check if the file exists on the server
  IF NOT fgl_test("e",p_filename) THEN

    IF p_dialog THEN  --dialog information flag is active
      #The specified document does not exist on the server
      LET tmp_str = get_str_tool(673) CLIPPED, "\n", p_filename CLIPPED, "\n", p_optional_str
      #File does not exist - 
      CALL fgl_winmessage(get_str_tool(672),tmp_str,"error")
    END IF

    RETURN 0
  ELSE
    RETURN 1
  END IF

END FUNCTION


###########################################################
# FUNCTION tl_msg_is_empty(p_fieldname,p_optional_str)
#
# Message box for empty but required field data
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_is_empty(p_fieldname,p_optional_str)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

    LET tmp_str = get_str_tool(50) CLIPPED, "\n", p_fieldname CLIPPED, "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(50),tmp_str,"error")

END FUNCTION


###########################################################
# FUNCTION tl_msg_duplicate(p_fieldname,p_optional_str)
#
# Message box for duplicated but unique field data
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_duplicate_key(p_fieldname,p_optional_str)
  DEFINE 
    p_fieldname    VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

    LET tmp_str = get_str_tool(51) CLIPPED, "\n", p_fieldname CLIPPED, "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(51),tmp_str,"error")


END FUNCTION



###########################################################
# FUNCTION tl_msg_duplicate_doublekey(p_fieldname,p_optional_str)
#
# Message box for duplicated but unique field data (primary key with 2 columns)
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_duplicate_doublekey(p_fieldname1,p_fieldname2,p_optional_str)
  DEFINE 
    p_fieldname1   VARCHAR(50),
    p_fieldname2   VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

    LET tmp_str = get_str_tool(64) CLIPPED, "\n", p_fieldname1 CLIPPED, " / ", p_fieldname2 CLIPPED , "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(58),tmp_str,"error")


END FUNCTION




###########################################################
# FUNCTION tl_msg_duplicate_treblekey(p_fieldname1,p_fieldname2,p_fieldname3,p_optional_str)
#
# Message box for duplicated but unique field data (primary key with 3 columns)
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_duplicate_treblekey(p_fieldname1,p_fieldname2,p_fieldname3,p_optional_str)
  DEFINE 
    p_fieldname1   VARCHAR(50),
    p_fieldname2   VARCHAR(50),
    p_fieldname3   VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

    LET tmp_str = get_str_tool(68) CLIPPED, "\n", p_fieldname1 CLIPPED, " / ", p_fieldname2 CLIPPED , " / ", p_fieldname3 CLIPPED , "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(58),tmp_str,"error")


END FUNCTION


###########################################################
# FUNCTION tl_msg_duplicate_fourfoldkey(p_fieldname1,p_fieldname2,p_fieldname3,p_fieldname4,p_optional_str)
#
# Message box for duplicated but unique field data (primary key with 4 columns)
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_duplicate_fourfoldkey(p_fieldname1,p_fieldname2,p_fieldname3,p_fieldname4,p_optional_str)
  DEFINE 
    p_fieldname1   VARCHAR(50),
    p_fieldname2   VARCHAR(50),
    p_fieldname3   VARCHAR(50),
    p_fieldname4   VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

    LET tmp_str = get_str_tool(69) CLIPPED, "\n", p_fieldname1 CLIPPED, " / ", p_fieldname2 CLIPPED , " / ", p_fieldname3 CLIPPED , " / ", p_fieldname4 CLIPPED, "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(58),tmp_str,"error")


END FUNCTION

###########################################################
# FUNCTION tl_msg_record_created(p_status,p_record_name,p_optional_str)
#
# Message box for record creation (after creation with status)
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_record_created(p_status,p_record_name,p_optional_str)
  DEFINE 
    p_status         SMALLINT,
    p_record_name    VARCHAR(50),
    p_optional_str   VARCHAR(100),
    tmp_str          VARCHAR(300)

    IF p_status THEN --sucess
      LET tmp_str = get_str_tool(55) CLIPPED, "\n", p_record_name CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(55),tmp_str,"info")
    ELSE  --failure
      LET tmp_str = get_str_tool(56) CLIPPED, "\n", p_record_name CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(56),tmp_str,"error")
    END IF

END FUNCTION


###########################################################
# FUNCTION tl_msg_record_modified(p_status,p_record_name,p_optional_str)
#
# Message box for record modification (after modication with status)
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_record_modified(p_status,p_record_name,p_optional_str)
  DEFINE 
    p_status         SMALLINT,
    p_record_name    VARCHAR(50),
    p_optional_str   VARCHAR(100),
    tmp_str          VARCHAR(300)


    IF p_status THEN --sucess
      LET tmp_str = get_str_tool(57) CLIPPED, "\n", p_record_name CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(57),tmp_str,"info")
    ELSE  --failure
      LET tmp_str = get_str_tool(58) CLIPPED, "\n", p_record_name CLIPPED, "\n", p_optional_str
      CALL fgl_winmessage(get_str_tool(58),tmp_str,"error")
    END IF

END FUNCTION


###########################################################
# FUNCTION tl_msg_input_abort(p_record_name,p_optional_str)
#
# Message box for input canceled by user
#
# RETURN NONE
###########################################################
FUNCTION tl_msg_input_abort(p_record_name,p_optional_str)
  DEFINE 
    p_record_name    VARCHAR(50),
    p_optional_str   VARCHAR(100),
    tmp_str          VARCHAR(300)

    LET tmp_str = get_str_tool(53) CLIPPED, "\n", p_record_name CLIPPED, "\n", p_optional_str
    CALL fgl_winmessage(get_str_tool(53),tmp_str,"info")


END FUNCTION



###########################################################
# FUNCTION validate_field_value_string_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# STRING - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION question_delete_record(p_table_name, p_optional_str)
  DEFINE 
    p_table_name    VARCHAR(50),
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

  #do you really want to delete...
  LET tmp_str = get_str_tool(59) CLIPPED, "\n", p_table_name CLIPPED, "\n",  p_optional_str
  RETURN yes_no(get_str_tool(17),tmp_str)

END FUNCTION


###########################################################
# FUNCTION validate_field_value_string_exists(p_fieldname, p_value,p_optional_str,p_dialog)
#
# STRING - Validate the existence (value) of a variable - return TRUE/FALSE and display optionally a message
#
# RETURN BOOLEAN
###########################################################
FUNCTION question_abort_input(p_optional_str)
  DEFINE 
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

  #do you really want to abort input...
  LET tmp_str = get_str_tool(54) CLIPPED, "\n", p_optional_str
  RETURN yes_no(get_str_tool(53),tmp_str) 


END FUNCTION



###########################################################
# FUNCTION question_not_exist_create(p_optional_str)
#
# Dialog - telling the user 'no record exists' do you want to create one..
#
# RETURN BOOLEAN
###########################################################
FUNCTION question_not_exist_create(p_optional_str)
  DEFINE 
    p_optional_str VARCHAR(100),
    tmp_str        VARCHAR(300)

  #Does not exist - create ?
  LET tmp_str = get_str_tool(66) CLIPPED, "\n", get_str_tool(67) CLIPPED, "\n", p_optional_str
  RETURN yes_no(get_str_tool(66),tmp_str) 


END FUNCTION














