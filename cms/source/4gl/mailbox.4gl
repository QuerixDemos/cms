##########################################################################
# cms-demo                                                               #
# Property of Querix Ltd.                                                #
# Copyright (C) 2016  Querix Ltd. All rights reserved.                   #
# This program is free software: you can redistribute it.                #
# You may modify this program only using Lycia.                          #
#                                                                        #
# This program is distributed in the hope that it will be useful,        #
# but without any warranty; without even the implied warranty of         #
# merchantability or fitness for a particular purpose.                   #
#                                                                        #
# Email: info@querix.com                                                 #
##########################################################################

########################################################################################################################
# Mailbox functions
########################################################################################################################
#
# FUNCTION:                                             DESCRIPTION:                               RETURN:
# mailbox(value)                                        Mailbox main menu                          NONE
# open_mailbox(p_operator_id)                           Old mailbox function - not used            NONE
# get_contact_id_from_email(p_email)                    get contact_id of the email address        l_contact_id
# get_mailbox_rec_from_mail_id(p_mail_id)               get the mailbox record by using the id     l_mailbox_rec.*
# get_long_desc_from_email(p_email)                     Returns the long description of an email   l_contact_id
# open_operator_mail_inbox_data_source(p_operator_id)   Data Source for operator-Mail-Inbox        NONE
# open_operator_mail_inbox(p_operator_id)               Mail Inbox from current operator           NONE
# open_operator_mail_outbox_data_source(p_operator_id)  Data Source for operator-Mail-Outbox       NONE
# open_operator_mail_outbox(p_operator_id)              Display Mail-Outbox from current operator  NONE
#
#
########################################################################################################################


#################################################
# GLOBALS
#################################################
GLOBALS "globals.4gl"

#################################################
# Mailbox Ring Menus
#################################################

#################################################
# FUNCTION mailbox(value)
#################################################
FUNCTION mailbox(value)
  DEFINE value LIKE operator.operator_id,
    menu_str_arr1 DYNAMIC ARRAY OF VARCHAR(20), --i.e. used for rinmenu strings
    menu_str_arr2 DYNAMIC ARRAY OF VARCHAR(100) --i.e. used for rinmenu strings

  #OPEN WINDOW w_mailbox_main AT 1,1 WITH 24 ROWS, 80 COLUMNS

	CALL ui.Interface.setImage("qx://application/icon16/webemail/mailbox.png")
	CALL ui.Interface.setText("Mailbox Center")


  MENU "Mailcentre"
    BEFORE MENU
      CALL set_help_id(301)
      CALL publish_toolbar("Mailbox",0)
        #need to assign strings to temp_strings - MENU does not support functions for strings
        LET menu_str_arr1[1] = get_str(501)
        LET menu_str_arr2[1] = get_str(502)
        LET menu_str_arr1[2] = get_str(503)
        LET menu_str_arr2[2] = get_str(504)
        LET menu_str_arr1[3] = get_str(505)
        LET menu_str_arr2[3] = get_str(506)
        LET menu_str_arr1[4] = get_str(507)
        LET menu_str_arr2[4] = get_str(508)
        LET menu_str_arr1[5] = get_str(509)
        LET menu_str_arr2[5] = get_str(510)
        LET menu_str_arr1[6] = get_str(511)
        LET menu_str_arr2[6] = get_str(512)
        LET menu_str_arr1[7] = get_str(513)
        LET menu_str_arr2[7] = get_str(514)
        LET menu_str_arr1[8] = get_str(515)
        LET menu_str_arr2[8] = get_str(516)
        LET menu_str_arr1[9] = get_str(517)
        LET menu_str_arr2[9] = get_str(518)
        LET menu_str_arr1[10] = get_str(519)
        LET menu_str_arr2[10] = get_str(520)



    COMMAND KEY(F401,"I")  menu_str_arr1[1]  menu_str_arr2[1] Help 301  --Mail-Inbox
      CALL open_operator_mail_inbox(value,NULL,1)

    COMMAND KEY (F402,"O")  menu_str_arr1[2]  menu_str_arr2[2]  Help 302  --Mail Outbox
      CALL  open_operator_mail_outbox(value,NULL,1)
      #CALL sent_email(value)
    COMMAND KEY(F12,INTERRUPT)menu_str_arr1[3]  menu_str_arr2[3]  Help 51   --exit / return
      EXIT MENU

    ON ACTION ("actExit") 
      DISPLAY "actExit handler - DEBUG 001"
      EXIT MENU
  END MENU

  #Possible toolbar clean up
  CALL publish_toolbar("Mailbox",1)

  #CLOSE WINDOW w_mailbox_main
END FUNCTION


#################################################
# FUNCTION open_operator_mail_inbox_data_source(p_operator_id)
#
# Data Source for operator-Mail-Inbox
#
# RETURN NONE
#################################################
FUNCTION open_operator_mail_inbox_data_source(p_order_field,p_ord_dir,mail_act_open_status_filter)
  DEFINE 
    local_debug        SMALLINT,
    sql_stmt           CHAR(2048),
    p_ord_dir          SMALLINT,
    p_ord_dir_str      VARCHAR(4),
    p_order_field      VARCHAR(30),
    mail_act_open_status_filter   SMALLINT

  LET local_debug = FALSE

  IF p_order_field IS NULL  THEN
    LET p_order_field = "recv_date"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF

  LET sql_stmt = "SELECT " ,
                   "mailbox.mail_id, ", 
                   "mailbox.recv_date, ",
                   "contact.cont_name,  ",
                   "mailbox.from_email_address, ", 
                   "mailbox.to_email_address, ", 
                   "activity.short_desc, ", 
                   "mailbox.read_flag, ", 
                   "mailbox.urgent_flag, ",
                   "mailbox.mail_status, ",
                   "activity.long_desc ",
                 "FROM  mailbox, activity, contact ",
                 "WHERE mailbox.to_email_address = ? ",
                   "AND mailbox.activity_id = activity.activity_id ",
                   "AND mailbox.from_email_address = contact.cont_email ",
                   "AND activity.act_type = 1 "
                   #"AND mailbox.mail_status = 0 "

  CASE mail_act_open_status_filter 
    WHEN 1  -- open activity email
      LET sql_stmt = sql_stmt CLIPPED, " AND activity.close_date IS NULL "

    WHEN 2  -- closed activity email
      LET sql_stmt = sql_stmt CLIPPED, " AND activity.close_date IS NOT NULL "

    WHEN 3  -- all emails
      #nothing to add in this case
      #LET sql_stmt = sql_stmt CLIPPED, " AND activity.close_date IS NOT NULL "
    OTHERWISE
      LET tmp_str = "Invalid value in mail_act_open_status_filter=", mail_act_open_status_filter
      CALL fgl_winmessage("Internal 4GL source code error",tmp_str,"error")
  END CASE

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  PREPARE p_mail_inbox FROM sql_stmt
  DECLARE c_mail_inbox CURSOR FOR p_mail_inbox

END FUNCTION


#################################################
# FUNCTION open_operator_mail_inbox(p_operator_id)
#
# Mail Inbox from current operator
#
# RETURN NONE
#################################################
FUNCTION open_operator_mail_inbox(p_operator_id,p_order_field,p_accept_action)
  DEFINE 
    p_operator_id            LIKE operator.operator_id,
    i                        INTEGER,
    curr_pa                  SMALLINT,
    p_operator_recv_id       LIKE contact.cont_name,
    p_operator2              LIKE contact.cont_id,
    p_operator_subject       LIKE activity.short_desc,
    mail                     LIKE activity.activity_id,
    local_debug              SMALLINT,
    l_operator_email_address LIKE operator.email_address,
    l_email_address          LIKE contact.cont_email,
    l_contact_id             LIKE contact.cont_id,
    l_mail_arr               DYNAMIC ARRAY OF t_mailbox_in_scroll_rec,
    l_long_desc_preview      LIKE activity.long_desc,
    p_accept_action          SMALLINT,
    p_order_field,p_order_field2  VARCHAR(128), 
    error_msg                     VARCHAR(200),
    mail_act_open_status_filter   SMALLINT

  DEFINE g_data RECORD
    long_desc   LIKE activity.long_desc
  END RECORD

  DEFINE g_data2 RECORD
    long_desc   LIKE activity.long_desc
  END RECORD

  LOCATE g_data2.long_desc IN MEMORY
  LOCATE g_data.long_desc  IN MEMORY

  LET local_debug = FALSE  --0=off 1=on
  LET mail_act_open_status_filter = 1  --1=open 2=closed 3=all
    LET l_operator_email_address = get_operator_email_address(p_operator_id)

  IF local_debug = 1 THEN
    DISPLAY "open_mail_inbox() p_operator_id=",p_operator_id
    DISPLAY "open_mail_inbox() l_operator_email_address=",l_operator_email_address
  END IF


  IF NOT p_order_field THEN
    LET p_order_field = "recv_date"
  END IF

  IF NOT p_accept_action THEN
    LET p_accept_action = 1  --View is default 
  END IF

	CALL ui.Interface.setImage("qx://application/icon16/webemail/mailbox_received.png")
	CALL ui.Interface.setText("Mail-Inbox")


    CALL fgl_window_open("w_mailbox_detail", 1,1 ,get_form_path("f_mail_inbox_operator_l2"),FALSE)
    CALL populate_mail_inbox_form_labels_g()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL open_operator_mail_inbox_data_source(p_order_field,get_toggle_switch(), mail_act_open_status_filter)

    LET i = 1
    FOREACH c_mail_inbox USING l_operator_email_address  INTO l_mail_arr[i].*

      IF local_debug THEN
        DISPLAY "----- ", i
        DISPLAY "open_mail_inbox() mail_id=", l_mail_arr[i].mail_id
        DISPLAY "open_mail_inbox() recv_date=", l_mail_arr[i].recv_date
        DISPLAY "open_mail_inbox() cont_name=", l_mail_arr[i].cont_name
        DISPLAY "open_mail_inbox() from_email_address=", l_mail_arr[i].from_email_address
        DISPLAY "open_mail_inbox() to_email_address=", l_mail_arr[i].to_email_address
        DISPLAY "open_mail_inbox() short_desc=", l_mail_arr[i].short_desc
        DISPLAY "open_mail_inbox() read_flag=", l_mail_arr[i].read_flag
        DISPLAY "open_mail_inbox() urgent_flag=", l_mail_arr[i].urgent_flag
        DISPLAY "open_mail_inbox() mail_status=", l_mail_arr[i].mail_status
      END IF


      IF l_mail_arr[i].read_flag = "1" THEN
        LET l_mail_arr[i].read_flag = "R"
      ELSE
        LET l_mail_arr[i].read_flag = "U"
      END IF

      IF l_mail_arr[i].mail_status = "1" THEN
        LET l_mail_arr[i].mail_status = "C"
      ELSE
        LET l_mail_arr[i].mail_status = "O"
      END IF


      IF l_mail_arr[i].urgent_flag = "1" THEN
        LET l_mail_arr[i].urgent_flag = "!"
      ELSE
        LET l_mail_arr[i].urgent_flag = "-"
      END IF

      LET i = i + 1
      IF i > 100 THEN
        EXIT FOREACH
      END IF

    END FOREACH
    LET i = i - 1

		IF i > 0 THEN
			CALL l_mail_arr.resize(i)  --correct the last element of the dynamic array
		END IF
	
		
    #CALL set_count(i)
    if local_debug = true then
      display "###############################", i
      display "display array size set by set_count() to", i
      display "###############################", i

    end if
    DISPLAY ARRAY l_mail_arr TO sa_mailbox.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 301

      BEFORE DISPLAY
        CALL set_help_id(410)
        CALL publish_toolbar("MailInbox",0)

      BEFORE ROW ---PREVIEW body
        LET curr_pa = ARR_CURR()
        LET mail = l_mail_arr[curr_pa].mail_id  --retrieve current pos's id from list
        DISPLAY get_long_desc_from_email(mail) TO long_desc
        #l_long_desc_preview
        IF local_debug = 1 THEN
          DISPLAY "open_mail_inbox() curr_pa=", curr_pa
          DISPLAY "open_mail_inbox() mail=", mail
        END IF

      ON KEY(F12,INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)   ---View Email - get more details on email
        LET i = arr_curr()
        LET mail = l_mail_arr[i].mail_id  --retrieve current pos's id from list

        CASE p_accept_action
     
          WHEN 0  --just return id
            EXIT WHILE

          WHEN 1  --view
            CALL email_view(mail,0)--0=incoming email  1=outgoing email
            EXIT DISPLAY

          WHEN 2  --edit
            CALL fgl_winmessage("Not implemented","open_operator_mail_inbox()\Email Edit is not implemented","info")
            #CALL email_edit(mail)
            EXIT DISPLAY

          WHEN 3  --delete
            CALL email_delete(mail)
            EXIT DISPLAY

          WHEN 4  --print
            CALL fgl_winmessage("Not implemented","open_operator_mail_inbox()\Email Print is not implemented","info")
            EXIT DISPLAY

          WHEN 5
            CALL activity_close(mail)
            EXIT DISPLAY

          OTHERWISE
            LET error_msg = "open_operator_mail_inbox(p_operator_id,p_order_field,p_accept_action= " ,p_accept_action
            CALL fgl_winmessage("open_operator_mail_inbox() - 4GL Source Error",error_msg, "error") 
        END CASE

        CALL set_help_id(411)
        #LET curr_pa = ARR_CURR()


        IF local_debug = 1 THEN
          DISPLAY "open_mail_inbox() curr_pa=", curr_pa
          DISPLAY "open_mail_inbox() mail=", mail

        END IF


      ON KEY (F3)   ---view an email
        CALL set_help_id(412)
        LET curr_pa = ARR_CURR()
        LET mail = l_mail_arr[curr_pa].mail_id

        CALL email_view(mail,0)  --0=incoming email  1=outgoing email
        EXIT DISPLAY

      ON KEY (F4)   ---reply to an email
        CALL set_help_id(412)
        LET curr_pa = ARR_CURR()
        LET mail = l_mail_arr[curr_pa].mail_id

        CALL email_reply(mail)
        EXIT DISPLAY


      ON KEY (F5)   ---forward an email
        CALL set_help_id(412)
        LET curr_pa = ARR_CURR()
        LET mail = l_mail_arr[curr_pa].mail_id

        CALL email_forward(mail)
        EXIT DISPLAY

      ON KEY (F6)   ---re-diret email
        CALL set_help_id(412)
        LET curr_pa = ARR_CURR()
        LET mail = l_mail_arr[curr_pa].mail_id

        CALL email_redirect(mail)
        EXIT DISPLAY

      ON KEY (F7)   ---mark email as read
        CALL set_help_id(413)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id

        CALL email_set_read(mail)

        EXIT DISPLAY

      ON KEY (F8)   ---mark email as unread
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL email_set_unread(mail)
        EXIT DISPLAY


      ON KEY (F9)   ---mark email as open
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL email_open(mail)
        EXIT DISPLAY


      ON KEY (F10)   ---mark email as closed
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL email_close(mail)
        EXIT DISPLAY

      ON KEY (F11)   ---delete E-Mail
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL email_delete(mail)
        EXIT DISPLAY

      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "mail_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "recv_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "from_email_address"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "to_email_address"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F19)
        LET p_order_field2 = p_order_field

        LET p_order_field = "read_flag"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F20)
        LET p_order_field2 = p_order_field

        LET p_order_field = "urgent_flag"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F21)
        LET p_order_field2 = p_order_field

        LET p_order_field = "mail_status"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F22)  --Open activity emails  
        LET mail_act_open_status_filter = 1  --1=open 2=closed 3=all
        EXIT DISPLAY

      ON KEY(F23)  --Open activity emails  
        LET mail_act_open_status_filter = 2  --1=open 2=closed 3=all
        EXIT DISPLAY

      ON KEY(F24)  --Open activity emails  
        LET mail_act_open_status_filter = 3  --1=open 2=closed 3=all
        EXIT DISPLAY

      ON ACTION "actExitMenu"	
      	LET int_flag = TRUE	
		EXIT DISPLAY

     ON ACTION "actExit"		
      	LET int_flag = TRUE	
		EXIT DISPLAY
	
     ON ACTION "actLogOut"
     	CALL disable_current_session_record()		
      	LET int_flag = TRUE	
		EXIT DISPLAY

    END DISPLAY 

    IF int_flag THEN
      LET int_flag = FALSE
      EXIT WHILE
    END IF

  END WHILE

  CALL set_help_id(410)
  CALL publish_toolbar("MailInbox",0)

  CALL fgl_window_close("w_mailbox_detail") 

  #clean up allocated memory for TEXT/BLOB field
  #FOR i = 1 TO SIZEOF(l_mail_arr)
  #  FREE l_mail_arr[i].long_desc
  #END FOR

  FREE g_data.long_desc
  FREE g_data2.long_desc
  FREE l_long_desc_preview
  #FREE l_sel_rec.long_desc

END FUNCTION


#################################################
# FUNCTION open_operator_mail_outbox_data_source(p_operator_id)
#
# Data Source for operator-Mail-Outbox
#
# RETURN NONE
#################################################
FUNCTION open_operator_mail_outbox_data_source(p_order_field,p_ord_dir,mail_status_filter)
  DEFINE 
    local_debug          SMALLINT,
    sql_stmt             CHAR(2048),
    p_ord_dir            SMALLINT,
    p_ord_dir_str        VARCHAR(4),
    p_order_field        VARCHAR(30),
    mail_status_filter   SMALLINT

  LET local_debug = 0

  IF p_order_field IS NULL  THEN
    LET p_order_field = "mail_id"
  END IF

  IF p_ord_dir IS NULL OR (p_ord_dir >1 OR p_ord_dir < 0) THEN
    LET p_ord_dir = 1
  END IF

  IF p_ord_dir = 0 THEN
    LET p_ord_dir_str = "DESC"
  ELSE
    LET p_ord_dir_str = "ASC"
  END IF


  LET sql_stmt = "SELECT ",
                   "mailbox.mail_id, ",
                   "mailbox.recv_date, ",
                   "contact.cont_name, ",
                   "mailbox.from_email_address, ", 
                   "mailbox.to_email_address, ", 
                   "activity.short_desc, ",
                   "mailbox.urgent_flag, ",
                   "mailbox.mail_status, ",
                   "activity.long_desc ",
      
                 "FROM  mailbox, activity, contact ",
                 "WHERE mailbox.from_email_address = ? ",
                   #?=l_operator_email_address
                   "AND mailbox.activity_id = activity.activity_id ",
                   "AND mailbox.to_email_address = contact.cont_email ",
                   "AND activity.act_type = 1 " 
                   #"AND mailbox.mail_status = ? " 

  CASE mail_status_filter
    WHEN 1
      LET sql_stmt = sql_stmt CLIPPED, " AND mailbox.mail_status = 0 "
    WHEN 2
      LET sql_stmt = sql_stmt CLIPPED, " AND mailbox.mail_status = 1 "
    WHEN 3
      #don't add this condition
      #LET sql_stmt = sql_stmt CLIPPED, " AND mailbox.mail_status = 1 "
    OTHERWISE
      LET tmp_str = "Open_operator_mail_outbox_data_source() - mail_status_filter=", mail_status_filter
      CALL fgl_winmessage("Internal 4gl demo source error",tmp_str, "error")
  END CASE 

  IF p_order_field IS NOT NULL THEN
    LET sql_stmt = sql_stmt CLIPPED, " ORDER BY ", p_order_field CLIPPED, " ", p_ord_dir_str CLIPPED
  END IF

  IF local_debug THEN
    DISPLAY "open_operator_mail_outbox_data_source() - SQL"
    DISPLAY sql_stmt[1,100]
    DISPLAY sql_stmt[101,200]
    DISPLAY sql_stmt[201,300]
    DISPLAY sql_stmt[301,400]
    DISPLAY sql_stmt[401,500]
  END IF

  PREPARE p_mail_outbox FROM sql_stmt
  DECLARE c_mail_outbox CURSOR FOR p_mail_outbox

END FUNCTION


#################################################
# FUNCTION open_operator_mail_outbox(p_operator_id)
#
# Display Mail-Outbox from current operator
#
# RETURN NONE
#################################################
FUNCTION open_operator_mail_outbox(p_operator_id,p_order_field,p_accept_action)
  DEFINE 
    p_operator_id                 LIKE operator.operator_id,
    i                             INTEGER,
    #curr_pa        SMALLINT,
    p_operator_recv_id            LIKE contact.cont_name,
    p_operator2                   LIKE contact.cont_id,
    p_operator_subject            LIKE activity.short_desc,
    mail                          LIKE activity.activity_id,
    local_debug                   BOOLEAN,
    l_operator_email_address      VARCHAR(100),
    l_mail_arr                    DYNAMIC ARRAY OF t_mailbox_out_scroll_rec,
    l_long_desc_preview           LIKE activity.long_desc,
    p_accept_action               SMALLINT,
    p_order_field,p_order_field2  VARCHAR(128), 
    error_msg                     VARCHAR(200),
    mail_status_filter            SMALLINT

  LET local_debug = FALSE  --0=off 1=on

  LET l_operator_email_address = get_operator_email_address(p_operator_id)
  LET mail_status_filter = 1
  IF local_debug = 1 THEN
    DISPLAY "open_mail_inbox() p_operator_id=",p_operator_id
    DISPLAY "open_mail_inbox() l_operator_email_address=",l_operator_email_address
  END IF

	CALL ui.Interface.setImage("qx://application/icon16/webemail/mailbox_sent.png")
	CALL ui.Interface.setText("Mail-Outbox")
	

    CALL fgl_window_open("w_mailbox_detail", 1,1, get_form_path("f_mail_outbox_operator_l2"),FALSE)
    CALL fgl_settitle(get_str(523))  --"CMS Demo - E-Mail Outbox")
    CALL grid_header_mail_outbox_operator()
    CALL populate_mail_outbox_form_labels_g()

  WHILE TRUE
    #If the user clicks twice the same column header, it will change the sorting order
    #If the user clicks a different column header, it needs to be reset to Ascending
    IF p_order_field <> p_order_field2 THEN
      CALL toggle_switch_on()
    END IF

    CALL open_operator_mail_outbox_data_source(p_order_field,get_toggle_switch(),mail_status_filter)

    LET i = 1
    FOREACH c_mail_outbox USING l_operator_email_address INTO l_mail_arr[i].*


      IF local_debug = 1 THEN
        DISPLAY "open_mail_inbox() mail_id=", l_mail_arr[i].mail_id
        DISPLAY "open_mail_inbox() recv_date=", l_mail_arr[i].recv_date
        DISPLAY "open_mail_inbox() cont_name=", l_mail_arr[i].cont_name
        DISPLAY "open_mail_inbox() from_email_address=", l_mail_arr[i].from_email_address
        DISPLAY "open_mail_inbox() to_email_address=", l_mail_arr[i].to_email_address
        DISPLAY "open_mail_inbox() short_desc=", l_mail_arr[i].short_desc
        DISPLAY "open_mail_inbox() urgent_flag=", l_mail_arr[i].urgent_flag
        DISPLAY "open_mail_inbox() mail_status=", l_mail_arr[i].mail_status

      END IF


      IF l_mail_arr[i].mail_status THEN
        LET l_mail_arr[i].mail_status = "S"  --email already sent
      ELSE
        LET l_mail_arr[i].mail_status = "Q"  --Q=mail queue
      END IF

      IF l_mail_arr[i].urgent_flag THEN
        LET l_mail_arr[i].urgent_flag = "!"  --high priority
      ELSE
        LET l_mail_arr[i].urgent_flag = "-"  --normal priority
      END IF

      LET i = i + 1
    END FOREACH

    LET i = i - 1

		IF i > 0 THEN
			CALL l_mail_arr.resize(i) --correct the last element of the dynamic array
		END IF
		    
		
    #CALL set_count(i)
    if local_debug = true then
      display "###############################", i
      display "display array size set by set_count() to", i
      display "###############################", i

    end if


    DISPLAY ARRAY l_mail_arr TO sa_mailbox.* 
      ATTRIBUTES(CURRENT ROW DISPLAY = "BLUE, REVERSE") HELP 302

      BEFORE DISPLAY
        CALL set_help_id(410)
        CALL publish_toolbar("MailOutbox",0)

      BEFORE ROW ---PREVIEW body
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id  --retrieve current pos's id from list
        DISPLAY get_long_desc_from_email(mail) TO long_desc
        IF local_debug = 1 THEN
          DISPLAY "open_mail_outbox() i=", i
          DISPLAY "open_mail_outbox() mail=", mail
        END IF

      ON KEY (INTERRUPT)
        EXIT WHILE

      ON KEY (ACCEPT)   ---View Email - get more details on email
        CALL set_help_id(411)
        LET i = ARR_CURR()

        LET mail = l_mail_arr[i].mail_id  --retrieve current pos's id from list

        IF local_debug = 1 THEN
          DISPLAY "open_mail_inbox() i=", i
          DISPLAY "open_mail_inbox() mail=", mail

        END IF

        LET i = arr_curr()
        LET mail = l_mail_arr[i].mail_id  --retrieve current pos's id from list

        CASE p_accept_action
     
          WHEN 0  --just return id
            EXIT WHILE

          WHEN 1  --view
            CALL email_view(mail,1) --0=incoming email  1=outgoing email
            EXIT DISPLAY

          WHEN 2  --edit
            CALL fgl_winmessage("Not implemented","open_operator_mail_inbox()\Email Edit is not implemented","info")
            #CALL email_edit(mail)
            EXIT DISPLAY

          WHEN 3  --delete
            #CALL fgl_winmessage("Not implemented","open_operator_mail_inbox()\Email Delete is not implemented","info")
            CALL email_delete(mail)
            EXIT DISPLAY

          WHEN 4  --print
            CALL fgl_winmessage("Not implemented","open_operator_mail_inbox()\Email Print is not implemented","info")
            EXIT DISPLAY

          WHEN 5  --???
            CALL activity_close(mail)
            EXIT DISPLAY

          OTHERWISE
            LET error_msg = "open_operator_mail_inbox(p_operator_id,p_order_field,p_accept_action= " ,p_accept_action
            CALL fgl_winmessage("open_operator_mail_inbox() - 4GL Source Error",error_msg, "error") 
        END CASE

        CALL set_help_id(411)
        #LET curr_pa = ARR_CURR()


        IF local_debug = 1 THEN
          DISPLAY "open_mail_inbox() i=", i
          DISPLAY "open_mail_inbox() mail=", mail

        END IF

      #changing to email send/queue
      ON KEY (F9)   ---mark email as closed
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL set_mail_status_to_sent(mail)
        EXIT DISPLAY

{
      ON KEY (F10)   ---mark email as open
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL set_mail_status_to_queued(mail)
        EXIT DISPLAY
}


      ON KEY (F11)   ---delete E-Mail
        CALL set_help_id(414)
        LET i = ARR_CURR()
        LET mail = l_mail_arr[i].mail_id
        CALL email_delete(mail)
        EXIT DISPLAY


      #Column Sort short cuts
      ON KEY(F13)
        LET p_order_field2 = p_order_field

        LET p_order_field = "mail_id"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F14)
        LET p_order_field2 = p_order_field

        LET p_order_field = "recv_date"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F15)
        LET p_order_field2 = p_order_field

        LET p_order_field = "cont_name"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      ON KEY(F16)
        LET p_order_field2 = p_order_field

        LET p_order_field = "from_email_address"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY



      ON KEY(F17)
        LET p_order_field2 = p_order_field

        LET p_order_field = "to_email_address"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F18)
        LET p_order_field2 = p_order_field

        LET p_order_field = "short_desc"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY


      ON KEY(F19)
        LET p_order_field2 = p_order_field

        LET p_order_field = "urgent_flag"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F20)
        LET p_order_field2 = p_order_field

        LET p_order_field = "mail_status"

        IF p_order_field = p_order_field2 THEN
          CALL toggle_switch()
        END IF

        EXIT DISPLAY

      ON KEY(F21)
        LET mail_status_filter = 1
        EXIT DISPLAY

      ON KEY(F22)
        LET mail_status_filter = 2
        EXIT DISPLAY


      ON KEY(F23)
        LET mail_status_filter = 3
        EXIT DISPLAY


      AFTER DISPLAY

    END DISPLAY 

  IF int_flag THEN
    LET int_flag = FALSE
    EXIT WHILE 
  END IF

  END WHILE

  CALL set_help_id(410)
  CALL publish_toolbar("MailOutbox",1)

  CALL fgl_window_close("w_mailbox_detail") 

  #FREE g_data.long_desc
  #FREE g_data2.long_desc
  #FREE l_sel_rec.long_desc
  FREE l_long_desc_preview
END FUNCTION

